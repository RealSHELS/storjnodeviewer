
unit iphsshtunnel;



{$UNDEF USE_DYNAMIC_LOADING}
{$UNDEF USE_EXPLICIT_INIT}
{$UNDEF ANSICHAR_IS_BYTE}
{$UNDEF NO_ANSI}
{$UNDEF USE_CDECL}

{$IFDEF ENFORCE_DYNAMIC_LOADING}
  {$DEFINE USE_DYNAMIC_LOADING}
  {$DEFINE USEIPWORKSSSHDLL}
{$ENDIF}

{$IFDEF FPC}  //Lazarus
  {$IFDEF FPC_UNICODESTRINGS}
    // ensure that mode Delphi Unicode is enabled, not a modeswitch unicodestrings
    {$MODE DELPHIUNICODE}
  {$ELSE}
    {$MODE DELPHI}
  {$ENDIF}
  {$DEFINE USEIPWORKSSSHDLL}
  {$IFDEF DARWIN}
    {$DEFINE MACOS}
  {$ENDIF}
{$ENDIF}

{$IFNDEF FPC}
  {$IF CompilerVersion >= 24} // XE3+
    {$LEGACYIFEND ON}
  {$IFEND}
{$ENDIF}

{$IF DEFINED(LINUX) OR DEFINED(ANDROID) OR DEFINED(MACOS) OR DEFINED(IOS)}
  {$DEFINE USEIPWORKSSSHDLL}
  {$DEFINE ANSICHAR_IS_BYTE}
  {$DEFINE NO_ANSI}
  {$DEFINE USE_CDECL}
{$IFEND}

{$IF DEFINED(LINUX) OR DEFINED(MACOS)}
  {$IFNDEF IOS}
    {$DEFINE USE_DYNAMIC_LOADING}
  {$ENDIF}
{$IFEND}

{$IF DEFINED(ANDROID)}
  {$DEFINE USE_EXPLICIT_INIT}
{$IFEND}

{$IFDEF USE_IPWORKSSSH_ANDROID_SHARED_LIBRARY}
  {$DEFINE USE_DYNAMIC_LOADING}
{$ENDIF}


{$IF DEFINED(UNICODE) AND NOT DEFINED(MSWINDOWS)}
  {$DEFINE NONWINUNICODE}
{$IFEND}



{$IFNDEF USEIPWORKSSSHDLL}
  // in case if loading DLL from resources
  {$DEFINE USE_DYNAMIC_LOADING}
{$ENDIF}

{$UNDEF UNICODELIB}
{$IF DEFINED(MSWINDOWS)}
  {$DEFINE UNICODELIB}
{$IFEND}


{$IFDEF UNICODE}
  {$IFNDEF UNICODELIB}
    {$DEFINE UNICODE_ON_ANSI}
  {$ELSE}
    {$DEFINE UNICODE_ON_UNICODE}
  {$ENDIF}
{$ELSE}
  {$IFDEF UNICODELIB}
    {$DEFINE ANSI_ON_UNICODE}
  {$ELSE}
    {$DEFINE ANSI_ON_ANSI}
  {$ENDIF}
{$ENDIF}

interface

uses
{$IFDEF FPC}  //Lazarus
  SysUtils, Classes,
{$ELSE}
  {$IF CompilerVersion >= 23}System.SysUtils{$ELSE}SysUtils{$IFEND}, {$IF CompilerVersion >= 23}System.Classes{$ELSE}Classes{$IFEND}, 
{$ENDIF}
  {$IFDEF FPC}
  dynlibs,
  {$ELSE}
  {$IFDEF POSIX}
  Posix.Base,
  {$ENDIF}
  {$ENDIF}
  iphtypes, iphcore, iphkeys;

type

  (*** Enum Types ***)
  TiphsshtunnelFirewallTypes = iphtypes.TiphFirewallTypes;

  TiphsshtunnelSSHAuthModes = iphtypes.TiphSSHAuthModes;

  TiphsshtunnelSSHCertStoreTypes = iphtypes.TiphCertStoreTypes;

  (*** Event Delegate Types ***)
  TConnectedEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    StatusCode: Integer;
    const Description: String
  ) of Object;


  TConnectionRequestEvent = procedure (
    Sender: TObject;
    const Address: String;
    Port: Integer;
    var Accept: Boolean
  ) of Object;


  TDataInEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    Text: String;
    TextB: TBytes;
    EOL: Boolean
  ) of Object;


  TDisconnectedEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    StatusCode: Integer;
    const Description: String
  ) of Object;


  TErrorEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    ErrorCode: Integer;
    const Description: String
  ) of Object;


  TLogEvent = procedure (
    Sender: TObject;
    LogLevel: Integer;
    const Message: String;
    const LogType: String
  ) of Object;


  TReconnectAttemptEvent = procedure (
    Sender: TObject;
    const Host: String;
    Port: Integer;
    RetryCount: Integer;
    RetriesRemaining: Integer;
    MaxRetryCount: Integer;
    RetryInterval: Integer;
    RetryType: Integer;
    RemoteListeningPort: Integer;
    var Cancel: Boolean
  ) of Object;


  TSSHCustomAuthEvent = procedure (
    Sender: TObject;
    var Packet: String
  ) of Object;


  TSSHKeyboardInteractiveEvent = procedure (
    Sender: TObject;
    const Name: String;
    const Instructions: String;
    const Prompt: String;
    var Response: String;
    EchoResponse: Boolean
  ) of Object;


  TSSHServerAuthenticationEvent = procedure (
    Sender: TObject;
    HostKey: String;
    HostKeyB: TBytes;
    const Fingerprint: String;
    const KeyAlgorithm: String;
    const CertSubject: String;
    const CertIssuer: String;
    const Status: String;
    var Accept: Boolean
  ) of Object;


  TSSHStatusEvent = procedure (
    Sender: TObject;
    const Message: String
  ) of Object;


  (*** Exception Type ***)
  EiphSSHTunnel = class(EIPWorksSSH)
  end;

  (*** Component Type ***)
{$IFNDEF FPC}
  {$IF CompilerVersion >= 23}
  [ComponentPlatformsAttribute(
    pidWin32 or pidWin64
    {$IF CompilerVersion >= 29}
    or pidLinux64
    {$IFEND}
    {$IF CompilerVersion >= 31}
    or pidOSX64
    {$IFEND}
   )]
  {$IFEND}
{$ENDIF}
  TiphSSHTunnel = class(TiphTypesCore)
    private
      (*** Event Handler Procedure Pointers ***)
      FOnConnected: TConnectedEvent;

      FOnConnectionRequest: TConnectionRequestEvent;

      FOnDataIn: TDataInEvent;

      FOnDisconnected: TDisconnectedEvent;

      FOnError: TErrorEvent;

      FOnLog: TLogEvent;

      FOnReconnectAttempt: TReconnectAttemptEvent;

      FOnSSHCustomAuth: TSSHCustomAuthEvent;

      FOnSSHKeyboardInteractive: TSSHKeyboardInteractiveEvent;

      FOnSSHServerAuthentication: TSSHServerAuthenticationEvent;

      FOnSSHStatus: TSSHStatusEvent;

      m_ctl: Pointer;
      (*** Inner objects for types and collections ***)
      FConnections: TiphConnectionList;

      FFirewall: TiphFirewall;

      function HasData: Boolean;
      procedure ReadHnd(Reader: TStream);
      procedure WriteHnd(Writer: TStream);
      function BStr2CStr(pBStr: Pointer; lenBStr: Integer; bArr: TBytes): String;

      procedure SetRuntimeLicense(key: String);
      function GetRuntimeLicense: String;

    protected
      procedure AboutDlg; override;
      procedure DefineProperties(Filer: TFiler); override;

      function ThrowCoreException(Err: Integer; const Desc: {$ifndef NO_ANSI}AnsiString{$else}string{$endif}): EIPWorksSSH;
      procedure TreatErr(Err: Integer; const Desc: String);

      (*** Property Getters/Setters ***)
      function  get_Connected: Boolean;

      procedure set_Connected(valConnected: Boolean);

      function  get_ConnectionBacklog: Integer;

      procedure set_ConnectionBacklog(valConnectionBacklog: Integer);

      function  get_ClientCount: Integer;

      function  get_ClientAcceptData(ClientId: Integer): Boolean;

      procedure set_ClientAcceptData(ClientId: Integer; valClientAcceptData: Boolean);

      function  get_ClientBytesSent(ClientId: Integer): Integer;

      function  get_ClientConnected(ClientId: Integer): Boolean;

      procedure set_ClientConnected(ClientId: Integer; valClientConnected: Boolean);

      function  get_ClientConnectionId(ClientId: Integer): String;

      procedure set_ClientDataToSend(ClientId: Integer; valClientDataToSend: String);

      procedure set_ClientDataToSendB(ClientId: Integer; valClientDataToSend: TBytes);

      function  get_ClientEOL(ClientId: Integer): String;

      procedure set_ClientEOL(ClientId: Integer; valClientEOL: String);

      function  get_ClientEOLB(ClientId: Integer): TBytes;

      procedure set_ClientEOLB(ClientId: Integer; valClientEOL: TBytes);

      function  get_ClientIdleTimeout(ClientId: Integer): Integer;

      procedure set_ClientIdleTimeout(ClientId: Integer; valClientIdleTimeout: Integer);

      function  get_ClientLocalAddress(ClientId: Integer): String;

      function  get_ClientReadyToSend(ClientId: Integer): Boolean;

      function  get_ClientRecordLength(ClientId: Integer): Integer;

      procedure set_ClientRecordLength(ClientId: Integer; valClientRecordLength: Integer);

      function  get_ClientRemoteHost(ClientId: Integer): String;

      function  get_ClientRemotePort(ClientId: Integer): Integer;

      function  get_ClientSingleLineMode(ClientId: Integer): Boolean;

      procedure set_ClientSingleLineMode(ClientId: Integer; valClientSingleLineMode: Boolean);

      function  get_ClientTimeout(ClientId: Integer): Integer;

      procedure set_ClientTimeout(ClientId: Integer; valClientTimeout: Integer);

      function  get_ClientUserData(ClientId: Integer): String;

      procedure set_ClientUserData(ClientId: Integer; valClientUserData: String);

      function  get_ClientUserDataB(ClientId: Integer): TBytes;

      procedure set_ClientUserDataB(ClientId: Integer; valClientUserData: TBytes);

      function  get_DefaultEOL: String;

      procedure set_DefaultEOL(valDefaultEOL: String);

      function  get_DefaultEOLB: TBytes;

      procedure set_DefaultEOLB(valDefaultEOL: TBytes);

      function  get_DefaultSingleLineMode: Boolean;

      procedure set_DefaultSingleLineMode(valDefaultSingleLineMode: Boolean);

      function  get_DefaultTimeout: Integer;

      procedure set_DefaultTimeout(valDefaultTimeout: Integer);

      function  get_FirewallAutoDetect: Boolean;

      procedure set_FirewallAutoDetect(valFirewallAutoDetect: Boolean);

      function  get_FirewallType: TiphsshtunnelFirewallTypes;

      procedure set_FirewallType(valFirewallType: TiphsshtunnelFirewallTypes);

      function  get_FirewallHost: String;

      procedure set_FirewallHost(valFirewallHost: String);

      function  get_FirewallPassword: String;

      procedure set_FirewallPassword(valFirewallPassword: String);

      function  get_FirewallPort: Integer;

      procedure set_FirewallPort(valFirewallPort: Integer);

      function  get_FirewallUser: String;

      procedure set_FirewallUser(valFirewallUser: String);

      function  get_KeepAlive: Boolean;

      procedure set_KeepAlive(valKeepAlive: Boolean);

      function  get_Linger: Boolean;

      procedure set_Linger(valLinger: Boolean);

      function  get_Listening: Boolean;

      procedure set_Listening(valListening: Boolean);

      function  get_LocalHost: String;

      procedure set_LocalHost(valLocalHost: String);

      function  get_LocalPort: Integer;

      procedure set_LocalPort(valLocalPort: Integer);

      function  get_SSHAcceptServerHostKeyEncoded: String;

      procedure set_SSHAcceptServerHostKeyEncoded(valSSHAcceptServerHostKeyEncoded: String);

      function  get_SSHAcceptServerHostKeyEncodedB: TBytes;

      procedure set_SSHAcceptServerHostKeyEncodedB(valSSHAcceptServerHostKeyEncoded: TBytes);

      function  get_SSHAuthMode: TiphsshtunnelSSHAuthModes;

      procedure set_SSHAuthMode(valSSHAuthMode: TiphsshtunnelSSHAuthModes);

      function  get_SSHCertEncoded: String;

      procedure set_SSHCertEncoded(valSSHCertEncoded: String);

      function  get_SSHCertEncodedB: TBytes;

      procedure set_SSHCertEncodedB(valSSHCertEncoded: TBytes);

      function  get_SSHCertStore: String;

      procedure set_SSHCertStore(valSSHCertStore: String);

      function  get_SSHCertStoreB: TBytes;

      procedure set_SSHCertStoreB(valSSHCertStore: TBytes);

      function  get_SSHCertStorePassword: String;

      procedure set_SSHCertStorePassword(valSSHCertStorePassword: String);

      function  get_SSHCertStoreType: TiphsshtunnelSSHCertStoreTypes;

      procedure set_SSHCertStoreType(valSSHCertStoreType: TiphsshtunnelSSHCertStoreTypes);

      function  get_SSHCertSubject: String;

      procedure set_SSHCertSubject(valSSHCertSubject: String);

      function  get_SSHCompressionAlgorithms: String;

      procedure set_SSHCompressionAlgorithms(valSSHCompressionAlgorithms: String);

      function  get_SSHEncryptionAlgorithms: String;

      procedure set_SSHEncryptionAlgorithms(valSSHEncryptionAlgorithms: String);

      function  get_SSHForwardHost: String;

      procedure set_SSHForwardHost(valSSHForwardHost: String);

      function  get_SSHForwardPort: Integer;

      procedure set_SSHForwardPort(valSSHForwardPort: Integer);

      function  get_SSHHost: String;

      procedure set_SSHHost(valSSHHost: String);

      function  get_SSHPassword: String;

      procedure set_SSHPassword(valSSHPassword: String);

      function  get_SSHPort: Integer;

      procedure set_SSHPort(valSSHPort: Integer);

      function  get_SSHUser: String;

      procedure set_SSHUser(valSSHUser: String);


      (*** Property Getters/Setters: OO API ***)
      function  get_Connections: TiphConnectionList;
      function  get_Firewall: TiphFirewall;
      procedure set_Firewall(Value : TiphFirewall);
      
 
    public
      constructor Create(AOwner: TComponent); overload; override;
      constructor Create(AOwner: TComponent; RuntimeLicense: string); reintroduce; overload;
      destructor Destroy; override;
      function ReportEventException(E: Exception; Event: String; ReFire: Boolean = True): Integer;

      property RuntimeLicense: String read GetRuntimeLicense write SetRuntimeLicense;

      procedure SetClientDataToSend(ClientId: Integer; lpClientDataToSend: PLXAnsiChar; lenClientDataToSend: Cardinal);

      procedure SetClientEOL(ClientId: Integer; lpClientEOL: PLXAnsiChar; lenClientEOL: Cardinal);

      procedure SetClientUserData(ClientId: Integer; lpClientUserData: PLXAnsiChar; lenClientUserData: Cardinal);

      procedure SetDefaultEOL(lpDefaultEOL: PLXAnsiChar; lenDefaultEOL: Cardinal);

      procedure SetSSHAcceptServerHostKeyEncoded(lpSSHAcceptServerHostKeyEncoded: PLXAnsiChar; lenSSHAcceptServerHostKeyEncoded: Cardinal);

      procedure SetSSHCertEncoded(lpSSHCertEncoded: PLXAnsiChar; lenSSHCertEncoded: Cardinal);

      procedure SetSSHCertStore(lpSSHCertStore: PLXAnsiChar; lenSSHCertStore: Cardinal);


      (*** Runtime Property Definitions ***)

      property Connected: Boolean read get_Connected write set_Connected;



      property ConnectionBacklog: Integer read get_ConnectionBacklog write set_ConnectionBacklog;



      property ClientCount: Integer read get_ClientCount;



      property ClientAcceptData[ClientId: Integer]: Boolean read get_ClientAcceptData write set_ClientAcceptData;



      property ClientBytesSent[ClientId: Integer]: Integer read get_ClientBytesSent;



      property ClientConnected[ClientId: Integer]: Boolean read get_ClientConnected write set_ClientConnected;



      property ClientConnectionId[ClientId: Integer]: String read get_ClientConnectionId;



      property ClientDataToSend[ClientId: Integer]: String write set_ClientDataToSend;



      property ClientDataToSendB[ClientId: Integer]: TBytes write set_ClientDataToSendB;



      property ClientEOL[ClientId: Integer]: String read get_ClientEOL write set_ClientEOL;



      property ClientEOLB[ClientId: Integer]: TBytes read get_ClientEOLB write set_ClientEOLB;



      property ClientIdleTimeout[ClientId: Integer]: Integer read get_ClientIdleTimeout write set_ClientIdleTimeout;



      property ClientLocalAddress[ClientId: Integer]: String read get_ClientLocalAddress;



      property ClientReadyToSend[ClientId: Integer]: Boolean read get_ClientReadyToSend;



      property ClientRecordLength[ClientId: Integer]: Integer read get_ClientRecordLength write set_ClientRecordLength;



      property ClientRemoteHost[ClientId: Integer]: String read get_ClientRemoteHost;



      property ClientRemotePort[ClientId: Integer]: Integer read get_ClientRemotePort;



      property ClientSingleLineMode[ClientId: Integer]: Boolean read get_ClientSingleLineMode write set_ClientSingleLineMode;



      property ClientTimeout[ClientId: Integer]: Integer read get_ClientTimeout write set_ClientTimeout;



      property ClientUserData[ClientId: Integer]: String read get_ClientUserData write set_ClientUserData;



      property ClientUserDataB[ClientId: Integer]: TBytes read get_ClientUserDataB write set_ClientUserDataB;



      property DefaultEOLB: TBytes read get_DefaultEOLB write set_DefaultEOLB;



      property Listening: Boolean read get_Listening write set_Listening;



      property SSHAcceptServerHostKeyEncoded: String read get_SSHAcceptServerHostKeyEncoded write set_SSHAcceptServerHostKeyEncoded;



      property SSHAcceptServerHostKeyEncodedB: TBytes read get_SSHAcceptServerHostKeyEncodedB write set_SSHAcceptServerHostKeyEncodedB;



      property SSHCertEncoded: String read get_SSHCertEncoded write set_SSHCertEncoded;



      property SSHCertEncodedB: TBytes read get_SSHCertEncodedB write set_SSHCertEncodedB;



      property SSHCertStoreB: TBytes read get_SSHCertStoreB write set_SSHCertStoreB;



      (*** Runtime properties: OO API ***)
      property Connections: TiphConnectionList read get_Connections;

      property Firewall: TiphFirewall read get_Firewall write set_Firewall;


{$IFNDEF DELPHI3}
      (*** Method Definitions ***)
      function  Config(ConfigurationString: String): String;

      procedure Connect();

      function  DecodePacket(EncodedPacket: String): TBytes;

      procedure Disconnect();

      procedure DoEvents();

      function  EncodePacket(Packet: TBytes): String;

      function  GetSSHParam(Payload: TBytes; Field: String): String;

      function  GetSSHParamBytes(Payload: TBytes; Field: String): TBytes;

      procedure Reset();

      function  SetSSHParam(Payload: TBytes; FieldType: String; FieldValue: String): TBytes;

      procedure Shutdown();

      procedure StartListening();

      procedure StopListening();

{$ENDIF}

    published
      (*** Design-time Property Definitions ***)

      property DefaultEOL: String read get_DefaultEOL write set_DefaultEOL;



      property DefaultSingleLineMode: Boolean read get_DefaultSingleLineMode write set_DefaultSingleLineMode default False;



      property DefaultTimeout: Integer read get_DefaultTimeout write set_DefaultTimeout default 0;



      property FirewallAutoDetect: Boolean read get_FirewallAutoDetect write set_FirewallAutoDetect default False;



      property FirewallType: TiphsshtunnelFirewallTypes read get_FirewallType write set_FirewallType default fwNone;



      property FirewallHost: String read get_FirewallHost write set_FirewallHost;



      property FirewallPassword: String read get_FirewallPassword write set_FirewallPassword;



      property FirewallPort: Integer read get_FirewallPort write set_FirewallPort default 0;



      property FirewallUser: String read get_FirewallUser write set_FirewallUser;



      property KeepAlive: Boolean read get_KeepAlive write set_KeepAlive default False;



      property Linger: Boolean read get_Linger write set_Linger default True;



      property LocalHost: String read get_LocalHost write set_LocalHost stored False;



      property LocalPort: Integer read get_LocalPort write set_LocalPort default 0;



      property SSHAuthMode: TiphsshtunnelSSHAuthModes read get_SSHAuthMode write set_SSHAuthMode default amPassword;



      property SSHCertStore: String read get_SSHCertStore write set_SSHCertStore;



      property SSHCertStorePassword: String read get_SSHCertStorePassword write set_SSHCertStorePassword;



      property SSHCertStoreType: TiphsshtunnelSSHCertStoreTypes read get_SSHCertStoreType write set_SSHCertStoreType default cstUser;



      property SSHCertSubject: String read get_SSHCertSubject write set_SSHCertSubject;



      property SSHCompressionAlgorithms: String read get_SSHCompressionAlgorithms write set_SSHCompressionAlgorithms;



      property SSHEncryptionAlgorithms: String read get_SSHEncryptionAlgorithms write set_SSHEncryptionAlgorithms;



      property SSHForwardHost: String read get_SSHForwardHost write set_SSHForwardHost;



      property SSHForwardPort: Integer read get_SSHForwardPort write set_SSHForwardPort default 0;



      property SSHHost: String read get_SSHHost write set_SSHHost;



      property SSHPassword: String read get_SSHPassword write set_SSHPassword;



      property SSHPort: Integer read get_SSHPort write set_SSHPort default 22;



      property SSHUser: String read get_SSHUser write set_SSHUser;


      (*** Event Handler Bindings ***)
      property OnConnected: TConnectedEvent read FOnConnected write FOnConnected;

      property OnConnectionRequest: TConnectionRequestEvent read FOnConnectionRequest write FOnConnectionRequest;

      property OnDataIn: TDataInEvent read FOnDataIn write FOnDataIn;

      property OnDisconnected: TDisconnectedEvent read FOnDisconnected write FOnDisconnected;

      property OnError: TErrorEvent read FOnError write FOnError;

      property OnLog: TLogEvent read FOnLog write FOnLog;

      property OnReconnectAttempt: TReconnectAttemptEvent read FOnReconnectAttempt write FOnReconnectAttempt;

      property OnSSHCustomAuth: TSSHCustomAuthEvent read FOnSSHCustomAuth write FOnSSHCustomAuth;

      property OnSSHKeyboardInteractive: TSSHKeyboardInteractiveEvent read FOnSSHKeyboardInteractive write FOnSSHKeyboardInteractive;

      property OnSSHServerAuthentication: TSSHServerAuthenticationEvent read FOnSSHServerAuthentication write FOnSSHServerAuthentication;

      property OnSSHStatus: TSSHStatusEvent read FOnSSHStatus write FOnSSHStatus;

    end;

    procedure Register;

implementation


{$T-}

{$IFDEF MSWINDOWS} // Both DLL and DRU loading require Windows dependency
{$IFDEF FPC}  //Lazarus
uses Messages;
{$ELSE}
uses {$IF CompilerVersion >= 23}Winapi.Windows{$ELSE}Windows{$IFEND}, {$IF CompilerVersion >= 23}Winapi.Messages{$ELSE}Messages{$IFEND};
{$ENDIF}
{$ENDIF}

const
  PID_SSHTunnel_Connected                                          = 1;

  PID_SSHTunnel_ConnectionBacklog                                  = 2;

  PID_SSHTunnel_ClientCount                                        = 3;

  PID_SSHTunnel_ClientAcceptData                                   = 4;

  PID_SSHTunnel_ClientBytesSent                                    = 5;

  PID_SSHTunnel_ClientConnected                                    = 6;

  PID_SSHTunnel_ClientConnectionId                                 = 7;

  PID_SSHTunnel_ClientDataToSend                                   = 8;

  PID_SSHTunnel_ClientEOL                                          = 9;

  PID_SSHTunnel_ClientIdleTimeout                                  = 10;

  PID_SSHTunnel_ClientLocalAddress                                 = 11;

  PID_SSHTunnel_ClientMaxLineLength                                = 12;

  PID_SSHTunnel_ClientReadyToSend                                  = 13;

  PID_SSHTunnel_ClientRecordLength                                 = 14;

  PID_SSHTunnel_ClientRemoteHost                                   = 15;

  PID_SSHTunnel_ClientRemotePort                                   = 16;

  PID_SSHTunnel_ClientSingleLineMode                               = 17;

  PID_SSHTunnel_ClientTimeout                                      = 18;

  PID_SSHTunnel_ClientUserData                                     = 20;

  PID_SSHTunnel_DefaultEOL                                         = 21;

  PID_SSHTunnel_DefaultSingleLineMode                              = 22;

  PID_SSHTunnel_DefaultTimeout                                     = 23;

  PID_SSHTunnel_FirewallAutoDetect                                 = 24;

  PID_SSHTunnel_FirewallType                                       = 25;

  PID_SSHTunnel_FirewallHost                                       = 26;

  PID_SSHTunnel_FirewallPassword                                   = 27;

  PID_SSHTunnel_FirewallPort                                       = 28;

  PID_SSHTunnel_FirewallUser                                       = 29;

  PID_SSHTunnel_KeepAlive                                          = 30;

  PID_SSHTunnel_Linger                                             = 31;

  PID_SSHTunnel_Listening                                          = 32;

  PID_SSHTunnel_LocalHost                                          = 33;

  PID_SSHTunnel_LocalPort                                          = 34;

  PID_SSHTunnel_SSHAcceptServerHostKeyEncoded                      = 36;

  PID_SSHTunnel_SSHAuthMode                                        = 63;

  PID_SSHTunnel_SSHCertEncoded                                     = 65;

  PID_SSHTunnel_SSHCertStore                                       = 81;

  PID_SSHTunnel_SSHCertStorePassword                               = 82;

  PID_SSHTunnel_SSHCertStoreType                                   = 83;

  PID_SSHTunnel_SSHCertSubject                                     = 84;

  PID_SSHTunnel_SSHCompressionAlgorithms                           = 92;

  PID_SSHTunnel_SSHEncryptionAlgorithms                            = 93;

  PID_SSHTunnel_SSHForwardHost                                     = 94;

  PID_SSHTunnel_SSHForwardPort                                     = 95;

  PID_SSHTunnel_SSHHost                                            = 96;

  PID_SSHTunnel_SSHPassword                                        = 97;

  PID_SSHTunnel_SSHPort                                            = 98;

  PID_SSHTunnel_SSHUser                                            = 99;

  EID_SSHTunnel_Connected = 1;

  EID_SSHTunnel_ConnectionRequest = 2;

  EID_SSHTunnel_DataIn = 3;

  EID_SSHTunnel_Disconnected = 4;

  EID_SSHTunnel_Error = 5;

  EID_SSHTunnel_Log = 6;

  EID_SSHTunnel_ReconnectAttempt = 7;

  EID_SSHTunnel_SSHCustomAuth = 8;

  EID_SSHTunnel_SSHKeyboardInteractive = 9;

  EID_SSHTunnel_SSHServerAuthentication = 10;

  EID_SSHTunnel_SSHStatus = 11;

  MID_SSHTunnel_Config = 2;

  MID_SSHTunnel_Connect = 3;

  MID_SSHTunnel_DecodePacket = 4;

  MID_SSHTunnel_Disconnect = 5;

  MID_SSHTunnel_DoEvents = 6;

  MID_SSHTunnel_EncodePacket = 7;

  MID_SSHTunnel_GetSSHParam = 8;

  MID_SSHTunnel_GetSSHParamBytes = 9;

  MID_SSHTunnel_Reset = 10;

  MID_SSHTunnel_SetSSHParam = 11;

  MID_SSHTunnel_Shutdown = 12;

  MID_SSHTunnel_StartListening = 13;

  MID_SSHTunnel_StopListening = 14;

  CREATE_OPT = {$IFDEF UNICODE}1{$ELSE}0{$ENDIF};
  EVTSTR_OPT = {$IFDEF UNICODE}2{$ELSE}1{$ENDIF};

  {$IFDEF MSWINDOWS}
  DLLNAME = 'IPWORKSSSH22.DLL';
  {$ENDIF}
  {$IFDEF LINUX}
  DLLNAME = 'libipworksssh.so.22.0';
  {$ENDIF}
  {$IFDEF ANDROID}
  {$IFDEF USE_DYNAMIC_LOADING}
  DLLNAME = 'libipworksssh.so.22.0';
  {$ELSE}
  DLLNAME = 'libipworksssh.22.0.a';
  {$ENDIF}
  {$ENDIF}
  {$IFDEF IOS}
  DLLNAME = 'libipworksssh.22.0.a';
  {$ELSE}
  {$IFDEF MACOS}
  DLLNAME = '@rpath/libipworksssh.22.0.dylib';
  {$ENDIF}
  {$ENDIF}

{$WARNINGS OFF}

type
  INTARR = array[0..32] of Integer;
  LPINTARR = ^INTARR;
  LPINT = ^Cardinal;
  MEventHandle = function(lpContext: TiphSSHTunnel; event_id: Integer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  PEventHandle = ^MEventHandle;


{$IFNDEF USEIPWORKSSSHDLL}
var
  pEntryPoint: Pointer;
  pBaseAddress: Pointer;
  iLoadCount: Integer;
{$ELSE}
{$IFDEF USE_DYNAMIC_LOADING}
var
  hLib : {$ifndef FPC}HMODULE{$else}TLibHandle{$endif};
  iLoadCount: Integer;
{$ENDIF USE_DYNAMIC_LOADING}
{$ENDIF USEIPWORKSSSHDLL}


{$IFDEF USE_DYNAMIC_LOADING}
  _IPWorksSSH_EvtStr:                          function(pEvtStr: Pointer; id: Integer; value: Pointer; opt: Integer): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_Stream:                          function(pStream: Pointer; op: Integer; params: LPVOIDARR; retVal: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHTunnel_Create:                function(pMethod: PEventHandle; pObject: TiphSSHTunnel; pKey: Pointer; opts: Integer): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHTunnel_Destroy:               function(p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHTunnel_Set:                   function(p: Pointer; index: Integer; arridx: Integer; value: Pointer; len: Cardinal): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHTunnel_Get:                   function(p: Pointer; index: Integer; arridx: Integer; len: LPINT; llVal: PInt64): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHTunnel_GetLastError:          function(p: Pointer): PLXAnsiChar; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHTunnel_GetLastErrorCode:      function(p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHTunnel_SetLastErrorAndCode:   function(p: Pointer; code: Integer; message: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHTunnel_GetEventError:         function(p: Pointer): PLXAnsiChar; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHTunnel_GetEventErrorCode:     function(p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHTunnel_SetEventErrorAndCode:  function(p: Pointer; code: Integer; message: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  {$IFDEF USE_EXPLICIT_INIT}
  _IPWorksSSH_SSHTunnel_StaticInit:            function(instance: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHTunnel_StaticDestroy:         function(): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  {$ENDIF}
  _IPWorksSSH_SSHTunnel_CheckIndex:            function(p: Pointer; index: Integer; arridx: Integer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHTunnel_Do:                    function(p: Pointer; method_id: Integer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR; llVal: PInt64): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
{$ELSE}
  function _IPWorksSSH_EvtStr                         (pEvtStr: Pointer; id: Integer; value: Pointer; opt: Integer): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_EvtStr' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_Stream                         (pStream: Pointer; op: Integer; params: LPVOIDARR; retVal: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_Stream' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHTunnel_Create               (pMethod: PEventHandle; pObject: TiphSSHTunnel; pKey: Pointer; opts: Integer): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHTunnel_Create' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHTunnel_Destroy              (p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHTunnel_Destroy' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHTunnel_Set                  (p: Pointer; index: Integer; arridx: Integer; value: Pointer; len: Cardinal): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHTunnel_Set' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHTunnel_Get                  (p: Pointer; index: Integer; arridx: Integer; len: LPINT; llVal: PInt64): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHTunnel_Get' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHTunnel_GetLastError         (p: Pointer): PLXAnsiChar; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHTunnel_GetLastError' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHTunnel_GetLastErrorCode     (p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHTunnel_GetLastErrorCode' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHTunnel_SetLastErrorAndCode  (p: Pointer; code: Integer; message: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHTunnel_SetLastErrorAndCode' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHTunnel_GetEventError        (p: Pointer): PLXAnsiChar; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHTunnel_GetEventError' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHTunnel_GetEventErrorCode    (p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHTunnel_GetEventErrorCode' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHTunnel_SetEventErrorAndCode (p: Pointer; code: Integer; message: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHTunnel_SetEventErrorAndCode' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHTunnel_StaticInit           (instance: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHTunnel_StaticInit' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHTunnel_StaticDestroy        (): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHTunnel_StaticDestroy' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHTunnel_CheckIndex           (p: Pointer; index: Integer; arridx: Integer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHTunnel_CheckIndex' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHTunnel_Do                   (p: Pointer; method_id: Integer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR; llVal: PInt64): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHTunnel_Do' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
{$ENDIF}

{$HINTS OFF}

(*** Fire Event Methods ***)
function FireConnected(lpContext: TiphSSHTunnel; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_StatusCode: Integer;

  tmp_Description: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnConnected) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_StatusCode := Integer(params^[1]);

  tmp_Description := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  try lpContext.FOnConnected(lpContext, tmp_ConnectionId, tmp_StatusCode, tmp_Description)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Connected'); end;

end;


function FireConnectionRequest(lpContext: TiphSSHTunnel; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_Address: String;

  tmp_Port: Integer;

  tmp_Accept: Boolean;

begin
  result := 0;
  if not Assigned(lpContext.FOnConnectionRequest) then exit;

  tmp_Address := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[0]);
  tmp_Port := Integer(params^[1]);

  tmp_Accept := Boolean(params^[2]);

  try lpContext.FOnConnectionRequest(lpContext, tmp_Address, tmp_Port, tmp_Accept)
  except on E: Exception do result := lpContext.ReportEventException(E, 'ConnectionRequest'); end;

  params^[2] := Pointer(tmp_Accept);

end;


function FireDataIn(lpContext: TiphSSHTunnel; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_Text: String;
  tmp_TextB: TBytes;

  tmp_EOL: Boolean;

begin
  result := 0;
  if not Assigned(lpContext.FOnDataIn) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  SetLength(tmp_TextB, cbparam^[1]);
  Move(Pointer(params^[1])^, Pointer(tmp_TextB)^, cbparam^[1]); // IMPORTANT: Do NOT multiply the length value!
  tmp_Text := lpContext.BStr2CStr(params^[1], cbparam^[1], tmp_TextB);
  tmp_EOL := Boolean(params^[2]);

  try lpContext.FOnDataIn(lpContext, tmp_ConnectionId, tmp_Text, tmp_TextB, tmp_EOL)
  except on E: Exception do result := lpContext.ReportEventException(E, 'DataIn'); end;

end;


function FireDisconnected(lpContext: TiphSSHTunnel; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_StatusCode: Integer;

  tmp_Description: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnDisconnected) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_StatusCode := Integer(params^[1]);

  tmp_Description := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  try lpContext.FOnDisconnected(lpContext, tmp_ConnectionId, tmp_StatusCode, tmp_Description)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Disconnected'); end;

end;


function FireError(lpContext: TiphSSHTunnel; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_ErrorCode: Integer;

  tmp_Description: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnError) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_ErrorCode := Integer(params^[1]);

  tmp_Description := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  try lpContext.FOnError(lpContext, tmp_ConnectionId, tmp_ErrorCode, tmp_Description)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Error', False); end;

end;


function FireLog(lpContext: TiphSSHTunnel; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_LogLevel: Integer;

  tmp_Message: String;

  tmp_LogType: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnLog) then exit;

  tmp_LogLevel := Integer(params^[0]);

  tmp_Message := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_LogType := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  try lpContext.FOnLog(lpContext, tmp_LogLevel, tmp_Message, tmp_LogType)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Log'); end;

end;


function FireReconnectAttempt(lpContext: TiphSSHTunnel; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_Host: String;

  tmp_Port: Integer;

  tmp_RetryCount: Integer;

  tmp_RetriesRemaining: Integer;

  tmp_MaxRetryCount: Integer;

  tmp_RetryInterval: Integer;

  tmp_RetryType: Integer;

  tmp_RemoteListeningPort: Integer;

  tmp_Cancel: Boolean;

begin
  result := 0;
  if not Assigned(lpContext.FOnReconnectAttempt) then exit;

  tmp_Host := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[0]);
  tmp_Port := Integer(params^[1]);

  tmp_RetryCount := Integer(params^[2]);

  tmp_RetriesRemaining := Integer(params^[3]);

  tmp_MaxRetryCount := Integer(params^[4]);

  tmp_RetryInterval := Integer(params^[5]);

  tmp_RetryType := Integer(params^[6]);

  tmp_RemoteListeningPort := Integer(params^[7]);

  tmp_Cancel := Boolean(params^[8]);

  try lpContext.FOnReconnectAttempt(lpContext, tmp_Host, tmp_Port, tmp_RetryCount, tmp_RetriesRemaining, tmp_MaxRetryCount, tmp_RetryInterval, tmp_RetryType, tmp_RemoteListeningPort, tmp_Cancel)
  except on E: Exception do result := lpContext.ReportEventException(E, 'ReconnectAttempt'); end;

  params^[8] := Pointer(tmp_Cancel);

end;


function FireSSHCustomAuth(lpContext: TiphSSHTunnel; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_Packet: String; // dev platform string
  {$ifdef UNICODE_ON_ANSI}
  tmp_PacketB: LXAnsiString; // library string
  {$endif}

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHCustomAuth) then exit;

  tmp_Packet := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(_IPWorksSSH_EvtStr(params^[0], 0, nil, EVTSTR_OPT));
  try lpContext.FOnSSHCustomAuth(lpContext, tmp_Packet)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHCustomAuth'); end;

  {$ifdef UNICODE_ON_ANSI}
  tmp_PacketB := ToLinuxAnsiString(tmp_Packet);
  {$endif}
  _IPWorksSSH_EvtStr(params^[0], 2, {$ifdef UNICODE_ON_ANSI}@tmp_PacketB[0]{$else}PChar(tmp_Packet){$endif}, EVTSTR_OPT);
end;


function FireSSHKeyboardInteractive(lpContext: TiphSSHTunnel; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_Name: String;

  tmp_Instructions: String;

  tmp_Prompt: String;

  tmp_Response: String; // dev platform string
  {$ifdef UNICODE_ON_ANSI}
  tmp_ResponseB: LXAnsiString; // library string
  {$endif}

  tmp_EchoResponse: Boolean;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHKeyboardInteractive) then exit;

  tmp_Name := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[0]);
  tmp_Instructions := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_Prompt := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_Response := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(_IPWorksSSH_EvtStr(params^[3], 0, nil, EVTSTR_OPT));
  tmp_EchoResponse := Boolean(params^[4]);

  try lpContext.FOnSSHKeyboardInteractive(lpContext, tmp_Name, tmp_Instructions, tmp_Prompt, tmp_Response, tmp_EchoResponse)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHKeyboardInteractive'); end;

  {$ifdef UNICODE_ON_ANSI}
  tmp_ResponseB := ToLinuxAnsiString(tmp_Response);
  {$endif}
  _IPWorksSSH_EvtStr(params^[3], 2, {$ifdef UNICODE_ON_ANSI}@tmp_ResponseB[0]{$else}PChar(tmp_Response){$endif}, EVTSTR_OPT);
end;


function FireSSHServerAuthentication(lpContext: TiphSSHTunnel; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_HostKey: String;
  tmp_HostKeyB: TBytes;

  tmp_Fingerprint: String;

  tmp_KeyAlgorithm: String;

  tmp_CertSubject: String;

  tmp_CertIssuer: String;

  tmp_Status: String;

  tmp_Accept: Boolean;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHServerAuthentication) then exit;

  SetLength(tmp_HostKeyB, cbparam^[0]);
  Move(Pointer(params^[0])^, Pointer(tmp_HostKeyB)^, cbparam^[0]); // IMPORTANT: Do NOT multiply the length value!
  tmp_HostKey := lpContext.BStr2CStr(params^[0], cbparam^[0], tmp_HostKeyB);
  tmp_Fingerprint := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_KeyAlgorithm := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_CertSubject := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[3]);
  tmp_CertIssuer := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[4]);
  tmp_Status := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[5]);
  tmp_Accept := Boolean(params^[6]);

  try lpContext.FOnSSHServerAuthentication(lpContext, tmp_HostKey, tmp_HostKeyB, tmp_Fingerprint, tmp_KeyAlgorithm, tmp_CertSubject, tmp_CertIssuer, tmp_Status, tmp_Accept)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHServerAuthentication'); end;

  params^[6] := Pointer(tmp_Accept);

end;


function FireSSHStatus(lpContext: TiphSSHTunnel; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_Message: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHStatus) then exit;

  tmp_Message := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[0]);
  try lpContext.FOnSSHStatus(lpContext, tmp_Message)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHStatus'); end;

end;



(*** Event Sink ***)
function FireEvents(lpContext: TiphSSHTunnel; event_id: Integer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
var
  x: Integer;
begin
  result := 0;

  case event_id of
    EID_SSHTunnel_Connected: begin result := FireConnected(lpContext, cparam, params, cbparam); end;

    EID_SSHTunnel_ConnectionRequest: begin result := FireConnectionRequest(lpContext, cparam, params, cbparam); end;

    EID_SSHTunnel_DataIn: begin result := FireDataIn(lpContext, cparam, params, cbparam); end;

    EID_SSHTunnel_Disconnected: begin result := FireDisconnected(lpContext, cparam, params, cbparam); end;

    EID_SSHTunnel_Error: begin result := FireError(lpContext, cparam, params, cbparam); end;

    EID_SSHTunnel_Log: begin result := FireLog(lpContext, cparam, params, cbparam); end;

    EID_SSHTunnel_ReconnectAttempt: begin result := FireReconnectAttempt(lpContext, cparam, params, cbparam); end;

    EID_SSHTunnel_SSHCustomAuth: begin result := FireSSHCustomAuth(lpContext, cparam, params, cbparam); end;

    EID_SSHTunnel_SSHKeyboardInteractive: begin result := FireSSHKeyboardInteractive(lpContext, cparam, params, cbparam); end;

    EID_SSHTunnel_SSHServerAuthentication: begin result := FireSSHServerAuthentication(lpContext, cparam, params, cbparam); end;

    EID_SSHTunnel_SSHStatus: begin result := FireSSHStatus(lpContext, cparam, params, cbparam); end;

    99999: begin x := 0; end;
  end;
end;

procedure Register;
begin
  iphCore.Register;
  RegisterComponents('IPWorks SSH', [TiphSSHTunnel]);
end;

{*********************************************************************************}
procedure TiphSSHTunnel.SetClientDataToSend(ClientId: Integer; lpClientDataToSend: PLXAnsiChar; lenClientDataToSend: Cardinal);
var
  err: integer;
begin
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_ClientDataToSend, ClientId, Pointer(lpClientDataToSend), lenClientDataToSend);
  if err <> 0 then TreatErr(err, '');
end;

procedure TiphSSHTunnel.SetClientEOL(ClientId: Integer; lpClientEOL: PLXAnsiChar; lenClientEOL: Cardinal);
var
  err: integer;
begin
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_ClientEOL, ClientId, Pointer(lpClientEOL), lenClientEOL);
  if err <> 0 then TreatErr(err, '');
end;

procedure TiphSSHTunnel.SetClientUserData(ClientId: Integer; lpClientUserData: PLXAnsiChar; lenClientUserData: Cardinal);
var
  err: integer;
begin
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_ClientUserData, ClientId, Pointer(lpClientUserData), lenClientUserData);
  if err <> 0 then TreatErr(err, '');
end;

procedure TiphSSHTunnel.SetDefaultEOL(lpDefaultEOL: PLXAnsiChar; lenDefaultEOL: Cardinal);
var
  err: integer;
begin
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_DefaultEOL, 0, Pointer(lpDefaultEOL), lenDefaultEOL);
  if err <> 0 then TreatErr(err, '');
end;

procedure TiphSSHTunnel.SetSSHAcceptServerHostKeyEncoded(lpSSHAcceptServerHostKeyEncoded: PLXAnsiChar; lenSSHAcceptServerHostKeyEncoded: Cardinal);
var
  err: integer;
begin
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_SSHAcceptServerHostKeyEncoded, 0, Pointer(lpSSHAcceptServerHostKeyEncoded), lenSSHAcceptServerHostKeyEncoded);
  if err <> 0 then TreatErr(err, '');
end;

procedure TiphSSHTunnel.SetSSHCertEncoded(lpSSHCertEncoded: PLXAnsiChar; lenSSHCertEncoded: Cardinal);
var
  err: integer;
begin
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_SSHCertEncoded, 0, Pointer(lpSSHCertEncoded), lenSSHCertEncoded);
  if err <> 0 then TreatErr(err, '');
end;

procedure TiphSSHTunnel.SetSSHCertStore(lpSSHCertStore: PLXAnsiChar; lenSSHCertStore: Cardinal);
var
  err: integer;
begin
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_SSHCertStore, 0, Pointer(lpSSHCertStore), lenSSHCertStore);
  if err <> 0 then TreatErr(err, '');
end;


type
  TiphConnectionsConnectionList = class(TiphConnectionList)
  protected
    function OwnerCtl : TiphSSHTunnel;
    // Only need to override one or two methods here
    function CtlGetCount: integer; override;
    procedure CtlSetCount(Value : integer); override;
    function CreateElemInstance(Index : integer): TiphConnection; override;
  end;

  TiphConnectionsConnection = class(TiphConnection)
  protected
    function OwnerCtl : TiphSSHTunnel;
    function GetPropAcceptData: Boolean; override;

    procedure SetPropAcceptData(Value: Boolean); override;

    function GetPropBytesSent: Integer; override;

    function GetPropConnected: Boolean; override;

    procedure SetPropConnected(Value: Boolean); override;

    function GetPropConnectionId: String; override;

    procedure SetPropDataToSend(Value: String); override;

    procedure SetPropDataToSendB(Value: TBytes); override;

    function GetPropEOL: String; override;

    procedure SetPropEOL(Value: String); override;

    function  GetPropEOLB : TBytes; override;

    procedure SetPropEOLB(Value: TBytes); override;

    function GetPropIdleTimeout: Integer; override;

    procedure SetPropIdleTimeout(Value: Integer); override;

    function GetPropLocalAddress: String; override;

    function GetPropReadyToSend: Boolean; override;

    function GetPropRecordLength: Integer; override;

    procedure SetPropRecordLength(Value: Integer); override;

    function GetPropRemoteHost: String; override;

    function GetPropRemotePort: Integer; override;

    function GetPropSingleLineMode: Boolean; override;

    procedure SetPropSingleLineMode(Value: Boolean); override;

    function GetPropTimeout: Integer; override;

    procedure SetPropTimeout(Value: Integer); override;

    function GetPropUserData: String; override;

    procedure SetPropUserData(Value: String); override;

    function  GetPropUserDataB : TBytes; override;

    procedure SetPropUserDataB(Value: TBytes); override;

  end;

function TiphConnectionsConnection.OwnerCtl : TiphSSHTunnel;
begin
  Result := TiphSSHTunnel(FOwnerCtl);
end;

function TiphConnectionsConnection.GetPropAcceptData: Boolean;
begin
  Result := (OwnerCtl.get_ClientAcceptData(Index));
end;


procedure TiphConnectionsConnection.SetPropAcceptData(Value: Boolean);
begin
  OwnerCtl.set_ClientAcceptData(Index, (Value));
end;

function TiphConnectionsConnection.GetPropBytesSent: Integer;
begin
  Result := (OwnerCtl.get_ClientBytesSent(Index));
end;


function TiphConnectionsConnection.GetPropConnected: Boolean;
begin
  Result := (OwnerCtl.get_ClientConnected(Index));
end;


procedure TiphConnectionsConnection.SetPropConnected(Value: Boolean);
begin
  OwnerCtl.set_ClientConnected(Index, (Value));
end;

function TiphConnectionsConnection.GetPropConnectionId: String;
begin
  Result := (OwnerCtl.get_ClientConnectionId(Index));
end;


procedure TiphConnectionsConnection.SetPropDataToSend(Value: String);
begin
  OwnerCtl.set_ClientDataToSend(Index, (Value));
end;

procedure TiphConnectionsConnection.SetPropDataToSendB(Value: TBytes); 
begin
  OwnerCtl.set_ClientDataToSendB(Index, Value);
end;

function TiphConnectionsConnection.GetPropEOL: String;
begin
  Result := (OwnerCtl.get_ClientEOL(Index));
end;


procedure TiphConnectionsConnection.SetPropEOL(Value: String);
begin
  OwnerCtl.set_ClientEOL(Index, (Value));
end;

function  TiphConnectionsConnection.GetPropEOLB : TBytes; 
begin
  Result := OwnerCtl.get_ClientEOLB(Index);
end;


procedure TiphConnectionsConnection.SetPropEOLB(Value: TBytes); 
begin
  OwnerCtl.set_ClientEOLB(Index, Value);
end;

function TiphConnectionsConnection.GetPropIdleTimeout: Integer;
begin
  Result := (OwnerCtl.get_ClientIdleTimeout(Index));
end;


procedure TiphConnectionsConnection.SetPropIdleTimeout(Value: Integer);
begin
  OwnerCtl.set_ClientIdleTimeout(Index, (Value));
end;

function TiphConnectionsConnection.GetPropLocalAddress: String;
begin
  Result := (OwnerCtl.get_ClientLocalAddress(Index));
end;


function TiphConnectionsConnection.GetPropReadyToSend: Boolean;
begin
  Result := (OwnerCtl.get_ClientReadyToSend(Index));
end;


function TiphConnectionsConnection.GetPropRecordLength: Integer;
begin
  Result := (OwnerCtl.get_ClientRecordLength(Index));
end;


procedure TiphConnectionsConnection.SetPropRecordLength(Value: Integer);
begin
  OwnerCtl.set_ClientRecordLength(Index, (Value));
end;

function TiphConnectionsConnection.GetPropRemoteHost: String;
begin
  Result := (OwnerCtl.get_ClientRemoteHost(Index));
end;


function TiphConnectionsConnection.GetPropRemotePort: Integer;
begin
  Result := (OwnerCtl.get_ClientRemotePort(Index));
end;


function TiphConnectionsConnection.GetPropSingleLineMode: Boolean;
begin
  Result := (OwnerCtl.get_ClientSingleLineMode(Index));
end;


procedure TiphConnectionsConnection.SetPropSingleLineMode(Value: Boolean);
begin
  OwnerCtl.set_ClientSingleLineMode(Index, (Value));
end;

function TiphConnectionsConnection.GetPropTimeout: Integer;
begin
  Result := (OwnerCtl.get_ClientTimeout(Index));
end;


procedure TiphConnectionsConnection.SetPropTimeout(Value: Integer);
begin
  OwnerCtl.set_ClientTimeout(Index, (Value));
end;

function TiphConnectionsConnection.GetPropUserData: String;
begin
  Result := (OwnerCtl.get_ClientUserData(Index));
end;


procedure TiphConnectionsConnection.SetPropUserData(Value: String);
begin
  OwnerCtl.set_ClientUserData(Index, (Value));
end;

function  TiphConnectionsConnection.GetPropUserDataB : TBytes; 
begin
  Result := OwnerCtl.get_ClientUserDataB(Index);
end;


procedure TiphConnectionsConnection.SetPropUserDataB(Value: TBytes); 
begin
  OwnerCtl.set_ClientUserDataB(Index, Value);
end;


function TiphConnectionsConnectionList.OwnerCtl : TiphSSHTunnel;
begin
  Result := TiphSSHTunnel(FOwnerCtl);
end;

// Collection's overrides
function TiphConnectionsConnectionList.CtlGetCount: integer;
begin
  Result := OwnerCtl.get_ClientCount();
end;

procedure TiphConnectionsConnectionList.CtlSetCount(Value : integer);
begin
  // The collection is read-only
end;

function TiphConnectionsConnectionList.CreateElemInstance(Index : integer): TiphConnection;
begin
  Result := TiphConnectionsConnection.Create(FOwnerCtl, true);
  Result.Index := Index;
end;

type
  TiphFirewallFirewall = class(TiphFirewall)
  protected
    function OwnerCtl : TiphSSHTunnel;
    function GetPropAutoDetect: Boolean; override;

    procedure SetPropAutoDetect(Value: Boolean); override;

    function GetPropFirewallType: TiphFirewallTypes; override;

    procedure SetPropFirewallType(Value: TiphFirewallTypes); override;

    function GetPropHost: String; override;

    procedure SetPropHost(Value: String); override;

    function GetPropPassword: String; override;

    procedure SetPropPassword(Value: String); override;

    function GetPropPort: Integer; override;

    procedure SetPropPort(Value: Integer); override;

    function GetPropUser: String; override;

    procedure SetPropUser(Value: String); override;

  end;

function TiphFirewallFirewall.OwnerCtl : TiphSSHTunnel;
begin
  Result := TiphSSHTunnel(FOwnerCtl);
end;

function TiphFirewallFirewall.GetPropAutoDetect: Boolean;
begin
  Result := (OwnerCtl.get_FirewallAutoDetect());
end;

procedure TiphFirewallFirewall.SetPropAutoDetect(Value: Boolean);
begin
  OwnerCtl.set_FirewallAutoDetect((Value));
end;

function TiphFirewallFirewall.GetPropFirewallType: TiphFirewallTypes;
begin
  Result := TiphFirewallTypes(OwnerCtl.get_FirewallType());
end;

procedure TiphFirewallFirewall.SetPropFirewallType(Value: TiphFirewallTypes);
begin
  OwnerCtl.set_FirewallType(TiphsshtunnelFirewallTypes(Value));
end;

function TiphFirewallFirewall.GetPropHost: String;
begin
  Result := (OwnerCtl.get_FirewallHost());
end;

procedure TiphFirewallFirewall.SetPropHost(Value: String);
begin
  OwnerCtl.set_FirewallHost((Value));
end;

function TiphFirewallFirewall.GetPropPassword: String;
begin
  Result := (OwnerCtl.get_FirewallPassword());
end;

procedure TiphFirewallFirewall.SetPropPassword(Value: String);
begin
  OwnerCtl.set_FirewallPassword((Value));
end;

function TiphFirewallFirewall.GetPropPort: Integer;
begin
  Result := (OwnerCtl.get_FirewallPort());
end;

procedure TiphFirewallFirewall.SetPropPort(Value: Integer);
begin
  OwnerCtl.set_FirewallPort((Value));
end;

function TiphFirewallFirewall.GetPropUser: String;
begin
  Result := (OwnerCtl.get_FirewallUser());
end;

procedure TiphFirewallFirewall.SetPropUser(Value: String);
begin
  OwnerCtl.set_FirewallUser((Value));
end;



{*********************************************************************************}

constructor TiphSSHTunnel.Create(AOwner: TComponent);
begin
  Create(AOwner, IPWORKSSSH_OEMKEY_81);
  Config('CodePage=65001');
end;

constructor TiphSSHTunnel.Create(AOwner: TComponent; RuntimeLicense: string);
begin
  inherited Create(AOwner);
  m_ctl := nil;

  if @_IPWorksSSH_SSHTunnel_Create <> nil then
    m_ctl := _IPWorksSSH_SSHTunnel_Create(@FireEvents, self, nil, CREATE_OPT);
  if m_ctl = nil then
    TreatErr(-1, 'IPWorks SSH SSHTunnel: Error creating component');
  _IPWorksSSH_SSHTunnel_Set(m_ctl, 8002{PID_KEYCHECK_DLP}, 0, 
    {$ifdef ANSICHAR_IS_BYTE}
    Pointer(ToLinuxAnsiString(RuntimeLicense)),
    {$else}
    Pointer(PAnsiChar(AnsiString(RuntimeLicense))),
    {$endif}
    0);

  FConnections := TiphConnectionsConnectionList.Create(Self, true);

  FFirewall := TiphFirewallFirewall.Create(Self, false);


  try set_DefaultEOL('') except on E:Exception do end;

  try set_DefaultSingleLineMode(false) except on E:Exception do end;

  try set_DefaultTimeout(0) except on E:Exception do end;

  try set_FirewallAutoDetect(false) except on E:Exception do end;

  try set_FirewallType(fwNone) except on E:Exception do end;

  try set_FirewallHost('') except on E:Exception do end;

  try set_FirewallPassword('') except on E:Exception do end;

  try set_FirewallPort(0) except on E:Exception do end;

  try set_FirewallUser('') except on E:Exception do end;

  try set_KeepAlive(false) except on E:Exception do end;

  try set_Linger(true) except on E:Exception do end;

  try set_LocalPort(0) except on E:Exception do end;

  try set_SSHAuthMode(amPassword) except on E:Exception do end;

  try set_SSHCertStore('MY') except on E:Exception do end;

  try set_SSHCertStorePassword('') except on E:Exception do end;

  try set_SSHCertStoreType(cstUser) except on E:Exception do end;

  try set_SSHCertSubject('') except on E:Exception do end;

  try set_SSHCompressionAlgorithms('none,zlib') except on E:Exception do end;

  try set_SSHEncryptionAlgorithms('aes256-ctr,aes192-ctr,aes128-ctr,aes256-cbc,aes192-cbc,aes128-cbc,3des-ctr,3des-cbc,blowfish-cbc,arcfour256,arcfour128,arcfour,cast128-cbc,aes256-gcm@openssh.com,aes128-gcm@openssh.com,chacha20-poly1305@openssh.com') except on E:Exception do end;

  try set_SSHForwardHost('') except on E:Exception do end;

  try set_SSHForwardPort(0) except on E:Exception do end;

  try set_SSHHost('') except on E:Exception do end;

  try set_SSHPassword('') except on E:Exception do end;

  try set_SSHPort(22) except on E:Exception do end;

  try set_SSHUser('') except on E:Exception do end;

end;

destructor TiphSSHTunnel.Destroy;
begin
  FreeAndNil(FConnections);

  FreeAndNil(FFirewall);

  
  
  if m_ctl <> nil then begin
    if @_IPWorksSSH_SSHTunnel_Destroy <> nil then
      _IPWorksSSH_SSHTunnel_Destroy(m_ctl);
  end;
  m_ctl := nil;
  inherited Destroy;
end;

function TiphSSHTunnel.ThrowCoreException(err: Integer; const desc: {$ifndef NO_ANSI}AnsiString{$else}string{$endif}): EIPWorksSSH;
begin
  result := EiphSSHTunnel.CreateCode(err, desc);
end;

function TiphSSHTunnel.ReportEventException(e: Exception; Event: String; ReFire: Boolean): Integer;
var
  msg: String;
begin
  result := -1;
  msg := 'An unhandled error occurred in the ' + Event + ' event handler: ' + E.Message;
  if (m_ctl <> nil) and (@_IPWorksSSH_SSHTunnel_SetEventErrorAndCode <> nil) then begin
    if E is EIPWorksSSH then begin
      result := EIPWorksSSH(e).Code;
      msg := E.Message;
    end;
    _IPWorksSSH_SSHTunnel_SetEventErrorAndCode(m_ctl, result, 
      {$ifdef ANSICHAR_IS_BYTE}
      Pointer(ToLinuxAnsiString(msg)));
      {$else}
      Pointer(PAnsiChar(AnsiString(msg))));
      {$endif}
  end;
  if ReFire and Assigned(FOnError) then FOnError(self, 0, result, msg);
end;

{*********************************************************************************}

procedure TiphSSHTunnel.AboutDlg;
var
  p : LPVOIDARR;
  pc: LPINTARR;
begin
{$IFDEF LINUX}
  inherited AboutDlg;
{$ELSE}
  p := nil;
  pc := nil;
  if @_IPWorksSSH_SSHTunnel_Do <> nil then
    _IPWorksSSH_SSHTunnel_Do(m_ctl, 255, 0, p, pc, nil);
{$ENDIF}
end;

{*********************************************************************************}

procedure TiphSSHTunnel.SetRuntimeLicense(key: String);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, 8001{PID_RUNTIME_LICENSE}, 0, 
    {$ifdef ANSICHAR_IS_BYTE}
    Pointer(ToLinuxAnsiString(key)),
    {$else}
    Pointer(PAnsiChar(AnsiString(key))),
    {$endif}
    0);

  if err <> 0 then TreatErr(err, '');
end;

function TiphSSHTunnel.GetRuntimeLicense: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';
  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, 8001{PID_RUNTIME_LICENSE}, 0, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$ifdef ANSICHAR_IS_BYTE}
  result := FromLinuxAnsiString(PLXAnsiChar(tmp));
  {$else}
  result := PAnsiChar(tmp);
  {$endif}
end;

{*********************************************************************************}

function TiphSSHTunnel.HasData: Boolean;
begin
  result := false;
end;

procedure TiphSSHTunnel.ReadHnd(Reader: TStream);
begin
end;

procedure TiphSSHTunnel.WriteHnd(Writer: TStream);
begin
end;

function TiphSSHTunnel.BStr2CStr(pBStr: Pointer; lenBStr: Integer; bArr: TBytes): String;
{$IF DEFINED(UNICODE) AND DEFINED(MSWINDOWS)}
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
{$IFEND}
begin
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  param[0] := pBStr; paramcb[0] := lenBStr;
  param[1] := nil;   paramcb[1] := 0;
  param[2] := nil;   paramcb[2] := 0;
  _IPWorksSSH_SSHTunnel_Do(m_ctl, 2003, 2, @param, @paramcb, nil);
  SetLength(Result, Integer(param[2]));
  if Length(Result) > 0 then begin
    param[1] := PChar(Result); paramcb[1] := Length(Result);
    _IPWorksSSH_SSHTunnel_Do(m_ctl, 2003, 2, @param, @paramcb, nil);
  end;
  {$ELSE}
  try
    Result := TEncoding.Default.GetString(bArr);
  except
    on E: Exception do
      SetString(Result, PAnsiChar(pBStr), lenBStr);
   end;
  {$ENDIF}
{$ELSE}
  SetString(Result, PAnsiChar(pBStr), lenBStr);
{$ENDIF}
end;

procedure TiphSSHTunnel.DefineProperties(Filer: TFiler);
begin
  inherited DefineProperties(Filer);
  Filer.DefineBinaryProperty('RegHnd', ReadHnd, WriteHnd, HasData);
end;

{*********************************************************************************}

procedure TiphSSHTunnel.TreatErr(Err: Integer; const desc: String);
var
  msg : {$ifdef NO_ANSI}string{$else}LXAnsiString{$endif};
begin
  {$ifdef NO_ANSI}
  msg := desc;
  {$else}
  {$ifdef ANSICHAR_IS_BYTE}
  msg := ToLinuxAnsiString(desc);
  {$else}
  msg := LXAnsiString(desc);
  {$endif}
  {$endif}
  if Length(msg) = 0 then
  begin
    if @_IPWorksSSH_SSHTunnel_GetLastError <> nil then
      {$ifdef NO_ANSI}
      {$ifdef ANSICHAR_IS_BYTE}
      msg := FromLinuxAnsiString(_IPWorksSSH_SSHTunnel_GetLastError(m_ctl));
      {$else}
      msg := _IPWorksSSH_SSHTunnel_GetLastError(m_ctl);
      {$endif}
      {$else}
      msg := _IPWorksSSH_SSHTunnel_GetLastError(m_ctl);
      {$endif}
  end;

  raise ThrowCoreException(err, msg);
end;

{*********************************************************************************}

(*** Property Getters/Setters ***)
function TiphSSHTunnel.get_Connected: Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_Connected, 0, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


procedure TiphSSHTunnel.set_Connected(valConnected: Boolean);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_Connected, 0, Pointer(valConnected), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_ConnectionBacklog: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_ConnectionBacklog, 0, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSSHTunnel.set_ConnectionBacklog(valConnectionBacklog: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_ConnectionBacklog, 0, Pointer(valConnectionBacklog), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_ClientCount: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_ClientCount, 0, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


function TiphSSHTunnel.get_ClientAcceptData(ClientId: Integer): Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);
  if @_IPWorksSSH_SSHTunnel_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHTunnel_CheckIndex(m_ctl, PID_SSHTunnel_ClientAcceptData, ClientId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for ClientAcceptData');

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_ClientAcceptData, ClientId, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


procedure TiphSSHTunnel.set_ClientAcceptData(ClientId: Integer; valClientAcceptData: Boolean);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_ClientAcceptData, ClientId, Pointer(valClientAcceptData), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_ClientBytesSent(ClientId: Integer): Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);
  if @_IPWorksSSH_SSHTunnel_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHTunnel_CheckIndex(m_ctl, PID_SSHTunnel_ClientBytesSent, ClientId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for ClientBytesSent');

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_ClientBytesSent, ClientId, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


function TiphSSHTunnel.get_ClientConnected(ClientId: Integer): Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);
  if @_IPWorksSSH_SSHTunnel_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHTunnel_CheckIndex(m_ctl, PID_SSHTunnel_ClientConnected, ClientId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for ClientConnected');

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_ClientConnected, ClientId, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


procedure TiphSSHTunnel.set_ClientConnected(ClientId: Integer; valClientConnected: Boolean);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_ClientConnected, ClientId, Pointer(valClientConnected), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_ClientConnectionId(ClientId: Integer): String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';
  if @_IPWorksSSH_SSHTunnel_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHTunnel_CheckIndex(m_ctl, PID_SSHTunnel_ClientConnectionId, ClientId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for ClientConnectionId');

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_ClientConnectionId{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, ClientId, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSSHTunnel.set_ClientDataToSend(ClientId: Integer; valClientDataToSend: String);
var
  err: Integer;
  {$IFDEF NONWINUNICODE}tmp: TBytes;{$ENDIF}
begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_ClientDataToSend+20000, ClientId, Pointer(PWideChar(valClientDataToSend)), Length(valClientDataToSend));
  {$ELSE}
  tmp := TEncoding.UTF8.GetBytes(valClientDataToSend);
  set_ClientDataToSendB(ClientId, tmp);
  {$ENDIF}
{$ELSE}
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_ClientDataToSend, ClientId, Pointer(PAnsiChar(valClientDataToSend)), Length(valClientDataToSend));
{$ENDIF}
  if err <> 0 then TreatErr(err, '');
end;


procedure TiphSSHTunnel.set_ClientDataToSendB(ClientId: Integer; valClientDataToSend: TBytes);
var
  err: Integer;
begin
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_ClientDataToSend, ClientId, Pointer(valClientDataToSend), Length(valClientDataToSend));
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_ClientEOL(ClientId: Integer): String;
var
  tmp: {$IFDEF NONWINUNICODE}TBytes{$ELSE}Pointer; len: Cardinal{$ENDIF};
  err: Integer;
begin
  result := '';
  if @_IPWorksSSH_SSHTunnel_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHTunnel_CheckIndex(m_ctl, PID_SSHTunnel_ClientEOL, ClientId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for ClientEOL');

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := {$IFDEF NONWINUNICODE}get_ClientEOLB(ClientId){$ELSE}_IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_ClientEOL{$IFDEF UNICODE_ON_UNICODE}+20000{$ENDIF}, ClientId, @len, nil){$ENDIF};
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$IFDEF NONWINUNICODE}result := TEncoding.UTF8.GetString(tmp);{$ELSE}SetString(result, PChar(tmp), len);{$ENDIF}
end;


procedure TiphSSHTunnel.set_ClientEOL(ClientId: Integer; valClientEOL: String);
var
  err: Integer;
  {$IFDEF NONWINUNICODE}tmp: TBytes;{$ENDIF}
begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_ClientEOL+20000, ClientId, Pointer(PWideChar(valClientEOL)), Length(valClientEOL));
  {$ELSE}
  tmp := TEncoding.UTF8.GetBytes(valClientEOL);
  set_ClientEOLB(ClientId, tmp);
  {$ENDIF}
{$ELSE}
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_ClientEOL, ClientId, Pointer(PAnsiChar(valClientEOL)), Length(valClientEOL));
{$ENDIF}
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_ClientEOLB(ClientId: Integer): TBytes;
var
  tmp: Pointer;
  len: Cardinal;
  err: Integer;
begin
  result := nil;
  if @_IPWorksSSH_SSHTunnel_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHTunnel_CheckIndex(m_ctl, PID_SSHTunnel_ClientEOL, ClientId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for ClientEOLB');
  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_ClientEOL, ClientId, @len, nil);
  SetLength(result, len);
  Move(Pointer(tmp)^, Pointer(result)^, len); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSSHTunnel.set_ClientEOLB(ClientId: Integer; valClientEOL: TBytes);
var
  err: Integer;
begin
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_ClientEOL, ClientId, Pointer(valClientEOL), Length(valClientEOL));
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_ClientIdleTimeout(ClientId: Integer): Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);
  if @_IPWorksSSH_SSHTunnel_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHTunnel_CheckIndex(m_ctl, PID_SSHTunnel_ClientIdleTimeout, ClientId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for ClientIdleTimeout');

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_ClientIdleTimeout, ClientId, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSSHTunnel.set_ClientIdleTimeout(ClientId: Integer; valClientIdleTimeout: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_ClientIdleTimeout, ClientId, Pointer(valClientIdleTimeout), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_ClientLocalAddress(ClientId: Integer): String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';
  if @_IPWorksSSH_SSHTunnel_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHTunnel_CheckIndex(m_ctl, PID_SSHTunnel_ClientLocalAddress, ClientId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for ClientLocalAddress');

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_ClientLocalAddress{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, ClientId, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphSSHTunnel.get_ClientReadyToSend(ClientId: Integer): Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);
  if @_IPWorksSSH_SSHTunnel_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHTunnel_CheckIndex(m_ctl, PID_SSHTunnel_ClientReadyToSend, ClientId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for ClientReadyToSend');

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_ClientReadyToSend, ClientId, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


function TiphSSHTunnel.get_ClientRecordLength(ClientId: Integer): Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);
  if @_IPWorksSSH_SSHTunnel_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHTunnel_CheckIndex(m_ctl, PID_SSHTunnel_ClientRecordLength, ClientId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for ClientRecordLength');

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_ClientRecordLength, ClientId, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSSHTunnel.set_ClientRecordLength(ClientId: Integer; valClientRecordLength: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_ClientRecordLength, ClientId, Pointer(valClientRecordLength), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_ClientRemoteHost(ClientId: Integer): String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';
  if @_IPWorksSSH_SSHTunnel_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHTunnel_CheckIndex(m_ctl, PID_SSHTunnel_ClientRemoteHost, ClientId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for ClientRemoteHost');

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_ClientRemoteHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, ClientId, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphSSHTunnel.get_ClientRemotePort(ClientId: Integer): Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);
  if @_IPWorksSSH_SSHTunnel_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHTunnel_CheckIndex(m_ctl, PID_SSHTunnel_ClientRemotePort, ClientId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for ClientRemotePort');

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_ClientRemotePort, ClientId, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


function TiphSSHTunnel.get_ClientSingleLineMode(ClientId: Integer): Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);
  if @_IPWorksSSH_SSHTunnel_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHTunnel_CheckIndex(m_ctl, PID_SSHTunnel_ClientSingleLineMode, ClientId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for ClientSingleLineMode');

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_ClientSingleLineMode, ClientId, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


procedure TiphSSHTunnel.set_ClientSingleLineMode(ClientId: Integer; valClientSingleLineMode: Boolean);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_ClientSingleLineMode, ClientId, Pointer(valClientSingleLineMode), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_ClientTimeout(ClientId: Integer): Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);
  if @_IPWorksSSH_SSHTunnel_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHTunnel_CheckIndex(m_ctl, PID_SSHTunnel_ClientTimeout, ClientId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for ClientTimeout');

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_ClientTimeout, ClientId, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSSHTunnel.set_ClientTimeout(ClientId: Integer; valClientTimeout: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_ClientTimeout, ClientId, Pointer(valClientTimeout), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_ClientUserData(ClientId: Integer): String;
var
  tmp: {$IFDEF NONWINUNICODE}TBytes{$ELSE}Pointer; len: Cardinal{$ENDIF};
  err: Integer;
begin
  result := '';
  if @_IPWorksSSH_SSHTunnel_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHTunnel_CheckIndex(m_ctl, PID_SSHTunnel_ClientUserData, ClientId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for ClientUserData');

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := {$IFDEF NONWINUNICODE}get_ClientUserDataB(ClientId){$ELSE}_IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_ClientUserData{$IFDEF UNICODE_ON_UNICODE}+20000{$ENDIF}, ClientId, @len, nil){$ENDIF};
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$IFDEF NONWINUNICODE}result := TEncoding.UTF8.GetString(tmp);{$ELSE}SetString(result, PChar(tmp), len);{$ENDIF}
end;


procedure TiphSSHTunnel.set_ClientUserData(ClientId: Integer; valClientUserData: String);
var
  err: Integer;
  {$IFDEF NONWINUNICODE}tmp: TBytes;{$ENDIF}
begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_ClientUserData+20000, ClientId, Pointer(PWideChar(valClientUserData)), Length(valClientUserData));
  {$ELSE}
  tmp := TEncoding.UTF8.GetBytes(valClientUserData);
  set_ClientUserDataB(ClientId, tmp);
  {$ENDIF}
{$ELSE}
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_ClientUserData, ClientId, Pointer(PAnsiChar(valClientUserData)), Length(valClientUserData));
{$ENDIF}
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_ClientUserDataB(ClientId: Integer): TBytes;
var
  tmp: Pointer;
  len: Cardinal;
  err: Integer;
begin
  result := nil;
  if @_IPWorksSSH_SSHTunnel_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHTunnel_CheckIndex(m_ctl, PID_SSHTunnel_ClientUserData, ClientId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for ClientUserDataB');
  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_ClientUserData, ClientId, @len, nil);
  SetLength(result, len);
  Move(Pointer(tmp)^, Pointer(result)^, len); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSSHTunnel.set_ClientUserDataB(ClientId: Integer; valClientUserData: TBytes);
var
  err: Integer;
begin
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_ClientUserData, ClientId, Pointer(valClientUserData), Length(valClientUserData));
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_DefaultEOL: String;
var
  tmp: {$IFDEF NONWINUNICODE}TBytes{$ELSE}Pointer; len: Cardinal{$ENDIF};
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := {$IFDEF NONWINUNICODE}get_DefaultEOLB{$ELSE}_IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_DefaultEOL{$IFDEF UNICODE_ON_UNICODE}+20000{$ENDIF}, 0, @len, nil){$ENDIF};
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$IFDEF NONWINUNICODE}result := TEncoding.UTF8.GetString(tmp);{$ELSE}SetString(result, PChar(tmp), len);{$ENDIF}
end;


procedure TiphSSHTunnel.set_DefaultEOL(valDefaultEOL: String);
var
  err: Integer;
  {$IFDEF NONWINUNICODE}tmp: TBytes;{$ENDIF}
begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_DefaultEOL+20000, 0, Pointer(PWideChar(valDefaultEOL)), Length(valDefaultEOL));
  {$ELSE}
  tmp := TEncoding.UTF8.GetBytes(valDefaultEOL);
  set_DefaultEOLB(tmp);
  {$ENDIF}
{$ELSE}
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_DefaultEOL, 0, Pointer(PAnsiChar(valDefaultEOL)), Length(valDefaultEOL));
{$ENDIF}
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_DefaultEOLB: TBytes;
var
  tmp: Pointer;
  len: Cardinal;
begin
  result := nil;
  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_DefaultEOL, 0, @len, nil);
  SetLength(result, len);
  Move(Pointer(tmp)^, Pointer(result)^, len); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSSHTunnel.set_DefaultEOLB(valDefaultEOL: TBytes);
var
  err: Integer;
begin
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_DefaultEOL, 0, Pointer(valDefaultEOL), Length(valDefaultEOL));
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_DefaultSingleLineMode: Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_DefaultSingleLineMode, 0, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


procedure TiphSSHTunnel.set_DefaultSingleLineMode(valDefaultSingleLineMode: Boolean);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_DefaultSingleLineMode, 0, Pointer(valDefaultSingleLineMode), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_DefaultTimeout: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_DefaultTimeout, 0, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSSHTunnel.set_DefaultTimeout(valDefaultTimeout: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_DefaultTimeout, 0, Pointer(valDefaultTimeout), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_FirewallAutoDetect: Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_FirewallAutoDetect, 0, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


procedure TiphSSHTunnel.set_FirewallAutoDetect(valFirewallAutoDetect: Boolean);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_FirewallAutoDetect, 0, Pointer(valFirewallAutoDetect), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_FirewallType: TiphsshtunnelFirewallTypes;
var
  tmp: Pointer;
  err: Integer;
begin
  result := TiphsshtunnelFirewallTypes(0);

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_FirewallType, 0, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := TiphsshtunnelFirewallTypes(tmp);
end;


procedure TiphSSHTunnel.set_FirewallType(valFirewallType: TiphsshtunnelFirewallTypes);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_FirewallType, 0, Pointer(valFirewallType), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_FirewallHost: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_FirewallHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSSHTunnel.set_FirewallHost(valFirewallHost: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valFirewallHost);
  {$ENDIF}
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_FirewallHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valFirewallHost){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_FirewallPassword: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_FirewallPassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSSHTunnel.set_FirewallPassword(valFirewallPassword: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valFirewallPassword);
  {$ENDIF}
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_FirewallPassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valFirewallPassword){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_FirewallPort: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_FirewallPort, 0, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSSHTunnel.set_FirewallPort(valFirewallPort: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_FirewallPort, 0, Pointer(valFirewallPort), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_FirewallUser: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_FirewallUser{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSSHTunnel.set_FirewallUser(valFirewallUser: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valFirewallUser);
  {$ENDIF}
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_FirewallUser{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valFirewallUser){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_KeepAlive: Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_KeepAlive, 0, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


procedure TiphSSHTunnel.set_KeepAlive(valKeepAlive: Boolean);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_KeepAlive, 0, Pointer(valKeepAlive), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_Linger: Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_Linger, 0, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


procedure TiphSSHTunnel.set_Linger(valLinger: Boolean);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_Linger, 0, Pointer(valLinger), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_Listening: Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_Listening, 0, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


procedure TiphSSHTunnel.set_Listening(valListening: Boolean);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_Listening, 0, Pointer(valListening), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_LocalHost: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_LocalHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSSHTunnel.set_LocalHost(valLocalHost: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valLocalHost);
  {$ENDIF}
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_LocalHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valLocalHost){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_LocalPort: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_LocalPort, 0, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSSHTunnel.set_LocalPort(valLocalPort: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_LocalPort, 0, Pointer(valLocalPort), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_SSHAcceptServerHostKeyEncoded: String;
var
  tmp: {$IFDEF NONWINUNICODE}TBytes{$ELSE}Pointer; len: Cardinal{$ENDIF};
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := {$IFDEF NONWINUNICODE}get_SSHAcceptServerHostKeyEncodedB{$ELSE}_IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_SSHAcceptServerHostKeyEncoded{$IFDEF UNICODE_ON_UNICODE}+20000{$ENDIF}, 0, @len, nil){$ENDIF};
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$IFDEF NONWINUNICODE}result := TEncoding.UTF8.GetString(tmp);{$ELSE}SetString(result, PChar(tmp), len);{$ENDIF}
end;


procedure TiphSSHTunnel.set_SSHAcceptServerHostKeyEncoded(valSSHAcceptServerHostKeyEncoded: String);
var
  err: Integer;
  {$IFDEF NONWINUNICODE}tmp: TBytes;{$ENDIF}
begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_SSHAcceptServerHostKeyEncoded+20000, 0, Pointer(PWideChar(valSSHAcceptServerHostKeyEncoded)), Length(valSSHAcceptServerHostKeyEncoded));
  {$ELSE}
  tmp := TEncoding.UTF8.GetBytes(valSSHAcceptServerHostKeyEncoded);
  set_SSHAcceptServerHostKeyEncodedB(tmp);
  {$ENDIF}
{$ELSE}
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_SSHAcceptServerHostKeyEncoded, 0, Pointer(PAnsiChar(valSSHAcceptServerHostKeyEncoded)), Length(valSSHAcceptServerHostKeyEncoded));
{$ENDIF}
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_SSHAcceptServerHostKeyEncodedB: TBytes;
var
  tmp: Pointer;
  len: Cardinal;
begin
  result := nil;
  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_SSHAcceptServerHostKeyEncoded, 0, @len, nil);
  SetLength(result, len);
  Move(Pointer(tmp)^, Pointer(result)^, len); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSSHTunnel.set_SSHAcceptServerHostKeyEncodedB(valSSHAcceptServerHostKeyEncoded: TBytes);
var
  err: Integer;
begin
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_SSHAcceptServerHostKeyEncoded, 0, Pointer(valSSHAcceptServerHostKeyEncoded), Length(valSSHAcceptServerHostKeyEncoded));
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_SSHAuthMode: TiphsshtunnelSSHAuthModes;
var
  tmp: Pointer;
  err: Integer;
begin
  result := TiphsshtunnelSSHAuthModes(0);

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_SSHAuthMode, 0, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := TiphsshtunnelSSHAuthModes(tmp);
end;


procedure TiphSSHTunnel.set_SSHAuthMode(valSSHAuthMode: TiphsshtunnelSSHAuthModes);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_SSHAuthMode, 0, Pointer(valSSHAuthMode), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_SSHCertEncoded: String;
var
  tmp: {$IFDEF NONWINUNICODE}TBytes{$ELSE}Pointer; len: Cardinal{$ENDIF};
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := {$IFDEF NONWINUNICODE}get_SSHCertEncodedB{$ELSE}_IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_SSHCertEncoded{$IFDEF UNICODE_ON_UNICODE}+20000{$ENDIF}, 0, @len, nil){$ENDIF};
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$IFDEF NONWINUNICODE}result := TEncoding.UTF8.GetString(tmp);{$ELSE}SetString(result, PChar(tmp), len);{$ENDIF}
end;


procedure TiphSSHTunnel.set_SSHCertEncoded(valSSHCertEncoded: String);
var
  err: Integer;
  {$IFDEF NONWINUNICODE}tmp: TBytes;{$ENDIF}
begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_SSHCertEncoded+20000, 0, Pointer(PWideChar(valSSHCertEncoded)), Length(valSSHCertEncoded));
  {$ELSE}
  tmp := TEncoding.UTF8.GetBytes(valSSHCertEncoded);
  set_SSHCertEncodedB(tmp);
  {$ENDIF}
{$ELSE}
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_SSHCertEncoded, 0, Pointer(PAnsiChar(valSSHCertEncoded)), Length(valSSHCertEncoded));
{$ENDIF}
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_SSHCertEncodedB: TBytes;
var
  tmp: Pointer;
  len: Cardinal;
begin
  result := nil;
  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_SSHCertEncoded, 0, @len, nil);
  SetLength(result, len);
  Move(Pointer(tmp)^, Pointer(result)^, len); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSSHTunnel.set_SSHCertEncodedB(valSSHCertEncoded: TBytes);
var
  err: Integer;
begin
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_SSHCertEncoded, 0, Pointer(valSSHCertEncoded), Length(valSSHCertEncoded));
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_SSHCertStore: String;
var
  tmp: {$IFDEF NONWINUNICODE}TBytes{$ELSE}Pointer; len: Cardinal{$ENDIF};
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := {$IFDEF NONWINUNICODE}get_SSHCertStoreB{$ELSE}_IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_SSHCertStore{$IFDEF UNICODE_ON_UNICODE}+20000{$ENDIF}, 0, @len, nil){$ENDIF};
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$IFDEF NONWINUNICODE}result := TEncoding.UTF8.GetString(tmp);{$ELSE}SetString(result, PChar(tmp), len);{$ENDIF}
end;


procedure TiphSSHTunnel.set_SSHCertStore(valSSHCertStore: String);
var
  err: Integer;
  {$IFDEF NONWINUNICODE}tmp: TBytes;{$ENDIF}
begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_SSHCertStore+20000, 0, Pointer(PWideChar(valSSHCertStore)), Length(valSSHCertStore));
  {$ELSE}
  tmp := TEncoding.UTF8.GetBytes(valSSHCertStore);
  set_SSHCertStoreB(tmp);
  {$ENDIF}
{$ELSE}
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_SSHCertStore, 0, Pointer(PAnsiChar(valSSHCertStore)), Length(valSSHCertStore));
{$ENDIF}
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_SSHCertStoreB: TBytes;
var
  tmp: Pointer;
  len: Cardinal;
begin
  result := nil;
  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_SSHCertStore, 0, @len, nil);
  SetLength(result, len);
  Move(Pointer(tmp)^, Pointer(result)^, len); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSSHTunnel.set_SSHCertStoreB(valSSHCertStore: TBytes);
var
  err: Integer;
begin
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_SSHCertStore, 0, Pointer(valSSHCertStore), Length(valSSHCertStore));
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_SSHCertStorePassword: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_SSHCertStorePassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSSHTunnel.set_SSHCertStorePassword(valSSHCertStorePassword: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHCertStorePassword);
  {$ENDIF}
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_SSHCertStorePassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHCertStorePassword){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_SSHCertStoreType: TiphsshtunnelSSHCertStoreTypes;
var
  tmp: Pointer;
  err: Integer;
begin
  result := TiphsshtunnelSSHCertStoreTypes(0);

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_SSHCertStoreType, 0, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := TiphsshtunnelSSHCertStoreTypes(tmp);
end;


procedure TiphSSHTunnel.set_SSHCertStoreType(valSSHCertStoreType: TiphsshtunnelSSHCertStoreTypes);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_SSHCertStoreType, 0, Pointer(valSSHCertStoreType), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_SSHCertSubject: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_SSHCertSubject{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSSHTunnel.set_SSHCertSubject(valSSHCertSubject: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHCertSubject);
  {$ENDIF}
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_SSHCertSubject{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHCertSubject){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_SSHCompressionAlgorithms: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_SSHCompressionAlgorithms{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSSHTunnel.set_SSHCompressionAlgorithms(valSSHCompressionAlgorithms: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHCompressionAlgorithms);
  {$ENDIF}
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_SSHCompressionAlgorithms{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHCompressionAlgorithms){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_SSHEncryptionAlgorithms: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_SSHEncryptionAlgorithms{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSSHTunnel.set_SSHEncryptionAlgorithms(valSSHEncryptionAlgorithms: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHEncryptionAlgorithms);
  {$ENDIF}
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_SSHEncryptionAlgorithms{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHEncryptionAlgorithms){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_SSHForwardHost: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_SSHForwardHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSSHTunnel.set_SSHForwardHost(valSSHForwardHost: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHForwardHost);
  {$ENDIF}
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_SSHForwardHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHForwardHost){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_SSHForwardPort: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_SSHForwardPort, 0, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSSHTunnel.set_SSHForwardPort(valSSHForwardPort: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_SSHForwardPort, 0, Pointer(valSSHForwardPort), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_SSHHost: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_SSHHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSSHTunnel.set_SSHHost(valSSHHost: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHHost);
  {$ENDIF}
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_SSHHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHHost){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_SSHPassword: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_SSHPassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSSHTunnel.set_SSHPassword(valSSHPassword: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHPassword);
  {$ENDIF}
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_SSHPassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHPassword){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_SSHPort: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_SSHPort, 0, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSSHTunnel.set_SSHPort(valSSHPort: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_SSHPort, 0, Pointer(valSSHPort), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHTunnel.get_SSHUser: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHTunnel_Get(m_ctl, PID_SSHTunnel_SSHUser{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SSHTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSSHTunnel.set_SSHUser(valSSHUser: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SSHTunnel_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHUser);
  {$ENDIF}
  err := _IPWorksSSH_SSHTunnel_Set(m_ctl, PID_SSHTunnel_SSHUser{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHUser){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;



{*********************************************************************************}

(*** Property Getters/Setters: OO API ***)
function  TiphSSHTunnel.get_Connections: TiphConnectionList;
begin
  Result := FConnections;
end;

function  TiphSSHTunnel.get_Firewall: TiphFirewall;
begin
  Result := FFirewall;
end;

procedure TiphSSHTunnel.set_Firewall(Value : TiphFirewall);
begin
  FFirewall.Assign(Value);
end;


{*********************************************************************************}

{$IFNDEF DELPHI3}
(*** Methods ***)
function TiphSSHTunnel.Config(ConfigurationString: String): String;
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
  tmp_ConfigurationString: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := '';

  tmp_ConfigurationString := {$ifndef UNICODE_ON_ANSI}ConfigurationString{$else}ToLinuxAnsiString(ConfigurationString){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_ConfigurationString){$else}@tmp_ConfigurationString[0]{$endif};

  if @_IPWorksSSH_SSHTunnel_Do = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Do(m_ctl, MID_SSHTunnel_Config{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  result := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(param[1]);
end;


procedure TiphSSHTunnel.Connect();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SSHTunnel_Do = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Do(m_ctl, MID_SSHTunnel_Connect{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


function TiphSSHTunnel.DecodePacket(EncodedPacket: String): TBytes;
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
  tmp_EncodedPacket: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := nil;

  tmp_EncodedPacket := {$ifndef UNICODE_ON_ANSI}EncodedPacket{$else}ToLinuxAnsiString(EncodedPacket){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_EncodedPacket){$else}@tmp_EncodedPacket[0]{$endif};

  if @_IPWorksSSH_SSHTunnel_Do = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Do(m_ctl, MID_SSHTunnel_DecodePacket{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  SetLength(result, paramcb[1]);
  Move(Pointer(param[1])^, Pointer(result)^, paramcb[1]); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSSHTunnel.Disconnect();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SSHTunnel_Do = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Do(m_ctl, MID_SSHTunnel_Disconnect{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSSHTunnel.DoEvents();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SSHTunnel_Do = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Do(m_ctl, MID_SSHTunnel_DoEvents{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


function TiphSSHTunnel.EncodePacket(Packet: TBytes): String;
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := '';

  param[0] := Pointer(Packet);
  paramcb[0] := Length(Packet);

  if @_IPWorksSSH_SSHTunnel_Do = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Do(m_ctl, MID_SSHTunnel_EncodePacket{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  result := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(param[1]);
end;


function TiphSSHTunnel.GetSSHParam(Payload: TBytes; Field: String): String;
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
  i, err: Integer;
  tmp_Field: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 2 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := '';

  param[0] := Pointer(Payload);
  paramcb[0] := Length(Payload);

  tmp_Field := {$ifndef UNICODE_ON_ANSI}Field{$else}ToLinuxAnsiString(Field){$endif};
  param[1] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_Field){$else}@tmp_Field[0]{$endif};

  if @_IPWorksSSH_SSHTunnel_Do = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Do(m_ctl, MID_SSHTunnel_GetSSHParam{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 2, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  result := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(param[2]);
end;


function TiphSSHTunnel.GetSSHParamBytes(Payload: TBytes; Field: String): TBytes;
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
  i, err: Integer;
  tmp_Field: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 2 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := nil;

  param[0] := Pointer(Payload);
  paramcb[0] := Length(Payload);

  tmp_Field := {$ifndef UNICODE_ON_ANSI}Field{$else}ToLinuxAnsiString(Field){$endif};
  param[1] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_Field){$else}@tmp_Field[0]{$endif};

  if @_IPWorksSSH_SSHTunnel_Do = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Do(m_ctl, MID_SSHTunnel_GetSSHParamBytes{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 2, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  SetLength(result, paramcb[2]);
  Move(Pointer(param[2])^, Pointer(result)^, paramcb[2]); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSSHTunnel.Reset();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SSHTunnel_Do = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Do(m_ctl, MID_SSHTunnel_Reset{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


function TiphSSHTunnel.SetSSHParam(Payload: TBytes; FieldType: String; FieldValue: String): TBytes;
var
  param: array[0..3] of Pointer;
  paramcb: array[0..3] of Integer;
  i, err: Integer;
  tmp_FieldType: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
  tmp_FieldValue: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 3 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := nil;

  param[0] := Pointer(Payload);
  paramcb[0] := Length(Payload);

  tmp_FieldType := {$ifndef UNICODE_ON_ANSI}FieldType{$else}ToLinuxAnsiString(FieldType){$endif};
  param[1] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_FieldType){$else}@tmp_FieldType[0]{$endif};

  tmp_FieldValue := {$ifndef UNICODE_ON_ANSI}FieldValue{$else}ToLinuxAnsiString(FieldValue){$endif};
  param[2] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_FieldValue){$else}@tmp_FieldValue[0]{$endif};

  if @_IPWorksSSH_SSHTunnel_Do = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Do(m_ctl, MID_SSHTunnel_SetSSHParam{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 3, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  SetLength(result, paramcb[3]);
  Move(Pointer(param[3])^, Pointer(result)^, paramcb[3]); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSSHTunnel.Shutdown();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SSHTunnel_Do = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Do(m_ctl, MID_SSHTunnel_Shutdown{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSSHTunnel.StartListening();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SSHTunnel_Do = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Do(m_ctl, MID_SSHTunnel_StartListening{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSSHTunnel.StopListening();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SSHTunnel_Do = nil then exit;
  err := _IPWorksSSH_SSHTunnel_Do(m_ctl, MID_SSHTunnel_StopListening{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


{$ENDIF}


{$IFDEF USE_DYNAMIC_LOADING}

procedure ClearProcs();
begin
  _IPWorksSSH_EvtStr := nil;
  _IPWorksSSH_Stream := nil;
  _IPWorksSSH_SSHTunnel_Create := nil;
  _IPWorksSSH_SSHTunnel_Destroy := nil;
  _IPWorksSSH_SSHTunnel_Set := nil;
  _IPWorksSSH_SSHTunnel_Get := nil;
  _IPWorksSSH_SSHTunnel_GetLastError := nil;
  _IPWorksSSH_SSHTunnel_GetLastErrorCode := nil;
  _IPWorksSSH_SSHTunnel_SetLastErrorAndCode := nil;
  _IPWorksSSH_SSHTunnel_GetEventError := nil;
  _IPWorksSSH_SSHTunnel_GetEventErrorCode := nil;
  _IPWorksSSH_SSHTunnel_SetEventErrorAndCode := nil;
  {$IFDEF USE_EXPLICIT_INIT}
  _IPWorksSSH_SSHTunnel_StaticInit := nil;
  _IPWorksSSH_SSHTunnel_StaticDestroy := nil;
  {$ENDIF}
  _IPWorksSSH_SSHTunnel_CheckIndex := nil;
  _IPWorksSSH_SSHTunnel_Do := nil;
end;

{$IFDEF USEIPWORKSSSHDLL}
procedure LoadDLL();
begin
  ClearProcs();

  hLib := LoadLibrary(PChar(DLLNAME));
  if hLib = 0 then exit;

  @_IPWorksSSH_EvtStr                         := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_EvtStr');
  @_IPWorksSSH_Stream                         := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_Stream');
  @_IPWorksSSH_SSHTunnel_Create               := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHTunnel_Create');
  @_IPWorksSSH_SSHTunnel_Destroy              := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHTunnel_Destroy');
  @_IPWorksSSH_SSHTunnel_Set                  := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHTunnel_Set');
  @_IPWorksSSH_SSHTunnel_Get                  := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHTunnel_Get');
  @_IPWorksSSH_SSHTunnel_GetLastError         := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHTunnel_GetLastError');
  @_IPWorksSSH_SSHTunnel_GetLastErrorCode     := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHTunnel_GetLastErrorCode');
  @_IPWorksSSH_SSHTunnel_SetLastErrorAndCode  := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHTunnel_SetLastErrorAndCode');
  @_IPWorksSSH_SSHTunnel_GetEventError        := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHTunnel_GetEventError');
  @_IPWorksSSH_SSHTunnel_GetEventErrorCode    := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHTunnel_GetEventErrorCode');
  @_IPWorksSSH_SSHTunnel_SetEventErrorAndCode := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHTunnel_SetEventErrorAndCode');
  {$IFDEF USE_EXPLICIT_INIT}
  @_IPWorksSSH_SSHTunnel_StaticInit           := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHTunnel_StaticInit');
  @_IPWorksSSH_SSHTunnel_StaticDestroy        := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHTunnel_StaticDestroy');
  {$ENDIF}
  @_IPWorksSSH_SSHTunnel_CheckIndex           := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHTunnel_CheckIndex');
  @_IPWorksSSH_SSHTunnel_Do                   := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHTunnel_Do');
end;
{$ELSE}
procedure LoadRESDLL();
var
  hResInfo: HRSRC;
  hResData: HGLOBAL;
  pResData: Pointer;
begin
  ClearProcs();

  hResInfo := FindResource(HInstance, 'ipworksssh22_dta', RT_RCDATA);
  if hResInfo = 0 then exit;

  hResData := LoadResource(HInstance, hResInfo);
  if hResData = 0 then exit;

  pResData := LockResource(hResData);
  if pResData = nil then exit;

  pBaseAddress := IPWorksSSHLoadDRU(pResData, pEntryPoint);
  if Assigned(pBaseAddress) then begin
    @_IPWorksSSH_EvtStr                         := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_EvtStr');
    @_IPWorksSSH_Stream                         := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_Stream');
    @_IPWorksSSH_SSHTunnel_Create               := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHTunnel_Create');
    @_IPWorksSSH_SSHTunnel_Destroy              := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHTunnel_Destroy');
    @_IPWorksSSH_SSHTunnel_Set                  := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHTunnel_Set');
    @_IPWorksSSH_SSHTunnel_Get                  := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHTunnel_Get');
    @_IPWorksSSH_SSHTunnel_GetLastError         := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHTunnel_GetLastError');
    @_IPWorksSSH_SSHTunnel_GetLastErrorCode     := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHTunnel_GetLastErrorCode');
    @_IPWorksSSH_SSHTunnel_SetLastErrorAndCode  := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHTunnel_SetLastErrorAndCode');
    @_IPWorksSSH_SSHTunnel_GetEventError        := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHTunnel_GetEventError');
    @_IPWorksSSH_SSHTunnel_GetEventErrorCode    := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHTunnel_GetEventErrorCode');
    @_IPWorksSSH_SSHTunnel_SetEventErrorAndCode := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHTunnel_SetEventErrorAndCode');
    @_IPWorksSSH_SSHTunnel_CheckIndex           := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHTunnel_CheckIndex');
    @_IPWorksSSH_SSHTunnel_Do                   := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHTunnel_Do');
  end;

  FreeResource(hResData);
end;
{$ENDIF USEIPWORKSSSHDLL}

initialization
begin
  iLoadCount := iLoadCount + 1;
  if iLoadCount > 1 then exit;

  {$IFDEF USEIPWORKSSSHDLL}
  hLib := 0;
  LoadDLL();
  {$ELSE}
  pBaseAddress := nil;
  pEntryPoint := nil;
  LoadResDLL();
  {$ENDIF}

  {$IFDEF USE_EXPLICIT_INIT}
  if @_IPWorksSSH_SSHTunnel_StaticInit <> nil then
    _IPWorksSSH_SSHTunnel_StaticInit(nil);
  {$ENDIF}
end;

finalization
begin
  iLoadCount := iLoadCount - 1;
  if iLoadCount > 0 then exit;

  {$IFDEF USE_EXPLICIT_INIT}
  if @_IPWorksSSH_SSHTunnel_StaticDestroy <> nil then
    _IPWorksSSH_SSHTunnel_StaticDestroy();
  {$ENDIF}

  ClearProcs();

  {$IFDEF USEIPWORKSSSHDLL}
  if hLib <> 0 then
  begin
    {$ifndef FPC}FreeLibrary{$else}UnloadLibrary{$endif}(hLib);
    hLib := 0;
  end;
  {$ELSE}
  IPWorksSSHFreeDRU(pBaseAddress, pEntryPoint);
  pBaseAddress := nil;
  pEntryPoint := nil;
  {$ENDIF}
  iLoadCount := 0;
end;

{$ELSE USE_DYNAMIC_LOADING}

// in case if static library used

{$IFDEF USE_EXPLICIT_INIT}

initialization
  _IPWorksSSH_SSHTunnel_StaticInit(nil);

finalization
  _IPWorksSSH_SSHTunnel_StaticDestroy();

{$ENDIF USE_EXPLICIT_INIT}

{$ENDIF USE_DYNAMIC_LOADING}


end.
