
unit iphsshserver;



{$UNDEF USE_DYNAMIC_LOADING}
{$UNDEF USE_EXPLICIT_INIT}
{$UNDEF ANSICHAR_IS_BYTE}
{$UNDEF NO_ANSI}
{$UNDEF USE_CDECL}

{$IFDEF ENFORCE_DYNAMIC_LOADING}
  {$DEFINE USE_DYNAMIC_LOADING}
  {$DEFINE USEIPWORKSSSHDLL}
{$ENDIF}

{$IFDEF FPC}  //Lazarus
  {$IFDEF FPC_UNICODESTRINGS}
    // ensure that mode Delphi Unicode is enabled, not a modeswitch unicodestrings
    {$MODE DELPHIUNICODE}
  {$ELSE}
    {$MODE DELPHI}
  {$ENDIF}
  {$DEFINE USEIPWORKSSSHDLL}
  {$IFDEF DARWIN}
    {$DEFINE MACOS}
  {$ENDIF}
{$ENDIF}

{$IFNDEF FPC}
  {$IF CompilerVersion >= 24} // XE3+
    {$LEGACYIFEND ON}
  {$IFEND}
{$ENDIF}

{$IF DEFINED(LINUX) OR DEFINED(ANDROID) OR DEFINED(MACOS) OR DEFINED(IOS)}
  {$DEFINE USEIPWORKSSSHDLL}
  {$DEFINE ANSICHAR_IS_BYTE}
  {$DEFINE NO_ANSI}
  {$DEFINE USE_CDECL}
{$IFEND}

{$IF DEFINED(LINUX) OR DEFINED(MACOS)}
  {$IFNDEF IOS}
    {$DEFINE USE_DYNAMIC_LOADING}
  {$ENDIF}
{$IFEND}

{$IF DEFINED(ANDROID)}
  {$DEFINE USE_EXPLICIT_INIT}
{$IFEND}

{$IFDEF USE_IPWORKSSSH_ANDROID_SHARED_LIBRARY}
  {$DEFINE USE_DYNAMIC_LOADING}
{$ENDIF}


{$IF DEFINED(UNICODE) AND NOT DEFINED(MSWINDOWS)}
  {$DEFINE NONWINUNICODE}
{$IFEND}



{$IFNDEF USEIPWORKSSSHDLL}
  // in case if loading DLL from resources
  {$DEFINE USE_DYNAMIC_LOADING}
{$ENDIF}

{$UNDEF UNICODELIB}
{$IF DEFINED(MSWINDOWS)}
  {$DEFINE UNICODELIB}
{$IFEND}


{$IFDEF UNICODE}
  {$IFNDEF UNICODELIB}
    {$DEFINE UNICODE_ON_ANSI}
  {$ELSE}
    {$DEFINE UNICODE_ON_UNICODE}
  {$ENDIF}
{$ELSE}
  {$IFDEF UNICODELIB}
    {$DEFINE ANSI_ON_UNICODE}
  {$ELSE}
    {$DEFINE ANSI_ON_ANSI}
  {$ENDIF}
{$ENDIF}

interface

uses
{$IFDEF FPC}  //Lazarus
  SysUtils, Classes,
{$ELSE}
  {$IF CompilerVersion >= 23}System.SysUtils{$ELSE}SysUtils{$IFEND}, {$IF CompilerVersion >= 23}System.Classes{$ELSE}Classes{$IFEND}, 
{$ENDIF}
  {$IFDEF FPC}
  dynlibs,
  {$ELSE}
  {$IFDEF POSIX}
  Posix.Base,
  {$ENDIF}
  {$ENDIF}
  iphtypes, iphcore, iphkeys;

type

  (*** Enum Types ***)
  TiphsshserverSSHCertStoreTypes = iphtypes.TiphCertStoreTypes;

  (*** Event Delegate Types ***)
  TConnectedEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    StatusCode: Integer;
    const Description: String;
    var CertStoreType: Integer;
    var CertStore: String;
    var CertPassword: String;
    var CertSubject: String
  ) of Object;


  TConnectionRequestEvent = procedure (
    Sender: TObject;
    const Address: String;
    Port: Integer;
    var Accept: Boolean
  ) of Object;


  TDisconnectedEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    StatusCode: Integer;
    const Description: String
  ) of Object;


  TErrorEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    ErrorCode: Integer;
    const Description: String
  ) of Object;


  TLogEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    LogLevel: Integer;
    const Message: String;
    const LogType: String
  ) of Object;


  TSSHChannelClosedEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    ChannelId: Integer
  ) of Object;


  TSSHChannelDataInEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    ChannelId: Integer;
    Data: String;
    DataB: TBytes
  ) of Object;


  TSSHChannelEOFEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    ChannelId: Integer
  ) of Object;


  TSSHChannelOpenedEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    ChannelId: Integer
  ) of Object;


  TSSHChannelOpenRequestEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    ChannelId: Integer;
    const Service: String;
    Parameters: String;
    ParametersB: TBytes;
    var Accept: Boolean
  ) of Object;


  TSSHChannelReadyToSendEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    ChannelId: Integer
  ) of Object;


  TSSHChannelRequestEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    ChannelId: Integer;
    const RequestType: String;
    Packet: String;
    PacketB: TBytes;
    var Success: Boolean
  ) of Object;


  TSSHChannelRequestedEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    ChannelId: Integer;
    const RequestType: String;
    Packet: String;
    PacketB: TBytes
  ) of Object;


  TSSHServiceRequestEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    const Service: String;
    var Accept: Boolean
  ) of Object;


  TSSHStatusEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    const Message: String
  ) of Object;


  TSSHTunnelClosedEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    const Address: String;
    Port: Integer
  ) of Object;


  TSSHTunnelRequestedEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    Direction: Integer;
    const Address: String;
    var Port: Integer;
    var Accept: Boolean
  ) of Object;


  TSSHUserAuthRequestEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    const User: String;
    const Service: String;
    const AuthMethod: String;
    const AuthParam: String;
    var Accept: Boolean;
    var PartialSuccess: Boolean;
    var AvailableMethods: String;
    const KeyAlgorithm: String
  ) of Object;


  (*** Exception Type ***)
  EiphSSHServer = class(EIPWorksSSH)
  end;

  (*** Component Type ***)
{$IFNDEF FPC}
  {$IF CompilerVersion >= 23}
  [ComponentPlatformsAttribute(
    pidWin32 or pidWin64
    {$IF CompilerVersion >= 29}
    or pidLinux64
    {$IFEND}
    {$IF CompilerVersion >= 31}
    or pidOSX64
    {$IFEND}
   )]
  {$IFEND}
{$ENDIF}
  TiphSSHServer = class(TiphTypesCore)
    private
      (*** Event Handler Procedure Pointers ***)
      FOnConnected: TConnectedEvent;

      FOnConnectionRequest: TConnectionRequestEvent;

      FOnDisconnected: TDisconnectedEvent;

      FOnError: TErrorEvent;

      FOnLog: TLogEvent;

      FOnSSHChannelClosed: TSSHChannelClosedEvent;

      FOnSSHChannelDataIn: TSSHChannelDataInEvent;

      FOnSSHChannelEOF: TSSHChannelEOFEvent;

      FOnSSHChannelOpened: TSSHChannelOpenedEvent;

      FOnSSHChannelOpenRequest: TSSHChannelOpenRequestEvent;

      FOnSSHChannelReadyToSend: TSSHChannelReadyToSendEvent;

      FOnSSHChannelRequest: TSSHChannelRequestEvent;

      FOnSSHChannelRequested: TSSHChannelRequestedEvent;

      FOnSSHServiceRequest: TSSHServiceRequestEvent;

      FOnSSHStatus: TSSHStatusEvent;

      FOnSSHTunnelClosed: TSSHTunnelClosedEvent;

      FOnSSHTunnelRequested: TSSHTunnelRequestedEvent;

      FOnSSHUserAuthRequest: TSSHUserAuthRequestEvent;

      m_ctl: Pointer;
      (*** Inner objects for types and collections ***)
      FChannels: TiphSSHChannelList;

      FConnections: TiphSSHConnectionList;

      FKeyboardInteractivePrompts: TiphSSHPromptList;

      function HasData: Boolean;
      procedure ReadHnd(Reader: TStream);
      procedure WriteHnd(Writer: TStream);
      function BStr2CStr(pBStr: Pointer; lenBStr: Integer; bArr: TBytes): String;

      procedure SetRuntimeLicense(key: String);
      function GetRuntimeLicense: String;

    protected
      procedure AboutDlg; override;
      procedure DefineProperties(Filer: TFiler); override;

      function ThrowCoreException(Err: Integer; const Desc: {$ifndef NO_ANSI}AnsiString{$else}string{$endif}): EIPWorksSSH;
      procedure TreatErr(Err: Integer; const Desc: String);

      (*** Property Getters/Setters ***)
      function  get_SSHChannelCount: Integer;

      function  get_BytesSent(SSHChannelId: Integer): Integer;

      function  get_ChannelId(SSHChannelId: Integer): String;

      procedure set_DataToSend(SSHChannelId: Integer; valDataToSend: String);

      procedure set_DataToSendB(SSHChannelId: Integer; valDataToSend: TBytes);

      function  get_ReadyToSend(SSHChannelId: Integer): Boolean;

      function  get_RecordLength(SSHChannelId: Integer): Integer;

      procedure set_RecordLength(SSHChannelId: Integer; valRecordLength: Integer);

      function  get_Service(SSHChannelId: Integer): String;

      function  get_ConnectionBacklog: Integer;

      procedure set_ConnectionBacklog(valConnectionBacklog: Integer);

      function  get_SSHConnectionCount: Integer;

      function  get_SSHConnectionConnected(ConnectionId: Integer): Boolean;

      procedure set_SSHConnectionConnected(ConnectionId: Integer; valSSHConnectionConnected: Boolean);

      function  get_SSHConnectionLocalAddress(ConnectionId: Integer): String;

      function  get_SSHConnectionRemoteHost(ConnectionId: Integer): String;

      function  get_SSHConnectionRemotePort(ConnectionId: Integer): Integer;

      function  get_SSHConnectionTimeout(ConnectionId: Integer): Integer;

      procedure set_SSHConnectionTimeout(ConnectionId: Integer; valSSHConnectionTimeout: Integer);

      function  get_DefaultAuthMethods: String;

      procedure set_DefaultAuthMethods(valDefaultAuthMethods: String);

      function  get_DefaultIdleTimeout: Integer;

      procedure set_DefaultIdleTimeout(valDefaultIdleTimeout: Integer);

      function  get_DefaultTimeout: Integer;

      procedure set_DefaultTimeout(valDefaultTimeout: Integer);

      function  get_KeyboardInteractiveMessage: String;

      procedure set_KeyboardInteractiveMessage(valKeyboardInteractiveMessage: String);

      function  get_KeyboardInteractivePromptCount: Integer;

      procedure set_KeyboardInteractivePromptCount(valKeyboardInteractivePromptCount: Integer);

      function  get_KeyboardInteractivePromptEcho(PromptIndex: Integer): Boolean;

      procedure set_KeyboardInteractivePromptEcho(PromptIndex: Integer; valKeyboardInteractivePromptEcho: Boolean);

      function  get_KeyboardInteractivePromptPrompt(PromptIndex: Integer): String;

      procedure set_KeyboardInteractivePromptPrompt(PromptIndex: Integer; valKeyboardInteractivePromptPrompt: String);

      function  get_Listening: Boolean;

      procedure set_Listening(valListening: Boolean);

      function  get_LocalHost: String;

      procedure set_LocalHost(valLocalHost: String);

      function  get_LocalPort: Integer;

      procedure set_LocalPort(valLocalPort: Integer);

      function  get_SSHCertEncoded: String;

      procedure set_SSHCertEncoded(valSSHCertEncoded: String);

      function  get_SSHCertEncodedB: TBytes;

      procedure set_SSHCertEncodedB(valSSHCertEncoded: TBytes);

      function  get_SSHCertStore: String;

      procedure set_SSHCertStore(valSSHCertStore: String);

      function  get_SSHCertStoreB: TBytes;

      procedure set_SSHCertStoreB(valSSHCertStore: TBytes);

      function  get_SSHCertStorePassword: String;

      procedure set_SSHCertStorePassword(valSSHCertStorePassword: String);

      function  get_SSHCertStoreType: TiphsshserverSSHCertStoreTypes;

      procedure set_SSHCertStoreType(valSSHCertStoreType: TiphsshserverSSHCertStoreTypes);

      function  get_SSHCertSubject: String;

      procedure set_SSHCertSubject(valSSHCertSubject: String);

      function  get_SSHCompressionAlgorithms: String;

      procedure set_SSHCompressionAlgorithms(valSSHCompressionAlgorithms: String);

      function  get_SSHEncryptionAlgorithms: String;

      procedure set_SSHEncryptionAlgorithms(valSSHEncryptionAlgorithms: String);


      (*** Property Getters/Setters: OO API ***)
      function  get_Channels: TiphSSHChannelList;
      function  get_Connections: TiphSSHConnectionList;
      function  get_KeyboardInteractivePrompts: TiphSSHPromptList;
      procedure set_KeyboardInteractivePrompts(Value : TiphSSHPromptList);
      

    public
      constructor Create(AOwner: TComponent); overload; override;
      constructor Create(AOwner: TComponent; RuntimeLicense: string); reintroduce; overload;
      destructor Destroy; override;
      function ReportEventException(E: Exception; Event: String; ReFire: Boolean = True): Integer;

      property RuntimeLicense: String read GetRuntimeLicense write SetRuntimeLicense;

      procedure SetDataToSend(SSHChannelId: Integer; lpDataToSend: PLXAnsiChar; lenDataToSend: Cardinal);

      procedure SetSSHCertEncoded(lpSSHCertEncoded: PLXAnsiChar; lenSSHCertEncoded: Cardinal);

      procedure SetSSHCertStore(lpSSHCertStore: PLXAnsiChar; lenSSHCertStore: Cardinal);


      (*** Runtime Property Definitions ***)

      property SSHChannelCount: Integer read get_SSHChannelCount;



      property BytesSent[SSHChannelId: Integer]: Integer read get_BytesSent;



      property ChannelId[SSHChannelId: Integer]: String read get_ChannelId;



      property DataToSend[SSHChannelId: Integer]: String write set_DataToSend;



      property DataToSendB[SSHChannelId: Integer]: TBytes write set_DataToSendB;



      property ReadyToSend[SSHChannelId: Integer]: Boolean read get_ReadyToSend;



      property RecordLength[SSHChannelId: Integer]: Integer read get_RecordLength write set_RecordLength;



      property Service[SSHChannelId: Integer]: String read get_Service;



      property ConnectionBacklog: Integer read get_ConnectionBacklog write set_ConnectionBacklog;



      property SSHConnectionCount: Integer read get_SSHConnectionCount;



      property SSHConnectionConnected[ConnectionId: Integer]: Boolean read get_SSHConnectionConnected write set_SSHConnectionConnected;



      property SSHConnectionLocalAddress[ConnectionId: Integer]: String read get_SSHConnectionLocalAddress;



      property SSHConnectionRemoteHost[ConnectionId: Integer]: String read get_SSHConnectionRemoteHost;



      property SSHConnectionRemotePort[ConnectionId: Integer]: Integer read get_SSHConnectionRemotePort;



      property SSHConnectionTimeout[ConnectionId: Integer]: Integer read get_SSHConnectionTimeout write set_SSHConnectionTimeout;



      property KeyboardInteractivePromptCount: Integer read get_KeyboardInteractivePromptCount write set_KeyboardInteractivePromptCount;



      property KeyboardInteractivePromptEcho[PromptIndex: Integer]: Boolean read get_KeyboardInteractivePromptEcho write set_KeyboardInteractivePromptEcho;



      property KeyboardInteractivePromptPrompt[PromptIndex: Integer]: String read get_KeyboardInteractivePromptPrompt write set_KeyboardInteractivePromptPrompt;



      property Listening: Boolean read get_Listening write set_Listening;



      property SSHCertEncoded: String read get_SSHCertEncoded write set_SSHCertEncoded;



      property SSHCertEncodedB: TBytes read get_SSHCertEncodedB write set_SSHCertEncodedB;



      property SSHCertStoreB: TBytes read get_SSHCertStoreB write set_SSHCertStoreB;



      (*** Runtime properties: OO API ***)
      property Channels: TiphSSHChannelList read get_Channels;

      property Connections: TiphSSHConnectionList read get_Connections;

      property KeyboardInteractivePrompts: TiphSSHPromptList read get_KeyboardInteractivePrompts write set_KeyboardInteractivePrompts;


{$IFNDEF DELPHI3}
      (*** Method Definitions ***)
      procedure ChangeRecordLength(ChannelId: Integer; RecordLength: Integer);

      procedure CloseChannel(ChannelId: Integer);

      function  Config(ConfigurationString: String): String;

      procedure Disconnect(ConnectionId: Integer);

      procedure DoEvents();

      procedure ExchangeKeys(ConnectionId: Integer);

      function  GetSSHParam(Payload: TBytes; Field: String): String;

      function  GetSSHParamBytes(Payload: TBytes; Field: String): TBytes;

      function  OpenChannel(ConnectionId: Integer; ChannelType: String): String;

      procedure Reset();

      procedure SendBytes(ChannelId: Integer; Data: TBytes);

      procedure SendChannelData(ChannelId: Integer; Data: TBytes);

      procedure SendSSHPacket(ChannelId: Integer; PacketType: Integer; Payload: TBytes);

      procedure SendText(ChannelId: Integer; Text: String);

      function  SetSSHParam(Payload: TBytes; FieldType: String; FieldValue: String): TBytes;

      procedure Shutdown();

      procedure StartListening();

      procedure StopListening();

{$ENDIF}

    published
      (*** Design-time Property Definitions ***)

      property DefaultAuthMethods: String read get_DefaultAuthMethods write set_DefaultAuthMethods;



      property DefaultIdleTimeout: Integer read get_DefaultIdleTimeout write set_DefaultIdleTimeout default 0;



      property DefaultTimeout: Integer read get_DefaultTimeout write set_DefaultTimeout default 60;



      property KeyboardInteractiveMessage: String read get_KeyboardInteractiveMessage write set_KeyboardInteractiveMessage;



      property LocalHost: String read get_LocalHost write set_LocalHost stored False;



      property LocalPort: Integer read get_LocalPort write set_LocalPort default 22;



      property SSHCertStore: String read get_SSHCertStore write set_SSHCertStore;



      property SSHCertStorePassword: String read get_SSHCertStorePassword write set_SSHCertStorePassword;



      property SSHCertStoreType: TiphsshserverSSHCertStoreTypes read get_SSHCertStoreType write set_SSHCertStoreType default cstUser;



      property SSHCertSubject: String read get_SSHCertSubject write set_SSHCertSubject;



      property SSHCompressionAlgorithms: String read get_SSHCompressionAlgorithms write set_SSHCompressionAlgorithms;



      property SSHEncryptionAlgorithms: String read get_SSHEncryptionAlgorithms write set_SSHEncryptionAlgorithms;


      (*** Event Handler Bindings ***)
      property OnConnected: TConnectedEvent read FOnConnected write FOnConnected;

      property OnConnectionRequest: TConnectionRequestEvent read FOnConnectionRequest write FOnConnectionRequest;

      property OnDisconnected: TDisconnectedEvent read FOnDisconnected write FOnDisconnected;

      property OnError: TErrorEvent read FOnError write FOnError;

      property OnLog: TLogEvent read FOnLog write FOnLog;

      property OnSSHChannelClosed: TSSHChannelClosedEvent read FOnSSHChannelClosed write FOnSSHChannelClosed;

      property OnSSHChannelDataIn: TSSHChannelDataInEvent read FOnSSHChannelDataIn write FOnSSHChannelDataIn;

      property OnSSHChannelEOF: TSSHChannelEOFEvent read FOnSSHChannelEOF write FOnSSHChannelEOF;

      property OnSSHChannelOpened: TSSHChannelOpenedEvent read FOnSSHChannelOpened write FOnSSHChannelOpened;

      property OnSSHChannelOpenRequest: TSSHChannelOpenRequestEvent read FOnSSHChannelOpenRequest write FOnSSHChannelOpenRequest;

      property OnSSHChannelReadyToSend: TSSHChannelReadyToSendEvent read FOnSSHChannelReadyToSend write FOnSSHChannelReadyToSend;

      property OnSSHChannelRequest: TSSHChannelRequestEvent read FOnSSHChannelRequest write FOnSSHChannelRequest;

      property OnSSHChannelRequested: TSSHChannelRequestedEvent read FOnSSHChannelRequested write FOnSSHChannelRequested;

      property OnSSHServiceRequest: TSSHServiceRequestEvent read FOnSSHServiceRequest write FOnSSHServiceRequest;

      property OnSSHStatus: TSSHStatusEvent read FOnSSHStatus write FOnSSHStatus;

      property OnSSHTunnelClosed: TSSHTunnelClosedEvent read FOnSSHTunnelClosed write FOnSSHTunnelClosed;

      property OnSSHTunnelRequested: TSSHTunnelRequestedEvent read FOnSSHTunnelRequested write FOnSSHTunnelRequested;

      property OnSSHUserAuthRequest: TSSHUserAuthRequestEvent read FOnSSHUserAuthRequest write FOnSSHUserAuthRequest;

    end;

    procedure Register;

implementation


{$T-}

{$IFDEF MSWINDOWS} // Both DLL and DRU loading require Windows dependency
{$IFDEF FPC}  //Lazarus
uses Messages;
{$ELSE}
uses {$IF CompilerVersion >= 23}Winapi.Windows{$ELSE}Windows{$IFEND}, {$IF CompilerVersion >= 23}Winapi.Messages{$ELSE}Messages{$IFEND};
{$ENDIF}
{$ENDIF}

const
  PID_SSHServer_SSHChannelCount                                    = 1;

  PID_SSHServer_BytesSent                                          = 2;

  PID_SSHServer_ChannelId                                          = 3;

  PID_SSHServer_DataToSend                                         = 4;

  PID_SSHServer_ReadyToSend                                        = 5;

  PID_SSHServer_RecordLength                                       = 6;

  PID_SSHServer_Service                                            = 7;

  PID_SSHServer_ConnectionBacklog                                  = 8;

  PID_SSHServer_SSHConnectionCount                                 = 9;

  PID_SSHServer_SSHConnectionConnected                             = 10;

  PID_SSHServer_SSHConnectionConnectionId                          = 11;

  PID_SSHServer_SSHConnectionLocalAddress                          = 12;

  PID_SSHServer_SSHConnectionRemoteHost                            = 13;

  PID_SSHServer_SSHConnectionRemotePort                            = 14;

  PID_SSHServer_SSHConnectionTimeout                               = 15;

  PID_SSHServer_DefaultAuthMethods                                 = 16;

  PID_SSHServer_DefaultIdleTimeout                                 = 17;

  PID_SSHServer_DefaultTimeout                                     = 18;

  PID_SSHServer_KeyboardInteractiveMessage                         = 19;

  PID_SSHServer_KeyboardInteractivePromptCount                     = 20;

  PID_SSHServer_KeyboardInteractivePromptEcho                      = 21;

  PID_SSHServer_KeyboardInteractivePromptPrompt                    = 22;

  PID_SSHServer_Listening                                          = 23;

  PID_SSHServer_LocalHost                                          = 24;

  PID_SSHServer_LocalPort                                          = 25;

  PID_SSHServer_SSHCertEncoded                                     = 27;

  PID_SSHServer_SSHCertStore                                       = 43;

  PID_SSHServer_SSHCertStorePassword                               = 44;

  PID_SSHServer_SSHCertStoreType                                   = 45;

  PID_SSHServer_SSHCertSubject                                     = 46;

  PID_SSHServer_SSHCompressionAlgorithms                           = 54;

  PID_SSHServer_SSHEncryptionAlgorithms                            = 55;

  EID_SSHServer_Connected = 1;

  EID_SSHServer_ConnectionRequest = 2;

  EID_SSHServer_Disconnected = 3;

  EID_SSHServer_Error = 4;

  EID_SSHServer_Log = 5;

  EID_SSHServer_SSHChannelClosed = 6;

  EID_SSHServer_SSHChannelDataIn = 7;

  EID_SSHServer_SSHChannelEOF = 8;

  EID_SSHServer_SSHChannelOpened = 9;

  EID_SSHServer_SSHChannelOpenRequest = 10;

  EID_SSHServer_SSHChannelReadyToSend = 11;

  EID_SSHServer_SSHChannelRequest = 12;

  EID_SSHServer_SSHChannelRequested = 13;

  EID_SSHServer_SSHServiceRequest = 14;

  EID_SSHServer_SSHStatus = 15;

  EID_SSHServer_SSHTunnelClosed = 16;

  EID_SSHServer_SSHTunnelRequested = 17;

  EID_SSHServer_SSHUserAuthRequest = 18;

  MID_SSHServer_ChangeRecordLength = 2;

  MID_SSHServer_CloseChannel = 3;

  MID_SSHServer_Config = 4;

  MID_SSHServer_Disconnect = 5;

  MID_SSHServer_DoEvents = 6;

  MID_SSHServer_ExchangeKeys = 7;

  MID_SSHServer_GetSSHParam = 8;

  MID_SSHServer_GetSSHParamBytes = 9;

  MID_SSHServer_OpenChannel = 10;

  MID_SSHServer_Reset = 11;

  MID_SSHServer_SendBytes = 12;

  MID_SSHServer_SendChannelData = 13;

  MID_SSHServer_SendSSHPacket = 14;

  MID_SSHServer_SendText = 15;

  MID_SSHServer_SetSSHParam = 16;

  MID_SSHServer_Shutdown = 17;

  MID_SSHServer_StartListening = 18;

  MID_SSHServer_StopListening = 19;

  CREATE_OPT = {$IFDEF UNICODE}1{$ELSE}0{$ENDIF};
  EVTSTR_OPT = {$IFDEF UNICODE}2{$ELSE}1{$ENDIF};

  {$IFDEF MSWINDOWS}
  DLLNAME = 'IPWORKSSSH22.DLL';
  {$ENDIF}
  {$IFDEF LINUX}
  DLLNAME = 'libipworksssh.so.22.0';
  {$ENDIF}
  {$IFDEF ANDROID}
  {$IFDEF USE_DYNAMIC_LOADING}
  DLLNAME = 'libipworksssh.so.22.0';
  {$ELSE}
  DLLNAME = 'libipworksssh.22.0.a';
  {$ENDIF}
  {$ENDIF}
  {$IFDEF IOS}
  DLLNAME = 'libipworksssh.22.0.a';
  {$ELSE}
  {$IFDEF MACOS}
  DLLNAME = '@rpath/libipworksssh.22.0.dylib';
  {$ENDIF}
  {$ENDIF}

{$WARNINGS OFF}

type
  INTARR = array[0..32] of Integer;
  LPINTARR = ^INTARR;
  LPINT = ^Cardinal;
  MEventHandle = function(lpContext: TiphSSHServer; event_id: Integer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  PEventHandle = ^MEventHandle;


{$IFNDEF USEIPWORKSSSHDLL}
var
  pEntryPoint: Pointer;
  pBaseAddress: Pointer;
  iLoadCount: Integer;
{$ELSE}
{$IFDEF USE_DYNAMIC_LOADING}
var
  hLib : {$ifndef FPC}HMODULE{$else}TLibHandle{$endif};
  iLoadCount: Integer;
{$ENDIF USE_DYNAMIC_LOADING}
{$ENDIF USEIPWORKSSSHDLL}


{$IFDEF USE_DYNAMIC_LOADING}
  _IPWorksSSH_EvtStr:                          function(pEvtStr: Pointer; id: Integer; value: Pointer; opt: Integer): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_Stream:                          function(pStream: Pointer; op: Integer; params: LPVOIDARR; retVal: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHServer_Create:                function(pMethod: PEventHandle; pObject: TiphSSHServer; pKey: Pointer; opts: Integer): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHServer_Destroy:               function(p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHServer_Set:                   function(p: Pointer; index: Integer; arridx: Integer; value: Pointer; len: Cardinal): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHServer_Get:                   function(p: Pointer; index: Integer; arridx: Integer; len: LPINT; llVal: PInt64): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHServer_GetLastError:          function(p: Pointer): PLXAnsiChar; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHServer_GetLastErrorCode:      function(p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHServer_SetLastErrorAndCode:   function(p: Pointer; code: Integer; message: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHServer_GetEventError:         function(p: Pointer): PLXAnsiChar; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHServer_GetEventErrorCode:     function(p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHServer_SetEventErrorAndCode:  function(p: Pointer; code: Integer; message: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  {$IFDEF USE_EXPLICIT_INIT}
  _IPWorksSSH_SSHServer_StaticInit:            function(instance: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHServer_StaticDestroy:         function(): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  {$ENDIF}
  _IPWorksSSH_SSHServer_CheckIndex:            function(p: Pointer; index: Integer; arridx: Integer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHServer_Do:                    function(p: Pointer; method_id: Integer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR; llVal: PInt64): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
{$ELSE}
  function _IPWorksSSH_EvtStr                         (pEvtStr: Pointer; id: Integer; value: Pointer; opt: Integer): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_EvtStr' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_Stream                         (pStream: Pointer; op: Integer; params: LPVOIDARR; retVal: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_Stream' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHServer_Create               (pMethod: PEventHandle; pObject: TiphSSHServer; pKey: Pointer; opts: Integer): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHServer_Create' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHServer_Destroy              (p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHServer_Destroy' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHServer_Set                  (p: Pointer; index: Integer; arridx: Integer; value: Pointer; len: Cardinal): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHServer_Set' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHServer_Get                  (p: Pointer; index: Integer; arridx: Integer; len: LPINT; llVal: PInt64): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHServer_Get' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHServer_GetLastError         (p: Pointer): PLXAnsiChar; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHServer_GetLastError' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHServer_GetLastErrorCode     (p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHServer_GetLastErrorCode' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHServer_SetLastErrorAndCode  (p: Pointer; code: Integer; message: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHServer_SetLastErrorAndCode' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHServer_GetEventError        (p: Pointer): PLXAnsiChar; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHServer_GetEventError' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHServer_GetEventErrorCode    (p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHServer_GetEventErrorCode' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHServer_SetEventErrorAndCode (p: Pointer; code: Integer; message: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHServer_SetEventErrorAndCode' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHServer_StaticInit           (instance: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHServer_StaticInit' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHServer_StaticDestroy        (): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHServer_StaticDestroy' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHServer_CheckIndex           (p: Pointer; index: Integer; arridx: Integer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHServer_CheckIndex' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHServer_Do                   (p: Pointer; method_id: Integer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR; llVal: PInt64): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHServer_Do' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
{$ENDIF}

{$HINTS OFF}

(*** Fire Event Methods ***)
function FireConnected(lpContext: TiphSSHServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_StatusCode: Integer;

  tmp_Description: String;

  tmp_CertStoreType: Integer;

  tmp_CertStore: String; // dev platform string
  {$ifdef UNICODE_ON_ANSI}
  tmp_CertStoreB: LXAnsiString; // library string
  {$endif}

  tmp_CertPassword: String; // dev platform string
  {$ifdef UNICODE_ON_ANSI}
  tmp_CertPasswordB: LXAnsiString; // library string
  {$endif}

  tmp_CertSubject: String; // dev platform string
  {$ifdef UNICODE_ON_ANSI}
  tmp_CertSubjectB: LXAnsiString; // library string
  {$endif}

begin
  result := 0;
  if not Assigned(lpContext.FOnConnected) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_StatusCode := Integer(params^[1]);

  tmp_Description := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_CertStoreType := Integer(params^[3]);

  tmp_CertStore := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(_IPWorksSSH_EvtStr(params^[4], 0, nil, EVTSTR_OPT));
  tmp_CertPassword := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(_IPWorksSSH_EvtStr(params^[5], 0, nil, EVTSTR_OPT));
  tmp_CertSubject := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(_IPWorksSSH_EvtStr(params^[6], 0, nil, EVTSTR_OPT));
  try lpContext.FOnConnected(lpContext, tmp_ConnectionId, tmp_StatusCode, tmp_Description, tmp_CertStoreType, tmp_CertStore, tmp_CertPassword, tmp_CertSubject)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Connected'); end;

  params^[3] := Pointer(tmp_CertStoreType);

  {$ifdef UNICODE_ON_ANSI}
  tmp_CertStoreB := ToLinuxAnsiString(tmp_CertStore);
  {$endif}
  _IPWorksSSH_EvtStr(params^[4], 2, {$ifdef UNICODE_ON_ANSI}@tmp_CertStoreB[0]{$else}PChar(tmp_CertStore){$endif}, EVTSTR_OPT);
  {$ifdef UNICODE_ON_ANSI}
  tmp_CertPasswordB := ToLinuxAnsiString(tmp_CertPassword);
  {$endif}
  _IPWorksSSH_EvtStr(params^[5], 2, {$ifdef UNICODE_ON_ANSI}@tmp_CertPasswordB[0]{$else}PChar(tmp_CertPassword){$endif}, EVTSTR_OPT);
  {$ifdef UNICODE_ON_ANSI}
  tmp_CertSubjectB := ToLinuxAnsiString(tmp_CertSubject);
  {$endif}
  _IPWorksSSH_EvtStr(params^[6], 2, {$ifdef UNICODE_ON_ANSI}@tmp_CertSubjectB[0]{$else}PChar(tmp_CertSubject){$endif}, EVTSTR_OPT);
end;


function FireConnectionRequest(lpContext: TiphSSHServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_Address: String;

  tmp_Port: Integer;

  tmp_Accept: Boolean;

begin
  result := 0;
  if not Assigned(lpContext.FOnConnectionRequest) then exit;

  tmp_Address := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[0]);
  tmp_Port := Integer(params^[1]);

  tmp_Accept := Boolean(params^[2]);

  try lpContext.FOnConnectionRequest(lpContext, tmp_Address, tmp_Port, tmp_Accept)
  except on E: Exception do result := lpContext.ReportEventException(E, 'ConnectionRequest'); end;

  params^[2] := Pointer(tmp_Accept);

end;


function FireDisconnected(lpContext: TiphSSHServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_StatusCode: Integer;

  tmp_Description: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnDisconnected) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_StatusCode := Integer(params^[1]);

  tmp_Description := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  try lpContext.FOnDisconnected(lpContext, tmp_ConnectionId, tmp_StatusCode, tmp_Description)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Disconnected'); end;

end;


function FireError(lpContext: TiphSSHServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_ErrorCode: Integer;

  tmp_Description: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnError) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_ErrorCode := Integer(params^[1]);

  tmp_Description := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  try lpContext.FOnError(lpContext, tmp_ConnectionId, tmp_ErrorCode, tmp_Description)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Error', False); end;

end;


function FireLog(lpContext: TiphSSHServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_LogLevel: Integer;

  tmp_Message: String;

  tmp_LogType: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnLog) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_LogLevel := Integer(params^[1]);

  tmp_Message := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_LogType := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[3]);
  try lpContext.FOnLog(lpContext, tmp_ConnectionId, tmp_LogLevel, tmp_Message, tmp_LogType)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Log'); end;

end;


function FireSSHChannelClosed(lpContext: TiphSSHServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_ChannelId: Integer;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHChannelClosed) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_ChannelId := Integer(params^[1]);

  try lpContext.FOnSSHChannelClosed(lpContext, tmp_ConnectionId, tmp_ChannelId)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHChannelClosed'); end;

end;


function FireSSHChannelDataIn(lpContext: TiphSSHServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_ChannelId: Integer;

  tmp_Data: String;
  tmp_DataB: TBytes;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHChannelDataIn) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_ChannelId := Integer(params^[1]);

  SetLength(tmp_DataB, cbparam^[2]);
  Move(Pointer(params^[2])^, Pointer(tmp_DataB)^, cbparam^[2]); // IMPORTANT: Do NOT multiply the length value!
  tmp_Data := lpContext.BStr2CStr(params^[2], cbparam^[2], tmp_DataB);
  try lpContext.FOnSSHChannelDataIn(lpContext, tmp_ConnectionId, tmp_ChannelId, tmp_Data, tmp_DataB)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHChannelDataIn'); end;

end;


function FireSSHChannelEOF(lpContext: TiphSSHServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_ChannelId: Integer;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHChannelEOF) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_ChannelId := Integer(params^[1]);

  try lpContext.FOnSSHChannelEOF(lpContext, tmp_ConnectionId, tmp_ChannelId)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHChannelEOF'); end;

end;


function FireSSHChannelOpened(lpContext: TiphSSHServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_ChannelId: Integer;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHChannelOpened) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_ChannelId := Integer(params^[1]);

  try lpContext.FOnSSHChannelOpened(lpContext, tmp_ConnectionId, tmp_ChannelId)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHChannelOpened'); end;

end;


function FireSSHChannelOpenRequest(lpContext: TiphSSHServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_ChannelId: Integer;

  tmp_Service: String;

  tmp_Parameters: String;
  tmp_ParametersB: TBytes;

  tmp_Accept: Boolean;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHChannelOpenRequest) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_ChannelId := Integer(params^[1]);

  tmp_Service := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  SetLength(tmp_ParametersB, cbparam^[3]);
  Move(Pointer(params^[3])^, Pointer(tmp_ParametersB)^, cbparam^[3]); // IMPORTANT: Do NOT multiply the length value!
  tmp_Parameters := lpContext.BStr2CStr(params^[3], cbparam^[3], tmp_ParametersB);
  tmp_Accept := Boolean(params^[4]);

  try lpContext.FOnSSHChannelOpenRequest(lpContext, tmp_ConnectionId, tmp_ChannelId, tmp_Service, tmp_Parameters, tmp_ParametersB, tmp_Accept)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHChannelOpenRequest'); end;

  params^[4] := Pointer(tmp_Accept);

end;


function FireSSHChannelReadyToSend(lpContext: TiphSSHServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_ChannelId: Integer;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHChannelReadyToSend) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_ChannelId := Integer(params^[1]);

  try lpContext.FOnSSHChannelReadyToSend(lpContext, tmp_ConnectionId, tmp_ChannelId)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHChannelReadyToSend'); end;

end;


function FireSSHChannelRequest(lpContext: TiphSSHServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_ChannelId: Integer;

  tmp_RequestType: String;

  tmp_Packet: String;
  tmp_PacketB: TBytes;

  tmp_Success: Boolean;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHChannelRequest) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_ChannelId := Integer(params^[1]);

  tmp_RequestType := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  SetLength(tmp_PacketB, cbparam^[3]);
  Move(Pointer(params^[3])^, Pointer(tmp_PacketB)^, cbparam^[3]); // IMPORTANT: Do NOT multiply the length value!
  tmp_Packet := lpContext.BStr2CStr(params^[3], cbparam^[3], tmp_PacketB);
  tmp_Success := Boolean(params^[4]);

  try lpContext.FOnSSHChannelRequest(lpContext, tmp_ConnectionId, tmp_ChannelId, tmp_RequestType, tmp_Packet, tmp_PacketB, tmp_Success)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHChannelRequest'); end;

  params^[4] := Pointer(tmp_Success);

end;


function FireSSHChannelRequested(lpContext: TiphSSHServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_ChannelId: Integer;

  tmp_RequestType: String;

  tmp_Packet: String;
  tmp_PacketB: TBytes;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHChannelRequested) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_ChannelId := Integer(params^[1]);

  tmp_RequestType := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  SetLength(tmp_PacketB, cbparam^[3]);
  Move(Pointer(params^[3])^, Pointer(tmp_PacketB)^, cbparam^[3]); // IMPORTANT: Do NOT multiply the length value!
  tmp_Packet := lpContext.BStr2CStr(params^[3], cbparam^[3], tmp_PacketB);
  try lpContext.FOnSSHChannelRequested(lpContext, tmp_ConnectionId, tmp_ChannelId, tmp_RequestType, tmp_Packet, tmp_PacketB)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHChannelRequested'); end;

end;


function FireSSHServiceRequest(lpContext: TiphSSHServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_Service: String;

  tmp_Accept: Boolean;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHServiceRequest) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_Service := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_Accept := Boolean(params^[2]);

  try lpContext.FOnSSHServiceRequest(lpContext, tmp_ConnectionId, tmp_Service, tmp_Accept)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHServiceRequest'); end;

  params^[2] := Pointer(tmp_Accept);

end;


function FireSSHStatus(lpContext: TiphSSHServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_Message: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHStatus) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_Message := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  try lpContext.FOnSSHStatus(lpContext, tmp_ConnectionId, tmp_Message)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHStatus'); end;

end;


function FireSSHTunnelClosed(lpContext: TiphSSHServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_Address: String;

  tmp_Port: Integer;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHTunnelClosed) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_Address := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_Port := Integer(params^[2]);

  try lpContext.FOnSSHTunnelClosed(lpContext, tmp_ConnectionId, tmp_Address, tmp_Port)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHTunnelClosed'); end;

end;


function FireSSHTunnelRequested(lpContext: TiphSSHServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_Direction: Integer;

  tmp_Address: String;

  tmp_Port: Integer;

  tmp_Accept: Boolean;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHTunnelRequested) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_Direction := Integer(params^[1]);

  tmp_Address := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_Port := Integer(params^[3]);

  tmp_Accept := Boolean(params^[4]);

  try lpContext.FOnSSHTunnelRequested(lpContext, tmp_ConnectionId, tmp_Direction, tmp_Address, tmp_Port, tmp_Accept)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHTunnelRequested'); end;

  params^[3] := Pointer(tmp_Port);

  params^[4] := Pointer(tmp_Accept);

end;


function FireSSHUserAuthRequest(lpContext: TiphSSHServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_User: String;

  tmp_Service: String;

  tmp_AuthMethod: String;

  tmp_AuthParam: String;

  tmp_Accept: Boolean;

  tmp_PartialSuccess: Boolean;

  tmp_AvailableMethods: String; // dev platform string
  {$ifdef UNICODE_ON_ANSI}
  tmp_AvailableMethodsB: LXAnsiString; // library string
  {$endif}

  tmp_KeyAlgorithm: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHUserAuthRequest) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_User := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_Service := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_AuthMethod := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[3]);
  tmp_AuthParam := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[4]);
  tmp_Accept := Boolean(params^[5]);

  tmp_PartialSuccess := Boolean(params^[6]);

  tmp_AvailableMethods := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(_IPWorksSSH_EvtStr(params^[7], 0, nil, EVTSTR_OPT));
  tmp_KeyAlgorithm := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[8]);
  try lpContext.FOnSSHUserAuthRequest(lpContext, tmp_ConnectionId, tmp_User, tmp_Service, tmp_AuthMethod, tmp_AuthParam, tmp_Accept, tmp_PartialSuccess, tmp_AvailableMethods, tmp_KeyAlgorithm)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHUserAuthRequest'); end;

  params^[5] := Pointer(tmp_Accept);

  params^[6] := Pointer(tmp_PartialSuccess);

  {$ifdef UNICODE_ON_ANSI}
  tmp_AvailableMethodsB := ToLinuxAnsiString(tmp_AvailableMethods);
  {$endif}
  _IPWorksSSH_EvtStr(params^[7], 2, {$ifdef UNICODE_ON_ANSI}@tmp_AvailableMethodsB[0]{$else}PChar(tmp_AvailableMethods){$endif}, EVTSTR_OPT);
end;



(*** Event Sink ***)
function FireEvents(lpContext: TiphSSHServer; event_id: Integer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
var
  x: Integer;
begin
  result := 0;

  case event_id of
    EID_SSHServer_Connected: begin result := FireConnected(lpContext, cparam, params, cbparam); end;

    EID_SSHServer_ConnectionRequest: begin result := FireConnectionRequest(lpContext, cparam, params, cbparam); end;

    EID_SSHServer_Disconnected: begin result := FireDisconnected(lpContext, cparam, params, cbparam); end;

    EID_SSHServer_Error: begin result := FireError(lpContext, cparam, params, cbparam); end;

    EID_SSHServer_Log: begin result := FireLog(lpContext, cparam, params, cbparam); end;

    EID_SSHServer_SSHChannelClosed: begin result := FireSSHChannelClosed(lpContext, cparam, params, cbparam); end;

    EID_SSHServer_SSHChannelDataIn: begin result := FireSSHChannelDataIn(lpContext, cparam, params, cbparam); end;

    EID_SSHServer_SSHChannelEOF: begin result := FireSSHChannelEOF(lpContext, cparam, params, cbparam); end;

    EID_SSHServer_SSHChannelOpened: begin result := FireSSHChannelOpened(lpContext, cparam, params, cbparam); end;

    EID_SSHServer_SSHChannelOpenRequest: begin result := FireSSHChannelOpenRequest(lpContext, cparam, params, cbparam); end;

    EID_SSHServer_SSHChannelReadyToSend: begin result := FireSSHChannelReadyToSend(lpContext, cparam, params, cbparam); end;

    EID_SSHServer_SSHChannelRequest: begin result := FireSSHChannelRequest(lpContext, cparam, params, cbparam); end;

    EID_SSHServer_SSHChannelRequested: begin result := FireSSHChannelRequested(lpContext, cparam, params, cbparam); end;

    EID_SSHServer_SSHServiceRequest: begin result := FireSSHServiceRequest(lpContext, cparam, params, cbparam); end;

    EID_SSHServer_SSHStatus: begin result := FireSSHStatus(lpContext, cparam, params, cbparam); end;

    EID_SSHServer_SSHTunnelClosed: begin result := FireSSHTunnelClosed(lpContext, cparam, params, cbparam); end;

    EID_SSHServer_SSHTunnelRequested: begin result := FireSSHTunnelRequested(lpContext, cparam, params, cbparam); end;

    EID_SSHServer_SSHUserAuthRequest: begin result := FireSSHUserAuthRequest(lpContext, cparam, params, cbparam); end;

    99999: begin x := 0; end;
  end;
end;

procedure Register;
begin
  iphCore.Register;
  RegisterComponents('IPWorks SSH', [TiphSSHServer]);
end;

{*********************************************************************************}
procedure TiphSSHServer.SetDataToSend(SSHChannelId: Integer; lpDataToSend: PLXAnsiChar; lenDataToSend: Cardinal);
var
  err: integer;
begin
  if @_IPWorksSSH_SSHServer_Set = nil then exit;
  err := _IPWorksSSH_SSHServer_Set(m_ctl, PID_SSHServer_DataToSend, SSHChannelId, Pointer(lpDataToSend), lenDataToSend);
  if err <> 0 then TreatErr(err, '');
end;

procedure TiphSSHServer.SetSSHCertEncoded(lpSSHCertEncoded: PLXAnsiChar; lenSSHCertEncoded: Cardinal);
var
  err: integer;
begin
  if @_IPWorksSSH_SSHServer_Set = nil then exit;
  err := _IPWorksSSH_SSHServer_Set(m_ctl, PID_SSHServer_SSHCertEncoded, 0, Pointer(lpSSHCertEncoded), lenSSHCertEncoded);
  if err <> 0 then TreatErr(err, '');
end;

procedure TiphSSHServer.SetSSHCertStore(lpSSHCertStore: PLXAnsiChar; lenSSHCertStore: Cardinal);
var
  err: integer;
begin
  if @_IPWorksSSH_SSHServer_Set = nil then exit;
  err := _IPWorksSSH_SSHServer_Set(m_ctl, PID_SSHServer_SSHCertStore, 0, Pointer(lpSSHCertStore), lenSSHCertStore);
  if err <> 0 then TreatErr(err, '');
end;


type
  TiphChannelsSSHChannelList = class(TiphSSHChannelList)
  protected
    function OwnerCtl : TiphSSHServer;
    // Only need to override one or two methods here
    function CtlGetCount: integer; override;
    procedure CtlSetCount(Value : integer); override;
    function CreateElemInstance(Index : integer): TiphSSHChannel; override;
  end;

  TiphChannelsSSHChannel = class(TiphSSHChannel)
  protected
    function OwnerCtl : TiphSSHServer;
    function GetPropBytesSent: Integer; override;

    function GetPropChannelId: String; override;

    procedure SetPropDataToSend(Value: String); override;

    procedure SetPropDataToSendB(Value: TBytes); override;

    function GetPropReadyToSend: Boolean; override;

    function GetPropRecordLength: Integer; override;

    procedure SetPropRecordLength(Value: Integer); override;

    function GetPropService: String; override;

  end;

function TiphChannelsSSHChannel.OwnerCtl : TiphSSHServer;
begin
  Result := TiphSSHServer(FOwnerCtl);
end;

function TiphChannelsSSHChannel.GetPropBytesSent: Integer;
begin
  Result := (OwnerCtl.get_BytesSent(Index));
end;


function TiphChannelsSSHChannel.GetPropChannelId: String;
begin
  Result := (OwnerCtl.get_ChannelId(Index));
end;


procedure TiphChannelsSSHChannel.SetPropDataToSend(Value: String);
begin
  OwnerCtl.set_DataToSend(Index, (Value));
end;

procedure TiphChannelsSSHChannel.SetPropDataToSendB(Value: TBytes); 
begin
  OwnerCtl.set_DataToSendB(Index, Value);
end;

function TiphChannelsSSHChannel.GetPropReadyToSend: Boolean;
begin
  Result := (OwnerCtl.get_ReadyToSend(Index));
end;


function TiphChannelsSSHChannel.GetPropRecordLength: Integer;
begin
  Result := (OwnerCtl.get_RecordLength(Index));
end;


procedure TiphChannelsSSHChannel.SetPropRecordLength(Value: Integer);
begin
  OwnerCtl.set_RecordLength(Index, (Value));
end;

function TiphChannelsSSHChannel.GetPropService: String;
begin
  Result := (OwnerCtl.get_Service(Index));
end;



function TiphChannelsSSHChannelList.OwnerCtl : TiphSSHServer;
begin
  Result := TiphSSHServer(FOwnerCtl);
end;

// Collection's overrides
function TiphChannelsSSHChannelList.CtlGetCount: integer;
begin
  Result := OwnerCtl.get_SSHChannelCount();
end;

procedure TiphChannelsSSHChannelList.CtlSetCount(Value : integer);
begin
  // The collection is read-only
end;

function TiphChannelsSSHChannelList.CreateElemInstance(Index : integer): TiphSSHChannel;
begin
  Result := TiphChannelsSSHChannel.Create(FOwnerCtl, true);
  Result.Index := Index;
end;

type
  TiphConnectionsSSHConnectionList = class(TiphSSHConnectionList)
  protected
    function OwnerCtl : TiphSSHServer;
    // Only need to override one or two methods here
    function CtlGetCount: integer; override;
    procedure CtlSetCount(Value : integer); override;
    function CreateElemInstance(Index : integer): TiphSSHConnection; override;
  end;

  TiphConnectionsSSHConnection = class(TiphSSHConnection)
  protected
    function OwnerCtl : TiphSSHServer;
    function GetPropConnected: Boolean; override;

    procedure SetPropConnected(Value: Boolean); override;

    function GetPropLocalAddress: String; override;

    function GetPropRemoteHost: String; override;

    function GetPropRemotePort: Integer; override;

    function GetPropTimeout: Integer; override;

    procedure SetPropTimeout(Value: Integer); override;

  end;

function TiphConnectionsSSHConnection.OwnerCtl : TiphSSHServer;
begin
  Result := TiphSSHServer(FOwnerCtl);
end;

function TiphConnectionsSSHConnection.GetPropConnected: Boolean;
begin
  Result := (OwnerCtl.get_SSHConnectionConnected(Index));
end;


procedure TiphConnectionsSSHConnection.SetPropConnected(Value: Boolean);
begin
  OwnerCtl.set_SSHConnectionConnected(Index, (Value));
end;

function TiphConnectionsSSHConnection.GetPropLocalAddress: String;
begin
  Result := (OwnerCtl.get_SSHConnectionLocalAddress(Index));
end;


function TiphConnectionsSSHConnection.GetPropRemoteHost: String;
begin
  Result := (OwnerCtl.get_SSHConnectionRemoteHost(Index));
end;


function TiphConnectionsSSHConnection.GetPropRemotePort: Integer;
begin
  Result := (OwnerCtl.get_SSHConnectionRemotePort(Index));
end;


function TiphConnectionsSSHConnection.GetPropTimeout: Integer;
begin
  Result := (OwnerCtl.get_SSHConnectionTimeout(Index));
end;


procedure TiphConnectionsSSHConnection.SetPropTimeout(Value: Integer);
begin
  OwnerCtl.set_SSHConnectionTimeout(Index, (Value));
end;


function TiphConnectionsSSHConnectionList.OwnerCtl : TiphSSHServer;
begin
  Result := TiphSSHServer(FOwnerCtl);
end;

// Collection's overrides
function TiphConnectionsSSHConnectionList.CtlGetCount: integer;
begin
  Result := OwnerCtl.get_SSHConnectionCount();
end;

procedure TiphConnectionsSSHConnectionList.CtlSetCount(Value : integer);
begin
  // The collection is read-only
end;

function TiphConnectionsSSHConnectionList.CreateElemInstance(Index : integer): TiphSSHConnection;
begin
  Result := TiphConnectionsSSHConnection.Create(FOwnerCtl, true);
  Result.Index := Index;
end;

type
  TiphKeyboardInteractivePromptsSSHPromptList = class(TiphSSHPromptList)
  protected
    function OwnerCtl : TiphSSHServer;
    // Only need to override one or two methods here
    function CtlGetCount: integer; override;
    procedure CtlSetCount(Value : integer); override;
    function CreateElemInstance(Index : integer): TiphSSHPrompt; override;
  end;

  TiphKeyboardInteractivePromptsSSHPrompt = class(TiphSSHPrompt)
  protected
    function OwnerCtl : TiphSSHServer;
    function GetPropEcho: Boolean; override;

    procedure SetPropEcho(Value: Boolean); override;

    function GetPropPrompt: String; override;

    procedure SetPropPrompt(Value: String); override;

  end;

function TiphKeyboardInteractivePromptsSSHPrompt.OwnerCtl : TiphSSHServer;
begin
  Result := TiphSSHServer(FOwnerCtl);
end;

function TiphKeyboardInteractivePromptsSSHPrompt.GetPropEcho: Boolean;
begin
  Result := (OwnerCtl.get_KeyboardInteractivePromptEcho(Index));
end;


procedure TiphKeyboardInteractivePromptsSSHPrompt.SetPropEcho(Value: Boolean);
begin
  OwnerCtl.set_KeyboardInteractivePromptEcho(Index, (Value));
end;

function TiphKeyboardInteractivePromptsSSHPrompt.GetPropPrompt: String;
begin
  Result := (OwnerCtl.get_KeyboardInteractivePromptPrompt(Index));
end;


procedure TiphKeyboardInteractivePromptsSSHPrompt.SetPropPrompt(Value: String);
begin
  OwnerCtl.set_KeyboardInteractivePromptPrompt(Index, (Value));
end;


function TiphKeyboardInteractivePromptsSSHPromptList.OwnerCtl : TiphSSHServer;
begin
  Result := TiphSSHServer(FOwnerCtl);
end;

// Collection's overrides
function TiphKeyboardInteractivePromptsSSHPromptList.CtlGetCount: integer;
begin
  Result := OwnerCtl.get_KeyboardInteractivePromptCount();
end;

procedure TiphKeyboardInteractivePromptsSSHPromptList.CtlSetCount(Value : integer);
begin
  OwnerCtl.set_KeyboardInteractivePromptCount(Value);
end;

function TiphKeyboardInteractivePromptsSSHPromptList.CreateElemInstance(Index : integer): TiphSSHPrompt;
begin
  Result := TiphKeyboardInteractivePromptsSSHPrompt.Create(FOwnerCtl, false);
  Result.Index := Index;
end;


{*********************************************************************************}

constructor TiphSSHServer.Create(AOwner: TComponent);
begin
  Create(AOwner, IPWORKSSSH_OEMKEY_73);
  Config('CodePage=65001');
end;

constructor TiphSSHServer.Create(AOwner: TComponent; RuntimeLicense: string);
begin
  inherited Create(AOwner);
  m_ctl := nil;

  if @_IPWorksSSH_SSHServer_Create <> nil then
    m_ctl := _IPWorksSSH_SSHServer_Create(@FireEvents, self, nil, CREATE_OPT);
  if m_ctl = nil then
    TreatErr(-1, 'IPWorks SSH SSHServer: Error creating component');
  _IPWorksSSH_SSHServer_Set(m_ctl, 8002{PID_KEYCHECK_DLP}, 0, 
    {$ifdef ANSICHAR_IS_BYTE}
    Pointer(ToLinuxAnsiString(RuntimeLicense)),
    {$else}
    Pointer(PAnsiChar(AnsiString(RuntimeLicense))),
    {$endif}
    0);

  FChannels := TiphChannelsSSHChannelList.Create(Self, true);

  FConnections := TiphConnectionsSSHConnectionList.Create(Self, true);

  FKeyboardInteractivePrompts := TiphKeyboardInteractivePromptsSSHPromptList.Create(Self, false);


  try set_DefaultAuthMethods('password,publickey') except on E:Exception do end;

  try set_DefaultIdleTimeout(0) except on E:Exception do end;

  try set_DefaultTimeout(60) except on E:Exception do end;

  try set_KeyboardInteractiveMessage('') except on E:Exception do end;

  try set_LocalPort(22) except on E:Exception do end;

  try set_SSHCertStore('MY') except on E:Exception do end;

  try set_SSHCertStorePassword('') except on E:Exception do end;

  try set_SSHCertStoreType(cstUser) except on E:Exception do end;

  try set_SSHCertSubject('') except on E:Exception do end;

  try set_SSHCompressionAlgorithms('none,zlib') except on E:Exception do end;

  try set_SSHEncryptionAlgorithms('aes256-ctr,aes192-ctr,aes128-ctr,aes256-cbc,aes192-cbc,aes128-cbc,3des-ctr,3des-cbc,blowfish-cbc,arcfour256,arcfour128,arcfour,cast128-cbc,aes256-gcm@openssh.com,aes128-gcm@openssh.com,chacha20-poly1305@openssh.com') except on E:Exception do end;

end;

destructor TiphSSHServer.Destroy;
begin
  FreeAndNil(FChannels);

  FreeAndNil(FConnections);

  FreeAndNil(FKeyboardInteractivePrompts);

  
  
  if m_ctl <> nil then begin
    if @_IPWorksSSH_SSHServer_Destroy <> nil then
      _IPWorksSSH_SSHServer_Destroy(m_ctl);
  end;
  m_ctl := nil;
  inherited Destroy;
end;

function TiphSSHServer.ThrowCoreException(err: Integer; const desc: {$ifndef NO_ANSI}AnsiString{$else}string{$endif}): EIPWorksSSH;
begin
  result := EiphSSHServer.CreateCode(err, desc);
end;

function TiphSSHServer.ReportEventException(e: Exception; Event: String; ReFire: Boolean): Integer;
var
  msg: String;
begin
  result := -1;
  msg := 'An unhandled error occurred in the ' + Event + ' event handler: ' + E.Message;
  if (m_ctl <> nil) and (@_IPWorksSSH_SSHServer_SetEventErrorAndCode <> nil) then begin
    if E is EIPWorksSSH then begin
      result := EIPWorksSSH(e).Code;
      msg := E.Message;
    end;
    _IPWorksSSH_SSHServer_SetEventErrorAndCode(m_ctl, result, 
      {$ifdef ANSICHAR_IS_BYTE}
      Pointer(ToLinuxAnsiString(msg)));
      {$else}
      Pointer(PAnsiChar(AnsiString(msg))));
      {$endif}
  end;
  if ReFire and Assigned(FOnError) then FOnError(self, 0, result, msg);
end;

{*********************************************************************************}

procedure TiphSSHServer.AboutDlg;
var
  p : LPVOIDARR;
  pc: LPINTARR;
begin
{$IFDEF LINUX}
  inherited AboutDlg;
{$ELSE}
  p := nil;
  pc := nil;
  if @_IPWorksSSH_SSHServer_Do <> nil then
    _IPWorksSSH_SSHServer_Do(m_ctl, 255, 0, p, pc, nil);
{$ENDIF}
end;

{*********************************************************************************}

procedure TiphSSHServer.SetRuntimeLicense(key: String);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHServer_Set = nil then exit;
  err := _IPWorksSSH_SSHServer_Set(m_ctl, 8001{PID_RUNTIME_LICENSE}, 0, 
    {$ifdef ANSICHAR_IS_BYTE}
    Pointer(ToLinuxAnsiString(key)),
    {$else}
    Pointer(PAnsiChar(AnsiString(key))),
    {$endif}
    0);

  if err <> 0 then TreatErr(err, '');
end;

function TiphSSHServer.GetRuntimeLicense: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';
  if @_IPWorksSSH_SSHServer_Get = nil then exit;
  tmp := _IPWorksSSH_SSHServer_Get(m_ctl, 8001{PID_RUNTIME_LICENSE}, 0, nil, nil);
  err := _IPWorksSSH_SSHServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$ifdef ANSICHAR_IS_BYTE}
  result := FromLinuxAnsiString(PLXAnsiChar(tmp));
  {$else}
  result := PAnsiChar(tmp);
  {$endif}
end;

{*********************************************************************************}

function TiphSSHServer.HasData: Boolean;
begin
  result := false;
end;

procedure TiphSSHServer.ReadHnd(Reader: TStream);
begin
end;

procedure TiphSSHServer.WriteHnd(Writer: TStream);
begin
end;

function TiphSSHServer.BStr2CStr(pBStr: Pointer; lenBStr: Integer; bArr: TBytes): String;
{$IF DEFINED(UNICODE) AND DEFINED(MSWINDOWS)}
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
{$IFEND}
begin
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  param[0] := pBStr; paramcb[0] := lenBStr;
  param[1] := nil;   paramcb[1] := 0;
  param[2] := nil;   paramcb[2] := 0;
  _IPWorksSSH_SSHServer_Do(m_ctl, 2003, 2, @param, @paramcb, nil);
  SetLength(Result, Integer(param[2]));
  if Length(Result) > 0 then begin
    param[1] := PChar(Result); paramcb[1] := Length(Result);
    _IPWorksSSH_SSHServer_Do(m_ctl, 2003, 2, @param, @paramcb, nil);
  end;
  {$ELSE}
  try
    Result := TEncoding.Default.GetString(bArr);
  except
    on E: Exception do
      SetString(Result, PAnsiChar(pBStr), lenBStr);
   end;
  {$ENDIF}
{$ELSE}
  SetString(Result, PAnsiChar(pBStr), lenBStr);
{$ENDIF}
end;

procedure TiphSSHServer.DefineProperties(Filer: TFiler);
begin
  inherited DefineProperties(Filer);
  Filer.DefineBinaryProperty('RegHnd', ReadHnd, WriteHnd, HasData);
end;

{*********************************************************************************}

procedure TiphSSHServer.TreatErr(Err: Integer; const desc: String);
var
  msg : {$ifdef NO_ANSI}string{$else}LXAnsiString{$endif};
begin
  {$ifdef NO_ANSI}
  msg := desc;
  {$else}
  {$ifdef ANSICHAR_IS_BYTE}
  msg := ToLinuxAnsiString(desc);
  {$else}
  msg := LXAnsiString(desc);
  {$endif}
  {$endif}
  if Length(msg) = 0 then
  begin
    if @_IPWorksSSH_SSHServer_GetLastError <> nil then
      {$ifdef NO_ANSI}
      {$ifdef ANSICHAR_IS_BYTE}
      msg := FromLinuxAnsiString(_IPWorksSSH_SSHServer_GetLastError(m_ctl));
      {$else}
      msg := _IPWorksSSH_SSHServer_GetLastError(m_ctl);
      {$endif}
      {$else}
      msg := _IPWorksSSH_SSHServer_GetLastError(m_ctl);
      {$endif}
  end;

  raise ThrowCoreException(err, msg);
end;

{*********************************************************************************}

(*** Property Getters/Setters ***)
function TiphSSHServer.get_SSHChannelCount: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SSHServer_Get = nil then exit;
  tmp := _IPWorksSSH_SSHServer_Get(m_ctl, PID_SSHServer_SSHChannelCount, 0, nil, nil);
  err := _IPWorksSSH_SSHServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


function TiphSSHServer.get_BytesSent(SSHChannelId: Integer): Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);
  if @_IPWorksSSH_SSHServer_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHServer_CheckIndex(m_ctl, PID_SSHServer_BytesSent, SSHChannelId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for BytesSent');

  if @_IPWorksSSH_SSHServer_Get = nil then exit;
  tmp := _IPWorksSSH_SSHServer_Get(m_ctl, PID_SSHServer_BytesSent, SSHChannelId, nil, nil);
  err := _IPWorksSSH_SSHServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


function TiphSSHServer.get_ChannelId(SSHChannelId: Integer): String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';
  if @_IPWorksSSH_SSHServer_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHServer_CheckIndex(m_ctl, PID_SSHServer_ChannelId, SSHChannelId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for ChannelId');

  if @_IPWorksSSH_SSHServer_Get = nil then exit;
  tmp := _IPWorksSSH_SSHServer_Get(m_ctl, PID_SSHServer_ChannelId{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, SSHChannelId, nil, nil);
  err := _IPWorksSSH_SSHServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSSHServer.set_DataToSend(SSHChannelId: Integer; valDataToSend: String);
var
  err: Integer;
  {$IFDEF NONWINUNICODE}tmp: TBytes;{$ENDIF}
begin
  err := 0;
  if @_IPWorksSSH_SSHServer_Set = nil then exit;
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  err := _IPWorksSSH_SSHServer_Set(m_ctl, PID_SSHServer_DataToSend+20000, SSHChannelId, Pointer(PWideChar(valDataToSend)), Length(valDataToSend));
  {$ELSE}
  tmp := TEncoding.UTF8.GetBytes(valDataToSend);
  set_DataToSendB(SSHChannelId, tmp);
  {$ENDIF}
{$ELSE}
  err := _IPWorksSSH_SSHServer_Set(m_ctl, PID_SSHServer_DataToSend, SSHChannelId, Pointer(PAnsiChar(valDataToSend)), Length(valDataToSend));
{$ENDIF}
  if err <> 0 then TreatErr(err, '');
end;


procedure TiphSSHServer.set_DataToSendB(SSHChannelId: Integer; valDataToSend: TBytes);
var
  err: Integer;
begin
  err := _IPWorksSSH_SSHServer_Set(m_ctl, PID_SSHServer_DataToSend, SSHChannelId, Pointer(valDataToSend), Length(valDataToSend));
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHServer.get_ReadyToSend(SSHChannelId: Integer): Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);
  if @_IPWorksSSH_SSHServer_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHServer_CheckIndex(m_ctl, PID_SSHServer_ReadyToSend, SSHChannelId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for ReadyToSend');

  if @_IPWorksSSH_SSHServer_Get = nil then exit;
  tmp := _IPWorksSSH_SSHServer_Get(m_ctl, PID_SSHServer_ReadyToSend, SSHChannelId, nil, nil);
  err := _IPWorksSSH_SSHServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


function TiphSSHServer.get_RecordLength(SSHChannelId: Integer): Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);
  if @_IPWorksSSH_SSHServer_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHServer_CheckIndex(m_ctl, PID_SSHServer_RecordLength, SSHChannelId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for RecordLength');

  if @_IPWorksSSH_SSHServer_Get = nil then exit;
  tmp := _IPWorksSSH_SSHServer_Get(m_ctl, PID_SSHServer_RecordLength, SSHChannelId, nil, nil);
  err := _IPWorksSSH_SSHServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSSHServer.set_RecordLength(SSHChannelId: Integer; valRecordLength: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHServer_Set = nil then exit;
  err := _IPWorksSSH_SSHServer_Set(m_ctl, PID_SSHServer_RecordLength, SSHChannelId, Pointer(valRecordLength), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHServer.get_Service(SSHChannelId: Integer): String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';
  if @_IPWorksSSH_SSHServer_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHServer_CheckIndex(m_ctl, PID_SSHServer_Service, SSHChannelId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for Service');

  if @_IPWorksSSH_SSHServer_Get = nil then exit;
  tmp := _IPWorksSSH_SSHServer_Get(m_ctl, PID_SSHServer_Service{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, SSHChannelId, nil, nil);
  err := _IPWorksSSH_SSHServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphSSHServer.get_ConnectionBacklog: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SSHServer_Get = nil then exit;
  tmp := _IPWorksSSH_SSHServer_Get(m_ctl, PID_SSHServer_ConnectionBacklog, 0, nil, nil);
  err := _IPWorksSSH_SSHServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSSHServer.set_ConnectionBacklog(valConnectionBacklog: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHServer_Set = nil then exit;
  err := _IPWorksSSH_SSHServer_Set(m_ctl, PID_SSHServer_ConnectionBacklog, 0, Pointer(valConnectionBacklog), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHServer.get_SSHConnectionCount: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SSHServer_Get = nil then exit;
  tmp := _IPWorksSSH_SSHServer_Get(m_ctl, PID_SSHServer_SSHConnectionCount, 0, nil, nil);
  err := _IPWorksSSH_SSHServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


function TiphSSHServer.get_SSHConnectionConnected(ConnectionId: Integer): Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);
  if @_IPWorksSSH_SSHServer_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHServer_CheckIndex(m_ctl, PID_SSHServer_SSHConnectionConnected, ConnectionId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for SSHConnectionConnected');

  if @_IPWorksSSH_SSHServer_Get = nil then exit;
  tmp := _IPWorksSSH_SSHServer_Get(m_ctl, PID_SSHServer_SSHConnectionConnected, ConnectionId, nil, nil);
  err := _IPWorksSSH_SSHServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


procedure TiphSSHServer.set_SSHConnectionConnected(ConnectionId: Integer; valSSHConnectionConnected: Boolean);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHServer_Set = nil then exit;
  err := _IPWorksSSH_SSHServer_Set(m_ctl, PID_SSHServer_SSHConnectionConnected, ConnectionId, Pointer(valSSHConnectionConnected), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHServer.get_SSHConnectionLocalAddress(ConnectionId: Integer): String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';
  if @_IPWorksSSH_SSHServer_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHServer_CheckIndex(m_ctl, PID_SSHServer_SSHConnectionLocalAddress, ConnectionId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for SSHConnectionLocalAddress');

  if @_IPWorksSSH_SSHServer_Get = nil then exit;
  tmp := _IPWorksSSH_SSHServer_Get(m_ctl, PID_SSHServer_SSHConnectionLocalAddress{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, ConnectionId, nil, nil);
  err := _IPWorksSSH_SSHServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphSSHServer.get_SSHConnectionRemoteHost(ConnectionId: Integer): String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';
  if @_IPWorksSSH_SSHServer_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHServer_CheckIndex(m_ctl, PID_SSHServer_SSHConnectionRemoteHost, ConnectionId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for SSHConnectionRemoteHost');

  if @_IPWorksSSH_SSHServer_Get = nil then exit;
  tmp := _IPWorksSSH_SSHServer_Get(m_ctl, PID_SSHServer_SSHConnectionRemoteHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, ConnectionId, nil, nil);
  err := _IPWorksSSH_SSHServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphSSHServer.get_SSHConnectionRemotePort(ConnectionId: Integer): Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);
  if @_IPWorksSSH_SSHServer_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHServer_CheckIndex(m_ctl, PID_SSHServer_SSHConnectionRemotePort, ConnectionId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for SSHConnectionRemotePort');

  if @_IPWorksSSH_SSHServer_Get = nil then exit;
  tmp := _IPWorksSSH_SSHServer_Get(m_ctl, PID_SSHServer_SSHConnectionRemotePort, ConnectionId, nil, nil);
  err := _IPWorksSSH_SSHServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


function TiphSSHServer.get_SSHConnectionTimeout(ConnectionId: Integer): Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);
  if @_IPWorksSSH_SSHServer_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHServer_CheckIndex(m_ctl, PID_SSHServer_SSHConnectionTimeout, ConnectionId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for SSHConnectionTimeout');

  if @_IPWorksSSH_SSHServer_Get = nil then exit;
  tmp := _IPWorksSSH_SSHServer_Get(m_ctl, PID_SSHServer_SSHConnectionTimeout, ConnectionId, nil, nil);
  err := _IPWorksSSH_SSHServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSSHServer.set_SSHConnectionTimeout(ConnectionId: Integer; valSSHConnectionTimeout: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHServer_Set = nil then exit;
  err := _IPWorksSSH_SSHServer_Set(m_ctl, PID_SSHServer_SSHConnectionTimeout, ConnectionId, Pointer(valSSHConnectionTimeout), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHServer.get_DefaultAuthMethods: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHServer_Get = nil then exit;
  tmp := _IPWorksSSH_SSHServer_Get(m_ctl, PID_SSHServer_DefaultAuthMethods{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SSHServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSSHServer.set_DefaultAuthMethods(valDefaultAuthMethods: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SSHServer_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valDefaultAuthMethods);
  {$ENDIF}
  err := _IPWorksSSH_SSHServer_Set(m_ctl, PID_SSHServer_DefaultAuthMethods{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valDefaultAuthMethods){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHServer.get_DefaultIdleTimeout: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SSHServer_Get = nil then exit;
  tmp := _IPWorksSSH_SSHServer_Get(m_ctl, PID_SSHServer_DefaultIdleTimeout, 0, nil, nil);
  err := _IPWorksSSH_SSHServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSSHServer.set_DefaultIdleTimeout(valDefaultIdleTimeout: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHServer_Set = nil then exit;
  err := _IPWorksSSH_SSHServer_Set(m_ctl, PID_SSHServer_DefaultIdleTimeout, 0, Pointer(valDefaultIdleTimeout), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHServer.get_DefaultTimeout: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SSHServer_Get = nil then exit;
  tmp := _IPWorksSSH_SSHServer_Get(m_ctl, PID_SSHServer_DefaultTimeout, 0, nil, nil);
  err := _IPWorksSSH_SSHServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSSHServer.set_DefaultTimeout(valDefaultTimeout: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHServer_Set = nil then exit;
  err := _IPWorksSSH_SSHServer_Set(m_ctl, PID_SSHServer_DefaultTimeout, 0, Pointer(valDefaultTimeout), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHServer.get_KeyboardInteractiveMessage: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHServer_Get = nil then exit;
  tmp := _IPWorksSSH_SSHServer_Get(m_ctl, PID_SSHServer_KeyboardInteractiveMessage{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SSHServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSSHServer.set_KeyboardInteractiveMessage(valKeyboardInteractiveMessage: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SSHServer_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valKeyboardInteractiveMessage);
  {$ENDIF}
  err := _IPWorksSSH_SSHServer_Set(m_ctl, PID_SSHServer_KeyboardInteractiveMessage{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valKeyboardInteractiveMessage){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHServer.get_KeyboardInteractivePromptCount: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SSHServer_Get = nil then exit;
  tmp := _IPWorksSSH_SSHServer_Get(m_ctl, PID_SSHServer_KeyboardInteractivePromptCount, 0, nil, nil);
  err := _IPWorksSSH_SSHServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSSHServer.set_KeyboardInteractivePromptCount(valKeyboardInteractivePromptCount: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHServer_Set = nil then exit;
  err := _IPWorksSSH_SSHServer_Set(m_ctl, PID_SSHServer_KeyboardInteractivePromptCount, 0, Pointer(valKeyboardInteractivePromptCount), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHServer.get_KeyboardInteractivePromptEcho(PromptIndex: Integer): Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);
  if @_IPWorksSSH_SSHServer_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHServer_CheckIndex(m_ctl, PID_SSHServer_KeyboardInteractivePromptEcho, PromptIndex);
  if err <> 0 then TreatErr(err, 'Invalid array index value for KeyboardInteractivePromptEcho');

  if @_IPWorksSSH_SSHServer_Get = nil then exit;
  tmp := _IPWorksSSH_SSHServer_Get(m_ctl, PID_SSHServer_KeyboardInteractivePromptEcho, PromptIndex, nil, nil);
  err := _IPWorksSSH_SSHServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


procedure TiphSSHServer.set_KeyboardInteractivePromptEcho(PromptIndex: Integer; valKeyboardInteractivePromptEcho: Boolean);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHServer_Set = nil then exit;
  err := _IPWorksSSH_SSHServer_Set(m_ctl, PID_SSHServer_KeyboardInteractivePromptEcho, PromptIndex, Pointer(valKeyboardInteractivePromptEcho), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHServer.get_KeyboardInteractivePromptPrompt(PromptIndex: Integer): String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';
  if @_IPWorksSSH_SSHServer_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHServer_CheckIndex(m_ctl, PID_SSHServer_KeyboardInteractivePromptPrompt, PromptIndex);
  if err <> 0 then TreatErr(err, 'Invalid array index value for KeyboardInteractivePromptPrompt');

  if @_IPWorksSSH_SSHServer_Get = nil then exit;
  tmp := _IPWorksSSH_SSHServer_Get(m_ctl, PID_SSHServer_KeyboardInteractivePromptPrompt{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, PromptIndex, nil, nil);
  err := _IPWorksSSH_SSHServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSSHServer.set_KeyboardInteractivePromptPrompt(PromptIndex: Integer; valKeyboardInteractivePromptPrompt: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SSHServer_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valKeyboardInteractivePromptPrompt);
  {$ENDIF}
  err := _IPWorksSSH_SSHServer_Set(m_ctl, PID_SSHServer_KeyboardInteractivePromptPrompt{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, PromptIndex, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valKeyboardInteractivePromptPrompt){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHServer.get_Listening: Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);

  if @_IPWorksSSH_SSHServer_Get = nil then exit;
  tmp := _IPWorksSSH_SSHServer_Get(m_ctl, PID_SSHServer_Listening, 0, nil, nil);
  err := _IPWorksSSH_SSHServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


procedure TiphSSHServer.set_Listening(valListening: Boolean);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHServer_Set = nil then exit;
  err := _IPWorksSSH_SSHServer_Set(m_ctl, PID_SSHServer_Listening, 0, Pointer(valListening), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHServer.get_LocalHost: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHServer_Get = nil then exit;
  tmp := _IPWorksSSH_SSHServer_Get(m_ctl, PID_SSHServer_LocalHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SSHServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSSHServer.set_LocalHost(valLocalHost: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SSHServer_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valLocalHost);
  {$ENDIF}
  err := _IPWorksSSH_SSHServer_Set(m_ctl, PID_SSHServer_LocalHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valLocalHost){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHServer.get_LocalPort: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SSHServer_Get = nil then exit;
  tmp := _IPWorksSSH_SSHServer_Get(m_ctl, PID_SSHServer_LocalPort, 0, nil, nil);
  err := _IPWorksSSH_SSHServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSSHServer.set_LocalPort(valLocalPort: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHServer_Set = nil then exit;
  err := _IPWorksSSH_SSHServer_Set(m_ctl, PID_SSHServer_LocalPort, 0, Pointer(valLocalPort), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHServer.get_SSHCertEncoded: String;
var
  tmp: {$IFDEF NONWINUNICODE}TBytes{$ELSE}Pointer; len: Cardinal{$ENDIF};
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHServer_Get = nil then exit;
  tmp := {$IFDEF NONWINUNICODE}get_SSHCertEncodedB{$ELSE}_IPWorksSSH_SSHServer_Get(m_ctl, PID_SSHServer_SSHCertEncoded{$IFDEF UNICODE_ON_UNICODE}+20000{$ENDIF}, 0, @len, nil){$ENDIF};
  err := _IPWorksSSH_SSHServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$IFDEF NONWINUNICODE}result := TEncoding.UTF8.GetString(tmp);{$ELSE}SetString(result, PChar(tmp), len);{$ENDIF}
end;


procedure TiphSSHServer.set_SSHCertEncoded(valSSHCertEncoded: String);
var
  err: Integer;
  {$IFDEF NONWINUNICODE}tmp: TBytes;{$ENDIF}
begin
  err := 0;
  if @_IPWorksSSH_SSHServer_Set = nil then exit;
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  err := _IPWorksSSH_SSHServer_Set(m_ctl, PID_SSHServer_SSHCertEncoded+20000, 0, Pointer(PWideChar(valSSHCertEncoded)), Length(valSSHCertEncoded));
  {$ELSE}
  tmp := TEncoding.UTF8.GetBytes(valSSHCertEncoded);
  set_SSHCertEncodedB(tmp);
  {$ENDIF}
{$ELSE}
  err := _IPWorksSSH_SSHServer_Set(m_ctl, PID_SSHServer_SSHCertEncoded, 0, Pointer(PAnsiChar(valSSHCertEncoded)), Length(valSSHCertEncoded));
{$ENDIF}
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHServer.get_SSHCertEncodedB: TBytes;
var
  tmp: Pointer;
  len: Cardinal;
begin
  result := nil;
  if @_IPWorksSSH_SSHServer_Get = nil then exit;
  tmp := _IPWorksSSH_SSHServer_Get(m_ctl, PID_SSHServer_SSHCertEncoded, 0, @len, nil);
  SetLength(result, len);
  Move(Pointer(tmp)^, Pointer(result)^, len); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSSHServer.set_SSHCertEncodedB(valSSHCertEncoded: TBytes);
var
  err: Integer;
begin
  err := _IPWorksSSH_SSHServer_Set(m_ctl, PID_SSHServer_SSHCertEncoded, 0, Pointer(valSSHCertEncoded), Length(valSSHCertEncoded));
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHServer.get_SSHCertStore: String;
var
  tmp: {$IFDEF NONWINUNICODE}TBytes{$ELSE}Pointer; len: Cardinal{$ENDIF};
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHServer_Get = nil then exit;
  tmp := {$IFDEF NONWINUNICODE}get_SSHCertStoreB{$ELSE}_IPWorksSSH_SSHServer_Get(m_ctl, PID_SSHServer_SSHCertStore{$IFDEF UNICODE_ON_UNICODE}+20000{$ENDIF}, 0, @len, nil){$ENDIF};
  err := _IPWorksSSH_SSHServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$IFDEF NONWINUNICODE}result := TEncoding.UTF8.GetString(tmp);{$ELSE}SetString(result, PChar(tmp), len);{$ENDIF}
end;


procedure TiphSSHServer.set_SSHCertStore(valSSHCertStore: String);
var
  err: Integer;
  {$IFDEF NONWINUNICODE}tmp: TBytes;{$ENDIF}
begin
  err := 0;
  if @_IPWorksSSH_SSHServer_Set = nil then exit;
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  err := _IPWorksSSH_SSHServer_Set(m_ctl, PID_SSHServer_SSHCertStore+20000, 0, Pointer(PWideChar(valSSHCertStore)), Length(valSSHCertStore));
  {$ELSE}
  tmp := TEncoding.UTF8.GetBytes(valSSHCertStore);
  set_SSHCertStoreB(tmp);
  {$ENDIF}
{$ELSE}
  err := _IPWorksSSH_SSHServer_Set(m_ctl, PID_SSHServer_SSHCertStore, 0, Pointer(PAnsiChar(valSSHCertStore)), Length(valSSHCertStore));
{$ENDIF}
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHServer.get_SSHCertStoreB: TBytes;
var
  tmp: Pointer;
  len: Cardinal;
begin
  result := nil;
  if @_IPWorksSSH_SSHServer_Get = nil then exit;
  tmp := _IPWorksSSH_SSHServer_Get(m_ctl, PID_SSHServer_SSHCertStore, 0, @len, nil);
  SetLength(result, len);
  Move(Pointer(tmp)^, Pointer(result)^, len); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSSHServer.set_SSHCertStoreB(valSSHCertStore: TBytes);
var
  err: Integer;
begin
  err := _IPWorksSSH_SSHServer_Set(m_ctl, PID_SSHServer_SSHCertStore, 0, Pointer(valSSHCertStore), Length(valSSHCertStore));
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHServer.get_SSHCertStorePassword: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHServer_Get = nil then exit;
  tmp := _IPWorksSSH_SSHServer_Get(m_ctl, PID_SSHServer_SSHCertStorePassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SSHServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSSHServer.set_SSHCertStorePassword(valSSHCertStorePassword: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SSHServer_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHCertStorePassword);
  {$ENDIF}
  err := _IPWorksSSH_SSHServer_Set(m_ctl, PID_SSHServer_SSHCertStorePassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHCertStorePassword){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHServer.get_SSHCertStoreType: TiphsshserverSSHCertStoreTypes;
var
  tmp: Pointer;
  err: Integer;
begin
  result := TiphsshserverSSHCertStoreTypes(0);

  if @_IPWorksSSH_SSHServer_Get = nil then exit;
  tmp := _IPWorksSSH_SSHServer_Get(m_ctl, PID_SSHServer_SSHCertStoreType, 0, nil, nil);
  err := _IPWorksSSH_SSHServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := TiphsshserverSSHCertStoreTypes(tmp);
end;


procedure TiphSSHServer.set_SSHCertStoreType(valSSHCertStoreType: TiphsshserverSSHCertStoreTypes);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHServer_Set = nil then exit;
  err := _IPWorksSSH_SSHServer_Set(m_ctl, PID_SSHServer_SSHCertStoreType, 0, Pointer(valSSHCertStoreType), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHServer.get_SSHCertSubject: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHServer_Get = nil then exit;
  tmp := _IPWorksSSH_SSHServer_Get(m_ctl, PID_SSHServer_SSHCertSubject{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SSHServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSSHServer.set_SSHCertSubject(valSSHCertSubject: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SSHServer_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHCertSubject);
  {$ENDIF}
  err := _IPWorksSSH_SSHServer_Set(m_ctl, PID_SSHServer_SSHCertSubject{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHCertSubject){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHServer.get_SSHCompressionAlgorithms: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHServer_Get = nil then exit;
  tmp := _IPWorksSSH_SSHServer_Get(m_ctl, PID_SSHServer_SSHCompressionAlgorithms{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SSHServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSSHServer.set_SSHCompressionAlgorithms(valSSHCompressionAlgorithms: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SSHServer_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHCompressionAlgorithms);
  {$ENDIF}
  err := _IPWorksSSH_SSHServer_Set(m_ctl, PID_SSHServer_SSHCompressionAlgorithms{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHCompressionAlgorithms){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHServer.get_SSHEncryptionAlgorithms: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHServer_Get = nil then exit;
  tmp := _IPWorksSSH_SSHServer_Get(m_ctl, PID_SSHServer_SSHEncryptionAlgorithms{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SSHServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSSHServer.set_SSHEncryptionAlgorithms(valSSHEncryptionAlgorithms: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SSHServer_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHEncryptionAlgorithms);
  {$ENDIF}
  err := _IPWorksSSH_SSHServer_Set(m_ctl, PID_SSHServer_SSHEncryptionAlgorithms{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHEncryptionAlgorithms){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;



{*********************************************************************************}

(*** Property Getters/Setters: OO API ***)
function  TiphSSHServer.get_Channels: TiphSSHChannelList;
begin
  Result := FChannels;
end;

function  TiphSSHServer.get_Connections: TiphSSHConnectionList;
begin
  Result := FConnections;
end;

function  TiphSSHServer.get_KeyboardInteractivePrompts: TiphSSHPromptList;
begin
  Result := FKeyboardInteractivePrompts;
end;

procedure TiphSSHServer.set_KeyboardInteractivePrompts(Value : TiphSSHPromptList);
begin
  FKeyboardInteractivePrompts.Assign(Value);
end;


{*********************************************************************************}

{$IFNDEF DELPHI3}
(*** Methods ***)
procedure TiphSSHServer.ChangeRecordLength(ChannelId: Integer; RecordLength: Integer);
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 2 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  param[0] := Pointer(ChannelId);

  param[1] := Pointer(RecordLength);

  if @_IPWorksSSH_SSHServer_Do = nil then exit;
  err := _IPWorksSSH_SSHServer_Do(m_ctl, MID_SSHServer_ChangeRecordLength{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 2, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSSHServer.CloseChannel(ChannelId: Integer);
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  param[0] := Pointer(ChannelId);

  if @_IPWorksSSH_SSHServer_Do = nil then exit;
  err := _IPWorksSSH_SSHServer_Do(m_ctl, MID_SSHServer_CloseChannel{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


function TiphSSHServer.Config(ConfigurationString: String): String;
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
  tmp_ConfigurationString: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := '';

  tmp_ConfigurationString := {$ifndef UNICODE_ON_ANSI}ConfigurationString{$else}ToLinuxAnsiString(ConfigurationString){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_ConfigurationString){$else}@tmp_ConfigurationString[0]{$endif};

  if @_IPWorksSSH_SSHServer_Do = nil then exit;
  err := _IPWorksSSH_SSHServer_Do(m_ctl, MID_SSHServer_Config{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  result := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(param[1]);
end;


procedure TiphSSHServer.Disconnect(ConnectionId: Integer);
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  param[0] := Pointer(ConnectionId);

  if @_IPWorksSSH_SSHServer_Do = nil then exit;
  err := _IPWorksSSH_SSHServer_Do(m_ctl, MID_SSHServer_Disconnect{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSSHServer.DoEvents();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SSHServer_Do = nil then exit;
  err := _IPWorksSSH_SSHServer_Do(m_ctl, MID_SSHServer_DoEvents{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSSHServer.ExchangeKeys(ConnectionId: Integer);
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  param[0] := Pointer(ConnectionId);

  if @_IPWorksSSH_SSHServer_Do = nil then exit;
  err := _IPWorksSSH_SSHServer_Do(m_ctl, MID_SSHServer_ExchangeKeys{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


function TiphSSHServer.GetSSHParam(Payload: TBytes; Field: String): String;
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
  i, err: Integer;
  tmp_Field: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 2 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := '';

  param[0] := Pointer(Payload);
  paramcb[0] := Length(Payload);

  tmp_Field := {$ifndef UNICODE_ON_ANSI}Field{$else}ToLinuxAnsiString(Field){$endif};
  param[1] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_Field){$else}@tmp_Field[0]{$endif};

  if @_IPWorksSSH_SSHServer_Do = nil then exit;
  err := _IPWorksSSH_SSHServer_Do(m_ctl, MID_SSHServer_GetSSHParam{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 2, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  result := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(param[2]);
end;


function TiphSSHServer.GetSSHParamBytes(Payload: TBytes; Field: String): TBytes;
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
  i, err: Integer;
  tmp_Field: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 2 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := nil;

  param[0] := Pointer(Payload);
  paramcb[0] := Length(Payload);

  tmp_Field := {$ifndef UNICODE_ON_ANSI}Field{$else}ToLinuxAnsiString(Field){$endif};
  param[1] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_Field){$else}@tmp_Field[0]{$endif};

  if @_IPWorksSSH_SSHServer_Do = nil then exit;
  err := _IPWorksSSH_SSHServer_Do(m_ctl, MID_SSHServer_GetSSHParamBytes{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 2, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  SetLength(result, paramcb[2]);
  Move(Pointer(param[2])^, Pointer(result)^, paramcb[2]); // IMPORTANT: Do NOT multiply the length value!
end;


function TiphSSHServer.OpenChannel(ConnectionId: Integer; ChannelType: String): String;
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
  i, err: Integer;
  tmp_ChannelType: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 2 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := '';

  param[0] := Pointer(ConnectionId);

  tmp_ChannelType := {$ifndef UNICODE_ON_ANSI}ChannelType{$else}ToLinuxAnsiString(ChannelType){$endif};
  param[1] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_ChannelType){$else}@tmp_ChannelType[0]{$endif};

  if @_IPWorksSSH_SSHServer_Do = nil then exit;
  err := _IPWorksSSH_SSHServer_Do(m_ctl, MID_SSHServer_OpenChannel{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 2, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  result := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(param[2]);
end;


procedure TiphSSHServer.Reset();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SSHServer_Do = nil then exit;
  err := _IPWorksSSH_SSHServer_Do(m_ctl, MID_SSHServer_Reset{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSSHServer.SendBytes(ChannelId: Integer; Data: TBytes);
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 2 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  param[0] := Pointer(ChannelId);

  param[1] := Pointer(Data);
  paramcb[1] := Length(Data);

  if @_IPWorksSSH_SSHServer_Do = nil then exit;
  err := _IPWorksSSH_SSHServer_Do(m_ctl, MID_SSHServer_SendBytes{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 2, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSSHServer.SendChannelData(ChannelId: Integer; Data: TBytes);
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 2 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  param[0] := Pointer(ChannelId);

  param[1] := Pointer(Data);
  paramcb[1] := Length(Data);

  if @_IPWorksSSH_SSHServer_Do = nil then exit;
  err := _IPWorksSSH_SSHServer_Do(m_ctl, MID_SSHServer_SendChannelData{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 2, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSSHServer.SendSSHPacket(ChannelId: Integer; PacketType: Integer; Payload: TBytes);
var
  param: array[0..3] of Pointer;
  paramcb: array[0..3] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 3 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  param[0] := Pointer(ChannelId);

  param[1] := Pointer(PacketType);

  param[2] := Pointer(Payload);
  paramcb[2] := Length(Payload);

  if @_IPWorksSSH_SSHServer_Do = nil then exit;
  err := _IPWorksSSH_SSHServer_Do(m_ctl, MID_SSHServer_SendSSHPacket{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 3, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSSHServer.SendText(ChannelId: Integer; Text: String);
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
  i, err: Integer;
  tmp_Text: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 2 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  param[0] := Pointer(ChannelId);

  tmp_Text := {$ifndef UNICODE_ON_ANSI}Text{$else}ToLinuxAnsiString(Text){$endif};
  param[1] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_Text){$else}@tmp_Text[0]{$endif};

  if @_IPWorksSSH_SSHServer_Do = nil then exit;
  err := _IPWorksSSH_SSHServer_Do(m_ctl, MID_SSHServer_SendText{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 2, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


function TiphSSHServer.SetSSHParam(Payload: TBytes; FieldType: String; FieldValue: String): TBytes;
var
  param: array[0..3] of Pointer;
  paramcb: array[0..3] of Integer;
  i, err: Integer;
  tmp_FieldType: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
  tmp_FieldValue: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 3 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := nil;

  param[0] := Pointer(Payload);
  paramcb[0] := Length(Payload);

  tmp_FieldType := {$ifndef UNICODE_ON_ANSI}FieldType{$else}ToLinuxAnsiString(FieldType){$endif};
  param[1] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_FieldType){$else}@tmp_FieldType[0]{$endif};

  tmp_FieldValue := {$ifndef UNICODE_ON_ANSI}FieldValue{$else}ToLinuxAnsiString(FieldValue){$endif};
  param[2] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_FieldValue){$else}@tmp_FieldValue[0]{$endif};

  if @_IPWorksSSH_SSHServer_Do = nil then exit;
  err := _IPWorksSSH_SSHServer_Do(m_ctl, MID_SSHServer_SetSSHParam{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 3, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  SetLength(result, paramcb[3]);
  Move(Pointer(param[3])^, Pointer(result)^, paramcb[3]); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSSHServer.Shutdown();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SSHServer_Do = nil then exit;
  err := _IPWorksSSH_SSHServer_Do(m_ctl, MID_SSHServer_Shutdown{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSSHServer.StartListening();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SSHServer_Do = nil then exit;
  err := _IPWorksSSH_SSHServer_Do(m_ctl, MID_SSHServer_StartListening{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSSHServer.StopListening();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SSHServer_Do = nil then exit;
  err := _IPWorksSSH_SSHServer_Do(m_ctl, MID_SSHServer_StopListening{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


{$ENDIF}


{$IFDEF USE_DYNAMIC_LOADING}

procedure ClearProcs();
begin
  _IPWorksSSH_EvtStr := nil;
  _IPWorksSSH_Stream := nil;
  _IPWorksSSH_SSHServer_Create := nil;
  _IPWorksSSH_SSHServer_Destroy := nil;
  _IPWorksSSH_SSHServer_Set := nil;
  _IPWorksSSH_SSHServer_Get := nil;
  _IPWorksSSH_SSHServer_GetLastError := nil;
  _IPWorksSSH_SSHServer_GetLastErrorCode := nil;
  _IPWorksSSH_SSHServer_SetLastErrorAndCode := nil;
  _IPWorksSSH_SSHServer_GetEventError := nil;
  _IPWorksSSH_SSHServer_GetEventErrorCode := nil;
  _IPWorksSSH_SSHServer_SetEventErrorAndCode := nil;
  {$IFDEF USE_EXPLICIT_INIT}
  _IPWorksSSH_SSHServer_StaticInit := nil;
  _IPWorksSSH_SSHServer_StaticDestroy := nil;
  {$ENDIF}
  _IPWorksSSH_SSHServer_CheckIndex := nil;
  _IPWorksSSH_SSHServer_Do := nil;
end;

{$IFDEF USEIPWORKSSSHDLL}
procedure LoadDLL();
begin
  ClearProcs();

  hLib := LoadLibrary(PChar(DLLNAME));
  if hLib = 0 then exit;

  @_IPWorksSSH_EvtStr                         := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_EvtStr');
  @_IPWorksSSH_Stream                         := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_Stream');
  @_IPWorksSSH_SSHServer_Create               := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHServer_Create');
  @_IPWorksSSH_SSHServer_Destroy              := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHServer_Destroy');
  @_IPWorksSSH_SSHServer_Set                  := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHServer_Set');
  @_IPWorksSSH_SSHServer_Get                  := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHServer_Get');
  @_IPWorksSSH_SSHServer_GetLastError         := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHServer_GetLastError');
  @_IPWorksSSH_SSHServer_GetLastErrorCode     := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHServer_GetLastErrorCode');
  @_IPWorksSSH_SSHServer_SetLastErrorAndCode  := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHServer_SetLastErrorAndCode');
  @_IPWorksSSH_SSHServer_GetEventError        := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHServer_GetEventError');
  @_IPWorksSSH_SSHServer_GetEventErrorCode    := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHServer_GetEventErrorCode');
  @_IPWorksSSH_SSHServer_SetEventErrorAndCode := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHServer_SetEventErrorAndCode');
  {$IFDEF USE_EXPLICIT_INIT}
  @_IPWorksSSH_SSHServer_StaticInit           := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHServer_StaticInit');
  @_IPWorksSSH_SSHServer_StaticDestroy        := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHServer_StaticDestroy');
  {$ENDIF}
  @_IPWorksSSH_SSHServer_CheckIndex           := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHServer_CheckIndex');
  @_IPWorksSSH_SSHServer_Do                   := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHServer_Do');
end;
{$ELSE}
procedure LoadRESDLL();
var
  hResInfo: HRSRC;
  hResData: HGLOBAL;
  pResData: Pointer;
begin
  ClearProcs();

  hResInfo := FindResource(HInstance, 'ipworksssh22_dta', RT_RCDATA);
  if hResInfo = 0 then exit;

  hResData := LoadResource(HInstance, hResInfo);
  if hResData = 0 then exit;

  pResData := LockResource(hResData);
  if pResData = nil then exit;

  pBaseAddress := IPWorksSSHLoadDRU(pResData, pEntryPoint);
  if Assigned(pBaseAddress) then begin
    @_IPWorksSSH_EvtStr                         := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_EvtStr');
    @_IPWorksSSH_Stream                         := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_Stream');
    @_IPWorksSSH_SSHServer_Create               := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHServer_Create');
    @_IPWorksSSH_SSHServer_Destroy              := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHServer_Destroy');
    @_IPWorksSSH_SSHServer_Set                  := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHServer_Set');
    @_IPWorksSSH_SSHServer_Get                  := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHServer_Get');
    @_IPWorksSSH_SSHServer_GetLastError         := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHServer_GetLastError');
    @_IPWorksSSH_SSHServer_GetLastErrorCode     := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHServer_GetLastErrorCode');
    @_IPWorksSSH_SSHServer_SetLastErrorAndCode  := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHServer_SetLastErrorAndCode');
    @_IPWorksSSH_SSHServer_GetEventError        := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHServer_GetEventError');
    @_IPWorksSSH_SSHServer_GetEventErrorCode    := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHServer_GetEventErrorCode');
    @_IPWorksSSH_SSHServer_SetEventErrorAndCode := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHServer_SetEventErrorAndCode');
    @_IPWorksSSH_SSHServer_CheckIndex           := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHServer_CheckIndex');
    @_IPWorksSSH_SSHServer_Do                   := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHServer_Do');
  end;

  FreeResource(hResData);
end;
{$ENDIF USEIPWORKSSSHDLL}

initialization
begin
  iLoadCount := iLoadCount + 1;
  if iLoadCount > 1 then exit;

  {$IFDEF USEIPWORKSSSHDLL}
  hLib := 0;
  LoadDLL();
  {$ELSE}
  pBaseAddress := nil;
  pEntryPoint := nil;
  LoadResDLL();
  {$ENDIF}

  {$IFDEF USE_EXPLICIT_INIT}
  if @_IPWorksSSH_SSHServer_StaticInit <> nil then
    _IPWorksSSH_SSHServer_StaticInit(nil);
  {$ENDIF}
end;

finalization
begin
  iLoadCount := iLoadCount - 1;
  if iLoadCount > 0 then exit;

  {$IFDEF USE_EXPLICIT_INIT}
  if @_IPWorksSSH_SSHServer_StaticDestroy <> nil then
    _IPWorksSSH_SSHServer_StaticDestroy();
  {$ENDIF}

  ClearProcs();

  {$IFDEF USEIPWORKSSSHDLL}
  if hLib <> 0 then
  begin
    {$ifndef FPC}FreeLibrary{$else}UnloadLibrary{$endif}(hLib);
    hLib := 0;
  end;
  {$ELSE}
  IPWorksSSHFreeDRU(pBaseAddress, pEntryPoint);
  pBaseAddress := nil;
  pEntryPoint := nil;
  {$ENDIF}
  iLoadCount := 0;
end;

{$ELSE USE_DYNAMIC_LOADING}

// in case if static library used

{$IFDEF USE_EXPLICIT_INIT}

initialization
  _IPWorksSSH_SSHServer_StaticInit(nil);

finalization
  _IPWorksSSH_SSHServer_StaticDestroy();

{$ENDIF USE_EXPLICIT_INIT}

{$ENDIF USE_DYNAMIC_LOADING}


end.
