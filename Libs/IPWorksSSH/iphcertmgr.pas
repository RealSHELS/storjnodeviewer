
unit iphcertmgr;



{$UNDEF USE_DYNAMIC_LOADING}
{$UNDEF USE_EXPLICIT_INIT}
{$UNDEF ANSICHAR_IS_BYTE}
{$UNDEF NO_ANSI}
{$UNDEF USE_CDECL}

{$IFDEF ENFORCE_DYNAMIC_LOADING}
  {$DEFINE USE_DYNAMIC_LOADING}
  {$DEFINE USEIPWORKSSSHDLL}
{$ENDIF}

{$IFDEF FPC}  //Lazarus
  {$IFDEF FPC_UNICODESTRINGS}
    // ensure that mode Delphi Unicode is enabled, not a modeswitch unicodestrings
    {$MODE DELPHIUNICODE}
  {$ELSE}
    {$MODE DELPHI}
  {$ENDIF}
  {$DEFINE USEIPWORKSSSHDLL}
  {$IFDEF DARWIN}
    {$DEFINE MACOS}
  {$ENDIF}
{$ENDIF}

{$IFNDEF FPC}
  {$IF CompilerVersion >= 24} // XE3+
    {$LEGACYIFEND ON}
  {$IFEND}
{$ENDIF}

{$IF DEFINED(LINUX) OR DEFINED(ANDROID) OR DEFINED(MACOS) OR DEFINED(IOS)}
  {$DEFINE USEIPWORKSSSHDLL}
  {$DEFINE ANSICHAR_IS_BYTE}
  {$DEFINE NO_ANSI}
  {$DEFINE USE_CDECL}
{$IFEND}

{$IF DEFINED(LINUX) OR DEFINED(MACOS)}
  {$IFNDEF IOS}
    {$DEFINE USE_DYNAMIC_LOADING}
  {$ENDIF}
{$IFEND}

{$IF DEFINED(ANDROID)}
  {$DEFINE USE_EXPLICIT_INIT}
{$IFEND}

{$IFDEF USE_IPWORKSSSH_ANDROID_SHARED_LIBRARY}
  {$DEFINE USE_DYNAMIC_LOADING}
{$ENDIF}


{$IF DEFINED(UNICODE) AND NOT DEFINED(MSWINDOWS)}
  {$DEFINE NONWINUNICODE}
{$IFEND}



{$IFNDEF USEIPWORKSSSHDLL}
  // in case if loading DLL from resources
  {$DEFINE USE_DYNAMIC_LOADING}
{$ENDIF}

{$UNDEF UNICODELIB}
{$IF DEFINED(MSWINDOWS)}
  {$DEFINE UNICODELIB}
{$IFEND}


{$IFDEF UNICODE}
  {$IFNDEF UNICODELIB}
    {$DEFINE UNICODE_ON_ANSI}
  {$ELSE}
    {$DEFINE UNICODE_ON_UNICODE}
  {$ENDIF}
{$ELSE}
  {$IFDEF UNICODELIB}
    {$DEFINE ANSI_ON_UNICODE}
  {$ELSE}
    {$DEFINE ANSI_ON_ANSI}
  {$ENDIF}
{$ENDIF}

interface

uses
{$IFDEF FPC}  //Lazarus
  SysUtils, Classes,
{$ELSE}
  {$IF CompilerVersion >= 23}System.SysUtils{$ELSE}SysUtils{$IFEND}, {$IF CompilerVersion >= 23}System.Classes{$ELSE}Classes{$IFEND}, 
{$ENDIF}
  {$IFDEF FPC}
  dynlibs,
  {$ELSE}
  {$IFDEF POSIX}
  Posix.Base,
  {$ENDIF}
  {$ENDIF}
  iphtypes, iphcore, iphkeys;

type

  (*** Enum Types ***)
  TiphcertmgrCertStoreTypes = iphtypes.TiphCertStoreTypes;

  (*** Event Delegate Types ***)
  TCertChainEvent = procedure (
    Sender: TObject;
    CertEncoded: String;
    CertEncodedB: TBytes;
    const CertSubject: String;
    const CertIssuer: String;
    const CertSerialNumber: String;
    TrustStatus: Integer;
    TrustInfo: Integer
  ) of Object;


  TCertListEvent = procedure (
    Sender: TObject;
    CertEncoded: String;
    CertEncodedB: TBytes;
    const CertSubject: String;
    const CertIssuer: String;
    const CertSerialNumber: String;
    HasPrivateKey: Boolean
  ) of Object;


  TErrorEvent = procedure (
    Sender: TObject;
    ErrorCode: Integer;
    const Description: String
  ) of Object;


  TKeyListEvent = procedure (
    Sender: TObject;
    const KeyContainer: String;
    KeyType: Integer;
    const AlgId: String;
    KeyLen: Integer
  ) of Object;


  TLogEvent = procedure (
    Sender: TObject;
    LogLevel: Integer;
    const Message: String;
    const LogType: String
  ) of Object;


  TStoreListEvent = procedure (
    Sender: TObject;
    const CertStore: String
  ) of Object;


  (*** Exception Type ***)
  EiphCertMgr = class(EIPWorksSSH)
  end;

  (*** Component Type ***)
{$IFNDEF FPC}
  {$IF CompilerVersion >= 23}
  [ComponentPlatformsAttribute(
    pidWin32 or pidWin64
    {$IF CompilerVersion >= 29}
    or pidLinux64
    {$IFEND}
    {$IF CompilerVersion >= 31}
    or pidOSX64
    {$IFEND}
   )]
  {$IFEND}
{$ENDIF}
  TiphCertMgr = class(TiphTypesCore)
    private
      (*** Event Handler Procedure Pointers ***)
      FOnCertChain: TCertChainEvent;

      FOnCertList: TCertListEvent;

      FOnError: TErrorEvent;

      FOnKeyList: TKeyListEvent;

      FOnLog: TLogEvent;

      FOnStoreList: TStoreListEvent;

      m_ctl: Pointer;
      (*** Inner objects for types and collections ***)
      FCertExtensions: TiphCertExtensionList;

      function HasData: Boolean;
      procedure ReadHnd(Reader: TStream);
      procedure WriteHnd(Writer: TStream);
      function BStr2CStr(pBStr: Pointer; lenBStr: Integer; bArr: TBytes): String;

      procedure SetRuntimeLicense(key: String);
      function GetRuntimeLicense: String;

    protected
      procedure AboutDlg; override;
      procedure DefineProperties(Filer: TFiler); override;

      function ThrowCoreException(Err: Integer; const Desc: {$ifndef NO_ANSI}AnsiString{$else}string{$endif}): EIPWorksSSH;
      procedure TreatErr(Err: Integer; const Desc: String);

      (*** Property Getters/Setters ***)
      function  get_CertEffectiveDate: String;

      function  get_CertEncoded: String;

      procedure set_CertEncoded(valCertEncoded: String);

      function  get_CertEncodedB: TBytes;

      procedure set_CertEncodedB(valCertEncoded: TBytes);

      function  get_CertExpirationDate: String;

      function  get_CertExtendedKeyUsage: String;

      procedure set_CertExtendedKeyUsage(valCertExtendedKeyUsage: String);

      function  get_CertFingerprint: String;

      function  get_CertFingerprintSHA1: String;

      function  get_CertFingerprintSHA256: String;

      function  get_CertIssuer: String;

      function  get_CertKeyPassword: String;

      procedure set_CertKeyPassword(valCertKeyPassword: String);

      function  get_CertPrivateKey: String;

      function  get_CertPrivateKeyAvailable: Boolean;

      function  get_CertPrivateKeyContainer: String;

      function  get_CertPublicKey: String;

      function  get_CertPublicKeyAlgorithm: String;

      procedure set_CertPublicKeyAlgorithm(valCertPublicKeyAlgorithm: String);

      function  get_CertPublicKeyLength: Integer;

      function  get_CertSerialNumber: String;

      function  get_CertSignatureAlgorithm: String;

      function  get_CertSubject: String;

      procedure set_CertSubject(valCertSubject: String);

      function  get_CertSubjectAltNames: String;

      function  get_CertThumbprintMD5: String;

      function  get_CertThumbprintSHA1: String;

      function  get_CertThumbprintSHA256: String;

      function  get_CertUsage: String;

      procedure set_CertUsage(valCertUsage: String);

      function  get_CertUsageFlags: Integer;

      procedure set_CertUsageFlags(valCertUsageFlags: Integer);

      function  get_CertVersion: String;

      function  get_CertExtensionCount: Integer;

      procedure set_CertExtensionCount(valCertExtensionCount: Integer);

      function  get_CertExtensionCritical(CertExtensionIndex: Integer): Boolean;

      function  get_CertExtensionOID(CertExtensionIndex: Integer): String;

      function  get_CertExtensionValue(CertExtensionIndex: Integer): String;

      function  get_CertExtensionValueB(CertExtensionIndex: Integer): TBytes;

      function  get_CertStore: String;

      procedure set_CertStore(valCertStore: String);

      function  get_CertStoreB: TBytes;

      procedure set_CertStoreB(valCertStore: TBytes);

      function  get_CertStorePassword: String;

      procedure set_CertStorePassword(valCertStorePassword: String);

      function  get_CertStoreType: TiphcertmgrCertStoreTypes;

      procedure set_CertStoreType(valCertStoreType: TiphcertmgrCertStoreTypes);

      function  get_ExportedCert: String;

      function  get_ExportedCertB: TBytes;

      function  get_ExportFormat: String;

      procedure set_ExportFormat(valExportFormat: String);

      function  get_ExportPrivateKey: Boolean;

      procedure set_ExportPrivateKey(valExportPrivateKey: Boolean);


      (*** Property Getters/Setters: OO API ***)
      function  get_CertExtensions: TiphCertExtensionList;
      procedure set_CertExtensions(Value : TiphCertExtensionList);
      

    public
      constructor Create(AOwner: TComponent); overload; override;
      constructor Create(AOwner: TComponent; RuntimeLicense: string); reintroduce; overload;
      destructor Destroy; override;
      function ReportEventException(E: Exception; Event: String; ReFire: Boolean = True): Integer;

      property RuntimeLicense: String read GetRuntimeLicense write SetRuntimeLicense;

      procedure SetCertEncoded(lpCertEncoded: PLXAnsiChar; lenCertEncoded: Cardinal);

      procedure SetCertExtensionValue(CertExtensionIndex: Integer; lpCertExtensionValue: PLXAnsiChar; lenCertExtensionValue: Cardinal);

      procedure SetCertStore(lpCertStore: PLXAnsiChar; lenCertStore: Cardinal);

      procedure SetExportedCert(lpExportedCert: PLXAnsiChar; lenExportedCert: Cardinal);


      (*** Runtime Property Definitions ***)

      property CertEncoded: String read get_CertEncoded write set_CertEncoded;



      property CertEncodedB: TBytes read get_CertEncodedB write set_CertEncodedB;



      property CertExtensionCount: Integer read get_CertExtensionCount write set_CertExtensionCount;



      property CertExtensionCritical[CertExtensionIndex: Integer]: Boolean read get_CertExtensionCritical;



      property CertExtensionOID[CertExtensionIndex: Integer]: String read get_CertExtensionOID;



      property CertExtensionValue[CertExtensionIndex: Integer]: String read get_CertExtensionValue;



      property CertExtensionValueB[CertExtensionIndex: Integer]: TBytes read get_CertExtensionValueB;



      property CertStoreB: TBytes read get_CertStoreB write set_CertStoreB;



      property ExportedCert: String read get_ExportedCert;



      property ExportedCertB: TBytes read get_ExportedCertB;



      (*** Runtime properties: OO API ***)
      property CertExtensions: TiphCertExtensionList read get_CertExtensions write set_CertExtensions;


{$IFNDEF DELPHI3}
      (*** Method Definitions ***)
      function  Config(ConfigurationString: String): String;

      procedure CreateCertificate(CertSubject: String; SerialNumber: Integer);

      procedure CreateKey(KeyName: String);

      procedure DeleteCertificate();

      procedure DeleteKey(KeyName: String);

      procedure ExportCertificate(CertFile: String; Password: String);

      function  GenerateCSR(CertSubject: String; KeyName: String): String;

      procedure ImportCertificate(CertFile: String; Password: String; Subject: String);

      procedure ImportSignedCSR(SignedCSR: TBytes; KeyName: String);

      procedure IssueCertificate(CertSubject: String; SerialNumber: Integer);

      function  ListCertificateStores(): String;

      function  ListKeys(): String;

      function  ListMachineStores(): String;

      function  ListStoreCertificates(): String;

      procedure ReadCertificate(FileName: String);

      function  ReadCSR(CSR: String): String;

      procedure Reset();

      procedure SaveCertificate(FileName: String);

      function  ShowCertificateChain(): String;

      function  SignCSR(CSR: TBytes; SerialNumber: Integer): String;

{$ENDIF}

    published
      (*** Design-time Property Definitions ***)

      property CertEffectiveDate: String read get_CertEffectiveDate write SetNoopString stored False;



      property CertExpirationDate: String read get_CertExpirationDate write SetNoopString stored False;



      property CertExtendedKeyUsage: String read get_CertExtendedKeyUsage write set_CertExtendedKeyUsage;



      property CertFingerprint: String read get_CertFingerprint write SetNoopString stored False;



      property CertFingerprintSHA1: String read get_CertFingerprintSHA1 write SetNoopString stored False;



      property CertFingerprintSHA256: String read get_CertFingerprintSHA256 write SetNoopString stored False;



      property CertIssuer: String read get_CertIssuer write SetNoopString stored False;



      property CertKeyPassword: String read get_CertKeyPassword write set_CertKeyPassword;



      property CertPrivateKey: String read get_CertPrivateKey write SetNoopString stored False;



      property CertPrivateKeyAvailable: Boolean read get_CertPrivateKeyAvailable write SetNoopBoolean stored False;



      property CertPrivateKeyContainer: String read get_CertPrivateKeyContainer write SetNoopString stored False;



      property CertPublicKey: String read get_CertPublicKey write SetNoopString stored False;



      property CertPublicKeyAlgorithm: String read get_CertPublicKeyAlgorithm write set_CertPublicKeyAlgorithm;



      property CertPublicKeyLength: Integer read get_CertPublicKeyLength write SetNoopInteger stored False;



      property CertSerialNumber: String read get_CertSerialNumber write SetNoopString stored False;



      property CertSignatureAlgorithm: String read get_CertSignatureAlgorithm write SetNoopString stored False;



      property CertSubject: String read get_CertSubject write set_CertSubject;



      property CertSubjectAltNames: String read get_CertSubjectAltNames write SetNoopString stored False;



      property CertThumbprintMD5: String read get_CertThumbprintMD5 write SetNoopString stored False;



      property CertThumbprintSHA1: String read get_CertThumbprintSHA1 write SetNoopString stored False;



      property CertThumbprintSHA256: String read get_CertThumbprintSHA256 write SetNoopString stored False;



      property CertUsage: String read get_CertUsage write set_CertUsage;



      property CertUsageFlags: Integer read get_CertUsageFlags write set_CertUsageFlags default 0;



      property CertVersion: String read get_CertVersion write SetNoopString stored False;



      property CertStore: String read get_CertStore write set_CertStore;



      property CertStorePassword: String read get_CertStorePassword write set_CertStorePassword;



      property CertStoreType: TiphcertmgrCertStoreTypes read get_CertStoreType write set_CertStoreType default cstUser;



      property ExportFormat: String read get_ExportFormat write set_ExportFormat;



      property ExportPrivateKey: Boolean read get_ExportPrivateKey write set_ExportPrivateKey default true;


      (*** Event Handler Bindings ***)
      property OnCertChain: TCertChainEvent read FOnCertChain write FOnCertChain;

      property OnCertList: TCertListEvent read FOnCertList write FOnCertList;

      property OnError: TErrorEvent read FOnError write FOnError;

      property OnKeyList: TKeyListEvent read FOnKeyList write FOnKeyList;

      property OnLog: TLogEvent read FOnLog write FOnLog;

      property OnStoreList: TStoreListEvent read FOnStoreList write FOnStoreList;

    end;

    procedure Register;

implementation


{$T-}

{$IFDEF MSWINDOWS} // Both DLL and DRU loading require Windows dependency
{$IFDEF FPC}  //Lazarus
uses Messages;
{$ELSE}
uses {$IF CompilerVersion >= 23}Winapi.Windows{$ELSE}Windows{$IFEND}, {$IF CompilerVersion >= 23}Winapi.Messages{$ELSE}Messages{$IFEND};
{$ENDIF}
{$ENDIF}

const
  PID_CertMgr_CertEffectiveDate                                  = 1;

  PID_CertMgr_CertEncoded                                        = 2;

  PID_CertMgr_CertExpirationDate                                 = 3;

  PID_CertMgr_CertExtendedKeyUsage                               = 4;

  PID_CertMgr_CertFingerprint                                    = 5;

  PID_CertMgr_CertFingerprintSHA1                                = 6;

  PID_CertMgr_CertFingerprintSHA256                              = 7;

  PID_CertMgr_CertIssuer                                         = 8;

  PID_CertMgr_CertKeyPassword                                    = 9;

  PID_CertMgr_CertPrivateKey                                     = 10;

  PID_CertMgr_CertPrivateKeyAvailable                            = 11;

  PID_CertMgr_CertPrivateKeyContainer                            = 12;

  PID_CertMgr_CertPublicKey                                      = 13;

  PID_CertMgr_CertPublicKeyAlgorithm                             = 14;

  PID_CertMgr_CertPublicKeyLength                                = 15;

  PID_CertMgr_CertSerialNumber                                   = 16;

  PID_CertMgr_CertSignatureAlgorithm                             = 17;

  PID_CertMgr_CertSubject                                        = 21;

  PID_CertMgr_CertSubjectAltNames                                = 22;

  PID_CertMgr_CertThumbprintMD5                                  = 23;

  PID_CertMgr_CertThumbprintSHA1                                 = 24;

  PID_CertMgr_CertThumbprintSHA256                               = 25;

  PID_CertMgr_CertUsage                                          = 26;

  PID_CertMgr_CertUsageFlags                                     = 27;

  PID_CertMgr_CertVersion                                        = 28;

  PID_CertMgr_CertExtensionCount                                 = 29;

  PID_CertMgr_CertExtensionCritical                              = 30;

  PID_CertMgr_CertExtensionOID                                   = 31;

  PID_CertMgr_CertExtensionValue                                 = 32;

  PID_CertMgr_CertStore                                          = 33;

  PID_CertMgr_CertStorePassword                                  = 34;

  PID_CertMgr_CertStoreType                                      = 35;

  PID_CertMgr_ExportedCert                                       = 36;

  PID_CertMgr_ExportFormat                                       = 37;

  PID_CertMgr_ExportPrivateKey                                   = 38;

  EID_CertMgr_CertChain = 1;

  EID_CertMgr_CertList = 2;

  EID_CertMgr_Error = 3;

  EID_CertMgr_KeyList = 4;

  EID_CertMgr_Log = 5;

  EID_CertMgr_StoreList = 6;

  MID_CertMgr_Config = 2;

  MID_CertMgr_CreateCertificate = 3;

  MID_CertMgr_CreateKey = 4;

  MID_CertMgr_DeleteCertificate = 5;

  MID_CertMgr_DeleteKey = 6;

  MID_CertMgr_ExportCertificate = 7;

  MID_CertMgr_GenerateCSR = 8;

  MID_CertMgr_ImportCertificate = 9;

  MID_CertMgr_ImportSignedCSR = 10;

  MID_CertMgr_IssueCertificate = 11;

  MID_CertMgr_ListCertificateStores = 12;

  MID_CertMgr_ListKeys = 13;

  MID_CertMgr_ListMachineStores = 14;

  MID_CertMgr_ListStoreCertificates = 15;

  MID_CertMgr_ReadCertificate = 16;

  MID_CertMgr_ReadCSR = 17;

  MID_CertMgr_Reset = 18;

  MID_CertMgr_SaveCertificate = 19;

  MID_CertMgr_ShowCertificateChain = 20;

  MID_CertMgr_SignCSR = 21;

  CREATE_OPT = {$IFDEF UNICODE}1{$ELSE}0{$ENDIF};
  EVTSTR_OPT = {$IFDEF UNICODE}2{$ELSE}1{$ENDIF};

  {$IFDEF MSWINDOWS}
  DLLNAME = 'IPWORKSSSH22.DLL';
  {$ENDIF}
  {$IFDEF LINUX}
  DLLNAME = 'libipworksssh.so.22.0';
  {$ENDIF}
  {$IFDEF ANDROID}
  {$IFDEF USE_DYNAMIC_LOADING}
  DLLNAME = 'libipworksssh.so.22.0';
  {$ELSE}
  DLLNAME = 'libipworksssh.22.0.a';
  {$ENDIF}
  {$ENDIF}
  {$IFDEF IOS}
  DLLNAME = 'libipworksssh.22.0.a';
  {$ELSE}
  {$IFDEF MACOS}
  DLLNAME = '@rpath/libipworksssh.22.0.dylib';
  {$ENDIF}
  {$ENDIF}

{$WARNINGS OFF}

type
  INTARR = array[0..32] of Integer;
  LPINTARR = ^INTARR;
  LPINT = ^Cardinal;
  MEventHandle = function(lpContext: TiphCertMgr; event_id: Integer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  PEventHandle = ^MEventHandle;


{$IFNDEF USEIPWORKSSSHDLL}
var
  pEntryPoint: Pointer;
  pBaseAddress: Pointer;
  iLoadCount: Integer;
{$ELSE}
{$IFDEF USE_DYNAMIC_LOADING}
var
  hLib : {$ifndef FPC}HMODULE{$else}TLibHandle{$endif};
  iLoadCount: Integer;
{$ENDIF USE_DYNAMIC_LOADING}
{$ENDIF USEIPWORKSSSHDLL}


{$IFDEF USE_DYNAMIC_LOADING}
  _IPWorksSSH_EvtStr:                        function(pEvtStr: Pointer; id: Integer; value: Pointer; opt: Integer): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_Stream:                        function(pStream: Pointer; op: Integer; params: LPVOIDARR; retVal: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_CertMgr_Create:                function(pMethod: PEventHandle; pObject: TiphCertMgr; pKey: Pointer; opts: Integer): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_CertMgr_Destroy:               function(p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_CertMgr_Set:                   function(p: Pointer; index: Integer; arridx: Integer; value: Pointer; len: Cardinal): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_CertMgr_Get:                   function(p: Pointer; index: Integer; arridx: Integer; len: LPINT; llVal: PInt64): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_CertMgr_GetLastError:          function(p: Pointer): PLXAnsiChar; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_CertMgr_GetLastErrorCode:      function(p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_CertMgr_SetLastErrorAndCode:   function(p: Pointer; code: Integer; message: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_CertMgr_GetEventError:         function(p: Pointer): PLXAnsiChar; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_CertMgr_GetEventErrorCode:     function(p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_CertMgr_SetEventErrorAndCode:  function(p: Pointer; code: Integer; message: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  {$IFDEF USE_EXPLICIT_INIT}
  _IPWorksSSH_CertMgr_StaticInit:            function(instance: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_CertMgr_StaticDestroy:         function(): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  {$ENDIF}
  _IPWorksSSH_CertMgr_CheckIndex:            function(p: Pointer; index: Integer; arridx: Integer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_CertMgr_Do:                    function(p: Pointer; method_id: Integer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR; llVal: PInt64): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
{$ELSE}
  function _IPWorksSSH_EvtStr                       (pEvtStr: Pointer; id: Integer; value: Pointer; opt: Integer): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_EvtStr' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_Stream                       (pStream: Pointer; op: Integer; params: LPVOIDARR; retVal: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_Stream' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_CertMgr_Create               (pMethod: PEventHandle; pObject: TiphCertMgr; pKey: Pointer; opts: Integer): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_CertMgr_Create' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_CertMgr_Destroy              (p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_CertMgr_Destroy' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_CertMgr_Set                  (p: Pointer; index: Integer; arridx: Integer; value: Pointer; len: Cardinal): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_CertMgr_Set' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_CertMgr_Get                  (p: Pointer; index: Integer; arridx: Integer; len: LPINT; llVal: PInt64): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_CertMgr_Get' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_CertMgr_GetLastError         (p: Pointer): PLXAnsiChar; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_CertMgr_GetLastError' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_CertMgr_GetLastErrorCode     (p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_CertMgr_GetLastErrorCode' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_CertMgr_SetLastErrorAndCode  (p: Pointer; code: Integer; message: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_CertMgr_SetLastErrorAndCode' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_CertMgr_GetEventError        (p: Pointer): PLXAnsiChar; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_CertMgr_GetEventError' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_CertMgr_GetEventErrorCode    (p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_CertMgr_GetEventErrorCode' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_CertMgr_SetEventErrorAndCode (p: Pointer; code: Integer; message: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_CertMgr_SetEventErrorAndCode' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_CertMgr_StaticInit           (instance: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_CertMgr_StaticInit' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_CertMgr_StaticDestroy        (): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_CertMgr_StaticDestroy' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_CertMgr_CheckIndex           (p: Pointer; index: Integer; arridx: Integer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_CertMgr_CheckIndex' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_CertMgr_Do                   (p: Pointer; method_id: Integer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR; llVal: PInt64): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_CertMgr_Do' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
{$ENDIF}

{$HINTS OFF}

(*** Fire Event Methods ***)
function FireCertChain(lpContext: TiphCertMgr; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_CertEncoded: String;
  tmp_CertEncodedB: TBytes;

  tmp_CertSubject: String;

  tmp_CertIssuer: String;

  tmp_CertSerialNumber: String;

  tmp_TrustStatus: Integer;

  tmp_TrustInfo: Integer;

begin
  result := 0;
  if not Assigned(lpContext.FOnCertChain) then exit;

  SetLength(tmp_CertEncodedB, cbparam^[0]);
  Move(Pointer(params^[0])^, Pointer(tmp_CertEncodedB)^, cbparam^[0]); // IMPORTANT: Do NOT multiply the length value!
  tmp_CertEncoded := lpContext.BStr2CStr(params^[0], cbparam^[0], tmp_CertEncodedB);
  tmp_CertSubject := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_CertIssuer := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_CertSerialNumber := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[3]);
  tmp_TrustStatus := Integer(params^[4]);

  tmp_TrustInfo := Integer(params^[5]);

  try lpContext.FOnCertChain(lpContext, tmp_CertEncoded, tmp_CertEncodedB, tmp_CertSubject, tmp_CertIssuer, tmp_CertSerialNumber, tmp_TrustStatus, tmp_TrustInfo)
  except on E: Exception do result := lpContext.ReportEventException(E, 'CertChain'); end;

end;


function FireCertList(lpContext: TiphCertMgr; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_CertEncoded: String;
  tmp_CertEncodedB: TBytes;

  tmp_CertSubject: String;

  tmp_CertIssuer: String;

  tmp_CertSerialNumber: String;

  tmp_HasPrivateKey: Boolean;

begin
  result := 0;
  if not Assigned(lpContext.FOnCertList) then exit;

  SetLength(tmp_CertEncodedB, cbparam^[0]);
  Move(Pointer(params^[0])^, Pointer(tmp_CertEncodedB)^, cbparam^[0]); // IMPORTANT: Do NOT multiply the length value!
  tmp_CertEncoded := lpContext.BStr2CStr(params^[0], cbparam^[0], tmp_CertEncodedB);
  tmp_CertSubject := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_CertIssuer := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_CertSerialNumber := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[3]);
  tmp_HasPrivateKey := Boolean(params^[4]);

  try lpContext.FOnCertList(lpContext, tmp_CertEncoded, tmp_CertEncodedB, tmp_CertSubject, tmp_CertIssuer, tmp_CertSerialNumber, tmp_HasPrivateKey)
  except on E: Exception do result := lpContext.ReportEventException(E, 'CertList'); end;

end;


function FireError(lpContext: TiphCertMgr; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ErrorCode: Integer;

  tmp_Description: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnError) then exit;

  tmp_ErrorCode := Integer(params^[0]);

  tmp_Description := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  try lpContext.FOnError(lpContext, tmp_ErrorCode, tmp_Description)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Error', False); end;

end;


function FireKeyList(lpContext: TiphCertMgr; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_KeyContainer: String;

  tmp_KeyType: Integer;

  tmp_AlgId: String;

  tmp_KeyLen: Integer;

begin
  result := 0;
  if not Assigned(lpContext.FOnKeyList) then exit;

  tmp_KeyContainer := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[0]);
  tmp_KeyType := Integer(params^[1]);

  tmp_AlgId := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_KeyLen := Integer(params^[3]);

  try lpContext.FOnKeyList(lpContext, tmp_KeyContainer, tmp_KeyType, tmp_AlgId, tmp_KeyLen)
  except on E: Exception do result := lpContext.ReportEventException(E, 'KeyList'); end;

end;


function FireLog(lpContext: TiphCertMgr; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_LogLevel: Integer;

  tmp_Message: String;

  tmp_LogType: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnLog) then exit;

  tmp_LogLevel := Integer(params^[0]);

  tmp_Message := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_LogType := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  try lpContext.FOnLog(lpContext, tmp_LogLevel, tmp_Message, tmp_LogType)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Log'); end;

end;


function FireStoreList(lpContext: TiphCertMgr; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_CertStore: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnStoreList) then exit;

  tmp_CertStore := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[0]);
  try lpContext.FOnStoreList(lpContext, tmp_CertStore)
  except on E: Exception do result := lpContext.ReportEventException(E, 'StoreList'); end;

end;



(*** Event Sink ***)
function FireEvents(lpContext: TiphCertMgr; event_id: Integer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
var
  x: Integer;
begin
  result := 0;

  case event_id of
    EID_CertMgr_CertChain: begin result := FireCertChain(lpContext, cparam, params, cbparam); end;

    EID_CertMgr_CertList: begin result := FireCertList(lpContext, cparam, params, cbparam); end;

    EID_CertMgr_Error: begin result := FireError(lpContext, cparam, params, cbparam); end;

    EID_CertMgr_KeyList: begin result := FireKeyList(lpContext, cparam, params, cbparam); end;

    EID_CertMgr_Log: begin result := FireLog(lpContext, cparam, params, cbparam); end;

    EID_CertMgr_StoreList: begin result := FireStoreList(lpContext, cparam, params, cbparam); end;

    99999: begin x := 0; end;
  end;
end;

procedure Register;
begin
  iphCore.Register;
  RegisterComponents('IPWorks SSH', [TiphCertMgr]);
end;

{*********************************************************************************}
procedure TiphCertMgr.SetCertEncoded(lpCertEncoded: PLXAnsiChar; lenCertEncoded: Cardinal);
var
  err: integer;
begin
  if @_IPWorksSSH_CertMgr_Set = nil then exit;
  err := _IPWorksSSH_CertMgr_Set(m_ctl, PID_CertMgr_CertEncoded, 0, Pointer(lpCertEncoded), lenCertEncoded);
  if err <> 0 then TreatErr(err, '');
end;

procedure TiphCertMgr.SetCertExtensionValue(CertExtensionIndex: Integer; lpCertExtensionValue: PLXAnsiChar; lenCertExtensionValue: Cardinal);
var
  err: integer;
begin
  if @_IPWorksSSH_CertMgr_Set = nil then exit;
  err := _IPWorksSSH_CertMgr_Set(m_ctl, PID_CertMgr_CertExtensionValue, CertExtensionIndex, Pointer(lpCertExtensionValue), lenCertExtensionValue);
  if err <> 0 then TreatErr(err, '');
end;

procedure TiphCertMgr.SetCertStore(lpCertStore: PLXAnsiChar; lenCertStore: Cardinal);
var
  err: integer;
begin
  if @_IPWorksSSH_CertMgr_Set = nil then exit;
  err := _IPWorksSSH_CertMgr_Set(m_ctl, PID_CertMgr_CertStore, 0, Pointer(lpCertStore), lenCertStore);
  if err <> 0 then TreatErr(err, '');
end;

procedure TiphCertMgr.SetExportedCert(lpExportedCert: PLXAnsiChar; lenExportedCert: Cardinal);
var
  err: integer;
begin
  if @_IPWorksSSH_CertMgr_Set = nil then exit;
  err := _IPWorksSSH_CertMgr_Set(m_ctl, PID_CertMgr_ExportedCert, 0, Pointer(lpExportedCert), lenExportedCert);
  if err <> 0 then TreatErr(err, '');
end;


type
  TiphCertExtensionsCertExtensionList = class(TiphCertExtensionList)
  protected
    function OwnerCtl : TiphCertMgr;
    // Only need to override one or two methods here
    function CtlGetCount: integer; override;
    procedure CtlSetCount(Value : integer); override;
    function CreateElemInstance(Index : integer): TiphCertExtension; override;
  end;

  TiphCertExtensionsCertExtension = class(TiphCertExtension)
  protected
    function OwnerCtl : TiphCertMgr;
    function GetPropCritical: Boolean; override;

    function GetPropOID: String; override;

    function GetPropValue: String; override;

    function  GetPropValueB : TBytes; override;

  end;

function TiphCertExtensionsCertExtension.OwnerCtl : TiphCertMgr;
begin
  Result := TiphCertMgr(FOwnerCtl);
end;

function TiphCertExtensionsCertExtension.GetPropCritical: Boolean;
begin
  Result := (OwnerCtl.get_CertExtensionCritical(Index));
end;


function TiphCertExtensionsCertExtension.GetPropOID: String;
begin
  Result := (OwnerCtl.get_CertExtensionOID(Index));
end;


function TiphCertExtensionsCertExtension.GetPropValue: String;
begin
  Result := (OwnerCtl.get_CertExtensionValue(Index));
end;


function  TiphCertExtensionsCertExtension.GetPropValueB : TBytes; 
begin
  Result := OwnerCtl.get_CertExtensionValueB(Index);
end;



function TiphCertExtensionsCertExtensionList.OwnerCtl : TiphCertMgr;
begin
  Result := TiphCertMgr(FOwnerCtl);
end;

// Collection's overrides
function TiphCertExtensionsCertExtensionList.CtlGetCount: integer;
begin
  Result := OwnerCtl.get_CertExtensionCount();
end;

procedure TiphCertExtensionsCertExtensionList.CtlSetCount(Value : integer);
begin
  OwnerCtl.set_CertExtensionCount(Value);
end;

function TiphCertExtensionsCertExtensionList.CreateElemInstance(Index : integer): TiphCertExtension;
begin
  Result := TiphCertExtensionsCertExtension.Create(FOwnerCtl, false);
  Result.Index := Index;
end;


{*********************************************************************************}

constructor TiphCertMgr.Create(AOwner: TComponent);
begin
  Create(AOwner, IPWORKSSSH_OEMKEY_57);
  Config('CodePage=65001');
end;

constructor TiphCertMgr.Create(AOwner: TComponent; RuntimeLicense: string);
begin
  inherited Create(AOwner);
  m_ctl := nil;

  if @_IPWorksSSH_CertMgr_Create <> nil then
    m_ctl := _IPWorksSSH_CertMgr_Create(@FireEvents, self, nil, CREATE_OPT);
  if m_ctl = nil then
    TreatErr(-1, 'IPWorks SSH CertMgr: Error creating component');
  _IPWorksSSH_CertMgr_Set(m_ctl, 8002{PID_KEYCHECK_DLP}, 0, 
    {$ifdef ANSICHAR_IS_BYTE}
    Pointer(ToLinuxAnsiString(RuntimeLicense)),
    {$else}
    Pointer(PAnsiChar(AnsiString(RuntimeLicense))),
    {$endif}
    0);

  FCertExtensions := TiphCertExtensionsCertExtensionList.Create(Self, false);


  try set_CertExtendedKeyUsage('') except on E:Exception do end;

  try set_CertKeyPassword('') except on E:Exception do end;

  try set_CertPublicKeyAlgorithm('') except on E:Exception do end;

  try set_CertSubject('') except on E:Exception do end;

  try set_CertUsage('') except on E:Exception do end;

  try set_CertUsageFlags(0) except on E:Exception do end;

  try set_CertStore('MY') except on E:Exception do end;

  try set_CertStorePassword('') except on E:Exception do end;

  try set_CertStoreType(cstUser) except on E:Exception do end;

  try set_ExportFormat('PFX') except on E:Exception do end;

  try set_ExportPrivateKey(true) except on E:Exception do end;

end;

destructor TiphCertMgr.Destroy;
begin
  FreeAndNil(FCertExtensions);

  
  
  if m_ctl <> nil then begin
    if @_IPWorksSSH_CertMgr_Destroy <> nil then
      _IPWorksSSH_CertMgr_Destroy(m_ctl);
  end;
  m_ctl := nil;
  inherited Destroy;
end;

function TiphCertMgr.ThrowCoreException(err: Integer; const desc: {$ifndef NO_ANSI}AnsiString{$else}string{$endif}): EIPWorksSSH;
begin
  result := EiphCertMgr.CreateCode(err, desc);
end;

function TiphCertMgr.ReportEventException(e: Exception; Event: String; ReFire: Boolean): Integer;
var
  msg: String;
begin
  result := -1;
  msg := 'An unhandled error occurred in the ' + Event + ' event handler: ' + E.Message;
  if (m_ctl <> nil) and (@_IPWorksSSH_CertMgr_SetEventErrorAndCode <> nil) then begin
    if E is EIPWorksSSH then begin
      result := EIPWorksSSH(e).Code;
      msg := E.Message;
    end;
    _IPWorksSSH_CertMgr_SetEventErrorAndCode(m_ctl, result, 
      {$ifdef ANSICHAR_IS_BYTE}
      Pointer(ToLinuxAnsiString(msg)));
      {$else}
      Pointer(PAnsiChar(AnsiString(msg))));
      {$endif}
  end;
  if ReFire and Assigned(FOnError) then FOnError(self, result, msg);
end;

{*********************************************************************************}

procedure TiphCertMgr.AboutDlg;
var
  p : LPVOIDARR;
  pc: LPINTARR;
begin
{$IFDEF LINUX}
  inherited AboutDlg;
{$ELSE}
  p := nil;
  pc := nil;
  if @_IPWorksSSH_CertMgr_Do <> nil then
    _IPWorksSSH_CertMgr_Do(m_ctl, 255, 0, p, pc, nil);
{$ENDIF}
end;

{*********************************************************************************}

procedure TiphCertMgr.SetRuntimeLicense(key: String);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_CertMgr_Set = nil then exit;
  err := _IPWorksSSH_CertMgr_Set(m_ctl, 8001{PID_RUNTIME_LICENSE}, 0, 
    {$ifdef ANSICHAR_IS_BYTE}
    Pointer(ToLinuxAnsiString(key)),
    {$else}
    Pointer(PAnsiChar(AnsiString(key))),
    {$endif}
    0);

  if err <> 0 then TreatErr(err, '');
end;

function TiphCertMgr.GetRuntimeLicense: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';
  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, 8001{PID_RUNTIME_LICENSE}, 0, nil, nil);
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$ifdef ANSICHAR_IS_BYTE}
  result := FromLinuxAnsiString(PLXAnsiChar(tmp));
  {$else}
  result := PAnsiChar(tmp);
  {$endif}
end;

{*********************************************************************************}

function TiphCertMgr.HasData: Boolean;
begin
  result := false;
end;

procedure TiphCertMgr.ReadHnd(Reader: TStream);
begin
end;

procedure TiphCertMgr.WriteHnd(Writer: TStream);
begin
end;

function TiphCertMgr.BStr2CStr(pBStr: Pointer; lenBStr: Integer; bArr: TBytes): String;
{$IF DEFINED(UNICODE) AND DEFINED(MSWINDOWS)}
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
{$IFEND}
begin
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  param[0] := pBStr; paramcb[0] := lenBStr;
  param[1] := nil;   paramcb[1] := 0;
  param[2] := nil;   paramcb[2] := 0;
  _IPWorksSSH_CertMgr_Do(m_ctl, 2003, 2, @param, @paramcb, nil);
  SetLength(Result, Integer(param[2]));
  if Length(Result) > 0 then begin
    param[1] := PChar(Result); paramcb[1] := Length(Result);
    _IPWorksSSH_CertMgr_Do(m_ctl, 2003, 2, @param, @paramcb, nil);
  end;
  {$ELSE}
  try
    Result := TEncoding.Default.GetString(bArr);
  except
    on E: Exception do
      SetString(Result, PAnsiChar(pBStr), lenBStr);
   end;
  {$ENDIF}
{$ELSE}
  SetString(Result, PAnsiChar(pBStr), lenBStr);
{$ENDIF}
end;

procedure TiphCertMgr.DefineProperties(Filer: TFiler);
begin
  inherited DefineProperties(Filer);
  Filer.DefineBinaryProperty('RegHnd', ReadHnd, WriteHnd, HasData);
end;

{*********************************************************************************}

procedure TiphCertMgr.TreatErr(Err: Integer; const desc: String);
var
  msg : {$ifdef NO_ANSI}string{$else}LXAnsiString{$endif};
begin
  {$ifdef NO_ANSI}
  msg := desc;
  {$else}
  {$ifdef ANSICHAR_IS_BYTE}
  msg := ToLinuxAnsiString(desc);
  {$else}
  msg := LXAnsiString(desc);
  {$endif}
  {$endif}
  if Length(msg) = 0 then
  begin
    if @_IPWorksSSH_CertMgr_GetLastError <> nil then
      {$ifdef NO_ANSI}
      {$ifdef ANSICHAR_IS_BYTE}
      msg := FromLinuxAnsiString(_IPWorksSSH_CertMgr_GetLastError(m_ctl));
      {$else}
      msg := _IPWorksSSH_CertMgr_GetLastError(m_ctl);
      {$endif}
      {$else}
      msg := _IPWorksSSH_CertMgr_GetLastError(m_ctl);
      {$endif}
  end;

  raise ThrowCoreException(err, msg);
end;

{*********************************************************************************}

(*** Property Getters/Setters ***)
function TiphCertMgr.get_CertEffectiveDate: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertEffectiveDate{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphCertMgr.get_CertEncoded: String;
var
  tmp: {$IFDEF NONWINUNICODE}TBytes{$ELSE}Pointer; len: Cardinal{$ENDIF};
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := {$IFDEF NONWINUNICODE}get_CertEncodedB{$ELSE}_IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertEncoded{$IFDEF UNICODE_ON_UNICODE}+20000{$ENDIF}, 0, @len, nil){$ENDIF};
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$IFDEF NONWINUNICODE}result := TEncoding.UTF8.GetString(tmp);{$ELSE}SetString(result, PChar(tmp), len);{$ENDIF}
end;


procedure TiphCertMgr.set_CertEncoded(valCertEncoded: String);
var
  err: Integer;
  {$IFDEF NONWINUNICODE}tmp: TBytes;{$ENDIF}
begin
  err := 0;
  if @_IPWorksSSH_CertMgr_Set = nil then exit;
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  err := _IPWorksSSH_CertMgr_Set(m_ctl, PID_CertMgr_CertEncoded+20000, 0, Pointer(PWideChar(valCertEncoded)), Length(valCertEncoded));
  {$ELSE}
  tmp := TEncoding.UTF8.GetBytes(valCertEncoded);
  set_CertEncodedB(tmp);
  {$ENDIF}
{$ELSE}
  err := _IPWorksSSH_CertMgr_Set(m_ctl, PID_CertMgr_CertEncoded, 0, Pointer(PAnsiChar(valCertEncoded)), Length(valCertEncoded));
{$ENDIF}
  if err <> 0 then TreatErr(err, '');
end;


function TiphCertMgr.get_CertEncodedB: TBytes;
var
  tmp: Pointer;
  len: Cardinal;
begin
  result := nil;
  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertEncoded, 0, @len, nil);
  SetLength(result, len);
  Move(Pointer(tmp)^, Pointer(result)^, len); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphCertMgr.set_CertEncodedB(valCertEncoded: TBytes);
var
  err: Integer;
begin
  err := _IPWorksSSH_CertMgr_Set(m_ctl, PID_CertMgr_CertEncoded, 0, Pointer(valCertEncoded), Length(valCertEncoded));
  if err <> 0 then TreatErr(err, '');
end;


function TiphCertMgr.get_CertExpirationDate: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertExpirationDate{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphCertMgr.get_CertExtendedKeyUsage: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertExtendedKeyUsage{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphCertMgr.set_CertExtendedKeyUsage(valCertExtendedKeyUsage: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_CertMgr_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valCertExtendedKeyUsage);
  {$ENDIF}
  err := _IPWorksSSH_CertMgr_Set(m_ctl, PID_CertMgr_CertExtendedKeyUsage{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valCertExtendedKeyUsage){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphCertMgr.get_CertFingerprint: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertFingerprint{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphCertMgr.get_CertFingerprintSHA1: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertFingerprintSHA1{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphCertMgr.get_CertFingerprintSHA256: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertFingerprintSHA256{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphCertMgr.get_CertIssuer: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertIssuer{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphCertMgr.get_CertKeyPassword: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertKeyPassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphCertMgr.set_CertKeyPassword(valCertKeyPassword: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_CertMgr_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valCertKeyPassword);
  {$ENDIF}
  err := _IPWorksSSH_CertMgr_Set(m_ctl, PID_CertMgr_CertKeyPassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valCertKeyPassword){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphCertMgr.get_CertPrivateKey: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertPrivateKey{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphCertMgr.get_CertPrivateKeyAvailable: Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertPrivateKeyAvailable, 0, nil, nil);
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


function TiphCertMgr.get_CertPrivateKeyContainer: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertPrivateKeyContainer{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphCertMgr.get_CertPublicKey: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertPublicKey{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphCertMgr.get_CertPublicKeyAlgorithm: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertPublicKeyAlgorithm{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphCertMgr.set_CertPublicKeyAlgorithm(valCertPublicKeyAlgorithm: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_CertMgr_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valCertPublicKeyAlgorithm);
  {$ENDIF}
  err := _IPWorksSSH_CertMgr_Set(m_ctl, PID_CertMgr_CertPublicKeyAlgorithm{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valCertPublicKeyAlgorithm){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphCertMgr.get_CertPublicKeyLength: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertPublicKeyLength, 0, nil, nil);
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


function TiphCertMgr.get_CertSerialNumber: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertSerialNumber{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphCertMgr.get_CertSignatureAlgorithm: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertSignatureAlgorithm{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphCertMgr.get_CertSubject: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertSubject{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphCertMgr.set_CertSubject(valCertSubject: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_CertMgr_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valCertSubject);
  {$ENDIF}
  err := _IPWorksSSH_CertMgr_Set(m_ctl, PID_CertMgr_CertSubject{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valCertSubject){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphCertMgr.get_CertSubjectAltNames: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertSubjectAltNames{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphCertMgr.get_CertThumbprintMD5: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertThumbprintMD5{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphCertMgr.get_CertThumbprintSHA1: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertThumbprintSHA1{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphCertMgr.get_CertThumbprintSHA256: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertThumbprintSHA256{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphCertMgr.get_CertUsage: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertUsage{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphCertMgr.set_CertUsage(valCertUsage: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_CertMgr_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valCertUsage);
  {$ENDIF}
  err := _IPWorksSSH_CertMgr_Set(m_ctl, PID_CertMgr_CertUsage{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valCertUsage){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphCertMgr.get_CertUsageFlags: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertUsageFlags, 0, nil, nil);
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphCertMgr.set_CertUsageFlags(valCertUsageFlags: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_CertMgr_Set = nil then exit;
  err := _IPWorksSSH_CertMgr_Set(m_ctl, PID_CertMgr_CertUsageFlags, 0, Pointer(valCertUsageFlags), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphCertMgr.get_CertVersion: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertVersion{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphCertMgr.get_CertExtensionCount: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertExtensionCount, 0, nil, nil);
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphCertMgr.set_CertExtensionCount(valCertExtensionCount: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_CertMgr_Set = nil then exit;
  err := _IPWorksSSH_CertMgr_Set(m_ctl, PID_CertMgr_CertExtensionCount, 0, Pointer(valCertExtensionCount), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphCertMgr.get_CertExtensionCritical(CertExtensionIndex: Integer): Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);
  if @_IPWorksSSH_CertMgr_CheckIndex = nil then exit;
  err := _IPWorksSSH_CertMgr_CheckIndex(m_ctl, PID_CertMgr_CertExtensionCritical, CertExtensionIndex);
  if err <> 0 then TreatErr(err, 'Invalid array index value for CertExtensionCritical');

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertExtensionCritical, CertExtensionIndex, nil, nil);
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


function TiphCertMgr.get_CertExtensionOID(CertExtensionIndex: Integer): String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';
  if @_IPWorksSSH_CertMgr_CheckIndex = nil then exit;
  err := _IPWorksSSH_CertMgr_CheckIndex(m_ctl, PID_CertMgr_CertExtensionOID, CertExtensionIndex);
  if err <> 0 then TreatErr(err, 'Invalid array index value for CertExtensionOID');

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertExtensionOID{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, CertExtensionIndex, nil, nil);
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphCertMgr.get_CertExtensionValue(CertExtensionIndex: Integer): String;
var
  tmp: {$IFDEF NONWINUNICODE}TBytes{$ELSE}Pointer; len: Cardinal{$ENDIF};
  err: Integer;
begin
  result := '';
  if @_IPWorksSSH_CertMgr_CheckIndex = nil then exit;
  err := _IPWorksSSH_CertMgr_CheckIndex(m_ctl, PID_CertMgr_CertExtensionValue, CertExtensionIndex);
  if err <> 0 then TreatErr(err, 'Invalid array index value for CertExtensionValue');

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := {$IFDEF NONWINUNICODE}get_CertExtensionValueB(CertExtensionIndex){$ELSE}_IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertExtensionValue{$IFDEF UNICODE_ON_UNICODE}+20000{$ENDIF}, CertExtensionIndex, @len, nil){$ENDIF};
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$IFDEF NONWINUNICODE}result := TEncoding.UTF8.GetString(tmp);{$ELSE}SetString(result, PChar(tmp), len);{$ENDIF}
end;


function TiphCertMgr.get_CertExtensionValueB(CertExtensionIndex: Integer): TBytes;
var
  tmp: Pointer;
  len: Cardinal;
  err: Integer;
begin
  result := nil;
  if @_IPWorksSSH_CertMgr_CheckIndex = nil then exit;
  err := _IPWorksSSH_CertMgr_CheckIndex(m_ctl, PID_CertMgr_CertExtensionValue, CertExtensionIndex);
  if err <> 0 then TreatErr(err, 'Invalid array index value for CertExtensionValueB');
  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertExtensionValue, CertExtensionIndex, @len, nil);
  SetLength(result, len);
  Move(Pointer(tmp)^, Pointer(result)^, len); // IMPORTANT: Do NOT multiply the length value!
end;


function TiphCertMgr.get_CertStore: String;
var
  tmp: {$IFDEF NONWINUNICODE}TBytes{$ELSE}Pointer; len: Cardinal{$ENDIF};
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := {$IFDEF NONWINUNICODE}get_CertStoreB{$ELSE}_IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertStore{$IFDEF UNICODE_ON_UNICODE}+20000{$ENDIF}, 0, @len, nil){$ENDIF};
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$IFDEF NONWINUNICODE}result := TEncoding.UTF8.GetString(tmp);{$ELSE}SetString(result, PChar(tmp), len);{$ENDIF}
end;


procedure TiphCertMgr.set_CertStore(valCertStore: String);
var
  err: Integer;
  {$IFDEF NONWINUNICODE}tmp: TBytes;{$ENDIF}
begin
  err := 0;
  if @_IPWorksSSH_CertMgr_Set = nil then exit;
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  err := _IPWorksSSH_CertMgr_Set(m_ctl, PID_CertMgr_CertStore+20000, 0, Pointer(PWideChar(valCertStore)), Length(valCertStore));
  {$ELSE}
  tmp := TEncoding.UTF8.GetBytes(valCertStore);
  set_CertStoreB(tmp);
  {$ENDIF}
{$ELSE}
  err := _IPWorksSSH_CertMgr_Set(m_ctl, PID_CertMgr_CertStore, 0, Pointer(PAnsiChar(valCertStore)), Length(valCertStore));
{$ENDIF}
  if err <> 0 then TreatErr(err, '');
end;


function TiphCertMgr.get_CertStoreB: TBytes;
var
  tmp: Pointer;
  len: Cardinal;
begin
  result := nil;
  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertStore, 0, @len, nil);
  SetLength(result, len);
  Move(Pointer(tmp)^, Pointer(result)^, len); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphCertMgr.set_CertStoreB(valCertStore: TBytes);
var
  err: Integer;
begin
  err := _IPWorksSSH_CertMgr_Set(m_ctl, PID_CertMgr_CertStore, 0, Pointer(valCertStore), Length(valCertStore));
  if err <> 0 then TreatErr(err, '');
end;


function TiphCertMgr.get_CertStorePassword: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertStorePassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphCertMgr.set_CertStorePassword(valCertStorePassword: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_CertMgr_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valCertStorePassword);
  {$ENDIF}
  err := _IPWorksSSH_CertMgr_Set(m_ctl, PID_CertMgr_CertStorePassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valCertStorePassword){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphCertMgr.get_CertStoreType: TiphcertmgrCertStoreTypes;
var
  tmp: Pointer;
  err: Integer;
begin
  result := TiphcertmgrCertStoreTypes(0);

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_CertStoreType, 0, nil, nil);
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := TiphcertmgrCertStoreTypes(tmp);
end;


procedure TiphCertMgr.set_CertStoreType(valCertStoreType: TiphcertmgrCertStoreTypes);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_CertMgr_Set = nil then exit;
  err := _IPWorksSSH_CertMgr_Set(m_ctl, PID_CertMgr_CertStoreType, 0, Pointer(valCertStoreType), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphCertMgr.get_ExportedCert: String;
var
  tmp: {$IFDEF NONWINUNICODE}TBytes{$ELSE}Pointer; len: Cardinal{$ENDIF};
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := {$IFDEF NONWINUNICODE}get_ExportedCertB{$ELSE}_IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_ExportedCert{$IFDEF UNICODE_ON_UNICODE}+20000{$ENDIF}, 0, @len, nil){$ENDIF};
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$IFDEF NONWINUNICODE}result := TEncoding.UTF8.GetString(tmp);{$ELSE}SetString(result, PChar(tmp), len);{$ENDIF}
end;


function TiphCertMgr.get_ExportedCertB: TBytes;
var
  tmp: Pointer;
  len: Cardinal;
begin
  result := nil;
  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_ExportedCert, 0, @len, nil);
  SetLength(result, len);
  Move(Pointer(tmp)^, Pointer(result)^, len); // IMPORTANT: Do NOT multiply the length value!
end;


function TiphCertMgr.get_ExportFormat: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_ExportFormat{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphCertMgr.set_ExportFormat(valExportFormat: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_CertMgr_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valExportFormat);
  {$ENDIF}
  err := _IPWorksSSH_CertMgr_Set(m_ctl, PID_CertMgr_ExportFormat{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valExportFormat){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphCertMgr.get_ExportPrivateKey: Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);

  if @_IPWorksSSH_CertMgr_Get = nil then exit;
  tmp := _IPWorksSSH_CertMgr_Get(m_ctl, PID_CertMgr_ExportPrivateKey, 0, nil, nil);
  err := _IPWorksSSH_CertMgr_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


procedure TiphCertMgr.set_ExportPrivateKey(valExportPrivateKey: Boolean);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_CertMgr_Set = nil then exit;
  err := _IPWorksSSH_CertMgr_Set(m_ctl, PID_CertMgr_ExportPrivateKey, 0, Pointer(valExportPrivateKey), 0);
  if err <> 0 then TreatErr(err, '');
end;



{*********************************************************************************}

(*** Property Getters/Setters: OO API ***)
function  TiphCertMgr.get_CertExtensions: TiphCertExtensionList;
begin
  Result := FCertExtensions;
end;

procedure TiphCertMgr.set_CertExtensions(Value : TiphCertExtensionList);
begin
  FCertExtensions.Assign(Value);
end;


{*********************************************************************************}

{$IFNDEF DELPHI3}
(*** Methods ***)
function TiphCertMgr.Config(ConfigurationString: String): String;
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
  tmp_ConfigurationString: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := '';

  tmp_ConfigurationString := {$ifndef UNICODE_ON_ANSI}ConfigurationString{$else}ToLinuxAnsiString(ConfigurationString){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_ConfigurationString){$else}@tmp_ConfigurationString[0]{$endif};

  if @_IPWorksSSH_CertMgr_Do = nil then exit;
  err := _IPWorksSSH_CertMgr_Do(m_ctl, MID_CertMgr_Config{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  result := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(param[1]);
end;


procedure TiphCertMgr.CreateCertificate(CertSubject: String; SerialNumber: Integer);
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
  i, err: Integer;
  tmp_CertSubject: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 2 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  tmp_CertSubject := {$ifndef UNICODE_ON_ANSI}CertSubject{$else}ToLinuxAnsiString(CertSubject){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_CertSubject){$else}@tmp_CertSubject[0]{$endif};

  param[1] := Pointer(SerialNumber);


  if @_IPWorksSSH_CertMgr_Do = nil then exit;
  err := _IPWorksSSH_CertMgr_Do(m_ctl, MID_CertMgr_CreateCertificate{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 2, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphCertMgr.CreateKey(KeyName: String);
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
  tmp_KeyName: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  tmp_KeyName := {$ifndef UNICODE_ON_ANSI}KeyName{$else}ToLinuxAnsiString(KeyName){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_KeyName){$else}@tmp_KeyName[0]{$endif};

  if @_IPWorksSSH_CertMgr_Do = nil then exit;
  err := _IPWorksSSH_CertMgr_Do(m_ctl, MID_CertMgr_CreateKey{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphCertMgr.DeleteCertificate();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_CertMgr_Do = nil then exit;
  err := _IPWorksSSH_CertMgr_Do(m_ctl, MID_CertMgr_DeleteCertificate{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphCertMgr.DeleteKey(KeyName: String);
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
  tmp_KeyName: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  tmp_KeyName := {$ifndef UNICODE_ON_ANSI}KeyName{$else}ToLinuxAnsiString(KeyName){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_KeyName){$else}@tmp_KeyName[0]{$endif};

  if @_IPWorksSSH_CertMgr_Do = nil then exit;
  err := _IPWorksSSH_CertMgr_Do(m_ctl, MID_CertMgr_DeleteKey{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphCertMgr.ExportCertificate(CertFile: String; Password: String);
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
  i, err: Integer;
  tmp_CertFile: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
  tmp_Password: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 2 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  tmp_CertFile := {$ifndef UNICODE_ON_ANSI}CertFile{$else}ToLinuxAnsiString(CertFile){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_CertFile){$else}@tmp_CertFile[0]{$endif};

  tmp_Password := {$ifndef UNICODE_ON_ANSI}Password{$else}ToLinuxAnsiString(Password){$endif};
  param[1] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_Password){$else}@tmp_Password[0]{$endif};

  if @_IPWorksSSH_CertMgr_Do = nil then exit;
  err := _IPWorksSSH_CertMgr_Do(m_ctl, MID_CertMgr_ExportCertificate{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 2, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


function TiphCertMgr.GenerateCSR(CertSubject: String; KeyName: String): String;
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
  i, err: Integer;
  tmp_CertSubject: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
  tmp_KeyName: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 2 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := '';

  tmp_CertSubject := {$ifndef UNICODE_ON_ANSI}CertSubject{$else}ToLinuxAnsiString(CertSubject){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_CertSubject){$else}@tmp_CertSubject[0]{$endif};

  tmp_KeyName := {$ifndef UNICODE_ON_ANSI}KeyName{$else}ToLinuxAnsiString(KeyName){$endif};
  param[1] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_KeyName){$else}@tmp_KeyName[0]{$endif};

  if @_IPWorksSSH_CertMgr_Do = nil then exit;
  err := _IPWorksSSH_CertMgr_Do(m_ctl, MID_CertMgr_GenerateCSR{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 2, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  result := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(param[2]);
end;


procedure TiphCertMgr.ImportCertificate(CertFile: String; Password: String; Subject: String);
var
  param: array[0..3] of Pointer;
  paramcb: array[0..3] of Integer;
  i, err: Integer;
  tmp_CertFile: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
  tmp_Password: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
  tmp_Subject: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 3 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  tmp_CertFile := {$ifndef UNICODE_ON_ANSI}CertFile{$else}ToLinuxAnsiString(CertFile){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_CertFile){$else}@tmp_CertFile[0]{$endif};

  tmp_Password := {$ifndef UNICODE_ON_ANSI}Password{$else}ToLinuxAnsiString(Password){$endif};
  param[1] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_Password){$else}@tmp_Password[0]{$endif};

  tmp_Subject := {$ifndef UNICODE_ON_ANSI}Subject{$else}ToLinuxAnsiString(Subject){$endif};
  param[2] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_Subject){$else}@tmp_Subject[0]{$endif};

  if @_IPWorksSSH_CertMgr_Do = nil then exit;
  err := _IPWorksSSH_CertMgr_Do(m_ctl, MID_CertMgr_ImportCertificate{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 3, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphCertMgr.ImportSignedCSR(SignedCSR: TBytes; KeyName: String);
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
  i, err: Integer;
  tmp_KeyName: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 2 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  param[0] := Pointer(SignedCSR);
  paramcb[0] := Length(SignedCSR);

  tmp_KeyName := {$ifndef UNICODE_ON_ANSI}KeyName{$else}ToLinuxAnsiString(KeyName){$endif};
  param[1] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_KeyName){$else}@tmp_KeyName[0]{$endif};

  if @_IPWorksSSH_CertMgr_Do = nil then exit;
  err := _IPWorksSSH_CertMgr_Do(m_ctl, MID_CertMgr_ImportSignedCSR{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 2, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphCertMgr.IssueCertificate(CertSubject: String; SerialNumber: Integer);
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
  i, err: Integer;
  tmp_CertSubject: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 2 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  tmp_CertSubject := {$ifndef UNICODE_ON_ANSI}CertSubject{$else}ToLinuxAnsiString(CertSubject){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_CertSubject){$else}@tmp_CertSubject[0]{$endif};

  param[1] := Pointer(SerialNumber);


  if @_IPWorksSSH_CertMgr_Do = nil then exit;
  err := _IPWorksSSH_CertMgr_Do(m_ctl, MID_CertMgr_IssueCertificate{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 2, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


function TiphCertMgr.ListCertificateStores(): String;
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := '';

  if @_IPWorksSSH_CertMgr_Do = nil then exit;
  err := _IPWorksSSH_CertMgr_Do(m_ctl, MID_CertMgr_ListCertificateStores{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  result := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(param[0]);
end;


function TiphCertMgr.ListKeys(): String;
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := '';

  if @_IPWorksSSH_CertMgr_Do = nil then exit;
  err := _IPWorksSSH_CertMgr_Do(m_ctl, MID_CertMgr_ListKeys{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  result := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(param[0]);
end;


function TiphCertMgr.ListMachineStores(): String;
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := '';

  if @_IPWorksSSH_CertMgr_Do = nil then exit;
  err := _IPWorksSSH_CertMgr_Do(m_ctl, MID_CertMgr_ListMachineStores{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  result := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(param[0]);
end;


function TiphCertMgr.ListStoreCertificates(): String;
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := '';

  if @_IPWorksSSH_CertMgr_Do = nil then exit;
  err := _IPWorksSSH_CertMgr_Do(m_ctl, MID_CertMgr_ListStoreCertificates{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  result := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(param[0]);
end;


procedure TiphCertMgr.ReadCertificate(FileName: String);
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
  tmp_FileName: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  tmp_FileName := {$ifndef UNICODE_ON_ANSI}FileName{$else}ToLinuxAnsiString(FileName){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_FileName){$else}@tmp_FileName[0]{$endif};

  if @_IPWorksSSH_CertMgr_Do = nil then exit;
  err := _IPWorksSSH_CertMgr_Do(m_ctl, MID_CertMgr_ReadCertificate{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


function TiphCertMgr.ReadCSR(CSR: String): String;
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
  tmp_CSR: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := '';

  tmp_CSR := {$ifndef UNICODE_ON_ANSI}CSR{$else}ToLinuxAnsiString(CSR){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_CSR){$else}@tmp_CSR[0]{$endif};

  if @_IPWorksSSH_CertMgr_Do = nil then exit;
  err := _IPWorksSSH_CertMgr_Do(m_ctl, MID_CertMgr_ReadCSR{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  result := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(param[1]);
end;


procedure TiphCertMgr.Reset();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_CertMgr_Do = nil then exit;
  err := _IPWorksSSH_CertMgr_Do(m_ctl, MID_CertMgr_Reset{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphCertMgr.SaveCertificate(FileName: String);
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
  tmp_FileName: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  tmp_FileName := {$ifndef UNICODE_ON_ANSI}FileName{$else}ToLinuxAnsiString(FileName){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_FileName){$else}@tmp_FileName[0]{$endif};

  if @_IPWorksSSH_CertMgr_Do = nil then exit;
  err := _IPWorksSSH_CertMgr_Do(m_ctl, MID_CertMgr_SaveCertificate{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


function TiphCertMgr.ShowCertificateChain(): String;
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := '';

  if @_IPWorksSSH_CertMgr_Do = nil then exit;
  err := _IPWorksSSH_CertMgr_Do(m_ctl, MID_CertMgr_ShowCertificateChain{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  result := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(param[0]);
end;


function TiphCertMgr.SignCSR(CSR: TBytes; SerialNumber: Integer): String;
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 2 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := '';

  param[0] := Pointer(CSR);
  paramcb[0] := Length(CSR);

  param[1] := Pointer(SerialNumber);


  if @_IPWorksSSH_CertMgr_Do = nil then exit;
  err := _IPWorksSSH_CertMgr_Do(m_ctl, MID_CertMgr_SignCSR{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 2, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  result := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(param[2]);
end;


{$ENDIF}


{$IFDEF USE_DYNAMIC_LOADING}

procedure ClearProcs();
begin
  _IPWorksSSH_EvtStr := nil;
  _IPWorksSSH_Stream := nil;
  _IPWorksSSH_CertMgr_Create := nil;
  _IPWorksSSH_CertMgr_Destroy := nil;
  _IPWorksSSH_CertMgr_Set := nil;
  _IPWorksSSH_CertMgr_Get := nil;
  _IPWorksSSH_CertMgr_GetLastError := nil;
  _IPWorksSSH_CertMgr_GetLastErrorCode := nil;
  _IPWorksSSH_CertMgr_SetLastErrorAndCode := nil;
  _IPWorksSSH_CertMgr_GetEventError := nil;
  _IPWorksSSH_CertMgr_GetEventErrorCode := nil;
  _IPWorksSSH_CertMgr_SetEventErrorAndCode := nil;
  {$IFDEF USE_EXPLICIT_INIT}
  _IPWorksSSH_CertMgr_StaticInit := nil;
  _IPWorksSSH_CertMgr_StaticDestroy := nil;
  {$ENDIF}
  _IPWorksSSH_CertMgr_CheckIndex := nil;
  _IPWorksSSH_CertMgr_Do := nil;
end;

{$IFDEF USEIPWORKSSSHDLL}
procedure LoadDLL();
begin
  ClearProcs();

  hLib := LoadLibrary(PChar(DLLNAME));
  if hLib = 0 then exit;

  @_IPWorksSSH_EvtStr                       := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_EvtStr');
  @_IPWorksSSH_Stream                       := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_Stream');
  @_IPWorksSSH_CertMgr_Create               := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_CertMgr_Create');
  @_IPWorksSSH_CertMgr_Destroy              := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_CertMgr_Destroy');
  @_IPWorksSSH_CertMgr_Set                  := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_CertMgr_Set');
  @_IPWorksSSH_CertMgr_Get                  := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_CertMgr_Get');
  @_IPWorksSSH_CertMgr_GetLastError         := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_CertMgr_GetLastError');
  @_IPWorksSSH_CertMgr_GetLastErrorCode     := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_CertMgr_GetLastErrorCode');
  @_IPWorksSSH_CertMgr_SetLastErrorAndCode  := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_CertMgr_SetLastErrorAndCode');
  @_IPWorksSSH_CertMgr_GetEventError        := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_CertMgr_GetEventError');
  @_IPWorksSSH_CertMgr_GetEventErrorCode    := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_CertMgr_GetEventErrorCode');
  @_IPWorksSSH_CertMgr_SetEventErrorAndCode := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_CertMgr_SetEventErrorAndCode');
  {$IFDEF USE_EXPLICIT_INIT}
  @_IPWorksSSH_CertMgr_StaticInit           := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_CertMgr_StaticInit');
  @_IPWorksSSH_CertMgr_StaticDestroy        := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_CertMgr_StaticDestroy');
  {$ENDIF}
  @_IPWorksSSH_CertMgr_CheckIndex           := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_CertMgr_CheckIndex');
  @_IPWorksSSH_CertMgr_Do                   := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_CertMgr_Do');
end;
{$ELSE}
procedure LoadRESDLL();
var
  hResInfo: HRSRC;
  hResData: HGLOBAL;
  pResData: Pointer;
begin
  ClearProcs();

  hResInfo := FindResource(HInstance, 'ipworksssh22_dta', RT_RCDATA);
  if hResInfo = 0 then exit;

  hResData := LoadResource(HInstance, hResInfo);
  if hResData = 0 then exit;

  pResData := LockResource(hResData);
  if pResData = nil then exit;

  pBaseAddress := IPWorksSSHLoadDRU(pResData, pEntryPoint);
  if Assigned(pBaseAddress) then begin
    @_IPWorksSSH_EvtStr                       := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_EvtStr');
    @_IPWorksSSH_Stream                       := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_Stream');
    @_IPWorksSSH_CertMgr_Create               := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_CertMgr_Create');
    @_IPWorksSSH_CertMgr_Destroy              := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_CertMgr_Destroy');
    @_IPWorksSSH_CertMgr_Set                  := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_CertMgr_Set');
    @_IPWorksSSH_CertMgr_Get                  := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_CertMgr_Get');
    @_IPWorksSSH_CertMgr_GetLastError         := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_CertMgr_GetLastError');
    @_IPWorksSSH_CertMgr_GetLastErrorCode     := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_CertMgr_GetLastErrorCode');
    @_IPWorksSSH_CertMgr_SetLastErrorAndCode  := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_CertMgr_SetLastErrorAndCode');
    @_IPWorksSSH_CertMgr_GetEventError        := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_CertMgr_GetEventError');
    @_IPWorksSSH_CertMgr_GetEventErrorCode    := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_CertMgr_GetEventErrorCode');
    @_IPWorksSSH_CertMgr_SetEventErrorAndCode := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_CertMgr_SetEventErrorAndCode');
    @_IPWorksSSH_CertMgr_CheckIndex           := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_CertMgr_CheckIndex');
    @_IPWorksSSH_CertMgr_Do                   := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_CertMgr_Do');
  end;

  FreeResource(hResData);
end;
{$ENDIF USEIPWORKSSSHDLL}

initialization
begin
  iLoadCount := iLoadCount + 1;
  if iLoadCount > 1 then exit;

  {$IFDEF USEIPWORKSSSHDLL}
  hLib := 0;
  LoadDLL();
  {$ELSE}
  pBaseAddress := nil;
  pEntryPoint := nil;
  LoadResDLL();
  {$ENDIF}

  {$IFDEF USE_EXPLICIT_INIT}
  if @_IPWorksSSH_CertMgr_StaticInit <> nil then
    _IPWorksSSH_CertMgr_StaticInit(nil);
  {$ENDIF}
end;

finalization
begin
  iLoadCount := iLoadCount - 1;
  if iLoadCount > 0 then exit;

  {$IFDEF USE_EXPLICIT_INIT}
  if @_IPWorksSSH_CertMgr_StaticDestroy <> nil then
    _IPWorksSSH_CertMgr_StaticDestroy();
  {$ENDIF}

  ClearProcs();

  {$IFDEF USEIPWORKSSSHDLL}
  if hLib <> 0 then
  begin
    {$ifndef FPC}FreeLibrary{$else}UnloadLibrary{$endif}(hLib);
    hLib := 0;
  end;
  {$ELSE}
  IPWorksSSHFreeDRU(pBaseAddress, pEntryPoint);
  pBaseAddress := nil;
  pEntryPoint := nil;
  {$ENDIF}
  iLoadCount := 0;
end;

{$ELSE USE_DYNAMIC_LOADING}

// in case if static library used

{$IFDEF USE_EXPLICIT_INIT}

initialization
  _IPWorksSSH_CertMgr_StaticInit(nil);

finalization
  _IPWorksSSH_CertMgr_StaticDestroy();

{$ENDIF USE_EXPLICIT_INIT}

{$ENDIF USE_DYNAMIC_LOADING}


end.
