
unit iphsftpserver;



{$UNDEF USE_DYNAMIC_LOADING}
{$UNDEF USE_EXPLICIT_INIT}
{$UNDEF ANSICHAR_IS_BYTE}
{$UNDEF NO_ANSI}
{$UNDEF USE_CDECL}

{$IFDEF ENFORCE_DYNAMIC_LOADING}
  {$DEFINE USE_DYNAMIC_LOADING}
  {$DEFINE USEIPWORKSSSHDLL}
{$ENDIF}

{$IFDEF FPC}  //Lazarus
  {$IFDEF FPC_UNICODESTRINGS}
    // ensure that mode Delphi Unicode is enabled, not a modeswitch unicodestrings
    {$MODE DELPHIUNICODE}
  {$ELSE}
    {$MODE DELPHI}
  {$ENDIF}
  {$DEFINE USEIPWORKSSSHDLL}
  {$IFDEF DARWIN}
    {$DEFINE MACOS}
  {$ENDIF}
{$ENDIF}

{$IFNDEF FPC}
  {$IF CompilerVersion >= 24} // XE3+
    {$LEGACYIFEND ON}
  {$IFEND}
{$ENDIF}

{$IF DEFINED(LINUX) OR DEFINED(ANDROID) OR DEFINED(MACOS) OR DEFINED(IOS)}
  {$DEFINE USEIPWORKSSSHDLL}
  {$DEFINE ANSICHAR_IS_BYTE}
  {$DEFINE NO_ANSI}
  {$DEFINE USE_CDECL}
{$IFEND}

{$IF DEFINED(LINUX) OR DEFINED(MACOS)}
  {$IFNDEF IOS}
    {$DEFINE USE_DYNAMIC_LOADING}
  {$ENDIF}
{$IFEND}

{$IF DEFINED(ANDROID)}
  {$DEFINE USE_EXPLICIT_INIT}
{$IFEND}

{$IFDEF USE_IPWORKSSSH_ANDROID_SHARED_LIBRARY}
  {$DEFINE USE_DYNAMIC_LOADING}
{$ENDIF}


{$IF DEFINED(UNICODE) AND NOT DEFINED(MSWINDOWS)}
  {$DEFINE NONWINUNICODE}
{$IFEND}



{$IFNDEF USEIPWORKSSSHDLL}
  // in case if loading DLL from resources
  {$DEFINE USE_DYNAMIC_LOADING}
{$ENDIF}

{$UNDEF UNICODELIB}
{$IF DEFINED(MSWINDOWS)}
  {$DEFINE UNICODELIB}
{$IFEND}


{$IFDEF UNICODE}
  {$IFNDEF UNICODELIB}
    {$DEFINE UNICODE_ON_ANSI}
  {$ELSE}
    {$DEFINE UNICODE_ON_UNICODE}
  {$ENDIF}
{$ELSE}
  {$IFDEF UNICODELIB}
    {$DEFINE ANSI_ON_UNICODE}
  {$ELSE}
    {$DEFINE ANSI_ON_ANSI}
  {$ENDIF}
{$ENDIF}

interface

uses
{$IFDEF FPC}  //Lazarus
  SysUtils, Classes,
{$ELSE}
  {$IF CompilerVersion >= 23}System.SysUtils{$ELSE}SysUtils{$IFEND}, {$IF CompilerVersion >= 23}System.Classes{$ELSE}Classes{$IFEND}, 
{$ENDIF}
  {$IFDEF FPC}
  dynlibs,
  {$ELSE}
  {$IFDEF POSIX}
  Posix.Base,
  {$ENDIF}
  {$ENDIF}
  iphtypes, iphcore, iphkeys;

type

  (*** Enum Types ***)
  TiphsftpserverSSHCertStoreTypes = iphtypes.TiphCertStoreTypes;

  (*** Event Delegate Types ***)
  TConnectedEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    StatusCode: Integer;
    const Description: String;
    var CertStoreType: Integer;
    var CertStore: String;
    var CertPassword: String;
    var CertSubject: String
  ) of Object;


  TConnectionRequestEvent = procedure (
    Sender: TObject;
    const Address: String;
    Port: Integer;
    var Accept: Boolean
  ) of Object;


  TDirCreateEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    const User: String;
    const Path: String;
    FileType: Integer;
    FileSize: Int64;
    const FileOwner: String;
    const FileGroup: String;
    FilePermissions: Integer;
    FileATime: Int64;
    FileCreateTime: Int64;
    FileMTime: Int64;
    FileAttribBits: Integer;
    FileAttribBitsValid: Integer;
    const OtherAttributes: String;
    BeforeExec: Boolean;
    var StatusCode: Integer
  ) of Object;


  TDirListEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    const User: String;
    const Path: String;
    BeforeExec: Boolean;
    var StatusCode: Integer
  ) of Object;


  TDirRemoveEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    const User: String;
    const Path: String;
    BeforeExec: Boolean;
    var StatusCode: Integer
  ) of Object;


  TDisconnectedEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    StatusCode: Integer;
    const Description: String
  ) of Object;


  TErrorEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    ErrorCode: Integer;
    const Description: String
  ) of Object;


  TFileCloseEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    const User: String;
    const Path: String;
    const Handle: String;
    var StatusCode: Integer
  ) of Object;


  TFileOpenEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    const User: String;
    const Path: String;
    DesiredAccess: Integer;
    Flags: Integer;
    FileType: Integer;
    FileSize: Int64;
    const FileOwner: String;
    const FileGroup: String;
    FilePermissions: Integer;
    FileATime: Int64;
    FileCreateTime: Int64;
    FileMTime: Int64;
    FileAttribBits: Integer;
    FileAttribBitsValid: Integer;
    const OtherAttributes: String;
    var Handle: String;
    BeforeExec: Boolean;
    var StatusCode: Integer
  ) of Object;


  TFileReadEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    const User: String;
    const Handle: String;
    FileOffset: Int64;
    Length: Integer;
    var StatusCode: Integer
  ) of Object;


  TFileRemoveEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    const User: String;
    const Path: String;
    BeforeExec: Boolean;
    var StatusCode: Integer
  ) of Object;


  TFileRenameEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    const User: String;
    const Path: String;
    const NewPath: String;
    Flags: Integer;
    BeforeExec: Boolean;
    var StatusCode: Integer
  ) of Object;


  TFileWriteEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    const User: String;
    const Handle: String;
    FileOffset: Int64;
    BeforeExec: Boolean;
    var StatusCode: Integer
  ) of Object;


  TGetAttributesEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    const User: String;
    const Path: String;
    Flags: Integer;
    var FileType: Integer;
    var FileSize: Int64;
    var FileOwner: String;
    var FileGroup: String;
    var FilePermissions: Integer;
    var FileATime: Int64;
    var FileCreateTime: Int64;
    var FileMTime: Int64;
    var FileAttribBits: Integer;
    var FileAttribBitsValid: Integer;
    var OtherAttributes: String;
    var StatusCode: Integer
  ) of Object;


  TLogEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    LogLevel: Integer;
    const Message: String;
    const LogType: String
  ) of Object;


  TResolvePathEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    const User: String;
    const OriginalPath: String;
    ControlFlags: Integer;
    var RealPath: String;
    var StatusCode: Integer
  ) of Object;


  TSetAttributesEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    const User: String;
    const Path: String;
    FileType: Integer;
    FileSize: Int64;
    const FileOwner: String;
    const FileGroup: String;
    FilePermissions: Integer;
    FileATime: Int64;
    FileCreateTime: Int64;
    FileMTime: Int64;
    FileAttribBits: Integer;
    FileAttribBitsValid: Integer;
    const OtherAttributes: String;
    BeforeExec: Boolean;
    var StatusCode: Integer
  ) of Object;


  TSSHStatusEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    const Message: String
  ) of Object;


  TSSHUserAuthRequestEvent = procedure (
    Sender: TObject;
    ConnectionId: Integer;
    const User: String;
    const Service: String;
    const AuthMethod: String;
    const AuthParam: String;
    var Accept: Boolean;
    var PartialSuccess: Boolean;
    var AvailableMethods: String;
    var HomeDir: String;
    const KeyAlgorithm: String
  ) of Object;


  (*** Exception Type ***)
  EiphSFTPServer = class(EIPWorksSSH)
  end;

  (*** Component Type ***)
{$IFNDEF FPC}
  {$IF CompilerVersion >= 23}
  [ComponentPlatformsAttribute(
    pidWin32 or pidWin64
    {$IF CompilerVersion >= 29}
    or pidLinux64
    {$IFEND}
    {$IF CompilerVersion >= 31}
    or pidOSX64
    {$IFEND}
   )]
  {$IFEND}
{$ENDIF}
  TiphSFTPServer = class(TiphTypesCore)
    private
      (*** Event Handler Procedure Pointers ***)
      FOnConnected: TConnectedEvent;

      FOnConnectionRequest: TConnectionRequestEvent;

      FOnDirCreate: TDirCreateEvent;

      FOnDirList: TDirListEvent;

      FOnDirRemove: TDirRemoveEvent;

      FOnDisconnected: TDisconnectedEvent;

      FOnError: TErrorEvent;

      FOnFileClose: TFileCloseEvent;

      FOnFileOpen: TFileOpenEvent;

      FOnFileRead: TFileReadEvent;

      FOnFileRemove: TFileRemoveEvent;

      FOnFileRename: TFileRenameEvent;

      FOnFileWrite: TFileWriteEvent;

      FOnGetAttributes: TGetAttributesEvent;

      FOnLog: TLogEvent;

      FOnResolvePath: TResolvePathEvent;

      FOnSetAttributes: TSetAttributesEvent;

      FOnSSHStatus: TSSHStatusEvent;

      FOnSSHUserAuthRequest: TSSHUserAuthRequestEvent;

      m_ctl: Pointer;
      (*** Inner objects for types and collections ***)
      FConnections: TiphSFTPConnectionList;

      FKeyboardInteractivePrompts: TiphSSHPromptList;

      function HasData: Boolean;
      procedure ReadHnd(Reader: TStream);
      procedure WriteHnd(Writer: TStream);
      function BStr2CStr(pBStr: Pointer; lenBStr: Integer; bArr: TBytes): String;

      procedure SetRuntimeLicense(key: String);
      function GetRuntimeLicense: String;

    protected
      procedure AboutDlg; override;
      procedure DefineProperties(Filer: TFiler); override;

      function ThrowCoreException(Err: Integer; const Desc: {$ifndef NO_ANSI}AnsiString{$else}string{$endif}): EIPWorksSSH;
      procedure TreatErr(Err: Integer; const Desc: String);

      (*** Property Getters/Setters ***)
      function  get_ConnectionBacklog: Integer;

      procedure set_ConnectionBacklog(valConnectionBacklog: Integer);

      function  get_SFTPConnectionCount: Integer;

      function  get_SFTPConnectionConnected(ConnectionId: Integer): Boolean;

      procedure set_SFTPConnectionConnected(ConnectionId: Integer; valSFTPConnectionConnected: Boolean);

      function  get_SFTPConnectionErrorMessage(ConnectionId: Integer): String;

      function  get_SFTPConnectionFileData(ConnectionId: Integer): String;

      function  get_SFTPConnectionFileDataB(ConnectionId: Integer): TBytes;

      function  get_SFTPConnectionLocalAddress(ConnectionId: Integer): String;

      function  get_SFTPConnectionProtocolVersion(ConnectionId: Integer): Integer;

      function  get_SFTPConnectionRemoteHost(ConnectionId: Integer): String;

      function  get_SFTPConnectionRemotePort(ConnectionId: Integer): Integer;

      function  get_SFTPConnectionTimeout(ConnectionId: Integer): Integer;

      procedure set_SFTPConnectionTimeout(ConnectionId: Integer; valSFTPConnectionTimeout: Integer);

      function  get_DefaultAuthMethods: String;

      procedure set_DefaultAuthMethods(valDefaultAuthMethods: String);

      function  get_DefaultTimeout: Integer;

      procedure set_DefaultTimeout(valDefaultTimeout: Integer);

      function  get_KeyboardInteractiveMessage: String;

      procedure set_KeyboardInteractiveMessage(valKeyboardInteractiveMessage: String);

      function  get_KeyboardInteractivePromptCount: Integer;

      procedure set_KeyboardInteractivePromptCount(valKeyboardInteractivePromptCount: Integer);

      function  get_KeyboardInteractivePromptEcho(PromptIndex: Integer): Boolean;

      procedure set_KeyboardInteractivePromptEcho(PromptIndex: Integer; valKeyboardInteractivePromptEcho: Boolean);

      function  get_KeyboardInteractivePromptPrompt(PromptIndex: Integer): String;

      procedure set_KeyboardInteractivePromptPrompt(PromptIndex: Integer; valKeyboardInteractivePromptPrompt: String);

      function  get_Listening: Boolean;

      procedure set_Listening(valListening: Boolean);

      function  get_LocalHost: String;

      procedure set_LocalHost(valLocalHost: String);

      function  get_LocalPort: Integer;

      procedure set_LocalPort(valLocalPort: Integer);

      function  get_RootDirectory: String;

      procedure set_RootDirectory(valRootDirectory: String);

      function  get_SSHCertEncoded: String;

      procedure set_SSHCertEncoded(valSSHCertEncoded: String);

      function  get_SSHCertEncodedB: TBytes;

      procedure set_SSHCertEncodedB(valSSHCertEncoded: TBytes);

      function  get_SSHCertStore: String;

      procedure set_SSHCertStore(valSSHCertStore: String);

      function  get_SSHCertStoreB: TBytes;

      procedure set_SSHCertStoreB(valSSHCertStore: TBytes);

      function  get_SSHCertStorePassword: String;

      procedure set_SSHCertStorePassword(valSSHCertStorePassword: String);

      function  get_SSHCertStoreType: TiphsftpserverSSHCertStoreTypes;

      procedure set_SSHCertStoreType(valSSHCertStoreType: TiphsftpserverSSHCertStoreTypes);

      function  get_SSHCertSubject: String;

      procedure set_SSHCertSubject(valSSHCertSubject: String);

      function  get_SSHCompressionAlgorithms: String;

      procedure set_SSHCompressionAlgorithms(valSSHCompressionAlgorithms: String);

      function  get_SSHEncryptionAlgorithms: String;

      procedure set_SSHEncryptionAlgorithms(valSSHEncryptionAlgorithms: String);


      (*** Property Getters/Setters: OO API ***)
      function  get_Connections: TiphSFTPConnectionList;
      function  get_KeyboardInteractivePrompts: TiphSSHPromptList;
      procedure set_KeyboardInteractivePrompts(Value : TiphSSHPromptList);
      

    public
      constructor Create(AOwner: TComponent); overload; override;
      constructor Create(AOwner: TComponent; RuntimeLicense: string); reintroduce; overload;
      destructor Destroy; override;
      function ReportEventException(E: Exception; Event: String; ReFire: Boolean = True): Integer;

      property RuntimeLicense: String read GetRuntimeLicense write SetRuntimeLicense;

      procedure SetSFTPConnectionFileData(ConnectionId: Integer; lpSFTPConnectionFileData: PLXAnsiChar; lenSFTPConnectionFileData: Cardinal);

      procedure SetSSHCertEncoded(lpSSHCertEncoded: PLXAnsiChar; lenSSHCertEncoded: Cardinal);

      procedure SetSSHCertStore(lpSSHCertStore: PLXAnsiChar; lenSSHCertStore: Cardinal);


      (*** Runtime Property Definitions ***)

      property ConnectionBacklog: Integer read get_ConnectionBacklog write set_ConnectionBacklog;



      property SFTPConnectionCount: Integer read get_SFTPConnectionCount;



      property SFTPConnectionConnected[ConnectionId: Integer]: Boolean read get_SFTPConnectionConnected write set_SFTPConnectionConnected;



      property SFTPConnectionErrorMessage[ConnectionId: Integer]: String read get_SFTPConnectionErrorMessage;



      property SFTPConnectionFileData[ConnectionId: Integer]: String read get_SFTPConnectionFileData;



      property SFTPConnectionFileDataB[ConnectionId: Integer]: TBytes read get_SFTPConnectionFileDataB;



      property SFTPConnectionLocalAddress[ConnectionId: Integer]: String read get_SFTPConnectionLocalAddress;



      property SFTPConnectionProtocolVersion[ConnectionId: Integer]: Integer read get_SFTPConnectionProtocolVersion;



      property SFTPConnectionRemoteHost[ConnectionId: Integer]: String read get_SFTPConnectionRemoteHost;



      property SFTPConnectionRemotePort[ConnectionId: Integer]: Integer read get_SFTPConnectionRemotePort;



      property SFTPConnectionTimeout[ConnectionId: Integer]: Integer read get_SFTPConnectionTimeout write set_SFTPConnectionTimeout;



      property KeyboardInteractivePromptCount: Integer read get_KeyboardInteractivePromptCount write set_KeyboardInteractivePromptCount;



      property KeyboardInteractivePromptEcho[PromptIndex: Integer]: Boolean read get_KeyboardInteractivePromptEcho write set_KeyboardInteractivePromptEcho;



      property KeyboardInteractivePromptPrompt[PromptIndex: Integer]: String read get_KeyboardInteractivePromptPrompt write set_KeyboardInteractivePromptPrompt;



      property Listening: Boolean read get_Listening write set_Listening;



      property SSHCertEncoded: String read get_SSHCertEncoded write set_SSHCertEncoded;



      property SSHCertEncodedB: TBytes read get_SSHCertEncodedB write set_SSHCertEncodedB;



      property SSHCertStoreB: TBytes read get_SSHCertStoreB write set_SSHCertStoreB;



      (*** Runtime properties: OO API ***)
      property Connections: TiphSFTPConnectionList read get_Connections;

      property KeyboardInteractivePrompts: TiphSSHPromptList read get_KeyboardInteractivePrompts write set_KeyboardInteractivePrompts;


{$IFNDEF DELPHI3}
      (*** Method Definitions ***)
      function  Config(ConfigurationString: String): String;

      procedure Disconnect(ConnectionId: Integer);

      procedure DoEvents();

      procedure ExchangeKeys(ConnectionId: Integer);

      procedure Reset();

      procedure SetFileList(ConnectionId: Integer; List: String);

      procedure Shutdown();

      procedure StartListening();

      procedure StopListening();

{$ENDIF}

    published
      (*** Design-time Property Definitions ***)

      property DefaultAuthMethods: String read get_DefaultAuthMethods write set_DefaultAuthMethods;



      property DefaultTimeout: Integer read get_DefaultTimeout write set_DefaultTimeout default 60;



      property KeyboardInteractiveMessage: String read get_KeyboardInteractiveMessage write set_KeyboardInteractiveMessage;



      property LocalHost: String read get_LocalHost write set_LocalHost stored False;



      property LocalPort: Integer read get_LocalPort write set_LocalPort default 22;



      property RootDirectory: String read get_RootDirectory write set_RootDirectory;



      property SSHCertStore: String read get_SSHCertStore write set_SSHCertStore;



      property SSHCertStorePassword: String read get_SSHCertStorePassword write set_SSHCertStorePassword;



      property SSHCertStoreType: TiphsftpserverSSHCertStoreTypes read get_SSHCertStoreType write set_SSHCertStoreType default cstUser;



      property SSHCertSubject: String read get_SSHCertSubject write set_SSHCertSubject;



      property SSHCompressionAlgorithms: String read get_SSHCompressionAlgorithms write set_SSHCompressionAlgorithms;



      property SSHEncryptionAlgorithms: String read get_SSHEncryptionAlgorithms write set_SSHEncryptionAlgorithms;


      (*** Event Handler Bindings ***)
      property OnConnected: TConnectedEvent read FOnConnected write FOnConnected;

      property OnConnectionRequest: TConnectionRequestEvent read FOnConnectionRequest write FOnConnectionRequest;

      property OnDirCreate: TDirCreateEvent read FOnDirCreate write FOnDirCreate;

      property OnDirList: TDirListEvent read FOnDirList write FOnDirList;

      property OnDirRemove: TDirRemoveEvent read FOnDirRemove write FOnDirRemove;

      property OnDisconnected: TDisconnectedEvent read FOnDisconnected write FOnDisconnected;

      property OnError: TErrorEvent read FOnError write FOnError;

      property OnFileClose: TFileCloseEvent read FOnFileClose write FOnFileClose;

      property OnFileOpen: TFileOpenEvent read FOnFileOpen write FOnFileOpen;

      property OnFileRead: TFileReadEvent read FOnFileRead write FOnFileRead;

      property OnFileRemove: TFileRemoveEvent read FOnFileRemove write FOnFileRemove;

      property OnFileRename: TFileRenameEvent read FOnFileRename write FOnFileRename;

      property OnFileWrite: TFileWriteEvent read FOnFileWrite write FOnFileWrite;

      property OnGetAttributes: TGetAttributesEvent read FOnGetAttributes write FOnGetAttributes;

      property OnLog: TLogEvent read FOnLog write FOnLog;

      property OnResolvePath: TResolvePathEvent read FOnResolvePath write FOnResolvePath;

      property OnSetAttributes: TSetAttributesEvent read FOnSetAttributes write FOnSetAttributes;

      property OnSSHStatus: TSSHStatusEvent read FOnSSHStatus write FOnSSHStatus;

      property OnSSHUserAuthRequest: TSSHUserAuthRequestEvent read FOnSSHUserAuthRequest write FOnSSHUserAuthRequest;

    end;

    procedure Register;

implementation


{$T-}

{$IFDEF MSWINDOWS} // Both DLL and DRU loading require Windows dependency
{$IFDEF FPC}  //Lazarus
uses Messages;
{$ELSE}
uses {$IF CompilerVersion >= 23}Winapi.Windows{$ELSE}Windows{$IFEND}, {$IF CompilerVersion >= 23}Winapi.Messages{$ELSE}Messages{$IFEND};
{$ENDIF}
{$ENDIF}

const
  PID_SFTPServer_ConnectionBacklog                                  = 1;

  PID_SFTPServer_SFTPConnectionCount                                = 2;

  PID_SFTPServer_SFTPConnectionConnected                            = 3;

  PID_SFTPServer_SFTPConnectionConnectionId                         = 4;

  PID_SFTPServer_SFTPConnectionErrorMessage                         = 5;

  PID_SFTPServer_SFTPConnectionFileData                             = 6;

  PID_SFTPServer_SFTPConnectionLocalAddress                         = 7;

  PID_SFTPServer_SFTPConnectionProtocolVersion                      = 8;

  PID_SFTPServer_SFTPConnectionRemoteHost                           = 9;

  PID_SFTPServer_SFTPConnectionRemotePort                           = 10;

  PID_SFTPServer_SFTPConnectionTimeout                              = 11;

  PID_SFTPServer_DefaultAuthMethods                                 = 12;

  PID_SFTPServer_DefaultTimeout                                     = 13;

  PID_SFTPServer_KeyboardInteractiveMessage                         = 14;

  PID_SFTPServer_KeyboardInteractivePromptCount                     = 15;

  PID_SFTPServer_KeyboardInteractivePromptEcho                      = 16;

  PID_SFTPServer_KeyboardInteractivePromptPrompt                    = 17;

  PID_SFTPServer_Listening                                          = 18;

  PID_SFTPServer_LocalHost                                          = 19;

  PID_SFTPServer_LocalPort                                          = 20;

  PID_SFTPServer_RootDirectory                                      = 21;

  PID_SFTPServer_SSHCertEncoded                                     = 23;

  PID_SFTPServer_SSHCertStore                                       = 39;

  PID_SFTPServer_SSHCertStorePassword                               = 40;

  PID_SFTPServer_SSHCertStoreType                                   = 41;

  PID_SFTPServer_SSHCertSubject                                     = 42;

  PID_SFTPServer_SSHCompressionAlgorithms                           = 50;

  PID_SFTPServer_SSHEncryptionAlgorithms                            = 51;

  EID_SFTPServer_Connected = 1;

  EID_SFTPServer_ConnectionRequest = 2;

  EID_SFTPServer_DirCreate = 3;

  EID_SFTPServer_DirList = 4;

  EID_SFTPServer_DirRemove = 5;

  EID_SFTPServer_Disconnected = 6;

  EID_SFTPServer_Error = 7;

  EID_SFTPServer_FileClose = 8;

  EID_SFTPServer_FileOpen = 9;

  EID_SFTPServer_FileRead = 10;

  EID_SFTPServer_FileRemove = 11;

  EID_SFTPServer_FileRename = 12;

  EID_SFTPServer_FileWrite = 13;

  EID_SFTPServer_GetAttributes = 14;

  EID_SFTPServer_Log = 15;

  EID_SFTPServer_ResolvePath = 16;

  EID_SFTPServer_SetAttributes = 17;

  EID_SFTPServer_SSHStatus = 18;

  EID_SFTPServer_SSHUserAuthRequest = 19;

  MID_SFTPServer_Config = 2;

  MID_SFTPServer_Disconnect = 3;

  MID_SFTPServer_DoEvents = 4;

  MID_SFTPServer_ExchangeKeys = 5;

  MID_SFTPServer_Reset = 6;

  MID_SFTPServer_SetFileList = 7;

  MID_SFTPServer_Shutdown = 8;

  MID_SFTPServer_StartListening = 9;

  MID_SFTPServer_StopListening = 10;

  CREATE_OPT = {$IFDEF UNICODE}1{$ELSE}0{$ENDIF};
  EVTSTR_OPT = {$IFDEF UNICODE}2{$ELSE}1{$ENDIF};

  {$IFDEF MSWINDOWS}
  DLLNAME = 'IPWORKSSSH22.DLL';
  {$ENDIF}
  {$IFDEF LINUX}
  DLLNAME = 'libipworksssh.so.22.0';
  {$ENDIF}
  {$IFDEF ANDROID}
  {$IFDEF USE_DYNAMIC_LOADING}
  DLLNAME = 'libipworksssh.so.22.0';
  {$ELSE}
  DLLNAME = 'libipworksssh.22.0.a';
  {$ENDIF}
  {$ENDIF}
  {$IFDEF IOS}
  DLLNAME = 'libipworksssh.22.0.a';
  {$ELSE}
  {$IFDEF MACOS}
  DLLNAME = '@rpath/libipworksssh.22.0.dylib';
  {$ENDIF}
  {$ENDIF}

{$WARNINGS OFF}

type
  INTARR = array[0..32] of Integer;
  LPINTARR = ^INTARR;
  LPINT = ^Cardinal;
  MEventHandle = function(lpContext: TiphSFTPServer; event_id: Integer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  PEventHandle = ^MEventHandle;


{$IFNDEF USEIPWORKSSSHDLL}
var
  pEntryPoint: Pointer;
  pBaseAddress: Pointer;
  iLoadCount: Integer;
{$ELSE}
{$IFDEF USE_DYNAMIC_LOADING}
var
  hLib : {$ifndef FPC}HMODULE{$else}TLibHandle{$endif};
  iLoadCount: Integer;
{$ENDIF USE_DYNAMIC_LOADING}
{$ENDIF USEIPWORKSSSHDLL}


{$IFDEF USE_DYNAMIC_LOADING}
  _IPWorksSSH_EvtStr:                           function(pEvtStr: Pointer; id: Integer; value: Pointer; opt: Integer): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_Stream:                           function(pStream: Pointer; op: Integer; params: LPVOIDARR; retVal: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SFTPServer_Create:                function(pMethod: PEventHandle; pObject: TiphSFTPServer; pKey: Pointer; opts: Integer): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SFTPServer_Destroy:               function(p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SFTPServer_Set:                   function(p: Pointer; index: Integer; arridx: Integer; value: Pointer; len: Cardinal): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SFTPServer_Get:                   function(p: Pointer; index: Integer; arridx: Integer; len: LPINT; llVal: PInt64): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SFTPServer_GetLastError:          function(p: Pointer): PLXAnsiChar; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SFTPServer_GetLastErrorCode:      function(p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SFTPServer_SetLastErrorAndCode:   function(p: Pointer; code: Integer; message: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SFTPServer_GetEventError:         function(p: Pointer): PLXAnsiChar; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SFTPServer_GetEventErrorCode:     function(p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SFTPServer_SetEventErrorAndCode:  function(p: Pointer; code: Integer; message: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  {$IFDEF USE_EXPLICIT_INIT}
  _IPWorksSSH_SFTPServer_StaticInit:            function(instance: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SFTPServer_StaticDestroy:         function(): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  {$ENDIF}
  _IPWorksSSH_SFTPServer_CheckIndex:            function(p: Pointer; index: Integer; arridx: Integer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SFTPServer_Do:                    function(p: Pointer; method_id: Integer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR; llVal: PInt64): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
{$ELSE}
  function _IPWorksSSH_EvtStr                          (pEvtStr: Pointer; id: Integer; value: Pointer; opt: Integer): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_EvtStr' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_Stream                          (pStream: Pointer; op: Integer; params: LPVOIDARR; retVal: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_Stream' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SFTPServer_Create               (pMethod: PEventHandle; pObject: TiphSFTPServer; pKey: Pointer; opts: Integer): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SFTPServer_Create' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SFTPServer_Destroy              (p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SFTPServer_Destroy' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SFTPServer_Set                  (p: Pointer; index: Integer; arridx: Integer; value: Pointer; len: Cardinal): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SFTPServer_Set' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SFTPServer_Get                  (p: Pointer; index: Integer; arridx: Integer; len: LPINT; llVal: PInt64): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SFTPServer_Get' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SFTPServer_GetLastError         (p: Pointer): PLXAnsiChar; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SFTPServer_GetLastError' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SFTPServer_GetLastErrorCode     (p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SFTPServer_GetLastErrorCode' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SFTPServer_SetLastErrorAndCode  (p: Pointer; code: Integer; message: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SFTPServer_SetLastErrorAndCode' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SFTPServer_GetEventError        (p: Pointer): PLXAnsiChar; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SFTPServer_GetEventError' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SFTPServer_GetEventErrorCode    (p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SFTPServer_GetEventErrorCode' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SFTPServer_SetEventErrorAndCode (p: Pointer; code: Integer; message: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SFTPServer_SetEventErrorAndCode' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SFTPServer_StaticInit           (instance: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SFTPServer_StaticInit' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SFTPServer_StaticDestroy        (): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SFTPServer_StaticDestroy' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SFTPServer_CheckIndex           (p: Pointer; index: Integer; arridx: Integer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SFTPServer_CheckIndex' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SFTPServer_Do                   (p: Pointer; method_id: Integer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR; llVal: PInt64): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SFTPServer_Do' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
{$ENDIF}

{$HINTS OFF}

(*** Fire Event Methods ***)
function FireConnected(lpContext: TiphSFTPServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_StatusCode: Integer;

  tmp_Description: String;

  tmp_CertStoreType: Integer;

  tmp_CertStore: String; // dev platform string
  {$ifdef UNICODE_ON_ANSI}
  tmp_CertStoreB: LXAnsiString; // library string
  {$endif}

  tmp_CertPassword: String; // dev platform string
  {$ifdef UNICODE_ON_ANSI}
  tmp_CertPasswordB: LXAnsiString; // library string
  {$endif}

  tmp_CertSubject: String; // dev platform string
  {$ifdef UNICODE_ON_ANSI}
  tmp_CertSubjectB: LXAnsiString; // library string
  {$endif}

begin
  result := 0;
  if not Assigned(lpContext.FOnConnected) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_StatusCode := Integer(params^[1]);

  tmp_Description := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_CertStoreType := Integer(params^[3]);

  tmp_CertStore := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(_IPWorksSSH_EvtStr(params^[4], 0, nil, EVTSTR_OPT));
  tmp_CertPassword := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(_IPWorksSSH_EvtStr(params^[5], 0, nil, EVTSTR_OPT));
  tmp_CertSubject := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(_IPWorksSSH_EvtStr(params^[6], 0, nil, EVTSTR_OPT));
  try lpContext.FOnConnected(lpContext, tmp_ConnectionId, tmp_StatusCode, tmp_Description, tmp_CertStoreType, tmp_CertStore, tmp_CertPassword, tmp_CertSubject)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Connected'); end;

  params^[3] := Pointer(tmp_CertStoreType);

  {$ifdef UNICODE_ON_ANSI}
  tmp_CertStoreB := ToLinuxAnsiString(tmp_CertStore);
  {$endif}
  _IPWorksSSH_EvtStr(params^[4], 2, {$ifdef UNICODE_ON_ANSI}@tmp_CertStoreB[0]{$else}PChar(tmp_CertStore){$endif}, EVTSTR_OPT);
  {$ifdef UNICODE_ON_ANSI}
  tmp_CertPasswordB := ToLinuxAnsiString(tmp_CertPassword);
  {$endif}
  _IPWorksSSH_EvtStr(params^[5], 2, {$ifdef UNICODE_ON_ANSI}@tmp_CertPasswordB[0]{$else}PChar(tmp_CertPassword){$endif}, EVTSTR_OPT);
  {$ifdef UNICODE_ON_ANSI}
  tmp_CertSubjectB := ToLinuxAnsiString(tmp_CertSubject);
  {$endif}
  _IPWorksSSH_EvtStr(params^[6], 2, {$ifdef UNICODE_ON_ANSI}@tmp_CertSubjectB[0]{$else}PChar(tmp_CertSubject){$endif}, EVTSTR_OPT);
end;


function FireConnectionRequest(lpContext: TiphSFTPServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_Address: String;

  tmp_Port: Integer;

  tmp_Accept: Boolean;

begin
  result := 0;
  if not Assigned(lpContext.FOnConnectionRequest) then exit;

  tmp_Address := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[0]);
  tmp_Port := Integer(params^[1]);

  tmp_Accept := Boolean(params^[2]);

  try lpContext.FOnConnectionRequest(lpContext, tmp_Address, tmp_Port, tmp_Accept)
  except on E: Exception do result := lpContext.ReportEventException(E, 'ConnectionRequest'); end;

  params^[2] := Pointer(tmp_Accept);

end;


function FireDirCreate(lpContext: TiphSFTPServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_User: String;

  tmp_Path: String;

  tmp_FileType: Integer;

  tmp_FileSize: Int64;

  tmp_FileOwner: String;

  tmp_FileGroup: String;

  tmp_FilePermissions: Integer;

  tmp_FileATime: Int64;

  tmp_FileCreateTime: Int64;

  tmp_FileMTime: Int64;

  tmp_FileAttribBits: Integer;

  tmp_FileAttribBitsValid: Integer;

  tmp_OtherAttributes: String;

  tmp_BeforeExec: Boolean;

  tmp_StatusCode: Integer;

begin
  result := 0;
  if not Assigned(lpContext.FOnDirCreate) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_User := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_Path := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_FileType := Integer(params^[3]);

  tmp_FileSize := PInt64(params^[4])^;
  tmp_FileOwner := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[5]);
  tmp_FileGroup := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[6]);
  tmp_FilePermissions := Integer(params^[7]);

  tmp_FileATime := PInt64(params^[8])^;
  tmp_FileCreateTime := PInt64(params^[9])^;
  tmp_FileMTime := PInt64(params^[10])^;
  tmp_FileAttribBits := Integer(params^[11]);

  tmp_FileAttribBitsValid := Integer(params^[12]);

  tmp_OtherAttributes := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[13]);
  tmp_BeforeExec := Boolean(params^[14]);

  tmp_StatusCode := Integer(params^[15]);

  try lpContext.FOnDirCreate(lpContext, tmp_ConnectionId, tmp_User, tmp_Path, tmp_FileType, tmp_FileSize, tmp_FileOwner, tmp_FileGroup, tmp_FilePermissions, tmp_FileATime, tmp_FileCreateTime, tmp_FileMTime, tmp_FileAttribBits, tmp_FileAttribBitsValid, tmp_OtherAttributes, tmp_BeforeExec, tmp_StatusCode)
  except on E: Exception do result := lpContext.ReportEventException(E, 'DirCreate'); end;

  params^[15] := Pointer(tmp_StatusCode);

end;


function FireDirList(lpContext: TiphSFTPServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_User: String;

  tmp_Path: String;

  tmp_BeforeExec: Boolean;

  tmp_StatusCode: Integer;

begin
  result := 0;
  if not Assigned(lpContext.FOnDirList) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_User := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_Path := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_BeforeExec := Boolean(params^[3]);

  tmp_StatusCode := Integer(params^[4]);

  try lpContext.FOnDirList(lpContext, tmp_ConnectionId, tmp_User, tmp_Path, tmp_BeforeExec, tmp_StatusCode)
  except on E: Exception do result := lpContext.ReportEventException(E, 'DirList'); end;

  params^[4] := Pointer(tmp_StatusCode);

end;


function FireDirRemove(lpContext: TiphSFTPServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_User: String;

  tmp_Path: String;

  tmp_BeforeExec: Boolean;

  tmp_StatusCode: Integer;

begin
  result := 0;
  if not Assigned(lpContext.FOnDirRemove) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_User := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_Path := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_BeforeExec := Boolean(params^[3]);

  tmp_StatusCode := Integer(params^[4]);

  try lpContext.FOnDirRemove(lpContext, tmp_ConnectionId, tmp_User, tmp_Path, tmp_BeforeExec, tmp_StatusCode)
  except on E: Exception do result := lpContext.ReportEventException(E, 'DirRemove'); end;

  params^[4] := Pointer(tmp_StatusCode);

end;


function FireDisconnected(lpContext: TiphSFTPServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_StatusCode: Integer;

  tmp_Description: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnDisconnected) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_StatusCode := Integer(params^[1]);

  tmp_Description := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  try lpContext.FOnDisconnected(lpContext, tmp_ConnectionId, tmp_StatusCode, tmp_Description)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Disconnected'); end;

end;


function FireError(lpContext: TiphSFTPServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_ErrorCode: Integer;

  tmp_Description: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnError) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_ErrorCode := Integer(params^[1]);

  tmp_Description := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  try lpContext.FOnError(lpContext, tmp_ConnectionId, tmp_ErrorCode, tmp_Description)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Error', False); end;

end;


function FireFileClose(lpContext: TiphSFTPServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_User: String;

  tmp_Path: String;

  tmp_Handle: String;

  tmp_StatusCode: Integer;

begin
  result := 0;
  if not Assigned(lpContext.FOnFileClose) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_User := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_Path := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_Handle := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[3]);
  tmp_StatusCode := Integer(params^[4]);

  try lpContext.FOnFileClose(lpContext, tmp_ConnectionId, tmp_User, tmp_Path, tmp_Handle, tmp_StatusCode)
  except on E: Exception do result := lpContext.ReportEventException(E, 'FileClose'); end;

  params^[4] := Pointer(tmp_StatusCode);

end;


function FireFileOpen(lpContext: TiphSFTPServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_User: String;

  tmp_Path: String;

  tmp_DesiredAccess: Integer;

  tmp_Flags: Integer;

  tmp_FileType: Integer;

  tmp_FileSize: Int64;

  tmp_FileOwner: String;

  tmp_FileGroup: String;

  tmp_FilePermissions: Integer;

  tmp_FileATime: Int64;

  tmp_FileCreateTime: Int64;

  tmp_FileMTime: Int64;

  tmp_FileAttribBits: Integer;

  tmp_FileAttribBitsValid: Integer;

  tmp_OtherAttributes: String;

  tmp_Handle: String; // dev platform string
  {$ifdef UNICODE_ON_ANSI}
  tmp_HandleB: LXAnsiString; // library string
  {$endif}

  tmp_BeforeExec: Boolean;

  tmp_StatusCode: Integer;

begin
  result := 0;
  if not Assigned(lpContext.FOnFileOpen) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_User := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_Path := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_DesiredAccess := Integer(params^[3]);

  tmp_Flags := Integer(params^[4]);

  tmp_FileType := Integer(params^[5]);

  tmp_FileSize := PInt64(params^[6])^;
  tmp_FileOwner := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[7]);
  tmp_FileGroup := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[8]);
  tmp_FilePermissions := Integer(params^[9]);

  tmp_FileATime := PInt64(params^[10])^;
  tmp_FileCreateTime := PInt64(params^[11])^;
  tmp_FileMTime := PInt64(params^[12])^;
  tmp_FileAttribBits := Integer(params^[13]);

  tmp_FileAttribBitsValid := Integer(params^[14]);

  tmp_OtherAttributes := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[15]);
  tmp_Handle := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(_IPWorksSSH_EvtStr(params^[16], 0, nil, EVTSTR_OPT));
  tmp_BeforeExec := Boolean(params^[17]);

  tmp_StatusCode := Integer(params^[18]);

  try lpContext.FOnFileOpen(lpContext, tmp_ConnectionId, tmp_User, tmp_Path, tmp_DesiredAccess, tmp_Flags, tmp_FileType, tmp_FileSize, tmp_FileOwner, tmp_FileGroup, tmp_FilePermissions, tmp_FileATime, tmp_FileCreateTime, tmp_FileMTime, tmp_FileAttribBits, tmp_FileAttribBitsValid, tmp_OtherAttributes, tmp_Handle, tmp_BeforeExec, tmp_StatusCode)
  except on E: Exception do result := lpContext.ReportEventException(E, 'FileOpen'); end;

  {$ifdef UNICODE_ON_ANSI}
  tmp_HandleB := ToLinuxAnsiString(tmp_Handle);
  {$endif}
  _IPWorksSSH_EvtStr(params^[16], 2, {$ifdef UNICODE_ON_ANSI}@tmp_HandleB[0]{$else}PChar(tmp_Handle){$endif}, EVTSTR_OPT);
  params^[18] := Pointer(tmp_StatusCode);

end;


function FireFileRead(lpContext: TiphSFTPServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_User: String;

  tmp_Handle: String;

  tmp_FileOffset: Int64;

  tmp_Length: Integer;

  tmp_StatusCode: Integer;

begin
  result := 0;
  if not Assigned(lpContext.FOnFileRead) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_User := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_Handle := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_FileOffset := PInt64(params^[3])^;
  tmp_Length := Integer(params^[4]);

  tmp_StatusCode := Integer(params^[5]);

  try lpContext.FOnFileRead(lpContext, tmp_ConnectionId, tmp_User, tmp_Handle, tmp_FileOffset, tmp_Length, tmp_StatusCode)
  except on E: Exception do result := lpContext.ReportEventException(E, 'FileRead'); end;

  params^[5] := Pointer(tmp_StatusCode);

end;


function FireFileRemove(lpContext: TiphSFTPServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_User: String;

  tmp_Path: String;

  tmp_BeforeExec: Boolean;

  tmp_StatusCode: Integer;

begin
  result := 0;
  if not Assigned(lpContext.FOnFileRemove) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_User := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_Path := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_BeforeExec := Boolean(params^[3]);

  tmp_StatusCode := Integer(params^[4]);

  try lpContext.FOnFileRemove(lpContext, tmp_ConnectionId, tmp_User, tmp_Path, tmp_BeforeExec, tmp_StatusCode)
  except on E: Exception do result := lpContext.ReportEventException(E, 'FileRemove'); end;

  params^[4] := Pointer(tmp_StatusCode);

end;


function FireFileRename(lpContext: TiphSFTPServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_User: String;

  tmp_Path: String;

  tmp_NewPath: String;

  tmp_Flags: Integer;

  tmp_BeforeExec: Boolean;

  tmp_StatusCode: Integer;

begin
  result := 0;
  if not Assigned(lpContext.FOnFileRename) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_User := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_Path := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_NewPath := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[3]);
  tmp_Flags := Integer(params^[4]);

  tmp_BeforeExec := Boolean(params^[5]);

  tmp_StatusCode := Integer(params^[6]);

  try lpContext.FOnFileRename(lpContext, tmp_ConnectionId, tmp_User, tmp_Path, tmp_NewPath, tmp_Flags, tmp_BeforeExec, tmp_StatusCode)
  except on E: Exception do result := lpContext.ReportEventException(E, 'FileRename'); end;

  params^[6] := Pointer(tmp_StatusCode);

end;


function FireFileWrite(lpContext: TiphSFTPServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_User: String;

  tmp_Handle: String;

  tmp_FileOffset: Int64;

  tmp_BeforeExec: Boolean;

  tmp_StatusCode: Integer;

begin
  result := 0;
  if not Assigned(lpContext.FOnFileWrite) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_User := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_Handle := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_FileOffset := PInt64(params^[3])^;
  tmp_BeforeExec := Boolean(params^[4]);

  tmp_StatusCode := Integer(params^[5]);

  try lpContext.FOnFileWrite(lpContext, tmp_ConnectionId, tmp_User, tmp_Handle, tmp_FileOffset, tmp_BeforeExec, tmp_StatusCode)
  except on E: Exception do result := lpContext.ReportEventException(E, 'FileWrite'); end;

  params^[5] := Pointer(tmp_StatusCode);

end;


function FireGetAttributes(lpContext: TiphSFTPServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_User: String;

  tmp_Path: String;

  tmp_Flags: Integer;

  tmp_FileType: Integer;

  tmp_FileSize: Int64;

  tmp_FileOwner: String; // dev platform string
  {$ifdef UNICODE_ON_ANSI}
  tmp_FileOwnerB: LXAnsiString; // library string
  {$endif}

  tmp_FileGroup: String; // dev platform string
  {$ifdef UNICODE_ON_ANSI}
  tmp_FileGroupB: LXAnsiString; // library string
  {$endif}

  tmp_FilePermissions: Integer;

  tmp_FileATime: Int64;

  tmp_FileCreateTime: Int64;

  tmp_FileMTime: Int64;

  tmp_FileAttribBits: Integer;

  tmp_FileAttribBitsValid: Integer;

  tmp_OtherAttributes: String; // dev platform string
  {$ifdef UNICODE_ON_ANSI}
  tmp_OtherAttributesB: LXAnsiString; // library string
  {$endif}

  tmp_StatusCode: Integer;

begin
  result := 0;
  if not Assigned(lpContext.FOnGetAttributes) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_User := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_Path := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_Flags := Integer(params^[3]);

  tmp_FileType := Integer(params^[4]);

  tmp_FileSize := PInt64(params^[5])^;
  tmp_FileOwner := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(_IPWorksSSH_EvtStr(params^[6], 0, nil, EVTSTR_OPT));
  tmp_FileGroup := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(_IPWorksSSH_EvtStr(params^[7], 0, nil, EVTSTR_OPT));
  tmp_FilePermissions := Integer(params^[8]);

  tmp_FileATime := PInt64(params^[9])^;
  tmp_FileCreateTime := PInt64(params^[10])^;
  tmp_FileMTime := PInt64(params^[11])^;
  tmp_FileAttribBits := Integer(params^[12]);

  tmp_FileAttribBitsValid := Integer(params^[13]);

  tmp_OtherAttributes := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(_IPWorksSSH_EvtStr(params^[14], 0, nil, EVTSTR_OPT));
  tmp_StatusCode := Integer(params^[15]);

  try lpContext.FOnGetAttributes(lpContext, tmp_ConnectionId, tmp_User, tmp_Path, tmp_Flags, tmp_FileType, tmp_FileSize, tmp_FileOwner, tmp_FileGroup, tmp_FilePermissions, tmp_FileATime, tmp_FileCreateTime, tmp_FileMTime, tmp_FileAttribBits, tmp_FileAttribBitsValid, tmp_OtherAttributes, tmp_StatusCode)
  except on E: Exception do result := lpContext.ReportEventException(E, 'GetAttributes'); end;

  params^[4] := Pointer(tmp_FileType);

  PInt64(params^[5])^ := tmp_FileSize;
  {$ifdef UNICODE_ON_ANSI}
  tmp_FileOwnerB := ToLinuxAnsiString(tmp_FileOwner);
  {$endif}
  _IPWorksSSH_EvtStr(params^[6], 2, {$ifdef UNICODE_ON_ANSI}@tmp_FileOwnerB[0]{$else}PChar(tmp_FileOwner){$endif}, EVTSTR_OPT);
  {$ifdef UNICODE_ON_ANSI}
  tmp_FileGroupB := ToLinuxAnsiString(tmp_FileGroup);
  {$endif}
  _IPWorksSSH_EvtStr(params^[7], 2, {$ifdef UNICODE_ON_ANSI}@tmp_FileGroupB[0]{$else}PChar(tmp_FileGroup){$endif}, EVTSTR_OPT);
  params^[8] := Pointer(tmp_FilePermissions);

  PInt64(params^[9])^ := tmp_FileATime;
  PInt64(params^[10])^ := tmp_FileCreateTime;
  PInt64(params^[11])^ := tmp_FileMTime;
  params^[12] := Pointer(tmp_FileAttribBits);

  params^[13] := Pointer(tmp_FileAttribBitsValid);

  {$ifdef UNICODE_ON_ANSI}
  tmp_OtherAttributesB := ToLinuxAnsiString(tmp_OtherAttributes);
  {$endif}
  _IPWorksSSH_EvtStr(params^[14], 2, {$ifdef UNICODE_ON_ANSI}@tmp_OtherAttributesB[0]{$else}PChar(tmp_OtherAttributes){$endif}, EVTSTR_OPT);
  params^[15] := Pointer(tmp_StatusCode);

end;


function FireLog(lpContext: TiphSFTPServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_LogLevel: Integer;

  tmp_Message: String;

  tmp_LogType: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnLog) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_LogLevel := Integer(params^[1]);

  tmp_Message := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_LogType := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[3]);
  try lpContext.FOnLog(lpContext, tmp_ConnectionId, tmp_LogLevel, tmp_Message, tmp_LogType)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Log'); end;

end;


function FireResolvePath(lpContext: TiphSFTPServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_User: String;

  tmp_OriginalPath: String;

  tmp_ControlFlags: Integer;

  tmp_RealPath: String; // dev platform string
  {$ifdef UNICODE_ON_ANSI}
  tmp_RealPathB: LXAnsiString; // library string
  {$endif}

  tmp_StatusCode: Integer;

begin
  result := 0;
  if not Assigned(lpContext.FOnResolvePath) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_User := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_OriginalPath := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_ControlFlags := Integer(params^[3]);

  tmp_RealPath := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(_IPWorksSSH_EvtStr(params^[4], 0, nil, EVTSTR_OPT));
  tmp_StatusCode := Integer(params^[5]);

  try lpContext.FOnResolvePath(lpContext, tmp_ConnectionId, tmp_User, tmp_OriginalPath, tmp_ControlFlags, tmp_RealPath, tmp_StatusCode)
  except on E: Exception do result := lpContext.ReportEventException(E, 'ResolvePath'); end;

  {$ifdef UNICODE_ON_ANSI}
  tmp_RealPathB := ToLinuxAnsiString(tmp_RealPath);
  {$endif}
  _IPWorksSSH_EvtStr(params^[4], 2, {$ifdef UNICODE_ON_ANSI}@tmp_RealPathB[0]{$else}PChar(tmp_RealPath){$endif}, EVTSTR_OPT);
  params^[5] := Pointer(tmp_StatusCode);

end;


function FireSetAttributes(lpContext: TiphSFTPServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_User: String;

  tmp_Path: String;

  tmp_FileType: Integer;

  tmp_FileSize: Int64;

  tmp_FileOwner: String;

  tmp_FileGroup: String;

  tmp_FilePermissions: Integer;

  tmp_FileATime: Int64;

  tmp_FileCreateTime: Int64;

  tmp_FileMTime: Int64;

  tmp_FileAttribBits: Integer;

  tmp_FileAttribBitsValid: Integer;

  tmp_OtherAttributes: String;

  tmp_BeforeExec: Boolean;

  tmp_StatusCode: Integer;

begin
  result := 0;
  if not Assigned(lpContext.FOnSetAttributes) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_User := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_Path := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_FileType := Integer(params^[3]);

  tmp_FileSize := PInt64(params^[4])^;
  tmp_FileOwner := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[5]);
  tmp_FileGroup := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[6]);
  tmp_FilePermissions := Integer(params^[7]);

  tmp_FileATime := PInt64(params^[8])^;
  tmp_FileCreateTime := PInt64(params^[9])^;
  tmp_FileMTime := PInt64(params^[10])^;
  tmp_FileAttribBits := Integer(params^[11]);

  tmp_FileAttribBitsValid := Integer(params^[12]);

  tmp_OtherAttributes := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[13]);
  tmp_BeforeExec := Boolean(params^[14]);

  tmp_StatusCode := Integer(params^[15]);

  try lpContext.FOnSetAttributes(lpContext, tmp_ConnectionId, tmp_User, tmp_Path, tmp_FileType, tmp_FileSize, tmp_FileOwner, tmp_FileGroup, tmp_FilePermissions, tmp_FileATime, tmp_FileCreateTime, tmp_FileMTime, tmp_FileAttribBits, tmp_FileAttribBitsValid, tmp_OtherAttributes, tmp_BeforeExec, tmp_StatusCode)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SetAttributes'); end;

  params^[15] := Pointer(tmp_StatusCode);

end;


function FireSSHStatus(lpContext: TiphSFTPServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_Message: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHStatus) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_Message := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  try lpContext.FOnSSHStatus(lpContext, tmp_ConnectionId, tmp_Message)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHStatus'); end;

end;


function FireSSHUserAuthRequest(lpContext: TiphSFTPServer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionId: Integer;

  tmp_User: String;

  tmp_Service: String;

  tmp_AuthMethod: String;

  tmp_AuthParam: String;

  tmp_Accept: Boolean;

  tmp_PartialSuccess: Boolean;

  tmp_AvailableMethods: String; // dev platform string
  {$ifdef UNICODE_ON_ANSI}
  tmp_AvailableMethodsB: LXAnsiString; // library string
  {$endif}

  tmp_HomeDir: String; // dev platform string
  {$ifdef UNICODE_ON_ANSI}
  tmp_HomeDirB: LXAnsiString; // library string
  {$endif}

  tmp_KeyAlgorithm: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHUserAuthRequest) then exit;

  tmp_ConnectionId := Integer(params^[0]);

  tmp_User := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_Service := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_AuthMethod := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[3]);
  tmp_AuthParam := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[4]);
  tmp_Accept := Boolean(params^[5]);

  tmp_PartialSuccess := Boolean(params^[6]);

  tmp_AvailableMethods := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(_IPWorksSSH_EvtStr(params^[7], 0, nil, EVTSTR_OPT));
  tmp_HomeDir := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(_IPWorksSSH_EvtStr(params^[8], 0, nil, EVTSTR_OPT));
  tmp_KeyAlgorithm := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[9]);
  try lpContext.FOnSSHUserAuthRequest(lpContext, tmp_ConnectionId, tmp_User, tmp_Service, tmp_AuthMethod, tmp_AuthParam, tmp_Accept, tmp_PartialSuccess, tmp_AvailableMethods, tmp_HomeDir, tmp_KeyAlgorithm)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHUserAuthRequest'); end;

  params^[5] := Pointer(tmp_Accept);

  params^[6] := Pointer(tmp_PartialSuccess);

  {$ifdef UNICODE_ON_ANSI}
  tmp_AvailableMethodsB := ToLinuxAnsiString(tmp_AvailableMethods);
  {$endif}
  _IPWorksSSH_EvtStr(params^[7], 2, {$ifdef UNICODE_ON_ANSI}@tmp_AvailableMethodsB[0]{$else}PChar(tmp_AvailableMethods){$endif}, EVTSTR_OPT);
  {$ifdef UNICODE_ON_ANSI}
  tmp_HomeDirB := ToLinuxAnsiString(tmp_HomeDir);
  {$endif}
  _IPWorksSSH_EvtStr(params^[8], 2, {$ifdef UNICODE_ON_ANSI}@tmp_HomeDirB[0]{$else}PChar(tmp_HomeDir){$endif}, EVTSTR_OPT);
end;



(*** Event Sink ***)
function FireEvents(lpContext: TiphSFTPServer; event_id: Integer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
var
  x: Integer;
begin
  result := 0;

  case event_id of
    EID_SFTPServer_Connected: begin result := FireConnected(lpContext, cparam, params, cbparam); end;

    EID_SFTPServer_ConnectionRequest: begin result := FireConnectionRequest(lpContext, cparam, params, cbparam); end;

    EID_SFTPServer_DirCreate: begin result := FireDirCreate(lpContext, cparam, params, cbparam); end;

    EID_SFTPServer_DirList: begin result := FireDirList(lpContext, cparam, params, cbparam); end;

    EID_SFTPServer_DirRemove: begin result := FireDirRemove(lpContext, cparam, params, cbparam); end;

    EID_SFTPServer_Disconnected: begin result := FireDisconnected(lpContext, cparam, params, cbparam); end;

    EID_SFTPServer_Error: begin result := FireError(lpContext, cparam, params, cbparam); end;

    EID_SFTPServer_FileClose: begin result := FireFileClose(lpContext, cparam, params, cbparam); end;

    EID_SFTPServer_FileOpen: begin result := FireFileOpen(lpContext, cparam, params, cbparam); end;

    EID_SFTPServer_FileRead: begin result := FireFileRead(lpContext, cparam, params, cbparam); end;

    EID_SFTPServer_FileRemove: begin result := FireFileRemove(lpContext, cparam, params, cbparam); end;

    EID_SFTPServer_FileRename: begin result := FireFileRename(lpContext, cparam, params, cbparam); end;

    EID_SFTPServer_FileWrite: begin result := FireFileWrite(lpContext, cparam, params, cbparam); end;

    EID_SFTPServer_GetAttributes: begin result := FireGetAttributes(lpContext, cparam, params, cbparam); end;

    EID_SFTPServer_Log: begin result := FireLog(lpContext, cparam, params, cbparam); end;

    EID_SFTPServer_ResolvePath: begin result := FireResolvePath(lpContext, cparam, params, cbparam); end;

    EID_SFTPServer_SetAttributes: begin result := FireSetAttributes(lpContext, cparam, params, cbparam); end;

    EID_SFTPServer_SSHStatus: begin result := FireSSHStatus(lpContext, cparam, params, cbparam); end;

    EID_SFTPServer_SSHUserAuthRequest: begin result := FireSSHUserAuthRequest(lpContext, cparam, params, cbparam); end;

    99999: begin x := 0; end;
  end;
end;

procedure Register;
begin
  iphCore.Register;
  RegisterComponents('IPWorks SSH', [TiphSFTPServer]);
end;

{*********************************************************************************}
procedure TiphSFTPServer.SetSFTPConnectionFileData(ConnectionId: Integer; lpSFTPConnectionFileData: PLXAnsiChar; lenSFTPConnectionFileData: Cardinal);
var
  err: integer;
begin
  if @_IPWorksSSH_SFTPServer_Set = nil then exit;
  err := _IPWorksSSH_SFTPServer_Set(m_ctl, PID_SFTPServer_SFTPConnectionFileData, ConnectionId, Pointer(lpSFTPConnectionFileData), lenSFTPConnectionFileData);
  if err <> 0 then TreatErr(err, '');
end;

procedure TiphSFTPServer.SetSSHCertEncoded(lpSSHCertEncoded: PLXAnsiChar; lenSSHCertEncoded: Cardinal);
var
  err: integer;
begin
  if @_IPWorksSSH_SFTPServer_Set = nil then exit;
  err := _IPWorksSSH_SFTPServer_Set(m_ctl, PID_SFTPServer_SSHCertEncoded, 0, Pointer(lpSSHCertEncoded), lenSSHCertEncoded);
  if err <> 0 then TreatErr(err, '');
end;

procedure TiphSFTPServer.SetSSHCertStore(lpSSHCertStore: PLXAnsiChar; lenSSHCertStore: Cardinal);
var
  err: integer;
begin
  if @_IPWorksSSH_SFTPServer_Set = nil then exit;
  err := _IPWorksSSH_SFTPServer_Set(m_ctl, PID_SFTPServer_SSHCertStore, 0, Pointer(lpSSHCertStore), lenSSHCertStore);
  if err <> 0 then TreatErr(err, '');
end;


type
  TiphConnectionsSFTPConnectionList = class(TiphSFTPConnectionList)
  protected
    function OwnerCtl : TiphSFTPServer;
    // Only need to override one or two methods here
    function CtlGetCount: integer; override;
    procedure CtlSetCount(Value : integer); override;
    function CreateElemInstance(Index : integer): TiphSFTPConnection; override;
  end;

  TiphConnectionsSFTPConnection = class(TiphSFTPConnection)
  protected
    function OwnerCtl : TiphSFTPServer;
    function GetPropConnected: Boolean; override;

    procedure SetPropConnected(Value: Boolean); override;

    function GetPropErrorMessage: String; override;

    procedure SetPropErrorMessage(Value: String); override;

    function GetPropFileData: String; override;

    procedure SetPropFileData(Value: String); override;

    function  GetPropFileDataB : TBytes; override;

    procedure SetPropFileDataB(Value: TBytes); override;

    function GetPropLocalAddress: String; override;

    function GetPropProtocolVersion: Integer; override;

    function GetPropRemoteHost: String; override;

    function GetPropRemotePort: Integer; override;

    function GetPropTimeout: Integer; override;

    procedure SetPropTimeout(Value: Integer); override;

  end;

function TiphConnectionsSFTPConnection.OwnerCtl : TiphSFTPServer;
begin
  Result := TiphSFTPServer(FOwnerCtl);
end;

function TiphConnectionsSFTPConnection.GetPropConnected: Boolean;
begin
  Result := (OwnerCtl.get_SFTPConnectionConnected(Index));
end;


procedure TiphConnectionsSFTPConnection.SetPropConnected(Value: Boolean);
begin
  OwnerCtl.set_SFTPConnectionConnected(Index, (Value));
end;

function TiphConnectionsSFTPConnection.GetPropErrorMessage: String;
begin
  Result := (OwnerCtl.get_SFTPConnectionErrorMessage(Index));
end;


procedure TiphConnectionsSFTPConnection.SetPropErrorMessage(Value: String);
begin
  // The field is read-only
end;

function TiphConnectionsSFTPConnection.GetPropFileData: String;
begin
  Result := (OwnerCtl.get_SFTPConnectionFileData(Index));
end;


procedure TiphConnectionsSFTPConnection.SetPropFileData(Value: String);
begin
  // The field is read-only
end;

function  TiphConnectionsSFTPConnection.GetPropFileDataB : TBytes; 
begin
  Result := OwnerCtl.get_SFTPConnectionFileDataB(Index);
end;


procedure TiphConnectionsSFTPConnection.SetPropFileDataB(Value: TBytes); 
begin
  // The field is read-only
end;

function TiphConnectionsSFTPConnection.GetPropLocalAddress: String;
begin
  Result := (OwnerCtl.get_SFTPConnectionLocalAddress(Index));
end;


function TiphConnectionsSFTPConnection.GetPropProtocolVersion: Integer;
begin
  Result := (OwnerCtl.get_SFTPConnectionProtocolVersion(Index));
end;


function TiphConnectionsSFTPConnection.GetPropRemoteHost: String;
begin
  Result := (OwnerCtl.get_SFTPConnectionRemoteHost(Index));
end;


function TiphConnectionsSFTPConnection.GetPropRemotePort: Integer;
begin
  Result := (OwnerCtl.get_SFTPConnectionRemotePort(Index));
end;


function TiphConnectionsSFTPConnection.GetPropTimeout: Integer;
begin
  Result := (OwnerCtl.get_SFTPConnectionTimeout(Index));
end;


procedure TiphConnectionsSFTPConnection.SetPropTimeout(Value: Integer);
begin
  OwnerCtl.set_SFTPConnectionTimeout(Index, (Value));
end;


function TiphConnectionsSFTPConnectionList.OwnerCtl : TiphSFTPServer;
begin
  Result := TiphSFTPServer(FOwnerCtl);
end;

// Collection's overrides
function TiphConnectionsSFTPConnectionList.CtlGetCount: integer;
begin
  Result := OwnerCtl.get_SFTPConnectionCount();
end;

procedure TiphConnectionsSFTPConnectionList.CtlSetCount(Value : integer);
begin
  // The collection is read-only
end;

function TiphConnectionsSFTPConnectionList.CreateElemInstance(Index : integer): TiphSFTPConnection;
begin
  Result := TiphConnectionsSFTPConnection.Create(FOwnerCtl, true);
  Result.Index := Index;
end;

type
  TiphKeyboardInteractivePromptsSSHPromptList = class(TiphSSHPromptList)
  protected
    function OwnerCtl : TiphSFTPServer;
    // Only need to override one or two methods here
    function CtlGetCount: integer; override;
    procedure CtlSetCount(Value : integer); override;
    function CreateElemInstance(Index : integer): TiphSSHPrompt; override;
  end;

  TiphKeyboardInteractivePromptsSSHPrompt = class(TiphSSHPrompt)
  protected
    function OwnerCtl : TiphSFTPServer;
    function GetPropEcho: Boolean; override;

    procedure SetPropEcho(Value: Boolean); override;

    function GetPropPrompt: String; override;

    procedure SetPropPrompt(Value: String); override;

  end;

function TiphKeyboardInteractivePromptsSSHPrompt.OwnerCtl : TiphSFTPServer;
begin
  Result := TiphSFTPServer(FOwnerCtl);
end;

function TiphKeyboardInteractivePromptsSSHPrompt.GetPropEcho: Boolean;
begin
  Result := (OwnerCtl.get_KeyboardInteractivePromptEcho(Index));
end;


procedure TiphKeyboardInteractivePromptsSSHPrompt.SetPropEcho(Value: Boolean);
begin
  OwnerCtl.set_KeyboardInteractivePromptEcho(Index, (Value));
end;

function TiphKeyboardInteractivePromptsSSHPrompt.GetPropPrompt: String;
begin
  Result := (OwnerCtl.get_KeyboardInteractivePromptPrompt(Index));
end;


procedure TiphKeyboardInteractivePromptsSSHPrompt.SetPropPrompt(Value: String);
begin
  OwnerCtl.set_KeyboardInteractivePromptPrompt(Index, (Value));
end;


function TiphKeyboardInteractivePromptsSSHPromptList.OwnerCtl : TiphSFTPServer;
begin
  Result := TiphSFTPServer(FOwnerCtl);
end;

// Collection's overrides
function TiphKeyboardInteractivePromptsSSHPromptList.CtlGetCount: integer;
begin
  Result := OwnerCtl.get_KeyboardInteractivePromptCount();
end;

procedure TiphKeyboardInteractivePromptsSSHPromptList.CtlSetCount(Value : integer);
begin
  OwnerCtl.set_KeyboardInteractivePromptCount(Value);
end;

function TiphKeyboardInteractivePromptsSSHPromptList.CreateElemInstance(Index : integer): TiphSSHPrompt;
begin
  Result := TiphKeyboardInteractivePromptsSSHPrompt.Create(FOwnerCtl, false);
  Result.Index := Index;
end;


{*********************************************************************************}

constructor TiphSFTPServer.Create(AOwner: TComponent);
begin
  Create(AOwner, IPWORKSSSH_OEMKEY_74);
  Config('CodePage=65001');
end;

constructor TiphSFTPServer.Create(AOwner: TComponent; RuntimeLicense: string);
begin
  inherited Create(AOwner);
  m_ctl := nil;

  if @_IPWorksSSH_SFTPServer_Create <> nil then
    m_ctl := _IPWorksSSH_SFTPServer_Create(@FireEvents, self, nil, CREATE_OPT);
  if m_ctl = nil then
    TreatErr(-1, 'IPWorks SSH SFTPServer: Error creating component');
  _IPWorksSSH_SFTPServer_Set(m_ctl, 8002{PID_KEYCHECK_DLP}, 0, 
    {$ifdef ANSICHAR_IS_BYTE}
    Pointer(ToLinuxAnsiString(RuntimeLicense)),
    {$else}
    Pointer(PAnsiChar(AnsiString(RuntimeLicense))),
    {$endif}
    0);

  FConnections := TiphConnectionsSFTPConnectionList.Create(Self, true);

  FKeyboardInteractivePrompts := TiphKeyboardInteractivePromptsSSHPromptList.Create(Self, false);


  try set_DefaultAuthMethods('password,publickey') except on E:Exception do end;

  try set_DefaultTimeout(60) except on E:Exception do end;

  try set_KeyboardInteractiveMessage('') except on E:Exception do end;

  try set_LocalPort(22) except on E:Exception do end;

  try set_RootDirectory('') except on E:Exception do end;

  try set_SSHCertStore('MY') except on E:Exception do end;

  try set_SSHCertStorePassword('') except on E:Exception do end;

  try set_SSHCertStoreType(cstUser) except on E:Exception do end;

  try set_SSHCertSubject('') except on E:Exception do end;

  try set_SSHCompressionAlgorithms('none,zlib') except on E:Exception do end;

  try set_SSHEncryptionAlgorithms('aes256-ctr,aes192-ctr,aes128-ctr,aes256-cbc,aes192-cbc,aes128-cbc,3des-ctr,3des-cbc,blowfish-cbc,arcfour256,arcfour128,arcfour,cast128-cbc,aes256-gcm@openssh.com,aes128-gcm@openssh.com,chacha20-poly1305@openssh.com') except on E:Exception do end;

end;

destructor TiphSFTPServer.Destroy;
begin
  FreeAndNil(FConnections);

  FreeAndNil(FKeyboardInteractivePrompts);

  
  
  if m_ctl <> nil then begin
    if @_IPWorksSSH_SFTPServer_Destroy <> nil then
      _IPWorksSSH_SFTPServer_Destroy(m_ctl);
  end;
  m_ctl := nil;
  inherited Destroy;
end;

function TiphSFTPServer.ThrowCoreException(err: Integer; const desc: {$ifndef NO_ANSI}AnsiString{$else}string{$endif}): EIPWorksSSH;
begin
  result := EiphSFTPServer.CreateCode(err, desc);
end;

function TiphSFTPServer.ReportEventException(e: Exception; Event: String; ReFire: Boolean): Integer;
var
  msg: String;
begin
  result := -1;
  msg := 'An unhandled error occurred in the ' + Event + ' event handler: ' + E.Message;
  if (m_ctl <> nil) and (@_IPWorksSSH_SFTPServer_SetEventErrorAndCode <> nil) then begin
    if E is EIPWorksSSH then begin
      result := EIPWorksSSH(e).Code;
      msg := E.Message;
    end;
    _IPWorksSSH_SFTPServer_SetEventErrorAndCode(m_ctl, result, 
      {$ifdef ANSICHAR_IS_BYTE}
      Pointer(ToLinuxAnsiString(msg)));
      {$else}
      Pointer(PAnsiChar(AnsiString(msg))));
      {$endif}
  end;
  if ReFire and Assigned(FOnError) then FOnError(self, 0, result, msg);
end;

{*********************************************************************************}

procedure TiphSFTPServer.AboutDlg;
var
  p : LPVOIDARR;
  pc: LPINTARR;
begin
{$IFDEF LINUX}
  inherited AboutDlg;
{$ELSE}
  p := nil;
  pc := nil;
  if @_IPWorksSSH_SFTPServer_Do <> nil then
    _IPWorksSSH_SFTPServer_Do(m_ctl, 255, 0, p, pc, nil);
{$ENDIF}
end;

{*********************************************************************************}

procedure TiphSFTPServer.SetRuntimeLicense(key: String);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SFTPServer_Set = nil then exit;
  err := _IPWorksSSH_SFTPServer_Set(m_ctl, 8001{PID_RUNTIME_LICENSE}, 0, 
    {$ifdef ANSICHAR_IS_BYTE}
    Pointer(ToLinuxAnsiString(key)),
    {$else}
    Pointer(PAnsiChar(AnsiString(key))),
    {$endif}
    0);

  if err <> 0 then TreatErr(err, '');
end;

function TiphSFTPServer.GetRuntimeLicense: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';
  if @_IPWorksSSH_SFTPServer_Get = nil then exit;
  tmp := _IPWorksSSH_SFTPServer_Get(m_ctl, 8001{PID_RUNTIME_LICENSE}, 0, nil, nil);
  err := _IPWorksSSH_SFTPServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$ifdef ANSICHAR_IS_BYTE}
  result := FromLinuxAnsiString(PLXAnsiChar(tmp));
  {$else}
  result := PAnsiChar(tmp);
  {$endif}
end;

{*********************************************************************************}

function TiphSFTPServer.HasData: Boolean;
begin
  result := false;
end;

procedure TiphSFTPServer.ReadHnd(Reader: TStream);
begin
end;

procedure TiphSFTPServer.WriteHnd(Writer: TStream);
begin
end;

function TiphSFTPServer.BStr2CStr(pBStr: Pointer; lenBStr: Integer; bArr: TBytes): String;
{$IF DEFINED(UNICODE) AND DEFINED(MSWINDOWS)}
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
{$IFEND}
begin
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  param[0] := pBStr; paramcb[0] := lenBStr;
  param[1] := nil;   paramcb[1] := 0;
  param[2] := nil;   paramcb[2] := 0;
  _IPWorksSSH_SFTPServer_Do(m_ctl, 2003, 2, @param, @paramcb, nil);
  SetLength(Result, Integer(param[2]));
  if Length(Result) > 0 then begin
    param[1] := PChar(Result); paramcb[1] := Length(Result);
    _IPWorksSSH_SFTPServer_Do(m_ctl, 2003, 2, @param, @paramcb, nil);
  end;
  {$ELSE}
  try
    Result := TEncoding.Default.GetString(bArr);
  except
    on E: Exception do
      SetString(Result, PAnsiChar(pBStr), lenBStr);
   end;
  {$ENDIF}
{$ELSE}
  SetString(Result, PAnsiChar(pBStr), lenBStr);
{$ENDIF}
end;

procedure TiphSFTPServer.DefineProperties(Filer: TFiler);
begin
  inherited DefineProperties(Filer);
  Filer.DefineBinaryProperty('RegHnd', ReadHnd, WriteHnd, HasData);
end;

{*********************************************************************************}

procedure TiphSFTPServer.TreatErr(Err: Integer; const desc: String);
var
  msg : {$ifdef NO_ANSI}string{$else}LXAnsiString{$endif};
begin
  {$ifdef NO_ANSI}
  msg := desc;
  {$else}
  {$ifdef ANSICHAR_IS_BYTE}
  msg := ToLinuxAnsiString(desc);
  {$else}
  msg := LXAnsiString(desc);
  {$endif}
  {$endif}
  if Length(msg) = 0 then
  begin
    if @_IPWorksSSH_SFTPServer_GetLastError <> nil then
      {$ifdef NO_ANSI}
      {$ifdef ANSICHAR_IS_BYTE}
      msg := FromLinuxAnsiString(_IPWorksSSH_SFTPServer_GetLastError(m_ctl));
      {$else}
      msg := _IPWorksSSH_SFTPServer_GetLastError(m_ctl);
      {$endif}
      {$else}
      msg := _IPWorksSSH_SFTPServer_GetLastError(m_ctl);
      {$endif}
  end;

  raise ThrowCoreException(err, msg);
end;

{*********************************************************************************}

(*** Property Getters/Setters ***)
function TiphSFTPServer.get_ConnectionBacklog: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SFTPServer_Get = nil then exit;
  tmp := _IPWorksSSH_SFTPServer_Get(m_ctl, PID_SFTPServer_ConnectionBacklog, 0, nil, nil);
  err := _IPWorksSSH_SFTPServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSFTPServer.set_ConnectionBacklog(valConnectionBacklog: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SFTPServer_Set = nil then exit;
  err := _IPWorksSSH_SFTPServer_Set(m_ctl, PID_SFTPServer_ConnectionBacklog, 0, Pointer(valConnectionBacklog), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTPServer.get_SFTPConnectionCount: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SFTPServer_Get = nil then exit;
  tmp := _IPWorksSSH_SFTPServer_Get(m_ctl, PID_SFTPServer_SFTPConnectionCount, 0, nil, nil);
  err := _IPWorksSSH_SFTPServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


function TiphSFTPServer.get_SFTPConnectionConnected(ConnectionId: Integer): Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);
  if @_IPWorksSSH_SFTPServer_CheckIndex = nil then exit;
  err := _IPWorksSSH_SFTPServer_CheckIndex(m_ctl, PID_SFTPServer_SFTPConnectionConnected, ConnectionId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for SFTPConnectionConnected');

  if @_IPWorksSSH_SFTPServer_Get = nil then exit;
  tmp := _IPWorksSSH_SFTPServer_Get(m_ctl, PID_SFTPServer_SFTPConnectionConnected, ConnectionId, nil, nil);
  err := _IPWorksSSH_SFTPServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


procedure TiphSFTPServer.set_SFTPConnectionConnected(ConnectionId: Integer; valSFTPConnectionConnected: Boolean);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SFTPServer_Set = nil then exit;
  err := _IPWorksSSH_SFTPServer_Set(m_ctl, PID_SFTPServer_SFTPConnectionConnected, ConnectionId, Pointer(valSFTPConnectionConnected), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTPServer.get_SFTPConnectionErrorMessage(ConnectionId: Integer): String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';
  if @_IPWorksSSH_SFTPServer_CheckIndex = nil then exit;
  err := _IPWorksSSH_SFTPServer_CheckIndex(m_ctl, PID_SFTPServer_SFTPConnectionErrorMessage, ConnectionId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for SFTPConnectionErrorMessage');

  if @_IPWorksSSH_SFTPServer_Get = nil then exit;
  tmp := _IPWorksSSH_SFTPServer_Get(m_ctl, PID_SFTPServer_SFTPConnectionErrorMessage{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, ConnectionId, nil, nil);
  err := _IPWorksSSH_SFTPServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphSFTPServer.get_SFTPConnectionFileData(ConnectionId: Integer): String;
var
  tmp: {$IFDEF NONWINUNICODE}TBytes{$ELSE}Pointer; len: Cardinal{$ENDIF};
  err: Integer;
begin
  result := '';
  if @_IPWorksSSH_SFTPServer_CheckIndex = nil then exit;
  err := _IPWorksSSH_SFTPServer_CheckIndex(m_ctl, PID_SFTPServer_SFTPConnectionFileData, ConnectionId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for SFTPConnectionFileData');

  if @_IPWorksSSH_SFTPServer_Get = nil then exit;
  tmp := {$IFDEF NONWINUNICODE}get_SFTPConnectionFileDataB(ConnectionId){$ELSE}_IPWorksSSH_SFTPServer_Get(m_ctl, PID_SFTPServer_SFTPConnectionFileData{$IFDEF UNICODE_ON_UNICODE}+20000{$ENDIF}, ConnectionId, @len, nil){$ENDIF};
  err := _IPWorksSSH_SFTPServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$IFDEF NONWINUNICODE}result := TEncoding.UTF8.GetString(tmp);{$ELSE}SetString(result, PChar(tmp), len);{$ENDIF}
end;


function TiphSFTPServer.get_SFTPConnectionFileDataB(ConnectionId: Integer): TBytes;
var
  tmp: Pointer;
  len: Cardinal;
  err: Integer;
begin
  result := nil;
  if @_IPWorksSSH_SFTPServer_CheckIndex = nil then exit;
  err := _IPWorksSSH_SFTPServer_CheckIndex(m_ctl, PID_SFTPServer_SFTPConnectionFileData, ConnectionId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for SFTPConnectionFileDataB');
  if @_IPWorksSSH_SFTPServer_Get = nil then exit;
  tmp := _IPWorksSSH_SFTPServer_Get(m_ctl, PID_SFTPServer_SFTPConnectionFileData, ConnectionId, @len, nil);
  SetLength(result, len);
  Move(Pointer(tmp)^, Pointer(result)^, len); // IMPORTANT: Do NOT multiply the length value!
end;


function TiphSFTPServer.get_SFTPConnectionLocalAddress(ConnectionId: Integer): String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';
  if @_IPWorksSSH_SFTPServer_CheckIndex = nil then exit;
  err := _IPWorksSSH_SFTPServer_CheckIndex(m_ctl, PID_SFTPServer_SFTPConnectionLocalAddress, ConnectionId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for SFTPConnectionLocalAddress');

  if @_IPWorksSSH_SFTPServer_Get = nil then exit;
  tmp := _IPWorksSSH_SFTPServer_Get(m_ctl, PID_SFTPServer_SFTPConnectionLocalAddress{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, ConnectionId, nil, nil);
  err := _IPWorksSSH_SFTPServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphSFTPServer.get_SFTPConnectionProtocolVersion(ConnectionId: Integer): Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);
  if @_IPWorksSSH_SFTPServer_CheckIndex = nil then exit;
  err := _IPWorksSSH_SFTPServer_CheckIndex(m_ctl, PID_SFTPServer_SFTPConnectionProtocolVersion, ConnectionId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for SFTPConnectionProtocolVersion');

  if @_IPWorksSSH_SFTPServer_Get = nil then exit;
  tmp := _IPWorksSSH_SFTPServer_Get(m_ctl, PID_SFTPServer_SFTPConnectionProtocolVersion, ConnectionId, nil, nil);
  err := _IPWorksSSH_SFTPServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


function TiphSFTPServer.get_SFTPConnectionRemoteHost(ConnectionId: Integer): String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';
  if @_IPWorksSSH_SFTPServer_CheckIndex = nil then exit;
  err := _IPWorksSSH_SFTPServer_CheckIndex(m_ctl, PID_SFTPServer_SFTPConnectionRemoteHost, ConnectionId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for SFTPConnectionRemoteHost');

  if @_IPWorksSSH_SFTPServer_Get = nil then exit;
  tmp := _IPWorksSSH_SFTPServer_Get(m_ctl, PID_SFTPServer_SFTPConnectionRemoteHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, ConnectionId, nil, nil);
  err := _IPWorksSSH_SFTPServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphSFTPServer.get_SFTPConnectionRemotePort(ConnectionId: Integer): Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);
  if @_IPWorksSSH_SFTPServer_CheckIndex = nil then exit;
  err := _IPWorksSSH_SFTPServer_CheckIndex(m_ctl, PID_SFTPServer_SFTPConnectionRemotePort, ConnectionId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for SFTPConnectionRemotePort');

  if @_IPWorksSSH_SFTPServer_Get = nil then exit;
  tmp := _IPWorksSSH_SFTPServer_Get(m_ctl, PID_SFTPServer_SFTPConnectionRemotePort, ConnectionId, nil, nil);
  err := _IPWorksSSH_SFTPServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


function TiphSFTPServer.get_SFTPConnectionTimeout(ConnectionId: Integer): Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);
  if @_IPWorksSSH_SFTPServer_CheckIndex = nil then exit;
  err := _IPWorksSSH_SFTPServer_CheckIndex(m_ctl, PID_SFTPServer_SFTPConnectionTimeout, ConnectionId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for SFTPConnectionTimeout');

  if @_IPWorksSSH_SFTPServer_Get = nil then exit;
  tmp := _IPWorksSSH_SFTPServer_Get(m_ctl, PID_SFTPServer_SFTPConnectionTimeout, ConnectionId, nil, nil);
  err := _IPWorksSSH_SFTPServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSFTPServer.set_SFTPConnectionTimeout(ConnectionId: Integer; valSFTPConnectionTimeout: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SFTPServer_Set = nil then exit;
  err := _IPWorksSSH_SFTPServer_Set(m_ctl, PID_SFTPServer_SFTPConnectionTimeout, ConnectionId, Pointer(valSFTPConnectionTimeout), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTPServer.get_DefaultAuthMethods: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SFTPServer_Get = nil then exit;
  tmp := _IPWorksSSH_SFTPServer_Get(m_ctl, PID_SFTPServer_DefaultAuthMethods{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SFTPServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSFTPServer.set_DefaultAuthMethods(valDefaultAuthMethods: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SFTPServer_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valDefaultAuthMethods);
  {$ENDIF}
  err := _IPWorksSSH_SFTPServer_Set(m_ctl, PID_SFTPServer_DefaultAuthMethods{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valDefaultAuthMethods){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTPServer.get_DefaultTimeout: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SFTPServer_Get = nil then exit;
  tmp := _IPWorksSSH_SFTPServer_Get(m_ctl, PID_SFTPServer_DefaultTimeout, 0, nil, nil);
  err := _IPWorksSSH_SFTPServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSFTPServer.set_DefaultTimeout(valDefaultTimeout: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SFTPServer_Set = nil then exit;
  err := _IPWorksSSH_SFTPServer_Set(m_ctl, PID_SFTPServer_DefaultTimeout, 0, Pointer(valDefaultTimeout), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTPServer.get_KeyboardInteractiveMessage: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SFTPServer_Get = nil then exit;
  tmp := _IPWorksSSH_SFTPServer_Get(m_ctl, PID_SFTPServer_KeyboardInteractiveMessage{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SFTPServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSFTPServer.set_KeyboardInteractiveMessage(valKeyboardInteractiveMessage: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SFTPServer_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valKeyboardInteractiveMessage);
  {$ENDIF}
  err := _IPWorksSSH_SFTPServer_Set(m_ctl, PID_SFTPServer_KeyboardInteractiveMessage{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valKeyboardInteractiveMessage){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTPServer.get_KeyboardInteractivePromptCount: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SFTPServer_Get = nil then exit;
  tmp := _IPWorksSSH_SFTPServer_Get(m_ctl, PID_SFTPServer_KeyboardInteractivePromptCount, 0, nil, nil);
  err := _IPWorksSSH_SFTPServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSFTPServer.set_KeyboardInteractivePromptCount(valKeyboardInteractivePromptCount: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SFTPServer_Set = nil then exit;
  err := _IPWorksSSH_SFTPServer_Set(m_ctl, PID_SFTPServer_KeyboardInteractivePromptCount, 0, Pointer(valKeyboardInteractivePromptCount), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTPServer.get_KeyboardInteractivePromptEcho(PromptIndex: Integer): Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);
  if @_IPWorksSSH_SFTPServer_CheckIndex = nil then exit;
  err := _IPWorksSSH_SFTPServer_CheckIndex(m_ctl, PID_SFTPServer_KeyboardInteractivePromptEcho, PromptIndex);
  if err <> 0 then TreatErr(err, 'Invalid array index value for KeyboardInteractivePromptEcho');

  if @_IPWorksSSH_SFTPServer_Get = nil then exit;
  tmp := _IPWorksSSH_SFTPServer_Get(m_ctl, PID_SFTPServer_KeyboardInteractivePromptEcho, PromptIndex, nil, nil);
  err := _IPWorksSSH_SFTPServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


procedure TiphSFTPServer.set_KeyboardInteractivePromptEcho(PromptIndex: Integer; valKeyboardInteractivePromptEcho: Boolean);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SFTPServer_Set = nil then exit;
  err := _IPWorksSSH_SFTPServer_Set(m_ctl, PID_SFTPServer_KeyboardInteractivePromptEcho, PromptIndex, Pointer(valKeyboardInteractivePromptEcho), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTPServer.get_KeyboardInteractivePromptPrompt(PromptIndex: Integer): String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';
  if @_IPWorksSSH_SFTPServer_CheckIndex = nil then exit;
  err := _IPWorksSSH_SFTPServer_CheckIndex(m_ctl, PID_SFTPServer_KeyboardInteractivePromptPrompt, PromptIndex);
  if err <> 0 then TreatErr(err, 'Invalid array index value for KeyboardInteractivePromptPrompt');

  if @_IPWorksSSH_SFTPServer_Get = nil then exit;
  tmp := _IPWorksSSH_SFTPServer_Get(m_ctl, PID_SFTPServer_KeyboardInteractivePromptPrompt{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, PromptIndex, nil, nil);
  err := _IPWorksSSH_SFTPServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSFTPServer.set_KeyboardInteractivePromptPrompt(PromptIndex: Integer; valKeyboardInteractivePromptPrompt: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SFTPServer_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valKeyboardInteractivePromptPrompt);
  {$ENDIF}
  err := _IPWorksSSH_SFTPServer_Set(m_ctl, PID_SFTPServer_KeyboardInteractivePromptPrompt{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, PromptIndex, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valKeyboardInteractivePromptPrompt){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTPServer.get_Listening: Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);

  if @_IPWorksSSH_SFTPServer_Get = nil then exit;
  tmp := _IPWorksSSH_SFTPServer_Get(m_ctl, PID_SFTPServer_Listening, 0, nil, nil);
  err := _IPWorksSSH_SFTPServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


procedure TiphSFTPServer.set_Listening(valListening: Boolean);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SFTPServer_Set = nil then exit;
  err := _IPWorksSSH_SFTPServer_Set(m_ctl, PID_SFTPServer_Listening, 0, Pointer(valListening), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTPServer.get_LocalHost: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SFTPServer_Get = nil then exit;
  tmp := _IPWorksSSH_SFTPServer_Get(m_ctl, PID_SFTPServer_LocalHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SFTPServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSFTPServer.set_LocalHost(valLocalHost: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SFTPServer_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valLocalHost);
  {$ENDIF}
  err := _IPWorksSSH_SFTPServer_Set(m_ctl, PID_SFTPServer_LocalHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valLocalHost){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTPServer.get_LocalPort: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SFTPServer_Get = nil then exit;
  tmp := _IPWorksSSH_SFTPServer_Get(m_ctl, PID_SFTPServer_LocalPort, 0, nil, nil);
  err := _IPWorksSSH_SFTPServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSFTPServer.set_LocalPort(valLocalPort: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SFTPServer_Set = nil then exit;
  err := _IPWorksSSH_SFTPServer_Set(m_ctl, PID_SFTPServer_LocalPort, 0, Pointer(valLocalPort), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTPServer.get_RootDirectory: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SFTPServer_Get = nil then exit;
  tmp := _IPWorksSSH_SFTPServer_Get(m_ctl, PID_SFTPServer_RootDirectory{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SFTPServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSFTPServer.set_RootDirectory(valRootDirectory: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SFTPServer_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valRootDirectory);
  {$ENDIF}
  err := _IPWorksSSH_SFTPServer_Set(m_ctl, PID_SFTPServer_RootDirectory{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valRootDirectory){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTPServer.get_SSHCertEncoded: String;
var
  tmp: {$IFDEF NONWINUNICODE}TBytes{$ELSE}Pointer; len: Cardinal{$ENDIF};
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SFTPServer_Get = nil then exit;
  tmp := {$IFDEF NONWINUNICODE}get_SSHCertEncodedB{$ELSE}_IPWorksSSH_SFTPServer_Get(m_ctl, PID_SFTPServer_SSHCertEncoded{$IFDEF UNICODE_ON_UNICODE}+20000{$ENDIF}, 0, @len, nil){$ENDIF};
  err := _IPWorksSSH_SFTPServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$IFDEF NONWINUNICODE}result := TEncoding.UTF8.GetString(tmp);{$ELSE}SetString(result, PChar(tmp), len);{$ENDIF}
end;


procedure TiphSFTPServer.set_SSHCertEncoded(valSSHCertEncoded: String);
var
  err: Integer;
  {$IFDEF NONWINUNICODE}tmp: TBytes;{$ENDIF}
begin
  err := 0;
  if @_IPWorksSSH_SFTPServer_Set = nil then exit;
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  err := _IPWorksSSH_SFTPServer_Set(m_ctl, PID_SFTPServer_SSHCertEncoded+20000, 0, Pointer(PWideChar(valSSHCertEncoded)), Length(valSSHCertEncoded));
  {$ELSE}
  tmp := TEncoding.UTF8.GetBytes(valSSHCertEncoded);
  set_SSHCertEncodedB(tmp);
  {$ENDIF}
{$ELSE}
  err := _IPWorksSSH_SFTPServer_Set(m_ctl, PID_SFTPServer_SSHCertEncoded, 0, Pointer(PAnsiChar(valSSHCertEncoded)), Length(valSSHCertEncoded));
{$ENDIF}
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTPServer.get_SSHCertEncodedB: TBytes;
var
  tmp: Pointer;
  len: Cardinal;
begin
  result := nil;
  if @_IPWorksSSH_SFTPServer_Get = nil then exit;
  tmp := _IPWorksSSH_SFTPServer_Get(m_ctl, PID_SFTPServer_SSHCertEncoded, 0, @len, nil);
  SetLength(result, len);
  Move(Pointer(tmp)^, Pointer(result)^, len); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSFTPServer.set_SSHCertEncodedB(valSSHCertEncoded: TBytes);
var
  err: Integer;
begin
  err := _IPWorksSSH_SFTPServer_Set(m_ctl, PID_SFTPServer_SSHCertEncoded, 0, Pointer(valSSHCertEncoded), Length(valSSHCertEncoded));
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTPServer.get_SSHCertStore: String;
var
  tmp: {$IFDEF NONWINUNICODE}TBytes{$ELSE}Pointer; len: Cardinal{$ENDIF};
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SFTPServer_Get = nil then exit;
  tmp := {$IFDEF NONWINUNICODE}get_SSHCertStoreB{$ELSE}_IPWorksSSH_SFTPServer_Get(m_ctl, PID_SFTPServer_SSHCertStore{$IFDEF UNICODE_ON_UNICODE}+20000{$ENDIF}, 0, @len, nil){$ENDIF};
  err := _IPWorksSSH_SFTPServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$IFDEF NONWINUNICODE}result := TEncoding.UTF8.GetString(tmp);{$ELSE}SetString(result, PChar(tmp), len);{$ENDIF}
end;


procedure TiphSFTPServer.set_SSHCertStore(valSSHCertStore: String);
var
  err: Integer;
  {$IFDEF NONWINUNICODE}tmp: TBytes;{$ENDIF}
begin
  err := 0;
  if @_IPWorksSSH_SFTPServer_Set = nil then exit;
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  err := _IPWorksSSH_SFTPServer_Set(m_ctl, PID_SFTPServer_SSHCertStore+20000, 0, Pointer(PWideChar(valSSHCertStore)), Length(valSSHCertStore));
  {$ELSE}
  tmp := TEncoding.UTF8.GetBytes(valSSHCertStore);
  set_SSHCertStoreB(tmp);
  {$ENDIF}
{$ELSE}
  err := _IPWorksSSH_SFTPServer_Set(m_ctl, PID_SFTPServer_SSHCertStore, 0, Pointer(PAnsiChar(valSSHCertStore)), Length(valSSHCertStore));
{$ENDIF}
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTPServer.get_SSHCertStoreB: TBytes;
var
  tmp: Pointer;
  len: Cardinal;
begin
  result := nil;
  if @_IPWorksSSH_SFTPServer_Get = nil then exit;
  tmp := _IPWorksSSH_SFTPServer_Get(m_ctl, PID_SFTPServer_SSHCertStore, 0, @len, nil);
  SetLength(result, len);
  Move(Pointer(tmp)^, Pointer(result)^, len); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSFTPServer.set_SSHCertStoreB(valSSHCertStore: TBytes);
var
  err: Integer;
begin
  err := _IPWorksSSH_SFTPServer_Set(m_ctl, PID_SFTPServer_SSHCertStore, 0, Pointer(valSSHCertStore), Length(valSSHCertStore));
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTPServer.get_SSHCertStorePassword: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SFTPServer_Get = nil then exit;
  tmp := _IPWorksSSH_SFTPServer_Get(m_ctl, PID_SFTPServer_SSHCertStorePassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SFTPServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSFTPServer.set_SSHCertStorePassword(valSSHCertStorePassword: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SFTPServer_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHCertStorePassword);
  {$ENDIF}
  err := _IPWorksSSH_SFTPServer_Set(m_ctl, PID_SFTPServer_SSHCertStorePassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHCertStorePassword){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTPServer.get_SSHCertStoreType: TiphsftpserverSSHCertStoreTypes;
var
  tmp: Pointer;
  err: Integer;
begin
  result := TiphsftpserverSSHCertStoreTypes(0);

  if @_IPWorksSSH_SFTPServer_Get = nil then exit;
  tmp := _IPWorksSSH_SFTPServer_Get(m_ctl, PID_SFTPServer_SSHCertStoreType, 0, nil, nil);
  err := _IPWorksSSH_SFTPServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := TiphsftpserverSSHCertStoreTypes(tmp);
end;


procedure TiphSFTPServer.set_SSHCertStoreType(valSSHCertStoreType: TiphsftpserverSSHCertStoreTypes);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SFTPServer_Set = nil then exit;
  err := _IPWorksSSH_SFTPServer_Set(m_ctl, PID_SFTPServer_SSHCertStoreType, 0, Pointer(valSSHCertStoreType), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTPServer.get_SSHCertSubject: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SFTPServer_Get = nil then exit;
  tmp := _IPWorksSSH_SFTPServer_Get(m_ctl, PID_SFTPServer_SSHCertSubject{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SFTPServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSFTPServer.set_SSHCertSubject(valSSHCertSubject: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SFTPServer_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHCertSubject);
  {$ENDIF}
  err := _IPWorksSSH_SFTPServer_Set(m_ctl, PID_SFTPServer_SSHCertSubject{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHCertSubject){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTPServer.get_SSHCompressionAlgorithms: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SFTPServer_Get = nil then exit;
  tmp := _IPWorksSSH_SFTPServer_Get(m_ctl, PID_SFTPServer_SSHCompressionAlgorithms{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SFTPServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSFTPServer.set_SSHCompressionAlgorithms(valSSHCompressionAlgorithms: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SFTPServer_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHCompressionAlgorithms);
  {$ENDIF}
  err := _IPWorksSSH_SFTPServer_Set(m_ctl, PID_SFTPServer_SSHCompressionAlgorithms{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHCompressionAlgorithms){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTPServer.get_SSHEncryptionAlgorithms: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SFTPServer_Get = nil then exit;
  tmp := _IPWorksSSH_SFTPServer_Get(m_ctl, PID_SFTPServer_SSHEncryptionAlgorithms{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SFTPServer_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSFTPServer.set_SSHEncryptionAlgorithms(valSSHEncryptionAlgorithms: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SFTPServer_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHEncryptionAlgorithms);
  {$ENDIF}
  err := _IPWorksSSH_SFTPServer_Set(m_ctl, PID_SFTPServer_SSHEncryptionAlgorithms{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHEncryptionAlgorithms){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;



{*********************************************************************************}

(*** Property Getters/Setters: OO API ***)
function  TiphSFTPServer.get_Connections: TiphSFTPConnectionList;
begin
  Result := FConnections;
end;

function  TiphSFTPServer.get_KeyboardInteractivePrompts: TiphSSHPromptList;
begin
  Result := FKeyboardInteractivePrompts;
end;

procedure TiphSFTPServer.set_KeyboardInteractivePrompts(Value : TiphSSHPromptList);
begin
  FKeyboardInteractivePrompts.Assign(Value);
end;


{*********************************************************************************}

{$IFNDEF DELPHI3}
(*** Methods ***)
function TiphSFTPServer.Config(ConfigurationString: String): String;
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
  tmp_ConfigurationString: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := '';

  tmp_ConfigurationString := {$ifndef UNICODE_ON_ANSI}ConfigurationString{$else}ToLinuxAnsiString(ConfigurationString){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_ConfigurationString){$else}@tmp_ConfigurationString[0]{$endif};

  if @_IPWorksSSH_SFTPServer_Do = nil then exit;
  err := _IPWorksSSH_SFTPServer_Do(m_ctl, MID_SFTPServer_Config{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  result := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(param[1]);
end;


procedure TiphSFTPServer.Disconnect(ConnectionId: Integer);
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  param[0] := Pointer(ConnectionId);

  if @_IPWorksSSH_SFTPServer_Do = nil then exit;
  err := _IPWorksSSH_SFTPServer_Do(m_ctl, MID_SFTPServer_Disconnect{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSFTPServer.DoEvents();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SFTPServer_Do = nil then exit;
  err := _IPWorksSSH_SFTPServer_Do(m_ctl, MID_SFTPServer_DoEvents{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSFTPServer.ExchangeKeys(ConnectionId: Integer);
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  param[0] := Pointer(ConnectionId);

  if @_IPWorksSSH_SFTPServer_Do = nil then exit;
  err := _IPWorksSSH_SFTPServer_Do(m_ctl, MID_SFTPServer_ExchangeKeys{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSFTPServer.Reset();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SFTPServer_Do = nil then exit;
  err := _IPWorksSSH_SFTPServer_Do(m_ctl, MID_SFTPServer_Reset{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSFTPServer.SetFileList(ConnectionId: Integer; List: String);
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
  i, err: Integer;
  tmp_List: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 2 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  param[0] := Pointer(ConnectionId);

  tmp_List := {$ifndef UNICODE_ON_ANSI}List{$else}ToLinuxAnsiString(List){$endif};
  param[1] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_List){$else}@tmp_List[0]{$endif};

  if @_IPWorksSSH_SFTPServer_Do = nil then exit;
  err := _IPWorksSSH_SFTPServer_Do(m_ctl, MID_SFTPServer_SetFileList{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 2, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSFTPServer.Shutdown();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SFTPServer_Do = nil then exit;
  err := _IPWorksSSH_SFTPServer_Do(m_ctl, MID_SFTPServer_Shutdown{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSFTPServer.StartListening();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SFTPServer_Do = nil then exit;
  err := _IPWorksSSH_SFTPServer_Do(m_ctl, MID_SFTPServer_StartListening{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSFTPServer.StopListening();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SFTPServer_Do = nil then exit;
  err := _IPWorksSSH_SFTPServer_Do(m_ctl, MID_SFTPServer_StopListening{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


{$ENDIF}


{$IFDEF USE_DYNAMIC_LOADING}

procedure ClearProcs();
begin
  _IPWorksSSH_EvtStr := nil;
  _IPWorksSSH_Stream := nil;
  _IPWorksSSH_SFTPServer_Create := nil;
  _IPWorksSSH_SFTPServer_Destroy := nil;
  _IPWorksSSH_SFTPServer_Set := nil;
  _IPWorksSSH_SFTPServer_Get := nil;
  _IPWorksSSH_SFTPServer_GetLastError := nil;
  _IPWorksSSH_SFTPServer_GetLastErrorCode := nil;
  _IPWorksSSH_SFTPServer_SetLastErrorAndCode := nil;
  _IPWorksSSH_SFTPServer_GetEventError := nil;
  _IPWorksSSH_SFTPServer_GetEventErrorCode := nil;
  _IPWorksSSH_SFTPServer_SetEventErrorAndCode := nil;
  {$IFDEF USE_EXPLICIT_INIT}
  _IPWorksSSH_SFTPServer_StaticInit := nil;
  _IPWorksSSH_SFTPServer_StaticDestroy := nil;
  {$ENDIF}
  _IPWorksSSH_SFTPServer_CheckIndex := nil;
  _IPWorksSSH_SFTPServer_Do := nil;
end;

{$IFDEF USEIPWORKSSSHDLL}
procedure LoadDLL();
begin
  ClearProcs();

  hLib := LoadLibrary(PChar(DLLNAME));
  if hLib = 0 then exit;

  @_IPWorksSSH_EvtStr                          := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_EvtStr');
  @_IPWorksSSH_Stream                          := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_Stream');
  @_IPWorksSSH_SFTPServer_Create               := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SFTPServer_Create');
  @_IPWorksSSH_SFTPServer_Destroy              := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SFTPServer_Destroy');
  @_IPWorksSSH_SFTPServer_Set                  := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SFTPServer_Set');
  @_IPWorksSSH_SFTPServer_Get                  := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SFTPServer_Get');
  @_IPWorksSSH_SFTPServer_GetLastError         := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SFTPServer_GetLastError');
  @_IPWorksSSH_SFTPServer_GetLastErrorCode     := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SFTPServer_GetLastErrorCode');
  @_IPWorksSSH_SFTPServer_SetLastErrorAndCode  := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SFTPServer_SetLastErrorAndCode');
  @_IPWorksSSH_SFTPServer_GetEventError        := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SFTPServer_GetEventError');
  @_IPWorksSSH_SFTPServer_GetEventErrorCode    := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SFTPServer_GetEventErrorCode');
  @_IPWorksSSH_SFTPServer_SetEventErrorAndCode := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SFTPServer_SetEventErrorAndCode');
  {$IFDEF USE_EXPLICIT_INIT}
  @_IPWorksSSH_SFTPServer_StaticInit           := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SFTPServer_StaticInit');
  @_IPWorksSSH_SFTPServer_StaticDestroy        := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SFTPServer_StaticDestroy');
  {$ENDIF}
  @_IPWorksSSH_SFTPServer_CheckIndex           := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SFTPServer_CheckIndex');
  @_IPWorksSSH_SFTPServer_Do                   := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SFTPServer_Do');
end;
{$ELSE}
procedure LoadRESDLL();
var
  hResInfo: HRSRC;
  hResData: HGLOBAL;
  pResData: Pointer;
begin
  ClearProcs();

  hResInfo := FindResource(HInstance, 'ipworksssh22_dta', RT_RCDATA);
  if hResInfo = 0 then exit;

  hResData := LoadResource(HInstance, hResInfo);
  if hResData = 0 then exit;

  pResData := LockResource(hResData);
  if pResData = nil then exit;

  pBaseAddress := IPWorksSSHLoadDRU(pResData, pEntryPoint);
  if Assigned(pBaseAddress) then begin
    @_IPWorksSSH_EvtStr                          := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_EvtStr');
    @_IPWorksSSH_Stream                          := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_Stream');
    @_IPWorksSSH_SFTPServer_Create               := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SFTPServer_Create');
    @_IPWorksSSH_SFTPServer_Destroy              := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SFTPServer_Destroy');
    @_IPWorksSSH_SFTPServer_Set                  := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SFTPServer_Set');
    @_IPWorksSSH_SFTPServer_Get                  := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SFTPServer_Get');
    @_IPWorksSSH_SFTPServer_GetLastError         := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SFTPServer_GetLastError');
    @_IPWorksSSH_SFTPServer_GetLastErrorCode     := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SFTPServer_GetLastErrorCode');
    @_IPWorksSSH_SFTPServer_SetLastErrorAndCode  := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SFTPServer_SetLastErrorAndCode');
    @_IPWorksSSH_SFTPServer_GetEventError        := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SFTPServer_GetEventError');
    @_IPWorksSSH_SFTPServer_GetEventErrorCode    := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SFTPServer_GetEventErrorCode');
    @_IPWorksSSH_SFTPServer_SetEventErrorAndCode := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SFTPServer_SetEventErrorAndCode');
    @_IPWorksSSH_SFTPServer_CheckIndex           := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SFTPServer_CheckIndex');
    @_IPWorksSSH_SFTPServer_Do                   := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SFTPServer_Do');
  end;

  FreeResource(hResData);
end;
{$ENDIF USEIPWORKSSSHDLL}

initialization
begin
  iLoadCount := iLoadCount + 1;
  if iLoadCount > 1 then exit;

  {$IFDEF USEIPWORKSSSHDLL}
  hLib := 0;
  LoadDLL();
  {$ELSE}
  pBaseAddress := nil;
  pEntryPoint := nil;
  LoadResDLL();
  {$ENDIF}

  {$IFDEF USE_EXPLICIT_INIT}
  if @_IPWorksSSH_SFTPServer_StaticInit <> nil then
    _IPWorksSSH_SFTPServer_StaticInit(nil);
  {$ENDIF}
end;

finalization
begin
  iLoadCount := iLoadCount - 1;
  if iLoadCount > 0 then exit;

  {$IFDEF USE_EXPLICIT_INIT}
  if @_IPWorksSSH_SFTPServer_StaticDestroy <> nil then
    _IPWorksSSH_SFTPServer_StaticDestroy();
  {$ENDIF}

  ClearProcs();

  {$IFDEF USEIPWORKSSSHDLL}
  if hLib <> 0 then
  begin
    {$ifndef FPC}FreeLibrary{$else}UnloadLibrary{$endif}(hLib);
    hLib := 0;
  end;
  {$ELSE}
  IPWorksSSHFreeDRU(pBaseAddress, pEntryPoint);
  pBaseAddress := nil;
  pEntryPoint := nil;
  {$ENDIF}
  iLoadCount := 0;
end;

{$ELSE USE_DYNAMIC_LOADING}

// in case if static library used

{$IFDEF USE_EXPLICIT_INIT}

initialization
  _IPWorksSSH_SFTPServer_StaticInit(nil);

finalization
  _IPWorksSSH_SFTPServer_StaticDestroy();

{$ENDIF USE_EXPLICIT_INIT}

{$ENDIF USE_DYNAMIC_LOADING}


end.
