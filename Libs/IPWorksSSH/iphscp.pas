
unit iphscp;



{$UNDEF USE_DYNAMIC_LOADING}
{$UNDEF USE_EXPLICIT_INIT}
{$UNDEF ANSICHAR_IS_BYTE}
{$UNDEF NO_ANSI}
{$UNDEF USE_CDECL}

{$IFDEF ENFORCE_DYNAMIC_LOADING}
  {$DEFINE USE_DYNAMIC_LOADING}
  {$DEFINE USEIPWORKSSSHDLL}
{$ENDIF}

{$IFDEF FPC}  //Lazarus
  {$IFDEF FPC_UNICODESTRINGS}
    // ensure that mode Delphi Unicode is enabled, not a modeswitch unicodestrings
    {$MODE DELPHIUNICODE}
  {$ELSE}
    {$MODE DELPHI}
  {$ENDIF}
  {$DEFINE USEIPWORKSSSHDLL}
  {$IFDEF DARWIN}
    {$DEFINE MACOS}
  {$ENDIF}
{$ENDIF}

{$IFNDEF FPC}
  {$IF CompilerVersion >= 24} // XE3+
    {$LEGACYIFEND ON}
  {$IFEND}
{$ENDIF}

{$IF DEFINED(LINUX) OR DEFINED(ANDROID) OR DEFINED(MACOS) OR DEFINED(IOS)}
  {$DEFINE USEIPWORKSSSHDLL}
  {$DEFINE ANSICHAR_IS_BYTE}
  {$DEFINE NO_ANSI}
  {$DEFINE USE_CDECL}
{$IFEND}

{$IF DEFINED(LINUX) OR DEFINED(MACOS)}
  {$IFNDEF IOS}
    {$DEFINE USE_DYNAMIC_LOADING}
  {$ENDIF}
{$IFEND}

{$IF DEFINED(ANDROID)}
  {$DEFINE USE_EXPLICIT_INIT}
{$IFEND}

{$IFDEF USE_IPWORKSSSH_ANDROID_SHARED_LIBRARY}
  {$DEFINE USE_DYNAMIC_LOADING}
{$ENDIF}


{$IF DEFINED(UNICODE) AND NOT DEFINED(MSWINDOWS)}
  {$DEFINE NONWINUNICODE}
{$IFEND}



{$IFNDEF USEIPWORKSSSHDLL}
  // in case if loading DLL from resources
  {$DEFINE USE_DYNAMIC_LOADING}
{$ENDIF}

{$UNDEF UNICODELIB}
{$IF DEFINED(MSWINDOWS)}
  {$DEFINE UNICODELIB}
{$IFEND}


{$IFDEF UNICODE}
  {$IFNDEF UNICODELIB}
    {$DEFINE UNICODE_ON_ANSI}
  {$ELSE}
    {$DEFINE UNICODE_ON_UNICODE}
  {$ENDIF}
{$ELSE}
  {$IFDEF UNICODELIB}
    {$DEFINE ANSI_ON_UNICODE}
  {$ELSE}
    {$DEFINE ANSI_ON_ANSI}
  {$ENDIF}
{$ENDIF}

interface

uses
{$IFDEF FPC}  //Lazarus
  SysUtils, Classes,
{$ELSE}
  {$IF CompilerVersion >= 23}System.SysUtils{$ELSE}SysUtils{$IFEND}, {$IF CompilerVersion >= 23}System.Classes{$ELSE}Classes{$IFEND}, 
{$ENDIF}
  {$IFDEF FPC}
  dynlibs,
  {$ELSE}
  {$IFDEF POSIX}
  Posix.Base,
  {$ENDIF}
  {$ENDIF}
  iphtypes, iphcore, iphkeys;

type

  (*** Enum Types ***)
  TiphscpFirewallTypes = iphtypes.TiphFirewallTypes;

  TiphscpSSHAuthModes = iphtypes.TiphSSHAuthModes;

  TiphscpSSHCertStoreTypes = iphtypes.TiphCertStoreTypes;

  (*** Event Delegate Types ***)
  TConnectedEvent = procedure (
    Sender: TObject;
    StatusCode: Integer;
    const Description: String
  ) of Object;


  TConnectionStatusEvent = procedure (
    Sender: TObject;
    const ConnectionEvent: String;
    StatusCode: Integer;
    const Description: String
  ) of Object;


  TDisconnectedEvent = procedure (
    Sender: TObject;
    StatusCode: Integer;
    const Description: String
  ) of Object;


  TEndTransferEvent = procedure (
    Sender: TObject;
    Direction: Integer;
    const LocalFile: String;
    const RemoteFile: String;
    const RemotePath: String
  ) of Object;


  TErrorEvent = procedure (
    Sender: TObject;
    ErrorCode: Integer;
    const Description: String;
    const LocalFile: String;
    const RemoteFile: String;
    const RemotePath: String
  ) of Object;


  TLogEvent = procedure (
    Sender: TObject;
    LogLevel: Integer;
    const Message: String;
    const LogType: String
  ) of Object;


  TSSHCustomAuthEvent = procedure (
    Sender: TObject;
    var Packet: String
  ) of Object;


  TSSHKeyboardInteractiveEvent = procedure (
    Sender: TObject;
    const Name: String;
    const Instructions: String;
    const Prompt: String;
    var Response: String;
    EchoResponse: Boolean
  ) of Object;


  TSSHServerAuthenticationEvent = procedure (
    Sender: TObject;
    HostKey: String;
    HostKeyB: TBytes;
    const Fingerprint: String;
    const KeyAlgorithm: String;
    const CertSubject: String;
    const CertIssuer: String;
    const Status: String;
    var Accept: Boolean
  ) of Object;


  TSSHStatusEvent = procedure (
    Sender: TObject;
    const Message: String
  ) of Object;


  TStartTransferEvent = procedure (
    Sender: TObject;
    Direction: Integer;
    const LocalFile: String;
    const RemoteFile: String;
    const RemotePath: String;
    var FilePermissions: String
  ) of Object;


  TTransferEvent = procedure (
    Sender: TObject;
    Direction: Integer;
    const LocalFile: String;
    const RemoteFile: String;
    const RemotePath: String;
    BytesTransferred: Int64;
    PercentDone: Integer;
    Text: String;
    TextB: TBytes
  ) of Object;


  (*** Exception Type ***)
  EiphSCP = class(EIPWorksSSH)
  end;

  (*** Component Type ***)
{$IFNDEF FPC}
  {$IF CompilerVersion >= 23}
  [ComponentPlatformsAttribute(
    pidWin32 or pidWin64
    {$IF CompilerVersion >= 29}
    or pidLinux64
    {$IFEND}
    {$IF CompilerVersion >= 31}
    or pidOSX64
    {$IFEND}
   )]
  {$IFEND}
{$ENDIF}
  TiphSCP = class(TiphTypesCore)
    private
      (*** Event Handler Procedure Pointers ***)
      FOnConnected: TConnectedEvent;

      FOnConnectionStatus: TConnectionStatusEvent;

      FOnDisconnected: TDisconnectedEvent;

      FOnEndTransfer: TEndTransferEvent;

      FOnError: TErrorEvent;

      FOnLog: TLogEvent;

      FOnSSHCustomAuth: TSSHCustomAuthEvent;

      FOnSSHKeyboardInteractive: TSSHKeyboardInteractiveEvent;

      FOnSSHServerAuthentication: TSSHServerAuthenticationEvent;

      FOnSSHStatus: TSSHStatusEvent;

      FOnStartTransfer: TStartTransferEvent;

      FOnTransfer: TTransferEvent;

      m_ctl: Pointer;
      m_streamFromSetDownloadStream : TList;

      m_streamFromSetUploadStream : TList;

      (*** Inner objects for types and collections ***)
      FFirewall: TiphFirewall;

      function HasData: Boolean;
      procedure ReadHnd(Reader: TStream);
      procedure WriteHnd(Writer: TStream);
      function BStr2CStr(pBStr: Pointer; lenBStr: Integer; bArr: TBytes): String;

      procedure SetRuntimeLicense(key: String);
      function GetRuntimeLicense: String;

    protected
      procedure AboutDlg; override;
      procedure DefineProperties(Filer: TFiler); override;

      function ThrowCoreException(Err: Integer; const Desc: {$ifndef NO_ANSI}AnsiString{$else}string{$endif}): EIPWorksSSH;
      procedure TreatErr(Err: Integer; const Desc: String);

      (*** Property Getters/Setters ***)
      function  get_Connected: Boolean;

      procedure set_Connected(valConnected: Boolean);

      function  get_FilePermissions: String;

      procedure set_FilePermissions(valFilePermissions: String);

      function  get_FirewallAutoDetect: Boolean;

      procedure set_FirewallAutoDetect(valFirewallAutoDetect: Boolean);

      function  get_FirewallType: TiphscpFirewallTypes;

      procedure set_FirewallType(valFirewallType: TiphscpFirewallTypes);

      function  get_FirewallHost: String;

      procedure set_FirewallHost(valFirewallHost: String);

      function  get_FirewallPassword: String;

      procedure set_FirewallPassword(valFirewallPassword: String);

      function  get_FirewallPort: Integer;

      procedure set_FirewallPort(valFirewallPort: Integer);

      function  get_FirewallUser: String;

      procedure set_FirewallUser(valFirewallUser: String);

      function  get_LocalFile: String;

      procedure set_LocalFile(valLocalFile: String);

      function  get_LocalHost: String;

      procedure set_LocalHost(valLocalHost: String);

      function  get_LocalPort: Integer;

      procedure set_LocalPort(valLocalPort: Integer);

      function  get_Overwrite: Boolean;

      procedure set_Overwrite(valOverwrite: Boolean);

      function  get_RemoteFile: String;

      procedure set_RemoteFile(valRemoteFile: String);

      function  get_RemotePath: String;

      procedure set_RemotePath(valRemotePath: String);

      function  get_SSHAcceptServerHostKeyEncoded: String;

      procedure set_SSHAcceptServerHostKeyEncoded(valSSHAcceptServerHostKeyEncoded: String);

      function  get_SSHAcceptServerHostKeyEncodedB: TBytes;

      procedure set_SSHAcceptServerHostKeyEncodedB(valSSHAcceptServerHostKeyEncoded: TBytes);

      function  get_SSHAuthMode: TiphscpSSHAuthModes;

      procedure set_SSHAuthMode(valSSHAuthMode: TiphscpSSHAuthModes);

      function  get_SSHCertEncoded: String;

      procedure set_SSHCertEncoded(valSSHCertEncoded: String);

      function  get_SSHCertEncodedB: TBytes;

      procedure set_SSHCertEncodedB(valSSHCertEncoded: TBytes);

      function  get_SSHCertStore: String;

      procedure set_SSHCertStore(valSSHCertStore: String);

      function  get_SSHCertStoreB: TBytes;

      procedure set_SSHCertStoreB(valSSHCertStore: TBytes);

      function  get_SSHCertStorePassword: String;

      procedure set_SSHCertStorePassword(valSSHCertStorePassword: String);

      function  get_SSHCertStoreType: TiphscpSSHCertStoreTypes;

      procedure set_SSHCertStoreType(valSSHCertStoreType: TiphscpSSHCertStoreTypes);

      function  get_SSHCertSubject: String;

      procedure set_SSHCertSubject(valSSHCertSubject: String);

      function  get_SSHCompressionAlgorithms: String;

      procedure set_SSHCompressionAlgorithms(valSSHCompressionAlgorithms: String);

      function  get_SSHEncryptionAlgorithms: String;

      procedure set_SSHEncryptionAlgorithms(valSSHEncryptionAlgorithms: String);

      function  get_SSHHost: String;

      procedure set_SSHHost(valSSHHost: String);

      function  get_SSHPassword: String;

      procedure set_SSHPassword(valSSHPassword: String);

      function  get_SSHPort: Integer;

      procedure set_SSHPort(valSSHPort: Integer);

      function  get_SSHUser: String;

      procedure set_SSHUser(valSSHUser: String);

      function  get_Timeout: Integer;

      procedure set_Timeout(valTimeout: Integer);


      (*** Property Getters/Setters: OO API ***)
      function  get_Firewall: TiphFirewall;
      procedure set_Firewall(Value : TiphFirewall);
      
 
    public
      constructor Create(AOwner: TComponent); overload; override;
      constructor Create(AOwner: TComponent; RuntimeLicense: string); reintroduce; overload;
      destructor Destroy; override;
      function ReportEventException(E: Exception; Event: String; ReFire: Boolean = True): Integer;

      property RuntimeLicense: String read GetRuntimeLicense write SetRuntimeLicense;

      procedure SetSSHAcceptServerHostKeyEncoded(lpSSHAcceptServerHostKeyEncoded: PLXAnsiChar; lenSSHAcceptServerHostKeyEncoded: Cardinal);

      procedure SetSSHCertEncoded(lpSSHCertEncoded: PLXAnsiChar; lenSSHCertEncoded: Cardinal);

      procedure SetSSHCertStore(lpSSHCertStore: PLXAnsiChar; lenSSHCertStore: Cardinal);


      (*** Runtime Property Definitions ***)

      property Connected: Boolean read get_Connected write set_Connected;



      property SSHAcceptServerHostKeyEncoded: String read get_SSHAcceptServerHostKeyEncoded write set_SSHAcceptServerHostKeyEncoded;



      property SSHAcceptServerHostKeyEncodedB: TBytes read get_SSHAcceptServerHostKeyEncodedB write set_SSHAcceptServerHostKeyEncodedB;



      property SSHCertEncoded: String read get_SSHCertEncoded write set_SSHCertEncoded;



      property SSHCertEncodedB: TBytes read get_SSHCertEncodedB write set_SSHCertEncodedB;



      property SSHCertStoreB: TBytes read get_SSHCertStoreB write set_SSHCertStoreB;



      (*** Runtime properties: OO API ***)
      property Firewall: TiphFirewall read get_Firewall write set_Firewall;


{$IFNDEF DELPHI3}
      (*** Method Definitions ***)
      function  Config(ConfigurationString: String): String;

      function  DecodePacket(EncodedPacket: String): TBytes;

      procedure DoEvents();

      procedure Download();

      function  EncodePacket(Packet: TBytes): String;

      function  GetSSHParam(Payload: TBytes; Field: String): String;

      function  GetSSHParamBytes(Payload: TBytes; Field: String): TBytes;

      procedure Interrupt();

      procedure Reset();

      procedure SetDownloadStream(DownloadStream: TStream);

      function  SetSSHParam(Payload: TBytes; FieldType: String; FieldValue: String): TBytes;

      procedure SetUploadStream(UploadStream: TStream);

      procedure SSHLogoff();

      procedure SSHLogon(SSHHost: String; SSHPort: Integer);

      procedure Upload();

{$ENDIF}

    published
      (*** Design-time Property Definitions ***)

      property FilePermissions: String read get_FilePermissions write set_FilePermissions;



      property FirewallAutoDetect: Boolean read get_FirewallAutoDetect write set_FirewallAutoDetect default False;



      property FirewallType: TiphscpFirewallTypes read get_FirewallType write set_FirewallType default fwNone;



      property FirewallHost: String read get_FirewallHost write set_FirewallHost;



      property FirewallPassword: String read get_FirewallPassword write set_FirewallPassword;



      property FirewallPort: Integer read get_FirewallPort write set_FirewallPort default 0;



      property FirewallUser: String read get_FirewallUser write set_FirewallUser;



      property LocalFile: String read get_LocalFile write set_LocalFile;



      property LocalHost: String read get_LocalHost write set_LocalHost stored False;



      property LocalPort: Integer read get_LocalPort write set_LocalPort default 0;



      property Overwrite: Boolean read get_Overwrite write set_Overwrite default false;



      property RemoteFile: String read get_RemoteFile write set_RemoteFile;



      property RemotePath: String read get_RemotePath write set_RemotePath;



      property SSHAuthMode: TiphscpSSHAuthModes read get_SSHAuthMode write set_SSHAuthMode default amPassword;



      property SSHCertStore: String read get_SSHCertStore write set_SSHCertStore;



      property SSHCertStorePassword: String read get_SSHCertStorePassword write set_SSHCertStorePassword;



      property SSHCertStoreType: TiphscpSSHCertStoreTypes read get_SSHCertStoreType write set_SSHCertStoreType default cstUser;



      property SSHCertSubject: String read get_SSHCertSubject write set_SSHCertSubject;



      property SSHCompressionAlgorithms: String read get_SSHCompressionAlgorithms write set_SSHCompressionAlgorithms;



      property SSHEncryptionAlgorithms: String read get_SSHEncryptionAlgorithms write set_SSHEncryptionAlgorithms;



      property SSHHost: String read get_SSHHost write set_SSHHost;



      property SSHPassword: String read get_SSHPassword write set_SSHPassword;



      property SSHPort: Integer read get_SSHPort write set_SSHPort default 22;



      property SSHUser: String read get_SSHUser write set_SSHUser;



      property Timeout: Integer read get_Timeout write set_Timeout default 60;


      (*** Event Handler Bindings ***)
      property OnConnected: TConnectedEvent read FOnConnected write FOnConnected;

      property OnConnectionStatus: TConnectionStatusEvent read FOnConnectionStatus write FOnConnectionStatus;

      property OnDisconnected: TDisconnectedEvent read FOnDisconnected write FOnDisconnected;

      property OnEndTransfer: TEndTransferEvent read FOnEndTransfer write FOnEndTransfer;

      property OnError: TErrorEvent read FOnError write FOnError;

      property OnLog: TLogEvent read FOnLog write FOnLog;

      property OnSSHCustomAuth: TSSHCustomAuthEvent read FOnSSHCustomAuth write FOnSSHCustomAuth;

      property OnSSHKeyboardInteractive: TSSHKeyboardInteractiveEvent read FOnSSHKeyboardInteractive write FOnSSHKeyboardInteractive;

      property OnSSHServerAuthentication: TSSHServerAuthenticationEvent read FOnSSHServerAuthentication write FOnSSHServerAuthentication;

      property OnSSHStatus: TSSHStatusEvent read FOnSSHStatus write FOnSSHStatus;

      property OnStartTransfer: TStartTransferEvent read FOnStartTransfer write FOnStartTransfer;

      property OnTransfer: TTransferEvent read FOnTransfer write FOnTransfer;

    end;

    procedure Register;

implementation


{$T-}

{$IFDEF MSWINDOWS} // Both DLL and DRU loading require Windows dependency
{$IFDEF FPC}  //Lazarus
uses Messages;
{$ELSE}
uses {$IF CompilerVersion >= 23}Winapi.Windows{$ELSE}Windows{$IFEND}, {$IF CompilerVersion >= 23}Winapi.Messages{$ELSE}Messages{$IFEND};
{$ENDIF}
{$ENDIF}

const
  PID_SCP_Connected                                          = 1;

  PID_SCP_FilePermissions                                    = 2;

  PID_SCP_FirewallAutoDetect                                 = 3;

  PID_SCP_FirewallType                                       = 4;

  PID_SCP_FirewallHost                                       = 5;

  PID_SCP_FirewallPassword                                   = 6;

  PID_SCP_FirewallPort                                       = 7;

  PID_SCP_FirewallUser                                       = 8;

  PID_SCP_LocalFile                                          = 9;

  PID_SCP_LocalHost                                          = 10;

  PID_SCP_LocalPort                                          = 11;

  PID_SCP_Overwrite                                          = 12;

  PID_SCP_RemoteFile                                         = 13;

  PID_SCP_RemotePath                                         = 14;

  PID_SCP_SSHAcceptServerHostKeyEncoded                      = 16;

  PID_SCP_SSHAuthMode                                        = 43;

  PID_SCP_SSHCertEncoded                                     = 45;

  PID_SCP_SSHCertStore                                       = 61;

  PID_SCP_SSHCertStorePassword                               = 62;

  PID_SCP_SSHCertStoreType                                   = 63;

  PID_SCP_SSHCertSubject                                     = 64;

  PID_SCP_SSHCompressionAlgorithms                           = 72;

  PID_SCP_SSHEncryptionAlgorithms                            = 73;

  PID_SCP_SSHHost                                            = 74;

  PID_SCP_SSHPassword                                        = 75;

  PID_SCP_SSHPort                                            = 76;

  PID_SCP_SSHUser                                            = 77;

  PID_SCP_Timeout                                            = 78;

  EID_SCP_Connected = 1;

  EID_SCP_ConnectionStatus = 2;

  EID_SCP_Disconnected = 3;

  EID_SCP_EndTransfer = 4;

  EID_SCP_Error = 5;

  EID_SCP_Log = 6;

  EID_SCP_SSHCustomAuth = 7;

  EID_SCP_SSHKeyboardInteractive = 8;

  EID_SCP_SSHServerAuthentication = 9;

  EID_SCP_SSHStatus = 10;

  EID_SCP_StartTransfer = 11;

  EID_SCP_Transfer = 12;

  MID_SCP_Config = 2;

  MID_SCP_DecodePacket = 3;

  MID_SCP_DoEvents = 4;

  MID_SCP_Download = 5;

  MID_SCP_EncodePacket = 6;

  MID_SCP_GetSSHParam = 7;

  MID_SCP_GetSSHParamBytes = 8;

  MID_SCP_Interrupt = 9;

  MID_SCP_Reset = 10;

  MID_SCP_SetDownloadStream = 11;

  MID_SCP_SetSSHParam = 12;

  MID_SCP_SetUploadStream = 13;

  MID_SCP_SSHLogoff = 14;

  MID_SCP_SSHLogon = 15;

  MID_SCP_Upload = 16;

  CREATE_OPT = {$IFDEF UNICODE}1{$ELSE}0{$ENDIF};
  EVTSTR_OPT = {$IFDEF UNICODE}2{$ELSE}1{$ENDIF};

  {$IFDEF MSWINDOWS}
  DLLNAME = 'IPWORKSSSH22.DLL';
  {$ENDIF}
  {$IFDEF LINUX}
  DLLNAME = 'libipworksssh.so.22.0';
  {$ENDIF}
  {$IFDEF ANDROID}
  {$IFDEF USE_DYNAMIC_LOADING}
  DLLNAME = 'libipworksssh.so.22.0';
  {$ELSE}
  DLLNAME = 'libipworksssh.22.0.a';
  {$ENDIF}
  {$ENDIF}
  {$IFDEF IOS}
  DLLNAME = 'libipworksssh.22.0.a';
  {$ELSE}
  {$IFDEF MACOS}
  DLLNAME = '@rpath/libipworksssh.22.0.dylib';
  {$ENDIF}
  {$ENDIF}

{$WARNINGS OFF}

type
  INTARR = array[0..32] of Integer;
  LPINTARR = ^INTARR;
  LPINT = ^Cardinal;
  MEventHandle = function(lpContext: TiphSCP; event_id: Integer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  PEventHandle = ^MEventHandle;


{$IFNDEF USEIPWORKSSSHDLL}
var
  pEntryPoint: Pointer;
  pBaseAddress: Pointer;
  iLoadCount: Integer;
{$ELSE}
{$IFDEF USE_DYNAMIC_LOADING}
var
  hLib : {$ifndef FPC}HMODULE{$else}TLibHandle{$endif};
  iLoadCount: Integer;
{$ENDIF USE_DYNAMIC_LOADING}
{$ENDIF USEIPWORKSSSHDLL}


{$IFDEF USE_DYNAMIC_LOADING}
  _IPWorksSSH_EvtStr:                    function(pEvtStr: Pointer; id: Integer; value: Pointer; opt: Integer): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_Stream:                    function(pStream: Pointer; op: Integer; params: LPVOIDARR; retVal: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SCP_Create:                function(pMethod: PEventHandle; pObject: TiphSCP; pKey: Pointer; opts: Integer): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SCP_Destroy:               function(p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SCP_Set:                   function(p: Pointer; index: Integer; arridx: Integer; value: Pointer; len: Cardinal): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SCP_Get:                   function(p: Pointer; index: Integer; arridx: Integer; len: LPINT; llVal: PInt64): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SCP_GetLastError:          function(p: Pointer): PLXAnsiChar; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SCP_GetLastErrorCode:      function(p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SCP_SetLastErrorAndCode:   function(p: Pointer; code: Integer; message: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SCP_GetEventError:         function(p: Pointer): PLXAnsiChar; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SCP_GetEventErrorCode:     function(p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SCP_SetEventErrorAndCode:  function(p: Pointer; code: Integer; message: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  {$IFDEF USE_EXPLICIT_INIT}
  _IPWorksSSH_SCP_StaticInit:            function(instance: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SCP_StaticDestroy:         function(): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  {$ENDIF}
  _IPWorksSSH_SCP_CheckIndex:            function(p: Pointer; index: Integer; arridx: Integer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SCP_Do:                    function(p: Pointer; method_id: Integer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR; llVal: PInt64): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
{$ELSE}
  function _IPWorksSSH_EvtStr                   (pEvtStr: Pointer; id: Integer; value: Pointer; opt: Integer): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_EvtStr' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_Stream                   (pStream: Pointer; op: Integer; params: LPVOIDARR; retVal: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_Stream' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SCP_Create               (pMethod: PEventHandle; pObject: TiphSCP; pKey: Pointer; opts: Integer): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SCP_Create' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SCP_Destroy              (p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SCP_Destroy' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SCP_Set                  (p: Pointer; index: Integer; arridx: Integer; value: Pointer; len: Cardinal): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SCP_Set' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SCP_Get                  (p: Pointer; index: Integer; arridx: Integer; len: LPINT; llVal: PInt64): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SCP_Get' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SCP_GetLastError         (p: Pointer): PLXAnsiChar; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SCP_GetLastError' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SCP_GetLastErrorCode     (p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SCP_GetLastErrorCode' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SCP_SetLastErrorAndCode  (p: Pointer; code: Integer; message: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SCP_SetLastErrorAndCode' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SCP_GetEventError        (p: Pointer): PLXAnsiChar; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SCP_GetEventError' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SCP_GetEventErrorCode    (p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SCP_GetEventErrorCode' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SCP_SetEventErrorAndCode (p: Pointer; code: Integer; message: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SCP_SetEventErrorAndCode' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SCP_StaticInit           (instance: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SCP_StaticInit' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SCP_StaticDestroy        (): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SCP_StaticDestroy' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SCP_CheckIndex           (p: Pointer; index: Integer; arridx: Integer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SCP_CheckIndex' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SCP_Do                   (p: Pointer; method_id: Integer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR; llVal: PInt64): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SCP_Do' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
{$ENDIF}

{$HINTS OFF}

(*** Fire Event Methods ***)
function FireConnected(lpContext: TiphSCP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_StatusCode: Integer;

  tmp_Description: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnConnected) then exit;

  tmp_StatusCode := Integer(params^[0]);

  tmp_Description := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  try lpContext.FOnConnected(lpContext, tmp_StatusCode, tmp_Description)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Connected'); end;

end;


function FireConnectionStatus(lpContext: TiphSCP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionEvent: String;

  tmp_StatusCode: Integer;

  tmp_Description: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnConnectionStatus) then exit;

  tmp_ConnectionEvent := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[0]);
  tmp_StatusCode := Integer(params^[1]);

  tmp_Description := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  try lpContext.FOnConnectionStatus(lpContext, tmp_ConnectionEvent, tmp_StatusCode, tmp_Description)
  except on E: Exception do result := lpContext.ReportEventException(E, 'ConnectionStatus'); end;

end;


function FireDisconnected(lpContext: TiphSCP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_StatusCode: Integer;

  tmp_Description: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnDisconnected) then exit;

  tmp_StatusCode := Integer(params^[0]);

  tmp_Description := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  try lpContext.FOnDisconnected(lpContext, tmp_StatusCode, tmp_Description)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Disconnected'); end;

end;


function FireEndTransfer(lpContext: TiphSCP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_Direction: Integer;

  tmp_LocalFile: String;

  tmp_RemoteFile: String;

  tmp_RemotePath: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnEndTransfer) then exit;

  tmp_Direction := Integer(params^[0]);

  tmp_LocalFile := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_RemoteFile := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_RemotePath := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[3]);
  try lpContext.FOnEndTransfer(lpContext, tmp_Direction, tmp_LocalFile, tmp_RemoteFile, tmp_RemotePath)
  except on E: Exception do result := lpContext.ReportEventException(E, 'EndTransfer'); end;

end;


function FireError(lpContext: TiphSCP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ErrorCode: Integer;

  tmp_Description: String;

  tmp_LocalFile: String;

  tmp_RemoteFile: String;

  tmp_RemotePath: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnError) then exit;

  tmp_ErrorCode := Integer(params^[0]);

  tmp_Description := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_LocalFile := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_RemoteFile := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[3]);
  tmp_RemotePath := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[4]);
  try lpContext.FOnError(lpContext, tmp_ErrorCode, tmp_Description, tmp_LocalFile, tmp_RemoteFile, tmp_RemotePath)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Error', False); end;

end;


function FireLog(lpContext: TiphSCP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_LogLevel: Integer;

  tmp_Message: String;

  tmp_LogType: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnLog) then exit;

  tmp_LogLevel := Integer(params^[0]);

  tmp_Message := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_LogType := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  try lpContext.FOnLog(lpContext, tmp_LogLevel, tmp_Message, tmp_LogType)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Log'); end;

end;


function FireSSHCustomAuth(lpContext: TiphSCP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_Packet: String; // dev platform string
  {$ifdef UNICODE_ON_ANSI}
  tmp_PacketB: LXAnsiString; // library string
  {$endif}

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHCustomAuth) then exit;

  tmp_Packet := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(_IPWorksSSH_EvtStr(params^[0], 0, nil, EVTSTR_OPT));
  try lpContext.FOnSSHCustomAuth(lpContext, tmp_Packet)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHCustomAuth'); end;

  {$ifdef UNICODE_ON_ANSI}
  tmp_PacketB := ToLinuxAnsiString(tmp_Packet);
  {$endif}
  _IPWorksSSH_EvtStr(params^[0], 2, {$ifdef UNICODE_ON_ANSI}@tmp_PacketB[0]{$else}PChar(tmp_Packet){$endif}, EVTSTR_OPT);
end;


function FireSSHKeyboardInteractive(lpContext: TiphSCP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_Name: String;

  tmp_Instructions: String;

  tmp_Prompt: String;

  tmp_Response: String; // dev platform string
  {$ifdef UNICODE_ON_ANSI}
  tmp_ResponseB: LXAnsiString; // library string
  {$endif}

  tmp_EchoResponse: Boolean;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHKeyboardInteractive) then exit;

  tmp_Name := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[0]);
  tmp_Instructions := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_Prompt := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_Response := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(_IPWorksSSH_EvtStr(params^[3], 0, nil, EVTSTR_OPT));
  tmp_EchoResponse := Boolean(params^[4]);

  try lpContext.FOnSSHKeyboardInteractive(lpContext, tmp_Name, tmp_Instructions, tmp_Prompt, tmp_Response, tmp_EchoResponse)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHKeyboardInteractive'); end;

  {$ifdef UNICODE_ON_ANSI}
  tmp_ResponseB := ToLinuxAnsiString(tmp_Response);
  {$endif}
  _IPWorksSSH_EvtStr(params^[3], 2, {$ifdef UNICODE_ON_ANSI}@tmp_ResponseB[0]{$else}PChar(tmp_Response){$endif}, EVTSTR_OPT);
end;


function FireSSHServerAuthentication(lpContext: TiphSCP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_HostKey: String;
  tmp_HostKeyB: TBytes;

  tmp_Fingerprint: String;

  tmp_KeyAlgorithm: String;

  tmp_CertSubject: String;

  tmp_CertIssuer: String;

  tmp_Status: String;

  tmp_Accept: Boolean;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHServerAuthentication) then exit;

  SetLength(tmp_HostKeyB, cbparam^[0]);
  Move(Pointer(params^[0])^, Pointer(tmp_HostKeyB)^, cbparam^[0]); // IMPORTANT: Do NOT multiply the length value!
  tmp_HostKey := lpContext.BStr2CStr(params^[0], cbparam^[0], tmp_HostKeyB);
  tmp_Fingerprint := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_KeyAlgorithm := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_CertSubject := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[3]);
  tmp_CertIssuer := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[4]);
  tmp_Status := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[5]);
  tmp_Accept := Boolean(params^[6]);

  try lpContext.FOnSSHServerAuthentication(lpContext, tmp_HostKey, tmp_HostKeyB, tmp_Fingerprint, tmp_KeyAlgorithm, tmp_CertSubject, tmp_CertIssuer, tmp_Status, tmp_Accept)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHServerAuthentication'); end;

  params^[6] := Pointer(tmp_Accept);

end;


function FireSSHStatus(lpContext: TiphSCP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_Message: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHStatus) then exit;

  tmp_Message := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[0]);
  try lpContext.FOnSSHStatus(lpContext, tmp_Message)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHStatus'); end;

end;


function FireStartTransfer(lpContext: TiphSCP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_Direction: Integer;

  tmp_LocalFile: String;

  tmp_RemoteFile: String;

  tmp_RemotePath: String;

  tmp_FilePermissions: String; // dev platform string
  {$ifdef UNICODE_ON_ANSI}
  tmp_FilePermissionsB: LXAnsiString; // library string
  {$endif}

begin
  result := 0;
  if not Assigned(lpContext.FOnStartTransfer) then exit;

  tmp_Direction := Integer(params^[0]);

  tmp_LocalFile := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_RemoteFile := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_RemotePath := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[3]);
  tmp_FilePermissions := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(_IPWorksSSH_EvtStr(params^[4], 0, nil, EVTSTR_OPT));
  try lpContext.FOnStartTransfer(lpContext, tmp_Direction, tmp_LocalFile, tmp_RemoteFile, tmp_RemotePath, tmp_FilePermissions)
  except on E: Exception do result := lpContext.ReportEventException(E, 'StartTransfer'); end;

  {$ifdef UNICODE_ON_ANSI}
  tmp_FilePermissionsB := ToLinuxAnsiString(tmp_FilePermissions);
  {$endif}
  _IPWorksSSH_EvtStr(params^[4], 2, {$ifdef UNICODE_ON_ANSI}@tmp_FilePermissionsB[0]{$else}PChar(tmp_FilePermissions){$endif}, EVTSTR_OPT);
end;


function FireTransfer(lpContext: TiphSCP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_Direction: Integer;

  tmp_LocalFile: String;

  tmp_RemoteFile: String;

  tmp_RemotePath: String;

  tmp_BytesTransferred: Int64;

  tmp_PercentDone: Integer;

  tmp_Text: String;
  tmp_TextB: TBytes;

begin
  result := 0;
  if not Assigned(lpContext.FOnTransfer) then exit;

  tmp_Direction := Integer(params^[0]);

  tmp_LocalFile := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_RemoteFile := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_RemotePath := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[3]);
  tmp_BytesTransferred := PInt64(params^[4])^;
  tmp_PercentDone := Integer(params^[5]);

  SetLength(tmp_TextB, cbparam^[6]);
  Move(Pointer(params^[6])^, Pointer(tmp_TextB)^, cbparam^[6]); // IMPORTANT: Do NOT multiply the length value!
  tmp_Text := lpContext.BStr2CStr(params^[6], cbparam^[6], tmp_TextB);
  try lpContext.FOnTransfer(lpContext, tmp_Direction, tmp_LocalFile, tmp_RemoteFile, tmp_RemotePath, tmp_BytesTransferred, tmp_PercentDone, tmp_Text, tmp_TextB)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Transfer'); end;

end;


function MapID2Stream(lpContext: TiphSCP; params: LPVOIDARR) : TStream;
var
  streamID : Integer;
  arrayidx : Integer;
begin
  streamID := PInteger(params^[0])^;
  if (streamID and $40000000) = 0 then
  begin
    arrayidx := (streamID and $BFFFFFFF) shr 16;
    streamID := (streamID and $0000FFFF);
    if streamID = MID_SCP_SetDownloadStream then
    begin
      if (lpContext.m_streamFromSetDownloadStream <> nil) and (arrayidx < lpContext.m_streamFromSetDownloadStream.Count) then
        result := lpContext.m_streamFromSetDownloadStream.Items[arrayidx]
      else 
        result := nil;
      exit;
    end;

    if streamID = MID_SCP_SetUploadStream then
    begin
      if (lpContext.m_streamFromSetUploadStream <> nil) and (arrayidx < lpContext.m_streamFromSetUploadStream.Count) then
        result := lpContext.m_streamFromSetUploadStream.Items[arrayidx]
      else 
        result := nil;
      exit;
    end;

  end;
  if (streamID and $40000000) = $40000000 then
  begin
    arrayidx := (streamID and $BFFFFFFF) shr 16;
    streamID := (streamID and $0000FFFF);
  
  end;
  result := nil;
end;

function FireStreamEvents_Read(lpContext: TiphSCP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_stream: TStream;
  x: Integer;
begin
  result := 0;
  try
    tmp_stream := MapID2Stream(lpContext, params);
    if tmp_stream = nil then
    begin
      result := -10000;
      exit;
    end;
    x := tmp_stream.Read(PByte(params^[1])^, cbparam^[1]);
    cbparam^[1] := x;
  except on E: Exception do
    begin
      result := -10000;
      exit;
    end;
  end;
end;

function FireStreamEvents_Write(lpContext: TiphSCP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_stream: TStream;
  x: Integer;
begin
  result := 0;
  try
    tmp_stream := MapID2Stream(lpContext, params);
    if tmp_stream = nil then
    begin
      result := -10000;
      exit;
    end;
    x := tmp_stream.Write(PByte(params^[1])^, cbparam^[1]);
    cbparam^[1] := x;
  except on E: Exception do
    begin
      result := -10000;
      exit;
    end;
  end;
end;

function FireStreamEvents_Size(lpContext: TiphSCP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_stream: TStream;
begin
  result := 0;
  try
    tmp_stream := MapID2Stream(lpContext, params);
    if tmp_stream = nil then
    begin
      result := -10000;
      exit;
    end;
    (PInt64(params^[1]))^ := tmp_stream.Size;
  except on E: Exception do
    begin
      result := -10000;
      exit;
    end;
  end;
end;

function FireStreamEvents_Close(lpContext: TiphSCP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_stream: TStream;
begin
  result := 0;
  try
    tmp_stream := MapID2Stream(lpContext, params);
    if tmp_stream = nil then
    begin
      result := -10000;
      exit;
    end;
    tmp_stream.Free;
  except on E: Exception do
    begin
      result := -10000;
      exit;
    end;
  end;
end;

function FireStreamEvents_Seek(lpContext: TiphSCP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var 
  tmp_stream: TStream;
  tmp_seeklen: Integer;
  tmp_seekpos: Integer;
  x: Int64;
begin
  result := 0;
  try
    tmp_stream := MapID2Stream(lpContext, params);
    if tmp_stream = nil then
    begin
      result := -10000;
      exit;
    end;
    tmp_seeklen := PInt64(params^[1])^;
    tmp_seekpos := PInteger(params^[2])^;
    case tmp_seekpos of
      STREAM_SEEK_FROM_END: begin x := tmp_stream.Seek(tmp_seeklen, soEnd); end;
      STREAM_SEEK_FROM_BEGIN: begin x := tmp_stream.Seek(tmp_seeklen, soBeginning); end;
      STREAM_SEEK_FROM_CURRENT: begin x := tmp_stream.Seek(tmp_seeklen, soCurrent); end;
    end;
    (PInt64(params^[1]))^ := x;
  except on E : Exception do
    begin
      result := -10000;
      exit;
    end;
  end;
end;


(*** Event Sink ***)
function FireEvents(lpContext: TiphSCP; event_id: Integer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
var
  x: Integer;
begin
  result := 0;

  case event_id of
    EID_SCP_Connected: begin result := FireConnected(lpContext, cparam, params, cbparam); end;

    EID_SCP_ConnectionStatus: begin result := FireConnectionStatus(lpContext, cparam, params, cbparam); end;

    EID_SCP_Disconnected: begin result := FireDisconnected(lpContext, cparam, params, cbparam); end;

    EID_SCP_EndTransfer: begin result := FireEndTransfer(lpContext, cparam, params, cbparam); end;

    EID_SCP_Error: begin result := FireError(lpContext, cparam, params, cbparam); end;

    EID_SCP_Log: begin result := FireLog(lpContext, cparam, params, cbparam); end;

    EID_SCP_SSHCustomAuth: begin result := FireSSHCustomAuth(lpContext, cparam, params, cbparam); end;

    EID_SCP_SSHKeyboardInteractive: begin result := FireSSHKeyboardInteractive(lpContext, cparam, params, cbparam); end;

    EID_SCP_SSHServerAuthentication: begin result := FireSSHServerAuthentication(lpContext, cparam, params, cbparam); end;

    EID_SCP_SSHStatus: begin result := FireSSHStatus(lpContext, cparam, params, cbparam); end;

    EID_SCP_StartTransfer: begin result := FireStartTransfer(lpContext, cparam, params, cbparam); end;

    EID_SCP_Transfer: begin result := FireTransfer(lpContext, cparam, params, cbparam); end;

    STREAM_OP_READ: begin result := FireStreamEvents_Read(lpContext, cparam, params, cbparam); end;
    STREAM_OP_WRITE: begin result := FireStreamEvents_Write(lpContext, cparam, params, cbparam); end;
    STREAM_OP_GET_LENGTH: begin result := FireStreamEvents_Size(lpContext, cparam, params, cbparam); end;
    STREAM_OP_CLOSE: begin result := FireStreamEvents_Close(lpContext, cparam, params, cbparam); end;
    STREAM_OP_SEEK: begin result := FireStreamEvents_Seek(lpContext, cparam, params, cbparam); end;
    99999: begin x := 0; end;
  end;
end;

procedure Register;
begin
  iphCore.Register;
  RegisterComponents('IPWorks SSH', [TiphSCP]);
end;

{*********************************************************************************}
procedure TiphSCP.SetSSHAcceptServerHostKeyEncoded(lpSSHAcceptServerHostKeyEncoded: PLXAnsiChar; lenSSHAcceptServerHostKeyEncoded: Cardinal);
var
  err: integer;
begin
  if @_IPWorksSSH_SCP_Set = nil then exit;
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_SSHAcceptServerHostKeyEncoded, 0, Pointer(lpSSHAcceptServerHostKeyEncoded), lenSSHAcceptServerHostKeyEncoded);
  if err <> 0 then TreatErr(err, '');
end;

procedure TiphSCP.SetSSHCertEncoded(lpSSHCertEncoded: PLXAnsiChar; lenSSHCertEncoded: Cardinal);
var
  err: integer;
begin
  if @_IPWorksSSH_SCP_Set = nil then exit;
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_SSHCertEncoded, 0, Pointer(lpSSHCertEncoded), lenSSHCertEncoded);
  if err <> 0 then TreatErr(err, '');
end;

procedure TiphSCP.SetSSHCertStore(lpSSHCertStore: PLXAnsiChar; lenSSHCertStore: Cardinal);
var
  err: integer;
begin
  if @_IPWorksSSH_SCP_Set = nil then exit;
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_SSHCertStore, 0, Pointer(lpSSHCertStore), lenSSHCertStore);
  if err <> 0 then TreatErr(err, '');
end;


type
  TiphFirewallFirewall = class(TiphFirewall)
  protected
    function OwnerCtl : TiphSCP;
    function GetPropAutoDetect: Boolean; override;

    procedure SetPropAutoDetect(Value: Boolean); override;

    function GetPropFirewallType: TiphFirewallTypes; override;

    procedure SetPropFirewallType(Value: TiphFirewallTypes); override;

    function GetPropHost: String; override;

    procedure SetPropHost(Value: String); override;

    function GetPropPassword: String; override;

    procedure SetPropPassword(Value: String); override;

    function GetPropPort: Integer; override;

    procedure SetPropPort(Value: Integer); override;

    function GetPropUser: String; override;

    procedure SetPropUser(Value: String); override;

  end;

function TiphFirewallFirewall.OwnerCtl : TiphSCP;
begin
  Result := TiphSCP(FOwnerCtl);
end;

function TiphFirewallFirewall.GetPropAutoDetect: Boolean;
begin
  Result := (OwnerCtl.get_FirewallAutoDetect());
end;

procedure TiphFirewallFirewall.SetPropAutoDetect(Value: Boolean);
begin
  OwnerCtl.set_FirewallAutoDetect((Value));
end;

function TiphFirewallFirewall.GetPropFirewallType: TiphFirewallTypes;
begin
  Result := TiphFirewallTypes(OwnerCtl.get_FirewallType());
end;

procedure TiphFirewallFirewall.SetPropFirewallType(Value: TiphFirewallTypes);
begin
  OwnerCtl.set_FirewallType(TiphscpFirewallTypes(Value));
end;

function TiphFirewallFirewall.GetPropHost: String;
begin
  Result := (OwnerCtl.get_FirewallHost());
end;

procedure TiphFirewallFirewall.SetPropHost(Value: String);
begin
  OwnerCtl.set_FirewallHost((Value));
end;

function TiphFirewallFirewall.GetPropPassword: String;
begin
  Result := (OwnerCtl.get_FirewallPassword());
end;

procedure TiphFirewallFirewall.SetPropPassword(Value: String);
begin
  OwnerCtl.set_FirewallPassword((Value));
end;

function TiphFirewallFirewall.GetPropPort: Integer;
begin
  Result := (OwnerCtl.get_FirewallPort());
end;

procedure TiphFirewallFirewall.SetPropPort(Value: Integer);
begin
  OwnerCtl.set_FirewallPort((Value));
end;

function TiphFirewallFirewall.GetPropUser: String;
begin
  Result := (OwnerCtl.get_FirewallUser());
end;

procedure TiphFirewallFirewall.SetPropUser(Value: String);
begin
  OwnerCtl.set_FirewallUser((Value));
end;



{*********************************************************************************}

constructor TiphSCP.Create(AOwner: TComponent);
begin
  Create(AOwner, IPWORKSSSH_OEMKEY_18);
  Config('CodePage=65001');
end;

constructor TiphSCP.Create(AOwner: TComponent; RuntimeLicense: string);
begin
  inherited Create(AOwner);
  m_ctl := nil;

  if @_IPWorksSSH_SCP_Create <> nil then
    m_ctl := _IPWorksSSH_SCP_Create(@FireEvents, self, nil, CREATE_OPT);
  if m_ctl = nil then
    TreatErr(-1, 'IPWorks SSH SCP: Error creating component');
  _IPWorksSSH_SCP_Set(m_ctl, 8002{PID_KEYCHECK_DLP}, 0, 
    {$ifdef ANSICHAR_IS_BYTE}
    Pointer(ToLinuxAnsiString(RuntimeLicense)),
    {$else}
    Pointer(PAnsiChar(AnsiString(RuntimeLicense))),
    {$endif}
    0);

  FFirewall := TiphFirewallFirewall.Create(Self, false);


  try set_FilePermissions('0600') except on E:Exception do end;

  try set_FirewallAutoDetect(false) except on E:Exception do end;

  try set_FirewallType(fwNone) except on E:Exception do end;

  try set_FirewallHost('') except on E:Exception do end;

  try set_FirewallPassword('') except on E:Exception do end;

  try set_FirewallPort(0) except on E:Exception do end;

  try set_FirewallUser('') except on E:Exception do end;

  try set_LocalFile('') except on E:Exception do end;

  try set_LocalPort(0) except on E:Exception do end;

  try set_Overwrite(false) except on E:Exception do end;

  try set_RemoteFile('') except on E:Exception do end;

  try set_RemotePath('') except on E:Exception do end;

  try set_SSHAuthMode(amPassword) except on E:Exception do end;

  try set_SSHCertStore('MY') except on E:Exception do end;

  try set_SSHCertStorePassword('') except on E:Exception do end;

  try set_SSHCertStoreType(cstUser) except on E:Exception do end;

  try set_SSHCertSubject('') except on E:Exception do end;

  try set_SSHCompressionAlgorithms('none,zlib') except on E:Exception do end;

  try set_SSHEncryptionAlgorithms('aes256-ctr,aes192-ctr,aes128-ctr,aes256-cbc,aes192-cbc,aes128-cbc,3des-ctr,3des-cbc,blowfish-cbc,arcfour256,arcfour128,arcfour,cast128-cbc,aes256-gcm@openssh.com,aes128-gcm@openssh.com,chacha20-poly1305@openssh.com') except on E:Exception do end;

  try set_SSHHost('') except on E:Exception do end;

  try set_SSHPassword('') except on E:Exception do end;

  try set_SSHPort(22) except on E:Exception do end;

  try set_SSHUser('') except on E:Exception do end;

  try set_Timeout(60) except on E:Exception do end;

end;

destructor TiphSCP.Destroy;
begin
  FreeAndNil(FFirewall);

  
  if m_streamFromSetDownloadStream <> nil then
    FreeAndNil(m_streamFromSetDownloadStream);

  if m_streamFromSetUploadStream <> nil then
    FreeAndNil(m_streamFromSetUploadStream);

  
  if m_ctl <> nil then begin
    if @_IPWorksSSH_SCP_Destroy <> nil then
      _IPWorksSSH_SCP_Destroy(m_ctl);
  end;
  m_ctl := nil;
  inherited Destroy;
end;

function TiphSCP.ThrowCoreException(err: Integer; const desc: {$ifndef NO_ANSI}AnsiString{$else}string{$endif}): EIPWorksSSH;
begin
  result := EiphSCP.CreateCode(err, desc);
end;

function TiphSCP.ReportEventException(e: Exception; Event: String; ReFire: Boolean): Integer;
var
  msg: String;
begin
  result := -1;
  msg := 'An unhandled error occurred in the ' + Event + ' event handler: ' + E.Message;
  if (m_ctl <> nil) and (@_IPWorksSSH_SCP_SetEventErrorAndCode <> nil) then begin
    if E is EIPWorksSSH then begin
      result := EIPWorksSSH(e).Code;
      msg := E.Message;
    end;
    _IPWorksSSH_SCP_SetEventErrorAndCode(m_ctl, result, 
      {$ifdef ANSICHAR_IS_BYTE}
      Pointer(ToLinuxAnsiString(msg)));
      {$else}
      Pointer(PAnsiChar(AnsiString(msg))));
      {$endif}
  end;
  if ReFire and Assigned(FOnError) then FOnError(self, result, msg, '', '', '');
end;

{*********************************************************************************}

procedure TiphSCP.AboutDlg;
var
  p : LPVOIDARR;
  pc: LPINTARR;
begin
{$IFDEF LINUX}
  inherited AboutDlg;
{$ELSE}
  p := nil;
  pc := nil;
  if @_IPWorksSSH_SCP_Do <> nil then
    _IPWorksSSH_SCP_Do(m_ctl, 255, 0, p, pc, nil);
{$ENDIF}
end;

{*********************************************************************************}

procedure TiphSCP.SetRuntimeLicense(key: String);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SCP_Set = nil then exit;
  err := _IPWorksSSH_SCP_Set(m_ctl, 8001{PID_RUNTIME_LICENSE}, 0, 
    {$ifdef ANSICHAR_IS_BYTE}
    Pointer(ToLinuxAnsiString(key)),
    {$else}
    Pointer(PAnsiChar(AnsiString(key))),
    {$endif}
    0);

  if err <> 0 then TreatErr(err, '');
end;

function TiphSCP.GetRuntimeLicense: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';
  if @_IPWorksSSH_SCP_Get = nil then exit;
  tmp := _IPWorksSSH_SCP_Get(m_ctl, 8001{PID_RUNTIME_LICENSE}, 0, nil, nil);
  err := _IPWorksSSH_SCP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$ifdef ANSICHAR_IS_BYTE}
  result := FromLinuxAnsiString(PLXAnsiChar(tmp));
  {$else}
  result := PAnsiChar(tmp);
  {$endif}
end;

{*********************************************************************************}

function TiphSCP.HasData: Boolean;
begin
  result := false;
end;

procedure TiphSCP.ReadHnd(Reader: TStream);
begin
end;

procedure TiphSCP.WriteHnd(Writer: TStream);
begin
end;

function TiphSCP.BStr2CStr(pBStr: Pointer; lenBStr: Integer; bArr: TBytes): String;
{$IF DEFINED(UNICODE) AND DEFINED(MSWINDOWS)}
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
{$IFEND}
begin
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  param[0] := pBStr; paramcb[0] := lenBStr;
  param[1] := nil;   paramcb[1] := 0;
  param[2] := nil;   paramcb[2] := 0;
  _IPWorksSSH_SCP_Do(m_ctl, 2003, 2, @param, @paramcb, nil);
  SetLength(Result, Integer(param[2]));
  if Length(Result) > 0 then begin
    param[1] := PChar(Result); paramcb[1] := Length(Result);
    _IPWorksSSH_SCP_Do(m_ctl, 2003, 2, @param, @paramcb, nil);
  end;
  {$ELSE}
  try
    Result := TEncoding.Default.GetString(bArr);
  except
    on E: Exception do
      SetString(Result, PAnsiChar(pBStr), lenBStr);
   end;
  {$ENDIF}
{$ELSE}
  SetString(Result, PAnsiChar(pBStr), lenBStr);
{$ENDIF}
end;

procedure TiphSCP.DefineProperties(Filer: TFiler);
begin
  inherited DefineProperties(Filer);
  Filer.DefineBinaryProperty('RegHnd', ReadHnd, WriteHnd, HasData);
end;

{*********************************************************************************}

procedure TiphSCP.TreatErr(Err: Integer; const desc: String);
var
  msg : {$ifdef NO_ANSI}string{$else}LXAnsiString{$endif};
begin
  {$ifdef NO_ANSI}
  msg := desc;
  {$else}
  {$ifdef ANSICHAR_IS_BYTE}
  msg := ToLinuxAnsiString(desc);
  {$else}
  msg := LXAnsiString(desc);
  {$endif}
  {$endif}
  if Length(msg) = 0 then
  begin
    if @_IPWorksSSH_SCP_GetLastError <> nil then
      {$ifdef NO_ANSI}
      {$ifdef ANSICHAR_IS_BYTE}
      msg := FromLinuxAnsiString(_IPWorksSSH_SCP_GetLastError(m_ctl));
      {$else}
      msg := _IPWorksSSH_SCP_GetLastError(m_ctl);
      {$endif}
      {$else}
      msg := _IPWorksSSH_SCP_GetLastError(m_ctl);
      {$endif}
  end;

  raise ThrowCoreException(err, msg);
end;

{*********************************************************************************}

(*** Property Getters/Setters ***)
function TiphSCP.get_Connected: Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);

  if @_IPWorksSSH_SCP_Get = nil then exit;
  tmp := _IPWorksSSH_SCP_Get(m_ctl, PID_SCP_Connected, 0, nil, nil);
  err := _IPWorksSSH_SCP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


procedure TiphSCP.set_Connected(valConnected: Boolean);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SCP_Set = nil then exit;
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_Connected, 0, Pointer(valConnected), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSCP.get_FilePermissions: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SCP_Get = nil then exit;
  tmp := _IPWorksSSH_SCP_Get(m_ctl, PID_SCP_FilePermissions{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SCP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSCP.set_FilePermissions(valFilePermissions: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SCP_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valFilePermissions);
  {$ENDIF}
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_FilePermissions{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valFilePermissions){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSCP.get_FirewallAutoDetect: Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);

  if @_IPWorksSSH_SCP_Get = nil then exit;
  tmp := _IPWorksSSH_SCP_Get(m_ctl, PID_SCP_FirewallAutoDetect, 0, nil, nil);
  err := _IPWorksSSH_SCP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


procedure TiphSCP.set_FirewallAutoDetect(valFirewallAutoDetect: Boolean);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SCP_Set = nil then exit;
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_FirewallAutoDetect, 0, Pointer(valFirewallAutoDetect), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSCP.get_FirewallType: TiphscpFirewallTypes;
var
  tmp: Pointer;
  err: Integer;
begin
  result := TiphscpFirewallTypes(0);

  if @_IPWorksSSH_SCP_Get = nil then exit;
  tmp := _IPWorksSSH_SCP_Get(m_ctl, PID_SCP_FirewallType, 0, nil, nil);
  err := _IPWorksSSH_SCP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := TiphscpFirewallTypes(tmp);
end;


procedure TiphSCP.set_FirewallType(valFirewallType: TiphscpFirewallTypes);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SCP_Set = nil then exit;
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_FirewallType, 0, Pointer(valFirewallType), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSCP.get_FirewallHost: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SCP_Get = nil then exit;
  tmp := _IPWorksSSH_SCP_Get(m_ctl, PID_SCP_FirewallHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SCP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSCP.set_FirewallHost(valFirewallHost: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SCP_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valFirewallHost);
  {$ENDIF}
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_FirewallHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valFirewallHost){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSCP.get_FirewallPassword: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SCP_Get = nil then exit;
  tmp := _IPWorksSSH_SCP_Get(m_ctl, PID_SCP_FirewallPassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SCP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSCP.set_FirewallPassword(valFirewallPassword: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SCP_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valFirewallPassword);
  {$ENDIF}
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_FirewallPassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valFirewallPassword){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSCP.get_FirewallPort: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SCP_Get = nil then exit;
  tmp := _IPWorksSSH_SCP_Get(m_ctl, PID_SCP_FirewallPort, 0, nil, nil);
  err := _IPWorksSSH_SCP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSCP.set_FirewallPort(valFirewallPort: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SCP_Set = nil then exit;
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_FirewallPort, 0, Pointer(valFirewallPort), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSCP.get_FirewallUser: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SCP_Get = nil then exit;
  tmp := _IPWorksSSH_SCP_Get(m_ctl, PID_SCP_FirewallUser{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SCP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSCP.set_FirewallUser(valFirewallUser: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SCP_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valFirewallUser);
  {$ENDIF}
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_FirewallUser{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valFirewallUser){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSCP.get_LocalFile: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SCP_Get = nil then exit;
  tmp := _IPWorksSSH_SCP_Get(m_ctl, PID_SCP_LocalFile{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SCP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSCP.set_LocalFile(valLocalFile: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SCP_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valLocalFile);
  {$ENDIF}
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_LocalFile{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valLocalFile){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSCP.get_LocalHost: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SCP_Get = nil then exit;
  tmp := _IPWorksSSH_SCP_Get(m_ctl, PID_SCP_LocalHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SCP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSCP.set_LocalHost(valLocalHost: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SCP_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valLocalHost);
  {$ENDIF}
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_LocalHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valLocalHost){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSCP.get_LocalPort: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SCP_Get = nil then exit;
  tmp := _IPWorksSSH_SCP_Get(m_ctl, PID_SCP_LocalPort, 0, nil, nil);
  err := _IPWorksSSH_SCP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSCP.set_LocalPort(valLocalPort: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SCP_Set = nil then exit;
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_LocalPort, 0, Pointer(valLocalPort), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSCP.get_Overwrite: Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);

  if @_IPWorksSSH_SCP_Get = nil then exit;
  tmp := _IPWorksSSH_SCP_Get(m_ctl, PID_SCP_Overwrite, 0, nil, nil);
  err := _IPWorksSSH_SCP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


procedure TiphSCP.set_Overwrite(valOverwrite: Boolean);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SCP_Set = nil then exit;
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_Overwrite, 0, Pointer(valOverwrite), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSCP.get_RemoteFile: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SCP_Get = nil then exit;
  tmp := _IPWorksSSH_SCP_Get(m_ctl, PID_SCP_RemoteFile{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SCP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSCP.set_RemoteFile(valRemoteFile: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SCP_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valRemoteFile);
  {$ENDIF}
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_RemoteFile{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valRemoteFile){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSCP.get_RemotePath: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SCP_Get = nil then exit;
  tmp := _IPWorksSSH_SCP_Get(m_ctl, PID_SCP_RemotePath{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SCP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSCP.set_RemotePath(valRemotePath: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SCP_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valRemotePath);
  {$ENDIF}
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_RemotePath{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valRemotePath){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSCP.get_SSHAcceptServerHostKeyEncoded: String;
var
  tmp: {$IFDEF NONWINUNICODE}TBytes{$ELSE}Pointer; len: Cardinal{$ENDIF};
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SCP_Get = nil then exit;
  tmp := {$IFDEF NONWINUNICODE}get_SSHAcceptServerHostKeyEncodedB{$ELSE}_IPWorksSSH_SCP_Get(m_ctl, PID_SCP_SSHAcceptServerHostKeyEncoded{$IFDEF UNICODE_ON_UNICODE}+20000{$ENDIF}, 0, @len, nil){$ENDIF};
  err := _IPWorksSSH_SCP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$IFDEF NONWINUNICODE}result := TEncoding.UTF8.GetString(tmp);{$ELSE}SetString(result, PChar(tmp), len);{$ENDIF}
end;


procedure TiphSCP.set_SSHAcceptServerHostKeyEncoded(valSSHAcceptServerHostKeyEncoded: String);
var
  err: Integer;
  {$IFDEF NONWINUNICODE}tmp: TBytes;{$ENDIF}
begin
  err := 0;
  if @_IPWorksSSH_SCP_Set = nil then exit;
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_SSHAcceptServerHostKeyEncoded+20000, 0, Pointer(PWideChar(valSSHAcceptServerHostKeyEncoded)), Length(valSSHAcceptServerHostKeyEncoded));
  {$ELSE}
  tmp := TEncoding.UTF8.GetBytes(valSSHAcceptServerHostKeyEncoded);
  set_SSHAcceptServerHostKeyEncodedB(tmp);
  {$ENDIF}
{$ELSE}
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_SSHAcceptServerHostKeyEncoded, 0, Pointer(PAnsiChar(valSSHAcceptServerHostKeyEncoded)), Length(valSSHAcceptServerHostKeyEncoded));
{$ENDIF}
  if err <> 0 then TreatErr(err, '');
end;


function TiphSCP.get_SSHAcceptServerHostKeyEncodedB: TBytes;
var
  tmp: Pointer;
  len: Cardinal;
begin
  result := nil;
  if @_IPWorksSSH_SCP_Get = nil then exit;
  tmp := _IPWorksSSH_SCP_Get(m_ctl, PID_SCP_SSHAcceptServerHostKeyEncoded, 0, @len, nil);
  SetLength(result, len);
  Move(Pointer(tmp)^, Pointer(result)^, len); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSCP.set_SSHAcceptServerHostKeyEncodedB(valSSHAcceptServerHostKeyEncoded: TBytes);
var
  err: Integer;
begin
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_SSHAcceptServerHostKeyEncoded, 0, Pointer(valSSHAcceptServerHostKeyEncoded), Length(valSSHAcceptServerHostKeyEncoded));
  if err <> 0 then TreatErr(err, '');
end;


function TiphSCP.get_SSHAuthMode: TiphscpSSHAuthModes;
var
  tmp: Pointer;
  err: Integer;
begin
  result := TiphscpSSHAuthModes(0);

  if @_IPWorksSSH_SCP_Get = nil then exit;
  tmp := _IPWorksSSH_SCP_Get(m_ctl, PID_SCP_SSHAuthMode, 0, nil, nil);
  err := _IPWorksSSH_SCP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := TiphscpSSHAuthModes(tmp);
end;


procedure TiphSCP.set_SSHAuthMode(valSSHAuthMode: TiphscpSSHAuthModes);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SCP_Set = nil then exit;
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_SSHAuthMode, 0, Pointer(valSSHAuthMode), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSCP.get_SSHCertEncoded: String;
var
  tmp: {$IFDEF NONWINUNICODE}TBytes{$ELSE}Pointer; len: Cardinal{$ENDIF};
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SCP_Get = nil then exit;
  tmp := {$IFDEF NONWINUNICODE}get_SSHCertEncodedB{$ELSE}_IPWorksSSH_SCP_Get(m_ctl, PID_SCP_SSHCertEncoded{$IFDEF UNICODE_ON_UNICODE}+20000{$ENDIF}, 0, @len, nil){$ENDIF};
  err := _IPWorksSSH_SCP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$IFDEF NONWINUNICODE}result := TEncoding.UTF8.GetString(tmp);{$ELSE}SetString(result, PChar(tmp), len);{$ENDIF}
end;


procedure TiphSCP.set_SSHCertEncoded(valSSHCertEncoded: String);
var
  err: Integer;
  {$IFDEF NONWINUNICODE}tmp: TBytes;{$ENDIF}
begin
  err := 0;
  if @_IPWorksSSH_SCP_Set = nil then exit;
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_SSHCertEncoded+20000, 0, Pointer(PWideChar(valSSHCertEncoded)), Length(valSSHCertEncoded));
  {$ELSE}
  tmp := TEncoding.UTF8.GetBytes(valSSHCertEncoded);
  set_SSHCertEncodedB(tmp);
  {$ENDIF}
{$ELSE}
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_SSHCertEncoded, 0, Pointer(PAnsiChar(valSSHCertEncoded)), Length(valSSHCertEncoded));
{$ENDIF}
  if err <> 0 then TreatErr(err, '');
end;


function TiphSCP.get_SSHCertEncodedB: TBytes;
var
  tmp: Pointer;
  len: Cardinal;
begin
  result := nil;
  if @_IPWorksSSH_SCP_Get = nil then exit;
  tmp := _IPWorksSSH_SCP_Get(m_ctl, PID_SCP_SSHCertEncoded, 0, @len, nil);
  SetLength(result, len);
  Move(Pointer(tmp)^, Pointer(result)^, len); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSCP.set_SSHCertEncodedB(valSSHCertEncoded: TBytes);
var
  err: Integer;
begin
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_SSHCertEncoded, 0, Pointer(valSSHCertEncoded), Length(valSSHCertEncoded));
  if err <> 0 then TreatErr(err, '');
end;


function TiphSCP.get_SSHCertStore: String;
var
  tmp: {$IFDEF NONWINUNICODE}TBytes{$ELSE}Pointer; len: Cardinal{$ENDIF};
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SCP_Get = nil then exit;
  tmp := {$IFDEF NONWINUNICODE}get_SSHCertStoreB{$ELSE}_IPWorksSSH_SCP_Get(m_ctl, PID_SCP_SSHCertStore{$IFDEF UNICODE_ON_UNICODE}+20000{$ENDIF}, 0, @len, nil){$ENDIF};
  err := _IPWorksSSH_SCP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$IFDEF NONWINUNICODE}result := TEncoding.UTF8.GetString(tmp);{$ELSE}SetString(result, PChar(tmp), len);{$ENDIF}
end;


procedure TiphSCP.set_SSHCertStore(valSSHCertStore: String);
var
  err: Integer;
  {$IFDEF NONWINUNICODE}tmp: TBytes;{$ENDIF}
begin
  err := 0;
  if @_IPWorksSSH_SCP_Set = nil then exit;
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_SSHCertStore+20000, 0, Pointer(PWideChar(valSSHCertStore)), Length(valSSHCertStore));
  {$ELSE}
  tmp := TEncoding.UTF8.GetBytes(valSSHCertStore);
  set_SSHCertStoreB(tmp);
  {$ENDIF}
{$ELSE}
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_SSHCertStore, 0, Pointer(PAnsiChar(valSSHCertStore)), Length(valSSHCertStore));
{$ENDIF}
  if err <> 0 then TreatErr(err, '');
end;


function TiphSCP.get_SSHCertStoreB: TBytes;
var
  tmp: Pointer;
  len: Cardinal;
begin
  result := nil;
  if @_IPWorksSSH_SCP_Get = nil then exit;
  tmp := _IPWorksSSH_SCP_Get(m_ctl, PID_SCP_SSHCertStore, 0, @len, nil);
  SetLength(result, len);
  Move(Pointer(tmp)^, Pointer(result)^, len); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSCP.set_SSHCertStoreB(valSSHCertStore: TBytes);
var
  err: Integer;
begin
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_SSHCertStore, 0, Pointer(valSSHCertStore), Length(valSSHCertStore));
  if err <> 0 then TreatErr(err, '');
end;


function TiphSCP.get_SSHCertStorePassword: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SCP_Get = nil then exit;
  tmp := _IPWorksSSH_SCP_Get(m_ctl, PID_SCP_SSHCertStorePassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SCP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSCP.set_SSHCertStorePassword(valSSHCertStorePassword: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SCP_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHCertStorePassword);
  {$ENDIF}
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_SSHCertStorePassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHCertStorePassword){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSCP.get_SSHCertStoreType: TiphscpSSHCertStoreTypes;
var
  tmp: Pointer;
  err: Integer;
begin
  result := TiphscpSSHCertStoreTypes(0);

  if @_IPWorksSSH_SCP_Get = nil then exit;
  tmp := _IPWorksSSH_SCP_Get(m_ctl, PID_SCP_SSHCertStoreType, 0, nil, nil);
  err := _IPWorksSSH_SCP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := TiphscpSSHCertStoreTypes(tmp);
end;


procedure TiphSCP.set_SSHCertStoreType(valSSHCertStoreType: TiphscpSSHCertStoreTypes);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SCP_Set = nil then exit;
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_SSHCertStoreType, 0, Pointer(valSSHCertStoreType), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSCP.get_SSHCertSubject: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SCP_Get = nil then exit;
  tmp := _IPWorksSSH_SCP_Get(m_ctl, PID_SCP_SSHCertSubject{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SCP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSCP.set_SSHCertSubject(valSSHCertSubject: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SCP_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHCertSubject);
  {$ENDIF}
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_SSHCertSubject{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHCertSubject){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSCP.get_SSHCompressionAlgorithms: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SCP_Get = nil then exit;
  tmp := _IPWorksSSH_SCP_Get(m_ctl, PID_SCP_SSHCompressionAlgorithms{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SCP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSCP.set_SSHCompressionAlgorithms(valSSHCompressionAlgorithms: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SCP_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHCompressionAlgorithms);
  {$ENDIF}
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_SSHCompressionAlgorithms{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHCompressionAlgorithms){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSCP.get_SSHEncryptionAlgorithms: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SCP_Get = nil then exit;
  tmp := _IPWorksSSH_SCP_Get(m_ctl, PID_SCP_SSHEncryptionAlgorithms{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SCP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSCP.set_SSHEncryptionAlgorithms(valSSHEncryptionAlgorithms: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SCP_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHEncryptionAlgorithms);
  {$ENDIF}
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_SSHEncryptionAlgorithms{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHEncryptionAlgorithms){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSCP.get_SSHHost: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SCP_Get = nil then exit;
  tmp := _IPWorksSSH_SCP_Get(m_ctl, PID_SCP_SSHHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SCP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSCP.set_SSHHost(valSSHHost: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SCP_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHHost);
  {$ENDIF}
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_SSHHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHHost){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSCP.get_SSHPassword: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SCP_Get = nil then exit;
  tmp := _IPWorksSSH_SCP_Get(m_ctl, PID_SCP_SSHPassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SCP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSCP.set_SSHPassword(valSSHPassword: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SCP_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHPassword);
  {$ENDIF}
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_SSHPassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHPassword){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSCP.get_SSHPort: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SCP_Get = nil then exit;
  tmp := _IPWorksSSH_SCP_Get(m_ctl, PID_SCP_SSHPort, 0, nil, nil);
  err := _IPWorksSSH_SCP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSCP.set_SSHPort(valSSHPort: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SCP_Set = nil then exit;
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_SSHPort, 0, Pointer(valSSHPort), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSCP.get_SSHUser: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SCP_Get = nil then exit;
  tmp := _IPWorksSSH_SCP_Get(m_ctl, PID_SCP_SSHUser{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SCP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSCP.set_SSHUser(valSSHUser: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SCP_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHUser);
  {$ENDIF}
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_SSHUser{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHUser){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSCP.get_Timeout: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SCP_Get = nil then exit;
  tmp := _IPWorksSSH_SCP_Get(m_ctl, PID_SCP_Timeout, 0, nil, nil);
  err := _IPWorksSSH_SCP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSCP.set_Timeout(valTimeout: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SCP_Set = nil then exit;
  err := _IPWorksSSH_SCP_Set(m_ctl, PID_SCP_Timeout, 0, Pointer(valTimeout), 0);
  if err <> 0 then TreatErr(err, '');
end;



{*********************************************************************************}

(*** Property Getters/Setters: OO API ***)
function  TiphSCP.get_Firewall: TiphFirewall;
begin
  Result := FFirewall;
end;

procedure TiphSCP.set_Firewall(Value : TiphFirewall);
begin
  FFirewall.Assign(Value);
end;


{*********************************************************************************}

{$IFNDEF DELPHI3}
(*** Methods ***)
function TiphSCP.Config(ConfigurationString: String): String;
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
  tmp_ConfigurationString: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := '';

  tmp_ConfigurationString := {$ifndef UNICODE_ON_ANSI}ConfigurationString{$else}ToLinuxAnsiString(ConfigurationString){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_ConfigurationString){$else}@tmp_ConfigurationString[0]{$endif};

  if @_IPWorksSSH_SCP_Do = nil then exit;
  err := _IPWorksSSH_SCP_Do(m_ctl, MID_SCP_Config{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  result := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(param[1]);
end;


function TiphSCP.DecodePacket(EncodedPacket: String): TBytes;
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
  tmp_EncodedPacket: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := nil;

  tmp_EncodedPacket := {$ifndef UNICODE_ON_ANSI}EncodedPacket{$else}ToLinuxAnsiString(EncodedPacket){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_EncodedPacket){$else}@tmp_EncodedPacket[0]{$endif};

  if @_IPWorksSSH_SCP_Do = nil then exit;
  err := _IPWorksSSH_SCP_Do(m_ctl, MID_SCP_DecodePacket{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  SetLength(result, paramcb[1]);
  Move(Pointer(param[1])^, Pointer(result)^, paramcb[1]); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSCP.DoEvents();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SCP_Do = nil then exit;
  err := _IPWorksSSH_SCP_Do(m_ctl, MID_SCP_DoEvents{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSCP.Download();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SCP_Do = nil then exit;
  err := _IPWorksSSH_SCP_Do(m_ctl, MID_SCP_Download{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


function TiphSCP.EncodePacket(Packet: TBytes): String;
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := '';

  param[0] := Pointer(Packet);
  paramcb[0] := Length(Packet);

  if @_IPWorksSSH_SCP_Do = nil then exit;
  err := _IPWorksSSH_SCP_Do(m_ctl, MID_SCP_EncodePacket{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  result := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(param[1]);
end;


function TiphSCP.GetSSHParam(Payload: TBytes; Field: String): String;
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
  i, err: Integer;
  tmp_Field: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 2 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := '';

  param[0] := Pointer(Payload);
  paramcb[0] := Length(Payload);

  tmp_Field := {$ifndef UNICODE_ON_ANSI}Field{$else}ToLinuxAnsiString(Field){$endif};
  param[1] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_Field){$else}@tmp_Field[0]{$endif};

  if @_IPWorksSSH_SCP_Do = nil then exit;
  err := _IPWorksSSH_SCP_Do(m_ctl, MID_SCP_GetSSHParam{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 2, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  result := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(param[2]);
end;


function TiphSCP.GetSSHParamBytes(Payload: TBytes; Field: String): TBytes;
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
  i, err: Integer;
  tmp_Field: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 2 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := nil;

  param[0] := Pointer(Payload);
  paramcb[0] := Length(Payload);

  tmp_Field := {$ifndef UNICODE_ON_ANSI}Field{$else}ToLinuxAnsiString(Field){$endif};
  param[1] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_Field){$else}@tmp_Field[0]{$endif};

  if @_IPWorksSSH_SCP_Do = nil then exit;
  err := _IPWorksSSH_SCP_Do(m_ctl, MID_SCP_GetSSHParamBytes{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 2, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  SetLength(result, paramcb[2]);
  Move(Pointer(param[2])^, Pointer(result)^, paramcb[2]); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSCP.Interrupt();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SCP_Do = nil then exit;
  err := _IPWorksSSH_SCP_Do(m_ctl, MID_SCP_Interrupt{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSCP.Reset();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SCP_Do = nil then exit;
  err := _IPWorksSSH_SCP_Do(m_ctl, MID_SCP_Reset{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSCP.SetDownloadStream(DownloadStream: TStream);
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
  streamIndex: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  if m_streamFromSetDownloadStream = nil then 
    m_streamFromSetDownloadStream := TList.Create;
  streamIndex := 0;
  
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if DownloadStream <> nil then param[0] := Pointer(1);
  if m_streamFromSetDownloadStream.Count <= streamIndex then m_streamFromSetDownloadStream.Count := streamIndex + 1;
  m_streamFromSetDownloadStream[streamIndex] := DownloadStream;
  Inc(streamIndex);

  if @_IPWorksSSH_SCP_Do = nil then exit;
  err := _IPWorksSSH_SCP_Do(m_ctl, MID_SCP_SetDownloadStream{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


function TiphSCP.SetSSHParam(Payload: TBytes; FieldType: String; FieldValue: String): TBytes;
var
  param: array[0..3] of Pointer;
  paramcb: array[0..3] of Integer;
  i, err: Integer;
  tmp_FieldType: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
  tmp_FieldValue: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 3 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := nil;

  param[0] := Pointer(Payload);
  paramcb[0] := Length(Payload);

  tmp_FieldType := {$ifndef UNICODE_ON_ANSI}FieldType{$else}ToLinuxAnsiString(FieldType){$endif};
  param[1] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_FieldType){$else}@tmp_FieldType[0]{$endif};

  tmp_FieldValue := {$ifndef UNICODE_ON_ANSI}FieldValue{$else}ToLinuxAnsiString(FieldValue){$endif};
  param[2] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_FieldValue){$else}@tmp_FieldValue[0]{$endif};

  if @_IPWorksSSH_SCP_Do = nil then exit;
  err := _IPWorksSSH_SCP_Do(m_ctl, MID_SCP_SetSSHParam{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 3, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  SetLength(result, paramcb[3]);
  Move(Pointer(param[3])^, Pointer(result)^, paramcb[3]); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSCP.SetUploadStream(UploadStream: TStream);
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
  streamIndex: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  if m_streamFromSetUploadStream = nil then 
    m_streamFromSetUploadStream := TList.Create;
  streamIndex := 0;
  
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if UploadStream <> nil then param[0] := Pointer(1);
  if m_streamFromSetUploadStream.Count <= streamIndex then m_streamFromSetUploadStream.Count := streamIndex + 1;
  m_streamFromSetUploadStream[streamIndex] := UploadStream;
  Inc(streamIndex);

  if @_IPWorksSSH_SCP_Do = nil then exit;
  err := _IPWorksSSH_SCP_Do(m_ctl, MID_SCP_SetUploadStream{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSCP.SSHLogoff();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SCP_Do = nil then exit;
  err := _IPWorksSSH_SCP_Do(m_ctl, MID_SCP_SSHLogoff{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSCP.SSHLogon(SSHHost: String; SSHPort: Integer);
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
  i, err: Integer;
  tmp_SSHHost: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 2 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  tmp_SSHHost := {$ifndef UNICODE_ON_ANSI}SSHHost{$else}ToLinuxAnsiString(SSHHost){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_SSHHost){$else}@tmp_SSHHost[0]{$endif};

  param[1] := Pointer(SSHPort);


  if @_IPWorksSSH_SCP_Do = nil then exit;
  err := _IPWorksSSH_SCP_Do(m_ctl, MID_SCP_SSHLogon{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 2, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSCP.Upload();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SCP_Do = nil then exit;
  err := _IPWorksSSH_SCP_Do(m_ctl, MID_SCP_Upload{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


{$ENDIF}


{$IFDEF USE_DYNAMIC_LOADING}

procedure ClearProcs();
begin
  _IPWorksSSH_EvtStr := nil;
  _IPWorksSSH_Stream := nil;
  _IPWorksSSH_SCP_Create := nil;
  _IPWorksSSH_SCP_Destroy := nil;
  _IPWorksSSH_SCP_Set := nil;
  _IPWorksSSH_SCP_Get := nil;
  _IPWorksSSH_SCP_GetLastError := nil;
  _IPWorksSSH_SCP_GetLastErrorCode := nil;
  _IPWorksSSH_SCP_SetLastErrorAndCode := nil;
  _IPWorksSSH_SCP_GetEventError := nil;
  _IPWorksSSH_SCP_GetEventErrorCode := nil;
  _IPWorksSSH_SCP_SetEventErrorAndCode := nil;
  {$IFDEF USE_EXPLICIT_INIT}
  _IPWorksSSH_SCP_StaticInit := nil;
  _IPWorksSSH_SCP_StaticDestroy := nil;
  {$ENDIF}
  _IPWorksSSH_SCP_CheckIndex := nil;
  _IPWorksSSH_SCP_Do := nil;
end;

{$IFDEF USEIPWORKSSSHDLL}
procedure LoadDLL();
begin
  ClearProcs();

  hLib := LoadLibrary(PChar(DLLNAME));
  if hLib = 0 then exit;

  @_IPWorksSSH_EvtStr                   := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_EvtStr');
  @_IPWorksSSH_Stream                   := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_Stream');
  @_IPWorksSSH_SCP_Create               := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SCP_Create');
  @_IPWorksSSH_SCP_Destroy              := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SCP_Destroy');
  @_IPWorksSSH_SCP_Set                  := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SCP_Set');
  @_IPWorksSSH_SCP_Get                  := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SCP_Get');
  @_IPWorksSSH_SCP_GetLastError         := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SCP_GetLastError');
  @_IPWorksSSH_SCP_GetLastErrorCode     := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SCP_GetLastErrorCode');
  @_IPWorksSSH_SCP_SetLastErrorAndCode  := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SCP_SetLastErrorAndCode');
  @_IPWorksSSH_SCP_GetEventError        := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SCP_GetEventError');
  @_IPWorksSSH_SCP_GetEventErrorCode    := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SCP_GetEventErrorCode');
  @_IPWorksSSH_SCP_SetEventErrorAndCode := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SCP_SetEventErrorAndCode');
  {$IFDEF USE_EXPLICIT_INIT}
  @_IPWorksSSH_SCP_StaticInit           := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SCP_StaticInit');
  @_IPWorksSSH_SCP_StaticDestroy        := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SCP_StaticDestroy');
  {$ENDIF}
  @_IPWorksSSH_SCP_CheckIndex           := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SCP_CheckIndex');
  @_IPWorksSSH_SCP_Do                   := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SCP_Do');
end;
{$ELSE}
procedure LoadRESDLL();
var
  hResInfo: HRSRC;
  hResData: HGLOBAL;
  pResData: Pointer;
begin
  ClearProcs();

  hResInfo := FindResource(HInstance, 'ipworksssh22_dta', RT_RCDATA);
  if hResInfo = 0 then exit;

  hResData := LoadResource(HInstance, hResInfo);
  if hResData = 0 then exit;

  pResData := LockResource(hResData);
  if pResData = nil then exit;

  pBaseAddress := IPWorksSSHLoadDRU(pResData, pEntryPoint);
  if Assigned(pBaseAddress) then begin
    @_IPWorksSSH_EvtStr                   := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_EvtStr');
    @_IPWorksSSH_Stream                   := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_Stream');
    @_IPWorksSSH_SCP_Create               := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SCP_Create');
    @_IPWorksSSH_SCP_Destroy              := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SCP_Destroy');
    @_IPWorksSSH_SCP_Set                  := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SCP_Set');
    @_IPWorksSSH_SCP_Get                  := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SCP_Get');
    @_IPWorksSSH_SCP_GetLastError         := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SCP_GetLastError');
    @_IPWorksSSH_SCP_GetLastErrorCode     := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SCP_GetLastErrorCode');
    @_IPWorksSSH_SCP_SetLastErrorAndCode  := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SCP_SetLastErrorAndCode');
    @_IPWorksSSH_SCP_GetEventError        := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SCP_GetEventError');
    @_IPWorksSSH_SCP_GetEventErrorCode    := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SCP_GetEventErrorCode');
    @_IPWorksSSH_SCP_SetEventErrorAndCode := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SCP_SetEventErrorAndCode');
    @_IPWorksSSH_SCP_CheckIndex           := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SCP_CheckIndex');
    @_IPWorksSSH_SCP_Do                   := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SCP_Do');
  end;

  FreeResource(hResData);
end;
{$ENDIF USEIPWORKSSSHDLL}

initialization
begin
  iLoadCount := iLoadCount + 1;
  if iLoadCount > 1 then exit;

  {$IFDEF USEIPWORKSSSHDLL}
  hLib := 0;
  LoadDLL();
  {$ELSE}
  pBaseAddress := nil;
  pEntryPoint := nil;
  LoadResDLL();
  {$ENDIF}

  {$IFDEF USE_EXPLICIT_INIT}
  if @_IPWorksSSH_SCP_StaticInit <> nil then
    _IPWorksSSH_SCP_StaticInit(nil);
  {$ENDIF}
end;

finalization
begin
  iLoadCount := iLoadCount - 1;
  if iLoadCount > 0 then exit;

  {$IFDEF USE_EXPLICIT_INIT}
  if @_IPWorksSSH_SCP_StaticDestroy <> nil then
    _IPWorksSSH_SCP_StaticDestroy();
  {$ENDIF}

  ClearProcs();

  {$IFDEF USEIPWORKSSSHDLL}
  if hLib <> 0 then
  begin
    {$ifndef FPC}FreeLibrary{$else}UnloadLibrary{$endif}(hLib);
    hLib := 0;
  end;
  {$ELSE}
  IPWorksSSHFreeDRU(pBaseAddress, pEntryPoint);
  pBaseAddress := nil;
  pEntryPoint := nil;
  {$ENDIF}
  iLoadCount := 0;
end;

{$ELSE USE_DYNAMIC_LOADING}

// in case if static library used

{$IFDEF USE_EXPLICIT_INIT}

initialization
  _IPWorksSSH_SCP_StaticInit(nil);

finalization
  _IPWorksSSH_SCP_StaticDestroy();

{$ENDIF USE_EXPLICIT_INIT}

{$ENDIF USE_DYNAMIC_LOADING}


end.
