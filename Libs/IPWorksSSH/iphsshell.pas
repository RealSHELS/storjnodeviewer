
unit iphsshell;



{$UNDEF USE_DYNAMIC_LOADING}
{$UNDEF USE_EXPLICIT_INIT}
{$UNDEF ANSICHAR_IS_BYTE}
{$UNDEF NO_ANSI}
{$UNDEF USE_CDECL}

{$IFDEF ENFORCE_DYNAMIC_LOADING}
  {$DEFINE USE_DYNAMIC_LOADING}
  {$DEFINE USEIPWORKSSSHDLL}
{$ENDIF}

{$IFDEF FPC}  //Lazarus
  {$IFDEF FPC_UNICODESTRINGS}
    // ensure that mode Delphi Unicode is enabled, not a modeswitch unicodestrings
    {$MODE DELPHIUNICODE}
  {$ELSE}
    {$MODE DELPHI}
  {$ENDIF}
  {$DEFINE USEIPWORKSSSHDLL}
  {$IFDEF DARWIN}
    {$DEFINE MACOS}
  {$ENDIF}
{$ENDIF}

{$IFNDEF FPC}
  {$IF CompilerVersion >= 24} // XE3+
    {$LEGACYIFEND ON}
  {$IFEND}
{$ENDIF}

{$IF DEFINED(LINUX) OR DEFINED(ANDROID) OR DEFINED(MACOS) OR DEFINED(IOS)}
  {$DEFINE USEIPWORKSSSHDLL}
  {$DEFINE ANSICHAR_IS_BYTE}
  {$DEFINE NO_ANSI}
  {$DEFINE USE_CDECL}
{$IFEND}

{$IF DEFINED(LINUX) OR DEFINED(MACOS)}
  {$IFNDEF IOS}
    {$DEFINE USE_DYNAMIC_LOADING}
  {$ENDIF}
{$IFEND}

{$IF DEFINED(ANDROID)}
  {$DEFINE USE_EXPLICIT_INIT}
{$IFEND}

{$IFDEF USE_IPWORKSSSH_ANDROID_SHARED_LIBRARY}
  {$DEFINE USE_DYNAMIC_LOADING}
{$ENDIF}


{$IF DEFINED(UNICODE) AND NOT DEFINED(MSWINDOWS)}
  {$DEFINE NONWINUNICODE}
{$IFEND}



{$IFNDEF USEIPWORKSSSHDLL}
  // in case if loading DLL from resources
  {$DEFINE USE_DYNAMIC_LOADING}
{$ENDIF}

{$UNDEF UNICODELIB}
{$IF DEFINED(MSWINDOWS)}
  {$DEFINE UNICODELIB}
{$IFEND}


{$IFDEF UNICODE}
  {$IFNDEF UNICODELIB}
    {$DEFINE UNICODE_ON_ANSI}
  {$ELSE}
    {$DEFINE UNICODE_ON_UNICODE}
  {$ENDIF}
{$ELSE}
  {$IFDEF UNICODELIB}
    {$DEFINE ANSI_ON_UNICODE}
  {$ELSE}
    {$DEFINE ANSI_ON_ANSI}
  {$ENDIF}
{$ENDIF}

interface

uses
{$IFDEF FPC}  //Lazarus
  SysUtils, Classes,
{$ELSE}
  {$IF CompilerVersion >= 23}System.SysUtils{$ELSE}SysUtils{$IFEND}, {$IF CompilerVersion >= 23}System.Classes{$ELSE}Classes{$IFEND}, 
{$ENDIF}
  {$IFDEF FPC}
  dynlibs,
  {$ELSE}
  {$IFDEF POSIX}
  Posix.Base,
  {$ENDIF}
  {$ENDIF}
  iphtypes, iphcore, iphkeys;

type

  (*** Enum Types ***)
  TiphsshellFirewallTypes = iphtypes.TiphFirewallTypes;

  TiphsshellSSHAuthModes = iphtypes.TiphSSHAuthModes;

  TiphsshellSSHCertStoreTypes = iphtypes.TiphCertStoreTypes;

  (*** Event Delegate Types ***)
  TConnectedEvent = procedure (
    Sender: TObject;
    StatusCode: Integer;
    const Description: String
  ) of Object;


  TConnectionStatusEvent = procedure (
    Sender: TObject;
    const ConnectionEvent: String;
    StatusCode: Integer;
    const Description: String
  ) of Object;


  TDisconnectedEvent = procedure (
    Sender: TObject;
    StatusCode: Integer;
    const Description: String
  ) of Object;


  TErrorEvent = procedure (
    Sender: TObject;
    ErrorCode: Integer;
    const Description: String
  ) of Object;


  TLogEvent = procedure (
    Sender: TObject;
    LogLevel: Integer;
    const Message: String;
    const LogType: String
  ) of Object;


  TSSHCustomAuthEvent = procedure (
    Sender: TObject;
    var Packet: String
  ) of Object;


  TSSHKeyboardInteractiveEvent = procedure (
    Sender: TObject;
    const Name: String;
    const Instructions: String;
    const Prompt: String;
    var Response: String;
    EchoResponse: Boolean
  ) of Object;


  TSSHServerAuthenticationEvent = procedure (
    Sender: TObject;
    HostKey: String;
    HostKeyB: TBytes;
    const Fingerprint: String;
    const KeyAlgorithm: String;
    const CertSubject: String;
    const CertIssuer: String;
    const Status: String;
    var Accept: Boolean
  ) of Object;


  TSSHStatusEvent = procedure (
    Sender: TObject;
    const Message: String
  ) of Object;


  TStderrEvent = procedure (
    Sender: TObject;
    Text: String;
    TextB: TBytes
  ) of Object;


  TStdoutEvent = procedure (
    Sender: TObject;
    Text: String;
    TextB: TBytes
  ) of Object;


  (*** Exception Type ***)
  EiphSShell = class(EIPWorksSSH)
  end;

  (*** Component Type ***)
{$IFNDEF FPC}
  {$IF CompilerVersion >= 23}
  [ComponentPlatformsAttribute(
    pidWin32 or pidWin64
    {$IF CompilerVersion >= 29}
    or pidLinux64
    {$IFEND}
    {$IF CompilerVersion >= 31}
    or pidOSX64
    {$IFEND}
   )]
  {$IFEND}
{$ENDIF}
  TiphSShell = class(TiphTypesCore)
    private
      (*** Event Handler Procedure Pointers ***)
      FOnConnected: TConnectedEvent;

      FOnConnectionStatus: TConnectionStatusEvent;

      FOnDisconnected: TDisconnectedEvent;

      FOnError: TErrorEvent;

      FOnLog: TLogEvent;

      FOnSSHCustomAuth: TSSHCustomAuthEvent;

      FOnSSHKeyboardInteractive: TSSHKeyboardInteractiveEvent;

      FOnSSHServerAuthentication: TSSHServerAuthenticationEvent;

      FOnSSHStatus: TSSHStatusEvent;

      FOnStderr: TStderrEvent;

      FOnStdout: TStdoutEvent;

      m_ctl: Pointer;
      (*** Inner objects for types and collections ***)
      FFirewall: TiphFirewall;

      function HasData: Boolean;
      procedure ReadHnd(Reader: TStream);
      procedure WriteHnd(Writer: TStream);
      function BStr2CStr(pBStr: Pointer; lenBStr: Integer; bArr: TBytes): String;

      procedure SetRuntimeLicense(key: String);
      function GetRuntimeLicense: String;

    protected
      procedure AboutDlg; override;
      procedure DefineProperties(Filer: TFiler); override;

      function ThrowCoreException(Err: Integer; const Desc: {$ifndef NO_ANSI}AnsiString{$else}string{$endif}): EIPWorksSSH;
      procedure TreatErr(Err: Integer; const Desc: String);

      (*** Property Getters/Setters ***)
      function  get_Command: String;

      procedure set_Command(valCommand: String);

      function  get_Connected: Boolean;

      procedure set_Connected(valConnected: Boolean);

      function  get_ErrorMessage: String;

      function  get_FirewallAutoDetect: Boolean;

      procedure set_FirewallAutoDetect(valFirewallAutoDetect: Boolean);

      function  get_FirewallType: TiphsshellFirewallTypes;

      procedure set_FirewallType(valFirewallType: TiphsshellFirewallTypes);

      function  get_FirewallHost: String;

      procedure set_FirewallHost(valFirewallHost: String);

      function  get_FirewallPassword: String;

      procedure set_FirewallPassword(valFirewallPassword: String);

      function  get_FirewallPort: Integer;

      procedure set_FirewallPort(valFirewallPort: Integer);

      function  get_FirewallUser: String;

      procedure set_FirewallUser(valFirewallUser: String);

      function  get_LocalHost: String;

      procedure set_LocalHost(valLocalHost: String);

      function  get_LocalPort: Integer;

      procedure set_LocalPort(valLocalPort: Integer);

      function  get_SSHAcceptServerHostKeyEncoded: String;

      procedure set_SSHAcceptServerHostKeyEncoded(valSSHAcceptServerHostKeyEncoded: String);

      function  get_SSHAcceptServerHostKeyEncodedB: TBytes;

      procedure set_SSHAcceptServerHostKeyEncodedB(valSSHAcceptServerHostKeyEncoded: TBytes);

      function  get_SSHAuthMode: TiphsshellSSHAuthModes;

      procedure set_SSHAuthMode(valSSHAuthMode: TiphsshellSSHAuthModes);

      function  get_SSHCertEncoded: String;

      procedure set_SSHCertEncoded(valSSHCertEncoded: String);

      function  get_SSHCertEncodedB: TBytes;

      procedure set_SSHCertEncodedB(valSSHCertEncoded: TBytes);

      function  get_SSHCertStore: String;

      procedure set_SSHCertStore(valSSHCertStore: String);

      function  get_SSHCertStoreB: TBytes;

      procedure set_SSHCertStoreB(valSSHCertStore: TBytes);

      function  get_SSHCertStorePassword: String;

      procedure set_SSHCertStorePassword(valSSHCertStorePassword: String);

      function  get_SSHCertStoreType: TiphsshellSSHCertStoreTypes;

      procedure set_SSHCertStoreType(valSSHCertStoreType: TiphsshellSSHCertStoreTypes);

      function  get_SSHCertSubject: String;

      procedure set_SSHCertSubject(valSSHCertSubject: String);

      function  get_SSHCompressionAlgorithms: String;

      procedure set_SSHCompressionAlgorithms(valSSHCompressionAlgorithms: String);

      function  get_SSHEncryptionAlgorithms: String;

      procedure set_SSHEncryptionAlgorithms(valSSHEncryptionAlgorithms: String);

      function  get_SSHHost: String;

      procedure set_SSHHost(valSSHHost: String);

      function  get_SSHPassword: String;

      procedure set_SSHPassword(valSSHPassword: String);

      function  get_SSHPort: Integer;

      procedure set_SSHPort(valSSHPort: Integer);

      function  get_SSHUser: String;

      procedure set_SSHUser(valSSHUser: String);

      procedure set_Stdin(valStdin: String);

      procedure set_StdinB(valStdin: TBytes);

      function  get_Timeout: Integer;

      procedure set_Timeout(valTimeout: Integer);


      (*** Property Getters/Setters: OO API ***)
      function  get_Firewall: TiphFirewall;
      procedure set_Firewall(Value : TiphFirewall);
      
 
    public
      constructor Create(AOwner: TComponent); overload; override;
      constructor Create(AOwner: TComponent; RuntimeLicense: string); reintroduce; overload;
      destructor Destroy; override;
      function ReportEventException(E: Exception; Event: String; ReFire: Boolean = True): Integer;

      property RuntimeLicense: String read GetRuntimeLicense write SetRuntimeLicense;

      procedure SetSSHAcceptServerHostKeyEncoded(lpSSHAcceptServerHostKeyEncoded: PLXAnsiChar; lenSSHAcceptServerHostKeyEncoded: Cardinal);

      procedure SetSSHCertEncoded(lpSSHCertEncoded: PLXAnsiChar; lenSSHCertEncoded: Cardinal);

      procedure SetSSHCertStore(lpSSHCertStore: PLXAnsiChar; lenSSHCertStore: Cardinal);

      procedure SetStdin(lpStdin: PLXAnsiChar; lenStdin: Cardinal);


      (*** Runtime Property Definitions ***)

      property Connected: Boolean read get_Connected write set_Connected;



      property SSHAcceptServerHostKeyEncoded: String read get_SSHAcceptServerHostKeyEncoded write set_SSHAcceptServerHostKeyEncoded;



      property SSHAcceptServerHostKeyEncodedB: TBytes read get_SSHAcceptServerHostKeyEncodedB write set_SSHAcceptServerHostKeyEncodedB;



      property SSHCertEncoded: String read get_SSHCertEncoded write set_SSHCertEncoded;



      property SSHCertEncodedB: TBytes read get_SSHCertEncodedB write set_SSHCertEncodedB;



      property SSHCertStoreB: TBytes read get_SSHCertStoreB write set_SSHCertStoreB;



      property Stdin: String write set_Stdin;



      property StdinB: TBytes write set_StdinB;



      (*** Runtime properties: OO API ***)
      property Firewall: TiphFirewall read get_Firewall write set_Firewall;


{$IFNDEF DELPHI3}
      (*** Method Definitions ***)
      function  Config(ConfigurationString: String): String;

      procedure Connect();

      function  DecodePacket(EncodedPacket: String): TBytes;

      procedure Disconnect();

      procedure DoEvents();

      function  EncodePacket(Packet: TBytes): String;

      procedure Execute(Command: String);

      function  GetSSHParam(Payload: TBytes; Field: String): String;

      function  GetSSHParamBytes(Payload: TBytes; Field: String): TBytes;

      procedure Interrupt();

      procedure Reset();

      procedure Send(Text: TBytes);

      procedure SendCommand(Command: String);

      procedure SendStdinBytes(Data: TBytes);

      procedure SendStdinText(Text: String);

      function  SetSSHParam(Payload: TBytes; FieldType: String; FieldValue: String): TBytes;

      procedure SSHLogoff();

      procedure SSHLogon(SSHHost: String; SSHPort: Integer);

{$ENDIF}

    published
      (*** Design-time Property Definitions ***)

      property Command: String read get_Command write set_Command stored False;



      property ErrorMessage: String read get_ErrorMessage write SetNoopString stored False;



      property FirewallAutoDetect: Boolean read get_FirewallAutoDetect write set_FirewallAutoDetect default False;



      property FirewallType: TiphsshellFirewallTypes read get_FirewallType write set_FirewallType default fwNone;



      property FirewallHost: String read get_FirewallHost write set_FirewallHost;



      property FirewallPassword: String read get_FirewallPassword write set_FirewallPassword;



      property FirewallPort: Integer read get_FirewallPort write set_FirewallPort default 0;



      property FirewallUser: String read get_FirewallUser write set_FirewallUser;



      property LocalHost: String read get_LocalHost write set_LocalHost stored False;



      property LocalPort: Integer read get_LocalPort write set_LocalPort default 0;



      property SSHAuthMode: TiphsshellSSHAuthModes read get_SSHAuthMode write set_SSHAuthMode default amPassword;



      property SSHCertStore: String read get_SSHCertStore write set_SSHCertStore;



      property SSHCertStorePassword: String read get_SSHCertStorePassword write set_SSHCertStorePassword;



      property SSHCertStoreType: TiphsshellSSHCertStoreTypes read get_SSHCertStoreType write set_SSHCertStoreType default cstUser;



      property SSHCertSubject: String read get_SSHCertSubject write set_SSHCertSubject;



      property SSHCompressionAlgorithms: String read get_SSHCompressionAlgorithms write set_SSHCompressionAlgorithms;



      property SSHEncryptionAlgorithms: String read get_SSHEncryptionAlgorithms write set_SSHEncryptionAlgorithms;



      property SSHHost: String read get_SSHHost write set_SSHHost;



      property SSHPassword: String read get_SSHPassword write set_SSHPassword;



      property SSHPort: Integer read get_SSHPort write set_SSHPort default 22;



      property SSHUser: String read get_SSHUser write set_SSHUser;



      property Timeout: Integer read get_Timeout write set_Timeout default 0;


      (*** Event Handler Bindings ***)
      property OnConnected: TConnectedEvent read FOnConnected write FOnConnected;

      property OnConnectionStatus: TConnectionStatusEvent read FOnConnectionStatus write FOnConnectionStatus;

      property OnDisconnected: TDisconnectedEvent read FOnDisconnected write FOnDisconnected;

      property OnError: TErrorEvent read FOnError write FOnError;

      property OnLog: TLogEvent read FOnLog write FOnLog;

      property OnSSHCustomAuth: TSSHCustomAuthEvent read FOnSSHCustomAuth write FOnSSHCustomAuth;

      property OnSSHKeyboardInteractive: TSSHKeyboardInteractiveEvent read FOnSSHKeyboardInteractive write FOnSSHKeyboardInteractive;

      property OnSSHServerAuthentication: TSSHServerAuthenticationEvent read FOnSSHServerAuthentication write FOnSSHServerAuthentication;

      property OnSSHStatus: TSSHStatusEvent read FOnSSHStatus write FOnSSHStatus;

      property OnStderr: TStderrEvent read FOnStderr write FOnStderr;

      property OnStdout: TStdoutEvent read FOnStdout write FOnStdout;

    end;

    procedure Register;

implementation


{$T-}

{$IFDEF MSWINDOWS} // Both DLL and DRU loading require Windows dependency
{$IFDEF FPC}  //Lazarus
uses Messages;
{$ELSE}
uses {$IF CompilerVersion >= 23}Winapi.Windows{$ELSE}Windows{$IFEND}, {$IF CompilerVersion >= 23}Winapi.Messages{$ELSE}Messages{$IFEND};
{$ENDIF}
{$ENDIF}

const
  PID_SShell_Command                                            = 1;

  PID_SShell_Connected                                          = 2;

  PID_SShell_ErrorMessage                                       = 3;

  PID_SShell_FirewallAutoDetect                                 = 4;

  PID_SShell_FirewallType                                       = 5;

  PID_SShell_FirewallHost                                       = 6;

  PID_SShell_FirewallPassword                                   = 7;

  PID_SShell_FirewallPort                                       = 8;

  PID_SShell_FirewallUser                                       = 9;

  PID_SShell_LocalHost                                          = 10;

  PID_SShell_LocalPort                                          = 11;

  PID_SShell_SSHAcceptServerHostKeyEncoded                      = 13;

  PID_SShell_SSHAuthMode                                        = 40;

  PID_SShell_SSHCertEncoded                                     = 42;

  PID_SShell_SSHCertStore                                       = 58;

  PID_SShell_SSHCertStorePassword                               = 59;

  PID_SShell_SSHCertStoreType                                   = 60;

  PID_SShell_SSHCertSubject                                     = 61;

  PID_SShell_SSHCompressionAlgorithms                           = 69;

  PID_SShell_SSHEncryptionAlgorithms                            = 70;

  PID_SShell_SSHHost                                            = 71;

  PID_SShell_SSHPassword                                        = 72;

  PID_SShell_SSHPort                                            = 73;

  PID_SShell_SSHUser                                            = 74;

  PID_SShell_Stdin                                              = 75;

  PID_SShell_Timeout                                            = 76;

  EID_SShell_Connected = 1;

  EID_SShell_ConnectionStatus = 2;

  EID_SShell_Disconnected = 3;

  EID_SShell_Error = 4;

  EID_SShell_Log = 5;

  EID_SShell_SSHCustomAuth = 6;

  EID_SShell_SSHKeyboardInteractive = 7;

  EID_SShell_SSHServerAuthentication = 8;

  EID_SShell_SSHStatus = 9;

  EID_SShell_Stderr = 10;

  EID_SShell_Stdout = 11;

  MID_SShell_Config = 2;

  MID_SShell_Connect = 3;

  MID_SShell_DecodePacket = 4;

  MID_SShell_Disconnect = 5;

  MID_SShell_DoEvents = 6;

  MID_SShell_EncodePacket = 7;

  MID_SShell_Execute = 8;

  MID_SShell_GetSSHParam = 9;

  MID_SShell_GetSSHParamBytes = 10;

  MID_SShell_Interrupt = 11;

  MID_SShell_Reset = 12;

  MID_SShell_Send = 13;

  MID_SShell_SendCommand = 14;

  MID_SShell_SendStdinBytes = 15;

  MID_SShell_SendStdinText = 16;

  MID_SShell_SetSSHParam = 17;

  MID_SShell_SSHLogoff = 18;

  MID_SShell_SSHLogon = 19;

  CREATE_OPT = {$IFDEF UNICODE}1{$ELSE}0{$ENDIF};
  EVTSTR_OPT = {$IFDEF UNICODE}2{$ELSE}1{$ENDIF};

  {$IFDEF MSWINDOWS}
  DLLNAME = 'IPWORKSSSH22.DLL';
  {$ENDIF}
  {$IFDEF LINUX}
  DLLNAME = 'libipworksssh.so.22.0';
  {$ENDIF}
  {$IFDEF ANDROID}
  {$IFDEF USE_DYNAMIC_LOADING}
  DLLNAME = 'libipworksssh.so.22.0';
  {$ELSE}
  DLLNAME = 'libipworksssh.22.0.a';
  {$ENDIF}
  {$ENDIF}
  {$IFDEF IOS}
  DLLNAME = 'libipworksssh.22.0.a';
  {$ELSE}
  {$IFDEF MACOS}
  DLLNAME = '@rpath/libipworksssh.22.0.dylib';
  {$ENDIF}
  {$ENDIF}

{$WARNINGS OFF}

type
  INTARR = array[0..32] of Integer;
  LPINTARR = ^INTARR;
  LPINT = ^Cardinal;
  MEventHandle = function(lpContext: TiphSShell; event_id: Integer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  PEventHandle = ^MEventHandle;


{$IFNDEF USEIPWORKSSSHDLL}
var
  pEntryPoint: Pointer;
  pBaseAddress: Pointer;
  iLoadCount: Integer;
{$ELSE}
{$IFDEF USE_DYNAMIC_LOADING}
var
  hLib : {$ifndef FPC}HMODULE{$else}TLibHandle{$endif};
  iLoadCount: Integer;
{$ENDIF USE_DYNAMIC_LOADING}
{$ENDIF USEIPWORKSSSHDLL}


{$IFDEF USE_DYNAMIC_LOADING}
  _IPWorksSSH_EvtStr:                       function(pEvtStr: Pointer; id: Integer; value: Pointer; opt: Integer): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_Stream:                       function(pStream: Pointer; op: Integer; params: LPVOIDARR; retVal: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SShell_Create:                function(pMethod: PEventHandle; pObject: TiphSShell; pKey: Pointer; opts: Integer): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SShell_Destroy:               function(p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SShell_Set:                   function(p: Pointer; index: Integer; arridx: Integer; value: Pointer; len: Cardinal): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SShell_Get:                   function(p: Pointer; index: Integer; arridx: Integer; len: LPINT; llVal: PInt64): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SShell_GetLastError:          function(p: Pointer): PLXAnsiChar; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SShell_GetLastErrorCode:      function(p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SShell_SetLastErrorAndCode:   function(p: Pointer; code: Integer; message: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SShell_GetEventError:         function(p: Pointer): PLXAnsiChar; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SShell_GetEventErrorCode:     function(p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SShell_SetEventErrorAndCode:  function(p: Pointer; code: Integer; message: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  {$IFDEF USE_EXPLICIT_INIT}
  _IPWorksSSH_SShell_StaticInit:            function(instance: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SShell_StaticDestroy:         function(): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  {$ENDIF}
  _IPWorksSSH_SShell_CheckIndex:            function(p: Pointer; index: Integer; arridx: Integer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SShell_Do:                    function(p: Pointer; method_id: Integer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR; llVal: PInt64): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
{$ELSE}
  function _IPWorksSSH_EvtStr                      (pEvtStr: Pointer; id: Integer; value: Pointer; opt: Integer): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_EvtStr' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_Stream                      (pStream: Pointer; op: Integer; params: LPVOIDARR; retVal: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_Stream' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SShell_Create               (pMethod: PEventHandle; pObject: TiphSShell; pKey: Pointer; opts: Integer): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SShell_Create' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SShell_Destroy              (p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SShell_Destroy' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SShell_Set                  (p: Pointer; index: Integer; arridx: Integer; value: Pointer; len: Cardinal): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SShell_Set' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SShell_Get                  (p: Pointer; index: Integer; arridx: Integer; len: LPINT; llVal: PInt64): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SShell_Get' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SShell_GetLastError         (p: Pointer): PLXAnsiChar; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SShell_GetLastError' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SShell_GetLastErrorCode     (p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SShell_GetLastErrorCode' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SShell_SetLastErrorAndCode  (p: Pointer; code: Integer; message: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SShell_SetLastErrorAndCode' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SShell_GetEventError        (p: Pointer): PLXAnsiChar; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SShell_GetEventError' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SShell_GetEventErrorCode    (p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SShell_GetEventErrorCode' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SShell_SetEventErrorAndCode (p: Pointer; code: Integer; message: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SShell_SetEventErrorAndCode' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SShell_StaticInit           (instance: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SShell_StaticInit' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SShell_StaticDestroy        (): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SShell_StaticDestroy' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SShell_CheckIndex           (p: Pointer; index: Integer; arridx: Integer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SShell_CheckIndex' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SShell_Do                   (p: Pointer; method_id: Integer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR; llVal: PInt64): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SShell_Do' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
{$ENDIF}

{$HINTS OFF}

(*** Fire Event Methods ***)
function FireConnected(lpContext: TiphSShell; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_StatusCode: Integer;

  tmp_Description: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnConnected) then exit;

  tmp_StatusCode := Integer(params^[0]);

  tmp_Description := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  try lpContext.FOnConnected(lpContext, tmp_StatusCode, tmp_Description)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Connected'); end;

end;


function FireConnectionStatus(lpContext: TiphSShell; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionEvent: String;

  tmp_StatusCode: Integer;

  tmp_Description: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnConnectionStatus) then exit;

  tmp_ConnectionEvent := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[0]);
  tmp_StatusCode := Integer(params^[1]);

  tmp_Description := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  try lpContext.FOnConnectionStatus(lpContext, tmp_ConnectionEvent, tmp_StatusCode, tmp_Description)
  except on E: Exception do result := lpContext.ReportEventException(E, 'ConnectionStatus'); end;

end;


function FireDisconnected(lpContext: TiphSShell; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_StatusCode: Integer;

  tmp_Description: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnDisconnected) then exit;

  tmp_StatusCode := Integer(params^[0]);

  tmp_Description := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  try lpContext.FOnDisconnected(lpContext, tmp_StatusCode, tmp_Description)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Disconnected'); end;

end;


function FireError(lpContext: TiphSShell; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ErrorCode: Integer;

  tmp_Description: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnError) then exit;

  tmp_ErrorCode := Integer(params^[0]);

  tmp_Description := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  try lpContext.FOnError(lpContext, tmp_ErrorCode, tmp_Description)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Error', False); end;

end;


function FireLog(lpContext: TiphSShell; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_LogLevel: Integer;

  tmp_Message: String;

  tmp_LogType: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnLog) then exit;

  tmp_LogLevel := Integer(params^[0]);

  tmp_Message := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_LogType := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  try lpContext.FOnLog(lpContext, tmp_LogLevel, tmp_Message, tmp_LogType)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Log'); end;

end;


function FireSSHCustomAuth(lpContext: TiphSShell; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_Packet: String; // dev platform string
  {$ifdef UNICODE_ON_ANSI}
  tmp_PacketB: LXAnsiString; // library string
  {$endif}

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHCustomAuth) then exit;

  tmp_Packet := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(_IPWorksSSH_EvtStr(params^[0], 0, nil, EVTSTR_OPT));
  try lpContext.FOnSSHCustomAuth(lpContext, tmp_Packet)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHCustomAuth'); end;

  {$ifdef UNICODE_ON_ANSI}
  tmp_PacketB := ToLinuxAnsiString(tmp_Packet);
  {$endif}
  _IPWorksSSH_EvtStr(params^[0], 2, {$ifdef UNICODE_ON_ANSI}@tmp_PacketB[0]{$else}PChar(tmp_Packet){$endif}, EVTSTR_OPT);
end;


function FireSSHKeyboardInteractive(lpContext: TiphSShell; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_Name: String;

  tmp_Instructions: String;

  tmp_Prompt: String;

  tmp_Response: String; // dev platform string
  {$ifdef UNICODE_ON_ANSI}
  tmp_ResponseB: LXAnsiString; // library string
  {$endif}

  tmp_EchoResponse: Boolean;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHKeyboardInteractive) then exit;

  tmp_Name := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[0]);
  tmp_Instructions := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_Prompt := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_Response := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(_IPWorksSSH_EvtStr(params^[3], 0, nil, EVTSTR_OPT));
  tmp_EchoResponse := Boolean(params^[4]);

  try lpContext.FOnSSHKeyboardInteractive(lpContext, tmp_Name, tmp_Instructions, tmp_Prompt, tmp_Response, tmp_EchoResponse)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHKeyboardInteractive'); end;

  {$ifdef UNICODE_ON_ANSI}
  tmp_ResponseB := ToLinuxAnsiString(tmp_Response);
  {$endif}
  _IPWorksSSH_EvtStr(params^[3], 2, {$ifdef UNICODE_ON_ANSI}@tmp_ResponseB[0]{$else}PChar(tmp_Response){$endif}, EVTSTR_OPT);
end;


function FireSSHServerAuthentication(lpContext: TiphSShell; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_HostKey: String;
  tmp_HostKeyB: TBytes;

  tmp_Fingerprint: String;

  tmp_KeyAlgorithm: String;

  tmp_CertSubject: String;

  tmp_CertIssuer: String;

  tmp_Status: String;

  tmp_Accept: Boolean;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHServerAuthentication) then exit;

  SetLength(tmp_HostKeyB, cbparam^[0]);
  Move(Pointer(params^[0])^, Pointer(tmp_HostKeyB)^, cbparam^[0]); // IMPORTANT: Do NOT multiply the length value!
  tmp_HostKey := lpContext.BStr2CStr(params^[0], cbparam^[0], tmp_HostKeyB);
  tmp_Fingerprint := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_KeyAlgorithm := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_CertSubject := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[3]);
  tmp_CertIssuer := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[4]);
  tmp_Status := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[5]);
  tmp_Accept := Boolean(params^[6]);

  try lpContext.FOnSSHServerAuthentication(lpContext, tmp_HostKey, tmp_HostKeyB, tmp_Fingerprint, tmp_KeyAlgorithm, tmp_CertSubject, tmp_CertIssuer, tmp_Status, tmp_Accept)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHServerAuthentication'); end;

  params^[6] := Pointer(tmp_Accept);

end;


function FireSSHStatus(lpContext: TiphSShell; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_Message: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHStatus) then exit;

  tmp_Message := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[0]);
  try lpContext.FOnSSHStatus(lpContext, tmp_Message)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHStatus'); end;

end;


function FireStderr(lpContext: TiphSShell; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_Text: String;
  tmp_TextB: TBytes;

begin
  result := 0;
  if not Assigned(lpContext.FOnStderr) then exit;

  SetLength(tmp_TextB, cbparam^[0]);
  Move(Pointer(params^[0])^, Pointer(tmp_TextB)^, cbparam^[0]); // IMPORTANT: Do NOT multiply the length value!
  tmp_Text := lpContext.BStr2CStr(params^[0], cbparam^[0], tmp_TextB);
  try lpContext.FOnStderr(lpContext, tmp_Text, tmp_TextB)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Stderr'); end;

end;


function FireStdout(lpContext: TiphSShell; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_Text: String;
  tmp_TextB: TBytes;

begin
  result := 0;
  if not Assigned(lpContext.FOnStdout) then exit;

  SetLength(tmp_TextB, cbparam^[0]);
  Move(Pointer(params^[0])^, Pointer(tmp_TextB)^, cbparam^[0]); // IMPORTANT: Do NOT multiply the length value!
  tmp_Text := lpContext.BStr2CStr(params^[0], cbparam^[0], tmp_TextB);
  try lpContext.FOnStdout(lpContext, tmp_Text, tmp_TextB)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Stdout'); end;

end;



(*** Event Sink ***)
function FireEvents(lpContext: TiphSShell; event_id: Integer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
var
  x: Integer;
begin
  result := 0;

  case event_id of
    EID_SShell_Connected: begin result := FireConnected(lpContext, cparam, params, cbparam); end;

    EID_SShell_ConnectionStatus: begin result := FireConnectionStatus(lpContext, cparam, params, cbparam); end;

    EID_SShell_Disconnected: begin result := FireDisconnected(lpContext, cparam, params, cbparam); end;

    EID_SShell_Error: begin result := FireError(lpContext, cparam, params, cbparam); end;

    EID_SShell_Log: begin result := FireLog(lpContext, cparam, params, cbparam); end;

    EID_SShell_SSHCustomAuth: begin result := FireSSHCustomAuth(lpContext, cparam, params, cbparam); end;

    EID_SShell_SSHKeyboardInteractive: begin result := FireSSHKeyboardInteractive(lpContext, cparam, params, cbparam); end;

    EID_SShell_SSHServerAuthentication: begin result := FireSSHServerAuthentication(lpContext, cparam, params, cbparam); end;

    EID_SShell_SSHStatus: begin result := FireSSHStatus(lpContext, cparam, params, cbparam); end;

    EID_SShell_Stderr: begin result := FireStderr(lpContext, cparam, params, cbparam); end;

    EID_SShell_Stdout: begin result := FireStdout(lpContext, cparam, params, cbparam); end;

    99999: begin x := 0; end;
  end;
end;

procedure Register;
begin
  iphCore.Register;
  RegisterComponents('IPWorks SSH', [TiphSShell]);
end;

{*********************************************************************************}
procedure TiphSShell.SetSSHAcceptServerHostKeyEncoded(lpSSHAcceptServerHostKeyEncoded: PLXAnsiChar; lenSSHAcceptServerHostKeyEncoded: Cardinal);
var
  err: integer;
begin
  if @_IPWorksSSH_SShell_Set = nil then exit;
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_SSHAcceptServerHostKeyEncoded, 0, Pointer(lpSSHAcceptServerHostKeyEncoded), lenSSHAcceptServerHostKeyEncoded);
  if err <> 0 then TreatErr(err, '');
end;

procedure TiphSShell.SetSSHCertEncoded(lpSSHCertEncoded: PLXAnsiChar; lenSSHCertEncoded: Cardinal);
var
  err: integer;
begin
  if @_IPWorksSSH_SShell_Set = nil then exit;
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_SSHCertEncoded, 0, Pointer(lpSSHCertEncoded), lenSSHCertEncoded);
  if err <> 0 then TreatErr(err, '');
end;

procedure TiphSShell.SetSSHCertStore(lpSSHCertStore: PLXAnsiChar; lenSSHCertStore: Cardinal);
var
  err: integer;
begin
  if @_IPWorksSSH_SShell_Set = nil then exit;
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_SSHCertStore, 0, Pointer(lpSSHCertStore), lenSSHCertStore);
  if err <> 0 then TreatErr(err, '');
end;

procedure TiphSShell.SetStdin(lpStdin: PLXAnsiChar; lenStdin: Cardinal);
var
  err: integer;
begin
  if @_IPWorksSSH_SShell_Set = nil then exit;
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_Stdin, 0, Pointer(lpStdin), lenStdin);
  if err <> 0 then TreatErr(err, '');
end;


type
  TiphFirewallFirewall = class(TiphFirewall)
  protected
    function OwnerCtl : TiphSShell;
    function GetPropAutoDetect: Boolean; override;

    procedure SetPropAutoDetect(Value: Boolean); override;

    function GetPropFirewallType: TiphFirewallTypes; override;

    procedure SetPropFirewallType(Value: TiphFirewallTypes); override;

    function GetPropHost: String; override;

    procedure SetPropHost(Value: String); override;

    function GetPropPassword: String; override;

    procedure SetPropPassword(Value: String); override;

    function GetPropPort: Integer; override;

    procedure SetPropPort(Value: Integer); override;

    function GetPropUser: String; override;

    procedure SetPropUser(Value: String); override;

  end;

function TiphFirewallFirewall.OwnerCtl : TiphSShell;
begin
  Result := TiphSShell(FOwnerCtl);
end;

function TiphFirewallFirewall.GetPropAutoDetect: Boolean;
begin
  Result := (OwnerCtl.get_FirewallAutoDetect());
end;

procedure TiphFirewallFirewall.SetPropAutoDetect(Value: Boolean);
begin
  OwnerCtl.set_FirewallAutoDetect((Value));
end;

function TiphFirewallFirewall.GetPropFirewallType: TiphFirewallTypes;
begin
  Result := TiphFirewallTypes(OwnerCtl.get_FirewallType());
end;

procedure TiphFirewallFirewall.SetPropFirewallType(Value: TiphFirewallTypes);
begin
  OwnerCtl.set_FirewallType(TiphsshellFirewallTypes(Value));
end;

function TiphFirewallFirewall.GetPropHost: String;
begin
  Result := (OwnerCtl.get_FirewallHost());
end;

procedure TiphFirewallFirewall.SetPropHost(Value: String);
begin
  OwnerCtl.set_FirewallHost((Value));
end;

function TiphFirewallFirewall.GetPropPassword: String;
begin
  Result := (OwnerCtl.get_FirewallPassword());
end;

procedure TiphFirewallFirewall.SetPropPassword(Value: String);
begin
  OwnerCtl.set_FirewallPassword((Value));
end;

function TiphFirewallFirewall.GetPropPort: Integer;
begin
  Result := (OwnerCtl.get_FirewallPort());
end;

procedure TiphFirewallFirewall.SetPropPort(Value: Integer);
begin
  OwnerCtl.set_FirewallPort((Value));
end;

function TiphFirewallFirewall.GetPropUser: String;
begin
  Result := (OwnerCtl.get_FirewallUser());
end;

procedure TiphFirewallFirewall.SetPropUser(Value: String);
begin
  OwnerCtl.set_FirewallUser((Value));
end;



{*********************************************************************************}

constructor TiphSShell.Create(AOwner: TComponent);
begin
  Create(AOwner, IPWORKSSSH_OEMKEY_63);
  Config('CodePage=65001');
end;

constructor TiphSShell.Create(AOwner: TComponent; RuntimeLicense: string);
begin
  inherited Create(AOwner);
  m_ctl := nil;

  if @_IPWorksSSH_SShell_Create <> nil then
    m_ctl := _IPWorksSSH_SShell_Create(@FireEvents, self, nil, CREATE_OPT);
  if m_ctl = nil then
    TreatErr(-1, 'IPWorks SSH SShell: Error creating component');
  _IPWorksSSH_SShell_Set(m_ctl, 8002{PID_KEYCHECK_DLP}, 0, 
    {$ifdef ANSICHAR_IS_BYTE}
    Pointer(ToLinuxAnsiString(RuntimeLicense)),
    {$else}
    Pointer(PAnsiChar(AnsiString(RuntimeLicense))),
    {$endif}
    0);

  FFirewall := TiphFirewallFirewall.Create(Self, false);


  try set_FirewallAutoDetect(false) except on E:Exception do end;

  try set_FirewallType(fwNone) except on E:Exception do end;

  try set_FirewallHost('') except on E:Exception do end;

  try set_FirewallPassword('') except on E:Exception do end;

  try set_FirewallPort(0) except on E:Exception do end;

  try set_FirewallUser('') except on E:Exception do end;

  try set_LocalPort(0) except on E:Exception do end;

  try set_SSHAuthMode(amPassword) except on E:Exception do end;

  try set_SSHCertStore('MY') except on E:Exception do end;

  try set_SSHCertStorePassword('') except on E:Exception do end;

  try set_SSHCertStoreType(cstUser) except on E:Exception do end;

  try set_SSHCertSubject('') except on E:Exception do end;

  try set_SSHCompressionAlgorithms('none,zlib') except on E:Exception do end;

  try set_SSHEncryptionAlgorithms('aes256-ctr,aes192-ctr,aes128-ctr,aes256-cbc,aes192-cbc,aes128-cbc,3des-ctr,3des-cbc,blowfish-cbc,arcfour256,arcfour128,arcfour,cast128-cbc,aes256-gcm@openssh.com,aes128-gcm@openssh.com,chacha20-poly1305@openssh.com') except on E:Exception do end;

  try set_SSHHost('') except on E:Exception do end;

  try set_SSHPassword('') except on E:Exception do end;

  try set_SSHPort(22) except on E:Exception do end;

  try set_SSHUser('') except on E:Exception do end;

  try set_Timeout(0) except on E:Exception do end;

end;

destructor TiphSShell.Destroy;
begin
  FreeAndNil(FFirewall);

  
  
  if m_ctl <> nil then begin
    if @_IPWorksSSH_SShell_Destroy <> nil then
      _IPWorksSSH_SShell_Destroy(m_ctl);
  end;
  m_ctl := nil;
  inherited Destroy;
end;

function TiphSShell.ThrowCoreException(err: Integer; const desc: {$ifndef NO_ANSI}AnsiString{$else}string{$endif}): EIPWorksSSH;
begin
  result := EiphSShell.CreateCode(err, desc);
end;

function TiphSShell.ReportEventException(e: Exception; Event: String; ReFire: Boolean): Integer;
var
  msg: String;
begin
  result := -1;
  msg := 'An unhandled error occurred in the ' + Event + ' event handler: ' + E.Message;
  if (m_ctl <> nil) and (@_IPWorksSSH_SShell_SetEventErrorAndCode <> nil) then begin
    if E is EIPWorksSSH then begin
      result := EIPWorksSSH(e).Code;
      msg := E.Message;
    end;
    _IPWorksSSH_SShell_SetEventErrorAndCode(m_ctl, result, 
      {$ifdef ANSICHAR_IS_BYTE}
      Pointer(ToLinuxAnsiString(msg)));
      {$else}
      Pointer(PAnsiChar(AnsiString(msg))));
      {$endif}
  end;
  if ReFire and Assigned(FOnError) then FOnError(self, result, msg);
end;

{*********************************************************************************}

procedure TiphSShell.AboutDlg;
var
  p : LPVOIDARR;
  pc: LPINTARR;
begin
{$IFDEF LINUX}
  inherited AboutDlg;
{$ELSE}
  p := nil;
  pc := nil;
  if @_IPWorksSSH_SShell_Do <> nil then
    _IPWorksSSH_SShell_Do(m_ctl, 255, 0, p, pc, nil);
{$ENDIF}
end;

{*********************************************************************************}

procedure TiphSShell.SetRuntimeLicense(key: String);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SShell_Set = nil then exit;
  err := _IPWorksSSH_SShell_Set(m_ctl, 8001{PID_RUNTIME_LICENSE}, 0, 
    {$ifdef ANSICHAR_IS_BYTE}
    Pointer(ToLinuxAnsiString(key)),
    {$else}
    Pointer(PAnsiChar(AnsiString(key))),
    {$endif}
    0);

  if err <> 0 then TreatErr(err, '');
end;

function TiphSShell.GetRuntimeLicense: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';
  if @_IPWorksSSH_SShell_Get = nil then exit;
  tmp := _IPWorksSSH_SShell_Get(m_ctl, 8001{PID_RUNTIME_LICENSE}, 0, nil, nil);
  err := _IPWorksSSH_SShell_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$ifdef ANSICHAR_IS_BYTE}
  result := FromLinuxAnsiString(PLXAnsiChar(tmp));
  {$else}
  result := PAnsiChar(tmp);
  {$endif}
end;

{*********************************************************************************}

function TiphSShell.HasData: Boolean;
begin
  result := false;
end;

procedure TiphSShell.ReadHnd(Reader: TStream);
begin
end;

procedure TiphSShell.WriteHnd(Writer: TStream);
begin
end;

function TiphSShell.BStr2CStr(pBStr: Pointer; lenBStr: Integer; bArr: TBytes): String;
{$IF DEFINED(UNICODE) AND DEFINED(MSWINDOWS)}
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
{$IFEND}
begin
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  param[0] := pBStr; paramcb[0] := lenBStr;
  param[1] := nil;   paramcb[1] := 0;
  param[2] := nil;   paramcb[2] := 0;
  _IPWorksSSH_SShell_Do(m_ctl, 2003, 2, @param, @paramcb, nil);
  SetLength(Result, Integer(param[2]));
  if Length(Result) > 0 then begin
    param[1] := PChar(Result); paramcb[1] := Length(Result);
    _IPWorksSSH_SShell_Do(m_ctl, 2003, 2, @param, @paramcb, nil);
  end;
  {$ELSE}
  try
    Result := TEncoding.Default.GetString(bArr);
  except
    on E: Exception do
      SetString(Result, PAnsiChar(pBStr), lenBStr);
   end;
  {$ENDIF}
{$ELSE}
  SetString(Result, PAnsiChar(pBStr), lenBStr);
{$ENDIF}
end;

procedure TiphSShell.DefineProperties(Filer: TFiler);
begin
  inherited DefineProperties(Filer);
  Filer.DefineBinaryProperty('RegHnd', ReadHnd, WriteHnd, HasData);
end;

{*********************************************************************************}

procedure TiphSShell.TreatErr(Err: Integer; const desc: String);
var
  msg : {$ifdef NO_ANSI}string{$else}LXAnsiString{$endif};
begin
  {$ifdef NO_ANSI}
  msg := desc;
  {$else}
  {$ifdef ANSICHAR_IS_BYTE}
  msg := ToLinuxAnsiString(desc);
  {$else}
  msg := LXAnsiString(desc);
  {$endif}
  {$endif}
  if Length(msg) = 0 then
  begin
    if @_IPWorksSSH_SShell_GetLastError <> nil then
      {$ifdef NO_ANSI}
      {$ifdef ANSICHAR_IS_BYTE}
      msg := FromLinuxAnsiString(_IPWorksSSH_SShell_GetLastError(m_ctl));
      {$else}
      msg := _IPWorksSSH_SShell_GetLastError(m_ctl);
      {$endif}
      {$else}
      msg := _IPWorksSSH_SShell_GetLastError(m_ctl);
      {$endif}
  end;

  raise ThrowCoreException(err, msg);
end;

{*********************************************************************************}

(*** Property Getters/Setters ***)
function TiphSShell.get_Command: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SShell_Get = nil then exit;
  tmp := _IPWorksSSH_SShell_Get(m_ctl, PID_SShell_Command{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SShell_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSShell.set_Command(valCommand: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SShell_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valCommand);
  {$ENDIF}
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_Command{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valCommand){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSShell.get_Connected: Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);

  if @_IPWorksSSH_SShell_Get = nil then exit;
  tmp := _IPWorksSSH_SShell_Get(m_ctl, PID_SShell_Connected, 0, nil, nil);
  err := _IPWorksSSH_SShell_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


procedure TiphSShell.set_Connected(valConnected: Boolean);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SShell_Set = nil then exit;
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_Connected, 0, Pointer(valConnected), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSShell.get_ErrorMessage: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SShell_Get = nil then exit;
  tmp := _IPWorksSSH_SShell_Get(m_ctl, PID_SShell_ErrorMessage{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SShell_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphSShell.get_FirewallAutoDetect: Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);

  if @_IPWorksSSH_SShell_Get = nil then exit;
  tmp := _IPWorksSSH_SShell_Get(m_ctl, PID_SShell_FirewallAutoDetect, 0, nil, nil);
  err := _IPWorksSSH_SShell_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


procedure TiphSShell.set_FirewallAutoDetect(valFirewallAutoDetect: Boolean);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SShell_Set = nil then exit;
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_FirewallAutoDetect, 0, Pointer(valFirewallAutoDetect), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSShell.get_FirewallType: TiphsshellFirewallTypes;
var
  tmp: Pointer;
  err: Integer;
begin
  result := TiphsshellFirewallTypes(0);

  if @_IPWorksSSH_SShell_Get = nil then exit;
  tmp := _IPWorksSSH_SShell_Get(m_ctl, PID_SShell_FirewallType, 0, nil, nil);
  err := _IPWorksSSH_SShell_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := TiphsshellFirewallTypes(tmp);
end;


procedure TiphSShell.set_FirewallType(valFirewallType: TiphsshellFirewallTypes);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SShell_Set = nil then exit;
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_FirewallType, 0, Pointer(valFirewallType), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSShell.get_FirewallHost: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SShell_Get = nil then exit;
  tmp := _IPWorksSSH_SShell_Get(m_ctl, PID_SShell_FirewallHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SShell_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSShell.set_FirewallHost(valFirewallHost: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SShell_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valFirewallHost);
  {$ENDIF}
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_FirewallHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valFirewallHost){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSShell.get_FirewallPassword: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SShell_Get = nil then exit;
  tmp := _IPWorksSSH_SShell_Get(m_ctl, PID_SShell_FirewallPassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SShell_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSShell.set_FirewallPassword(valFirewallPassword: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SShell_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valFirewallPassword);
  {$ENDIF}
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_FirewallPassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valFirewallPassword){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSShell.get_FirewallPort: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SShell_Get = nil then exit;
  tmp := _IPWorksSSH_SShell_Get(m_ctl, PID_SShell_FirewallPort, 0, nil, nil);
  err := _IPWorksSSH_SShell_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSShell.set_FirewallPort(valFirewallPort: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SShell_Set = nil then exit;
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_FirewallPort, 0, Pointer(valFirewallPort), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSShell.get_FirewallUser: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SShell_Get = nil then exit;
  tmp := _IPWorksSSH_SShell_Get(m_ctl, PID_SShell_FirewallUser{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SShell_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSShell.set_FirewallUser(valFirewallUser: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SShell_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valFirewallUser);
  {$ENDIF}
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_FirewallUser{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valFirewallUser){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSShell.get_LocalHost: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SShell_Get = nil then exit;
  tmp := _IPWorksSSH_SShell_Get(m_ctl, PID_SShell_LocalHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SShell_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSShell.set_LocalHost(valLocalHost: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SShell_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valLocalHost);
  {$ENDIF}
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_LocalHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valLocalHost){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSShell.get_LocalPort: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SShell_Get = nil then exit;
  tmp := _IPWorksSSH_SShell_Get(m_ctl, PID_SShell_LocalPort, 0, nil, nil);
  err := _IPWorksSSH_SShell_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSShell.set_LocalPort(valLocalPort: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SShell_Set = nil then exit;
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_LocalPort, 0, Pointer(valLocalPort), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSShell.get_SSHAcceptServerHostKeyEncoded: String;
var
  tmp: {$IFDEF NONWINUNICODE}TBytes{$ELSE}Pointer; len: Cardinal{$ENDIF};
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SShell_Get = nil then exit;
  tmp := {$IFDEF NONWINUNICODE}get_SSHAcceptServerHostKeyEncodedB{$ELSE}_IPWorksSSH_SShell_Get(m_ctl, PID_SShell_SSHAcceptServerHostKeyEncoded{$IFDEF UNICODE_ON_UNICODE}+20000{$ENDIF}, 0, @len, nil){$ENDIF};
  err := _IPWorksSSH_SShell_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$IFDEF NONWINUNICODE}result := TEncoding.UTF8.GetString(tmp);{$ELSE}SetString(result, PChar(tmp), len);{$ENDIF}
end;


procedure TiphSShell.set_SSHAcceptServerHostKeyEncoded(valSSHAcceptServerHostKeyEncoded: String);
var
  err: Integer;
  {$IFDEF NONWINUNICODE}tmp: TBytes;{$ENDIF}
begin
  err := 0;
  if @_IPWorksSSH_SShell_Set = nil then exit;
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_SSHAcceptServerHostKeyEncoded+20000, 0, Pointer(PWideChar(valSSHAcceptServerHostKeyEncoded)), Length(valSSHAcceptServerHostKeyEncoded));
  {$ELSE}
  tmp := TEncoding.UTF8.GetBytes(valSSHAcceptServerHostKeyEncoded);
  set_SSHAcceptServerHostKeyEncodedB(tmp);
  {$ENDIF}
{$ELSE}
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_SSHAcceptServerHostKeyEncoded, 0, Pointer(PAnsiChar(valSSHAcceptServerHostKeyEncoded)), Length(valSSHAcceptServerHostKeyEncoded));
{$ENDIF}
  if err <> 0 then TreatErr(err, '');
end;


function TiphSShell.get_SSHAcceptServerHostKeyEncodedB: TBytes;
var
  tmp: Pointer;
  len: Cardinal;
begin
  result := nil;
  if @_IPWorksSSH_SShell_Get = nil then exit;
  tmp := _IPWorksSSH_SShell_Get(m_ctl, PID_SShell_SSHAcceptServerHostKeyEncoded, 0, @len, nil);
  SetLength(result, len);
  Move(Pointer(tmp)^, Pointer(result)^, len); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSShell.set_SSHAcceptServerHostKeyEncodedB(valSSHAcceptServerHostKeyEncoded: TBytes);
var
  err: Integer;
begin
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_SSHAcceptServerHostKeyEncoded, 0, Pointer(valSSHAcceptServerHostKeyEncoded), Length(valSSHAcceptServerHostKeyEncoded));
  if err <> 0 then TreatErr(err, '');
end;


function TiphSShell.get_SSHAuthMode: TiphsshellSSHAuthModes;
var
  tmp: Pointer;
  err: Integer;
begin
  result := TiphsshellSSHAuthModes(0);

  if @_IPWorksSSH_SShell_Get = nil then exit;
  tmp := _IPWorksSSH_SShell_Get(m_ctl, PID_SShell_SSHAuthMode, 0, nil, nil);
  err := _IPWorksSSH_SShell_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := TiphsshellSSHAuthModes(tmp);
end;


procedure TiphSShell.set_SSHAuthMode(valSSHAuthMode: TiphsshellSSHAuthModes);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SShell_Set = nil then exit;
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_SSHAuthMode, 0, Pointer(valSSHAuthMode), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSShell.get_SSHCertEncoded: String;
var
  tmp: {$IFDEF NONWINUNICODE}TBytes{$ELSE}Pointer; len: Cardinal{$ENDIF};
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SShell_Get = nil then exit;
  tmp := {$IFDEF NONWINUNICODE}get_SSHCertEncodedB{$ELSE}_IPWorksSSH_SShell_Get(m_ctl, PID_SShell_SSHCertEncoded{$IFDEF UNICODE_ON_UNICODE}+20000{$ENDIF}, 0, @len, nil){$ENDIF};
  err := _IPWorksSSH_SShell_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$IFDEF NONWINUNICODE}result := TEncoding.UTF8.GetString(tmp);{$ELSE}SetString(result, PChar(tmp), len);{$ENDIF}
end;


procedure TiphSShell.set_SSHCertEncoded(valSSHCertEncoded: String);
var
  err: Integer;
  {$IFDEF NONWINUNICODE}tmp: TBytes;{$ENDIF}
begin
  err := 0;
  if @_IPWorksSSH_SShell_Set = nil then exit;
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_SSHCertEncoded+20000, 0, Pointer(PWideChar(valSSHCertEncoded)), Length(valSSHCertEncoded));
  {$ELSE}
  tmp := TEncoding.UTF8.GetBytes(valSSHCertEncoded);
  set_SSHCertEncodedB(tmp);
  {$ENDIF}
{$ELSE}
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_SSHCertEncoded, 0, Pointer(PAnsiChar(valSSHCertEncoded)), Length(valSSHCertEncoded));
{$ENDIF}
  if err <> 0 then TreatErr(err, '');
end;


function TiphSShell.get_SSHCertEncodedB: TBytes;
var
  tmp: Pointer;
  len: Cardinal;
begin
  result := nil;
  if @_IPWorksSSH_SShell_Get = nil then exit;
  tmp := _IPWorksSSH_SShell_Get(m_ctl, PID_SShell_SSHCertEncoded, 0, @len, nil);
  SetLength(result, len);
  Move(Pointer(tmp)^, Pointer(result)^, len); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSShell.set_SSHCertEncodedB(valSSHCertEncoded: TBytes);
var
  err: Integer;
begin
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_SSHCertEncoded, 0, Pointer(valSSHCertEncoded), Length(valSSHCertEncoded));
  if err <> 0 then TreatErr(err, '');
end;


function TiphSShell.get_SSHCertStore: String;
var
  tmp: {$IFDEF NONWINUNICODE}TBytes{$ELSE}Pointer; len: Cardinal{$ENDIF};
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SShell_Get = nil then exit;
  tmp := {$IFDEF NONWINUNICODE}get_SSHCertStoreB{$ELSE}_IPWorksSSH_SShell_Get(m_ctl, PID_SShell_SSHCertStore{$IFDEF UNICODE_ON_UNICODE}+20000{$ENDIF}, 0, @len, nil){$ENDIF};
  err := _IPWorksSSH_SShell_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$IFDEF NONWINUNICODE}result := TEncoding.UTF8.GetString(tmp);{$ELSE}SetString(result, PChar(tmp), len);{$ENDIF}
end;


procedure TiphSShell.set_SSHCertStore(valSSHCertStore: String);
var
  err: Integer;
  {$IFDEF NONWINUNICODE}tmp: TBytes;{$ENDIF}
begin
  err := 0;
  if @_IPWorksSSH_SShell_Set = nil then exit;
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_SSHCertStore+20000, 0, Pointer(PWideChar(valSSHCertStore)), Length(valSSHCertStore));
  {$ELSE}
  tmp := TEncoding.UTF8.GetBytes(valSSHCertStore);
  set_SSHCertStoreB(tmp);
  {$ENDIF}
{$ELSE}
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_SSHCertStore, 0, Pointer(PAnsiChar(valSSHCertStore)), Length(valSSHCertStore));
{$ENDIF}
  if err <> 0 then TreatErr(err, '');
end;


function TiphSShell.get_SSHCertStoreB: TBytes;
var
  tmp: Pointer;
  len: Cardinal;
begin
  result := nil;
  if @_IPWorksSSH_SShell_Get = nil then exit;
  tmp := _IPWorksSSH_SShell_Get(m_ctl, PID_SShell_SSHCertStore, 0, @len, nil);
  SetLength(result, len);
  Move(Pointer(tmp)^, Pointer(result)^, len); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSShell.set_SSHCertStoreB(valSSHCertStore: TBytes);
var
  err: Integer;
begin
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_SSHCertStore, 0, Pointer(valSSHCertStore), Length(valSSHCertStore));
  if err <> 0 then TreatErr(err, '');
end;


function TiphSShell.get_SSHCertStorePassword: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SShell_Get = nil then exit;
  tmp := _IPWorksSSH_SShell_Get(m_ctl, PID_SShell_SSHCertStorePassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SShell_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSShell.set_SSHCertStorePassword(valSSHCertStorePassword: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SShell_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHCertStorePassword);
  {$ENDIF}
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_SSHCertStorePassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHCertStorePassword){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSShell.get_SSHCertStoreType: TiphsshellSSHCertStoreTypes;
var
  tmp: Pointer;
  err: Integer;
begin
  result := TiphsshellSSHCertStoreTypes(0);

  if @_IPWorksSSH_SShell_Get = nil then exit;
  tmp := _IPWorksSSH_SShell_Get(m_ctl, PID_SShell_SSHCertStoreType, 0, nil, nil);
  err := _IPWorksSSH_SShell_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := TiphsshellSSHCertStoreTypes(tmp);
end;


procedure TiphSShell.set_SSHCertStoreType(valSSHCertStoreType: TiphsshellSSHCertStoreTypes);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SShell_Set = nil then exit;
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_SSHCertStoreType, 0, Pointer(valSSHCertStoreType), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSShell.get_SSHCertSubject: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SShell_Get = nil then exit;
  tmp := _IPWorksSSH_SShell_Get(m_ctl, PID_SShell_SSHCertSubject{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SShell_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSShell.set_SSHCertSubject(valSSHCertSubject: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SShell_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHCertSubject);
  {$ENDIF}
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_SSHCertSubject{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHCertSubject){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSShell.get_SSHCompressionAlgorithms: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SShell_Get = nil then exit;
  tmp := _IPWorksSSH_SShell_Get(m_ctl, PID_SShell_SSHCompressionAlgorithms{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SShell_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSShell.set_SSHCompressionAlgorithms(valSSHCompressionAlgorithms: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SShell_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHCompressionAlgorithms);
  {$ENDIF}
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_SSHCompressionAlgorithms{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHCompressionAlgorithms){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSShell.get_SSHEncryptionAlgorithms: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SShell_Get = nil then exit;
  tmp := _IPWorksSSH_SShell_Get(m_ctl, PID_SShell_SSHEncryptionAlgorithms{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SShell_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSShell.set_SSHEncryptionAlgorithms(valSSHEncryptionAlgorithms: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SShell_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHEncryptionAlgorithms);
  {$ENDIF}
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_SSHEncryptionAlgorithms{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHEncryptionAlgorithms){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSShell.get_SSHHost: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SShell_Get = nil then exit;
  tmp := _IPWorksSSH_SShell_Get(m_ctl, PID_SShell_SSHHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SShell_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSShell.set_SSHHost(valSSHHost: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SShell_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHHost);
  {$ENDIF}
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_SSHHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHHost){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSShell.get_SSHPassword: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SShell_Get = nil then exit;
  tmp := _IPWorksSSH_SShell_Get(m_ctl, PID_SShell_SSHPassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SShell_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSShell.set_SSHPassword(valSSHPassword: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SShell_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHPassword);
  {$ENDIF}
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_SSHPassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHPassword){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSShell.get_SSHPort: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SShell_Get = nil then exit;
  tmp := _IPWorksSSH_SShell_Get(m_ctl, PID_SShell_SSHPort, 0, nil, nil);
  err := _IPWorksSSH_SShell_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSShell.set_SSHPort(valSSHPort: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SShell_Set = nil then exit;
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_SSHPort, 0, Pointer(valSSHPort), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSShell.get_SSHUser: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SShell_Get = nil then exit;
  tmp := _IPWorksSSH_SShell_Get(m_ctl, PID_SShell_SSHUser{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SShell_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSShell.set_SSHUser(valSSHUser: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SShell_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHUser);
  {$ENDIF}
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_SSHUser{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHUser){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


procedure TiphSShell.set_Stdin(valStdin: String);
var
  err: Integer;
  {$IFDEF NONWINUNICODE}tmp: TBytes;{$ENDIF}
begin
  err := 0;
  if @_IPWorksSSH_SShell_Set = nil then exit;
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_Stdin+20000, 0, Pointer(PWideChar(valStdin)), Length(valStdin));
  {$ELSE}
  tmp := TEncoding.UTF8.GetBytes(valStdin);
  set_StdinB(tmp);
  {$ENDIF}
{$ELSE}
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_Stdin, 0, Pointer(PAnsiChar(valStdin)), Length(valStdin));
{$ENDIF}
  if err <> 0 then TreatErr(err, '');
end;


procedure TiphSShell.set_StdinB(valStdin: TBytes);
var
  err: Integer;
begin
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_Stdin, 0, Pointer(valStdin), Length(valStdin));
  if err <> 0 then TreatErr(err, '');
end;


function TiphSShell.get_Timeout: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SShell_Get = nil then exit;
  tmp := _IPWorksSSH_SShell_Get(m_ctl, PID_SShell_Timeout, 0, nil, nil);
  err := _IPWorksSSH_SShell_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSShell.set_Timeout(valTimeout: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SShell_Set = nil then exit;
  err := _IPWorksSSH_SShell_Set(m_ctl, PID_SShell_Timeout, 0, Pointer(valTimeout), 0);
  if err <> 0 then TreatErr(err, '');
end;



{*********************************************************************************}

(*** Property Getters/Setters: OO API ***)
function  TiphSShell.get_Firewall: TiphFirewall;
begin
  Result := FFirewall;
end;

procedure TiphSShell.set_Firewall(Value : TiphFirewall);
begin
  FFirewall.Assign(Value);
end;


{*********************************************************************************}

{$IFNDEF DELPHI3}
(*** Methods ***)
function TiphSShell.Config(ConfigurationString: String): String;
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
  tmp_ConfigurationString: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := '';

  tmp_ConfigurationString := {$ifndef UNICODE_ON_ANSI}ConfigurationString{$else}ToLinuxAnsiString(ConfigurationString){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_ConfigurationString){$else}@tmp_ConfigurationString[0]{$endif};

  if @_IPWorksSSH_SShell_Do = nil then exit;
  err := _IPWorksSSH_SShell_Do(m_ctl, MID_SShell_Config{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  result := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(param[1]);
end;


procedure TiphSShell.Connect();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SShell_Do = nil then exit;
  err := _IPWorksSSH_SShell_Do(m_ctl, MID_SShell_Connect{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


function TiphSShell.DecodePacket(EncodedPacket: String): TBytes;
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
  tmp_EncodedPacket: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := nil;

  tmp_EncodedPacket := {$ifndef UNICODE_ON_ANSI}EncodedPacket{$else}ToLinuxAnsiString(EncodedPacket){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_EncodedPacket){$else}@tmp_EncodedPacket[0]{$endif};

  if @_IPWorksSSH_SShell_Do = nil then exit;
  err := _IPWorksSSH_SShell_Do(m_ctl, MID_SShell_DecodePacket{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  SetLength(result, paramcb[1]);
  Move(Pointer(param[1])^, Pointer(result)^, paramcb[1]); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSShell.Disconnect();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SShell_Do = nil then exit;
  err := _IPWorksSSH_SShell_Do(m_ctl, MID_SShell_Disconnect{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSShell.DoEvents();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SShell_Do = nil then exit;
  err := _IPWorksSSH_SShell_Do(m_ctl, MID_SShell_DoEvents{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


function TiphSShell.EncodePacket(Packet: TBytes): String;
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := '';

  param[0] := Pointer(Packet);
  paramcb[0] := Length(Packet);

  if @_IPWorksSSH_SShell_Do = nil then exit;
  err := _IPWorksSSH_SShell_Do(m_ctl, MID_SShell_EncodePacket{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  result := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(param[1]);
end;


procedure TiphSShell.Execute(Command: String);
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
  tmp_Command: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  tmp_Command := {$ifndef UNICODE_ON_ANSI}Command{$else}ToLinuxAnsiString(Command){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_Command){$else}@tmp_Command[0]{$endif};

  if @_IPWorksSSH_SShell_Do = nil then exit;
  err := _IPWorksSSH_SShell_Do(m_ctl, MID_SShell_Execute{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


function TiphSShell.GetSSHParam(Payload: TBytes; Field: String): String;
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
  i, err: Integer;
  tmp_Field: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 2 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := '';

  param[0] := Pointer(Payload);
  paramcb[0] := Length(Payload);

  tmp_Field := {$ifndef UNICODE_ON_ANSI}Field{$else}ToLinuxAnsiString(Field){$endif};
  param[1] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_Field){$else}@tmp_Field[0]{$endif};

  if @_IPWorksSSH_SShell_Do = nil then exit;
  err := _IPWorksSSH_SShell_Do(m_ctl, MID_SShell_GetSSHParam{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 2, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  result := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(param[2]);
end;


function TiphSShell.GetSSHParamBytes(Payload: TBytes; Field: String): TBytes;
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
  i, err: Integer;
  tmp_Field: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 2 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := nil;

  param[0] := Pointer(Payload);
  paramcb[0] := Length(Payload);

  tmp_Field := {$ifndef UNICODE_ON_ANSI}Field{$else}ToLinuxAnsiString(Field){$endif};
  param[1] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_Field){$else}@tmp_Field[0]{$endif};

  if @_IPWorksSSH_SShell_Do = nil then exit;
  err := _IPWorksSSH_SShell_Do(m_ctl, MID_SShell_GetSSHParamBytes{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 2, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  SetLength(result, paramcb[2]);
  Move(Pointer(param[2])^, Pointer(result)^, paramcb[2]); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSShell.Interrupt();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SShell_Do = nil then exit;
  err := _IPWorksSSH_SShell_Do(m_ctl, MID_SShell_Interrupt{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSShell.Reset();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SShell_Do = nil then exit;
  err := _IPWorksSSH_SShell_Do(m_ctl, MID_SShell_Reset{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSShell.Send(Text: TBytes);
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  param[0] := Pointer(Text);
  paramcb[0] := Length(Text);

  if @_IPWorksSSH_SShell_Do = nil then exit;
  err := _IPWorksSSH_SShell_Do(m_ctl, MID_SShell_Send{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSShell.SendCommand(Command: String);
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
  tmp_Command: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  tmp_Command := {$ifndef UNICODE_ON_ANSI}Command{$else}ToLinuxAnsiString(Command){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_Command){$else}@tmp_Command[0]{$endif};

  if @_IPWorksSSH_SShell_Do = nil then exit;
  err := _IPWorksSSH_SShell_Do(m_ctl, MID_SShell_SendCommand{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSShell.SendStdinBytes(Data: TBytes);
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  param[0] := Pointer(Data);
  paramcb[0] := Length(Data);

  if @_IPWorksSSH_SShell_Do = nil then exit;
  err := _IPWorksSSH_SShell_Do(m_ctl, MID_SShell_SendStdinBytes{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSShell.SendStdinText(Text: String);
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
  tmp_Text: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  tmp_Text := {$ifndef UNICODE_ON_ANSI}Text{$else}ToLinuxAnsiString(Text){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_Text){$else}@tmp_Text[0]{$endif};

  if @_IPWorksSSH_SShell_Do = nil then exit;
  err := _IPWorksSSH_SShell_Do(m_ctl, MID_SShell_SendStdinText{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


function TiphSShell.SetSSHParam(Payload: TBytes; FieldType: String; FieldValue: String): TBytes;
var
  param: array[0..3] of Pointer;
  paramcb: array[0..3] of Integer;
  i, err: Integer;
  tmp_FieldType: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
  tmp_FieldValue: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 3 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := nil;

  param[0] := Pointer(Payload);
  paramcb[0] := Length(Payload);

  tmp_FieldType := {$ifndef UNICODE_ON_ANSI}FieldType{$else}ToLinuxAnsiString(FieldType){$endif};
  param[1] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_FieldType){$else}@tmp_FieldType[0]{$endif};

  tmp_FieldValue := {$ifndef UNICODE_ON_ANSI}FieldValue{$else}ToLinuxAnsiString(FieldValue){$endif};
  param[2] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_FieldValue){$else}@tmp_FieldValue[0]{$endif};

  if @_IPWorksSSH_SShell_Do = nil then exit;
  err := _IPWorksSSH_SShell_Do(m_ctl, MID_SShell_SetSSHParam{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 3, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  SetLength(result, paramcb[3]);
  Move(Pointer(param[3])^, Pointer(result)^, paramcb[3]); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSShell.SSHLogoff();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SShell_Do = nil then exit;
  err := _IPWorksSSH_SShell_Do(m_ctl, MID_SShell_SSHLogoff{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSShell.SSHLogon(SSHHost: String; SSHPort: Integer);
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
  i, err: Integer;
  tmp_SSHHost: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 2 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  tmp_SSHHost := {$ifndef UNICODE_ON_ANSI}SSHHost{$else}ToLinuxAnsiString(SSHHost){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_SSHHost){$else}@tmp_SSHHost[0]{$endif};

  param[1] := Pointer(SSHPort);


  if @_IPWorksSSH_SShell_Do = nil then exit;
  err := _IPWorksSSH_SShell_Do(m_ctl, MID_SShell_SSHLogon{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 2, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


{$ENDIF}


{$IFDEF USE_DYNAMIC_LOADING}

procedure ClearProcs();
begin
  _IPWorksSSH_EvtStr := nil;
  _IPWorksSSH_Stream := nil;
  _IPWorksSSH_SShell_Create := nil;
  _IPWorksSSH_SShell_Destroy := nil;
  _IPWorksSSH_SShell_Set := nil;
  _IPWorksSSH_SShell_Get := nil;
  _IPWorksSSH_SShell_GetLastError := nil;
  _IPWorksSSH_SShell_GetLastErrorCode := nil;
  _IPWorksSSH_SShell_SetLastErrorAndCode := nil;
  _IPWorksSSH_SShell_GetEventError := nil;
  _IPWorksSSH_SShell_GetEventErrorCode := nil;
  _IPWorksSSH_SShell_SetEventErrorAndCode := nil;
  {$IFDEF USE_EXPLICIT_INIT}
  _IPWorksSSH_SShell_StaticInit := nil;
  _IPWorksSSH_SShell_StaticDestroy := nil;
  {$ENDIF}
  _IPWorksSSH_SShell_CheckIndex := nil;
  _IPWorksSSH_SShell_Do := nil;
end;

{$IFDEF USEIPWORKSSSHDLL}
procedure LoadDLL();
begin
  ClearProcs();

  hLib := LoadLibrary(PChar(DLLNAME));
  if hLib = 0 then exit;

  @_IPWorksSSH_EvtStr                      := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_EvtStr');
  @_IPWorksSSH_Stream                      := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_Stream');
  @_IPWorksSSH_SShell_Create               := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SShell_Create');
  @_IPWorksSSH_SShell_Destroy              := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SShell_Destroy');
  @_IPWorksSSH_SShell_Set                  := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SShell_Set');
  @_IPWorksSSH_SShell_Get                  := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SShell_Get');
  @_IPWorksSSH_SShell_GetLastError         := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SShell_GetLastError');
  @_IPWorksSSH_SShell_GetLastErrorCode     := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SShell_GetLastErrorCode');
  @_IPWorksSSH_SShell_SetLastErrorAndCode  := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SShell_SetLastErrorAndCode');
  @_IPWorksSSH_SShell_GetEventError        := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SShell_GetEventError');
  @_IPWorksSSH_SShell_GetEventErrorCode    := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SShell_GetEventErrorCode');
  @_IPWorksSSH_SShell_SetEventErrorAndCode := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SShell_SetEventErrorAndCode');
  {$IFDEF USE_EXPLICIT_INIT}
  @_IPWorksSSH_SShell_StaticInit           := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SShell_StaticInit');
  @_IPWorksSSH_SShell_StaticDestroy        := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SShell_StaticDestroy');
  {$ENDIF}
  @_IPWorksSSH_SShell_CheckIndex           := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SShell_CheckIndex');
  @_IPWorksSSH_SShell_Do                   := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SShell_Do');
end;
{$ELSE}
procedure LoadRESDLL();
var
  hResInfo: HRSRC;
  hResData: HGLOBAL;
  pResData: Pointer;
begin
  ClearProcs();

  hResInfo := FindResource(HInstance, 'ipworksssh22_dta', RT_RCDATA);
  if hResInfo = 0 then exit;

  hResData := LoadResource(HInstance, hResInfo);
  if hResData = 0 then exit;

  pResData := LockResource(hResData);
  if pResData = nil then exit;

  pBaseAddress := IPWorksSSHLoadDRU(pResData, pEntryPoint);
  if Assigned(pBaseAddress) then begin
    @_IPWorksSSH_EvtStr                      := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_EvtStr');
    @_IPWorksSSH_Stream                      := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_Stream');
    @_IPWorksSSH_SShell_Create               := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SShell_Create');
    @_IPWorksSSH_SShell_Destroy              := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SShell_Destroy');
    @_IPWorksSSH_SShell_Set                  := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SShell_Set');
    @_IPWorksSSH_SShell_Get                  := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SShell_Get');
    @_IPWorksSSH_SShell_GetLastError         := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SShell_GetLastError');
    @_IPWorksSSH_SShell_GetLastErrorCode     := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SShell_GetLastErrorCode');
    @_IPWorksSSH_SShell_SetLastErrorAndCode  := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SShell_SetLastErrorAndCode');
    @_IPWorksSSH_SShell_GetEventError        := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SShell_GetEventError');
    @_IPWorksSSH_SShell_GetEventErrorCode    := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SShell_GetEventErrorCode');
    @_IPWorksSSH_SShell_SetEventErrorAndCode := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SShell_SetEventErrorAndCode');
    @_IPWorksSSH_SShell_CheckIndex           := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SShell_CheckIndex');
    @_IPWorksSSH_SShell_Do                   := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SShell_Do');
  end;

  FreeResource(hResData);
end;
{$ENDIF USEIPWORKSSSHDLL}

initialization
begin
  iLoadCount := iLoadCount + 1;
  if iLoadCount > 1 then exit;

  {$IFDEF USEIPWORKSSSHDLL}
  hLib := 0;
  LoadDLL();
  {$ELSE}
  pBaseAddress := nil;
  pEntryPoint := nil;
  LoadResDLL();
  {$ENDIF}

  {$IFDEF USE_EXPLICIT_INIT}
  if @_IPWorksSSH_SShell_StaticInit <> nil then
    _IPWorksSSH_SShell_StaticInit(nil);
  {$ENDIF}
end;

finalization
begin
  iLoadCount := iLoadCount - 1;
  if iLoadCount > 0 then exit;

  {$IFDEF USE_EXPLICIT_INIT}
  if @_IPWorksSSH_SShell_StaticDestroy <> nil then
    _IPWorksSSH_SShell_StaticDestroy();
  {$ENDIF}

  ClearProcs();

  {$IFDEF USEIPWORKSSSHDLL}
  if hLib <> 0 then
  begin
    {$ifndef FPC}FreeLibrary{$else}UnloadLibrary{$endif}(hLib);
    hLib := 0;
  end;
  {$ELSE}
  IPWorksSSHFreeDRU(pBaseAddress, pEntryPoint);
  pBaseAddress := nil;
  pEntryPoint := nil;
  {$ENDIF}
  iLoadCount := 0;
end;

{$ELSE USE_DYNAMIC_LOADING}

// in case if static library used

{$IFDEF USE_EXPLICIT_INIT}

initialization
  _IPWorksSSH_SShell_StaticInit(nil);

finalization
  _IPWorksSSH_SShell_StaticDestroy();

{$ENDIF USE_EXPLICIT_INIT}

{$ENDIF USE_DYNAMIC_LOADING}


end.
