
unit iphsftp;



{$UNDEF USE_DYNAMIC_LOADING}
{$UNDEF USE_EXPLICIT_INIT}
{$UNDEF ANSICHAR_IS_BYTE}
{$UNDEF NO_ANSI}
{$UNDEF USE_CDECL}

{$IFDEF ENFORCE_DYNAMIC_LOADING}
  {$DEFINE USE_DYNAMIC_LOADING}
  {$DEFINE USEIPWORKSSSHDLL}
{$ENDIF}

{$IFDEF FPC}  //Lazarus
  {$IFDEF FPC_UNICODESTRINGS}
    // ensure that mode Delphi Unicode is enabled, not a modeswitch unicodestrings
    {$MODE DELPHIUNICODE}
  {$ELSE}
    {$MODE DELPHI}
  {$ENDIF}
  {$DEFINE USEIPWORKSSSHDLL}
  {$IFDEF DARWIN}
    {$DEFINE MACOS}
  {$ENDIF}
{$ENDIF}

{$IFNDEF FPC}
  {$IF CompilerVersion >= 24} // XE3+
    {$LEGACYIFEND ON}
  {$IFEND}
{$ENDIF}

{$IF DEFINED(LINUX) OR DEFINED(ANDROID) OR DEFINED(MACOS) OR DEFINED(IOS)}
  {$DEFINE USEIPWORKSSSHDLL}
  {$DEFINE ANSICHAR_IS_BYTE}
  {$DEFINE NO_ANSI}
  {$DEFINE USE_CDECL}
{$IFEND}

{$IF DEFINED(LINUX) OR DEFINED(MACOS)}
  {$IFNDEF IOS}
    {$DEFINE USE_DYNAMIC_LOADING}
  {$ENDIF}
{$IFEND}

{$IF DEFINED(ANDROID)}
  {$DEFINE USE_EXPLICIT_INIT}
{$IFEND}

{$IFDEF USE_IPWORKSSSH_ANDROID_SHARED_LIBRARY}
  {$DEFINE USE_DYNAMIC_LOADING}
{$ENDIF}


{$IF DEFINED(UNICODE) AND NOT DEFINED(MSWINDOWS)}
  {$DEFINE NONWINUNICODE}
{$IFEND}



{$IFNDEF USEIPWORKSSSHDLL}
  // in case if loading DLL from resources
  {$DEFINE USE_DYNAMIC_LOADING}
{$ENDIF}

{$UNDEF UNICODELIB}
{$IF DEFINED(MSWINDOWS)}
  {$DEFINE UNICODELIB}
{$IFEND}


{$IFDEF UNICODE}
  {$IFNDEF UNICODELIB}
    {$DEFINE UNICODE_ON_ANSI}
  {$ELSE}
    {$DEFINE UNICODE_ON_UNICODE}
  {$ENDIF}
{$ELSE}
  {$IFDEF UNICODELIB}
    {$DEFINE ANSI_ON_UNICODE}
  {$ELSE}
    {$DEFINE ANSI_ON_ANSI}
  {$ENDIF}
{$ENDIF}

interface

uses
{$IFDEF FPC}  //Lazarus
  SysUtils, Classes,
{$ELSE}
  {$IF CompilerVersion >= 23}System.SysUtils{$ELSE}SysUtils{$IFEND}, {$IF CompilerVersion >= 23}System.Classes{$ELSE}Classes{$IFEND}, 
{$ENDIF}
  {$IFDEF FPC}
  dynlibs,
  {$ELSE}
  {$IFDEF POSIX}
  Posix.Base,
  {$ENDIF}
  {$ENDIF}
  iphtypes, iphcore, iphkeys;

type

  (*** Enum Types ***)
  TiphsftpFileTypes = iphtypes.TiphSFTPFileTypes;

  TiphsftpFirewallTypes = iphtypes.TiphFirewallTypes;

  TiphsftpSSHAuthModes = iphtypes.TiphSSHAuthModes;

  TiphsftpSSHCertStoreTypes = iphtypes.TiphCertStoreTypes;

  (*** Event Delegate Types ***)
  TConnectedEvent = procedure (
    Sender: TObject;
    StatusCode: Integer;
    const Description: String
  ) of Object;


  TConnectionStatusEvent = procedure (
    Sender: TObject;
    const ConnectionEvent: String;
    StatusCode: Integer;
    const Description: String
  ) of Object;


  TDirListEvent = procedure (
    Sender: TObject;
    const DirEntry: String;
    const FileName: String;
    IsDir: Boolean;
    FileSize: Int64;
    const FileTime: String;
    IsSymlink: Boolean
  ) of Object;


  TDisconnectedEvent = procedure (
    Sender: TObject;
    StatusCode: Integer;
    const Description: String
  ) of Object;


  TEndTransferEvent = procedure (
    Sender: TObject;
    Direction: Integer;
    const LocalFile: String;
    const RemoteFile: String
  ) of Object;


  TErrorEvent = procedure (
    Sender: TObject;
    ErrorCode: Integer;
    const Description: String;
    const LocalFile: String;
    const RemoteFile: String
  ) of Object;


  TLogEvent = procedure (
    Sender: TObject;
    LogLevel: Integer;
    const Message: String;
    const LogType: String
  ) of Object;


  TSSHCustomAuthEvent = procedure (
    Sender: TObject;
    var Packet: String
  ) of Object;


  TSSHKeyboardInteractiveEvent = procedure (
    Sender: TObject;
    const Name: String;
    const Instructions: String;
    const Prompt: String;
    var Response: String;
    EchoResponse: Boolean
  ) of Object;


  TSSHServerAuthenticationEvent = procedure (
    Sender: TObject;
    HostKey: String;
    HostKeyB: TBytes;
    const Fingerprint: String;
    const KeyAlgorithm: String;
    const CertSubject: String;
    const CertIssuer: String;
    const Status: String;
    var Accept: Boolean
  ) of Object;


  TSSHStatusEvent = procedure (
    Sender: TObject;
    const Message: String
  ) of Object;


  TStartTransferEvent = procedure (
    Sender: TObject;
    Direction: Integer;
    const LocalFile: String;
    const RemoteFile: String
  ) of Object;


  TTransferEvent = procedure (
    Sender: TObject;
    Direction: Integer;
    const LocalFile: String;
    const RemoteFile: String;
    BytesTransferred: Int64;
    PercentDone: Integer;
    Text: String;
    TextB: TBytes;
    var Cancel: Boolean
  ) of Object;


  (*** Exception Type ***)
  EiphSFTP = class(EIPWorksSSH)
  end;

  (*** Component Type ***)
{$IFNDEF FPC}
  {$IF CompilerVersion >= 23}
  [ComponentPlatformsAttribute(
    pidWin32 or pidWin64
    {$IF CompilerVersion >= 29}
    or pidLinux64
    {$IFEND}
    {$IF CompilerVersion >= 31}
    or pidOSX64
    {$IFEND}
   )]
  {$IFEND}
{$ENDIF}
  TiphSFTP = class(TiphTypesCore)
    private
      (*** Event Handler Procedure Pointers ***)
      FOnConnected: TConnectedEvent;

      FOnConnectionStatus: TConnectionStatusEvent;

      FOnDirList: TDirListEvent;

      FOnDisconnected: TDisconnectedEvent;

      FOnEndTransfer: TEndTransferEvent;

      FOnError: TErrorEvent;

      FOnLog: TLogEvent;

      FOnSSHCustomAuth: TSSHCustomAuthEvent;

      FOnSSHKeyboardInteractive: TSSHKeyboardInteractiveEvent;

      FOnSSHServerAuthentication: TSSHServerAuthenticationEvent;

      FOnSSHStatus: TSSHStatusEvent;

      FOnStartTransfer: TStartTransferEvent;

      FOnTransfer: TTransferEvent;

      m_ctl: Pointer;
      m_streamFromSetDownloadStream : TList;

      m_streamFromSetUploadStream : TList;

      (*** Inner objects for types and collections ***)
      FDirList: TiphDirEntryList;

      FFileAttributes: TiphSFTPFileAttributes;

      FFirewall: TiphFirewall;

      function HasData: Boolean;
      procedure ReadHnd(Reader: TStream);
      procedure WriteHnd(Writer: TStream);
      function BStr2CStr(pBStr: Pointer; lenBStr: Integer; bArr: TBytes): String;

      procedure SetRuntimeLicense(key: String);
      function GetRuntimeLicense: String;

    protected
      procedure AboutDlg; override;
      procedure DefineProperties(Filer: TFiler); override;

      function ThrowCoreException(Err: Integer; const Desc: {$ifndef NO_ANSI}AnsiString{$else}string{$endif}): EIPWorksSSH;
      procedure TreatErr(Err: Integer; const Desc: String);

      (*** Property Getters/Setters ***)
      function  get_Connected: Boolean;

      procedure set_Connected(valConnected: Boolean);

      function  get_DirListCount: Integer;

      function  get_DirListEntry(EntryIndex: Integer): String;

      function  get_DirListFileName(EntryIndex: Integer): String;

      function  get_DirListFileSize(EntryIndex: Integer): Int64;

      function  get_DirListFileTime(EntryIndex: Integer): String;

      function  get_DirListIsDir(EntryIndex: Integer): Boolean;

      function  get_DirListIsSymlink(EntryIndex: Integer): Boolean;

      function  get_FileAccessTime: Int64;

      procedure set_FileAccessTime(valFileAccessTime: Int64);

      function  get_FileACL: String;

      procedure set_FileACL(valFileACL: String);

      function  get_FileAllocationSize: Int64;

      function  get_FileAttributeBits: Integer;

      function  get_FileAttributeBitsValid: Integer;

      function  get_FileCreationTime: Int64;

      procedure set_FileCreationTime(valFileCreationTime: Int64);

      function  get_FileType: TiphsftpFileTypes;

      function  get_FileGroupId: String;

      procedure set_FileGroupId(valFileGroupId: String);

      function  get_FileIsDir: Boolean;

      function  get_FileIsSymlink: Boolean;

      function  get_FileModifiedTime: Int64;

      procedure set_FileModifiedTime(valFileModifiedTime: Int64);

      function  get_FileOwnerId: String;

      procedure set_FileOwnerId(valFileOwnerId: String);

      function  get_FilePermissions: Integer;

      procedure set_FilePermissions(valFilePermissions: Integer);

      function  get_FilePermissionsOctal: String;

      procedure set_FilePermissionsOctal(valFilePermissionsOctal: String);

      function  get_FileSize: Int64;

      function  get_FileExists: Boolean;

      function  get_FirewallAutoDetect: Boolean;

      procedure set_FirewallAutoDetect(valFirewallAutoDetect: Boolean);

      function  get_FirewallType: TiphsftpFirewallTypes;

      procedure set_FirewallType(valFirewallType: TiphsftpFirewallTypes);

      function  get_FirewallHost: String;

      procedure set_FirewallHost(valFirewallHost: String);

      function  get_FirewallPassword: String;

      procedure set_FirewallPassword(valFirewallPassword: String);

      function  get_FirewallPort: Integer;

      procedure set_FirewallPort(valFirewallPort: Integer);

      function  get_FirewallUser: String;

      procedure set_FirewallUser(valFirewallUser: String);

      function  get_Idle: Boolean;

      function  get_LocalFile: String;

      procedure set_LocalFile(valLocalFile: String);

      function  get_LocalHost: String;

      procedure set_LocalHost(valLocalHost: String);

      function  get_LocalPort: Integer;

      procedure set_LocalPort(valLocalPort: Integer);

      function  get_Overwrite: Boolean;

      procedure set_Overwrite(valOverwrite: Boolean);

      function  get_RemoteFile: String;

      procedure set_RemoteFile(valRemoteFile: String);

      function  get_RemotePath: String;

      procedure set_RemotePath(valRemotePath: String);

      function  get_SSHAcceptServerHostKeyEncoded: String;

      procedure set_SSHAcceptServerHostKeyEncoded(valSSHAcceptServerHostKeyEncoded: String);

      function  get_SSHAcceptServerHostKeyEncodedB: TBytes;

      procedure set_SSHAcceptServerHostKeyEncodedB(valSSHAcceptServerHostKeyEncoded: TBytes);

      function  get_SSHAuthMode: TiphsftpSSHAuthModes;

      procedure set_SSHAuthMode(valSSHAuthMode: TiphsftpSSHAuthModes);

      function  get_SSHCertEncoded: String;

      procedure set_SSHCertEncoded(valSSHCertEncoded: String);

      function  get_SSHCertEncodedB: TBytes;

      procedure set_SSHCertEncodedB(valSSHCertEncoded: TBytes);

      function  get_SSHCertStore: String;

      procedure set_SSHCertStore(valSSHCertStore: String);

      function  get_SSHCertStoreB: TBytes;

      procedure set_SSHCertStoreB(valSSHCertStore: TBytes);

      function  get_SSHCertStorePassword: String;

      procedure set_SSHCertStorePassword(valSSHCertStorePassword: String);

      function  get_SSHCertStoreType: TiphsftpSSHCertStoreTypes;

      procedure set_SSHCertStoreType(valSSHCertStoreType: TiphsftpSSHCertStoreTypes);

      function  get_SSHCertSubject: String;

      procedure set_SSHCertSubject(valSSHCertSubject: String);

      function  get_SSHCompressionAlgorithms: String;

      procedure set_SSHCompressionAlgorithms(valSSHCompressionAlgorithms: String);

      function  get_SSHEncryptionAlgorithms: String;

      procedure set_SSHEncryptionAlgorithms(valSSHEncryptionAlgorithms: String);

      function  get_SSHHost: String;

      procedure set_SSHHost(valSSHHost: String);

      function  get_SSHPassword: String;

      procedure set_SSHPassword(valSSHPassword: String);

      function  get_SSHPort: Integer;

      procedure set_SSHPort(valSSHPort: Integer);

      function  get_SSHUser: String;

      procedure set_SSHUser(valSSHUser: String);

      function  get_StartByte: Int64;

      procedure set_StartByte(valStartByte: Int64);

      function  get_Timeout: Integer;

      procedure set_Timeout(valTimeout: Integer);


      (*** Property Getters/Setters: OO API ***)
      function  get_DirList: TiphDirEntryList;
      function  get_FileAttributes: TiphSFTPFileAttributes;
      procedure set_FileAttributes(Value : TiphSFTPFileAttributes);
      
       function  get_Firewall: TiphFirewall;
      procedure set_Firewall(Value : TiphFirewall);
      
 
    public
      constructor Create(AOwner: TComponent); overload; override;
      constructor Create(AOwner: TComponent; RuntimeLicense: string); reintroduce; overload;
      destructor Destroy; override;
      function ReportEventException(E: Exception; Event: String; ReFire: Boolean = True): Integer;

      property RuntimeLicense: String read GetRuntimeLicense write SetRuntimeLicense;

      procedure SetSSHAcceptServerHostKeyEncoded(lpSSHAcceptServerHostKeyEncoded: PLXAnsiChar; lenSSHAcceptServerHostKeyEncoded: Cardinal);

      procedure SetSSHCertEncoded(lpSSHCertEncoded: PLXAnsiChar; lenSSHCertEncoded: Cardinal);

      procedure SetSSHCertStore(lpSSHCertStore: PLXAnsiChar; lenSSHCertStore: Cardinal);


      (*** Runtime Property Definitions ***)

      property Connected: Boolean read get_Connected write set_Connected;



      property DirListCount: Integer read get_DirListCount;



      property DirListEntry[EntryIndex: Integer]: String read get_DirListEntry;



      property DirListFileName[EntryIndex: Integer]: String read get_DirListFileName;



      property DirListFileSize[EntryIndex: Integer]: Int64 read get_DirListFileSize;



      property DirListFileTime[EntryIndex: Integer]: String read get_DirListFileTime;



      property DirListIsDir[EntryIndex: Integer]: Boolean read get_DirListIsDir;



      property DirListIsSymlink[EntryIndex: Integer]: Boolean read get_DirListIsSymlink;



      property FileAccessTime: Int64 read get_FileAccessTime write set_FileAccessTime;



      property FileACL: String read get_FileACL write set_FileACL;



      property FileAllocationSize: Int64 read get_FileAllocationSize;



      property FileAttributeBits: Integer read get_FileAttributeBits;



      property FileAttributeBitsValid: Integer read get_FileAttributeBitsValid;



      property FileCreationTime: Int64 read get_FileCreationTime write set_FileCreationTime;



      property FileType: TiphsftpFileTypes read get_FileType;



      property FileGroupId: String read get_FileGroupId write set_FileGroupId;



      property FileIsDir: Boolean read get_FileIsDir;



      property FileIsSymlink: Boolean read get_FileIsSymlink;



      property FileModifiedTime: Int64 read get_FileModifiedTime write set_FileModifiedTime;



      property FileOwnerId: String read get_FileOwnerId write set_FileOwnerId;



      property FilePermissions: Integer read get_FilePermissions write set_FilePermissions;



      property FilePermissionsOctal: String read get_FilePermissionsOctal write set_FilePermissionsOctal;



      property FileSize: Int64 read get_FileSize;



      property FileExists: Boolean read get_FileExists;



      property SSHAcceptServerHostKeyEncoded: String read get_SSHAcceptServerHostKeyEncoded write set_SSHAcceptServerHostKeyEncoded;



      property SSHAcceptServerHostKeyEncodedB: TBytes read get_SSHAcceptServerHostKeyEncodedB write set_SSHAcceptServerHostKeyEncodedB;



      property SSHCertEncoded: String read get_SSHCertEncoded write set_SSHCertEncoded;



      property SSHCertEncodedB: TBytes read get_SSHCertEncodedB write set_SSHCertEncodedB;



      property SSHCertStoreB: TBytes read get_SSHCertStoreB write set_SSHCertStoreB;



      (*** Runtime properties: OO API ***)
      property DirList: TiphDirEntryList read get_DirList;

      property FileAttributes: TiphSFTPFileAttributes read get_FileAttributes write set_FileAttributes;

      property Firewall: TiphFirewall read get_Firewall write set_Firewall;


{$IFNDEF DELPHI3}
      (*** Method Definitions ***)
      procedure Append();

      procedure ChangeRemotePath(RemotePath: String);

      function  CheckFileExists(): Boolean;

      function  Config(ConfigurationString: String): String;

      procedure Connect();

      procedure CreateFile(FileName: String);

      function  DecodePacket(EncodedPacket: String): TBytes;

      procedure DeleteFile(FileName: String);

      procedure Disconnect();

      procedure DoEvents();

      procedure Download();

      function  EncodePacket(Packet: TBytes): String;

      function  GetSSHParam(Payload: TBytes; Field: String): String;

      function  GetSSHParamBytes(Payload: TBytes; Field: String): TBytes;

      procedure Interrupt();

      procedure ListDirectory();

      procedure MakeDirectory(NewDir: String);

      procedure QueryFileAttributes();

      procedure QueueFile(LocalFile: String; RemoteFile: String);

      procedure RemoveDirectory(DirName: String);

      procedure RenameFile(NewName: String);

      procedure Reset();

      procedure ResetQueue();

      procedure SetDownloadStream(DownloadStream: TStream);

      function  SetSSHParam(Payload: TBytes; FieldType: String; FieldValue: String): TBytes;

      procedure SetUploadStream(UploadStream: TStream);

      procedure SSHLogoff();

      procedure SSHLogon(SSHHost: String; SSHPort: Integer);

      procedure UpdateFileAttributes();

      procedure Upload();

{$ENDIF}

    published
      (*** Design-time Property Definitions ***)

      property FirewallAutoDetect: Boolean read get_FirewallAutoDetect write set_FirewallAutoDetect default False;



      property FirewallType: TiphsftpFirewallTypes read get_FirewallType write set_FirewallType default fwNone;



      property FirewallHost: String read get_FirewallHost write set_FirewallHost;



      property FirewallPassword: String read get_FirewallPassword write set_FirewallPassword;



      property FirewallPort: Integer read get_FirewallPort write set_FirewallPort default 0;



      property FirewallUser: String read get_FirewallUser write set_FirewallUser;



      property Idle: Boolean read get_Idle write SetNoopBoolean stored False;



      property LocalFile: String read get_LocalFile write set_LocalFile;



      property LocalHost: String read get_LocalHost write set_LocalHost stored False;



      property LocalPort: Integer read get_LocalPort write set_LocalPort default 0;



      property Overwrite: Boolean read get_Overwrite write set_Overwrite default false;



      property RemoteFile: String read get_RemoteFile write set_RemoteFile;



      property RemotePath: String read get_RemotePath write set_RemotePath;



      property SSHAuthMode: TiphsftpSSHAuthModes read get_SSHAuthMode write set_SSHAuthMode default amPassword;



      property SSHCertStore: String read get_SSHCertStore write set_SSHCertStore;



      property SSHCertStorePassword: String read get_SSHCertStorePassword write set_SSHCertStorePassword;



      property SSHCertStoreType: TiphsftpSSHCertStoreTypes read get_SSHCertStoreType write set_SSHCertStoreType default cstUser;



      property SSHCertSubject: String read get_SSHCertSubject write set_SSHCertSubject;



      property SSHCompressionAlgorithms: String read get_SSHCompressionAlgorithms write set_SSHCompressionAlgorithms;



      property SSHEncryptionAlgorithms: String read get_SSHEncryptionAlgorithms write set_SSHEncryptionAlgorithms;



      property SSHHost: String read get_SSHHost write set_SSHHost;



      property SSHPassword: String read get_SSHPassword write set_SSHPassword;



      property SSHPort: Integer read get_SSHPort write set_SSHPort default 22;



      property SSHUser: String read get_SSHUser write set_SSHUser;



      property StartByte: Int64 read get_StartByte write set_StartByte default 0;



      property Timeout: Integer read get_Timeout write set_Timeout default 60;


      (*** Event Handler Bindings ***)
      property OnConnected: TConnectedEvent read FOnConnected write FOnConnected;

      property OnConnectionStatus: TConnectionStatusEvent read FOnConnectionStatus write FOnConnectionStatus;

      property OnDirList: TDirListEvent read FOnDirList write FOnDirList;

      property OnDisconnected: TDisconnectedEvent read FOnDisconnected write FOnDisconnected;

      property OnEndTransfer: TEndTransferEvent read FOnEndTransfer write FOnEndTransfer;

      property OnError: TErrorEvent read FOnError write FOnError;

      property OnLog: TLogEvent read FOnLog write FOnLog;

      property OnSSHCustomAuth: TSSHCustomAuthEvent read FOnSSHCustomAuth write FOnSSHCustomAuth;

      property OnSSHKeyboardInteractive: TSSHKeyboardInteractiveEvent read FOnSSHKeyboardInteractive write FOnSSHKeyboardInteractive;

      property OnSSHServerAuthentication: TSSHServerAuthenticationEvent read FOnSSHServerAuthentication write FOnSSHServerAuthentication;

      property OnSSHStatus: TSSHStatusEvent read FOnSSHStatus write FOnSSHStatus;

      property OnStartTransfer: TStartTransferEvent read FOnStartTransfer write FOnStartTransfer;

      property OnTransfer: TTransferEvent read FOnTransfer write FOnTransfer;

    end;

    procedure Register;

implementation


{$T-}

{$IFDEF MSWINDOWS} // Both DLL and DRU loading require Windows dependency
{$IFDEF FPC}  //Lazarus
uses Messages;
{$ELSE}
uses {$IF CompilerVersion >= 23}Winapi.Windows{$ELSE}Windows{$IFEND}, {$IF CompilerVersion >= 23}Winapi.Messages{$ELSE}Messages{$IFEND};
{$ENDIF}
{$ENDIF}

const
  PID_SFTP_Connected                                          = 1;

  PID_SFTP_DirListCount                                       = 2;

  PID_SFTP_DirListEntry                                       = 3;

  PID_SFTP_DirListFileName                                    = 4;

  PID_SFTP_DirListFileSize                                    = 5;

  PID_SFTP_DirListFileTime                                    = 6;

  PID_SFTP_DirListIsDir                                       = 7;

  PID_SFTP_DirListIsSymlink                                   = 8;

  PID_SFTP_FileAccessTime                                     = 9;

  PID_SFTP_FileAccessTimeNanos                                = 10;

  PID_SFTP_FileACL                                            = 11;

  PID_SFTP_FileAllocationSize                                 = 12;

  PID_SFTP_FileAttributeBits                                  = 13;

  PID_SFTP_FileAttributeBitsValid                             = 14;

  PID_SFTP_FileCreationTime                                   = 15;

  PID_SFTP_FileCreationTimeNanos                              = 16;

  PID_SFTP_FileType                                           = 17;

  PID_SFTP_FileFlags                                          = 18;

  PID_SFTP_FileGroupId                                        = 19;

  PID_SFTP_FileIsDir                                          = 20;

  PID_SFTP_FileIsSymlink                                      = 21;

  PID_SFTP_FileLinkCount                                      = 22;

  PID_SFTP_FileMIMEType                                       = 23;

  PID_SFTP_FileModifiedTime                                   = 24;

  PID_SFTP_FileModifiedTimeNanos                              = 25;

  PID_SFTP_FileOwnerId                                        = 26;

  PID_SFTP_FilePermissions                                    = 27;

  PID_SFTP_FilePermissionsOctal                               = 28;

  PID_SFTP_FileSize                                           = 29;

  PID_SFTP_FileTextHint                                       = 30;

  PID_SFTP_FileUntranslatedName                               = 31;

  PID_SFTP_FileExists                                         = 32;

  PID_SFTP_FirewallAutoDetect                                 = 33;

  PID_SFTP_FirewallType                                       = 34;

  PID_SFTP_FirewallHost                                       = 35;

  PID_SFTP_FirewallPassword                                   = 36;

  PID_SFTP_FirewallPort                                       = 37;

  PID_SFTP_FirewallUser                                       = 38;

  PID_SFTP_Idle                                               = 39;

  PID_SFTP_LocalFile                                          = 40;

  PID_SFTP_LocalHost                                          = 41;

  PID_SFTP_LocalPort                                          = 42;

  PID_SFTP_Overwrite                                          = 43;

  PID_SFTP_RemoteFile                                         = 44;

  PID_SFTP_RemotePath                                         = 45;

  PID_SFTP_SSHAcceptServerHostKeyEncoded                      = 47;

  PID_SFTP_SSHAuthMode                                        = 74;

  PID_SFTP_SSHCertEncoded                                     = 76;

  PID_SFTP_SSHCertStore                                       = 92;

  PID_SFTP_SSHCertStorePassword                               = 93;

  PID_SFTP_SSHCertStoreType                                   = 94;

  PID_SFTP_SSHCertSubject                                     = 95;

  PID_SFTP_SSHCompressionAlgorithms                           = 103;

  PID_SFTP_SSHEncryptionAlgorithms                            = 104;

  PID_SFTP_SSHHost                                            = 105;

  PID_SFTP_SSHPassword                                        = 106;

  PID_SFTP_SSHPort                                            = 107;

  PID_SFTP_SSHUser                                            = 108;

  PID_SFTP_StartByte                                          = 109;

  PID_SFTP_Timeout                                            = 110;

  EID_SFTP_Connected = 1;

  EID_SFTP_ConnectionStatus = 2;

  EID_SFTP_DirList = 3;

  EID_SFTP_Disconnected = 4;

  EID_SFTP_EndTransfer = 5;

  EID_SFTP_Error = 6;

  EID_SFTP_Log = 7;

  EID_SFTP_SSHCustomAuth = 8;

  EID_SFTP_SSHKeyboardInteractive = 9;

  EID_SFTP_SSHServerAuthentication = 10;

  EID_SFTP_SSHStatus = 11;

  EID_SFTP_StartTransfer = 12;

  EID_SFTP_Transfer = 13;

  MID_SFTP_Append = 2;

  MID_SFTP_ChangeRemotePath = 3;

  MID_SFTP_CheckFileExists = 4;

  MID_SFTP_Config = 5;

  MID_SFTP_Connect = 6;

  MID_SFTP_CreateFile = 7;

  MID_SFTP_DecodePacket = 8;

  MID_SFTP_DeleteFile = 9;

  MID_SFTP_Disconnect = 10;

  MID_SFTP_DoEvents = 11;

  MID_SFTP_Download = 12;

  MID_SFTP_EncodePacket = 13;

  MID_SFTP_GetSSHParam = 14;

  MID_SFTP_GetSSHParamBytes = 15;

  MID_SFTP_Interrupt = 16;

  MID_SFTP_ListDirectory = 17;

  MID_SFTP_MakeDirectory = 18;

  MID_SFTP_QueryFileAttributes = 19;

  MID_SFTP_QueueFile = 20;

  MID_SFTP_RemoveDirectory = 21;

  MID_SFTP_RenameFile = 22;

  MID_SFTP_Reset = 23;

  MID_SFTP_ResetQueue = 24;

  MID_SFTP_SetDownloadStream = 25;

  MID_SFTP_SetSSHParam = 26;

  MID_SFTP_SetUploadStream = 27;

  MID_SFTP_SSHLogoff = 28;

  MID_SFTP_SSHLogon = 29;

  MID_SFTP_UpdateFileAttributes = 30;

  MID_SFTP_Upload = 31;

  CREATE_OPT = {$IFDEF UNICODE}1{$ELSE}0{$ENDIF};
  EVTSTR_OPT = {$IFDEF UNICODE}2{$ELSE}1{$ENDIF};

  {$IFDEF MSWINDOWS}
  DLLNAME = 'IPWORKSSSH22.DLL';
  {$ENDIF}
  {$IFDEF LINUX}
  DLLNAME = 'libipworksssh.so.22.0';
  {$ENDIF}
  {$IFDEF ANDROID}
  {$IFDEF USE_DYNAMIC_LOADING}
  DLLNAME = 'libipworksssh.so.22.0';
  {$ELSE}
  DLLNAME = 'libipworksssh.22.0.a';
  {$ENDIF}
  {$ENDIF}
  {$IFDEF IOS}
  DLLNAME = 'libipworksssh.22.0.a';
  {$ELSE}
  {$IFDEF MACOS}
  DLLNAME = '@rpath/libipworksssh.22.0.dylib';
  {$ENDIF}
  {$ENDIF}

{$WARNINGS OFF}

type
  INTARR = array[0..32] of Integer;
  LPINTARR = ^INTARR;
  LPINT = ^Cardinal;
  MEventHandle = function(lpContext: TiphSFTP; event_id: Integer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  PEventHandle = ^MEventHandle;


{$IFNDEF USEIPWORKSSSHDLL}
var
  pEntryPoint: Pointer;
  pBaseAddress: Pointer;
  iLoadCount: Integer;
{$ELSE}
{$IFDEF USE_DYNAMIC_LOADING}
var
  hLib : {$ifndef FPC}HMODULE{$else}TLibHandle{$endif};
  iLoadCount: Integer;
{$ENDIF USE_DYNAMIC_LOADING}
{$ENDIF USEIPWORKSSSHDLL}


{$IFDEF USE_DYNAMIC_LOADING}
  _IPWorksSSH_EvtStr:                     function(pEvtStr: Pointer; id: Integer; value: Pointer; opt: Integer): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_Stream:                     function(pStream: Pointer; op: Integer; params: LPVOIDARR; retVal: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SFTP_Create:                function(pMethod: PEventHandle; pObject: TiphSFTP; pKey: Pointer; opts: Integer): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SFTP_Destroy:               function(p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SFTP_Set:                   function(p: Pointer; index: Integer; arridx: Integer; value: Pointer; len: Cardinal): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SFTP_Get:                   function(p: Pointer; index: Integer; arridx: Integer; len: LPINT; llVal: PInt64): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SFTP_GetLastError:          function(p: Pointer): PLXAnsiChar; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SFTP_GetLastErrorCode:      function(p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SFTP_SetLastErrorAndCode:   function(p: Pointer; code: Integer; message: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SFTP_GetEventError:         function(p: Pointer): PLXAnsiChar; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SFTP_GetEventErrorCode:     function(p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SFTP_SetEventErrorAndCode:  function(p: Pointer; code: Integer; message: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  {$IFDEF USE_EXPLICIT_INIT}
  _IPWorksSSH_SFTP_StaticInit:            function(instance: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SFTP_StaticDestroy:         function(): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  {$ENDIF}
  _IPWorksSSH_SFTP_CheckIndex:            function(p: Pointer; index: Integer; arridx: Integer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SFTP_Do:                    function(p: Pointer; method_id: Integer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR; llVal: PInt64): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
{$ELSE}
  function _IPWorksSSH_EvtStr                    (pEvtStr: Pointer; id: Integer; value: Pointer; opt: Integer): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_EvtStr' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_Stream                    (pStream: Pointer; op: Integer; params: LPVOIDARR; retVal: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_Stream' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SFTP_Create               (pMethod: PEventHandle; pObject: TiphSFTP; pKey: Pointer; opts: Integer): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SFTP_Create' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SFTP_Destroy              (p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SFTP_Destroy' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SFTP_Set                  (p: Pointer; index: Integer; arridx: Integer; value: Pointer; len: Cardinal): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SFTP_Set' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SFTP_Get                  (p: Pointer; index: Integer; arridx: Integer; len: LPINT; llVal: PInt64): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SFTP_Get' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SFTP_GetLastError         (p: Pointer): PLXAnsiChar; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SFTP_GetLastError' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SFTP_GetLastErrorCode     (p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SFTP_GetLastErrorCode' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SFTP_SetLastErrorAndCode  (p: Pointer; code: Integer; message: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SFTP_SetLastErrorAndCode' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SFTP_GetEventError        (p: Pointer): PLXAnsiChar; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SFTP_GetEventError' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SFTP_GetEventErrorCode    (p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SFTP_GetEventErrorCode' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SFTP_SetEventErrorAndCode (p: Pointer; code: Integer; message: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SFTP_SetEventErrorAndCode' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SFTP_StaticInit           (instance: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SFTP_StaticInit' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SFTP_StaticDestroy        (): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SFTP_StaticDestroy' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SFTP_CheckIndex           (p: Pointer; index: Integer; arridx: Integer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SFTP_CheckIndex' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SFTP_Do                   (p: Pointer; method_id: Integer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR; llVal: PInt64): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SFTP_Do' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
{$ENDIF}

{$HINTS OFF}

(*** Fire Event Methods ***)
function FireConnected(lpContext: TiphSFTP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_StatusCode: Integer;

  tmp_Description: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnConnected) then exit;

  tmp_StatusCode := Integer(params^[0]);

  tmp_Description := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  try lpContext.FOnConnected(lpContext, tmp_StatusCode, tmp_Description)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Connected'); end;

end;


function FireConnectionStatus(lpContext: TiphSFTP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionEvent: String;

  tmp_StatusCode: Integer;

  tmp_Description: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnConnectionStatus) then exit;

  tmp_ConnectionEvent := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[0]);
  tmp_StatusCode := Integer(params^[1]);

  tmp_Description := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  try lpContext.FOnConnectionStatus(lpContext, tmp_ConnectionEvent, tmp_StatusCode, tmp_Description)
  except on E: Exception do result := lpContext.ReportEventException(E, 'ConnectionStatus'); end;

end;


function FireDirList(lpContext: TiphSFTP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_DirEntry: String;

  tmp_FileName: String;

  tmp_IsDir: Boolean;

  tmp_FileSize: Int64;

  tmp_FileTime: String;

  tmp_IsSymlink: Boolean;

begin
  result := 0;
  if not Assigned(lpContext.FOnDirList) then exit;

  tmp_DirEntry := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[0]);
  tmp_FileName := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_IsDir := Boolean(params^[2]);

  tmp_FileSize := PInt64(params^[3])^;
  tmp_FileTime := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[4]);
  tmp_IsSymlink := Boolean(params^[5]);

  try lpContext.FOnDirList(lpContext, tmp_DirEntry, tmp_FileName, tmp_IsDir, tmp_FileSize, tmp_FileTime, tmp_IsSymlink)
  except on E: Exception do result := lpContext.ReportEventException(E, 'DirList'); end;

end;


function FireDisconnected(lpContext: TiphSFTP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_StatusCode: Integer;

  tmp_Description: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnDisconnected) then exit;

  tmp_StatusCode := Integer(params^[0]);

  tmp_Description := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  try lpContext.FOnDisconnected(lpContext, tmp_StatusCode, tmp_Description)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Disconnected'); end;

end;


function FireEndTransfer(lpContext: TiphSFTP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_Direction: Integer;

  tmp_LocalFile: String;

  tmp_RemoteFile: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnEndTransfer) then exit;

  tmp_Direction := Integer(params^[0]);

  tmp_LocalFile := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_RemoteFile := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  try lpContext.FOnEndTransfer(lpContext, tmp_Direction, tmp_LocalFile, tmp_RemoteFile)
  except on E: Exception do result := lpContext.ReportEventException(E, 'EndTransfer'); end;

end;


function FireError(lpContext: TiphSFTP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ErrorCode: Integer;

  tmp_Description: String;

  tmp_LocalFile: String;

  tmp_RemoteFile: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnError) then exit;

  tmp_ErrorCode := Integer(params^[0]);

  tmp_Description := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_LocalFile := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_RemoteFile := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[3]);
  try lpContext.FOnError(lpContext, tmp_ErrorCode, tmp_Description, tmp_LocalFile, tmp_RemoteFile)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Error', False); end;

end;


function FireLog(lpContext: TiphSFTP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_LogLevel: Integer;

  tmp_Message: String;

  tmp_LogType: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnLog) then exit;

  tmp_LogLevel := Integer(params^[0]);

  tmp_Message := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_LogType := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  try lpContext.FOnLog(lpContext, tmp_LogLevel, tmp_Message, tmp_LogType)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Log'); end;

end;


function FireSSHCustomAuth(lpContext: TiphSFTP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_Packet: String; // dev platform string
  {$ifdef UNICODE_ON_ANSI}
  tmp_PacketB: LXAnsiString; // library string
  {$endif}

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHCustomAuth) then exit;

  tmp_Packet := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(_IPWorksSSH_EvtStr(params^[0], 0, nil, EVTSTR_OPT));
  try lpContext.FOnSSHCustomAuth(lpContext, tmp_Packet)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHCustomAuth'); end;

  {$ifdef UNICODE_ON_ANSI}
  tmp_PacketB := ToLinuxAnsiString(tmp_Packet);
  {$endif}
  _IPWorksSSH_EvtStr(params^[0], 2, {$ifdef UNICODE_ON_ANSI}@tmp_PacketB[0]{$else}PChar(tmp_Packet){$endif}, EVTSTR_OPT);
end;


function FireSSHKeyboardInteractive(lpContext: TiphSFTP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_Name: String;

  tmp_Instructions: String;

  tmp_Prompt: String;

  tmp_Response: String; // dev platform string
  {$ifdef UNICODE_ON_ANSI}
  tmp_ResponseB: LXAnsiString; // library string
  {$endif}

  tmp_EchoResponse: Boolean;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHKeyboardInteractive) then exit;

  tmp_Name := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[0]);
  tmp_Instructions := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_Prompt := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_Response := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(_IPWorksSSH_EvtStr(params^[3], 0, nil, EVTSTR_OPT));
  tmp_EchoResponse := Boolean(params^[4]);

  try lpContext.FOnSSHKeyboardInteractive(lpContext, tmp_Name, tmp_Instructions, tmp_Prompt, tmp_Response, tmp_EchoResponse)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHKeyboardInteractive'); end;

  {$ifdef UNICODE_ON_ANSI}
  tmp_ResponseB := ToLinuxAnsiString(tmp_Response);
  {$endif}
  _IPWorksSSH_EvtStr(params^[3], 2, {$ifdef UNICODE_ON_ANSI}@tmp_ResponseB[0]{$else}PChar(tmp_Response){$endif}, EVTSTR_OPT);
end;


function FireSSHServerAuthentication(lpContext: TiphSFTP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_HostKey: String;
  tmp_HostKeyB: TBytes;

  tmp_Fingerprint: String;

  tmp_KeyAlgorithm: String;

  tmp_CertSubject: String;

  tmp_CertIssuer: String;

  tmp_Status: String;

  tmp_Accept: Boolean;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHServerAuthentication) then exit;

  SetLength(tmp_HostKeyB, cbparam^[0]);
  Move(Pointer(params^[0])^, Pointer(tmp_HostKeyB)^, cbparam^[0]); // IMPORTANT: Do NOT multiply the length value!
  tmp_HostKey := lpContext.BStr2CStr(params^[0], cbparam^[0], tmp_HostKeyB);
  tmp_Fingerprint := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_KeyAlgorithm := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_CertSubject := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[3]);
  tmp_CertIssuer := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[4]);
  tmp_Status := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[5]);
  tmp_Accept := Boolean(params^[6]);

  try lpContext.FOnSSHServerAuthentication(lpContext, tmp_HostKey, tmp_HostKeyB, tmp_Fingerprint, tmp_KeyAlgorithm, tmp_CertSubject, tmp_CertIssuer, tmp_Status, tmp_Accept)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHServerAuthentication'); end;

  params^[6] := Pointer(tmp_Accept);

end;


function FireSSHStatus(lpContext: TiphSFTP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_Message: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHStatus) then exit;

  tmp_Message := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[0]);
  try lpContext.FOnSSHStatus(lpContext, tmp_Message)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHStatus'); end;

end;


function FireStartTransfer(lpContext: TiphSFTP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_Direction: Integer;

  tmp_LocalFile: String;

  tmp_RemoteFile: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnStartTransfer) then exit;

  tmp_Direction := Integer(params^[0]);

  tmp_LocalFile := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_RemoteFile := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  try lpContext.FOnStartTransfer(lpContext, tmp_Direction, tmp_LocalFile, tmp_RemoteFile)
  except on E: Exception do result := lpContext.ReportEventException(E, 'StartTransfer'); end;

end;


function FireTransfer(lpContext: TiphSFTP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_Direction: Integer;

  tmp_LocalFile: String;

  tmp_RemoteFile: String;

  tmp_BytesTransferred: Int64;

  tmp_PercentDone: Integer;

  tmp_Text: String;
  tmp_TextB: TBytes;

  tmp_Cancel: Boolean;

begin
  result := 0;
  if not Assigned(lpContext.FOnTransfer) then exit;

  tmp_Direction := Integer(params^[0]);

  tmp_LocalFile := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_RemoteFile := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_BytesTransferred := PInt64(params^[3])^;
  tmp_PercentDone := Integer(params^[4]);

  SetLength(tmp_TextB, cbparam^[5]);
  Move(Pointer(params^[5])^, Pointer(tmp_TextB)^, cbparam^[5]); // IMPORTANT: Do NOT multiply the length value!
  tmp_Text := lpContext.BStr2CStr(params^[5], cbparam^[5], tmp_TextB);
  tmp_Cancel := Boolean(params^[6]);

  try lpContext.FOnTransfer(lpContext, tmp_Direction, tmp_LocalFile, tmp_RemoteFile, tmp_BytesTransferred, tmp_PercentDone, tmp_Text, tmp_TextB, tmp_Cancel)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Transfer'); end;

  params^[6] := Pointer(tmp_Cancel);

end;


function MapID2Stream(lpContext: TiphSFTP; params: LPVOIDARR) : TStream;
var
  streamID : Integer;
  arrayidx : Integer;
begin
  streamID := PInteger(params^[0])^;
  if (streamID and $40000000) = 0 then
  begin
    arrayidx := (streamID and $BFFFFFFF) shr 16;
    streamID := (streamID and $0000FFFF);
    if streamID = MID_SFTP_SetDownloadStream then
    begin
      if (lpContext.m_streamFromSetDownloadStream <> nil) and (arrayidx < lpContext.m_streamFromSetDownloadStream.Count) then
        result := lpContext.m_streamFromSetDownloadStream.Items[arrayidx]
      else 
        result := nil;
      exit;
    end;

    if streamID = MID_SFTP_SetUploadStream then
    begin
      if (lpContext.m_streamFromSetUploadStream <> nil) and (arrayidx < lpContext.m_streamFromSetUploadStream.Count) then
        result := lpContext.m_streamFromSetUploadStream.Items[arrayidx]
      else 
        result := nil;
      exit;
    end;

  end;
  if (streamID and $40000000) = $40000000 then
  begin
    arrayidx := (streamID and $BFFFFFFF) shr 16;
    streamID := (streamID and $0000FFFF);
  
  end;
  result := nil;
end;

function FireStreamEvents_Read(lpContext: TiphSFTP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_stream: TStream;
  x: Integer;
begin
  result := 0;
  try
    tmp_stream := MapID2Stream(lpContext, params);
    if tmp_stream = nil then
    begin
      result := -10000;
      exit;
    end;
    x := tmp_stream.Read(PByte(params^[1])^, cbparam^[1]);
    cbparam^[1] := x;
  except on E: Exception do
    begin
      result := -10000;
      exit;
    end;
  end;
end;

function FireStreamEvents_Write(lpContext: TiphSFTP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_stream: TStream;
  x: Integer;
begin
  result := 0;
  try
    tmp_stream := MapID2Stream(lpContext, params);
    if tmp_stream = nil then
    begin
      result := -10000;
      exit;
    end;
    x := tmp_stream.Write(PByte(params^[1])^, cbparam^[1]);
    cbparam^[1] := x;
  except on E: Exception do
    begin
      result := -10000;
      exit;
    end;
  end;
end;

function FireStreamEvents_Size(lpContext: TiphSFTP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_stream: TStream;
begin
  result := 0;
  try
    tmp_stream := MapID2Stream(lpContext, params);
    if tmp_stream = nil then
    begin
      result := -10000;
      exit;
    end;
    (PInt64(params^[1]))^ := tmp_stream.Size;
  except on E: Exception do
    begin
      result := -10000;
      exit;
    end;
  end;
end;

function FireStreamEvents_Close(lpContext: TiphSFTP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_stream: TStream;
begin
  result := 0;
  try
    tmp_stream := MapID2Stream(lpContext, params);
    if tmp_stream = nil then
    begin
      result := -10000;
      exit;
    end;
    tmp_stream.Free;
  except on E: Exception do
    begin
      result := -10000;
      exit;
    end;
  end;
end;

function FireStreamEvents_Seek(lpContext: TiphSFTP; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var 
  tmp_stream: TStream;
  tmp_seeklen: Integer;
  tmp_seekpos: Integer;
  x: Int64;
begin
  result := 0;
  try
    tmp_stream := MapID2Stream(lpContext, params);
    if tmp_stream = nil then
    begin
      result := -10000;
      exit;
    end;
    tmp_seeklen := PInt64(params^[1])^;
    tmp_seekpos := PInteger(params^[2])^;
    case tmp_seekpos of
      STREAM_SEEK_FROM_END: begin x := tmp_stream.Seek(tmp_seeklen, soEnd); end;
      STREAM_SEEK_FROM_BEGIN: begin x := tmp_stream.Seek(tmp_seeklen, soBeginning); end;
      STREAM_SEEK_FROM_CURRENT: begin x := tmp_stream.Seek(tmp_seeklen, soCurrent); end;
    end;
    (PInt64(params^[1]))^ := x;
  except on E : Exception do
    begin
      result := -10000;
      exit;
    end;
  end;
end;


(*** Event Sink ***)
function FireEvents(lpContext: TiphSFTP; event_id: Integer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
var
  x: Integer;
begin
  result := 0;

  case event_id of
    EID_SFTP_Connected: begin result := FireConnected(lpContext, cparam, params, cbparam); end;

    EID_SFTP_ConnectionStatus: begin result := FireConnectionStatus(lpContext, cparam, params, cbparam); end;

    EID_SFTP_DirList: begin result := FireDirList(lpContext, cparam, params, cbparam); end;

    EID_SFTP_Disconnected: begin result := FireDisconnected(lpContext, cparam, params, cbparam); end;

    EID_SFTP_EndTransfer: begin result := FireEndTransfer(lpContext, cparam, params, cbparam); end;

    EID_SFTP_Error: begin result := FireError(lpContext, cparam, params, cbparam); end;

    EID_SFTP_Log: begin result := FireLog(lpContext, cparam, params, cbparam); end;

    EID_SFTP_SSHCustomAuth: begin result := FireSSHCustomAuth(lpContext, cparam, params, cbparam); end;

    EID_SFTP_SSHKeyboardInteractive: begin result := FireSSHKeyboardInteractive(lpContext, cparam, params, cbparam); end;

    EID_SFTP_SSHServerAuthentication: begin result := FireSSHServerAuthentication(lpContext, cparam, params, cbparam); end;

    EID_SFTP_SSHStatus: begin result := FireSSHStatus(lpContext, cparam, params, cbparam); end;

    EID_SFTP_StartTransfer: begin result := FireStartTransfer(lpContext, cparam, params, cbparam); end;

    EID_SFTP_Transfer: begin result := FireTransfer(lpContext, cparam, params, cbparam); end;

    STREAM_OP_READ: begin result := FireStreamEvents_Read(lpContext, cparam, params, cbparam); end;
    STREAM_OP_WRITE: begin result := FireStreamEvents_Write(lpContext, cparam, params, cbparam); end;
    STREAM_OP_GET_LENGTH: begin result := FireStreamEvents_Size(lpContext, cparam, params, cbparam); end;
    STREAM_OP_CLOSE: begin result := FireStreamEvents_Close(lpContext, cparam, params, cbparam); end;
    STREAM_OP_SEEK: begin result := FireStreamEvents_Seek(lpContext, cparam, params, cbparam); end;
    99999: begin x := 0; end;
  end;
end;

procedure Register;
begin
  iphCore.Register;
  RegisterComponents('IPWorks SSH', [TiphSFTP]);
end;

{*********************************************************************************}
procedure TiphSFTP.SetSSHAcceptServerHostKeyEncoded(lpSSHAcceptServerHostKeyEncoded: PLXAnsiChar; lenSSHAcceptServerHostKeyEncoded: Cardinal);
var
  err: integer;
begin
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_SSHAcceptServerHostKeyEncoded, 0, Pointer(lpSSHAcceptServerHostKeyEncoded), lenSSHAcceptServerHostKeyEncoded);
  if err <> 0 then TreatErr(err, '');
end;

procedure TiphSFTP.SetSSHCertEncoded(lpSSHCertEncoded: PLXAnsiChar; lenSSHCertEncoded: Cardinal);
var
  err: integer;
begin
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_SSHCertEncoded, 0, Pointer(lpSSHCertEncoded), lenSSHCertEncoded);
  if err <> 0 then TreatErr(err, '');
end;

procedure TiphSFTP.SetSSHCertStore(lpSSHCertStore: PLXAnsiChar; lenSSHCertStore: Cardinal);
var
  err: integer;
begin
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_SSHCertStore, 0, Pointer(lpSSHCertStore), lenSSHCertStore);
  if err <> 0 then TreatErr(err, '');
end;


type
  TiphDirListDirEntryList = class(TiphDirEntryList)
  protected
    function OwnerCtl : TiphSFTP;
    // Only need to override one or two methods here
    function CtlGetCount: integer; override;
    procedure CtlSetCount(Value : integer); override;
    function CreateElemInstance(Index : integer): TiphDirEntry; override;
  end;

  TiphDirListDirEntry = class(TiphDirEntry)
  protected
    function OwnerCtl : TiphSFTP;
    function GetPropEntry: String; override;

    function GetPropFileName: String; override;

    function GetPropFileSize: Int64; override;

    function GetPropFileTime: String; override;

    function GetPropIsDir: Boolean; override;

    function GetPropIsSymlink: Boolean; override;

  end;

function TiphDirListDirEntry.OwnerCtl : TiphSFTP;
begin
  Result := TiphSFTP(FOwnerCtl);
end;

function TiphDirListDirEntry.GetPropEntry: String;
begin
  Result := (OwnerCtl.get_DirListEntry(Index));
end;


function TiphDirListDirEntry.GetPropFileName: String;
begin
  Result := (OwnerCtl.get_DirListFileName(Index));
end;


function TiphDirListDirEntry.GetPropFileSize: Int64;
begin
  Result := (OwnerCtl.get_DirListFileSize(Index));
end;


function TiphDirListDirEntry.GetPropFileTime: String;
begin
  Result := (OwnerCtl.get_DirListFileTime(Index));
end;


function TiphDirListDirEntry.GetPropIsDir: Boolean;
begin
  Result := (OwnerCtl.get_DirListIsDir(Index));
end;


function TiphDirListDirEntry.GetPropIsSymlink: Boolean;
begin
  Result := (OwnerCtl.get_DirListIsSymlink(Index));
end;



function TiphDirListDirEntryList.OwnerCtl : TiphSFTP;
begin
  Result := TiphSFTP(FOwnerCtl);
end;

// Collection's overrides
function TiphDirListDirEntryList.CtlGetCount: integer;
begin
  Result := OwnerCtl.get_DirListCount();
end;

procedure TiphDirListDirEntryList.CtlSetCount(Value : integer);
begin
  // The collection is read-only
end;

function TiphDirListDirEntryList.CreateElemInstance(Index : integer): TiphDirEntry;
begin
  Result := TiphDirListDirEntry.Create(FOwnerCtl, true);
  Result.Index := Index;
end;

type
  TiphFileAttributesSFTPFileAttributes = class(TiphSFTPFileAttributes)
  protected
    function OwnerCtl : TiphSFTP;
    function GetPropAccessTime: Int64; override;

    procedure SetPropAccessTime(Value: Int64); override;

    function GetPropACL: String; override;

    procedure SetPropACL(Value: String); override;

    function GetPropAllocationSize: Int64; override;

    function GetPropAttributeBits: Integer; override;

    function GetPropAttributeBitsValid: Integer; override;

    function GetPropCreationTime: Int64; override;

    procedure SetPropCreationTime(Value: Int64); override;

    function GetPropFileType: TiphSFTPFileTypes; override;

    function GetPropGroupId: String; override;

    procedure SetPropGroupId(Value: String); override;

    function GetPropIsDir: Boolean; override;

    function GetPropIsSymlink: Boolean; override;

    function GetPropModifiedTime: Int64; override;

    procedure SetPropModifiedTime(Value: Int64); override;

    function GetPropOwnerId: String; override;

    procedure SetPropOwnerId(Value: String); override;

    function GetPropPermissions: Integer; override;

    procedure SetPropPermissions(Value: Integer); override;

    function GetPropPermissionsOctal: String; override;

    procedure SetPropPermissionsOctal(Value: String); override;

    function GetPropSize: Int64; override;

  end;

function TiphFileAttributesSFTPFileAttributes.OwnerCtl : TiphSFTP;
begin
  Result := TiphSFTP(FOwnerCtl);
end;

function TiphFileAttributesSFTPFileAttributes.GetPropAccessTime: Int64;
begin
  Result := (OwnerCtl.get_FileAccessTime());
end;

procedure TiphFileAttributesSFTPFileAttributes.SetPropAccessTime(Value: Int64);
begin
  OwnerCtl.set_FileAccessTime((Value));
end;

function TiphFileAttributesSFTPFileAttributes.GetPropACL: String;
begin
  Result := (OwnerCtl.get_FileACL());
end;

procedure TiphFileAttributesSFTPFileAttributes.SetPropACL(Value: String);
begin
  OwnerCtl.set_FileACL((Value));
end;

function TiphFileAttributesSFTPFileAttributes.GetPropAllocationSize: Int64;
begin
  Result := (OwnerCtl.get_FileAllocationSize());
end;


function TiphFileAttributesSFTPFileAttributes.GetPropAttributeBits: Integer;
begin
  Result := (OwnerCtl.get_FileAttributeBits());
end;


function TiphFileAttributesSFTPFileAttributes.GetPropAttributeBitsValid: Integer;
begin
  Result := (OwnerCtl.get_FileAttributeBitsValid());
end;


function TiphFileAttributesSFTPFileAttributes.GetPropCreationTime: Int64;
begin
  Result := (OwnerCtl.get_FileCreationTime());
end;

procedure TiphFileAttributesSFTPFileAttributes.SetPropCreationTime(Value: Int64);
begin
  OwnerCtl.set_FileCreationTime((Value));
end;

function TiphFileAttributesSFTPFileAttributes.GetPropFileType: TiphSFTPFileTypes;
begin
  Result := TiphSFTPFileTypes(OwnerCtl.get_FileType());
end;


function TiphFileAttributesSFTPFileAttributes.GetPropGroupId: String;
begin
  Result := (OwnerCtl.get_FileGroupId());
end;

procedure TiphFileAttributesSFTPFileAttributes.SetPropGroupId(Value: String);
begin
  OwnerCtl.set_FileGroupId((Value));
end;

function TiphFileAttributesSFTPFileAttributes.GetPropIsDir: Boolean;
begin
  Result := (OwnerCtl.get_FileIsDir());
end;


function TiphFileAttributesSFTPFileAttributes.GetPropIsSymlink: Boolean;
begin
  Result := (OwnerCtl.get_FileIsSymlink());
end;


function TiphFileAttributesSFTPFileAttributes.GetPropModifiedTime: Int64;
begin
  Result := (OwnerCtl.get_FileModifiedTime());
end;

procedure TiphFileAttributesSFTPFileAttributes.SetPropModifiedTime(Value: Int64);
begin
  OwnerCtl.set_FileModifiedTime((Value));
end;

function TiphFileAttributesSFTPFileAttributes.GetPropOwnerId: String;
begin
  Result := (OwnerCtl.get_FileOwnerId());
end;

procedure TiphFileAttributesSFTPFileAttributes.SetPropOwnerId(Value: String);
begin
  OwnerCtl.set_FileOwnerId((Value));
end;

function TiphFileAttributesSFTPFileAttributes.GetPropPermissions: Integer;
begin
  Result := (OwnerCtl.get_FilePermissions());
end;

procedure TiphFileAttributesSFTPFileAttributes.SetPropPermissions(Value: Integer);
begin
  OwnerCtl.set_FilePermissions((Value));
end;

function TiphFileAttributesSFTPFileAttributes.GetPropPermissionsOctal: String;
begin
  Result := (OwnerCtl.get_FilePermissionsOctal());
end;

procedure TiphFileAttributesSFTPFileAttributes.SetPropPermissionsOctal(Value: String);
begin
  OwnerCtl.set_FilePermissionsOctal((Value));
end;

function TiphFileAttributesSFTPFileAttributes.GetPropSize: Int64;
begin
  Result := (OwnerCtl.get_FileSize());
end;



type
  TiphFirewallFirewall = class(TiphFirewall)
  protected
    function OwnerCtl : TiphSFTP;
    function GetPropAutoDetect: Boolean; override;

    procedure SetPropAutoDetect(Value: Boolean); override;

    function GetPropFirewallType: TiphFirewallTypes; override;

    procedure SetPropFirewallType(Value: TiphFirewallTypes); override;

    function GetPropHost: String; override;

    procedure SetPropHost(Value: String); override;

    function GetPropPassword: String; override;

    procedure SetPropPassword(Value: String); override;

    function GetPropPort: Integer; override;

    procedure SetPropPort(Value: Integer); override;

    function GetPropUser: String; override;

    procedure SetPropUser(Value: String); override;

  end;

function TiphFirewallFirewall.OwnerCtl : TiphSFTP;
begin
  Result := TiphSFTP(FOwnerCtl);
end;

function TiphFirewallFirewall.GetPropAutoDetect: Boolean;
begin
  Result := (OwnerCtl.get_FirewallAutoDetect());
end;

procedure TiphFirewallFirewall.SetPropAutoDetect(Value: Boolean);
begin
  OwnerCtl.set_FirewallAutoDetect((Value));
end;

function TiphFirewallFirewall.GetPropFirewallType: TiphFirewallTypes;
begin
  Result := TiphFirewallTypes(OwnerCtl.get_FirewallType());
end;

procedure TiphFirewallFirewall.SetPropFirewallType(Value: TiphFirewallTypes);
begin
  OwnerCtl.set_FirewallType(TiphsftpFirewallTypes(Value));
end;

function TiphFirewallFirewall.GetPropHost: String;
begin
  Result := (OwnerCtl.get_FirewallHost());
end;

procedure TiphFirewallFirewall.SetPropHost(Value: String);
begin
  OwnerCtl.set_FirewallHost((Value));
end;

function TiphFirewallFirewall.GetPropPassword: String;
begin
  Result := (OwnerCtl.get_FirewallPassword());
end;

procedure TiphFirewallFirewall.SetPropPassword(Value: String);
begin
  OwnerCtl.set_FirewallPassword((Value));
end;

function TiphFirewallFirewall.GetPropPort: Integer;
begin
  Result := (OwnerCtl.get_FirewallPort());
end;

procedure TiphFirewallFirewall.SetPropPort(Value: Integer);
begin
  OwnerCtl.set_FirewallPort((Value));
end;

function TiphFirewallFirewall.GetPropUser: String;
begin
  Result := (OwnerCtl.get_FirewallUser());
end;

procedure TiphFirewallFirewall.SetPropUser(Value: String);
begin
  OwnerCtl.set_FirewallUser((Value));
end;



{*********************************************************************************}

constructor TiphSFTP.Create(AOwner: TComponent);
begin
  Create(AOwner, IPWORKSSSH_OEMKEY_17);
  Config('CodePage=65001');
end;

constructor TiphSFTP.Create(AOwner: TComponent; RuntimeLicense: string);
begin
  inherited Create(AOwner);
  m_ctl := nil;

  if @_IPWorksSSH_SFTP_Create <> nil then
    m_ctl := _IPWorksSSH_SFTP_Create(@FireEvents, self, nil, CREATE_OPT);
  if m_ctl = nil then
    TreatErr(-1, 'IPWorks SSH SFTP: Error creating component');
  _IPWorksSSH_SFTP_Set(m_ctl, 8002{PID_KEYCHECK_DLP}, 0, 
    {$ifdef ANSICHAR_IS_BYTE}
    Pointer(ToLinuxAnsiString(RuntimeLicense)),
    {$else}
    Pointer(PAnsiChar(AnsiString(RuntimeLicense))),
    {$endif}
    0);

  FDirList := TiphDirListDirEntryList.Create(Self, true);

  FFileAttributes := TiphFileAttributesSFTPFileAttributes.Create(Self, false);

  FFirewall := TiphFirewallFirewall.Create(Self, false);


  try set_FirewallAutoDetect(false) except on E:Exception do end;

  try set_FirewallType(fwNone) except on E:Exception do end;

  try set_FirewallHost('') except on E:Exception do end;

  try set_FirewallPassword('') except on E:Exception do end;

  try set_FirewallPort(0) except on E:Exception do end;

  try set_FirewallUser('') except on E:Exception do end;

  try set_LocalFile('') except on E:Exception do end;

  try set_LocalPort(0) except on E:Exception do end;

  try set_Overwrite(false) except on E:Exception do end;

  try set_RemoteFile('') except on E:Exception do end;

  try set_RemotePath('') except on E:Exception do end;

  try set_SSHAuthMode(amPassword) except on E:Exception do end;

  try set_SSHCertStore('MY') except on E:Exception do end;

  try set_SSHCertStorePassword('') except on E:Exception do end;

  try set_SSHCertStoreType(cstUser) except on E:Exception do end;

  try set_SSHCertSubject('') except on E:Exception do end;

  try set_SSHCompressionAlgorithms('none,zlib') except on E:Exception do end;

  try set_SSHEncryptionAlgorithms('aes256-ctr,aes192-ctr,aes128-ctr,aes256-cbc,aes192-cbc,aes128-cbc,3des-ctr,3des-cbc,blowfish-cbc,arcfour256,arcfour128,arcfour,cast128-cbc,aes256-gcm@openssh.com,aes128-gcm@openssh.com,chacha20-poly1305@openssh.com') except on E:Exception do end;

  try set_SSHHost('') except on E:Exception do end;

  try set_SSHPassword('') except on E:Exception do end;

  try set_SSHPort(22) except on E:Exception do end;

  try set_SSHUser('') except on E:Exception do end;

  try set_StartByte(0) except on E:Exception do end;

  try set_Timeout(60) except on E:Exception do end;

end;

destructor TiphSFTP.Destroy;
begin
  FreeAndNil(FDirList);

  FreeAndNil(FFileAttributes);

  FreeAndNil(FFirewall);

  
  if m_streamFromSetDownloadStream <> nil then
    FreeAndNil(m_streamFromSetDownloadStream);

  if m_streamFromSetUploadStream <> nil then
    FreeAndNil(m_streamFromSetUploadStream);

  
  if m_ctl <> nil then begin
    if @_IPWorksSSH_SFTP_Destroy <> nil then
      _IPWorksSSH_SFTP_Destroy(m_ctl);
  end;
  m_ctl := nil;
  inherited Destroy;
end;

function TiphSFTP.ThrowCoreException(err: Integer; const desc: {$ifndef NO_ANSI}AnsiString{$else}string{$endif}): EIPWorksSSH;
begin
  result := EiphSFTP.CreateCode(err, desc);
end;

function TiphSFTP.ReportEventException(e: Exception; Event: String; ReFire: Boolean): Integer;
var
  msg: String;
begin
  result := -1;
  msg := 'An unhandled error occurred in the ' + Event + ' event handler: ' + E.Message;
  if (m_ctl <> nil) and (@_IPWorksSSH_SFTP_SetEventErrorAndCode <> nil) then begin
    if E is EIPWorksSSH then begin
      result := EIPWorksSSH(e).Code;
      msg := E.Message;
    end;
    _IPWorksSSH_SFTP_SetEventErrorAndCode(m_ctl, result, 
      {$ifdef ANSICHAR_IS_BYTE}
      Pointer(ToLinuxAnsiString(msg)));
      {$else}
      Pointer(PAnsiChar(AnsiString(msg))));
      {$endif}
  end;
  if ReFire and Assigned(FOnError) then FOnError(self, result, msg, '', '');
end;

{*********************************************************************************}

procedure TiphSFTP.AboutDlg;
var
  p : LPVOIDARR;
  pc: LPINTARR;
begin
{$IFDEF LINUX}
  inherited AboutDlg;
{$ELSE}
  p := nil;
  pc := nil;
  if @_IPWorksSSH_SFTP_Do <> nil then
    _IPWorksSSH_SFTP_Do(m_ctl, 255, 0, p, pc, nil);
{$ENDIF}
end;

{*********************************************************************************}

procedure TiphSFTP.SetRuntimeLicense(key: String);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  err := _IPWorksSSH_SFTP_Set(m_ctl, 8001{PID_RUNTIME_LICENSE}, 0, 
    {$ifdef ANSICHAR_IS_BYTE}
    Pointer(ToLinuxAnsiString(key)),
    {$else}
    Pointer(PAnsiChar(AnsiString(key))),
    {$endif}
    0);

  if err <> 0 then TreatErr(err, '');
end;

function TiphSFTP.GetRuntimeLicense: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';
  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, 8001{PID_RUNTIME_LICENSE}, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$ifdef ANSICHAR_IS_BYTE}
  result := FromLinuxAnsiString(PLXAnsiChar(tmp));
  {$else}
  result := PAnsiChar(tmp);
  {$endif}
end;

{*********************************************************************************}

function TiphSFTP.HasData: Boolean;
begin
  result := false;
end;

procedure TiphSFTP.ReadHnd(Reader: TStream);
begin
end;

procedure TiphSFTP.WriteHnd(Writer: TStream);
begin
end;

function TiphSFTP.BStr2CStr(pBStr: Pointer; lenBStr: Integer; bArr: TBytes): String;
{$IF DEFINED(UNICODE) AND DEFINED(MSWINDOWS)}
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
{$IFEND}
begin
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  param[0] := pBStr; paramcb[0] := lenBStr;
  param[1] := nil;   paramcb[1] := 0;
  param[2] := nil;   paramcb[2] := 0;
  _IPWorksSSH_SFTP_Do(m_ctl, 2003, 2, @param, @paramcb, nil);
  SetLength(Result, Integer(param[2]));
  if Length(Result) > 0 then begin
    param[1] := PChar(Result); paramcb[1] := Length(Result);
    _IPWorksSSH_SFTP_Do(m_ctl, 2003, 2, @param, @paramcb, nil);
  end;
  {$ELSE}
  try
    Result := TEncoding.Default.GetString(bArr);
  except
    on E: Exception do
      SetString(Result, PAnsiChar(pBStr), lenBStr);
   end;
  {$ENDIF}
{$ELSE}
  SetString(Result, PAnsiChar(pBStr), lenBStr);
{$ENDIF}
end;

procedure TiphSFTP.DefineProperties(Filer: TFiler);
begin
  inherited DefineProperties(Filer);
  Filer.DefineBinaryProperty('RegHnd', ReadHnd, WriteHnd, HasData);
end;

{*********************************************************************************}

procedure TiphSFTP.TreatErr(Err: Integer; const desc: String);
var
  msg : {$ifdef NO_ANSI}string{$else}LXAnsiString{$endif};
begin
  {$ifdef NO_ANSI}
  msg := desc;
  {$else}
  {$ifdef ANSICHAR_IS_BYTE}
  msg := ToLinuxAnsiString(desc);
  {$else}
  msg := LXAnsiString(desc);
  {$endif}
  {$endif}
  if Length(msg) = 0 then
  begin
    if @_IPWorksSSH_SFTP_GetLastError <> nil then
      {$ifdef NO_ANSI}
      {$ifdef ANSICHAR_IS_BYTE}
      msg := FromLinuxAnsiString(_IPWorksSSH_SFTP_GetLastError(m_ctl));
      {$else}
      msg := _IPWorksSSH_SFTP_GetLastError(m_ctl);
      {$endif}
      {$else}
      msg := _IPWorksSSH_SFTP_GetLastError(m_ctl);
      {$endif}
  end;

  raise ThrowCoreException(err, msg);
end;

{*********************************************************************************}

(*** Property Getters/Setters ***)
function TiphSFTP.get_Connected: Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_Connected, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


procedure TiphSFTP.set_Connected(valConnected: Boolean);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_Connected, 0, Pointer(valConnected), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_DirListCount: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_DirListCount, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


function TiphSFTP.get_DirListEntry(EntryIndex: Integer): String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';
  if @_IPWorksSSH_SFTP_CheckIndex = nil then exit;
  err := _IPWorksSSH_SFTP_CheckIndex(m_ctl, PID_SFTP_DirListEntry, EntryIndex);
  if err <> 0 then TreatErr(err, 'Invalid array index value for DirListEntry');

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_DirListEntry{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, EntryIndex, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphSFTP.get_DirListFileName(EntryIndex: Integer): String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';
  if @_IPWorksSSH_SFTP_CheckIndex = nil then exit;
  err := _IPWorksSSH_SFTP_CheckIndex(m_ctl, PID_SFTP_DirListFileName, EntryIndex);
  if err <> 0 then TreatErr(err, 'Invalid array index value for DirListFileName');

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_DirListFileName{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, EntryIndex, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphSFTP.get_DirListFileSize(EntryIndex: Integer): Int64;
var
  err: Integer;
begin
  result := Int64(0);
  if @_IPWorksSSH_SFTP_CheckIndex = nil then exit;
  err := _IPWorksSSH_SFTP_CheckIndex(m_ctl, PID_SFTP_DirListFileSize, EntryIndex);
  if err <> 0 then TreatErr(err, 'Invalid array index value for DirListFileSize');

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_DirListFileSize, EntryIndex, nil, @result);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  
end;


function TiphSFTP.get_DirListFileTime(EntryIndex: Integer): String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';
  if @_IPWorksSSH_SFTP_CheckIndex = nil then exit;
  err := _IPWorksSSH_SFTP_CheckIndex(m_ctl, PID_SFTP_DirListFileTime, EntryIndex);
  if err <> 0 then TreatErr(err, 'Invalid array index value for DirListFileTime');

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_DirListFileTime{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, EntryIndex, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphSFTP.get_DirListIsDir(EntryIndex: Integer): Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);
  if @_IPWorksSSH_SFTP_CheckIndex = nil then exit;
  err := _IPWorksSSH_SFTP_CheckIndex(m_ctl, PID_SFTP_DirListIsDir, EntryIndex);
  if err <> 0 then TreatErr(err, 'Invalid array index value for DirListIsDir');

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_DirListIsDir, EntryIndex, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


function TiphSFTP.get_DirListIsSymlink(EntryIndex: Integer): Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);
  if @_IPWorksSSH_SFTP_CheckIndex = nil then exit;
  err := _IPWorksSSH_SFTP_CheckIndex(m_ctl, PID_SFTP_DirListIsSymlink, EntryIndex);
  if err <> 0 then TreatErr(err, 'Invalid array index value for DirListIsSymlink');

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_DirListIsSymlink, EntryIndex, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


function TiphSFTP.get_FileAccessTime: Int64;
var
  err: Integer;
begin
  result := Int64(0);

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_FileAccessTime, 0, nil, @result);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSFTP.set_FileAccessTime(valFileAccessTime: Int64);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_FileAccessTime, 0, Pointer(@valFileAccessTime), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_FileACL: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_FileACL{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSFTP.set_FileACL(valFileACL: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valFileACL);
  {$ENDIF}
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_FileACL{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valFileACL){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_FileAllocationSize: Int64;
var
  err: Integer;
begin
  result := Int64(0);

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_FileAllocationSize, 0, nil, @result);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  
end;


function TiphSFTP.get_FileAttributeBits: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_FileAttributeBits, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


function TiphSFTP.get_FileAttributeBitsValid: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_FileAttributeBitsValid, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


function TiphSFTP.get_FileCreationTime: Int64;
var
  err: Integer;
begin
  result := Int64(0);

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_FileCreationTime, 0, nil, @result);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSFTP.set_FileCreationTime(valFileCreationTime: Int64);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_FileCreationTime, 0, Pointer(@valFileCreationTime), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_FileType: TiphsftpFileTypes;
var
  tmp: Pointer;
  err: Integer;
begin
  result := TiphsftpFileTypes(0);

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_FileType, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := TiphsftpFileTypes(tmp);
end;


function TiphSFTP.get_FileGroupId: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_FileGroupId{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSFTP.set_FileGroupId(valFileGroupId: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valFileGroupId);
  {$ENDIF}
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_FileGroupId{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valFileGroupId){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_FileIsDir: Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_FileIsDir, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


function TiphSFTP.get_FileIsSymlink: Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_FileIsSymlink, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


function TiphSFTP.get_FileModifiedTime: Int64;
var
  err: Integer;
begin
  result := Int64(0);

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_FileModifiedTime, 0, nil, @result);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSFTP.set_FileModifiedTime(valFileModifiedTime: Int64);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_FileModifiedTime, 0, Pointer(@valFileModifiedTime), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_FileOwnerId: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_FileOwnerId{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSFTP.set_FileOwnerId(valFileOwnerId: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valFileOwnerId);
  {$ENDIF}
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_FileOwnerId{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valFileOwnerId){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_FilePermissions: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_FilePermissions, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSFTP.set_FilePermissions(valFilePermissions: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_FilePermissions, 0, Pointer(valFilePermissions), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_FilePermissionsOctal: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_FilePermissionsOctal{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSFTP.set_FilePermissionsOctal(valFilePermissionsOctal: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valFilePermissionsOctal);
  {$ENDIF}
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_FilePermissionsOctal{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valFilePermissionsOctal){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_FileSize: Int64;
var
  err: Integer;
begin
  result := Int64(0);

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_FileSize, 0, nil, @result);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  
end;


function TiphSFTP.get_FileExists: Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_FileExists, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


function TiphSFTP.get_FirewallAutoDetect: Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_FirewallAutoDetect, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


procedure TiphSFTP.set_FirewallAutoDetect(valFirewallAutoDetect: Boolean);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_FirewallAutoDetect, 0, Pointer(valFirewallAutoDetect), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_FirewallType: TiphsftpFirewallTypes;
var
  tmp: Pointer;
  err: Integer;
begin
  result := TiphsftpFirewallTypes(0);

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_FirewallType, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := TiphsftpFirewallTypes(tmp);
end;


procedure TiphSFTP.set_FirewallType(valFirewallType: TiphsftpFirewallTypes);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_FirewallType, 0, Pointer(valFirewallType), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_FirewallHost: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_FirewallHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSFTP.set_FirewallHost(valFirewallHost: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valFirewallHost);
  {$ENDIF}
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_FirewallHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valFirewallHost){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_FirewallPassword: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_FirewallPassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSFTP.set_FirewallPassword(valFirewallPassword: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valFirewallPassword);
  {$ENDIF}
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_FirewallPassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valFirewallPassword){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_FirewallPort: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_FirewallPort, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSFTP.set_FirewallPort(valFirewallPort: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_FirewallPort, 0, Pointer(valFirewallPort), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_FirewallUser: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_FirewallUser{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSFTP.set_FirewallUser(valFirewallUser: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valFirewallUser);
  {$ENDIF}
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_FirewallUser{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valFirewallUser){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_Idle: Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_Idle, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


function TiphSFTP.get_LocalFile: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_LocalFile{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSFTP.set_LocalFile(valLocalFile: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valLocalFile);
  {$ENDIF}
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_LocalFile{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valLocalFile){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_LocalHost: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_LocalHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSFTP.set_LocalHost(valLocalHost: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valLocalHost);
  {$ENDIF}
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_LocalHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valLocalHost){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_LocalPort: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_LocalPort, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSFTP.set_LocalPort(valLocalPort: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_LocalPort, 0, Pointer(valLocalPort), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_Overwrite: Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_Overwrite, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


procedure TiphSFTP.set_Overwrite(valOverwrite: Boolean);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_Overwrite, 0, Pointer(valOverwrite), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_RemoteFile: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_RemoteFile{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSFTP.set_RemoteFile(valRemoteFile: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valRemoteFile);
  {$ENDIF}
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_RemoteFile{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valRemoteFile){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_RemotePath: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_RemotePath{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSFTP.set_RemotePath(valRemotePath: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valRemotePath);
  {$ENDIF}
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_RemotePath{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valRemotePath){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_SSHAcceptServerHostKeyEncoded: String;
var
  tmp: {$IFDEF NONWINUNICODE}TBytes{$ELSE}Pointer; len: Cardinal{$ENDIF};
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := {$IFDEF NONWINUNICODE}get_SSHAcceptServerHostKeyEncodedB{$ELSE}_IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_SSHAcceptServerHostKeyEncoded{$IFDEF UNICODE_ON_UNICODE}+20000{$ENDIF}, 0, @len, nil){$ENDIF};
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$IFDEF NONWINUNICODE}result := TEncoding.UTF8.GetString(tmp);{$ELSE}SetString(result, PChar(tmp), len);{$ENDIF}
end;


procedure TiphSFTP.set_SSHAcceptServerHostKeyEncoded(valSSHAcceptServerHostKeyEncoded: String);
var
  err: Integer;
  {$IFDEF NONWINUNICODE}tmp: TBytes;{$ENDIF}
begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_SSHAcceptServerHostKeyEncoded+20000, 0, Pointer(PWideChar(valSSHAcceptServerHostKeyEncoded)), Length(valSSHAcceptServerHostKeyEncoded));
  {$ELSE}
  tmp := TEncoding.UTF8.GetBytes(valSSHAcceptServerHostKeyEncoded);
  set_SSHAcceptServerHostKeyEncodedB(tmp);
  {$ENDIF}
{$ELSE}
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_SSHAcceptServerHostKeyEncoded, 0, Pointer(PAnsiChar(valSSHAcceptServerHostKeyEncoded)), Length(valSSHAcceptServerHostKeyEncoded));
{$ENDIF}
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_SSHAcceptServerHostKeyEncodedB: TBytes;
var
  tmp: Pointer;
  len: Cardinal;
begin
  result := nil;
  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_SSHAcceptServerHostKeyEncoded, 0, @len, nil);
  SetLength(result, len);
  Move(Pointer(tmp)^, Pointer(result)^, len); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSFTP.set_SSHAcceptServerHostKeyEncodedB(valSSHAcceptServerHostKeyEncoded: TBytes);
var
  err: Integer;
begin
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_SSHAcceptServerHostKeyEncoded, 0, Pointer(valSSHAcceptServerHostKeyEncoded), Length(valSSHAcceptServerHostKeyEncoded));
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_SSHAuthMode: TiphsftpSSHAuthModes;
var
  tmp: Pointer;
  err: Integer;
begin
  result := TiphsftpSSHAuthModes(0);

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_SSHAuthMode, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := TiphsftpSSHAuthModes(tmp);
end;


procedure TiphSFTP.set_SSHAuthMode(valSSHAuthMode: TiphsftpSSHAuthModes);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_SSHAuthMode, 0, Pointer(valSSHAuthMode), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_SSHCertEncoded: String;
var
  tmp: {$IFDEF NONWINUNICODE}TBytes{$ELSE}Pointer; len: Cardinal{$ENDIF};
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := {$IFDEF NONWINUNICODE}get_SSHCertEncodedB{$ELSE}_IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_SSHCertEncoded{$IFDEF UNICODE_ON_UNICODE}+20000{$ENDIF}, 0, @len, nil){$ENDIF};
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$IFDEF NONWINUNICODE}result := TEncoding.UTF8.GetString(tmp);{$ELSE}SetString(result, PChar(tmp), len);{$ENDIF}
end;


procedure TiphSFTP.set_SSHCertEncoded(valSSHCertEncoded: String);
var
  err: Integer;
  {$IFDEF NONWINUNICODE}tmp: TBytes;{$ENDIF}
begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_SSHCertEncoded+20000, 0, Pointer(PWideChar(valSSHCertEncoded)), Length(valSSHCertEncoded));
  {$ELSE}
  tmp := TEncoding.UTF8.GetBytes(valSSHCertEncoded);
  set_SSHCertEncodedB(tmp);
  {$ENDIF}
{$ELSE}
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_SSHCertEncoded, 0, Pointer(PAnsiChar(valSSHCertEncoded)), Length(valSSHCertEncoded));
{$ENDIF}
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_SSHCertEncodedB: TBytes;
var
  tmp: Pointer;
  len: Cardinal;
begin
  result := nil;
  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_SSHCertEncoded, 0, @len, nil);
  SetLength(result, len);
  Move(Pointer(tmp)^, Pointer(result)^, len); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSFTP.set_SSHCertEncodedB(valSSHCertEncoded: TBytes);
var
  err: Integer;
begin
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_SSHCertEncoded, 0, Pointer(valSSHCertEncoded), Length(valSSHCertEncoded));
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_SSHCertStore: String;
var
  tmp: {$IFDEF NONWINUNICODE}TBytes{$ELSE}Pointer; len: Cardinal{$ENDIF};
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := {$IFDEF NONWINUNICODE}get_SSHCertStoreB{$ELSE}_IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_SSHCertStore{$IFDEF UNICODE_ON_UNICODE}+20000{$ENDIF}, 0, @len, nil){$ENDIF};
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$IFDEF NONWINUNICODE}result := TEncoding.UTF8.GetString(tmp);{$ELSE}SetString(result, PChar(tmp), len);{$ENDIF}
end;


procedure TiphSFTP.set_SSHCertStore(valSSHCertStore: String);
var
  err: Integer;
  {$IFDEF NONWINUNICODE}tmp: TBytes;{$ENDIF}
begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_SSHCertStore+20000, 0, Pointer(PWideChar(valSSHCertStore)), Length(valSSHCertStore));
  {$ELSE}
  tmp := TEncoding.UTF8.GetBytes(valSSHCertStore);
  set_SSHCertStoreB(tmp);
  {$ENDIF}
{$ELSE}
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_SSHCertStore, 0, Pointer(PAnsiChar(valSSHCertStore)), Length(valSSHCertStore));
{$ENDIF}
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_SSHCertStoreB: TBytes;
var
  tmp: Pointer;
  len: Cardinal;
begin
  result := nil;
  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_SSHCertStore, 0, @len, nil);
  SetLength(result, len);
  Move(Pointer(tmp)^, Pointer(result)^, len); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSFTP.set_SSHCertStoreB(valSSHCertStore: TBytes);
var
  err: Integer;
begin
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_SSHCertStore, 0, Pointer(valSSHCertStore), Length(valSSHCertStore));
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_SSHCertStorePassword: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_SSHCertStorePassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSFTP.set_SSHCertStorePassword(valSSHCertStorePassword: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHCertStorePassword);
  {$ENDIF}
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_SSHCertStorePassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHCertStorePassword){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_SSHCertStoreType: TiphsftpSSHCertStoreTypes;
var
  tmp: Pointer;
  err: Integer;
begin
  result := TiphsftpSSHCertStoreTypes(0);

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_SSHCertStoreType, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := TiphsftpSSHCertStoreTypes(tmp);
end;


procedure TiphSFTP.set_SSHCertStoreType(valSSHCertStoreType: TiphsftpSSHCertStoreTypes);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_SSHCertStoreType, 0, Pointer(valSSHCertStoreType), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_SSHCertSubject: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_SSHCertSubject{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSFTP.set_SSHCertSubject(valSSHCertSubject: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHCertSubject);
  {$ENDIF}
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_SSHCertSubject{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHCertSubject){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_SSHCompressionAlgorithms: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_SSHCompressionAlgorithms{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSFTP.set_SSHCompressionAlgorithms(valSSHCompressionAlgorithms: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHCompressionAlgorithms);
  {$ENDIF}
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_SSHCompressionAlgorithms{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHCompressionAlgorithms){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_SSHEncryptionAlgorithms: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_SSHEncryptionAlgorithms{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSFTP.set_SSHEncryptionAlgorithms(valSSHEncryptionAlgorithms: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHEncryptionAlgorithms);
  {$ENDIF}
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_SSHEncryptionAlgorithms{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHEncryptionAlgorithms){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_SSHHost: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_SSHHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSFTP.set_SSHHost(valSSHHost: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHHost);
  {$ENDIF}
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_SSHHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHHost){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_SSHPassword: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_SSHPassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSFTP.set_SSHPassword(valSSHPassword: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHPassword);
  {$ENDIF}
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_SSHPassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHPassword){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_SSHPort: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_SSHPort, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSFTP.set_SSHPort(valSSHPort: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_SSHPort, 0, Pointer(valSSHPort), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_SSHUser: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_SSHUser{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSFTP.set_SSHUser(valSSHUser: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHUser);
  {$ENDIF}
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_SSHUser{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHUser){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_StartByte: Int64;
var
  err: Integer;
begin
  result := Int64(0);

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_StartByte, 0, nil, @result);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSFTP.set_StartByte(valStartByte: Int64);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_StartByte, 0, Pointer(@valStartByte), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSFTP.get_Timeout: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SFTP_Get = nil then exit;
  tmp := _IPWorksSSH_SFTP_Get(m_ctl, PID_SFTP_Timeout, 0, nil, nil);
  err := _IPWorksSSH_SFTP_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSFTP.set_Timeout(valTimeout: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SFTP_Set = nil then exit;
  err := _IPWorksSSH_SFTP_Set(m_ctl, PID_SFTP_Timeout, 0, Pointer(valTimeout), 0);
  if err <> 0 then TreatErr(err, '');
end;



{*********************************************************************************}

(*** Property Getters/Setters: OO API ***)
function  TiphSFTP.get_DirList: TiphDirEntryList;
begin
  Result := FDirList;
end;

function  TiphSFTP.get_FileAttributes: TiphSFTPFileAttributes;
begin
  Result := FFileAttributes;
end;

procedure TiphSFTP.set_FileAttributes(Value : TiphSFTPFileAttributes);
begin
  FFileAttributes.Assign(Value);
end;

function  TiphSFTP.get_Firewall: TiphFirewall;
begin
  Result := FFirewall;
end;

procedure TiphSFTP.set_Firewall(Value : TiphFirewall);
begin
  FFirewall.Assign(Value);
end;


{*********************************************************************************}

{$IFNDEF DELPHI3}
(*** Methods ***)
procedure TiphSFTP.Append();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SFTP_Do = nil then exit;
  err := _IPWorksSSH_SFTP_Do(m_ctl, MID_SFTP_Append{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSFTP.ChangeRemotePath(RemotePath: String);
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
  tmp_RemotePath: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  tmp_RemotePath := {$ifndef UNICODE_ON_ANSI}RemotePath{$else}ToLinuxAnsiString(RemotePath){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_RemotePath){$else}@tmp_RemotePath[0]{$endif};

  if @_IPWorksSSH_SFTP_Do = nil then exit;
  err := _IPWorksSSH_SFTP_Do(m_ctl, MID_SFTP_ChangeRemotePath{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


function TiphSFTP.CheckFileExists(): Boolean;
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := Boolean(0);

  if @_IPWorksSSH_SFTP_Do = nil then exit;
  err := _IPWorksSSH_SFTP_Do(m_ctl, MID_SFTP_CheckFileExists{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  result := (Integer(param[0]) <> 0);
end;


function TiphSFTP.Config(ConfigurationString: String): String;
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
  tmp_ConfigurationString: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := '';

  tmp_ConfigurationString := {$ifndef UNICODE_ON_ANSI}ConfigurationString{$else}ToLinuxAnsiString(ConfigurationString){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_ConfigurationString){$else}@tmp_ConfigurationString[0]{$endif};

  if @_IPWorksSSH_SFTP_Do = nil then exit;
  err := _IPWorksSSH_SFTP_Do(m_ctl, MID_SFTP_Config{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  result := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(param[1]);
end;


procedure TiphSFTP.Connect();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SFTP_Do = nil then exit;
  err := _IPWorksSSH_SFTP_Do(m_ctl, MID_SFTP_Connect{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSFTP.CreateFile(FileName: String);
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
  tmp_FileName: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  tmp_FileName := {$ifndef UNICODE_ON_ANSI}FileName{$else}ToLinuxAnsiString(FileName){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_FileName){$else}@tmp_FileName[0]{$endif};

  if @_IPWorksSSH_SFTP_Do = nil then exit;
  err := _IPWorksSSH_SFTP_Do(m_ctl, MID_SFTP_CreateFile{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


function TiphSFTP.DecodePacket(EncodedPacket: String): TBytes;
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
  tmp_EncodedPacket: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := nil;

  tmp_EncodedPacket := {$ifndef UNICODE_ON_ANSI}EncodedPacket{$else}ToLinuxAnsiString(EncodedPacket){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_EncodedPacket){$else}@tmp_EncodedPacket[0]{$endif};

  if @_IPWorksSSH_SFTP_Do = nil then exit;
  err := _IPWorksSSH_SFTP_Do(m_ctl, MID_SFTP_DecodePacket{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  SetLength(result, paramcb[1]);
  Move(Pointer(param[1])^, Pointer(result)^, paramcb[1]); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSFTP.DeleteFile(FileName: String);
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
  tmp_FileName: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  tmp_FileName := {$ifndef UNICODE_ON_ANSI}FileName{$else}ToLinuxAnsiString(FileName){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_FileName){$else}@tmp_FileName[0]{$endif};

  if @_IPWorksSSH_SFTP_Do = nil then exit;
  err := _IPWorksSSH_SFTP_Do(m_ctl, MID_SFTP_DeleteFile{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSFTP.Disconnect();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SFTP_Do = nil then exit;
  err := _IPWorksSSH_SFTP_Do(m_ctl, MID_SFTP_Disconnect{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSFTP.DoEvents();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SFTP_Do = nil then exit;
  err := _IPWorksSSH_SFTP_Do(m_ctl, MID_SFTP_DoEvents{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSFTP.Download();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SFTP_Do = nil then exit;
  err := _IPWorksSSH_SFTP_Do(m_ctl, MID_SFTP_Download{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


function TiphSFTP.EncodePacket(Packet: TBytes): String;
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := '';

  param[0] := Pointer(Packet);
  paramcb[0] := Length(Packet);

  if @_IPWorksSSH_SFTP_Do = nil then exit;
  err := _IPWorksSSH_SFTP_Do(m_ctl, MID_SFTP_EncodePacket{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  result := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(param[1]);
end;


function TiphSFTP.GetSSHParam(Payload: TBytes; Field: String): String;
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
  i, err: Integer;
  tmp_Field: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 2 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := '';

  param[0] := Pointer(Payload);
  paramcb[0] := Length(Payload);

  tmp_Field := {$ifndef UNICODE_ON_ANSI}Field{$else}ToLinuxAnsiString(Field){$endif};
  param[1] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_Field){$else}@tmp_Field[0]{$endif};

  if @_IPWorksSSH_SFTP_Do = nil then exit;
  err := _IPWorksSSH_SFTP_Do(m_ctl, MID_SFTP_GetSSHParam{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 2, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  result := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(param[2]);
end;


function TiphSFTP.GetSSHParamBytes(Payload: TBytes; Field: String): TBytes;
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
  i, err: Integer;
  tmp_Field: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 2 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := nil;

  param[0] := Pointer(Payload);
  paramcb[0] := Length(Payload);

  tmp_Field := {$ifndef UNICODE_ON_ANSI}Field{$else}ToLinuxAnsiString(Field){$endif};
  param[1] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_Field){$else}@tmp_Field[0]{$endif};

  if @_IPWorksSSH_SFTP_Do = nil then exit;
  err := _IPWorksSSH_SFTP_Do(m_ctl, MID_SFTP_GetSSHParamBytes{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 2, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  SetLength(result, paramcb[2]);
  Move(Pointer(param[2])^, Pointer(result)^, paramcb[2]); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSFTP.Interrupt();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SFTP_Do = nil then exit;
  err := _IPWorksSSH_SFTP_Do(m_ctl, MID_SFTP_Interrupt{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSFTP.ListDirectory();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SFTP_Do = nil then exit;
  err := _IPWorksSSH_SFTP_Do(m_ctl, MID_SFTP_ListDirectory{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSFTP.MakeDirectory(NewDir: String);
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
  tmp_NewDir: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  tmp_NewDir := {$ifndef UNICODE_ON_ANSI}NewDir{$else}ToLinuxAnsiString(NewDir){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_NewDir){$else}@tmp_NewDir[0]{$endif};

  if @_IPWorksSSH_SFTP_Do = nil then exit;
  err := _IPWorksSSH_SFTP_Do(m_ctl, MID_SFTP_MakeDirectory{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSFTP.QueryFileAttributes();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SFTP_Do = nil then exit;
  err := _IPWorksSSH_SFTP_Do(m_ctl, MID_SFTP_QueryFileAttributes{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSFTP.QueueFile(LocalFile: String; RemoteFile: String);
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
  i, err: Integer;
  tmp_LocalFile: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
  tmp_RemoteFile: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 2 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  tmp_LocalFile := {$ifndef UNICODE_ON_ANSI}LocalFile{$else}ToLinuxAnsiString(LocalFile){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_LocalFile){$else}@tmp_LocalFile[0]{$endif};

  tmp_RemoteFile := {$ifndef UNICODE_ON_ANSI}RemoteFile{$else}ToLinuxAnsiString(RemoteFile){$endif};
  param[1] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_RemoteFile){$else}@tmp_RemoteFile[0]{$endif};

  if @_IPWorksSSH_SFTP_Do = nil then exit;
  err := _IPWorksSSH_SFTP_Do(m_ctl, MID_SFTP_QueueFile{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 2, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSFTP.RemoveDirectory(DirName: String);
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
  tmp_DirName: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  tmp_DirName := {$ifndef UNICODE_ON_ANSI}DirName{$else}ToLinuxAnsiString(DirName){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_DirName){$else}@tmp_DirName[0]{$endif};

  if @_IPWorksSSH_SFTP_Do = nil then exit;
  err := _IPWorksSSH_SFTP_Do(m_ctl, MID_SFTP_RemoveDirectory{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSFTP.RenameFile(NewName: String);
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
  tmp_NewName: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  tmp_NewName := {$ifndef UNICODE_ON_ANSI}NewName{$else}ToLinuxAnsiString(NewName){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_NewName){$else}@tmp_NewName[0]{$endif};

  if @_IPWorksSSH_SFTP_Do = nil then exit;
  err := _IPWorksSSH_SFTP_Do(m_ctl, MID_SFTP_RenameFile{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSFTP.Reset();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SFTP_Do = nil then exit;
  err := _IPWorksSSH_SFTP_Do(m_ctl, MID_SFTP_Reset{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSFTP.ResetQueue();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SFTP_Do = nil then exit;
  err := _IPWorksSSH_SFTP_Do(m_ctl, MID_SFTP_ResetQueue{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSFTP.SetDownloadStream(DownloadStream: TStream);
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
  streamIndex: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  if m_streamFromSetDownloadStream = nil then 
    m_streamFromSetDownloadStream := TList.Create;
  streamIndex := 0;
  
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if DownloadStream <> nil then param[0] := Pointer(1);
  if m_streamFromSetDownloadStream.Count <= streamIndex then m_streamFromSetDownloadStream.Count := streamIndex + 1;
  m_streamFromSetDownloadStream[streamIndex] := DownloadStream;
  Inc(streamIndex);

  if @_IPWorksSSH_SFTP_Do = nil then exit;
  err := _IPWorksSSH_SFTP_Do(m_ctl, MID_SFTP_SetDownloadStream{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


function TiphSFTP.SetSSHParam(Payload: TBytes; FieldType: String; FieldValue: String): TBytes;
var
  param: array[0..3] of Pointer;
  paramcb: array[0..3] of Integer;
  i, err: Integer;
  tmp_FieldType: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
  tmp_FieldValue: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 3 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := nil;

  param[0] := Pointer(Payload);
  paramcb[0] := Length(Payload);

  tmp_FieldType := {$ifndef UNICODE_ON_ANSI}FieldType{$else}ToLinuxAnsiString(FieldType){$endif};
  param[1] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_FieldType){$else}@tmp_FieldType[0]{$endif};

  tmp_FieldValue := {$ifndef UNICODE_ON_ANSI}FieldValue{$else}ToLinuxAnsiString(FieldValue){$endif};
  param[2] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_FieldValue){$else}@tmp_FieldValue[0]{$endif};

  if @_IPWorksSSH_SFTP_Do = nil then exit;
  err := _IPWorksSSH_SFTP_Do(m_ctl, MID_SFTP_SetSSHParam{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 3, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  SetLength(result, paramcb[3]);
  Move(Pointer(param[3])^, Pointer(result)^, paramcb[3]); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSFTP.SetUploadStream(UploadStream: TStream);
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
  streamIndex: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  if m_streamFromSetUploadStream = nil then 
    m_streamFromSetUploadStream := TList.Create;
  streamIndex := 0;
  
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if UploadStream <> nil then param[0] := Pointer(1);
  if m_streamFromSetUploadStream.Count <= streamIndex then m_streamFromSetUploadStream.Count := streamIndex + 1;
  m_streamFromSetUploadStream[streamIndex] := UploadStream;
  Inc(streamIndex);

  if @_IPWorksSSH_SFTP_Do = nil then exit;
  err := _IPWorksSSH_SFTP_Do(m_ctl, MID_SFTP_SetUploadStream{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSFTP.SSHLogoff();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SFTP_Do = nil then exit;
  err := _IPWorksSSH_SFTP_Do(m_ctl, MID_SFTP_SSHLogoff{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSFTP.SSHLogon(SSHHost: String; SSHPort: Integer);
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
  i, err: Integer;
  tmp_SSHHost: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 2 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  tmp_SSHHost := {$ifndef UNICODE_ON_ANSI}SSHHost{$else}ToLinuxAnsiString(SSHHost){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_SSHHost){$else}@tmp_SSHHost[0]{$endif};

  param[1] := Pointer(SSHPort);


  if @_IPWorksSSH_SFTP_Do = nil then exit;
  err := _IPWorksSSH_SFTP_Do(m_ctl, MID_SFTP_SSHLogon{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 2, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSFTP.UpdateFileAttributes();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SFTP_Do = nil then exit;
  err := _IPWorksSSH_SFTP_Do(m_ctl, MID_SFTP_UpdateFileAttributes{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSFTP.Upload();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SFTP_Do = nil then exit;
  err := _IPWorksSSH_SFTP_Do(m_ctl, MID_SFTP_Upload{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


{$ENDIF}


{$IFDEF USE_DYNAMIC_LOADING}

procedure ClearProcs();
begin
  _IPWorksSSH_EvtStr := nil;
  _IPWorksSSH_Stream := nil;
  _IPWorksSSH_SFTP_Create := nil;
  _IPWorksSSH_SFTP_Destroy := nil;
  _IPWorksSSH_SFTP_Set := nil;
  _IPWorksSSH_SFTP_Get := nil;
  _IPWorksSSH_SFTP_GetLastError := nil;
  _IPWorksSSH_SFTP_GetLastErrorCode := nil;
  _IPWorksSSH_SFTP_SetLastErrorAndCode := nil;
  _IPWorksSSH_SFTP_GetEventError := nil;
  _IPWorksSSH_SFTP_GetEventErrorCode := nil;
  _IPWorksSSH_SFTP_SetEventErrorAndCode := nil;
  {$IFDEF USE_EXPLICIT_INIT}
  _IPWorksSSH_SFTP_StaticInit := nil;
  _IPWorksSSH_SFTP_StaticDestroy := nil;
  {$ENDIF}
  _IPWorksSSH_SFTP_CheckIndex := nil;
  _IPWorksSSH_SFTP_Do := nil;
end;

{$IFDEF USEIPWORKSSSHDLL}
procedure LoadDLL();
begin
  ClearProcs();

  hLib := LoadLibrary(PChar(DLLNAME));
  if hLib = 0 then exit;

  @_IPWorksSSH_EvtStr                    := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_EvtStr');
  @_IPWorksSSH_Stream                    := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_Stream');
  @_IPWorksSSH_SFTP_Create               := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SFTP_Create');
  @_IPWorksSSH_SFTP_Destroy              := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SFTP_Destroy');
  @_IPWorksSSH_SFTP_Set                  := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SFTP_Set');
  @_IPWorksSSH_SFTP_Get                  := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SFTP_Get');
  @_IPWorksSSH_SFTP_GetLastError         := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SFTP_GetLastError');
  @_IPWorksSSH_SFTP_GetLastErrorCode     := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SFTP_GetLastErrorCode');
  @_IPWorksSSH_SFTP_SetLastErrorAndCode  := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SFTP_SetLastErrorAndCode');
  @_IPWorksSSH_SFTP_GetEventError        := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SFTP_GetEventError');
  @_IPWorksSSH_SFTP_GetEventErrorCode    := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SFTP_GetEventErrorCode');
  @_IPWorksSSH_SFTP_SetEventErrorAndCode := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SFTP_SetEventErrorAndCode');
  {$IFDEF USE_EXPLICIT_INIT}
  @_IPWorksSSH_SFTP_StaticInit           := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SFTP_StaticInit');
  @_IPWorksSSH_SFTP_StaticDestroy        := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SFTP_StaticDestroy');
  {$ENDIF}
  @_IPWorksSSH_SFTP_CheckIndex           := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SFTP_CheckIndex');
  @_IPWorksSSH_SFTP_Do                   := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SFTP_Do');
end;
{$ELSE}
procedure LoadRESDLL();
var
  hResInfo: HRSRC;
  hResData: HGLOBAL;
  pResData: Pointer;
begin
  ClearProcs();

  hResInfo := FindResource(HInstance, 'ipworksssh22_dta', RT_RCDATA);
  if hResInfo = 0 then exit;

  hResData := LoadResource(HInstance, hResInfo);
  if hResData = 0 then exit;

  pResData := LockResource(hResData);
  if pResData = nil then exit;

  pBaseAddress := IPWorksSSHLoadDRU(pResData, pEntryPoint);
  if Assigned(pBaseAddress) then begin
    @_IPWorksSSH_EvtStr                    := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_EvtStr');
    @_IPWorksSSH_Stream                    := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_Stream');
    @_IPWorksSSH_SFTP_Create               := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SFTP_Create');
    @_IPWorksSSH_SFTP_Destroy              := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SFTP_Destroy');
    @_IPWorksSSH_SFTP_Set                  := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SFTP_Set');
    @_IPWorksSSH_SFTP_Get                  := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SFTP_Get');
    @_IPWorksSSH_SFTP_GetLastError         := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SFTP_GetLastError');
    @_IPWorksSSH_SFTP_GetLastErrorCode     := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SFTP_GetLastErrorCode');
    @_IPWorksSSH_SFTP_SetLastErrorAndCode  := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SFTP_SetLastErrorAndCode');
    @_IPWorksSSH_SFTP_GetEventError        := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SFTP_GetEventError');
    @_IPWorksSSH_SFTP_GetEventErrorCode    := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SFTP_GetEventErrorCode');
    @_IPWorksSSH_SFTP_SetEventErrorAndCode := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SFTP_SetEventErrorAndCode');
    @_IPWorksSSH_SFTP_CheckIndex           := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SFTP_CheckIndex');
    @_IPWorksSSH_SFTP_Do                   := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SFTP_Do');
  end;

  FreeResource(hResData);
end;
{$ENDIF USEIPWORKSSSHDLL}

initialization
begin
  iLoadCount := iLoadCount + 1;
  if iLoadCount > 1 then exit;

  {$IFDEF USEIPWORKSSSHDLL}
  hLib := 0;
  LoadDLL();
  {$ELSE}
  pBaseAddress := nil;
  pEntryPoint := nil;
  LoadResDLL();
  {$ENDIF}

  {$IFDEF USE_EXPLICIT_INIT}
  if @_IPWorksSSH_SFTP_StaticInit <> nil then
    _IPWorksSSH_SFTP_StaticInit(nil);
  {$ENDIF}
end;

finalization
begin
  iLoadCount := iLoadCount - 1;
  if iLoadCount > 0 then exit;

  {$IFDEF USE_EXPLICIT_INIT}
  if @_IPWorksSSH_SFTP_StaticDestroy <> nil then
    _IPWorksSSH_SFTP_StaticDestroy();
  {$ENDIF}

  ClearProcs();

  {$IFDEF USEIPWORKSSSHDLL}
  if hLib <> 0 then
  begin
    {$ifndef FPC}FreeLibrary{$else}UnloadLibrary{$endif}(hLib);
    hLib := 0;
  end;
  {$ELSE}
  IPWorksSSHFreeDRU(pBaseAddress, pEntryPoint);
  pBaseAddress := nil;
  pEntryPoint := nil;
  {$ENDIF}
  iLoadCount := 0;
end;

{$ELSE USE_DYNAMIC_LOADING}

// in case if static library used

{$IFDEF USE_EXPLICIT_INIT}

initialization
  _IPWorksSSH_SFTP_StaticInit(nil);

finalization
  _IPWorksSSH_SFTP_StaticDestroy();

{$ENDIF USE_EXPLICIT_INIT}

{$ENDIF USE_DYNAMIC_LOADING}


end.
