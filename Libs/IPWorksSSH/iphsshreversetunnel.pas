
unit iphsshreversetunnel;



{$UNDEF USE_DYNAMIC_LOADING}
{$UNDEF USE_EXPLICIT_INIT}
{$UNDEF ANSICHAR_IS_BYTE}
{$UNDEF NO_ANSI}
{$UNDEF USE_CDECL}

{$IFDEF ENFORCE_DYNAMIC_LOADING}
  {$DEFINE USE_DYNAMIC_LOADING}
  {$DEFINE USEIPWORKSSSHDLL}
{$ENDIF}

{$IFDEF FPC}  //Lazarus
  {$IFDEF FPC_UNICODESTRINGS}
    // ensure that mode Delphi Unicode is enabled, not a modeswitch unicodestrings
    {$MODE DELPHIUNICODE}
  {$ELSE}
    {$MODE DELPHI}
  {$ENDIF}
  {$DEFINE USEIPWORKSSSHDLL}
  {$IFDEF DARWIN}
    {$DEFINE MACOS}
  {$ENDIF}
{$ENDIF}

{$IFNDEF FPC}
  {$IF CompilerVersion >= 24} // XE3+
    {$LEGACYIFEND ON}
  {$IFEND}
{$ENDIF}

{$IF DEFINED(LINUX) OR DEFINED(ANDROID) OR DEFINED(MACOS) OR DEFINED(IOS)}
  {$DEFINE USEIPWORKSSSHDLL}
  {$DEFINE ANSICHAR_IS_BYTE}
  {$DEFINE NO_ANSI}
  {$DEFINE USE_CDECL}
{$IFEND}

{$IF DEFINED(LINUX) OR DEFINED(MACOS)}
  {$IFNDEF IOS}
    {$DEFINE USE_DYNAMIC_LOADING}
  {$ENDIF}
{$IFEND}

{$IF DEFINED(ANDROID)}
  {$DEFINE USE_EXPLICIT_INIT}
{$IFEND}

{$IFDEF USE_IPWORKSSSH_ANDROID_SHARED_LIBRARY}
  {$DEFINE USE_DYNAMIC_LOADING}
{$ENDIF}


{$IF DEFINED(UNICODE) AND NOT DEFINED(MSWINDOWS)}
  {$DEFINE NONWINUNICODE}
{$IFEND}



{$IFNDEF USEIPWORKSSSHDLL}
  // in case if loading DLL from resources
  {$DEFINE USE_DYNAMIC_LOADING}
{$ENDIF}

{$UNDEF UNICODELIB}
{$IF DEFINED(MSWINDOWS)}
  {$DEFINE UNICODELIB}
{$IFEND}


{$IFDEF UNICODE}
  {$IFNDEF UNICODELIB}
    {$DEFINE UNICODE_ON_ANSI}
  {$ELSE}
    {$DEFINE UNICODE_ON_UNICODE}
  {$ENDIF}
{$ELSE}
  {$IFDEF UNICODELIB}
    {$DEFINE ANSI_ON_UNICODE}
  {$ELSE}
    {$DEFINE ANSI_ON_ANSI}
  {$ENDIF}
{$ENDIF}

interface

uses
{$IFDEF FPC}  //Lazarus
  SysUtils, Classes,
{$ELSE}
  {$IF CompilerVersion >= 23}System.SysUtils{$ELSE}SysUtils{$IFEND}, {$IF CompilerVersion >= 23}System.Classes{$ELSE}Classes{$IFEND}, 
{$ENDIF}
  {$IFDEF FPC}
  dynlibs,
  {$ELSE}
  {$IFDEF POSIX}
  Posix.Base,
  {$ENDIF}
  {$ENDIF}
  iphtypes, iphcore, iphkeys;

type

  (*** Enum Types ***)
  TiphsshreversetunnelFirewallTypes = iphtypes.TiphFirewallTypes;

  TiphsshreversetunnelSSHAuthModes = iphtypes.TiphSSHAuthModes;

  TiphsshreversetunnelSSHCertStoreTypes = iphtypes.TiphCertStoreTypes;

  (*** Event Delegate Types ***)
  TConnectedEvent = procedure (
    Sender: TObject;
    StatusCode: Integer;
    const Description: String
  ) of Object;


  TConnectionStatusEvent = procedure (
    Sender: TObject;
    const ConnectionEvent: String;
    StatusCode: Integer;
    const Description: String
  ) of Object;


  TDisconnectedEvent = procedure (
    Sender: TObject;
    StatusCode: Integer;
    const Description: String
  ) of Object;


  TErrorEvent = procedure (
    Sender: TObject;
    ErrorCode: Integer;
    const Description: String
  ) of Object;


  TLogEvent = procedure (
    Sender: TObject;
    LogLevel: Integer;
    const Message: String;
    const LogType: String
  ) of Object;


  TReconnectAttemptEvent = procedure (
    Sender: TObject;
    const Host: String;
    Port: Integer;
    RetryCount: Integer;
    RetriesRemaining: Integer;
    MaxRetryCount: Integer;
    RetryInterval: Integer;
    RetryType: Integer;
    RemoteListeningPort: Integer;
    var Cancel: Boolean
  ) of Object;


  TSSHChannelClosedEvent = procedure (
    Sender: TObject;
    const ChannelId: String
  ) of Object;


  TSSHChannelDataEvent = procedure (
    Sender: TObject;
    const ChannelId: String;
    ChannelData: String;
    ChannelDataB: TBytes
  ) of Object;


  TSSHChannelEOFEvent = procedure (
    Sender: TObject;
    const ChannelId: String
  ) of Object;


  TSSHChannelOpenedEvent = procedure (
    Sender: TObject;
    const ChannelId: String
  ) of Object;


  TSSHChannelOpenRequestEvent = procedure (
    Sender: TObject;
    const ChannelId: String;
    const Service: String;
    const ConnectedAddress: String;
    ConnectedPort: Integer;
    const OriginAddress: String;
    OriginPort: Integer;
    var Accept: Boolean
  ) of Object;


  TSSHChannelReadyToSendEvent = procedure (
    Sender: TObject;
    const ChannelId: String
  ) of Object;


  TSSHChannelRequestEvent = procedure (
    Sender: TObject;
    const ChannelId: String;
    const RequestType: String;
    Packet: String;
    PacketB: TBytes;
    var Success: Boolean
  ) of Object;


  TSSHChannelRequestedEvent = procedure (
    Sender: TObject;
    const ChannelId: String;
    const RequestType: String;
    Packet: String;
    PacketB: TBytes
  ) of Object;


  TSSHCustomAuthEvent = procedure (
    Sender: TObject;
    var Packet: String
  ) of Object;


  TSSHKeyboardInteractiveEvent = procedure (
    Sender: TObject;
    const Name: String;
    const Instructions: String;
    const Prompt: String;
    var Response: String;
    EchoResponse: Boolean
  ) of Object;


  TSSHServerAuthenticationEvent = procedure (
    Sender: TObject;
    HostKey: String;
    HostKeyB: TBytes;
    const Fingerprint: String;
    const KeyAlgorithm: String;
    const CertSubject: String;
    const CertIssuer: String;
    const Status: String;
    var Accept: Boolean
  ) of Object;


  TSSHStatusEvent = procedure (
    Sender: TObject;
    const Message: String
  ) of Object;


  (*** Exception Type ***)
  EiphSSHReverseTunnel = class(EIPWorksSSH)
  end;

  (*** Component Type ***)
{$IFNDEF FPC}
  {$IF CompilerVersion >= 23}
  [ComponentPlatformsAttribute(
    pidWin32 or pidWin64
    {$IF CompilerVersion >= 29}
    or pidLinux64
    {$IFEND}
    {$IF CompilerVersion >= 31}
    or pidOSX64
    {$IFEND}
   )]
  {$IFEND}
{$ENDIF}
  TiphSSHReverseTunnel = class(TiphTypesCore)
    private
      (*** Event Handler Procedure Pointers ***)
      FOnConnected: TConnectedEvent;

      FOnConnectionStatus: TConnectionStatusEvent;

      FOnDisconnected: TDisconnectedEvent;

      FOnError: TErrorEvent;

      FOnLog: TLogEvent;

      FOnReconnectAttempt: TReconnectAttemptEvent;

      FOnSSHChannelClosed: TSSHChannelClosedEvent;

      FOnSSHChannelData: TSSHChannelDataEvent;

      FOnSSHChannelEOF: TSSHChannelEOFEvent;

      FOnSSHChannelOpened: TSSHChannelOpenedEvent;

      FOnSSHChannelOpenRequest: TSSHChannelOpenRequestEvent;

      FOnSSHChannelReadyToSend: TSSHChannelReadyToSendEvent;

      FOnSSHChannelRequest: TSSHChannelRequestEvent;

      FOnSSHChannelRequested: TSSHChannelRequestedEvent;

      FOnSSHCustomAuth: TSSHCustomAuthEvent;

      FOnSSHKeyboardInteractive: TSSHKeyboardInteractiveEvent;

      FOnSSHServerAuthentication: TSSHServerAuthenticationEvent;

      FOnSSHStatus: TSSHStatusEvent;

      m_ctl: Pointer;
      (*** Inner objects for types and collections ***)
      FChannels: TiphSSHRTChannelList;

      FFirewall: TiphFirewall;

      function HasData: Boolean;
      procedure ReadHnd(Reader: TStream);
      procedure WriteHnd(Writer: TStream);
      function BStr2CStr(pBStr: Pointer; lenBStr: Integer; bArr: TBytes): String;

      procedure SetRuntimeLicense(key: String);
      function GetRuntimeLicense: String;

    protected
      procedure AboutDlg; override;
      procedure DefineProperties(Filer: TFiler); override;

      function ThrowCoreException(Err: Integer; const Desc: {$ifndef NO_ANSI}AnsiString{$else}string{$endif}): EIPWorksSSH;
      procedure TreatErr(Err: Integer; const Desc: String);

      (*** Property Getters/Setters ***)
      function  get_SSHChannelCount: Integer;

      function  get_BytesSent(SSHChannelId: Integer): Integer;

      function  get_ChannelId(SSHChannelId: Integer): String;

      function  get_ConnectedAddress(SSHChannelId: Integer): String;

      function  get_ConnectedPort(SSHChannelId: Integer): Integer;

      procedure set_DataToSend(SSHChannelId: Integer; valDataToSend: String);

      procedure set_DataToSendB(SSHChannelId: Integer; valDataToSend: TBytes);

      function  get_ForwardingHost(SSHChannelId: Integer): String;

      function  get_ForwardingPort(SSHChannelId: Integer): Integer;

      function  get_OriginAddress(SSHChannelId: Integer): String;

      function  get_OriginPort(SSHChannelId: Integer): Integer;

      function  get_Service(SSHChannelId: Integer): String;

      function  get_Connected: Boolean;

      procedure set_Connected(valConnected: Boolean);

      function  get_FirewallAutoDetect: Boolean;

      procedure set_FirewallAutoDetect(valFirewallAutoDetect: Boolean);

      function  get_FirewallType: TiphsshreversetunnelFirewallTypes;

      procedure set_FirewallType(valFirewallType: TiphsshreversetunnelFirewallTypes);

      function  get_FirewallHost: String;

      procedure set_FirewallHost(valFirewallHost: String);

      function  get_FirewallPassword: String;

      procedure set_FirewallPassword(valFirewallPassword: String);

      function  get_FirewallPort: Integer;

      procedure set_FirewallPort(valFirewallPort: Integer);

      function  get_FirewallUser: String;

      procedure set_FirewallUser(valFirewallUser: String);

      function  get_LocalHost: String;

      procedure set_LocalHost(valLocalHost: String);

      function  get_LocalPort: Integer;

      procedure set_LocalPort(valLocalPort: Integer);

      function  get_SSHAcceptServerHostKeyEncoded: String;

      procedure set_SSHAcceptServerHostKeyEncoded(valSSHAcceptServerHostKeyEncoded: String);

      function  get_SSHAcceptServerHostKeyEncodedB: TBytes;

      procedure set_SSHAcceptServerHostKeyEncodedB(valSSHAcceptServerHostKeyEncoded: TBytes);

      function  get_SSHAuthMode: TiphsshreversetunnelSSHAuthModes;

      procedure set_SSHAuthMode(valSSHAuthMode: TiphsshreversetunnelSSHAuthModes);

      function  get_SSHCertEncoded: String;

      procedure set_SSHCertEncoded(valSSHCertEncoded: String);

      function  get_SSHCertEncodedB: TBytes;

      procedure set_SSHCertEncodedB(valSSHCertEncoded: TBytes);

      function  get_SSHCertStore: String;

      procedure set_SSHCertStore(valSSHCertStore: String);

      function  get_SSHCertStoreB: TBytes;

      procedure set_SSHCertStoreB(valSSHCertStore: TBytes);

      function  get_SSHCertStorePassword: String;

      procedure set_SSHCertStorePassword(valSSHCertStorePassword: String);

      function  get_SSHCertStoreType: TiphsshreversetunnelSSHCertStoreTypes;

      procedure set_SSHCertStoreType(valSSHCertStoreType: TiphsshreversetunnelSSHCertStoreTypes);

      function  get_SSHCertSubject: String;

      procedure set_SSHCertSubject(valSSHCertSubject: String);

      function  get_SSHCompressionAlgorithms: String;

      procedure set_SSHCompressionAlgorithms(valSSHCompressionAlgorithms: String);

      function  get_SSHEncryptionAlgorithms: String;

      procedure set_SSHEncryptionAlgorithms(valSSHEncryptionAlgorithms: String);

      function  get_SSHHost: String;

      procedure set_SSHHost(valSSHHost: String);

      function  get_SSHPassword: String;

      procedure set_SSHPassword(valSSHPassword: String);

      function  get_SSHPort: Integer;

      procedure set_SSHPort(valSSHPort: Integer);

      function  get_SSHUser: String;

      procedure set_SSHUser(valSSHUser: String);

      function  get_Timeout: Integer;

      procedure set_Timeout(valTimeout: Integer);


      (*** Property Getters/Setters: OO API ***)
      function  get_Channels: TiphSSHRTChannelList;
      function  get_Firewall: TiphFirewall;
      procedure set_Firewall(Value : TiphFirewall);
      
 
    public
      constructor Create(AOwner: TComponent); overload; override;
      constructor Create(AOwner: TComponent; RuntimeLicense: string); reintroduce; overload;
      destructor Destroy; override;
      function ReportEventException(E: Exception; Event: String; ReFire: Boolean = True): Integer;

      property RuntimeLicense: String read GetRuntimeLicense write SetRuntimeLicense;

      procedure SetDataToSend(SSHChannelId: Integer; lpDataToSend: PLXAnsiChar; lenDataToSend: Cardinal);

      procedure SetSSHAcceptServerHostKeyEncoded(lpSSHAcceptServerHostKeyEncoded: PLXAnsiChar; lenSSHAcceptServerHostKeyEncoded: Cardinal);

      procedure SetSSHCertEncoded(lpSSHCertEncoded: PLXAnsiChar; lenSSHCertEncoded: Cardinal);

      procedure SetSSHCertStore(lpSSHCertStore: PLXAnsiChar; lenSSHCertStore: Cardinal);


      (*** Runtime Property Definitions ***)

      property SSHChannelCount: Integer read get_SSHChannelCount;



      property BytesSent[SSHChannelId: Integer]: Integer read get_BytesSent;



      property ChannelId[SSHChannelId: Integer]: String read get_ChannelId;



      property ConnectedAddress[SSHChannelId: Integer]: String read get_ConnectedAddress;



      property ConnectedPort[SSHChannelId: Integer]: Integer read get_ConnectedPort;



      property DataToSend[SSHChannelId: Integer]: String write set_DataToSend;



      property DataToSendB[SSHChannelId: Integer]: TBytes write set_DataToSendB;



      property ForwardingHost[SSHChannelId: Integer]: String read get_ForwardingHost;



      property ForwardingPort[SSHChannelId: Integer]: Integer read get_ForwardingPort;



      property OriginAddress[SSHChannelId: Integer]: String read get_OriginAddress;



      property OriginPort[SSHChannelId: Integer]: Integer read get_OriginPort;



      property Service[SSHChannelId: Integer]: String read get_Service;



      property Connected: Boolean read get_Connected write set_Connected;



      property SSHAcceptServerHostKeyEncoded: String read get_SSHAcceptServerHostKeyEncoded write set_SSHAcceptServerHostKeyEncoded;



      property SSHAcceptServerHostKeyEncodedB: TBytes read get_SSHAcceptServerHostKeyEncodedB write set_SSHAcceptServerHostKeyEncodedB;



      property SSHCertEncoded: String read get_SSHCertEncoded write set_SSHCertEncoded;



      property SSHCertEncodedB: TBytes read get_SSHCertEncodedB write set_SSHCertEncodedB;



      property SSHCertStoreB: TBytes read get_SSHCertStoreB write set_SSHCertStoreB;



      (*** Runtime properties: OO API ***)
      property Channels: TiphSSHRTChannelList read get_Channels;

      property Firewall: TiphFirewall read get_Firewall write set_Firewall;


{$IFNDEF DELPHI3}
      (*** Method Definitions ***)
      procedure CancelForwarding(Address: String; Port: Integer);

      procedure CloseChannel(ChannelId: String);

      function  Config(ConfigurationString: String): String;

      function  DecodePacket(EncodedPacket: String): TBytes;

      procedure DoEvents();

      function  EncodePacket(Packet: TBytes): String;

      procedure ExchangeKeys();

      function  GetSSHParam(Payload: TBytes; Field: String): String;

      function  GetSSHParamBytes(Payload: TBytes; Field: String): TBytes;

      procedure RequestForwarding(Address: String; Port: Integer; ForwardingHost: String; ForwardingPort: Integer);

      procedure Reset();

      procedure SendChannelData(ChannelId: String; Data: TBytes);

      function  SetSSHParam(Payload: TBytes; FieldType: String; FieldValue: String): TBytes;

      procedure SSHLogoff();

      procedure SSHLogon(SSHHost: String; SSHPort: Integer);

{$ENDIF}

    published
      (*** Design-time Property Definitions ***)

      property FirewallAutoDetect: Boolean read get_FirewallAutoDetect write set_FirewallAutoDetect default False;



      property FirewallType: TiphsshreversetunnelFirewallTypes read get_FirewallType write set_FirewallType default fwNone;



      property FirewallHost: String read get_FirewallHost write set_FirewallHost;



      property FirewallPassword: String read get_FirewallPassword write set_FirewallPassword;



      property FirewallPort: Integer read get_FirewallPort write set_FirewallPort default 0;



      property FirewallUser: String read get_FirewallUser write set_FirewallUser;



      property LocalHost: String read get_LocalHost write set_LocalHost stored False;



      property LocalPort: Integer read get_LocalPort write set_LocalPort default 0;



      property SSHAuthMode: TiphsshreversetunnelSSHAuthModes read get_SSHAuthMode write set_SSHAuthMode default amPassword;



      property SSHCertStore: String read get_SSHCertStore write set_SSHCertStore;



      property SSHCertStorePassword: String read get_SSHCertStorePassword write set_SSHCertStorePassword;



      property SSHCertStoreType: TiphsshreversetunnelSSHCertStoreTypes read get_SSHCertStoreType write set_SSHCertStoreType default cstUser;



      property SSHCertSubject: String read get_SSHCertSubject write set_SSHCertSubject;



      property SSHCompressionAlgorithms: String read get_SSHCompressionAlgorithms write set_SSHCompressionAlgorithms;



      property SSHEncryptionAlgorithms: String read get_SSHEncryptionAlgorithms write set_SSHEncryptionAlgorithms;



      property SSHHost: String read get_SSHHost write set_SSHHost;



      property SSHPassword: String read get_SSHPassword write set_SSHPassword;



      property SSHPort: Integer read get_SSHPort write set_SSHPort default 22;



      property SSHUser: String read get_SSHUser write set_SSHUser;



      property Timeout: Integer read get_Timeout write set_Timeout default 60;


      (*** Event Handler Bindings ***)
      property OnConnected: TConnectedEvent read FOnConnected write FOnConnected;

      property OnConnectionStatus: TConnectionStatusEvent read FOnConnectionStatus write FOnConnectionStatus;

      property OnDisconnected: TDisconnectedEvent read FOnDisconnected write FOnDisconnected;

      property OnError: TErrorEvent read FOnError write FOnError;

      property OnLog: TLogEvent read FOnLog write FOnLog;

      property OnReconnectAttempt: TReconnectAttemptEvent read FOnReconnectAttempt write FOnReconnectAttempt;

      property OnSSHChannelClosed: TSSHChannelClosedEvent read FOnSSHChannelClosed write FOnSSHChannelClosed;

      property OnSSHChannelData: TSSHChannelDataEvent read FOnSSHChannelData write FOnSSHChannelData;

      property OnSSHChannelEOF: TSSHChannelEOFEvent read FOnSSHChannelEOF write FOnSSHChannelEOF;

      property OnSSHChannelOpened: TSSHChannelOpenedEvent read FOnSSHChannelOpened write FOnSSHChannelOpened;

      property OnSSHChannelOpenRequest: TSSHChannelOpenRequestEvent read FOnSSHChannelOpenRequest write FOnSSHChannelOpenRequest;

      property OnSSHChannelReadyToSend: TSSHChannelReadyToSendEvent read FOnSSHChannelReadyToSend write FOnSSHChannelReadyToSend;

      property OnSSHChannelRequest: TSSHChannelRequestEvent read FOnSSHChannelRequest write FOnSSHChannelRequest;

      property OnSSHChannelRequested: TSSHChannelRequestedEvent read FOnSSHChannelRequested write FOnSSHChannelRequested;

      property OnSSHCustomAuth: TSSHCustomAuthEvent read FOnSSHCustomAuth write FOnSSHCustomAuth;

      property OnSSHKeyboardInteractive: TSSHKeyboardInteractiveEvent read FOnSSHKeyboardInteractive write FOnSSHKeyboardInteractive;

      property OnSSHServerAuthentication: TSSHServerAuthenticationEvent read FOnSSHServerAuthentication write FOnSSHServerAuthentication;

      property OnSSHStatus: TSSHStatusEvent read FOnSSHStatus write FOnSSHStatus;

    end;

    procedure Register;

implementation


{$T-}

{$IFDEF MSWINDOWS} // Both DLL and DRU loading require Windows dependency
{$IFDEF FPC}  //Lazarus
uses Messages;
{$ELSE}
uses {$IF CompilerVersion >= 23}Winapi.Windows{$ELSE}Windows{$IFEND}, {$IF CompilerVersion >= 23}Winapi.Messages{$ELSE}Messages{$IFEND};
{$ENDIF}
{$ENDIF}

const
  PID_SSHReverseTunnel_SSHChannelCount                                    = 1;

  PID_SSHReverseTunnel_BytesSent                                          = 2;

  PID_SSHReverseTunnel_ChannelId                                          = 3;

  PID_SSHReverseTunnel_ConnectedAddress                                   = 4;

  PID_SSHReverseTunnel_ConnectedPort                                      = 5;

  PID_SSHReverseTunnel_DataToSend                                         = 6;

  PID_SSHReverseTunnel_ForwardingHost                                     = 7;

  PID_SSHReverseTunnel_ForwardingPort                                     = 8;

  PID_SSHReverseTunnel_OriginAddress                                      = 9;

  PID_SSHReverseTunnel_OriginPort                                         = 10;

  PID_SSHReverseTunnel_Service                                            = 11;

  PID_SSHReverseTunnel_Connected                                          = 12;

  PID_SSHReverseTunnel_FirewallAutoDetect                                 = 13;

  PID_SSHReverseTunnel_FirewallType                                       = 14;

  PID_SSHReverseTunnel_FirewallHost                                       = 15;

  PID_SSHReverseTunnel_FirewallPassword                                   = 16;

  PID_SSHReverseTunnel_FirewallPort                                       = 17;

  PID_SSHReverseTunnel_FirewallUser                                       = 18;

  PID_SSHReverseTunnel_LocalHost                                          = 19;

  PID_SSHReverseTunnel_LocalPort                                          = 20;

  PID_SSHReverseTunnel_SSHAcceptServerHostKeyEncoded                      = 22;

  PID_SSHReverseTunnel_SSHAuthMode                                        = 49;

  PID_SSHReverseTunnel_SSHCertEncoded                                     = 51;

  PID_SSHReverseTunnel_SSHCertStore                                       = 67;

  PID_SSHReverseTunnel_SSHCertStorePassword                               = 68;

  PID_SSHReverseTunnel_SSHCertStoreType                                   = 69;

  PID_SSHReverseTunnel_SSHCertSubject                                     = 70;

  PID_SSHReverseTunnel_SSHCompressionAlgorithms                           = 78;

  PID_SSHReverseTunnel_SSHEncryptionAlgorithms                            = 79;

  PID_SSHReverseTunnel_SSHHost                                            = 80;

  PID_SSHReverseTunnel_SSHPassword                                        = 81;

  PID_SSHReverseTunnel_SSHPort                                            = 82;

  PID_SSHReverseTunnel_SSHUser                                            = 83;

  PID_SSHReverseTunnel_Timeout                                            = 84;

  EID_SSHReverseTunnel_Connected = 1;

  EID_SSHReverseTunnel_ConnectionStatus = 2;

  EID_SSHReverseTunnel_Disconnected = 3;

  EID_SSHReverseTunnel_Error = 4;

  EID_SSHReverseTunnel_Log = 5;

  EID_SSHReverseTunnel_ReconnectAttempt = 6;

  EID_SSHReverseTunnel_SSHChannelClosed = 7;

  EID_SSHReverseTunnel_SSHChannelData = 8;

  EID_SSHReverseTunnel_SSHChannelEOF = 9;

  EID_SSHReverseTunnel_SSHChannelOpened = 10;

  EID_SSHReverseTunnel_SSHChannelOpenRequest = 11;

  EID_SSHReverseTunnel_SSHChannelReadyToSend = 12;

  EID_SSHReverseTunnel_SSHChannelRequest = 13;

  EID_SSHReverseTunnel_SSHChannelRequested = 14;

  EID_SSHReverseTunnel_SSHCustomAuth = 15;

  EID_SSHReverseTunnel_SSHKeyboardInteractive = 16;

  EID_SSHReverseTunnel_SSHServerAuthentication = 17;

  EID_SSHReverseTunnel_SSHStatus = 18;

  MID_SSHReverseTunnel_CancelForwarding = 2;

  MID_SSHReverseTunnel_CloseChannel = 3;

  MID_SSHReverseTunnel_Config = 4;

  MID_SSHReverseTunnel_DecodePacket = 5;

  MID_SSHReverseTunnel_DoEvents = 6;

  MID_SSHReverseTunnel_EncodePacket = 7;

  MID_SSHReverseTunnel_ExchangeKeys = 8;

  MID_SSHReverseTunnel_GetSSHParam = 9;

  MID_SSHReverseTunnel_GetSSHParamBytes = 10;

  MID_SSHReverseTunnel_RequestForwarding = 11;

  MID_SSHReverseTunnel_Reset = 12;

  MID_SSHReverseTunnel_SendChannelData = 13;

  MID_SSHReverseTunnel_SetSSHParam = 14;

  MID_SSHReverseTunnel_SSHLogoff = 15;

  MID_SSHReverseTunnel_SSHLogon = 16;

  CREATE_OPT = {$IFDEF UNICODE}1{$ELSE}0{$ENDIF};
  EVTSTR_OPT = {$IFDEF UNICODE}2{$ELSE}1{$ENDIF};

  {$IFDEF MSWINDOWS}
  DLLNAME = 'IPWORKSSSH22.DLL';
  {$ENDIF}
  {$IFDEF LINUX}
  DLLNAME = 'libipworksssh.so.22.0';
  {$ENDIF}
  {$IFDEF ANDROID}
  {$IFDEF USE_DYNAMIC_LOADING}
  DLLNAME = 'libipworksssh.so.22.0';
  {$ELSE}
  DLLNAME = 'libipworksssh.22.0.a';
  {$ENDIF}
  {$ENDIF}
  {$IFDEF IOS}
  DLLNAME = 'libipworksssh.22.0.a';
  {$ELSE}
  {$IFDEF MACOS}
  DLLNAME = '@rpath/libipworksssh.22.0.dylib';
  {$ENDIF}
  {$ENDIF}

{$WARNINGS OFF}

type
  INTARR = array[0..32] of Integer;
  LPINTARR = ^INTARR;
  LPINT = ^Cardinal;
  MEventHandle = function(lpContext: TiphSSHReverseTunnel; event_id: Integer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  PEventHandle = ^MEventHandle;


{$IFNDEF USEIPWORKSSSHDLL}
var
  pEntryPoint: Pointer;
  pBaseAddress: Pointer;
  iLoadCount: Integer;
{$ELSE}
{$IFDEF USE_DYNAMIC_LOADING}
var
  hLib : {$ifndef FPC}HMODULE{$else}TLibHandle{$endif};
  iLoadCount: Integer;
{$ENDIF USE_DYNAMIC_LOADING}
{$ENDIF USEIPWORKSSSHDLL}


{$IFDEF USE_DYNAMIC_LOADING}
  _IPWorksSSH_EvtStr:                                 function(pEvtStr: Pointer; id: Integer; value: Pointer; opt: Integer): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_Stream:                                 function(pStream: Pointer; op: Integer; params: LPVOIDARR; retVal: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHReverseTunnel_Create:                function(pMethod: PEventHandle; pObject: TiphSSHReverseTunnel; pKey: Pointer; opts: Integer): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHReverseTunnel_Destroy:               function(p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHReverseTunnel_Set:                   function(p: Pointer; index: Integer; arridx: Integer; value: Pointer; len: Cardinal): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHReverseTunnel_Get:                   function(p: Pointer; index: Integer; arridx: Integer; len: LPINT; llVal: PInt64): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHReverseTunnel_GetLastError:          function(p: Pointer): PLXAnsiChar; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode:      function(p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHReverseTunnel_SetLastErrorAndCode:   function(p: Pointer; code: Integer; message: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHReverseTunnel_GetEventError:         function(p: Pointer): PLXAnsiChar; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHReverseTunnel_GetEventErrorCode:     function(p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHReverseTunnel_SetEventErrorAndCode:  function(p: Pointer; code: Integer; message: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  {$IFDEF USE_EXPLICIT_INIT}
  _IPWorksSSH_SSHReverseTunnel_StaticInit:            function(instance: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHReverseTunnel_StaticDestroy:         function(): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  {$ENDIF}
  _IPWorksSSH_SSHReverseTunnel_CheckIndex:            function(p: Pointer; index: Integer; arridx: Integer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
  _IPWorksSSH_SSHReverseTunnel_Do:                    function(p: Pointer; method_id: Integer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR; llVal: PInt64): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
{$ELSE}
  function _IPWorksSSH_EvtStr                                (pEvtStr: Pointer; id: Integer; value: Pointer; opt: Integer): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_EvtStr' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_Stream                                (pStream: Pointer; op: Integer; params: LPVOIDARR; retVal: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_Stream' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHReverseTunnel_Create               (pMethod: PEventHandle; pObject: TiphSSHReverseTunnel; pKey: Pointer; opts: Integer): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHReverseTunnel_Create' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHReverseTunnel_Destroy              (p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHReverseTunnel_Destroy' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHReverseTunnel_Set                  (p: Pointer; index: Integer; arridx: Integer; value: Pointer; len: Cardinal): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHReverseTunnel_Set' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHReverseTunnel_Get                  (p: Pointer; index: Integer; arridx: Integer; len: LPINT; llVal: PInt64): Pointer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHReverseTunnel_Get' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHReverseTunnel_GetLastError         (p: Pointer): PLXAnsiChar; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHReverseTunnel_GetLastError' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode     (p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHReverseTunnel_GetLastErrorCode' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHReverseTunnel_SetLastErrorAndCode  (p: Pointer; code: Integer; message: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHReverseTunnel_SetLastErrorAndCode' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHReverseTunnel_GetEventError        (p: Pointer): PLXAnsiChar; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHReverseTunnel_GetEventError' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHReverseTunnel_GetEventErrorCode    (p: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHReverseTunnel_GetEventErrorCode' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHReverseTunnel_SetEventErrorAndCode (p: Pointer; code: Integer; message: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHReverseTunnel_SetEventErrorAndCode' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHReverseTunnel_StaticInit           (instance: Pointer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHReverseTunnel_StaticInit' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHReverseTunnel_StaticDestroy        (): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHReverseTunnel_StaticDestroy' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHReverseTunnel_CheckIndex           (p: Pointer; index: Integer; arridx: Integer): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHReverseTunnel_CheckIndex' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
  function _IPWorksSSH_SSHReverseTunnel_Do                   (p: Pointer; method_id: Integer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR; llVal: PInt64): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF}; external DLLNAME name {$IFNDEF FPC}{$IFDEF POSIX}_PU +{$ENDIF}{$ENDIF} 'IPWorksSSH_SSHReverseTunnel_Do' {$ifdef USE_DELAYED_LOADING} delayed {$endif};
{$ENDIF}

{$HINTS OFF}

(*** Fire Event Methods ***)
function FireConnected(lpContext: TiphSSHReverseTunnel; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_StatusCode: Integer;

  tmp_Description: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnConnected) then exit;

  tmp_StatusCode := Integer(params^[0]);

  tmp_Description := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  try lpContext.FOnConnected(lpContext, tmp_StatusCode, tmp_Description)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Connected'); end;

end;


function FireConnectionStatus(lpContext: TiphSSHReverseTunnel; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ConnectionEvent: String;

  tmp_StatusCode: Integer;

  tmp_Description: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnConnectionStatus) then exit;

  tmp_ConnectionEvent := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[0]);
  tmp_StatusCode := Integer(params^[1]);

  tmp_Description := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  try lpContext.FOnConnectionStatus(lpContext, tmp_ConnectionEvent, tmp_StatusCode, tmp_Description)
  except on E: Exception do result := lpContext.ReportEventException(E, 'ConnectionStatus'); end;

end;


function FireDisconnected(lpContext: TiphSSHReverseTunnel; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_StatusCode: Integer;

  tmp_Description: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnDisconnected) then exit;

  tmp_StatusCode := Integer(params^[0]);

  tmp_Description := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  try lpContext.FOnDisconnected(lpContext, tmp_StatusCode, tmp_Description)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Disconnected'); end;

end;


function FireError(lpContext: TiphSSHReverseTunnel; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ErrorCode: Integer;

  tmp_Description: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnError) then exit;

  tmp_ErrorCode := Integer(params^[0]);

  tmp_Description := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  try lpContext.FOnError(lpContext, tmp_ErrorCode, tmp_Description)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Error', False); end;

end;


function FireLog(lpContext: TiphSSHReverseTunnel; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_LogLevel: Integer;

  tmp_Message: String;

  tmp_LogType: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnLog) then exit;

  tmp_LogLevel := Integer(params^[0]);

  tmp_Message := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_LogType := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  try lpContext.FOnLog(lpContext, tmp_LogLevel, tmp_Message, tmp_LogType)
  except on E: Exception do result := lpContext.ReportEventException(E, 'Log'); end;

end;


function FireReconnectAttempt(lpContext: TiphSSHReverseTunnel; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_Host: String;

  tmp_Port: Integer;

  tmp_RetryCount: Integer;

  tmp_RetriesRemaining: Integer;

  tmp_MaxRetryCount: Integer;

  tmp_RetryInterval: Integer;

  tmp_RetryType: Integer;

  tmp_RemoteListeningPort: Integer;

  tmp_Cancel: Boolean;

begin
  result := 0;
  if not Assigned(lpContext.FOnReconnectAttempt) then exit;

  tmp_Host := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[0]);
  tmp_Port := Integer(params^[1]);

  tmp_RetryCount := Integer(params^[2]);

  tmp_RetriesRemaining := Integer(params^[3]);

  tmp_MaxRetryCount := Integer(params^[4]);

  tmp_RetryInterval := Integer(params^[5]);

  tmp_RetryType := Integer(params^[6]);

  tmp_RemoteListeningPort := Integer(params^[7]);

  tmp_Cancel := Boolean(params^[8]);

  try lpContext.FOnReconnectAttempt(lpContext, tmp_Host, tmp_Port, tmp_RetryCount, tmp_RetriesRemaining, tmp_MaxRetryCount, tmp_RetryInterval, tmp_RetryType, tmp_RemoteListeningPort, tmp_Cancel)
  except on E: Exception do result := lpContext.ReportEventException(E, 'ReconnectAttempt'); end;

  params^[8] := Pointer(tmp_Cancel);

end;


function FireSSHChannelClosed(lpContext: TiphSSHReverseTunnel; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ChannelId: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHChannelClosed) then exit;

  tmp_ChannelId := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[0]);
  try lpContext.FOnSSHChannelClosed(lpContext, tmp_ChannelId)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHChannelClosed'); end;

end;


function FireSSHChannelData(lpContext: TiphSSHReverseTunnel; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ChannelId: String;

  tmp_ChannelData: String;
  tmp_ChannelDataB: TBytes;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHChannelData) then exit;

  tmp_ChannelId := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[0]);
  SetLength(tmp_ChannelDataB, cbparam^[1]);
  Move(Pointer(params^[1])^, Pointer(tmp_ChannelDataB)^, cbparam^[1]); // IMPORTANT: Do NOT multiply the length value!
  tmp_ChannelData := lpContext.BStr2CStr(params^[1], cbparam^[1], tmp_ChannelDataB);
  try lpContext.FOnSSHChannelData(lpContext, tmp_ChannelId, tmp_ChannelData, tmp_ChannelDataB)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHChannelData'); end;

end;


function FireSSHChannelEOF(lpContext: TiphSSHReverseTunnel; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ChannelId: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHChannelEOF) then exit;

  tmp_ChannelId := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[0]);
  try lpContext.FOnSSHChannelEOF(lpContext, tmp_ChannelId)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHChannelEOF'); end;

end;


function FireSSHChannelOpened(lpContext: TiphSSHReverseTunnel; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ChannelId: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHChannelOpened) then exit;

  tmp_ChannelId := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[0]);
  try lpContext.FOnSSHChannelOpened(lpContext, tmp_ChannelId)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHChannelOpened'); end;

end;


function FireSSHChannelOpenRequest(lpContext: TiphSSHReverseTunnel; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ChannelId: String;

  tmp_Service: String;

  tmp_ConnectedAddress: String;

  tmp_ConnectedPort: Integer;

  tmp_OriginAddress: String;

  tmp_OriginPort: Integer;

  tmp_Accept: Boolean;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHChannelOpenRequest) then exit;

  tmp_ChannelId := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[0]);
  tmp_Service := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_ConnectedAddress := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_ConnectedPort := Integer(params^[3]);

  tmp_OriginAddress := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[4]);
  tmp_OriginPort := Integer(params^[5]);

  tmp_Accept := Boolean(params^[6]);

  try lpContext.FOnSSHChannelOpenRequest(lpContext, tmp_ChannelId, tmp_Service, tmp_ConnectedAddress, tmp_ConnectedPort, tmp_OriginAddress, tmp_OriginPort, tmp_Accept)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHChannelOpenRequest'); end;

  params^[6] := Pointer(tmp_Accept);

end;


function FireSSHChannelReadyToSend(lpContext: TiphSSHReverseTunnel; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ChannelId: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHChannelReadyToSend) then exit;

  tmp_ChannelId := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[0]);
  try lpContext.FOnSSHChannelReadyToSend(lpContext, tmp_ChannelId)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHChannelReadyToSend'); end;

end;


function FireSSHChannelRequest(lpContext: TiphSSHReverseTunnel; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ChannelId: String;

  tmp_RequestType: String;

  tmp_Packet: String;
  tmp_PacketB: TBytes;

  tmp_Success: Boolean;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHChannelRequest) then exit;

  tmp_ChannelId := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[0]);
  tmp_RequestType := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  SetLength(tmp_PacketB, cbparam^[2]);
  Move(Pointer(params^[2])^, Pointer(tmp_PacketB)^, cbparam^[2]); // IMPORTANT: Do NOT multiply the length value!
  tmp_Packet := lpContext.BStr2CStr(params^[2], cbparam^[2], tmp_PacketB);
  tmp_Success := Boolean(params^[3]);

  try lpContext.FOnSSHChannelRequest(lpContext, tmp_ChannelId, tmp_RequestType, tmp_Packet, tmp_PacketB, tmp_Success)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHChannelRequest'); end;

  params^[3] := Pointer(tmp_Success);

end;


function FireSSHChannelRequested(lpContext: TiphSSHReverseTunnel; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_ChannelId: String;

  tmp_RequestType: String;

  tmp_Packet: String;
  tmp_PacketB: TBytes;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHChannelRequested) then exit;

  tmp_ChannelId := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[0]);
  tmp_RequestType := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  SetLength(tmp_PacketB, cbparam^[2]);
  Move(Pointer(params^[2])^, Pointer(tmp_PacketB)^, cbparam^[2]); // IMPORTANT: Do NOT multiply the length value!
  tmp_Packet := lpContext.BStr2CStr(params^[2], cbparam^[2], tmp_PacketB);
  try lpContext.FOnSSHChannelRequested(lpContext, tmp_ChannelId, tmp_RequestType, tmp_Packet, tmp_PacketB)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHChannelRequested'); end;

end;


function FireSSHCustomAuth(lpContext: TiphSSHReverseTunnel; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_Packet: String; // dev platform string
  {$ifdef UNICODE_ON_ANSI}
  tmp_PacketB: LXAnsiString; // library string
  {$endif}

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHCustomAuth) then exit;

  tmp_Packet := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(_IPWorksSSH_EvtStr(params^[0], 0, nil, EVTSTR_OPT));
  try lpContext.FOnSSHCustomAuth(lpContext, tmp_Packet)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHCustomAuth'); end;

  {$ifdef UNICODE_ON_ANSI}
  tmp_PacketB := ToLinuxAnsiString(tmp_Packet);
  {$endif}
  _IPWorksSSH_EvtStr(params^[0], 2, {$ifdef UNICODE_ON_ANSI}@tmp_PacketB[0]{$else}PChar(tmp_Packet){$endif}, EVTSTR_OPT);
end;


function FireSSHKeyboardInteractive(lpContext: TiphSSHReverseTunnel; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_Name: String;

  tmp_Instructions: String;

  tmp_Prompt: String;

  tmp_Response: String; // dev platform string
  {$ifdef UNICODE_ON_ANSI}
  tmp_ResponseB: LXAnsiString; // library string
  {$endif}

  tmp_EchoResponse: Boolean;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHKeyboardInteractive) then exit;

  tmp_Name := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[0]);
  tmp_Instructions := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_Prompt := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_Response := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(_IPWorksSSH_EvtStr(params^[3], 0, nil, EVTSTR_OPT));
  tmp_EchoResponse := Boolean(params^[4]);

  try lpContext.FOnSSHKeyboardInteractive(lpContext, tmp_Name, tmp_Instructions, tmp_Prompt, tmp_Response, tmp_EchoResponse)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHKeyboardInteractive'); end;

  {$ifdef UNICODE_ON_ANSI}
  tmp_ResponseB := ToLinuxAnsiString(tmp_Response);
  {$endif}
  _IPWorksSSH_EvtStr(params^[3], 2, {$ifdef UNICODE_ON_ANSI}@tmp_ResponseB[0]{$else}PChar(tmp_Response){$endif}, EVTSTR_OPT);
end;


function FireSSHServerAuthentication(lpContext: TiphSSHReverseTunnel; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_HostKey: String;
  tmp_HostKeyB: TBytes;

  tmp_Fingerprint: String;

  tmp_KeyAlgorithm: String;

  tmp_CertSubject: String;

  tmp_CertIssuer: String;

  tmp_Status: String;

  tmp_Accept: Boolean;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHServerAuthentication) then exit;

  SetLength(tmp_HostKeyB, cbparam^[0]);
  Move(Pointer(params^[0])^, Pointer(tmp_HostKeyB)^, cbparam^[0]); // IMPORTANT: Do NOT multiply the length value!
  tmp_HostKey := lpContext.BStr2CStr(params^[0], cbparam^[0], tmp_HostKeyB);
  tmp_Fingerprint := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[1]);
  tmp_KeyAlgorithm := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[2]);
  tmp_CertSubject := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[3]);
  tmp_CertIssuer := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[4]);
  tmp_Status := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[5]);
  tmp_Accept := Boolean(params^[6]);

  try lpContext.FOnSSHServerAuthentication(lpContext, tmp_HostKey, tmp_HostKeyB, tmp_Fingerprint, tmp_KeyAlgorithm, tmp_CertSubject, tmp_CertIssuer, tmp_Status, tmp_Accept)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHServerAuthentication'); end;

  params^[6] := Pointer(tmp_Accept);

end;


function FireSSHStatus(lpContext: TiphSSHReverseTunnel; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer;
var
  tmp_Message: String;

begin
  result := 0;
  if not Assigned(lpContext.FOnSSHStatus) then exit;

  tmp_Message := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(params^[0]);
  try lpContext.FOnSSHStatus(lpContext, tmp_Message)
  except on E: Exception do result := lpContext.ReportEventException(E, 'SSHStatus'); end;

end;



(*** Event Sink ***)
function FireEvents(lpContext: TiphSSHReverseTunnel; event_id: Integer; cparam: Integer; params: LPVOIDARR; cbparam: LPINTARR): Integer; {$IFDEF USE_CDECL}cdecl{$ELSE}stdcall{$ENDIF};
var
  x: Integer;
begin
  result := 0;

  case event_id of
    EID_SSHReverseTunnel_Connected: begin result := FireConnected(lpContext, cparam, params, cbparam); end;

    EID_SSHReverseTunnel_ConnectionStatus: begin result := FireConnectionStatus(lpContext, cparam, params, cbparam); end;

    EID_SSHReverseTunnel_Disconnected: begin result := FireDisconnected(lpContext, cparam, params, cbparam); end;

    EID_SSHReverseTunnel_Error: begin result := FireError(lpContext, cparam, params, cbparam); end;

    EID_SSHReverseTunnel_Log: begin result := FireLog(lpContext, cparam, params, cbparam); end;

    EID_SSHReverseTunnel_ReconnectAttempt: begin result := FireReconnectAttempt(lpContext, cparam, params, cbparam); end;

    EID_SSHReverseTunnel_SSHChannelClosed: begin result := FireSSHChannelClosed(lpContext, cparam, params, cbparam); end;

    EID_SSHReverseTunnel_SSHChannelData: begin result := FireSSHChannelData(lpContext, cparam, params, cbparam); end;

    EID_SSHReverseTunnel_SSHChannelEOF: begin result := FireSSHChannelEOF(lpContext, cparam, params, cbparam); end;

    EID_SSHReverseTunnel_SSHChannelOpened: begin result := FireSSHChannelOpened(lpContext, cparam, params, cbparam); end;

    EID_SSHReverseTunnel_SSHChannelOpenRequest: begin result := FireSSHChannelOpenRequest(lpContext, cparam, params, cbparam); end;

    EID_SSHReverseTunnel_SSHChannelReadyToSend: begin result := FireSSHChannelReadyToSend(lpContext, cparam, params, cbparam); end;

    EID_SSHReverseTunnel_SSHChannelRequest: begin result := FireSSHChannelRequest(lpContext, cparam, params, cbparam); end;

    EID_SSHReverseTunnel_SSHChannelRequested: begin result := FireSSHChannelRequested(lpContext, cparam, params, cbparam); end;

    EID_SSHReverseTunnel_SSHCustomAuth: begin result := FireSSHCustomAuth(lpContext, cparam, params, cbparam); end;

    EID_SSHReverseTunnel_SSHKeyboardInteractive: begin result := FireSSHKeyboardInteractive(lpContext, cparam, params, cbparam); end;

    EID_SSHReverseTunnel_SSHServerAuthentication: begin result := FireSSHServerAuthentication(lpContext, cparam, params, cbparam); end;

    EID_SSHReverseTunnel_SSHStatus: begin result := FireSSHStatus(lpContext, cparam, params, cbparam); end;

    99999: begin x := 0; end;
  end;
end;

procedure Register;
begin
  iphCore.Register;
  RegisterComponents('IPWorks SSH', [TiphSSHReverseTunnel]);
end;

{*********************************************************************************}
procedure TiphSSHReverseTunnel.SetDataToSend(SSHChannelId: Integer; lpDataToSend: PLXAnsiChar; lenDataToSend: Cardinal);
var
  err: integer;
begin
  if @_IPWorksSSH_SSHReverseTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_DataToSend, SSHChannelId, Pointer(lpDataToSend), lenDataToSend);
  if err <> 0 then TreatErr(err, '');
end;

procedure TiphSSHReverseTunnel.SetSSHAcceptServerHostKeyEncoded(lpSSHAcceptServerHostKeyEncoded: PLXAnsiChar; lenSSHAcceptServerHostKeyEncoded: Cardinal);
var
  err: integer;
begin
  if @_IPWorksSSH_SSHReverseTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_SSHAcceptServerHostKeyEncoded, 0, Pointer(lpSSHAcceptServerHostKeyEncoded), lenSSHAcceptServerHostKeyEncoded);
  if err <> 0 then TreatErr(err, '');
end;

procedure TiphSSHReverseTunnel.SetSSHCertEncoded(lpSSHCertEncoded: PLXAnsiChar; lenSSHCertEncoded: Cardinal);
var
  err: integer;
begin
  if @_IPWorksSSH_SSHReverseTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_SSHCertEncoded, 0, Pointer(lpSSHCertEncoded), lenSSHCertEncoded);
  if err <> 0 then TreatErr(err, '');
end;

procedure TiphSSHReverseTunnel.SetSSHCertStore(lpSSHCertStore: PLXAnsiChar; lenSSHCertStore: Cardinal);
var
  err: integer;
begin
  if @_IPWorksSSH_SSHReverseTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_SSHCertStore, 0, Pointer(lpSSHCertStore), lenSSHCertStore);
  if err <> 0 then TreatErr(err, '');
end;


type
  TiphChannelsSSHRTChannelList = class(TiphSSHRTChannelList)
  protected
    function OwnerCtl : TiphSSHReverseTunnel;
    // Only need to override one or two methods here
    function CtlGetCount: integer; override;
    procedure CtlSetCount(Value : integer); override;
    function CreateElemInstance(Index : integer): TiphSSHRTChannel; override;
  end;

  TiphChannelsSSHRTChannel = class(TiphSSHRTChannel)
  protected
    function OwnerCtl : TiphSSHReverseTunnel;
    function GetPropBytesSent: Integer; override;

    function GetPropChannelId: String; override;

    function GetPropConnectedAddress: String; override;

    function GetPropConnectedPort: Integer; override;

    procedure SetPropDataToSend(Value: String); override;

    procedure SetPropDataToSendB(Value: TBytes); override;

    function GetPropForwardingHost: String; override;

    procedure SetPropForwardingHost(Value: String); override;

    function GetPropForwardingPort: Integer; override;

    procedure SetPropForwardingPort(Value: Integer); override;

    function GetPropOriginAddress: String; override;

    function GetPropOriginPort: Integer; override;

    function GetPropService: String; override;

  end;

function TiphChannelsSSHRTChannel.OwnerCtl : TiphSSHReverseTunnel;
begin
  Result := TiphSSHReverseTunnel(FOwnerCtl);
end;

function TiphChannelsSSHRTChannel.GetPropBytesSent: Integer;
begin
  Result := (OwnerCtl.get_BytesSent(Index));
end;


function TiphChannelsSSHRTChannel.GetPropChannelId: String;
begin
  Result := (OwnerCtl.get_ChannelId(Index));
end;


function TiphChannelsSSHRTChannel.GetPropConnectedAddress: String;
begin
  Result := (OwnerCtl.get_ConnectedAddress(Index));
end;


function TiphChannelsSSHRTChannel.GetPropConnectedPort: Integer;
begin
  Result := (OwnerCtl.get_ConnectedPort(Index));
end;


procedure TiphChannelsSSHRTChannel.SetPropDataToSend(Value: String);
begin
  OwnerCtl.set_DataToSend(Index, (Value));
end;

procedure TiphChannelsSSHRTChannel.SetPropDataToSendB(Value: TBytes); 
begin
  OwnerCtl.set_DataToSendB(Index, Value);
end;

function TiphChannelsSSHRTChannel.GetPropForwardingHost: String;
begin
  Result := (OwnerCtl.get_ForwardingHost(Index));
end;


procedure TiphChannelsSSHRTChannel.SetPropForwardingHost(Value: String);
begin
  // The field is read-only
end;

function TiphChannelsSSHRTChannel.GetPropForwardingPort: Integer;
begin
  Result := (OwnerCtl.get_ForwardingPort(Index));
end;


procedure TiphChannelsSSHRTChannel.SetPropForwardingPort(Value: Integer);
begin
  // The field is read-only
end;

function TiphChannelsSSHRTChannel.GetPropOriginAddress: String;
begin
  Result := (OwnerCtl.get_OriginAddress(Index));
end;


function TiphChannelsSSHRTChannel.GetPropOriginPort: Integer;
begin
  Result := (OwnerCtl.get_OriginPort(Index));
end;


function TiphChannelsSSHRTChannel.GetPropService: String;
begin
  Result := (OwnerCtl.get_Service(Index));
end;



function TiphChannelsSSHRTChannelList.OwnerCtl : TiphSSHReverseTunnel;
begin
  Result := TiphSSHReverseTunnel(FOwnerCtl);
end;

// Collection's overrides
function TiphChannelsSSHRTChannelList.CtlGetCount: integer;
begin
  Result := OwnerCtl.get_SSHChannelCount();
end;

procedure TiphChannelsSSHRTChannelList.CtlSetCount(Value : integer);
begin
  // The collection is read-only
end;

function TiphChannelsSSHRTChannelList.CreateElemInstance(Index : integer): TiphSSHRTChannel;
begin
  Result := TiphChannelsSSHRTChannel.Create(FOwnerCtl, true);
  Result.Index := Index;
end;

type
  TiphFirewallFirewall = class(TiphFirewall)
  protected
    function OwnerCtl : TiphSSHReverseTunnel;
    function GetPropAutoDetect: Boolean; override;

    procedure SetPropAutoDetect(Value: Boolean); override;

    function GetPropFirewallType: TiphFirewallTypes; override;

    procedure SetPropFirewallType(Value: TiphFirewallTypes); override;

    function GetPropHost: String; override;

    procedure SetPropHost(Value: String); override;

    function GetPropPassword: String; override;

    procedure SetPropPassword(Value: String); override;

    function GetPropPort: Integer; override;

    procedure SetPropPort(Value: Integer); override;

    function GetPropUser: String; override;

    procedure SetPropUser(Value: String); override;

  end;

function TiphFirewallFirewall.OwnerCtl : TiphSSHReverseTunnel;
begin
  Result := TiphSSHReverseTunnel(FOwnerCtl);
end;

function TiphFirewallFirewall.GetPropAutoDetect: Boolean;
begin
  Result := (OwnerCtl.get_FirewallAutoDetect());
end;

procedure TiphFirewallFirewall.SetPropAutoDetect(Value: Boolean);
begin
  OwnerCtl.set_FirewallAutoDetect((Value));
end;

function TiphFirewallFirewall.GetPropFirewallType: TiphFirewallTypes;
begin
  Result := TiphFirewallTypes(OwnerCtl.get_FirewallType());
end;

procedure TiphFirewallFirewall.SetPropFirewallType(Value: TiphFirewallTypes);
begin
  OwnerCtl.set_FirewallType(TiphsshreversetunnelFirewallTypes(Value));
end;

function TiphFirewallFirewall.GetPropHost: String;
begin
  Result := (OwnerCtl.get_FirewallHost());
end;

procedure TiphFirewallFirewall.SetPropHost(Value: String);
begin
  OwnerCtl.set_FirewallHost((Value));
end;

function TiphFirewallFirewall.GetPropPassword: String;
begin
  Result := (OwnerCtl.get_FirewallPassword());
end;

procedure TiphFirewallFirewall.SetPropPassword(Value: String);
begin
  OwnerCtl.set_FirewallPassword((Value));
end;

function TiphFirewallFirewall.GetPropPort: Integer;
begin
  Result := (OwnerCtl.get_FirewallPort());
end;

procedure TiphFirewallFirewall.SetPropPort(Value: Integer);
begin
  OwnerCtl.set_FirewallPort((Value));
end;

function TiphFirewallFirewall.GetPropUser: String;
begin
  Result := (OwnerCtl.get_FirewallUser());
end;

procedure TiphFirewallFirewall.SetPropUser(Value: String);
begin
  OwnerCtl.set_FirewallUser((Value));
end;



{*********************************************************************************}

constructor TiphSSHReverseTunnel.Create(AOwner: TComponent);
begin
  Create(AOwner, IPWORKSSSH_OEMKEY_19);
  Config('CodePage=65001');
end;

constructor TiphSSHReverseTunnel.Create(AOwner: TComponent; RuntimeLicense: string);
begin
  inherited Create(AOwner);
  m_ctl := nil;

  if @_IPWorksSSH_SSHReverseTunnel_Create <> nil then
    m_ctl := _IPWorksSSH_SSHReverseTunnel_Create(@FireEvents, self, nil, CREATE_OPT);
  if m_ctl = nil then
    TreatErr(-1, 'IPWorks SSH SSHReverseTunnel: Error creating component');
  _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, 8002{PID_KEYCHECK_DLP}, 0, 
    {$ifdef ANSICHAR_IS_BYTE}
    Pointer(ToLinuxAnsiString(RuntimeLicense)),
    {$else}
    Pointer(PAnsiChar(AnsiString(RuntimeLicense))),
    {$endif}
    0);

  FChannels := TiphChannelsSSHRTChannelList.Create(Self, true);

  FFirewall := TiphFirewallFirewall.Create(Self, false);


  try set_FirewallAutoDetect(false) except on E:Exception do end;

  try set_FirewallType(fwNone) except on E:Exception do end;

  try set_FirewallHost('') except on E:Exception do end;

  try set_FirewallPassword('') except on E:Exception do end;

  try set_FirewallPort(0) except on E:Exception do end;

  try set_FirewallUser('') except on E:Exception do end;

  try set_LocalPort(0) except on E:Exception do end;

  try set_SSHAuthMode(amPassword) except on E:Exception do end;

  try set_SSHCertStore('MY') except on E:Exception do end;

  try set_SSHCertStorePassword('') except on E:Exception do end;

  try set_SSHCertStoreType(cstUser) except on E:Exception do end;

  try set_SSHCertSubject('') except on E:Exception do end;

  try set_SSHCompressionAlgorithms('none,zlib') except on E:Exception do end;

  try set_SSHEncryptionAlgorithms('aes256-ctr,aes192-ctr,aes128-ctr,aes256-cbc,aes192-cbc,aes128-cbc,3des-ctr,3des-cbc,blowfish-cbc,arcfour256,arcfour128,arcfour,cast128-cbc,aes256-gcm@openssh.com,aes128-gcm@openssh.com,chacha20-poly1305@openssh.com') except on E:Exception do end;

  try set_SSHHost('') except on E:Exception do end;

  try set_SSHPassword('') except on E:Exception do end;

  try set_SSHPort(22) except on E:Exception do end;

  try set_SSHUser('') except on E:Exception do end;

  try set_Timeout(60) except on E:Exception do end;

end;

destructor TiphSSHReverseTunnel.Destroy;
begin
  FreeAndNil(FChannels);

  FreeAndNil(FFirewall);

  
  
  if m_ctl <> nil then begin
    if @_IPWorksSSH_SSHReverseTunnel_Destroy <> nil then
      _IPWorksSSH_SSHReverseTunnel_Destroy(m_ctl);
  end;
  m_ctl := nil;
  inherited Destroy;
end;

function TiphSSHReverseTunnel.ThrowCoreException(err: Integer; const desc: {$ifndef NO_ANSI}AnsiString{$else}string{$endif}): EIPWorksSSH;
begin
  result := EiphSSHReverseTunnel.CreateCode(err, desc);
end;

function TiphSSHReverseTunnel.ReportEventException(e: Exception; Event: String; ReFire: Boolean): Integer;
var
  msg: String;
begin
  result := -1;
  msg := 'An unhandled error occurred in the ' + Event + ' event handler: ' + E.Message;
  if (m_ctl <> nil) and (@_IPWorksSSH_SSHReverseTunnel_SetEventErrorAndCode <> nil) then begin
    if E is EIPWorksSSH then begin
      result := EIPWorksSSH(e).Code;
      msg := E.Message;
    end;
    _IPWorksSSH_SSHReverseTunnel_SetEventErrorAndCode(m_ctl, result, 
      {$ifdef ANSICHAR_IS_BYTE}
      Pointer(ToLinuxAnsiString(msg)));
      {$else}
      Pointer(PAnsiChar(AnsiString(msg))));
      {$endif}
  end;
  if ReFire and Assigned(FOnError) then FOnError(self, result, msg);
end;

{*********************************************************************************}

procedure TiphSSHReverseTunnel.AboutDlg;
var
  p : LPVOIDARR;
  pc: LPINTARR;
begin
{$IFDEF LINUX}
  inherited AboutDlg;
{$ELSE}
  p := nil;
  pc := nil;
  if @_IPWorksSSH_SSHReverseTunnel_Do <> nil then
    _IPWorksSSH_SSHReverseTunnel_Do(m_ctl, 255, 0, p, pc, nil);
{$ENDIF}
end;

{*********************************************************************************}

procedure TiphSSHReverseTunnel.SetRuntimeLicense(key: String);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHReverseTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, 8001{PID_RUNTIME_LICENSE}, 0, 
    {$ifdef ANSICHAR_IS_BYTE}
    Pointer(ToLinuxAnsiString(key)),
    {$else}
    Pointer(PAnsiChar(AnsiString(key))),
    {$endif}
    0);

  if err <> 0 then TreatErr(err, '');
end;

function TiphSSHReverseTunnel.GetRuntimeLicense: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';
  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, 8001{PID_RUNTIME_LICENSE}, 0, nil, nil);
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$ifdef ANSICHAR_IS_BYTE}
  result := FromLinuxAnsiString(PLXAnsiChar(tmp));
  {$else}
  result := PAnsiChar(tmp);
  {$endif}
end;

{*********************************************************************************}

function TiphSSHReverseTunnel.HasData: Boolean;
begin
  result := false;
end;

procedure TiphSSHReverseTunnel.ReadHnd(Reader: TStream);
begin
end;

procedure TiphSSHReverseTunnel.WriteHnd(Writer: TStream);
begin
end;

function TiphSSHReverseTunnel.BStr2CStr(pBStr: Pointer; lenBStr: Integer; bArr: TBytes): String;
{$IF DEFINED(UNICODE) AND DEFINED(MSWINDOWS)}
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
{$IFEND}
begin
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  param[0] := pBStr; paramcb[0] := lenBStr;
  param[1] := nil;   paramcb[1] := 0;
  param[2] := nil;   paramcb[2] := 0;
  _IPWorksSSH_SSHReverseTunnel_Do(m_ctl, 2003, 2, @param, @paramcb, nil);
  SetLength(Result, Integer(param[2]));
  if Length(Result) > 0 then begin
    param[1] := PChar(Result); paramcb[1] := Length(Result);
    _IPWorksSSH_SSHReverseTunnel_Do(m_ctl, 2003, 2, @param, @paramcb, nil);
  end;
  {$ELSE}
  try
    Result := TEncoding.Default.GetString(bArr);
  except
    on E: Exception do
      SetString(Result, PAnsiChar(pBStr), lenBStr);
   end;
  {$ENDIF}
{$ELSE}
  SetString(Result, PAnsiChar(pBStr), lenBStr);
{$ENDIF}
end;

procedure TiphSSHReverseTunnel.DefineProperties(Filer: TFiler);
begin
  inherited DefineProperties(Filer);
  Filer.DefineBinaryProperty('RegHnd', ReadHnd, WriteHnd, HasData);
end;

{*********************************************************************************}

procedure TiphSSHReverseTunnel.TreatErr(Err: Integer; const desc: String);
var
  msg : {$ifdef NO_ANSI}string{$else}LXAnsiString{$endif};
begin
  {$ifdef NO_ANSI}
  msg := desc;
  {$else}
  {$ifdef ANSICHAR_IS_BYTE}
  msg := ToLinuxAnsiString(desc);
  {$else}
  msg := LXAnsiString(desc);
  {$endif}
  {$endif}
  if Length(msg) = 0 then
  begin
    if @_IPWorksSSH_SSHReverseTunnel_GetLastError <> nil then
      {$ifdef NO_ANSI}
      {$ifdef ANSICHAR_IS_BYTE}
      msg := FromLinuxAnsiString(_IPWorksSSH_SSHReverseTunnel_GetLastError(m_ctl));
      {$else}
      msg := _IPWorksSSH_SSHReverseTunnel_GetLastError(m_ctl);
      {$endif}
      {$else}
      msg := _IPWorksSSH_SSHReverseTunnel_GetLastError(m_ctl);
      {$endif}
  end;

  raise ThrowCoreException(err, msg);
end;

{*********************************************************************************}

(*** Property Getters/Setters ***)
function TiphSSHReverseTunnel.get_SSHChannelCount: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_SSHChannelCount, 0, nil, nil);
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


function TiphSSHReverseTunnel.get_BytesSent(SSHChannelId: Integer): Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);
  if @_IPWorksSSH_SSHReverseTunnel_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_CheckIndex(m_ctl, PID_SSHReverseTunnel_BytesSent, SSHChannelId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for BytesSent');

  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_BytesSent, SSHChannelId, nil, nil);
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


function TiphSSHReverseTunnel.get_ChannelId(SSHChannelId: Integer): String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';
  if @_IPWorksSSH_SSHReverseTunnel_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_CheckIndex(m_ctl, PID_SSHReverseTunnel_ChannelId, SSHChannelId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for ChannelId');

  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_ChannelId{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, SSHChannelId, nil, nil);
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphSSHReverseTunnel.get_ConnectedAddress(SSHChannelId: Integer): String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';
  if @_IPWorksSSH_SSHReverseTunnel_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_CheckIndex(m_ctl, PID_SSHReverseTunnel_ConnectedAddress, SSHChannelId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for ConnectedAddress');

  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_ConnectedAddress{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, SSHChannelId, nil, nil);
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphSSHReverseTunnel.get_ConnectedPort(SSHChannelId: Integer): Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);
  if @_IPWorksSSH_SSHReverseTunnel_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_CheckIndex(m_ctl, PID_SSHReverseTunnel_ConnectedPort, SSHChannelId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for ConnectedPort');

  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_ConnectedPort, SSHChannelId, nil, nil);
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSSHReverseTunnel.set_DataToSend(SSHChannelId: Integer; valDataToSend: String);
var
  err: Integer;
  {$IFDEF NONWINUNICODE}tmp: TBytes;{$ENDIF}
begin
  err := 0;
  if @_IPWorksSSH_SSHReverseTunnel_Set = nil then exit;
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_DataToSend+20000, SSHChannelId, Pointer(PWideChar(valDataToSend)), Length(valDataToSend));
  {$ELSE}
  tmp := TEncoding.UTF8.GetBytes(valDataToSend);
  set_DataToSendB(SSHChannelId, tmp);
  {$ENDIF}
{$ELSE}
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_DataToSend, SSHChannelId, Pointer(PAnsiChar(valDataToSend)), Length(valDataToSend));
{$ENDIF}
  if err <> 0 then TreatErr(err, '');
end;


procedure TiphSSHReverseTunnel.set_DataToSendB(SSHChannelId: Integer; valDataToSend: TBytes);
var
  err: Integer;
begin
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_DataToSend, SSHChannelId, Pointer(valDataToSend), Length(valDataToSend));
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHReverseTunnel.get_ForwardingHost(SSHChannelId: Integer): String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';
  if @_IPWorksSSH_SSHReverseTunnel_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_CheckIndex(m_ctl, PID_SSHReverseTunnel_ForwardingHost, SSHChannelId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for ForwardingHost');

  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_ForwardingHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, SSHChannelId, nil, nil);
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphSSHReverseTunnel.get_ForwardingPort(SSHChannelId: Integer): Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);
  if @_IPWorksSSH_SSHReverseTunnel_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_CheckIndex(m_ctl, PID_SSHReverseTunnel_ForwardingPort, SSHChannelId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for ForwardingPort');

  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_ForwardingPort, SSHChannelId, nil, nil);
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


function TiphSSHReverseTunnel.get_OriginAddress(SSHChannelId: Integer): String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';
  if @_IPWorksSSH_SSHReverseTunnel_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_CheckIndex(m_ctl, PID_SSHReverseTunnel_OriginAddress, SSHChannelId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for OriginAddress');

  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_OriginAddress{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, SSHChannelId, nil, nil);
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphSSHReverseTunnel.get_OriginPort(SSHChannelId: Integer): Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);
  if @_IPWorksSSH_SSHReverseTunnel_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_CheckIndex(m_ctl, PID_SSHReverseTunnel_OriginPort, SSHChannelId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for OriginPort');

  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_OriginPort, SSHChannelId, nil, nil);
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


function TiphSSHReverseTunnel.get_Service(SSHChannelId: Integer): String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';
  if @_IPWorksSSH_SSHReverseTunnel_CheckIndex = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_CheckIndex(m_ctl, PID_SSHReverseTunnel_Service, SSHChannelId);
  if err <> 0 then TreatErr(err, 'Invalid array index value for Service');

  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_Service{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, SSHChannelId, nil, nil);
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


function TiphSSHReverseTunnel.get_Connected: Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);

  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_Connected, 0, nil, nil);
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


procedure TiphSSHReverseTunnel.set_Connected(valConnected: Boolean);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHReverseTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_Connected, 0, Pointer(valConnected), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHReverseTunnel.get_FirewallAutoDetect: Boolean;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Boolean(0);

  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_FirewallAutoDetect, 0, nil, nil);
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Boolean(tmp);
end;


procedure TiphSSHReverseTunnel.set_FirewallAutoDetect(valFirewallAutoDetect: Boolean);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHReverseTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_FirewallAutoDetect, 0, Pointer(valFirewallAutoDetect), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHReverseTunnel.get_FirewallType: TiphsshreversetunnelFirewallTypes;
var
  tmp: Pointer;
  err: Integer;
begin
  result := TiphsshreversetunnelFirewallTypes(0);

  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_FirewallType, 0, nil, nil);
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := TiphsshreversetunnelFirewallTypes(tmp);
end;


procedure TiphSSHReverseTunnel.set_FirewallType(valFirewallType: TiphsshreversetunnelFirewallTypes);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHReverseTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_FirewallType, 0, Pointer(valFirewallType), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHReverseTunnel.get_FirewallHost: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_FirewallHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSSHReverseTunnel.set_FirewallHost(valFirewallHost: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SSHReverseTunnel_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valFirewallHost);
  {$ENDIF}
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_FirewallHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valFirewallHost){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHReverseTunnel.get_FirewallPassword: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_FirewallPassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSSHReverseTunnel.set_FirewallPassword(valFirewallPassword: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SSHReverseTunnel_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valFirewallPassword);
  {$ENDIF}
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_FirewallPassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valFirewallPassword){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHReverseTunnel.get_FirewallPort: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_FirewallPort, 0, nil, nil);
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSSHReverseTunnel.set_FirewallPort(valFirewallPort: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHReverseTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_FirewallPort, 0, Pointer(valFirewallPort), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHReverseTunnel.get_FirewallUser: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_FirewallUser{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSSHReverseTunnel.set_FirewallUser(valFirewallUser: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SSHReverseTunnel_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valFirewallUser);
  {$ENDIF}
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_FirewallUser{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valFirewallUser){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHReverseTunnel.get_LocalHost: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_LocalHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSSHReverseTunnel.set_LocalHost(valLocalHost: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SSHReverseTunnel_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valLocalHost);
  {$ENDIF}
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_LocalHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valLocalHost){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHReverseTunnel.get_LocalPort: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_LocalPort, 0, nil, nil);
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSSHReverseTunnel.set_LocalPort(valLocalPort: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHReverseTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_LocalPort, 0, Pointer(valLocalPort), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHReverseTunnel.get_SSHAcceptServerHostKeyEncoded: String;
var
  tmp: {$IFDEF NONWINUNICODE}TBytes{$ELSE}Pointer; len: Cardinal{$ENDIF};
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := {$IFDEF NONWINUNICODE}get_SSHAcceptServerHostKeyEncodedB{$ELSE}_IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_SSHAcceptServerHostKeyEncoded{$IFDEF UNICODE_ON_UNICODE}+20000{$ENDIF}, 0, @len, nil){$ENDIF};
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$IFDEF NONWINUNICODE}result := TEncoding.UTF8.GetString(tmp);{$ELSE}SetString(result, PChar(tmp), len);{$ENDIF}
end;


procedure TiphSSHReverseTunnel.set_SSHAcceptServerHostKeyEncoded(valSSHAcceptServerHostKeyEncoded: String);
var
  err: Integer;
  {$IFDEF NONWINUNICODE}tmp: TBytes;{$ENDIF}
begin
  err := 0;
  if @_IPWorksSSH_SSHReverseTunnel_Set = nil then exit;
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_SSHAcceptServerHostKeyEncoded+20000, 0, Pointer(PWideChar(valSSHAcceptServerHostKeyEncoded)), Length(valSSHAcceptServerHostKeyEncoded));
  {$ELSE}
  tmp := TEncoding.UTF8.GetBytes(valSSHAcceptServerHostKeyEncoded);
  set_SSHAcceptServerHostKeyEncodedB(tmp);
  {$ENDIF}
{$ELSE}
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_SSHAcceptServerHostKeyEncoded, 0, Pointer(PAnsiChar(valSSHAcceptServerHostKeyEncoded)), Length(valSSHAcceptServerHostKeyEncoded));
{$ENDIF}
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHReverseTunnel.get_SSHAcceptServerHostKeyEncodedB: TBytes;
var
  tmp: Pointer;
  len: Cardinal;
begin
  result := nil;
  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_SSHAcceptServerHostKeyEncoded, 0, @len, nil);
  SetLength(result, len);
  Move(Pointer(tmp)^, Pointer(result)^, len); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSSHReverseTunnel.set_SSHAcceptServerHostKeyEncodedB(valSSHAcceptServerHostKeyEncoded: TBytes);
var
  err: Integer;
begin
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_SSHAcceptServerHostKeyEncoded, 0, Pointer(valSSHAcceptServerHostKeyEncoded), Length(valSSHAcceptServerHostKeyEncoded));
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHReverseTunnel.get_SSHAuthMode: TiphsshreversetunnelSSHAuthModes;
var
  tmp: Pointer;
  err: Integer;
begin
  result := TiphsshreversetunnelSSHAuthModes(0);

  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_SSHAuthMode, 0, nil, nil);
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := TiphsshreversetunnelSSHAuthModes(tmp);
end;


procedure TiphSSHReverseTunnel.set_SSHAuthMode(valSSHAuthMode: TiphsshreversetunnelSSHAuthModes);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHReverseTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_SSHAuthMode, 0, Pointer(valSSHAuthMode), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHReverseTunnel.get_SSHCertEncoded: String;
var
  tmp: {$IFDEF NONWINUNICODE}TBytes{$ELSE}Pointer; len: Cardinal{$ENDIF};
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := {$IFDEF NONWINUNICODE}get_SSHCertEncodedB{$ELSE}_IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_SSHCertEncoded{$IFDEF UNICODE_ON_UNICODE}+20000{$ENDIF}, 0, @len, nil){$ENDIF};
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$IFDEF NONWINUNICODE}result := TEncoding.UTF8.GetString(tmp);{$ELSE}SetString(result, PChar(tmp), len);{$ENDIF}
end;


procedure TiphSSHReverseTunnel.set_SSHCertEncoded(valSSHCertEncoded: String);
var
  err: Integer;
  {$IFDEF NONWINUNICODE}tmp: TBytes;{$ENDIF}
begin
  err := 0;
  if @_IPWorksSSH_SSHReverseTunnel_Set = nil then exit;
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_SSHCertEncoded+20000, 0, Pointer(PWideChar(valSSHCertEncoded)), Length(valSSHCertEncoded));
  {$ELSE}
  tmp := TEncoding.UTF8.GetBytes(valSSHCertEncoded);
  set_SSHCertEncodedB(tmp);
  {$ENDIF}
{$ELSE}
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_SSHCertEncoded, 0, Pointer(PAnsiChar(valSSHCertEncoded)), Length(valSSHCertEncoded));
{$ENDIF}
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHReverseTunnel.get_SSHCertEncodedB: TBytes;
var
  tmp: Pointer;
  len: Cardinal;
begin
  result := nil;
  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_SSHCertEncoded, 0, @len, nil);
  SetLength(result, len);
  Move(Pointer(tmp)^, Pointer(result)^, len); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSSHReverseTunnel.set_SSHCertEncodedB(valSSHCertEncoded: TBytes);
var
  err: Integer;
begin
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_SSHCertEncoded, 0, Pointer(valSSHCertEncoded), Length(valSSHCertEncoded));
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHReverseTunnel.get_SSHCertStore: String;
var
  tmp: {$IFDEF NONWINUNICODE}TBytes{$ELSE}Pointer; len: Cardinal{$ENDIF};
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := {$IFDEF NONWINUNICODE}get_SSHCertStoreB{$ELSE}_IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_SSHCertStore{$IFDEF UNICODE_ON_UNICODE}+20000{$ENDIF}, 0, @len, nil){$ENDIF};
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  {$IFDEF NONWINUNICODE}result := TEncoding.UTF8.GetString(tmp);{$ELSE}SetString(result, PChar(tmp), len);{$ENDIF}
end;


procedure TiphSSHReverseTunnel.set_SSHCertStore(valSSHCertStore: String);
var
  err: Integer;
  {$IFDEF NONWINUNICODE}tmp: TBytes;{$ENDIF}
begin
  err := 0;
  if @_IPWorksSSH_SSHReverseTunnel_Set = nil then exit;
{$IFDEF UNICODE}
  {$IFDEF MSWINDOWS}
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_SSHCertStore+20000, 0, Pointer(PWideChar(valSSHCertStore)), Length(valSSHCertStore));
  {$ELSE}
  tmp := TEncoding.UTF8.GetBytes(valSSHCertStore);
  set_SSHCertStoreB(tmp);
  {$ENDIF}
{$ELSE}
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_SSHCertStore, 0, Pointer(PAnsiChar(valSSHCertStore)), Length(valSSHCertStore));
{$ENDIF}
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHReverseTunnel.get_SSHCertStoreB: TBytes;
var
  tmp: Pointer;
  len: Cardinal;
begin
  result := nil;
  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_SSHCertStore, 0, @len, nil);
  SetLength(result, len);
  Move(Pointer(tmp)^, Pointer(result)^, len); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSSHReverseTunnel.set_SSHCertStoreB(valSSHCertStore: TBytes);
var
  err: Integer;
begin
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_SSHCertStore, 0, Pointer(valSSHCertStore), Length(valSSHCertStore));
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHReverseTunnel.get_SSHCertStorePassword: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_SSHCertStorePassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSSHReverseTunnel.set_SSHCertStorePassword(valSSHCertStorePassword: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SSHReverseTunnel_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHCertStorePassword);
  {$ENDIF}
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_SSHCertStorePassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHCertStorePassword){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHReverseTunnel.get_SSHCertStoreType: TiphsshreversetunnelSSHCertStoreTypes;
var
  tmp: Pointer;
  err: Integer;
begin
  result := TiphsshreversetunnelSSHCertStoreTypes(0);

  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_SSHCertStoreType, 0, nil, nil);
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := TiphsshreversetunnelSSHCertStoreTypes(tmp);
end;


procedure TiphSSHReverseTunnel.set_SSHCertStoreType(valSSHCertStoreType: TiphsshreversetunnelSSHCertStoreTypes);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHReverseTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_SSHCertStoreType, 0, Pointer(valSSHCertStoreType), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHReverseTunnel.get_SSHCertSubject: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_SSHCertSubject{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSSHReverseTunnel.set_SSHCertSubject(valSSHCertSubject: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SSHReverseTunnel_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHCertSubject);
  {$ENDIF}
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_SSHCertSubject{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHCertSubject){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHReverseTunnel.get_SSHCompressionAlgorithms: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_SSHCompressionAlgorithms{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSSHReverseTunnel.set_SSHCompressionAlgorithms(valSSHCompressionAlgorithms: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SSHReverseTunnel_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHCompressionAlgorithms);
  {$ENDIF}
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_SSHCompressionAlgorithms{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHCompressionAlgorithms){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHReverseTunnel.get_SSHEncryptionAlgorithms: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_SSHEncryptionAlgorithms{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSSHReverseTunnel.set_SSHEncryptionAlgorithms(valSSHEncryptionAlgorithms: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SSHReverseTunnel_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHEncryptionAlgorithms);
  {$ENDIF}
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_SSHEncryptionAlgorithms{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHEncryptionAlgorithms){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHReverseTunnel.get_SSHHost: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_SSHHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSSHReverseTunnel.set_SSHHost(valSSHHost: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SSHReverseTunnel_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHHost);
  {$ENDIF}
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_SSHHost{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHHost){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHReverseTunnel.get_SSHPassword: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_SSHPassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSSHReverseTunnel.set_SSHPassword(valSSHPassword: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SSHReverseTunnel_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHPassword);
  {$ENDIF}
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_SSHPassword{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHPassword){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHReverseTunnel.get_SSHPort: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_SSHPort, 0, nil, nil);
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSSHReverseTunnel.set_SSHPort(valSSHPort: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHReverseTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_SSHPort, 0, Pointer(valSSHPort), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHReverseTunnel.get_SSHUser: String;
var
  tmp: Pointer;
  err: Integer;
begin
  result := '';

  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_SSHUser{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, nil, nil);
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := {$ifndef UNICODE_ON_ANSI}PChar{$else}FromLinuxAnsiString{$endif}(tmp);
end;


procedure TiphSSHReverseTunnel.set_SSHUser(valSSHUser: String);
var
  err: Integer;
  {$IFDEF UNICODE_ON_ANSI}tmp: LXAnsiString;{$ENDIF}

begin
  err := 0;
  if @_IPWorksSSH_SSHReverseTunnel_Set = nil then exit;
  {$IFDEF UNICODE_ON_ANSI}
  tmp := ToLinuxAnsiString(valSSHUser);
  {$ENDIF}
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_SSHUser{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, 
    Pointer({$ifndef UNICODE_ON_ANSI}PChar(valSSHUser){$else}@tmp[0]{$endif}), 0);
  if err <> 0 then TreatErr(err, '');
end;


function TiphSSHReverseTunnel.get_Timeout: Integer;
var
  tmp: Pointer;
  err: Integer;
begin
  result := Integer(0);

  if @_IPWorksSSH_SSHReverseTunnel_Get = nil then exit;
  tmp := _IPWorksSSH_SSHReverseTunnel_Get(m_ctl, PID_SSHReverseTunnel_Timeout, 0, nil, nil);
  err := _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode(m_ctl);
  if err <> 0 then TreatErr(err, '');
  result := Integer(tmp);
end;


procedure TiphSSHReverseTunnel.set_Timeout(valTimeout: Integer);
var
  err: Integer;
begin
  err := 0;
  if @_IPWorksSSH_SSHReverseTunnel_Set = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_Set(m_ctl, PID_SSHReverseTunnel_Timeout, 0, Pointer(valTimeout), 0);
  if err <> 0 then TreatErr(err, '');
end;



{*********************************************************************************}

(*** Property Getters/Setters: OO API ***)
function  TiphSSHReverseTunnel.get_Channels: TiphSSHRTChannelList;
begin
  Result := FChannels;
end;

function  TiphSSHReverseTunnel.get_Firewall: TiphFirewall;
begin
  Result := FFirewall;
end;

procedure TiphSSHReverseTunnel.set_Firewall(Value : TiphFirewall);
begin
  FFirewall.Assign(Value);
end;


{*********************************************************************************}

{$IFNDEF DELPHI3}
(*** Methods ***)
procedure TiphSSHReverseTunnel.CancelForwarding(Address: String; Port: Integer);
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
  i, err: Integer;
  tmp_Address: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 2 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  tmp_Address := {$ifndef UNICODE_ON_ANSI}Address{$else}ToLinuxAnsiString(Address){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_Address){$else}@tmp_Address[0]{$endif};

  param[1] := Pointer(Port);

  if @_IPWorksSSH_SSHReverseTunnel_Do = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_Do(m_ctl, MID_SSHReverseTunnel_CancelForwarding{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 2, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSSHReverseTunnel.CloseChannel(ChannelId: String);
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
  tmp_ChannelId: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  tmp_ChannelId := {$ifndef UNICODE_ON_ANSI}ChannelId{$else}ToLinuxAnsiString(ChannelId){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_ChannelId){$else}@tmp_ChannelId[0]{$endif};

  if @_IPWorksSSH_SSHReverseTunnel_Do = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_Do(m_ctl, MID_SSHReverseTunnel_CloseChannel{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


function TiphSSHReverseTunnel.Config(ConfigurationString: String): String;
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
  tmp_ConfigurationString: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := '';

  tmp_ConfigurationString := {$ifndef UNICODE_ON_ANSI}ConfigurationString{$else}ToLinuxAnsiString(ConfigurationString){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_ConfigurationString){$else}@tmp_ConfigurationString[0]{$endif};

  if @_IPWorksSSH_SSHReverseTunnel_Do = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_Do(m_ctl, MID_SSHReverseTunnel_Config{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  result := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(param[1]);
end;


function TiphSSHReverseTunnel.DecodePacket(EncodedPacket: String): TBytes;
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
  tmp_EncodedPacket: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := nil;

  tmp_EncodedPacket := {$ifndef UNICODE_ON_ANSI}EncodedPacket{$else}ToLinuxAnsiString(EncodedPacket){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_EncodedPacket){$else}@tmp_EncodedPacket[0]{$endif};

  if @_IPWorksSSH_SSHReverseTunnel_Do = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_Do(m_ctl, MID_SSHReverseTunnel_DecodePacket{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  SetLength(result, paramcb[1]);
  Move(Pointer(param[1])^, Pointer(result)^, paramcb[1]); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSSHReverseTunnel.DoEvents();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SSHReverseTunnel_Do = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_Do(m_ctl, MID_SSHReverseTunnel_DoEvents{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


function TiphSSHReverseTunnel.EncodePacket(Packet: TBytes): String;
var
  param: array[0..1] of Pointer;
  paramcb: array[0..1] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 1 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := '';

  param[0] := Pointer(Packet);
  paramcb[0] := Length(Packet);

  if @_IPWorksSSH_SSHReverseTunnel_Do = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_Do(m_ctl, MID_SSHReverseTunnel_EncodePacket{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 1, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  result := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(param[1]);
end;


procedure TiphSSHReverseTunnel.ExchangeKeys();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SSHReverseTunnel_Do = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_Do(m_ctl, MID_SSHReverseTunnel_ExchangeKeys{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


function TiphSSHReverseTunnel.GetSSHParam(Payload: TBytes; Field: String): String;
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
  i, err: Integer;
  tmp_Field: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 2 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := '';

  param[0] := Pointer(Payload);
  paramcb[0] := Length(Payload);

  tmp_Field := {$ifndef UNICODE_ON_ANSI}Field{$else}ToLinuxAnsiString(Field){$endif};
  param[1] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_Field){$else}@tmp_Field[0]{$endif};

  if @_IPWorksSSH_SSHReverseTunnel_Do = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_Do(m_ctl, MID_SSHReverseTunnel_GetSSHParam{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 2, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  result := {$ifdef UNICODE_ON_ANSI}FromLinuxAnsiString{$else}PChar{$endif}(param[2]);
end;


function TiphSSHReverseTunnel.GetSSHParamBytes(Payload: TBytes; Field: String): TBytes;
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
  i, err: Integer;
  tmp_Field: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 2 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := nil;

  param[0] := Pointer(Payload);
  paramcb[0] := Length(Payload);

  tmp_Field := {$ifndef UNICODE_ON_ANSI}Field{$else}ToLinuxAnsiString(Field){$endif};
  param[1] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_Field){$else}@tmp_Field[0]{$endif};

  if @_IPWorksSSH_SSHReverseTunnel_Do = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_Do(m_ctl, MID_SSHReverseTunnel_GetSSHParamBytes{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 2, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  SetLength(result, paramcb[2]);
  Move(Pointer(param[2])^, Pointer(result)^, paramcb[2]); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSSHReverseTunnel.RequestForwarding(Address: String; Port: Integer; ForwardingHost: String; ForwardingPort: Integer);
var
  param: array[0..4] of Pointer;
  paramcb: array[0..4] of Integer;
  i, err: Integer;
  tmp_Address: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
  tmp_ForwardingHost: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 4 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  tmp_Address := {$ifndef UNICODE_ON_ANSI}Address{$else}ToLinuxAnsiString(Address){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_Address){$else}@tmp_Address[0]{$endif};

  param[1] := Pointer(Port);

  tmp_ForwardingHost := {$ifndef UNICODE_ON_ANSI}ForwardingHost{$else}ToLinuxAnsiString(ForwardingHost){$endif};
  param[2] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_ForwardingHost){$else}@tmp_ForwardingHost[0]{$endif};

  param[3] := Pointer(ForwardingPort);

  if @_IPWorksSSH_SSHReverseTunnel_Do = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_Do(m_ctl, MID_SSHReverseTunnel_RequestForwarding{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 4, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSSHReverseTunnel.Reset();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SSHReverseTunnel_Do = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_Do(m_ctl, MID_SSHReverseTunnel_Reset{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSSHReverseTunnel.SendChannelData(ChannelId: String; Data: TBytes);
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
  i, err: Integer;
  tmp_ChannelId: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 2 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  tmp_ChannelId := {$ifndef UNICODE_ON_ANSI}ChannelId{$else}ToLinuxAnsiString(ChannelId){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_ChannelId){$else}@tmp_ChannelId[0]{$endif};

  param[1] := Pointer(Data);
  paramcb[1] := Length(Data);

  if @_IPWorksSSH_SSHReverseTunnel_Do = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_Do(m_ctl, MID_SSHReverseTunnel_SendChannelData{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 2, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


function TiphSSHReverseTunnel.SetSSHParam(Payload: TBytes; FieldType: String; FieldValue: String): TBytes;
var
  param: array[0..3] of Pointer;
  paramcb: array[0..3] of Integer;
  i, err: Integer;
  tmp_FieldType: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
  tmp_FieldValue: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 3 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
   
  result := nil;

  param[0] := Pointer(Payload);
  paramcb[0] := Length(Payload);

  tmp_FieldType := {$ifndef UNICODE_ON_ANSI}FieldType{$else}ToLinuxAnsiString(FieldType){$endif};
  param[1] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_FieldType){$else}@tmp_FieldType[0]{$endif};

  tmp_FieldValue := {$ifndef UNICODE_ON_ANSI}FieldValue{$else}ToLinuxAnsiString(FieldValue){$endif};
  param[2] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_FieldValue){$else}@tmp_FieldValue[0]{$endif};

  if @_IPWorksSSH_SSHReverseTunnel_Do = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_Do(m_ctl, MID_SSHReverseTunnel_SetSSHParam{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 3, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
  SetLength(result, paramcb[3]);
  Move(Pointer(param[3])^, Pointer(result)^, paramcb[3]); // IMPORTANT: Do NOT multiply the length value!
end;


procedure TiphSSHReverseTunnel.SSHLogoff();
var
  param: array[0..0] of Pointer;
  paramcb: array[0..0] of Integer;
  i, err: Integer;
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 0 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  if @_IPWorksSSH_SSHReverseTunnel_Do = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_Do(m_ctl, MID_SSHReverseTunnel_SSHLogoff{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 0, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


procedure TiphSSHReverseTunnel.SSHLogon(SSHHost: String; SSHPort: Integer);
var
  param: array[0..2] of Pointer;
  paramcb: array[0..2] of Integer;
  i, err: Integer;
  tmp_SSHHost: {$ifndef UNICODE_ON_ANSI}string{$else}LXAnsiString{$endif};
begin
{$IFDEF WIN64}
  m_ctl := m_ctl; //Without this, the method can't be invoked correctly in 64bit. (Delphi XE Version 16.0.4115.38113)
{$ENDIF}
  for i := 0 to 2 do 
  begin
    param[i] := nil;
    paramcb[i] := 0;
  end;
  

  tmp_SSHHost := {$ifndef UNICODE_ON_ANSI}SSHHost{$else}ToLinuxAnsiString(SSHHost){$endif};
  param[0] := {$ifndef UNICODE_ON_ANSI}PChar(tmp_SSHHost){$else}@tmp_SSHHost[0]{$endif};

  param[1] := Pointer(SSHPort);


  if @_IPWorksSSH_SSHReverseTunnel_Do = nil then exit;
  err := _IPWorksSSH_SSHReverseTunnel_Do(m_ctl, MID_SSHReverseTunnel_SSHLogon{$IFDEF UNICODE_ON_UNICODE}+10000{$ENDIF}, 2, @param, @paramcb, nil);
  if err <> 0 then TreatErr(err, '');
  
end;


{$ENDIF}


{$IFDEF USE_DYNAMIC_LOADING}

procedure ClearProcs();
begin
  _IPWorksSSH_EvtStr := nil;
  _IPWorksSSH_Stream := nil;
  _IPWorksSSH_SSHReverseTunnel_Create := nil;
  _IPWorksSSH_SSHReverseTunnel_Destroy := nil;
  _IPWorksSSH_SSHReverseTunnel_Set := nil;
  _IPWorksSSH_SSHReverseTunnel_Get := nil;
  _IPWorksSSH_SSHReverseTunnel_GetLastError := nil;
  _IPWorksSSH_SSHReverseTunnel_GetLastErrorCode := nil;
  _IPWorksSSH_SSHReverseTunnel_SetLastErrorAndCode := nil;
  _IPWorksSSH_SSHReverseTunnel_GetEventError := nil;
  _IPWorksSSH_SSHReverseTunnel_GetEventErrorCode := nil;
  _IPWorksSSH_SSHReverseTunnel_SetEventErrorAndCode := nil;
  {$IFDEF USE_EXPLICIT_INIT}
  _IPWorksSSH_SSHReverseTunnel_StaticInit := nil;
  _IPWorksSSH_SSHReverseTunnel_StaticDestroy := nil;
  {$ENDIF}
  _IPWorksSSH_SSHReverseTunnel_CheckIndex := nil;
  _IPWorksSSH_SSHReverseTunnel_Do := nil;
end;

{$IFDEF USEIPWORKSSSHDLL}
procedure LoadDLL();
begin
  ClearProcs();

  hLib := LoadLibrary(PChar(DLLNAME));
  if hLib = 0 then exit;

  @_IPWorksSSH_EvtStr                                := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_EvtStr');
  @_IPWorksSSH_Stream                                := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_Stream');
  @_IPWorksSSH_SSHReverseTunnel_Create               := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHReverseTunnel_Create');
  @_IPWorksSSH_SSHReverseTunnel_Destroy              := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHReverseTunnel_Destroy');
  @_IPWorksSSH_SSHReverseTunnel_Set                  := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHReverseTunnel_Set');
  @_IPWorksSSH_SSHReverseTunnel_Get                  := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHReverseTunnel_Get');
  @_IPWorksSSH_SSHReverseTunnel_GetLastError         := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHReverseTunnel_GetLastError');
  @_IPWorksSSH_SSHReverseTunnel_GetLastErrorCode     := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHReverseTunnel_GetLastErrorCode');
  @_IPWorksSSH_SSHReverseTunnel_SetLastErrorAndCode  := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHReverseTunnel_SetLastErrorAndCode');
  @_IPWorksSSH_SSHReverseTunnel_GetEventError        := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHReverseTunnel_GetEventError');
  @_IPWorksSSH_SSHReverseTunnel_GetEventErrorCode    := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHReverseTunnel_GetEventErrorCode');
  @_IPWorksSSH_SSHReverseTunnel_SetEventErrorAndCode := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHReverseTunnel_SetEventErrorAndCode');
  {$IFDEF USE_EXPLICIT_INIT}
  @_IPWorksSSH_SSHReverseTunnel_StaticInit           := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHReverseTunnel_StaticInit');
  @_IPWorksSSH_SSHReverseTunnel_StaticDestroy        := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHReverseTunnel_StaticDestroy');
  {$ENDIF}
  @_IPWorksSSH_SSHReverseTunnel_CheckIndex           := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHReverseTunnel_CheckIndex');
  @_IPWorksSSH_SSHReverseTunnel_Do                   := {$ifndef FPC}GetProcAddress{$else}GetProcedureAddress{$endif}(hLib, 'IPWorksSSH_SSHReverseTunnel_Do');
end;
{$ELSE}
procedure LoadRESDLL();
var
  hResInfo: HRSRC;
  hResData: HGLOBAL;
  pResData: Pointer;
begin
  ClearProcs();

  hResInfo := FindResource(HInstance, 'ipworksssh22_dta', RT_RCDATA);
  if hResInfo = 0 then exit;

  hResData := LoadResource(HInstance, hResInfo);
  if hResData = 0 then exit;

  pResData := LockResource(hResData);
  if pResData = nil then exit;

  pBaseAddress := IPWorksSSHLoadDRU(pResData, pEntryPoint);
  if Assigned(pBaseAddress) then begin
    @_IPWorksSSH_EvtStr                                := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_EvtStr');
    @_IPWorksSSH_Stream                                := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_Stream');
    @_IPWorksSSH_SSHReverseTunnel_Create               := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHReverseTunnel_Create');
    @_IPWorksSSH_SSHReverseTunnel_Destroy              := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHReverseTunnel_Destroy');
    @_IPWorksSSH_SSHReverseTunnel_Set                  := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHReverseTunnel_Set');
    @_IPWorksSSH_SSHReverseTunnel_Get                  := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHReverseTunnel_Get');
    @_IPWorksSSH_SSHReverseTunnel_GetLastError         := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHReverseTunnel_GetLastError');
    @_IPWorksSSH_SSHReverseTunnel_GetLastErrorCode     := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHReverseTunnel_GetLastErrorCode');
    @_IPWorksSSH_SSHReverseTunnel_SetLastErrorAndCode  := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHReverseTunnel_SetLastErrorAndCode');
    @_IPWorksSSH_SSHReverseTunnel_GetEventError        := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHReverseTunnel_GetEventError');
    @_IPWorksSSH_SSHReverseTunnel_GetEventErrorCode    := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHReverseTunnel_GetEventErrorCode');
    @_IPWorksSSH_SSHReverseTunnel_SetEventErrorAndCode := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHReverseTunnel_SetEventErrorAndCode');
    @_IPWorksSSH_SSHReverseTunnel_CheckIndex           := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHReverseTunnel_CheckIndex');
    @_IPWorksSSH_SSHReverseTunnel_Do                   := IPWorksSSHFindFunc(pBaseAddress, 'IPWorksSSH_SSHReverseTunnel_Do');
  end;

  FreeResource(hResData);
end;
{$ENDIF USEIPWORKSSSHDLL}

initialization
begin
  iLoadCount := iLoadCount + 1;
  if iLoadCount > 1 then exit;

  {$IFDEF USEIPWORKSSSHDLL}
  hLib := 0;
  LoadDLL();
  {$ELSE}
  pBaseAddress := nil;
  pEntryPoint := nil;
  LoadResDLL();
  {$ENDIF}

  {$IFDEF USE_EXPLICIT_INIT}
  if @_IPWorksSSH_SSHReverseTunnel_StaticInit <> nil then
    _IPWorksSSH_SSHReverseTunnel_StaticInit(nil);
  {$ENDIF}
end;

finalization
begin
  iLoadCount := iLoadCount - 1;
  if iLoadCount > 0 then exit;

  {$IFDEF USE_EXPLICIT_INIT}
  if @_IPWorksSSH_SSHReverseTunnel_StaticDestroy <> nil then
    _IPWorksSSH_SSHReverseTunnel_StaticDestroy();
  {$ENDIF}

  ClearProcs();

  {$IFDEF USEIPWORKSSSHDLL}
  if hLib <> 0 then
  begin
    {$ifndef FPC}FreeLibrary{$else}UnloadLibrary{$endif}(hLib);
    hLib := 0;
  end;
  {$ELSE}
  IPWorksSSHFreeDRU(pBaseAddress, pEntryPoint);
  pBaseAddress := nil;
  pEntryPoint := nil;
  {$ENDIF}
  iLoadCount := 0;
end;

{$ELSE USE_DYNAMIC_LOADING}

// in case if static library used

{$IFDEF USE_EXPLICIT_INIT}

initialization
  _IPWorksSSH_SSHReverseTunnel_StaticInit(nil);

finalization
  _IPWorksSSH_SSHReverseTunnel_StaticDestroy();

{$ENDIF USE_EXPLICIT_INIT}

{$ENDIF USE_DYNAMIC_LOADING}


end.
