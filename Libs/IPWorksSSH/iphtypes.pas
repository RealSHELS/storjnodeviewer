
unit iphtypes;

{$IFDEF fpc}  //Lazarus
  {$IFDEF FPC_UNICODESTRINGS}
      // ensure that mode Delphi Unicode is enabled, not a modeswitch unicodestrings
    {$MODE DELPHIUNICODE}
  {$ELSE}
    {$MODE DELPHI}
  {$ENDIF}
{$ENDIF}

{$IFDEF VER130}
  {$DEFINE DELPHI_5}
{$ENDIF}

interface

uses
  iphcore,
{$IFDEF fpc}  //Lazarus
  SysUtils, Classes;
{$ELSE}
  {$IF CompilerVersion >= 23}System.SysUtils{$ELSEIF True}SysUtils{$IFEND}, {$IF CompilerVersion >= 23}System.Classes{$ELSEIF True}Classes{$IFEND};
{$ENDIF}

// Collections can exist in one of two modes: freestanding and bound
// A freestanding collection is not bound to any parent control. All objects and associated content are kept internally.
// A bound collection is bound to a parent control. Any calls are redirected to the parent control.

type
  TiphTypesCore = class(TiphCore)
  end;

  TiphCertStoreTypes = (

                   cstUser,

                   cstMachine,

                   cstPFXFile,

                   cstPFXBlob,

                   cstJKSFile,

                   cstJKSBlob,

                   cstPEMKeyFile,

                   cstPEMKeyBlob,

                   cstPublicKeyFile,

                   cstPublicKeyBlob,

                   cstSSHPublicKeyBlob,

                   cstP7BFile,

                   cstP7BBlob,

                   cstSSHPublicKeyFile,

                   cstPPKFile,

                   cstPPKBlob,

                   cstXMLFile,

                   cstXMLBlob,

                   cstJWKFile,

                   cstJWKBlob,

                   cstSecurityKey,

                   cstBCFKSFile,

                   cstBCFKSBlob,

                   cstPKCS11,

                   cstAuto{$IFNDEF DELPHI_5}=Ord(99){$ENDIF}
);

  TiphFirewallTypes = (

                   fwNone,

                   fwTunnel,

                   fwSOCKS4,

                   fwSOCKS5,

                   fwSOCKS4A{$IFNDEF DELPHI_5}=Ord(10){$ENDIF}
);

  TiphSSHAuthModes = (

                   amNone,

                   amMultiFactor,

                   amPassword,

                   amPublicKey,

                   amKeyboardInteractive,

                   amGSSAPIWithMic,

                   amCustom
);

  TiphSFTPFileTypes = (
{$IFDEF DELPHI_5}sftZero,{$ENDIF}
                   sftRegular{$IFNDEF DELPHI_5}=Ord(1){$ENDIF},

                   sftDirectory{$IFNDEF DELPHI_5}=Ord(2){$ENDIF},

                   sftSymLink{$IFNDEF DELPHI_5}=Ord(3){$ENDIF},

                   sftSpecial{$IFNDEF DELPHI_5}=Ord(4){$ENDIF},

                   sftUnknown{$IFNDEF DELPHI_5}=Ord(5){$ENDIF},

                   sftSocket{$IFNDEF DELPHI_5}=Ord(6){$ENDIF},

                   sftCharDevice{$IFNDEF DELPHI_5}=Ord(7){$ENDIF},

                   sftBlockDevice{$IFNDEF DELPHI_5}=Ord(8){$ENDIF},

                   sftFIFO{$IFNDEF DELPHI_5}=Ord(9){$ENDIF}
);


//////////////////////////////////////////////////////////////////////////////////////////

  TiphCertExtension = class(TPersistent)
  protected
    FOwnerCtl: TObject;
    FReadOnlyProp: boolean;
    FIndex: integer;
    FCritical: Boolean;

    FOID: String;

    FValue: TBytes;


    constructor Create(Owner: TObject; ReadOnly: boolean); overload;
    function GetPropCritical: Boolean; virtual;
    procedure SetPropCritical(Value: Boolean); virtual;
    function GetPropOID: String; virtual;
    procedure SetPropOID(Value: String); virtual;
    function GetPropValue: String; virtual;
    procedure SetPropValue(Value: String); virtual;
    function  GetPropValueB : TBytes; virtual;
    procedure SetPropValueB(Value: TBytes); virtual;


  public
    constructor Create(valoid: String; valvalue: TBytes; valcritical: Boolean); overload; virtual;

    destructor Destroy; override;

    procedure Assign(Source: TPersistent); override;
    function Clone: TiphCertExtension; virtual;

    property Critical: Boolean read GetPropCritical;
    property OID: String read GetPropOID;
    property Value: String read GetPropValue;
    property ValueB : TBytes read GetPropValueB;

    property Index: integer read FIndex write FIndex;
  end;

  TiphCertExtensionList = class(TPersistent)
  protected
    FItems: TList;
    FReadOnlyProp: Boolean;
    FOwnerCtl: TObject;
    FCount: integer;
  protected
    function GetCount: Integer;
    function GetItem(Idx: integer): TiphCertExtension;
    procedure SetItem(Idx: integer; Value: TiphCertExtension);
    function GetIsReadOnly: boolean;
    function GetIsFixedSize: boolean;
    function GetSyncRoot: TObject;
    function GetIsSynchronized: boolean;
    procedure ClearList;

    function IsFree: boolean;
    procedure SyncCount;
    procedure InternalSetCount(Value: integer);
    procedure InternalSetItem(Index: integer; Value: TiphCertExtension);

    function CtlGetCount: integer; virtual;
    procedure CtlSetCount(Value: integer); virtual;
    function CreateElemInstance(Index: integer): TiphCertExtension; virtual;

  public
    constructor Create(); overload;
    constructor Create(ReadOnly: Boolean); overload;
    constructor Create(Owner: TObject; ReadOnly: Boolean); overload;
    destructor Destroy; override;

    procedure Assign(Source: TPersistent); override;
    function Clone: TiphCertExtensionList;

    function Add(Value: TiphCertExtension): Integer; overload;
    function Add(): Integer; overload;
    procedure Remove(value: TiphCertExtension);

    function Contains(value: TiphCertExtension): Boolean;
    
    function IndexOf(value: TiphCertExtension): Integer;
    procedure Insert(Idx: Integer; value: TiphCertExtension);
    procedure RemoveAt(idx: Integer);
    
    
    procedure Clear();

    property Count: Integer read GetCount;
    property Item[Index: Integer]: TiphCertExtension read GetItem write SetItem; default;
    property IsReadOnly: boolean read GetIsReadOnly;
    property IsFixedSize: boolean read GetIsFixedSize;
    property SyncRoot: TObject read GetSyncRoot;
    property IsSynchronized: boolean read GetIsSynchronized;
  end;

  TiphConnection = class(TPersistent)
  protected
    FOwnerCtl: TObject;
    FReadOnlyProp: boolean;
    FIndex: integer;
    FAcceptData: Boolean;

    FBytesSent: Integer;

    FConnected: Boolean;

    FConnectionId: String;

    FDataToSend: TBytes;

    FEOL: TBytes;

    FIdleTimeout: Integer;

    FLocalAddress: String;

    FMaxLineLength: Integer;

    FReadyToSend: Boolean;

    FRecordLength: Integer;

    FRemoteHost: String;

    FRemotePort: Integer;

    FSingleLineMode: Boolean;

    FTimeout: Integer;

    FUserData: TBytes;


    constructor Create(Owner: TObject; ReadOnly: boolean); overload;
    function GetPropAcceptData: Boolean; virtual;
    procedure SetPropAcceptData(Value: Boolean); virtual;
    function GetPropBytesSent: Integer; virtual;
    procedure SetPropBytesSent(Value: Integer); virtual;
    function GetPropConnected: Boolean; virtual;
    procedure SetPropConnected(Value: Boolean); virtual;
    function GetPropConnectionId: String; virtual;
    procedure SetPropConnectionId(Value: String); virtual;
    function GetPropDataToSend: String; virtual;
    procedure SetPropDataToSend(Value: String); virtual;
    function  GetPropDataToSendB : TBytes; virtual;
    procedure SetPropDataToSendB(Value: TBytes); virtual;

    function GetPropEOL: String; virtual;
    procedure SetPropEOL(Value: String); virtual;
    function  GetPropEOLB : TBytes; virtual;
    procedure SetPropEOLB(Value: TBytes); virtual;

    function GetPropIdleTimeout: Integer; virtual;
    procedure SetPropIdleTimeout(Value: Integer); virtual;
    function GetPropLocalAddress: String; virtual;
    procedure SetPropLocalAddress(Value: String); virtual;
    function GetPropMaxLineLength: Integer; virtual;
    procedure SetPropMaxLineLength(Value: Integer); virtual;
    function GetPropReadyToSend: Boolean; virtual;
    procedure SetPropReadyToSend(Value: Boolean); virtual;
    function GetPropRecordLength: Integer; virtual;
    procedure SetPropRecordLength(Value: Integer); virtual;
    function GetPropRemoteHost: String; virtual;
    procedure SetPropRemoteHost(Value: String); virtual;
    function GetPropRemotePort: Integer; virtual;
    procedure SetPropRemotePort(Value: Integer); virtual;
    function GetPropSingleLineMode: Boolean; virtual;
    procedure SetPropSingleLineMode(Value: Boolean); virtual;
    function GetPropTimeout: Integer; virtual;
    procedure SetPropTimeout(Value: Integer); virtual;
    function GetPropUserData: String; virtual;
    procedure SetPropUserData(Value: String); virtual;
    function  GetPropUserDataB : TBytes; virtual;
    procedure SetPropUserDataB(Value: TBytes); virtual;


  public
    destructor Destroy; override;

    procedure Assign(Source: TPersistent); override;
    function Clone: TiphConnection; virtual;

    property AcceptData: Boolean read GetPropAcceptData write SetPropAcceptData;
    property BytesSent: Integer read GetPropBytesSent;
    property Connected: Boolean read GetPropConnected write SetPropConnected;
    property ConnectionId: String read GetPropConnectionId;
    property DataToSend: String  write SetPropDataToSend;
    property DataToSendB : TBytes  write SetPropDataToSendB;

    property EOL: String read GetPropEOL write SetPropEOL;
    property EOLB : TBytes read GetPropEOLB write SetPropEOLB;

    property IdleTimeout: Integer read GetPropIdleTimeout write SetPropIdleTimeout;
    property LocalAddress: String read GetPropLocalAddress;
    property MaxLineLength: Integer read GetPropMaxLineLength write SetPropMaxLineLength;
    property ReadyToSend: Boolean read GetPropReadyToSend;
    property RecordLength: Integer read GetPropRecordLength write SetPropRecordLength;
    property RemoteHost: String read GetPropRemoteHost;
    property RemotePort: Integer read GetPropRemotePort;
    property SingleLineMode: Boolean read GetPropSingleLineMode write SetPropSingleLineMode;
    property Timeout: Integer read GetPropTimeout write SetPropTimeout;
    property UserData: String read GetPropUserData write SetPropUserData;
    property UserDataB : TBytes read GetPropUserDataB write SetPropUserDataB;

    property Index: integer read FIndex write FIndex;
  end;

  TiphConnectionList = class(TPersistent)
  protected
    FItems: TList;
    FReadOnlyProp: Boolean;
    FOwnerCtl: TObject;
    FCount: integer;
  protected
    function GetCount: Integer;
    function GetItem(Idx: integer): TiphConnection;
    procedure SetItem(Idx: integer; Value: TiphConnection);
    function GetIsReadOnly: boolean;
    function GetIsFixedSize: boolean;
    function GetSyncRoot: TObject;
    function GetIsSynchronized: boolean;
    procedure ClearList;

    function IsFree: boolean;
    procedure SyncCount;
    procedure InternalSetCount(Value: integer);
    procedure InternalSetItem(Index: integer; Value: TiphConnection);

    function CtlGetCount: integer; virtual;
    procedure CtlSetCount(Value: integer); virtual;
    function CreateElemInstance(Index: integer): TiphConnection; virtual;

    procedure OptionallyAdjustItemList(MaxCount : integer);

  public
    constructor Create(); overload;
    constructor Create(ReadOnly: Boolean); overload;
    constructor Create(Owner: TObject; ReadOnly: Boolean); overload;
    destructor Destroy; override;

    procedure Assign(Source: TPersistent); override;
    function Clone: TiphConnectionList;

    function Add(Value: TiphConnection): Integer; overload;
    function Add(): Integer; overload;
    procedure Remove(value: TiphConnection);

    function Contains(value: TiphConnection): Boolean;
    
    procedure Add(Index : integer); overload;
    procedure Add(Index : integer; value: TiphConnection); overload;
    
    
    procedure Clear();

    property Count: Integer read GetCount;
    property Item[Index: Integer]: TiphConnection read GetItem write SetItem; default;
    property IsReadOnly: boolean read GetIsReadOnly;
    property IsFixedSize: boolean read GetIsFixedSize;
    property SyncRoot: TObject read GetSyncRoot;
    property IsSynchronized: boolean read GetIsSynchronized;
  end;

  TiphDirEntry = class(TPersistent)
  protected
    FOwnerCtl: TObject;
    FReadOnlyProp: boolean;
    FIndex: integer;
    FEntry: String;

    FFileName: String;

    FFileSize: Int64;

    FFileTime: String;

    FIsDir: Boolean;

    FIsSymlink: Boolean;


    constructor Create(Owner: TObject; ReadOnly: boolean); overload;
    function GetPropEntry: String; virtual;
    procedure SetPropEntry(Value: String); virtual;
    function GetPropFileName: String; virtual;
    procedure SetPropFileName(Value: String); virtual;
    function GetPropFileSize: Int64; virtual;
    procedure SetPropFileSize(Value: Int64); virtual;
    function GetPropFileTime: String; virtual;
    procedure SetPropFileTime(Value: String); virtual;
    function GetPropIsDir: Boolean; virtual;
    procedure SetPropIsDir(Value: Boolean); virtual;
    function GetPropIsSymlink: Boolean; virtual;
    procedure SetPropIsSymlink(Value: Boolean); virtual;

  public
    constructor Create(); overload; virtual;

    destructor Destroy; override;

    procedure Assign(Source: TPersistent); override;
    function Clone: TiphDirEntry; virtual;

    property Entry: String read GetPropEntry;
    property FileName: String read GetPropFileName;
    property FileSize: Int64 read GetPropFileSize;
    property FileTime: String read GetPropFileTime;
    property IsDir: Boolean read GetPropIsDir;
    property IsSymlink: Boolean read GetPropIsSymlink;
    property Index: integer read FIndex write FIndex;
  end;

  TiphDirEntryList = class(TPersistent)
  protected
    FItems: TList;
    FReadOnlyProp: Boolean;
    FOwnerCtl: TObject;
    FCount: integer;
  protected
    function GetCount: Integer;
    function GetItem(Idx: integer): TiphDirEntry;
    procedure SetItem(Idx: integer; Value: TiphDirEntry);
    function GetIsReadOnly: boolean;
    function GetIsFixedSize: boolean;
    function GetSyncRoot: TObject;
    function GetIsSynchronized: boolean;
    procedure ClearList;

    function IsFree: boolean;
    procedure SyncCount;
    procedure InternalSetCount(Value: integer);
    procedure InternalSetItem(Index: integer; Value: TiphDirEntry);

    function CtlGetCount: integer; virtual;
    procedure CtlSetCount(Value: integer); virtual;
    function CreateElemInstance(Index: integer): TiphDirEntry; virtual;

  public
    constructor Create(); overload;
    constructor Create(ReadOnly: Boolean); overload;
    constructor Create(Owner: TObject; ReadOnly: Boolean); overload;
    destructor Destroy; override;

    procedure Assign(Source: TPersistent); override;
    function Clone: TiphDirEntryList;

    function Add(Value: TiphDirEntry): Integer; overload;
    function Add(): Integer; overload;
    procedure Remove(value: TiphDirEntry);

    function Contains(value: TiphDirEntry): Boolean;
    
    function IndexOf(value: TiphDirEntry): Integer;
    procedure Insert(Idx: Integer; value: TiphDirEntry);
    procedure RemoveAt(idx: Integer);
    
    
    procedure Clear();

    property Count: Integer read GetCount;
    property Item[Index: Integer]: TiphDirEntry read GetItem write SetItem; default;
    property IsReadOnly: boolean read GetIsReadOnly;
    property IsFixedSize: boolean read GetIsFixedSize;
    property SyncRoot: TObject read GetSyncRoot;
    property IsSynchronized: boolean read GetIsSynchronized;
  end;

  TiphFirewall = class(TPersistent)
  protected
    FOwnerCtl: TObject;
    FReadOnlyProp: boolean;
    FIndex: integer;
    FAutoDetect: Boolean;

    FFirewallType: TiphFirewallTypes;

    FHost: String;

    FPassword: String;

    FPort: Integer;

    FUser: String;


    constructor Create(Owner: TObject; ReadOnly: boolean); overload;
    function GetPropAutoDetect: Boolean; virtual;
    procedure SetPropAutoDetect(Value: Boolean); virtual;
    function GetPropFirewallType: TiphFirewallTypes; virtual;
    procedure SetPropFirewallType(Value: TiphFirewallTypes); virtual;
    function GetPropHost: String; virtual;
    procedure SetPropHost(Value: String); virtual;
    function GetPropPassword: String; virtual;
    procedure SetPropPassword(Value: String); virtual;
    function GetPropPort: Integer; virtual;
    procedure SetPropPort(Value: Integer); virtual;
    function GetPropUser: String; virtual;
    procedure SetPropUser(Value: String); virtual;

  public
    constructor Create(); overload; virtual;

    destructor Destroy; override;

    procedure Assign(Source: TPersistent); override;
    function Clone: TiphFirewall; virtual;

    property AutoDetect: Boolean read GetPropAutoDetect write SetPropAutoDetect;
    property FirewallType: TiphFirewallTypes read GetPropFirewallType write SetPropFirewallType;
    property Host: String read GetPropHost write SetPropHost;
    property Password: String read GetPropPassword write SetPropPassword;
    property Port: Integer read GetPropPort write SetPropPort;
    property User: String read GetPropUser write SetPropUser;
    property Index: integer read FIndex write FIndex;
  end;

  TiphFirewallList = class(TPersistent)
  protected
    FItems: TList;
    FReadOnlyProp: Boolean;
    FOwnerCtl: TObject;
    FCount: integer;
  protected
    function GetCount: Integer;
    function GetItem(Idx: integer): TiphFirewall;
    procedure SetItem(Idx: integer; Value: TiphFirewall);
    function GetIsReadOnly: boolean;
    function GetIsFixedSize: boolean;
    function GetSyncRoot: TObject;
    function GetIsSynchronized: boolean;
    procedure ClearList;

    function IsFree: boolean;
    procedure SyncCount;
    procedure InternalSetCount(Value: integer);
    procedure InternalSetItem(Index: integer; Value: TiphFirewall);

    function CtlGetCount: integer; virtual;
    procedure CtlSetCount(Value: integer); virtual;
    function CreateElemInstance(Index: integer): TiphFirewall; virtual;

  public
    constructor Create(); overload;
    constructor Create(ReadOnly: Boolean); overload;
    constructor Create(Owner: TObject; ReadOnly: Boolean); overload;
    destructor Destroy; override;

    procedure Assign(Source: TPersistent); override;
    function Clone: TiphFirewallList;

    function Add(Value: TiphFirewall): Integer; overload;
    function Add(): Integer; overload;
    procedure Remove(value: TiphFirewall);

    function Contains(value: TiphFirewall): Boolean;
    
    function IndexOf(value: TiphFirewall): Integer;
    procedure Insert(Idx: Integer; value: TiphFirewall);
    procedure RemoveAt(idx: Integer);
    
    
    procedure Clear();

    property Count: Integer read GetCount;
    property Item[Index: Integer]: TiphFirewall read GetItem write SetItem; default;
    property IsReadOnly: boolean read GetIsReadOnly;
    property IsFixedSize: boolean read GetIsFixedSize;
    property SyncRoot: TObject read GetSyncRoot;
    property IsSynchronized: boolean read GetIsSynchronized;
  end;

  TiphSFTPConnection = class(TPersistent)
  protected
    FOwnerCtl: TObject;
    FReadOnlyProp: boolean;
    FIndex: integer;
    FConnected: Boolean;

    FConnectionId: String;

    FErrorMessage: String;

    FFileData: TBytes;

    FLocalAddress: String;

    FProtocolVersion: Integer;

    FRemoteHost: String;

    FRemotePort: Integer;

    FTimeout: Integer;


    constructor Create(Owner: TObject; ReadOnly: boolean); overload;
    function GetPropConnected: Boolean; virtual;
    procedure SetPropConnected(Value: Boolean); virtual;
    function GetPropConnectionId: String; virtual;
    procedure SetPropConnectionId(Value: String); virtual;
    function GetPropErrorMessage: String; virtual;
    procedure SetPropErrorMessage(Value: String); virtual;
    function GetPropFileData: String; virtual;
    procedure SetPropFileData(Value: String); virtual;
    function  GetPropFileDataB : TBytes; virtual;
    procedure SetPropFileDataB(Value: TBytes); virtual;

    function GetPropLocalAddress: String; virtual;
    procedure SetPropLocalAddress(Value: String); virtual;
    function GetPropProtocolVersion: Integer; virtual;
    procedure SetPropProtocolVersion(Value: Integer); virtual;
    function GetPropRemoteHost: String; virtual;
    procedure SetPropRemoteHost(Value: String); virtual;
    function GetPropRemotePort: Integer; virtual;
    procedure SetPropRemotePort(Value: Integer); virtual;
    function GetPropTimeout: Integer; virtual;
    procedure SetPropTimeout(Value: Integer); virtual;

  public
    destructor Destroy; override;

    procedure Assign(Source: TPersistent); override;
    function Clone: TiphSFTPConnection; virtual;

    property Connected: Boolean read GetPropConnected write SetPropConnected;
    property ConnectionId: String read GetPropConnectionId;
    property ErrorMessage: String read GetPropErrorMessage write SetPropErrorMessage;
    property FileData: String read GetPropFileData write SetPropFileData;
    property FileDataB : TBytes read GetPropFileDataB write SetPropFileDataB;

    property LocalAddress: String read GetPropLocalAddress;
    property ProtocolVersion: Integer read GetPropProtocolVersion;
    property RemoteHost: String read GetPropRemoteHost;
    property RemotePort: Integer read GetPropRemotePort;
    property Timeout: Integer read GetPropTimeout write SetPropTimeout;
    property Index: integer read FIndex write FIndex;
  end;

  TiphSFTPConnectionList = class(TPersistent)
  protected
    FItems: TList;
    FReadOnlyProp: Boolean;
    FOwnerCtl: TObject;
    FCount: integer;
  protected
    function GetCount: Integer;
    function GetItem(Idx: integer): TiphSFTPConnection;
    procedure SetItem(Idx: integer; Value: TiphSFTPConnection);
    function GetIsReadOnly: boolean;
    function GetIsFixedSize: boolean;
    function GetSyncRoot: TObject;
    function GetIsSynchronized: boolean;
    procedure ClearList;

    function IsFree: boolean;
    procedure SyncCount;
    procedure InternalSetCount(Value: integer);
    procedure InternalSetItem(Index: integer; Value: TiphSFTPConnection);

    function CtlGetCount: integer; virtual;
    procedure CtlSetCount(Value: integer); virtual;
    function CreateElemInstance(Index: integer): TiphSFTPConnection; virtual;

    procedure OptionallyAdjustItemList(MaxCount : integer);

  public
    constructor Create(); overload;
    constructor Create(ReadOnly: Boolean); overload;
    constructor Create(Owner: TObject; ReadOnly: Boolean); overload;
    destructor Destroy; override;

    procedure Assign(Source: TPersistent); override;
    function Clone: TiphSFTPConnectionList;

    function Add(Value: TiphSFTPConnection): Integer; overload;
    function Add(): Integer; overload;
    procedure Remove(value: TiphSFTPConnection);

    function Contains(value: TiphSFTPConnection): Boolean;
    
    procedure Add(Index : integer); overload;
    procedure Add(Index : integer; value: TiphSFTPConnection); overload;
    
    
    procedure Clear();

    property Count: Integer read GetCount;
    property Item[Index: Integer]: TiphSFTPConnection read GetItem write SetItem; default;
    property IsReadOnly: boolean read GetIsReadOnly;
    property IsFixedSize: boolean read GetIsFixedSize;
    property SyncRoot: TObject read GetSyncRoot;
    property IsSynchronized: boolean read GetIsSynchronized;
  end;

  TiphSFTPFileAttributes = class(TPersistent)
  protected
    FOwnerCtl: TObject;
    FReadOnlyProp: boolean;
    FIndex: integer;
    FAccessTime: Int64;

    FAccessTimeNanos: Integer;

    FACL: String;

    FAllocationSize: Int64;

    FAttributeBits: Integer;

    FAttributeBitsValid: Integer;

    FCreationTime: Int64;

    FCreationTimeNanos: Integer;

    FFileType: TiphSFTPFileTypes;

    FFlags: Integer;

    FGroupId: String;

    FIsDir: Boolean;

    FIsSymlink: Boolean;

    FLinkCount: Integer;

    FMIMEType: String;

    FModifiedTime: Int64;

    FModifiedTimeNanos: Integer;

    FOwnerId: String;

    FPermissions: Integer;

    FPermissionsOctal: String;

    FSize: Int64;

    FTextHint: Integer;

    FUntranslatedName: String;


    constructor Create(Owner: TObject; ReadOnly: boolean); overload;
    function GetPropAccessTime: Int64; virtual;
    procedure SetPropAccessTime(Value: Int64); virtual;
    function GetPropAccessTimeNanos: Integer; virtual;
    procedure SetPropAccessTimeNanos(Value: Integer); virtual;
    function GetPropACL: String; virtual;
    procedure SetPropACL(Value: String); virtual;
    function GetPropAllocationSize: Int64; virtual;
    procedure SetPropAllocationSize(Value: Int64); virtual;
    function GetPropAttributeBits: Integer; virtual;
    procedure SetPropAttributeBits(Value: Integer); virtual;
    function GetPropAttributeBitsValid: Integer; virtual;
    procedure SetPropAttributeBitsValid(Value: Integer); virtual;
    function GetPropCreationTime: Int64; virtual;
    procedure SetPropCreationTime(Value: Int64); virtual;
    function GetPropCreationTimeNanos: Integer; virtual;
    procedure SetPropCreationTimeNanos(Value: Integer); virtual;
    function GetPropFileType: TiphSFTPFileTypes; virtual;
    procedure SetPropFileType(Value: TiphSFTPFileTypes); virtual;
    function GetPropFlags: Integer; virtual;
    procedure SetPropFlags(Value: Integer); virtual;
    function GetPropGroupId: String; virtual;
    procedure SetPropGroupId(Value: String); virtual;
    function GetPropIsDir: Boolean; virtual;
    procedure SetPropIsDir(Value: Boolean); virtual;
    function GetPropIsSymlink: Boolean; virtual;
    procedure SetPropIsSymlink(Value: Boolean); virtual;
    function GetPropLinkCount: Integer; virtual;
    procedure SetPropLinkCount(Value: Integer); virtual;
    function GetPropMIMEType: String; virtual;
    procedure SetPropMIMEType(Value: String); virtual;
    function GetPropModifiedTime: Int64; virtual;
    procedure SetPropModifiedTime(Value: Int64); virtual;
    function GetPropModifiedTimeNanos: Integer; virtual;
    procedure SetPropModifiedTimeNanos(Value: Integer); virtual;
    function GetPropOwnerId: String; virtual;
    procedure SetPropOwnerId(Value: String); virtual;
    function GetPropPermissions: Integer; virtual;
    procedure SetPropPermissions(Value: Integer); virtual;
    function GetPropPermissionsOctal: String; virtual;
    procedure SetPropPermissionsOctal(Value: String); virtual;
    function GetPropSize: Int64; virtual;
    procedure SetPropSize(Value: Int64); virtual;
    function GetPropTextHint: Integer; virtual;
    procedure SetPropTextHint(Value: Integer); virtual;
    function GetPropUntranslatedName: String; virtual;
    procedure SetPropUntranslatedName(Value: String); virtual;

  public
    destructor Destroy; override;

    procedure Assign(Source: TPersistent); override;
    function Clone: TiphSFTPFileAttributes; virtual;

    property AccessTime: Int64 read GetPropAccessTime write SetPropAccessTime;
    property AccessTimeNanos: Integer read GetPropAccessTimeNanos write SetPropAccessTimeNanos;
    property ACL: String read GetPropACL write SetPropACL;
    property AllocationSize: Int64 read GetPropAllocationSize;
    property AttributeBits: Integer read GetPropAttributeBits;
    property AttributeBitsValid: Integer read GetPropAttributeBitsValid;
    property CreationTime: Int64 read GetPropCreationTime write SetPropCreationTime;
    property CreationTimeNanos: Integer read GetPropCreationTimeNanos write SetPropCreationTimeNanos;
    property FileType: TiphSFTPFileTypes read GetPropFileType;
    property Flags: Integer read GetPropFlags write SetPropFlags;
    property GroupId: String read GetPropGroupId write SetPropGroupId;
    property IsDir: Boolean read GetPropIsDir;
    property IsSymlink: Boolean read GetPropIsSymlink;
    property LinkCount: Integer read GetPropLinkCount;
    property MIMEType: String read GetPropMIMEType write SetPropMIMEType;
    property ModifiedTime: Int64 read GetPropModifiedTime write SetPropModifiedTime;
    property ModifiedTimeNanos: Integer read GetPropModifiedTimeNanos write SetPropModifiedTimeNanos;
    property OwnerId: String read GetPropOwnerId write SetPropOwnerId;
    property Permissions: Integer read GetPropPermissions write SetPropPermissions;
    property PermissionsOctal: String read GetPropPermissionsOctal write SetPropPermissionsOctal;
    property Size: Int64 read GetPropSize;
    property TextHint: Integer read GetPropTextHint;
    property UntranslatedName: String read GetPropUntranslatedName;
    property Index: integer read FIndex write FIndex;
  end;

  TiphSFTPFileAttributesList = class(TPersistent)
  protected
    FItems: TList;
    FReadOnlyProp: Boolean;
    FOwnerCtl: TObject;
    FCount: integer;
  protected
    function GetCount: Integer;
    function GetItem(Idx: integer): TiphSFTPFileAttributes;
    procedure SetItem(Idx: integer; Value: TiphSFTPFileAttributes);
    function GetIsReadOnly: boolean;
    function GetIsFixedSize: boolean;
    function GetSyncRoot: TObject;
    function GetIsSynchronized: boolean;
    procedure ClearList;

    function IsFree: boolean;
    procedure SyncCount;
    procedure InternalSetCount(Value: integer);
    procedure InternalSetItem(Index: integer; Value: TiphSFTPFileAttributes);

    function CtlGetCount: integer; virtual;
    procedure CtlSetCount(Value: integer); virtual;
    function CreateElemInstance(Index: integer): TiphSFTPFileAttributes; virtual;

  public
    constructor Create(); overload;
    constructor Create(ReadOnly: Boolean); overload;
    constructor Create(Owner: TObject; ReadOnly: Boolean); overload;
    destructor Destroy; override;

    procedure Assign(Source: TPersistent); override;
    function Clone: TiphSFTPFileAttributesList;

    function Add(Value: TiphSFTPFileAttributes): Integer; overload;
    function Add(): Integer; overload;
    procedure Remove(value: TiphSFTPFileAttributes);

    function Contains(value: TiphSFTPFileAttributes): Boolean;
    
    function IndexOf(value: TiphSFTPFileAttributes): Integer;
    procedure Insert(Idx: Integer; value: TiphSFTPFileAttributes);
    procedure RemoveAt(idx: Integer);
    
    
    procedure Clear();

    property Count: Integer read GetCount;
    property Item[Index: Integer]: TiphSFTPFileAttributes read GetItem write SetItem; default;
    property IsReadOnly: boolean read GetIsReadOnly;
    property IsFixedSize: boolean read GetIsFixedSize;
    property SyncRoot: TObject read GetSyncRoot;
    property IsSynchronized: boolean read GetIsSynchronized;
  end;

  TiphSSHChannel = class(TPersistent)
  protected
    FOwnerCtl: TObject;
    FReadOnlyProp: boolean;
    FIndex: integer;
    FBytesSent: Integer;

    FChannelId: String;

    FDataToSend: TBytes;

    FReadyToSend: Boolean;

    FRecordLength: Integer;

    FService: String;


    constructor Create(Owner: TObject; ReadOnly: boolean); overload;
    function GetPropBytesSent: Integer; virtual;
    procedure SetPropBytesSent(Value: Integer); virtual;
    function GetPropChannelId: String; virtual;
    procedure SetPropChannelId(Value: String); virtual;
    function GetPropDataToSend: String; virtual;
    procedure SetPropDataToSend(Value: String); virtual;
    function  GetPropDataToSendB : TBytes; virtual;
    procedure SetPropDataToSendB(Value: TBytes); virtual;

    function GetPropReadyToSend: Boolean; virtual;
    procedure SetPropReadyToSend(Value: Boolean); virtual;
    function GetPropRecordLength: Integer; virtual;
    procedure SetPropRecordLength(Value: Integer); virtual;
    function GetPropService: String; virtual;
    procedure SetPropService(Value: String); virtual;

  public
    destructor Destroy; override;

    procedure Assign(Source: TPersistent); override;
    function Clone: TiphSSHChannel; virtual;

    property BytesSent: Integer read GetPropBytesSent;
    property ChannelId: String read GetPropChannelId;
    property DataToSend: String  write SetPropDataToSend;
    property DataToSendB : TBytes  write SetPropDataToSendB;

    property ReadyToSend: Boolean read GetPropReadyToSend;
    property RecordLength: Integer read GetPropRecordLength write SetPropRecordLength;
    property Service: String read GetPropService;
    property Index: integer read FIndex write FIndex;
  end;

  TiphSSHChannelList = class(TPersistent)
  protected
    FItems: TList;
    FReadOnlyProp: Boolean;
    FOwnerCtl: TObject;
    FCount: integer;
  protected
    function GetCount: Integer;
    function GetItem(Idx: integer): TiphSSHChannel;
    procedure SetItem(Idx: integer; Value: TiphSSHChannel);
    function GetIsReadOnly: boolean;
    function GetIsFixedSize: boolean;
    function GetSyncRoot: TObject;
    function GetIsSynchronized: boolean;
    procedure ClearList;

    function IsFree: boolean;
    procedure SyncCount;
    procedure InternalSetCount(Value: integer);
    procedure InternalSetItem(Index: integer; Value: TiphSSHChannel);

    function CtlGetCount: integer; virtual;
    procedure CtlSetCount(Value: integer); virtual;
    function CreateElemInstance(Index: integer): TiphSSHChannel; virtual;

    procedure OptionallyAdjustItemList(MaxCount : integer);

  public
    constructor Create(); overload;
    constructor Create(ReadOnly: Boolean); overload;
    constructor Create(Owner: TObject; ReadOnly: Boolean); overload;
    destructor Destroy; override;

    procedure Assign(Source: TPersistent); override;
    function Clone: TiphSSHChannelList;

    function Add(Value: TiphSSHChannel): Integer; overload;
    function Add(): Integer; overload;
    procedure Remove(value: TiphSSHChannel);

    function Contains(value: TiphSSHChannel): Boolean;
    
    procedure Add(Index : integer); overload;
    procedure Add(Index : integer; value: TiphSSHChannel); overload;
    
    
    procedure Clear();

    property Count: Integer read GetCount;
    property Item[Index: Integer]: TiphSSHChannel read GetItem write SetItem; default;
    property IsReadOnly: boolean read GetIsReadOnly;
    property IsFixedSize: boolean read GetIsFixedSize;
    property SyncRoot: TObject read GetSyncRoot;
    property IsSynchronized: boolean read GetIsSynchronized;
  end;

  TiphSSHConnection = class(TPersistent)
  protected
    FOwnerCtl: TObject;
    FReadOnlyProp: boolean;
    FIndex: integer;
    FConnected: Boolean;

    FConnectionId: String;

    FLocalAddress: String;

    FRemoteHost: String;

    FRemotePort: Integer;

    FTimeout: Integer;


    constructor Create(Owner: TObject; ReadOnly: boolean); overload;
    function GetPropConnected: Boolean; virtual;
    procedure SetPropConnected(Value: Boolean); virtual;
    function GetPropConnectionId: String; virtual;
    procedure SetPropConnectionId(Value: String); virtual;
    function GetPropLocalAddress: String; virtual;
    procedure SetPropLocalAddress(Value: String); virtual;
    function GetPropRemoteHost: String; virtual;
    procedure SetPropRemoteHost(Value: String); virtual;
    function GetPropRemotePort: Integer; virtual;
    procedure SetPropRemotePort(Value: Integer); virtual;
    function GetPropTimeout: Integer; virtual;
    procedure SetPropTimeout(Value: Integer); virtual;

  public
    destructor Destroy; override;

    procedure Assign(Source: TPersistent); override;
    function Clone: TiphSSHConnection; virtual;

    property Connected: Boolean read GetPropConnected write SetPropConnected;
    property ConnectionId: String read GetPropConnectionId;
    property LocalAddress: String read GetPropLocalAddress;
    property RemoteHost: String read GetPropRemoteHost;
    property RemotePort: Integer read GetPropRemotePort;
    property Timeout: Integer read GetPropTimeout write SetPropTimeout;
    property Index: integer read FIndex write FIndex;
  end;

  TiphSSHConnectionList = class(TPersistent)
  protected
    FItems: TList;
    FReadOnlyProp: Boolean;
    FOwnerCtl: TObject;
    FCount: integer;
  protected
    function GetCount: Integer;
    function GetItem(Idx: integer): TiphSSHConnection;
    procedure SetItem(Idx: integer; Value: TiphSSHConnection);
    function GetIsReadOnly: boolean;
    function GetIsFixedSize: boolean;
    function GetSyncRoot: TObject;
    function GetIsSynchronized: boolean;
    procedure ClearList;

    function IsFree: boolean;
    procedure SyncCount;
    procedure InternalSetCount(Value: integer);
    procedure InternalSetItem(Index: integer; Value: TiphSSHConnection);

    function CtlGetCount: integer; virtual;
    procedure CtlSetCount(Value: integer); virtual;
    function CreateElemInstance(Index: integer): TiphSSHConnection; virtual;

    procedure OptionallyAdjustItemList(MaxCount : integer);

  public
    constructor Create(); overload;
    constructor Create(ReadOnly: Boolean); overload;
    constructor Create(Owner: TObject; ReadOnly: Boolean); overload;
    destructor Destroy; override;

    procedure Assign(Source: TPersistent); override;
    function Clone: TiphSSHConnectionList;

    function Add(Value: TiphSSHConnection): Integer; overload;
    function Add(): Integer; overload;
    procedure Remove(value: TiphSSHConnection);

    function Contains(value: TiphSSHConnection): Boolean;
    
    procedure Add(Index : integer); overload;
    procedure Add(Index : integer; value: TiphSSHConnection); overload;
    
    
    procedure Clear();

    property Count: Integer read GetCount;
    property Item[Index: Integer]: TiphSSHConnection read GetItem write SetItem; default;
    property IsReadOnly: boolean read GetIsReadOnly;
    property IsFixedSize: boolean read GetIsFixedSize;
    property SyncRoot: TObject read GetSyncRoot;
    property IsSynchronized: boolean read GetIsSynchronized;
  end;

  TiphSSHPlexOperation = class(TPersistent)
  protected
    FOwnerCtl: TObject;
    FReadOnlyProp: boolean;
    FIndex: integer;
    FOperationId: String;


    constructor Create(Owner: TObject; ReadOnly: boolean); overload;
    function GetPropOperationId: String; virtual;
    procedure SetPropOperationId(Value: String); virtual;

  public
    destructor Destroy; override;

    procedure Assign(Source: TPersistent); override;
    function Clone: TiphSSHPlexOperation; virtual;

    property OperationId: String read GetPropOperationId;
    property Index: integer read FIndex write FIndex;
  end;

  TiphSSHPlexOperationList = class(TPersistent)
  protected
    FItems: TList;
    FReadOnlyProp: Boolean;
    FOwnerCtl: TObject;
    FCount: integer;
  protected
    function GetCount: Integer;
    function GetItem(Idx: integer): TiphSSHPlexOperation;
    procedure SetItem(Idx: integer; Value: TiphSSHPlexOperation);
    function GetIsReadOnly: boolean;
    function GetIsFixedSize: boolean;
    function GetSyncRoot: TObject;
    function GetIsSynchronized: boolean;
    procedure ClearList;

    function IsFree: boolean;
    procedure SyncCount;
    procedure InternalSetCount(Value: integer);
    procedure InternalSetItem(Index: integer; Value: TiphSSHPlexOperation);

    function CtlGetCount: integer; virtual;
    procedure CtlSetCount(Value: integer); virtual;
    function CreateElemInstance(Index: integer): TiphSSHPlexOperation; virtual;

    procedure OptionallyAdjustItemList(MaxCount : integer);

  public
    constructor Create(); overload;
    constructor Create(ReadOnly: Boolean); overload;
    constructor Create(Owner: TObject; ReadOnly: Boolean); overload;
    destructor Destroy; override;

    procedure Assign(Source: TPersistent); override;
    function Clone: TiphSSHPlexOperationList;

    function Add(Value: TiphSSHPlexOperation): Integer; overload;
    function Add(): Integer; overload;
    procedure Remove(value: TiphSSHPlexOperation);

    function Contains(value: TiphSSHPlexOperation): Boolean;
    
    procedure Add(Index : integer); overload;
    procedure Add(Index : integer; value: TiphSSHPlexOperation); overload;
    
    
    procedure Clear();

    property Count: Integer read GetCount;
    property Item[Index: Integer]: TiphSSHPlexOperation read GetItem write SetItem; default;
    property IsReadOnly: boolean read GetIsReadOnly;
    property IsFixedSize: boolean read GetIsFixedSize;
    property SyncRoot: TObject read GetSyncRoot;
    property IsSynchronized: boolean read GetIsSynchronized;
  end;

  TiphSSHPrompt = class(TPersistent)
  protected
    FOwnerCtl: TObject;
    FReadOnlyProp: boolean;
    FIndex: integer;
    FEcho: Boolean;

    FPrompt: String;


    constructor Create(Owner: TObject; ReadOnly: boolean); overload;
    function GetPropEcho: Boolean; virtual;
    procedure SetPropEcho(Value: Boolean); virtual;
    function GetPropPrompt: String; virtual;
    procedure SetPropPrompt(Value: String); virtual;

  public
    constructor Create(); overload; virtual;

    constructor Create(valPrompt: String; valEcho: Boolean); overload; virtual;

    destructor Destroy; override;

    procedure Assign(Source: TPersistent); override;
    function Clone: TiphSSHPrompt; virtual;

    property Echo: Boolean read GetPropEcho write SetPropEcho;
    property Prompt: String read GetPropPrompt write SetPropPrompt;
    property Index: integer read FIndex write FIndex;
  end;

  TiphSSHPromptList = class(TPersistent)
  protected
    FItems: TList;
    FReadOnlyProp: Boolean;
    FOwnerCtl: TObject;
    FCount: integer;
  protected
    function GetCount: Integer;
    function GetItem(Idx: integer): TiphSSHPrompt;
    procedure SetItem(Idx: integer; Value: TiphSSHPrompt);
    function GetIsReadOnly: boolean;
    function GetIsFixedSize: boolean;
    function GetSyncRoot: TObject;
    function GetIsSynchronized: boolean;
    procedure ClearList;

    function IsFree: boolean;
    procedure SyncCount;
    procedure InternalSetCount(Value: integer);
    procedure InternalSetItem(Index: integer; Value: TiphSSHPrompt);

    function CtlGetCount: integer; virtual;
    procedure CtlSetCount(Value: integer); virtual;
    function CreateElemInstance(Index: integer): TiphSSHPrompt; virtual;

  public
    constructor Create(); overload;
    constructor Create(ReadOnly: Boolean); overload;
    constructor Create(Owner: TObject; ReadOnly: Boolean); overload;
    destructor Destroy; override;

    procedure Assign(Source: TPersistent); override;
    function Clone: TiphSSHPromptList;

    function Add(Value: TiphSSHPrompt): Integer; overload;
    function Add(): Integer; overload;
    procedure Remove(value: TiphSSHPrompt);

    function Contains(value: TiphSSHPrompt): Boolean;
    
    function IndexOf(value: TiphSSHPrompt): Integer;
    procedure Insert(Idx: Integer; value: TiphSSHPrompt);
    procedure RemoveAt(idx: Integer);
    
    
    procedure Clear();

    property Count: Integer read GetCount;
    property Item[Index: Integer]: TiphSSHPrompt read GetItem write SetItem; default;
    property IsReadOnly: boolean read GetIsReadOnly;
    property IsFixedSize: boolean read GetIsFixedSize;
    property SyncRoot: TObject read GetSyncRoot;
    property IsSynchronized: boolean read GetIsSynchronized;
  end;

  TiphSSHRTChannel = class(TPersistent)
  protected
    FOwnerCtl: TObject;
    FReadOnlyProp: boolean;
    FIndex: integer;
    FBytesSent: Integer;

    FChannelId: String;

    FConnectedAddress: String;

    FConnectedPort: Integer;

    FDataToSend: TBytes;

    FForwardingHost: String;

    FForwardingPort: Integer;

    FOriginAddress: String;

    FOriginPort: Integer;

    FService: String;


    constructor Create(Owner: TObject; ReadOnly: boolean); overload;
    function GetPropBytesSent: Integer; virtual;
    procedure SetPropBytesSent(Value: Integer); virtual;
    function GetPropChannelId: String; virtual;
    procedure SetPropChannelId(Value: String); virtual;
    function GetPropConnectedAddress: String; virtual;
    procedure SetPropConnectedAddress(Value: String); virtual;
    function GetPropConnectedPort: Integer; virtual;
    procedure SetPropConnectedPort(Value: Integer); virtual;
    function GetPropDataToSend: String; virtual;
    procedure SetPropDataToSend(Value: String); virtual;
    function  GetPropDataToSendB : TBytes; virtual;
    procedure SetPropDataToSendB(Value: TBytes); virtual;

    function GetPropForwardingHost: String; virtual;
    procedure SetPropForwardingHost(Value: String); virtual;
    function GetPropForwardingPort: Integer; virtual;
    procedure SetPropForwardingPort(Value: Integer); virtual;
    function GetPropOriginAddress: String; virtual;
    procedure SetPropOriginAddress(Value: String); virtual;
    function GetPropOriginPort: Integer; virtual;
    procedure SetPropOriginPort(Value: Integer); virtual;
    function GetPropService: String; virtual;
    procedure SetPropService(Value: String); virtual;

  public
    destructor Destroy; override;

    procedure Assign(Source: TPersistent); override;
    function Clone: TiphSSHRTChannel; virtual;

    property BytesSent: Integer read GetPropBytesSent;
    property ChannelId: String read GetPropChannelId;
    property ConnectedAddress: String read GetPropConnectedAddress;
    property ConnectedPort: Integer read GetPropConnectedPort;
    property DataToSend: String  write SetPropDataToSend;
    property DataToSendB : TBytes  write SetPropDataToSendB;

    property ForwardingHost: String read GetPropForwardingHost write SetPropForwardingHost;
    property ForwardingPort: Integer read GetPropForwardingPort write SetPropForwardingPort;
    property OriginAddress: String read GetPropOriginAddress;
    property OriginPort: Integer read GetPropOriginPort;
    property Service: String read GetPropService;
    property Index: integer read FIndex write FIndex;
  end;

  TiphSSHRTChannelList = class(TPersistent)
  protected
    FItems: TList;
    FReadOnlyProp: Boolean;
    FOwnerCtl: TObject;
    FCount: integer;
  protected
    function GetCount: Integer;
    function GetItem(Idx: integer): TiphSSHRTChannel;
    procedure SetItem(Idx: integer; Value: TiphSSHRTChannel);
    function GetIsReadOnly: boolean;
    function GetIsFixedSize: boolean;
    function GetSyncRoot: TObject;
    function GetIsSynchronized: boolean;
    procedure ClearList;

    function IsFree: boolean;
    procedure SyncCount;
    procedure InternalSetCount(Value: integer);
    procedure InternalSetItem(Index: integer; Value: TiphSSHRTChannel);

    function CtlGetCount: integer; virtual;
    procedure CtlSetCount(Value: integer); virtual;
    function CreateElemInstance(Index: integer): TiphSSHRTChannel; virtual;

    procedure OptionallyAdjustItemList(MaxCount : integer);

  public
    constructor Create(); overload;
    constructor Create(ReadOnly: Boolean); overload;
    constructor Create(Owner: TObject; ReadOnly: Boolean); overload;
    destructor Destroy; override;

    procedure Assign(Source: TPersistent); override;
    function Clone: TiphSSHRTChannelList;

    function Add(Value: TiphSSHRTChannel): Integer; overload;
    function Add(): Integer; overload;
    procedure Remove(value: TiphSSHRTChannel);

    function Contains(value: TiphSSHRTChannel): Boolean;
    
    procedure Add(Index : integer); overload;
    procedure Add(Index : integer; value: TiphSSHRTChannel); overload;
    
    
    procedure Clear();

    property Count: Integer read GetCount;
    property Item[Index: Integer]: TiphSSHRTChannel read GetItem write SetItem; default;
    property IsReadOnly: boolean read GetIsReadOnly;
    property IsFixedSize: boolean read GetIsFixedSize;
    property SyncRoot: TObject read GetSyncRoot;
    property IsSynchronized: boolean read GetIsSynchronized;
  end;


implementation

{$T-}
{$WARNINGS OFF}
{$HINTS OFF}



//////////////////////////////////////////////////////////////////////////////////////////
// TiphCertExtension type

constructor TiphCertExtension.Create(valoid: String; valvalue: TBytes; valcritical: Boolean);
begin
  inherited Create;
  FIndex := -1;
  FOwnerCtl := nil;
  FReadOnlyProp := false;
  Foid := valoid;

  Fvalue := valvalue;

  Fcritical := valcritical;

end;


constructor TiphCertExtension.Create(Owner: TObject; ReadOnly: boolean);
begin
  inherited Create;
  FOwnerCtl := Owner;
  FReadOnlyProp := ReadOnly;
  FIndex := -1;
end;

destructor TiphCertExtension.Destroy;
begin
  inherited;
end;

procedure TiphCertExtension.Assign(Source: TPersistent);
var
  i: integer;
  Src: TiphCertExtension;
begin
  // Copy contents using setters and getters. No owner or index is copied: this is simply an assignment
  // of a collection of fields to another object, not a creation of a full copy.
        if Source is TiphCertExtension then
  begin

    SetPropCritical(TiphCertExtension(Source).GetPropCritical);

    SetPropOID(TiphCertExtension(Source).GetPropOID);

    SetPropValueB(TiphCertExtension(Source).GetPropValueB);
  end
  else
    inherited;
end;

function TiphCertExtension.Clone: TiphCertExtension;
begin
  // Always clone to a freestanding instance of the base class
  Result := TiphCertExtension.Create(nil, FReadOnlyProp);
  Result.Assign(Self);
end;

function TiphCertExtension.GetPropCritical: Boolean;
begin
  Result := Boolean(FCritical);
end;

procedure TiphCertExtension.SetPropCritical(Value: Boolean);
begin
  FCritical := Value;
end;

function TiphCertExtension.GetPropOID: String;
begin
  Result := String(FOID);
end;

procedure TiphCertExtension.SetPropOID(Value: String);
begin
  FOID := Value;
end;

function TiphCertExtension.GetPropValue: String;
begin
  Result := {$IFDEF UNICODE}TEncoding.UTF8.GetString{$else}string{$endif}(FValue);
end;

procedure TiphCertExtension.SetPropValue(Value: String);
begin
  FValue := {$IFDEF UNICODE}TEncoding.UTF8.GetBytes{$else}TBytes{$endif}(Value);
end;

function  TiphCertExtension.GetPropValueB : TBytes; 
begin
  Result := FValue;
end;

procedure TiphCertExtension.SetPropValueB(Value: TBytes); 
begin
  FValue := Value;
end;


//////////////////////////////////////////////////////////////////////////////////////////
// TiphCertExtensionList type
constructor TiphCertExtensionList.Create();
begin
  inherited Create;
  FItems := TList.Create;
  FReadOnlyProp := false;
  FCount := 0;
  FOwnerCtl := nil;
end;

constructor TiphCertExtensionList.Create(ReadOnly: Boolean);
begin
  inherited Create;
  FItems := TList.Create;
  FReadOnlyProp := ReadOnly;
  FCount := 0;
  FOwnerCtl := nil;
end;

constructor TiphCertExtensionList.Create(Owner: TObject; ReadOnly: Boolean); 
begin
  inherited Create;
  FItems := TList.Create;
  FReadOnlyProp := ReadOnly;
  FCount := 0;
  FOwnerCtl := Owner;
end;

destructor TiphCertExtensionList.Destroy; 
begin
  ClearList;
  FreeAndNil(FItems);
  inherited;
end;

procedure TiphCertExtensionList.ClearList;
var
  Inst : TiphCertExtension;
  I: integer;
begin
  try
    for I := 0 to FItems.Count - 1 do
    begin
      Inst := TiphCertExtension(FItems[I]);
      {$ifndef AUTOREFCOUNT}
      Inst.Free;
      {$else}
      Inst.__ObjRelease;
      Inst.DisposeOf;
      {$endif};
    end;
  finally
    FItems.Clear;
  end;
end;

function TiphCertExtensionList.IsFree: boolean;
begin
  // This method tells the requestor whether the collection object is freestanding or bound
  Result := FOwnerCtl = nil;
end;

procedure TiphCertExtensionList.SyncCount;
var
  Inst : TiphCertExtension;
  I: integer;
begin
  // This method updates the number of objects held by the collection. It is only relevant for bound collections.
  if not IsFree then
  begin
    FCount := CtlGetCount();
    while FItems.Count < FCount do
    begin
      Inst := CreateElemInstance(FItems.Count);
      FItems.Add(Inst);
      {$ifdef AUTOREFCOUNT}
      Inst.__ObjAddRef;
      {$endif}
    end;
  end;
end;

procedure TiphCertExtensionList.InternalSetCount(Value: integer);
var
  Inst : TiphCertExtension;
begin
  // This method updates the number of elements in the underlying list. 
  // For both free and bound collections, it will verify that the list has sufficient capacity.
  while FItems.Count < Value do
  begin
    Inst := CreateElemInstance(FItems.Count);
    FItems.Add(Inst);
    {$ifdef AUTOREFCOUNT}
    Inst.__ObjAddRef;
    {$endif}
  end;

  if not IsFree then
    CtlSetCount(Value);
  FCount := Value;
end;

procedure TiphCertExtensionList.InternalSetItem(Index: integer; Value: TiphCertExtension);
begin
  // This method assigns (effectively copies) an item to collection.
  // It is expected that the boundaries have been checked before this method was called.
  TiphCertExtension(FItems[Index]).Assign(Value); // Assign, if needed, will redirect any setters to the underlying control's methods
end;

procedure TiphCertExtensionList.Assign(Source: TPersistent);
var
  i: integer;
  Src: TiphCertExtensionList;
begin
  if Source is TiphCertExtensionList then
  begin
    Src := TiphCertExtensionList(Source);
    InternalSetCount(Src.GetCount);
    for i := 0 to Src.GetCount - 1 do
      InternalSetItem(i, Src.GetItem(i));
      
  end
  else
    inherited;
end;

function TiphCertExtensionList.Clone: TiphCertExtensionList;
begin
  Result := TiphCertExtensionList.Create(FReadOnlyProp); // When cloning, always creating a freestanding object
  Result.Assign(Self);
  
end;

function TiphCertExtensionList.GetCount: Integer;
begin
  SyncCount;
  Result := FCount;
end;

function TiphCertExtensionList.GetItem(Idx: integer): TiphCertExtension;
begin
  SyncCount;  
  
  Result := TiphCertExtension(FItems[Idx]); 
end;

procedure TiphCertExtensionList.SetItem(Idx: integer; Value: TiphCertExtension);
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  
  if (Idx < 0) or (Idx >= GetCount) then
    raise Exception.Create('Index out of bounds');

  InternalSetItem(Idx, Value);
end;

function TiphCertExtensionList.GetIsReadOnly: boolean;
begin
  Result := FReadOnlyProp;
end;

function TiphCertExtensionList.GetIsFixedSize: boolean;
begin
  Result := false;
end;

function TiphCertExtensionList.GetSyncRoot: TObject;
begin
  raise Exception.Create('Not implemented');
end;

function TiphCertExtensionList.GetIsSynchronized: boolean;
begin
  Result := false;
end;

function TiphCertExtensionList.Add(Value: TiphCertExtension): Integer;
var
  OldCount: integer;
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OldCount := GetCount();
  InternalSetCount(OldCount + 1);
  InternalSetItem(OldCount, Value);
  Result := OldCount;
end;

function TiphCertExtensionList.Add(): Integer; 
var
  OldCount: integer;
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OldCount := GetCount();
  InternalSetCount(OldCount + 1);
  Result := OldCount;
end;

procedure TiphCertExtensionList.Remove(value: TiphCertExtension);
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  raise Exception.Create('Not implemented');
end;

function TiphCertExtensionList.Contains(value: TiphCertExtension): Boolean;
begin
  raise Exception.Create('Not implemented');
end;

function TiphCertExtensionList.IndexOf(value: TiphCertExtension): Integer;
begin
  raise Exception.Create('Not implemented');
end;

procedure TiphCertExtensionList.Insert(Idx: Integer; value: TiphCertExtension);
var
  OldCount: integer;
  I: integer;
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OldCount := GetCount();
  if (Idx < 0) or (Idx >= OldCount) then
    raise Exception.Create('Index out of bounds');
  InternalSetCount(OldCount + 1);
  for I := OldCount downto Idx + 1 do
    InternalSetItem(I, GetItem(i - 1));
  InternalSetItem(Idx, value);
end;

procedure TiphCertExtensionList.RemoveAt(idx: Integer);
var
  I: integer;
  OldCount: integer;
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OldCount := GetCount();
  if (Idx < 0) or (Idx >= OldCount) then
    raise Exception.Create('Index out of bounds');
  // Only copying the needed subset of elements, without cleaning them up
  for I := Idx to OldCount - 2 do
    InternalSetItem(I, GetItem(I + 1));
  InternalSetCount(OldCount - 1);
end;


procedure TiphCertExtensionList.Clear();
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  InternalSetCount(0);
end;

function TiphCertExtensionList.CtlGetCount: integer;
begin
  ; // to be overridden in derived classes
end;

procedure TiphCertExtensionList.CtlSetCount(Value: integer);
begin
  ; // to be overridden in derived classes
end;

function TiphCertExtensionList.CreateElemInstance(Index: integer): TiphCertExtension; 
begin
  Result := TiphCertExtension.Create(FOwnerCtl, FReadOnlyProp);
  Result.Index := Index;
end;



//////////////////////////////////////////////////////////////////////////////////////////
// TiphConnection type


constructor TiphConnection.Create(Owner: TObject; ReadOnly: boolean);
begin
  inherited Create;
  FOwnerCtl := Owner;
  FReadOnlyProp := ReadOnly;
  FIndex := -1;
end;

destructor TiphConnection.Destroy;
begin
  inherited;
end;

procedure TiphConnection.Assign(Source: TPersistent);
var
  i: integer;
  Src: TiphConnection;
begin
  // Copy contents using setters and getters. No owner or index is copied: this is simply an assignment
  // of a collection of fields to another object, not a creation of a full copy.
                                  if Source is TiphConnection then
  begin

    SetPropAcceptData(TiphConnection(Source).GetPropAcceptData);

    SetPropBytesSent(TiphConnection(Source).GetPropBytesSent);

    SetPropConnected(TiphConnection(Source).GetPropConnected);

    SetPropConnectionId(TiphConnection(Source).GetPropConnectionId);

    SetPropDataToSendB(TiphConnection(Source).GetPropDataToSendB);
    SetPropEOLB(TiphConnection(Source).GetPropEOLB);
    SetPropIdleTimeout(TiphConnection(Source).GetPropIdleTimeout);

    SetPropLocalAddress(TiphConnection(Source).GetPropLocalAddress);

    SetPropMaxLineLength(TiphConnection(Source).GetPropMaxLineLength);

    SetPropReadyToSend(TiphConnection(Source).GetPropReadyToSend);

    SetPropRecordLength(TiphConnection(Source).GetPropRecordLength);

    SetPropRemoteHost(TiphConnection(Source).GetPropRemoteHost);

    SetPropRemotePort(TiphConnection(Source).GetPropRemotePort);

    SetPropSingleLineMode(TiphConnection(Source).GetPropSingleLineMode);

    SetPropTimeout(TiphConnection(Source).GetPropTimeout);

    SetPropUserDataB(TiphConnection(Source).GetPropUserDataB);
  end
  else
    inherited;
end;

function TiphConnection.Clone: TiphConnection;
begin
  // Always clone to a freestanding instance of the base class
  Result := TiphConnection.Create(nil, FReadOnlyProp);
  Result.Assign(Self);
end;

function TiphConnection.GetPropAcceptData: Boolean;
begin
  Result := Boolean(FAcceptData);
end;

procedure TiphConnection.SetPropAcceptData(Value: Boolean);
begin
  FAcceptData := Value;
end;

function TiphConnection.GetPropBytesSent: Integer;
begin
  Result := Integer(FBytesSent);
end;

procedure TiphConnection.SetPropBytesSent(Value: Integer);
begin
  FBytesSent := Value;
end;

function TiphConnection.GetPropConnected: Boolean;
begin
  Result := Boolean(FConnected);
end;

procedure TiphConnection.SetPropConnected(Value: Boolean);
begin
  FConnected := Value;
end;

function TiphConnection.GetPropConnectionId: String;
begin
  Result := String(FConnectionId);
end;

procedure TiphConnection.SetPropConnectionId(Value: String);
begin
  FConnectionId := Value;
end;

function TiphConnection.GetPropDataToSend: String;
begin
  Result := {$IFDEF UNICODE}TEncoding.UTF8.GetString{$else}string{$endif}(FDataToSend);
end;

procedure TiphConnection.SetPropDataToSend(Value: String);
begin
  FDataToSend := {$IFDEF UNICODE}TEncoding.UTF8.GetBytes{$else}TBytes{$endif}(Value);
end;

function  TiphConnection.GetPropDataToSendB : TBytes; 
begin
  Result := FDataToSend;
end;

procedure TiphConnection.SetPropDataToSendB(Value: TBytes); 
begin
  FDataToSend := Value;
end;

function TiphConnection.GetPropEOL: String;
begin
  Result := {$IFDEF UNICODE}TEncoding.UTF8.GetString{$else}string{$endif}(FEOL);
end;

procedure TiphConnection.SetPropEOL(Value: String);
begin
  FEOL := {$IFDEF UNICODE}TEncoding.UTF8.GetBytes{$else}TBytes{$endif}(Value);
end;

function  TiphConnection.GetPropEOLB : TBytes; 
begin
  Result := FEOL;
end;

procedure TiphConnection.SetPropEOLB(Value: TBytes); 
begin
  FEOL := Value;
end;

function TiphConnection.GetPropIdleTimeout: Integer;
begin
  Result := Integer(FIdleTimeout);
end;

procedure TiphConnection.SetPropIdleTimeout(Value: Integer);
begin
  FIdleTimeout := Value;
end;

function TiphConnection.GetPropLocalAddress: String;
begin
  Result := String(FLocalAddress);
end;

procedure TiphConnection.SetPropLocalAddress(Value: String);
begin
  FLocalAddress := Value;
end;

function TiphConnection.GetPropMaxLineLength: Integer;
begin
  Result := Integer(FMaxLineLength);
end;

procedure TiphConnection.SetPropMaxLineLength(Value: Integer);
begin
  FMaxLineLength := Value;
end;

function TiphConnection.GetPropReadyToSend: Boolean;
begin
  Result := Boolean(FReadyToSend);
end;

procedure TiphConnection.SetPropReadyToSend(Value: Boolean);
begin
  FReadyToSend := Value;
end;

function TiphConnection.GetPropRecordLength: Integer;
begin
  Result := Integer(FRecordLength);
end;

procedure TiphConnection.SetPropRecordLength(Value: Integer);
begin
  FRecordLength := Value;
end;

function TiphConnection.GetPropRemoteHost: String;
begin
  Result := String(FRemoteHost);
end;

procedure TiphConnection.SetPropRemoteHost(Value: String);
begin
  FRemoteHost := Value;
end;

function TiphConnection.GetPropRemotePort: Integer;
begin
  Result := Integer(FRemotePort);
end;

procedure TiphConnection.SetPropRemotePort(Value: Integer);
begin
  FRemotePort := Value;
end;

function TiphConnection.GetPropSingleLineMode: Boolean;
begin
  Result := Boolean(FSingleLineMode);
end;

procedure TiphConnection.SetPropSingleLineMode(Value: Boolean);
begin
  FSingleLineMode := Value;
end;

function TiphConnection.GetPropTimeout: Integer;
begin
  Result := Integer(FTimeout);
end;

procedure TiphConnection.SetPropTimeout(Value: Integer);
begin
  FTimeout := Value;
end;

function TiphConnection.GetPropUserData: String;
begin
  Result := {$IFDEF UNICODE}TEncoding.UTF8.GetString{$else}string{$endif}(FUserData);
end;

procedure TiphConnection.SetPropUserData(Value: String);
begin
  FUserData := {$IFDEF UNICODE}TEncoding.UTF8.GetBytes{$else}TBytes{$endif}(Value);
end;

function  TiphConnection.GetPropUserDataB : TBytes; 
begin
  Result := FUserData;
end;

procedure TiphConnection.SetPropUserDataB(Value: TBytes); 
begin
  FUserData := Value;
end;


//////////////////////////////////////////////////////////////////////////////////////////
// TiphConnectionList type
constructor TiphConnectionList.Create();
begin
  inherited Create;
  FItems := TList.Create;
  FReadOnlyProp := false;
  FCount := 0;
  FOwnerCtl := nil;
end;

constructor TiphConnectionList.Create(ReadOnly: Boolean);
begin
  inherited Create;
  FItems := TList.Create;
  FReadOnlyProp := ReadOnly;
  FCount := 0;
  FOwnerCtl := nil;
end;

constructor TiphConnectionList.Create(Owner: TObject; ReadOnly: Boolean); 
begin
  inherited Create;
  FItems := TList.Create;
  FReadOnlyProp := ReadOnly;
  FCount := 0;
  FOwnerCtl := Owner;
end;

destructor TiphConnectionList.Destroy; 
begin
  ClearList;
  FreeAndNil(FItems);
  inherited;
end;

procedure TiphConnectionList.ClearList;
var
  Inst : TiphConnection;
  I: integer;
begin
  try
    for I := 0 to FItems.Count - 1 do
    begin
      Inst := TiphConnection(FItems[I]);
      {$ifndef AUTOREFCOUNT}
      Inst.Free;
      {$else}
      Inst.__ObjRelease;
      Inst.DisposeOf;
      {$endif};
    end;
  finally
    FItems.Clear;
  end;
end;

function TiphConnectionList.IsFree: boolean;
begin
  // This method tells the requestor whether the collection object is freestanding or bound
  Result := FOwnerCtl = nil;
end;

procedure TiphConnectionList.SyncCount;
var
  Inst : TiphConnection;
  I: integer;
begin
  // This method updates the number of objects held by the collection. It is only relevant for bound collections.
  if not IsFree then
  begin
    FCount := CtlGetCount();
    while FItems.Count < FCount do
    begin
      Inst := CreateElemInstance(FItems.Count);
      FItems.Add(Inst);
      {$ifdef AUTOREFCOUNT}
      Inst.__ObjAddRef;
      {$endif}
    end;
  end;
end;

procedure TiphConnectionList.InternalSetCount(Value: integer);
var
  Inst : TiphConnection;
begin
  // This method updates the number of elements in the underlying list. 
  // For both free and bound collections, it will verify that the list has sufficient capacity.
  while FItems.Count < Value do
  begin
    Inst := CreateElemInstance(FItems.Count);
    FItems.Add(Inst);
    {$ifdef AUTOREFCOUNT}
    Inst.__ObjAddRef;
    {$endif}
  end;

  if not IsFree then
    CtlSetCount(Value);
  FCount := Value;
end;

procedure TiphConnectionList.InternalSetItem(Index: integer; Value: TiphConnection);
begin
  // This method assigns (effectively copies) an item to collection.
  // It is expected that the boundaries have been checked before this method was called.
  TiphConnection(FItems[Index]).Assign(Value); // Assign, if needed, will redirect any setters to the underlying control's methods
end;

procedure TiphConnectionList.Assign(Source: TPersistent);
var
  i: integer;
  Src: TiphConnectionList;
begin
  if Source is TiphConnectionList then
  begin
    raise Exception.Create('Operation prohibited: Assign is not supported by hash table collections');
    
  end
  else
    inherited;
end;

function TiphConnectionList.Clone: TiphConnectionList;
begin
  raise Exception.Create('Operation prohibited: Cloning is not supported by hash table collections');

end;

function TiphConnectionList.GetCount: Integer;
begin
  SyncCount;
  Result := FCount;
end;

function TiphConnectionList.GetItem(Idx: integer): TiphConnection;
begin
  SyncCount;  
  OptionallyAdjustItemList(Idx + 1);

  
  Result := TiphConnection(FItems[Idx]); 
end;

procedure TiphConnectionList.SetItem(Idx: integer; Value: TiphConnection);
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OptionallyAdjustItemList(Idx + 1);

  InternalSetItem(Idx, Value);
end;

function TiphConnectionList.GetIsReadOnly: boolean;
begin
  Result := FReadOnlyProp;
end;

function TiphConnectionList.GetIsFixedSize: boolean;
begin
  Result := false;
end;

function TiphConnectionList.GetSyncRoot: TObject;
begin
  raise Exception.Create('Not implemented');
end;

function TiphConnectionList.GetIsSynchronized: boolean;
begin
  Result := false;
end;

function TiphConnectionList.Add(Value: TiphConnection): Integer;
var
  OldCount: integer;
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OldCount := GetCount();
  InternalSetCount(OldCount + 1);
  InternalSetItem(OldCount, Value);
  Result := OldCount;
end;

function TiphConnectionList.Add(): Integer; 
var
  OldCount: integer;
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OldCount := GetCount();
  InternalSetCount(OldCount + 1);
  Result := OldCount;
end;

procedure TiphConnectionList.Remove(value: TiphConnection);
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  raise Exception.Create('Not implemented');
end;

function TiphConnectionList.Contains(value: TiphConnection): Boolean;
begin
  raise Exception.Create('Not implemented');
end;

procedure TiphConnectionList.Add(Index : integer); 
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  
  OptionallyAdjustItemList(Index + 1);
end;

procedure TiphConnectionList.Add(Index : integer; value: TiphConnection); 
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;

  OptionallyAdjustItemList(Index + 1);
  InternalSetItem(Index, Value);
end;


procedure TiphConnectionList.Clear();
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  InternalSetCount(0);
end;

function TiphConnectionList.CtlGetCount: integer;
begin
  ; // to be overridden in derived classes
end;

procedure TiphConnectionList.CtlSetCount(Value: integer);
begin
  ; // to be overridden in derived classes
end;

function TiphConnectionList.CreateElemInstance(Index: integer): TiphConnection; 
begin
  Result := TiphConnection.Create(FOwnerCtl, FReadOnlyProp);
  Result.Index := Index;
end;

procedure TiphConnectionList.OptionallyAdjustItemList(MaxCount : integer);
const
  HT_MAX_LEN = 1048576;
var
  Inst : TiphConnection;
begin
  // Adjusting our internal list to allow for indices 
  // outside of the collection limits, which can
  // happen for hash table collections.
  if MaxCount > HT_MAX_LEN then // DoS prevention
    raise Exception.Create('Operation prohibited: too many items');
    
  while FItems.Count < MaxCount do
  begin
    Inst := CreateElemInstance(FItems.Count);
    FItems.Add(Inst);
    {$ifdef AUTOREFCOUNT}
    Inst.__ObjAddRef;
    {$endif}
  end;
end;



//////////////////////////////////////////////////////////////////////////////////////////
// TiphDirEntry type

constructor TiphDirEntry.Create();
begin
  inherited Create;
  FIndex := -1;
  FOwnerCtl := nil;
  FReadOnlyProp := false;
end;


constructor TiphDirEntry.Create(Owner: TObject; ReadOnly: boolean);
begin
  inherited Create;
  FOwnerCtl := Owner;
  FReadOnlyProp := ReadOnly;
  FIndex := -1;
end;

destructor TiphDirEntry.Destroy;
begin
  inherited;
end;

procedure TiphDirEntry.Assign(Source: TPersistent);
var
  i: integer;
  Src: TiphDirEntry;
begin
  // Copy contents using setters and getters. No owner or index is copied: this is simply an assignment
  // of a collection of fields to another object, not a creation of a full copy.
              if Source is TiphDirEntry then
  begin

    SetPropEntry(TiphDirEntry(Source).GetPropEntry);

    SetPropFileName(TiphDirEntry(Source).GetPropFileName);

    SetPropFileSize(TiphDirEntry(Source).GetPropFileSize);

    SetPropFileTime(TiphDirEntry(Source).GetPropFileTime);

    SetPropIsDir(TiphDirEntry(Source).GetPropIsDir);

    SetPropIsSymlink(TiphDirEntry(Source).GetPropIsSymlink);

  end
  else
    inherited;
end;

function TiphDirEntry.Clone: TiphDirEntry;
begin
  // Always clone to a freestanding instance of the base class
  Result := TiphDirEntry.Create(nil, FReadOnlyProp);
  Result.Assign(Self);
end;

function TiphDirEntry.GetPropEntry: String;
begin
  Result := String(FEntry);
end;

procedure TiphDirEntry.SetPropEntry(Value: String);
begin
  FEntry := Value;
end;

function TiphDirEntry.GetPropFileName: String;
begin
  Result := String(FFileName);
end;

procedure TiphDirEntry.SetPropFileName(Value: String);
begin
  FFileName := Value;
end;

function TiphDirEntry.GetPropFileSize: Int64;
begin
  Result := Int64(FFileSize);
end;

procedure TiphDirEntry.SetPropFileSize(Value: Int64);
begin
  FFileSize := Value;
end;

function TiphDirEntry.GetPropFileTime: String;
begin
  Result := String(FFileTime);
end;

procedure TiphDirEntry.SetPropFileTime(Value: String);
begin
  FFileTime := Value;
end;

function TiphDirEntry.GetPropIsDir: Boolean;
begin
  Result := Boolean(FIsDir);
end;

procedure TiphDirEntry.SetPropIsDir(Value: Boolean);
begin
  FIsDir := Value;
end;

function TiphDirEntry.GetPropIsSymlink: Boolean;
begin
  Result := Boolean(FIsSymlink);
end;

procedure TiphDirEntry.SetPropIsSymlink(Value: Boolean);
begin
  FIsSymlink := Value;
end;


//////////////////////////////////////////////////////////////////////////////////////////
// TiphDirEntryList type
constructor TiphDirEntryList.Create();
begin
  inherited Create;
  FItems := TList.Create;
  FReadOnlyProp := false;
  FCount := 0;
  FOwnerCtl := nil;
end;

constructor TiphDirEntryList.Create(ReadOnly: Boolean);
begin
  inherited Create;
  FItems := TList.Create;
  FReadOnlyProp := ReadOnly;
  FCount := 0;
  FOwnerCtl := nil;
end;

constructor TiphDirEntryList.Create(Owner: TObject; ReadOnly: Boolean); 
begin
  inherited Create;
  FItems := TList.Create;
  FReadOnlyProp := ReadOnly;
  FCount := 0;
  FOwnerCtl := Owner;
end;

destructor TiphDirEntryList.Destroy; 
begin
  ClearList;
  FreeAndNil(FItems);
  inherited;
end;

procedure TiphDirEntryList.ClearList;
var
  Inst : TiphDirEntry;
  I: integer;
begin
  try
    for I := 0 to FItems.Count - 1 do
    begin
      Inst := TiphDirEntry(FItems[I]);
      {$ifndef AUTOREFCOUNT}
      Inst.Free;
      {$else}
      Inst.__ObjRelease;
      Inst.DisposeOf;
      {$endif};
    end;
  finally
    FItems.Clear;
  end;
end;

function TiphDirEntryList.IsFree: boolean;
begin
  // This method tells the requestor whether the collection object is freestanding or bound
  Result := FOwnerCtl = nil;
end;

procedure TiphDirEntryList.SyncCount;
var
  Inst : TiphDirEntry;
  I: integer;
begin
  // This method updates the number of objects held by the collection. It is only relevant for bound collections.
  if not IsFree then
  begin
    FCount := CtlGetCount();
    while FItems.Count < FCount do
    begin
      Inst := CreateElemInstance(FItems.Count);
      FItems.Add(Inst);
      {$ifdef AUTOREFCOUNT}
      Inst.__ObjAddRef;
      {$endif}
    end;
  end;
end;

procedure TiphDirEntryList.InternalSetCount(Value: integer);
var
  Inst : TiphDirEntry;
begin
  // This method updates the number of elements in the underlying list. 
  // For both free and bound collections, it will verify that the list has sufficient capacity.
  while FItems.Count < Value do
  begin
    Inst := CreateElemInstance(FItems.Count);
    FItems.Add(Inst);
    {$ifdef AUTOREFCOUNT}
    Inst.__ObjAddRef;
    {$endif}
  end;

  if not IsFree then
    CtlSetCount(Value);
  FCount := Value;
end;

procedure TiphDirEntryList.InternalSetItem(Index: integer; Value: TiphDirEntry);
begin
  // This method assigns (effectively copies) an item to collection.
  // It is expected that the boundaries have been checked before this method was called.
  TiphDirEntry(FItems[Index]).Assign(Value); // Assign, if needed, will redirect any setters to the underlying control's methods
end;

procedure TiphDirEntryList.Assign(Source: TPersistent);
var
  i: integer;
  Src: TiphDirEntryList;
begin
  if Source is TiphDirEntryList then
  begin
    Src := TiphDirEntryList(Source);
    InternalSetCount(Src.GetCount);
    for i := 0 to Src.GetCount - 1 do
      InternalSetItem(i, Src.GetItem(i));
      
  end
  else
    inherited;
end;

function TiphDirEntryList.Clone: TiphDirEntryList;
begin
  Result := TiphDirEntryList.Create(FReadOnlyProp); // When cloning, always creating a freestanding object
  Result.Assign(Self);
  
end;

function TiphDirEntryList.GetCount: Integer;
begin
  SyncCount;
  Result := FCount;
end;

function TiphDirEntryList.GetItem(Idx: integer): TiphDirEntry;
begin
  SyncCount;  
  
  Result := TiphDirEntry(FItems[Idx]); 
end;

procedure TiphDirEntryList.SetItem(Idx: integer; Value: TiphDirEntry);
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  
  if (Idx < 0) or (Idx >= GetCount) then
    raise Exception.Create('Index out of bounds');

  InternalSetItem(Idx, Value);
end;

function TiphDirEntryList.GetIsReadOnly: boolean;
begin
  Result := FReadOnlyProp;
end;

function TiphDirEntryList.GetIsFixedSize: boolean;
begin
  Result := false;
end;

function TiphDirEntryList.GetSyncRoot: TObject;
begin
  raise Exception.Create('Not implemented');
end;

function TiphDirEntryList.GetIsSynchronized: boolean;
begin
  Result := false;
end;

function TiphDirEntryList.Add(Value: TiphDirEntry): Integer;
var
  OldCount: integer;
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OldCount := GetCount();
  InternalSetCount(OldCount + 1);
  InternalSetItem(OldCount, Value);
  Result := OldCount;
end;

function TiphDirEntryList.Add(): Integer; 
var
  OldCount: integer;
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OldCount := GetCount();
  InternalSetCount(OldCount + 1);
  Result := OldCount;
end;

procedure TiphDirEntryList.Remove(value: TiphDirEntry);
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  raise Exception.Create('Not implemented');
end;

function TiphDirEntryList.Contains(value: TiphDirEntry): Boolean;
begin
  raise Exception.Create('Not implemented');
end;

function TiphDirEntryList.IndexOf(value: TiphDirEntry): Integer;
begin
  raise Exception.Create('Not implemented');
end;

procedure TiphDirEntryList.Insert(Idx: Integer; value: TiphDirEntry);
var
  OldCount: integer;
  I: integer;
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OldCount := GetCount();
  if (Idx < 0) or (Idx >= OldCount) then
    raise Exception.Create('Index out of bounds');
  InternalSetCount(OldCount + 1);
  for I := OldCount downto Idx + 1 do
    InternalSetItem(I, GetItem(i - 1));
  InternalSetItem(Idx, value);
end;

procedure TiphDirEntryList.RemoveAt(idx: Integer);
var
  I: integer;
  OldCount: integer;
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OldCount := GetCount();
  if (Idx < 0) or (Idx >= OldCount) then
    raise Exception.Create('Index out of bounds');
  // Only copying the needed subset of elements, without cleaning them up
  for I := Idx to OldCount - 2 do
    InternalSetItem(I, GetItem(I + 1));
  InternalSetCount(OldCount - 1);
end;


procedure TiphDirEntryList.Clear();
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  InternalSetCount(0);
end;

function TiphDirEntryList.CtlGetCount: integer;
begin
  ; // to be overridden in derived classes
end;

procedure TiphDirEntryList.CtlSetCount(Value: integer);
begin
  ; // to be overridden in derived classes
end;

function TiphDirEntryList.CreateElemInstance(Index: integer): TiphDirEntry; 
begin
  Result := TiphDirEntry.Create(FOwnerCtl, FReadOnlyProp);
  Result.Index := Index;
end;



//////////////////////////////////////////////////////////////////////////////////////////
// TiphFirewall type

constructor TiphFirewall.Create();
begin
  inherited Create;
  FIndex := -1;
  FOwnerCtl := nil;
  FReadOnlyProp := false;
end;


constructor TiphFirewall.Create(Owner: TObject; ReadOnly: boolean);
begin
  inherited Create;
  FOwnerCtl := Owner;
  FReadOnlyProp := ReadOnly;
  FIndex := -1;
end;

destructor TiphFirewall.Destroy;
begin
  inherited;
end;

procedure TiphFirewall.Assign(Source: TPersistent);
var
  i: integer;
  Src: TiphFirewall;
begin
  // Copy contents using setters and getters. No owner or index is copied: this is simply an assignment
  // of a collection of fields to another object, not a creation of a full copy.
              if Source is TiphFirewall then
  begin

    SetPropAutoDetect(TiphFirewall(Source).GetPropAutoDetect);

    SetPropFirewallType(TiphFirewall(Source).GetPropFirewallType);

    SetPropHost(TiphFirewall(Source).GetPropHost);

    SetPropPassword(TiphFirewall(Source).GetPropPassword);

    SetPropPort(TiphFirewall(Source).GetPropPort);

    SetPropUser(TiphFirewall(Source).GetPropUser);

  end
  else
    inherited;
end;

function TiphFirewall.Clone: TiphFirewall;
begin
  // Always clone to a freestanding instance of the base class
  Result := TiphFirewall.Create(nil, FReadOnlyProp);
  Result.Assign(Self);
end;

function TiphFirewall.GetPropAutoDetect: Boolean;
begin
  Result := Boolean(FAutoDetect);
end;

procedure TiphFirewall.SetPropAutoDetect(Value: Boolean);
begin
  FAutoDetect := Value;
end;

function TiphFirewall.GetPropFirewallType: TiphFirewallTypes;
begin
  Result := {T}(FFirewallType);
end;

procedure TiphFirewall.SetPropFirewallType(Value: TiphFirewallTypes);
begin
  FFirewallType := Value;
end;

function TiphFirewall.GetPropHost: String;
begin
  Result := String(FHost);
end;

procedure TiphFirewall.SetPropHost(Value: String);
begin
  FHost := Value;
end;

function TiphFirewall.GetPropPassword: String;
begin
  Result := String(FPassword);
end;

procedure TiphFirewall.SetPropPassword(Value: String);
begin
  FPassword := Value;
end;

function TiphFirewall.GetPropPort: Integer;
begin
  Result := Integer(FPort);
end;

procedure TiphFirewall.SetPropPort(Value: Integer);
begin
  FPort := Value;
end;

function TiphFirewall.GetPropUser: String;
begin
  Result := String(FUser);
end;

procedure TiphFirewall.SetPropUser(Value: String);
begin
  FUser := Value;
end;


//////////////////////////////////////////////////////////////////////////////////////////
// TiphFirewallList type
constructor TiphFirewallList.Create();
begin
  inherited Create;
  FItems := TList.Create;
  FReadOnlyProp := false;
  FCount := 0;
  FOwnerCtl := nil;
end;

constructor TiphFirewallList.Create(ReadOnly: Boolean);
begin
  inherited Create;
  FItems := TList.Create;
  FReadOnlyProp := ReadOnly;
  FCount := 0;
  FOwnerCtl := nil;
end;

constructor TiphFirewallList.Create(Owner: TObject; ReadOnly: Boolean); 
begin
  inherited Create;
  FItems := TList.Create;
  FReadOnlyProp := ReadOnly;
  FCount := 0;
  FOwnerCtl := Owner;
end;

destructor TiphFirewallList.Destroy; 
begin
  ClearList;
  FreeAndNil(FItems);
  inherited;
end;

procedure TiphFirewallList.ClearList;
var
  Inst : TiphFirewall;
  I: integer;
begin
  try
    for I := 0 to FItems.Count - 1 do
    begin
      Inst := TiphFirewall(FItems[I]);
      {$ifndef AUTOREFCOUNT}
      Inst.Free;
      {$else}
      Inst.__ObjRelease;
      Inst.DisposeOf;
      {$endif};
    end;
  finally
    FItems.Clear;
  end;
end;

function TiphFirewallList.IsFree: boolean;
begin
  // This method tells the requestor whether the collection object is freestanding or bound
  Result := FOwnerCtl = nil;
end;

procedure TiphFirewallList.SyncCount;
var
  Inst : TiphFirewall;
  I: integer;
begin
  // This method updates the number of objects held by the collection. It is only relevant for bound collections.
  if not IsFree then
  begin
    FCount := CtlGetCount();
    while FItems.Count < FCount do
    begin
      Inst := CreateElemInstance(FItems.Count);
      FItems.Add(Inst);
      {$ifdef AUTOREFCOUNT}
      Inst.__ObjAddRef;
      {$endif}
    end;
  end;
end;

procedure TiphFirewallList.InternalSetCount(Value: integer);
var
  Inst : TiphFirewall;
begin
  // This method updates the number of elements in the underlying list. 
  // For both free and bound collections, it will verify that the list has sufficient capacity.
  while FItems.Count < Value do
  begin
    Inst := CreateElemInstance(FItems.Count);
    FItems.Add(Inst);
    {$ifdef AUTOREFCOUNT}
    Inst.__ObjAddRef;
    {$endif}
  end;

  if not IsFree then
    CtlSetCount(Value);
  FCount := Value;
end;

procedure TiphFirewallList.InternalSetItem(Index: integer; Value: TiphFirewall);
begin
  // This method assigns (effectively copies) an item to collection.
  // It is expected that the boundaries have been checked before this method was called.
  TiphFirewall(FItems[Index]).Assign(Value); // Assign, if needed, will redirect any setters to the underlying control's methods
end;

procedure TiphFirewallList.Assign(Source: TPersistent);
var
  i: integer;
  Src: TiphFirewallList;
begin
  if Source is TiphFirewallList then
  begin
    Src := TiphFirewallList(Source);
    InternalSetCount(Src.GetCount);
    for i := 0 to Src.GetCount - 1 do
      InternalSetItem(i, Src.GetItem(i));
      
  end
  else
    inherited;
end;

function TiphFirewallList.Clone: TiphFirewallList;
begin
  Result := TiphFirewallList.Create(FReadOnlyProp); // When cloning, always creating a freestanding object
  Result.Assign(Self);
  
end;

function TiphFirewallList.GetCount: Integer;
begin
  SyncCount;
  Result := FCount;
end;

function TiphFirewallList.GetItem(Idx: integer): TiphFirewall;
begin
  SyncCount;  
  
  Result := TiphFirewall(FItems[Idx]); 
end;

procedure TiphFirewallList.SetItem(Idx: integer; Value: TiphFirewall);
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  
  if (Idx < 0) or (Idx >= GetCount) then
    raise Exception.Create('Index out of bounds');

  InternalSetItem(Idx, Value);
end;

function TiphFirewallList.GetIsReadOnly: boolean;
begin
  Result := FReadOnlyProp;
end;

function TiphFirewallList.GetIsFixedSize: boolean;
begin
  Result := false;
end;

function TiphFirewallList.GetSyncRoot: TObject;
begin
  raise Exception.Create('Not implemented');
end;

function TiphFirewallList.GetIsSynchronized: boolean;
begin
  Result := false;
end;

function TiphFirewallList.Add(Value: TiphFirewall): Integer;
var
  OldCount: integer;
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OldCount := GetCount();
  InternalSetCount(OldCount + 1);
  InternalSetItem(OldCount, Value);
  Result := OldCount;
end;

function TiphFirewallList.Add(): Integer; 
var
  OldCount: integer;
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OldCount := GetCount();
  InternalSetCount(OldCount + 1);
  Result := OldCount;
end;

procedure TiphFirewallList.Remove(value: TiphFirewall);
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  raise Exception.Create('Not implemented');
end;

function TiphFirewallList.Contains(value: TiphFirewall): Boolean;
begin
  raise Exception.Create('Not implemented');
end;

function TiphFirewallList.IndexOf(value: TiphFirewall): Integer;
begin
  raise Exception.Create('Not implemented');
end;

procedure TiphFirewallList.Insert(Idx: Integer; value: TiphFirewall);
var
  OldCount: integer;
  I: integer;
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OldCount := GetCount();
  if (Idx < 0) or (Idx >= OldCount) then
    raise Exception.Create('Index out of bounds');
  InternalSetCount(OldCount + 1);
  for I := OldCount downto Idx + 1 do
    InternalSetItem(I, GetItem(i - 1));
  InternalSetItem(Idx, value);
end;

procedure TiphFirewallList.RemoveAt(idx: Integer);
var
  I: integer;
  OldCount: integer;
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OldCount := GetCount();
  if (Idx < 0) or (Idx >= OldCount) then
    raise Exception.Create('Index out of bounds');
  // Only copying the needed subset of elements, without cleaning them up
  for I := Idx to OldCount - 2 do
    InternalSetItem(I, GetItem(I + 1));
  InternalSetCount(OldCount - 1);
end;


procedure TiphFirewallList.Clear();
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  InternalSetCount(0);
end;

function TiphFirewallList.CtlGetCount: integer;
begin
  ; // to be overridden in derived classes
end;

procedure TiphFirewallList.CtlSetCount(Value: integer);
begin
  ; // to be overridden in derived classes
end;

function TiphFirewallList.CreateElemInstance(Index: integer): TiphFirewall; 
begin
  Result := TiphFirewall.Create(FOwnerCtl, FReadOnlyProp);
  Result.Index := Index;
end;



//////////////////////////////////////////////////////////////////////////////////////////
// TiphSFTPConnection type


constructor TiphSFTPConnection.Create(Owner: TObject; ReadOnly: boolean);
begin
  inherited Create;
  FOwnerCtl := Owner;
  FReadOnlyProp := ReadOnly;
  FIndex := -1;
end;

destructor TiphSFTPConnection.Destroy;
begin
  inherited;
end;

procedure TiphSFTPConnection.Assign(Source: TPersistent);
var
  i: integer;
  Src: TiphSFTPConnection;
begin
  // Copy contents using setters and getters. No owner or index is copied: this is simply an assignment
  // of a collection of fields to another object, not a creation of a full copy.
                    if Source is TiphSFTPConnection then
  begin

    SetPropConnected(TiphSFTPConnection(Source).GetPropConnected);

    SetPropConnectionId(TiphSFTPConnection(Source).GetPropConnectionId);

    SetPropErrorMessage(TiphSFTPConnection(Source).GetPropErrorMessage);

    SetPropFileDataB(TiphSFTPConnection(Source).GetPropFileDataB);
    SetPropLocalAddress(TiphSFTPConnection(Source).GetPropLocalAddress);

    SetPropProtocolVersion(TiphSFTPConnection(Source).GetPropProtocolVersion);

    SetPropRemoteHost(TiphSFTPConnection(Source).GetPropRemoteHost);

    SetPropRemotePort(TiphSFTPConnection(Source).GetPropRemotePort);

    SetPropTimeout(TiphSFTPConnection(Source).GetPropTimeout);

  end
  else
    inherited;
end;

function TiphSFTPConnection.Clone: TiphSFTPConnection;
begin
  // Always clone to a freestanding instance of the base class
  Result := TiphSFTPConnection.Create(nil, FReadOnlyProp);
  Result.Assign(Self);
end;

function TiphSFTPConnection.GetPropConnected: Boolean;
begin
  Result := Boolean(FConnected);
end;

procedure TiphSFTPConnection.SetPropConnected(Value: Boolean);
begin
  FConnected := Value;
end;

function TiphSFTPConnection.GetPropConnectionId: String;
begin
  Result := String(FConnectionId);
end;

procedure TiphSFTPConnection.SetPropConnectionId(Value: String);
begin
  FConnectionId := Value;
end;

function TiphSFTPConnection.GetPropErrorMessage: String;
begin
  Result := String(FErrorMessage);
end;

procedure TiphSFTPConnection.SetPropErrorMessage(Value: String);
begin
  FErrorMessage := Value;
end;

function TiphSFTPConnection.GetPropFileData: String;
begin
  Result := {$IFDEF UNICODE}TEncoding.UTF8.GetString{$else}string{$endif}(FFileData);
end;

procedure TiphSFTPConnection.SetPropFileData(Value: String);
begin
  FFileData := {$IFDEF UNICODE}TEncoding.UTF8.GetBytes{$else}TBytes{$endif}(Value);
end;

function  TiphSFTPConnection.GetPropFileDataB : TBytes; 
begin
  Result := FFileData;
end;

procedure TiphSFTPConnection.SetPropFileDataB(Value: TBytes); 
begin
  FFileData := Value;
end;

function TiphSFTPConnection.GetPropLocalAddress: String;
begin
  Result := String(FLocalAddress);
end;

procedure TiphSFTPConnection.SetPropLocalAddress(Value: String);
begin
  FLocalAddress := Value;
end;

function TiphSFTPConnection.GetPropProtocolVersion: Integer;
begin
  Result := Integer(FProtocolVersion);
end;

procedure TiphSFTPConnection.SetPropProtocolVersion(Value: Integer);
begin
  FProtocolVersion := Value;
end;

function TiphSFTPConnection.GetPropRemoteHost: String;
begin
  Result := String(FRemoteHost);
end;

procedure TiphSFTPConnection.SetPropRemoteHost(Value: String);
begin
  FRemoteHost := Value;
end;

function TiphSFTPConnection.GetPropRemotePort: Integer;
begin
  Result := Integer(FRemotePort);
end;

procedure TiphSFTPConnection.SetPropRemotePort(Value: Integer);
begin
  FRemotePort := Value;
end;

function TiphSFTPConnection.GetPropTimeout: Integer;
begin
  Result := Integer(FTimeout);
end;

procedure TiphSFTPConnection.SetPropTimeout(Value: Integer);
begin
  FTimeout := Value;
end;


//////////////////////////////////////////////////////////////////////////////////////////
// TiphSFTPConnectionList type
constructor TiphSFTPConnectionList.Create();
begin
  inherited Create;
  FItems := TList.Create;
  FReadOnlyProp := false;
  FCount := 0;
  FOwnerCtl := nil;
end;

constructor TiphSFTPConnectionList.Create(ReadOnly: Boolean);
begin
  inherited Create;
  FItems := TList.Create;
  FReadOnlyProp := ReadOnly;
  FCount := 0;
  FOwnerCtl := nil;
end;

constructor TiphSFTPConnectionList.Create(Owner: TObject; ReadOnly: Boolean); 
begin
  inherited Create;
  FItems := TList.Create;
  FReadOnlyProp := ReadOnly;
  FCount := 0;
  FOwnerCtl := Owner;
end;

destructor TiphSFTPConnectionList.Destroy; 
begin
  ClearList;
  FreeAndNil(FItems);
  inherited;
end;

procedure TiphSFTPConnectionList.ClearList;
var
  Inst : TiphSFTPConnection;
  I: integer;
begin
  try
    for I := 0 to FItems.Count - 1 do
    begin
      Inst := TiphSFTPConnection(FItems[I]);
      {$ifndef AUTOREFCOUNT}
      Inst.Free;
      {$else}
      Inst.__ObjRelease;
      Inst.DisposeOf;
      {$endif};
    end;
  finally
    FItems.Clear;
  end;
end;

function TiphSFTPConnectionList.IsFree: boolean;
begin
  // This method tells the requestor whether the collection object is freestanding or bound
  Result := FOwnerCtl = nil;
end;

procedure TiphSFTPConnectionList.SyncCount;
var
  Inst : TiphSFTPConnection;
  I: integer;
begin
  // This method updates the number of objects held by the collection. It is only relevant for bound collections.
  if not IsFree then
  begin
    FCount := CtlGetCount();
    while FItems.Count < FCount do
    begin
      Inst := CreateElemInstance(FItems.Count);
      FItems.Add(Inst);
      {$ifdef AUTOREFCOUNT}
      Inst.__ObjAddRef;
      {$endif}
    end;
  end;
end;

procedure TiphSFTPConnectionList.InternalSetCount(Value: integer);
var
  Inst : TiphSFTPConnection;
begin
  // This method updates the number of elements in the underlying list. 
  // For both free and bound collections, it will verify that the list has sufficient capacity.
  while FItems.Count < Value do
  begin
    Inst := CreateElemInstance(FItems.Count);
    FItems.Add(Inst);
    {$ifdef AUTOREFCOUNT}
    Inst.__ObjAddRef;
    {$endif}
  end;

  if not IsFree then
    CtlSetCount(Value);
  FCount := Value;
end;

procedure TiphSFTPConnectionList.InternalSetItem(Index: integer; Value: TiphSFTPConnection);
begin
  // This method assigns (effectively copies) an item to collection.
  // It is expected that the boundaries have been checked before this method was called.
  TiphSFTPConnection(FItems[Index]).Assign(Value); // Assign, if needed, will redirect any setters to the underlying control's methods
end;

procedure TiphSFTPConnectionList.Assign(Source: TPersistent);
var
  i: integer;
  Src: TiphSFTPConnectionList;
begin
  if Source is TiphSFTPConnectionList then
  begin
    raise Exception.Create('Operation prohibited: Assign is not supported by hash table collections');
    
  end
  else
    inherited;
end;

function TiphSFTPConnectionList.Clone: TiphSFTPConnectionList;
begin
  raise Exception.Create('Operation prohibited: Cloning is not supported by hash table collections');

end;

function TiphSFTPConnectionList.GetCount: Integer;
begin
  SyncCount;
  Result := FCount;
end;

function TiphSFTPConnectionList.GetItem(Idx: integer): TiphSFTPConnection;
begin
  SyncCount;  
  OptionallyAdjustItemList(Idx + 1);

  
  Result := TiphSFTPConnection(FItems[Idx]); 
end;

procedure TiphSFTPConnectionList.SetItem(Idx: integer; Value: TiphSFTPConnection);
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OptionallyAdjustItemList(Idx + 1);

  InternalSetItem(Idx, Value);
end;

function TiphSFTPConnectionList.GetIsReadOnly: boolean;
begin
  Result := FReadOnlyProp;
end;

function TiphSFTPConnectionList.GetIsFixedSize: boolean;
begin
  Result := false;
end;

function TiphSFTPConnectionList.GetSyncRoot: TObject;
begin
  raise Exception.Create('Not implemented');
end;

function TiphSFTPConnectionList.GetIsSynchronized: boolean;
begin
  Result := false;
end;

function TiphSFTPConnectionList.Add(Value: TiphSFTPConnection): Integer;
var
  OldCount: integer;
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OldCount := GetCount();
  InternalSetCount(OldCount + 1);
  InternalSetItem(OldCount, Value);
  Result := OldCount;
end;

function TiphSFTPConnectionList.Add(): Integer; 
var
  OldCount: integer;
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OldCount := GetCount();
  InternalSetCount(OldCount + 1);
  Result := OldCount;
end;

procedure TiphSFTPConnectionList.Remove(value: TiphSFTPConnection);
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  raise Exception.Create('Not implemented');
end;

function TiphSFTPConnectionList.Contains(value: TiphSFTPConnection): Boolean;
begin
  raise Exception.Create('Not implemented');
end;

procedure TiphSFTPConnectionList.Add(Index : integer); 
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  
  OptionallyAdjustItemList(Index + 1);
end;

procedure TiphSFTPConnectionList.Add(Index : integer; value: TiphSFTPConnection); 
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;

  OptionallyAdjustItemList(Index + 1);
  InternalSetItem(Index, Value);
end;


procedure TiphSFTPConnectionList.Clear();
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  InternalSetCount(0);
end;

function TiphSFTPConnectionList.CtlGetCount: integer;
begin
  ; // to be overridden in derived classes
end;

procedure TiphSFTPConnectionList.CtlSetCount(Value: integer);
begin
  ; // to be overridden in derived classes
end;

function TiphSFTPConnectionList.CreateElemInstance(Index: integer): TiphSFTPConnection; 
begin
  Result := TiphSFTPConnection.Create(FOwnerCtl, FReadOnlyProp);
  Result.Index := Index;
end;

procedure TiphSFTPConnectionList.OptionallyAdjustItemList(MaxCount : integer);
const
  HT_MAX_LEN = 1048576;
var
  Inst : TiphSFTPConnection;
begin
  // Adjusting our internal list to allow for indices 
  // outside of the collection limits, which can
  // happen for hash table collections.
  if MaxCount > HT_MAX_LEN then // DoS prevention
    raise Exception.Create('Operation prohibited: too many items');
    
  while FItems.Count < MaxCount do
  begin
    Inst := CreateElemInstance(FItems.Count);
    FItems.Add(Inst);
    {$ifdef AUTOREFCOUNT}
    Inst.__ObjAddRef;
    {$endif}
  end;
end;



//////////////////////////////////////////////////////////////////////////////////////////
// TiphSFTPFileAttributes type


constructor TiphSFTPFileAttributes.Create(Owner: TObject; ReadOnly: boolean);
begin
  inherited Create;
  FOwnerCtl := Owner;
  FReadOnlyProp := ReadOnly;
  FIndex := -1;
end;

destructor TiphSFTPFileAttributes.Destroy;
begin
  inherited;
end;

procedure TiphSFTPFileAttributes.Assign(Source: TPersistent);
var
  i: integer;
  Src: TiphSFTPFileAttributes;
begin
  // Copy contents using setters and getters. No owner or index is copied: this is simply an assignment
  // of a collection of fields to another object, not a creation of a full copy.
                                                if Source is TiphSFTPFileAttributes then
  begin

    SetPropAccessTime(TiphSFTPFileAttributes(Source).GetPropAccessTime);

    SetPropAccessTimeNanos(TiphSFTPFileAttributes(Source).GetPropAccessTimeNanos);

    SetPropACL(TiphSFTPFileAttributes(Source).GetPropACL);

    SetPropAllocationSize(TiphSFTPFileAttributes(Source).GetPropAllocationSize);

    SetPropAttributeBits(TiphSFTPFileAttributes(Source).GetPropAttributeBits);

    SetPropAttributeBitsValid(TiphSFTPFileAttributes(Source).GetPropAttributeBitsValid);

    SetPropCreationTime(TiphSFTPFileAttributes(Source).GetPropCreationTime);

    SetPropCreationTimeNanos(TiphSFTPFileAttributes(Source).GetPropCreationTimeNanos);

    SetPropFileType(TiphSFTPFileAttributes(Source).GetPropFileType);

    SetPropFlags(TiphSFTPFileAttributes(Source).GetPropFlags);

    SetPropGroupId(TiphSFTPFileAttributes(Source).GetPropGroupId);

    SetPropIsDir(TiphSFTPFileAttributes(Source).GetPropIsDir);

    SetPropIsSymlink(TiphSFTPFileAttributes(Source).GetPropIsSymlink);

    SetPropLinkCount(TiphSFTPFileAttributes(Source).GetPropLinkCount);

    SetPropMIMEType(TiphSFTPFileAttributes(Source).GetPropMIMEType);

    SetPropModifiedTime(TiphSFTPFileAttributes(Source).GetPropModifiedTime);

    SetPropModifiedTimeNanos(TiphSFTPFileAttributes(Source).GetPropModifiedTimeNanos);

    SetPropOwnerId(TiphSFTPFileAttributes(Source).GetPropOwnerId);

    SetPropPermissions(TiphSFTPFileAttributes(Source).GetPropPermissions);

    SetPropPermissionsOctal(TiphSFTPFileAttributes(Source).GetPropPermissionsOctal);

    SetPropSize(TiphSFTPFileAttributes(Source).GetPropSize);

    SetPropTextHint(TiphSFTPFileAttributes(Source).GetPropTextHint);

    SetPropUntranslatedName(TiphSFTPFileAttributes(Source).GetPropUntranslatedName);

  end
  else
    inherited;
end;

function TiphSFTPFileAttributes.Clone: TiphSFTPFileAttributes;
begin
  // Always clone to a freestanding instance of the base class
  Result := TiphSFTPFileAttributes.Create(nil, FReadOnlyProp);
  Result.Assign(Self);
end;

function TiphSFTPFileAttributes.GetPropAccessTime: Int64;
begin
  Result := Int64(FAccessTime);
end;

procedure TiphSFTPFileAttributes.SetPropAccessTime(Value: Int64);
begin
  FAccessTime := Value;
end;

function TiphSFTPFileAttributes.GetPropAccessTimeNanos: Integer;
begin
  Result := Integer(FAccessTimeNanos);
end;

procedure TiphSFTPFileAttributes.SetPropAccessTimeNanos(Value: Integer);
begin
  FAccessTimeNanos := Value;
end;

function TiphSFTPFileAttributes.GetPropACL: String;
begin
  Result := String(FACL);
end;

procedure TiphSFTPFileAttributes.SetPropACL(Value: String);
begin
  FACL := Value;
end;

function TiphSFTPFileAttributes.GetPropAllocationSize: Int64;
begin
  Result := Int64(FAllocationSize);
end;

procedure TiphSFTPFileAttributes.SetPropAllocationSize(Value: Int64);
begin
  FAllocationSize := Value;
end;

function TiphSFTPFileAttributes.GetPropAttributeBits: Integer;
begin
  Result := Integer(FAttributeBits);
end;

procedure TiphSFTPFileAttributes.SetPropAttributeBits(Value: Integer);
begin
  FAttributeBits := Value;
end;

function TiphSFTPFileAttributes.GetPropAttributeBitsValid: Integer;
begin
  Result := Integer(FAttributeBitsValid);
end;

procedure TiphSFTPFileAttributes.SetPropAttributeBitsValid(Value: Integer);
begin
  FAttributeBitsValid := Value;
end;

function TiphSFTPFileAttributes.GetPropCreationTime: Int64;
begin
  Result := Int64(FCreationTime);
end;

procedure TiphSFTPFileAttributes.SetPropCreationTime(Value: Int64);
begin
  FCreationTime := Value;
end;

function TiphSFTPFileAttributes.GetPropCreationTimeNanos: Integer;
begin
  Result := Integer(FCreationTimeNanos);
end;

procedure TiphSFTPFileAttributes.SetPropCreationTimeNanos(Value: Integer);
begin
  FCreationTimeNanos := Value;
end;

function TiphSFTPFileAttributes.GetPropFileType: TiphSFTPFileTypes;
begin
  Result := {T}(FFileType);
end;

procedure TiphSFTPFileAttributes.SetPropFileType(Value: TiphSFTPFileTypes);
begin
  FFileType := Value;
end;

function TiphSFTPFileAttributes.GetPropFlags: Integer;
begin
  Result := Integer(FFlags);
end;

procedure TiphSFTPFileAttributes.SetPropFlags(Value: Integer);
begin
  FFlags := Value;
end;

function TiphSFTPFileAttributes.GetPropGroupId: String;
begin
  Result := String(FGroupId);
end;

procedure TiphSFTPFileAttributes.SetPropGroupId(Value: String);
begin
  FGroupId := Value;
end;

function TiphSFTPFileAttributes.GetPropIsDir: Boolean;
begin
  Result := Boolean(FIsDir);
end;

procedure TiphSFTPFileAttributes.SetPropIsDir(Value: Boolean);
begin
  FIsDir := Value;
end;

function TiphSFTPFileAttributes.GetPropIsSymlink: Boolean;
begin
  Result := Boolean(FIsSymlink);
end;

procedure TiphSFTPFileAttributes.SetPropIsSymlink(Value: Boolean);
begin
  FIsSymlink := Value;
end;

function TiphSFTPFileAttributes.GetPropLinkCount: Integer;
begin
  Result := Integer(FLinkCount);
end;

procedure TiphSFTPFileAttributes.SetPropLinkCount(Value: Integer);
begin
  FLinkCount := Value;
end;

function TiphSFTPFileAttributes.GetPropMIMEType: String;
begin
  Result := String(FMIMEType);
end;

procedure TiphSFTPFileAttributes.SetPropMIMEType(Value: String);
begin
  FMIMEType := Value;
end;

function TiphSFTPFileAttributes.GetPropModifiedTime: Int64;
begin
  Result := Int64(FModifiedTime);
end;

procedure TiphSFTPFileAttributes.SetPropModifiedTime(Value: Int64);
begin
  FModifiedTime := Value;
end;

function TiphSFTPFileAttributes.GetPropModifiedTimeNanos: Integer;
begin
  Result := Integer(FModifiedTimeNanos);
end;

procedure TiphSFTPFileAttributes.SetPropModifiedTimeNanos(Value: Integer);
begin
  FModifiedTimeNanos := Value;
end;

function TiphSFTPFileAttributes.GetPropOwnerId: String;
begin
  Result := String(FOwnerId);
end;

procedure TiphSFTPFileAttributes.SetPropOwnerId(Value: String);
begin
  FOwnerId := Value;
end;

function TiphSFTPFileAttributes.GetPropPermissions: Integer;
begin
  Result := Integer(FPermissions);
end;

procedure TiphSFTPFileAttributes.SetPropPermissions(Value: Integer);
begin
  FPermissions := Value;
end;

function TiphSFTPFileAttributes.GetPropPermissionsOctal: String;
begin
  Result := String(FPermissionsOctal);
end;

procedure TiphSFTPFileAttributes.SetPropPermissionsOctal(Value: String);
begin
  FPermissionsOctal := Value;
end;

function TiphSFTPFileAttributes.GetPropSize: Int64;
begin
  Result := Int64(FSize);
end;

procedure TiphSFTPFileAttributes.SetPropSize(Value: Int64);
begin
  FSize := Value;
end;

function TiphSFTPFileAttributes.GetPropTextHint: Integer;
begin
  Result := Integer(FTextHint);
end;

procedure TiphSFTPFileAttributes.SetPropTextHint(Value: Integer);
begin
  FTextHint := Value;
end;

function TiphSFTPFileAttributes.GetPropUntranslatedName: String;
begin
  Result := String(FUntranslatedName);
end;

procedure TiphSFTPFileAttributes.SetPropUntranslatedName(Value: String);
begin
  FUntranslatedName := Value;
end;


//////////////////////////////////////////////////////////////////////////////////////////
// TiphSFTPFileAttributesList type
constructor TiphSFTPFileAttributesList.Create();
begin
  inherited Create;
  FItems := TList.Create;
  FReadOnlyProp := false;
  FCount := 0;
  FOwnerCtl := nil;
end;

constructor TiphSFTPFileAttributesList.Create(ReadOnly: Boolean);
begin
  inherited Create;
  FItems := TList.Create;
  FReadOnlyProp := ReadOnly;
  FCount := 0;
  FOwnerCtl := nil;
end;

constructor TiphSFTPFileAttributesList.Create(Owner: TObject; ReadOnly: Boolean); 
begin
  inherited Create;
  FItems := TList.Create;
  FReadOnlyProp := ReadOnly;
  FCount := 0;
  FOwnerCtl := Owner;
end;

destructor TiphSFTPFileAttributesList.Destroy; 
begin
  ClearList;
  FreeAndNil(FItems);
  inherited;
end;

procedure TiphSFTPFileAttributesList.ClearList;
var
  Inst : TiphSFTPFileAttributes;
  I: integer;
begin
  try
    for I := 0 to FItems.Count - 1 do
    begin
      Inst := TiphSFTPFileAttributes(FItems[I]);
      {$ifndef AUTOREFCOUNT}
      Inst.Free;
      {$else}
      Inst.__ObjRelease;
      Inst.DisposeOf;
      {$endif};
    end;
  finally
    FItems.Clear;
  end;
end;

function TiphSFTPFileAttributesList.IsFree: boolean;
begin
  // This method tells the requestor whether the collection object is freestanding or bound
  Result := FOwnerCtl = nil;
end;

procedure TiphSFTPFileAttributesList.SyncCount;
var
  Inst : TiphSFTPFileAttributes;
  I: integer;
begin
  // This method updates the number of objects held by the collection. It is only relevant for bound collections.
  if not IsFree then
  begin
    FCount := CtlGetCount();
    while FItems.Count < FCount do
    begin
      Inst := CreateElemInstance(FItems.Count);
      FItems.Add(Inst);
      {$ifdef AUTOREFCOUNT}
      Inst.__ObjAddRef;
      {$endif}
    end;
  end;
end;

procedure TiphSFTPFileAttributesList.InternalSetCount(Value: integer);
var
  Inst : TiphSFTPFileAttributes;
begin
  // This method updates the number of elements in the underlying list. 
  // For both free and bound collections, it will verify that the list has sufficient capacity.
  while FItems.Count < Value do
  begin
    Inst := CreateElemInstance(FItems.Count);
    FItems.Add(Inst);
    {$ifdef AUTOREFCOUNT}
    Inst.__ObjAddRef;
    {$endif}
  end;

  if not IsFree then
    CtlSetCount(Value);
  FCount := Value;
end;

procedure TiphSFTPFileAttributesList.InternalSetItem(Index: integer; Value: TiphSFTPFileAttributes);
begin
  // This method assigns (effectively copies) an item to collection.
  // It is expected that the boundaries have been checked before this method was called.
  TiphSFTPFileAttributes(FItems[Index]).Assign(Value); // Assign, if needed, will redirect any setters to the underlying control's methods
end;

procedure TiphSFTPFileAttributesList.Assign(Source: TPersistent);
var
  i: integer;
  Src: TiphSFTPFileAttributesList;
begin
  if Source is TiphSFTPFileAttributesList then
  begin
    Src := TiphSFTPFileAttributesList(Source);
    InternalSetCount(Src.GetCount);
    for i := 0 to Src.GetCount - 1 do
      InternalSetItem(i, Src.GetItem(i));
      
  end
  else
    inherited;
end;

function TiphSFTPFileAttributesList.Clone: TiphSFTPFileAttributesList;
begin
  Result := TiphSFTPFileAttributesList.Create(FReadOnlyProp); // When cloning, always creating a freestanding object
  Result.Assign(Self);
  
end;

function TiphSFTPFileAttributesList.GetCount: Integer;
begin
  SyncCount;
  Result := FCount;
end;

function TiphSFTPFileAttributesList.GetItem(Idx: integer): TiphSFTPFileAttributes;
begin
  SyncCount;  
  
  Result := TiphSFTPFileAttributes(FItems[Idx]); 
end;

procedure TiphSFTPFileAttributesList.SetItem(Idx: integer; Value: TiphSFTPFileAttributes);
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  
  if (Idx < 0) or (Idx >= GetCount) then
    raise Exception.Create('Index out of bounds');

  InternalSetItem(Idx, Value);
end;

function TiphSFTPFileAttributesList.GetIsReadOnly: boolean;
begin
  Result := FReadOnlyProp;
end;

function TiphSFTPFileAttributesList.GetIsFixedSize: boolean;
begin
  Result := false;
end;

function TiphSFTPFileAttributesList.GetSyncRoot: TObject;
begin
  raise Exception.Create('Not implemented');
end;

function TiphSFTPFileAttributesList.GetIsSynchronized: boolean;
begin
  Result := false;
end;

function TiphSFTPFileAttributesList.Add(Value: TiphSFTPFileAttributes): Integer;
var
  OldCount: integer;
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OldCount := GetCount();
  InternalSetCount(OldCount + 1);
  InternalSetItem(OldCount, Value);
  Result := OldCount;
end;

function TiphSFTPFileAttributesList.Add(): Integer; 
var
  OldCount: integer;
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OldCount := GetCount();
  InternalSetCount(OldCount + 1);
  Result := OldCount;
end;

procedure TiphSFTPFileAttributesList.Remove(value: TiphSFTPFileAttributes);
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  raise Exception.Create('Not implemented');
end;

function TiphSFTPFileAttributesList.Contains(value: TiphSFTPFileAttributes): Boolean;
begin
  raise Exception.Create('Not implemented');
end;

function TiphSFTPFileAttributesList.IndexOf(value: TiphSFTPFileAttributes): Integer;
begin
  raise Exception.Create('Not implemented');
end;

procedure TiphSFTPFileAttributesList.Insert(Idx: Integer; value: TiphSFTPFileAttributes);
var
  OldCount: integer;
  I: integer;
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OldCount := GetCount();
  if (Idx < 0) or (Idx >= OldCount) then
    raise Exception.Create('Index out of bounds');
  InternalSetCount(OldCount + 1);
  for I := OldCount downto Idx + 1 do
    InternalSetItem(I, GetItem(i - 1));
  InternalSetItem(Idx, value);
end;

procedure TiphSFTPFileAttributesList.RemoveAt(idx: Integer);
var
  I: integer;
  OldCount: integer;
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OldCount := GetCount();
  if (Idx < 0) or (Idx >= OldCount) then
    raise Exception.Create('Index out of bounds');
  // Only copying the needed subset of elements, without cleaning them up
  for I := Idx to OldCount - 2 do
    InternalSetItem(I, GetItem(I + 1));
  InternalSetCount(OldCount - 1);
end;


procedure TiphSFTPFileAttributesList.Clear();
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  InternalSetCount(0);
end;

function TiphSFTPFileAttributesList.CtlGetCount: integer;
begin
  ; // to be overridden in derived classes
end;

procedure TiphSFTPFileAttributesList.CtlSetCount(Value: integer);
begin
  ; // to be overridden in derived classes
end;

function TiphSFTPFileAttributesList.CreateElemInstance(Index: integer): TiphSFTPFileAttributes; 
begin
  Result := TiphSFTPFileAttributes.Create(FOwnerCtl, FReadOnlyProp);
  Result.Index := Index;
end;



//////////////////////////////////////////////////////////////////////////////////////////
// TiphSSHChannel type


constructor TiphSSHChannel.Create(Owner: TObject; ReadOnly: boolean);
begin
  inherited Create;
  FOwnerCtl := Owner;
  FReadOnlyProp := ReadOnly;
  FIndex := -1;
end;

destructor TiphSSHChannel.Destroy;
begin
  inherited;
end;

procedure TiphSSHChannel.Assign(Source: TPersistent);
var
  i: integer;
  Src: TiphSSHChannel;
begin
  // Copy contents using setters and getters. No owner or index is copied: this is simply an assignment
  // of a collection of fields to another object, not a creation of a full copy.
              if Source is TiphSSHChannel then
  begin

    SetPropBytesSent(TiphSSHChannel(Source).GetPropBytesSent);

    SetPropChannelId(TiphSSHChannel(Source).GetPropChannelId);

    SetPropDataToSendB(TiphSSHChannel(Source).GetPropDataToSendB);
    SetPropReadyToSend(TiphSSHChannel(Source).GetPropReadyToSend);

    SetPropRecordLength(TiphSSHChannel(Source).GetPropRecordLength);

    SetPropService(TiphSSHChannel(Source).GetPropService);

  end
  else
    inherited;
end;

function TiphSSHChannel.Clone: TiphSSHChannel;
begin
  // Always clone to a freestanding instance of the base class
  Result := TiphSSHChannel.Create(nil, FReadOnlyProp);
  Result.Assign(Self);
end;

function TiphSSHChannel.GetPropBytesSent: Integer;
begin
  Result := Integer(FBytesSent);
end;

procedure TiphSSHChannel.SetPropBytesSent(Value: Integer);
begin
  FBytesSent := Value;
end;

function TiphSSHChannel.GetPropChannelId: String;
begin
  Result := String(FChannelId);
end;

procedure TiphSSHChannel.SetPropChannelId(Value: String);
begin
  FChannelId := Value;
end;

function TiphSSHChannel.GetPropDataToSend: String;
begin
  Result := {$IFDEF UNICODE}TEncoding.UTF8.GetString{$else}string{$endif}(FDataToSend);
end;

procedure TiphSSHChannel.SetPropDataToSend(Value: String);
begin
  FDataToSend := {$IFDEF UNICODE}TEncoding.UTF8.GetBytes{$else}TBytes{$endif}(Value);
end;

function  TiphSSHChannel.GetPropDataToSendB : TBytes; 
begin
  Result := FDataToSend;
end;

procedure TiphSSHChannel.SetPropDataToSendB(Value: TBytes); 
begin
  FDataToSend := Value;
end;

function TiphSSHChannel.GetPropReadyToSend: Boolean;
begin
  Result := Boolean(FReadyToSend);
end;

procedure TiphSSHChannel.SetPropReadyToSend(Value: Boolean);
begin
  FReadyToSend := Value;
end;

function TiphSSHChannel.GetPropRecordLength: Integer;
begin
  Result := Integer(FRecordLength);
end;

procedure TiphSSHChannel.SetPropRecordLength(Value: Integer);
begin
  FRecordLength := Value;
end;

function TiphSSHChannel.GetPropService: String;
begin
  Result := String(FService);
end;

procedure TiphSSHChannel.SetPropService(Value: String);
begin
  FService := Value;
end;


//////////////////////////////////////////////////////////////////////////////////////////
// TiphSSHChannelList type
constructor TiphSSHChannelList.Create();
begin
  inherited Create;
  FItems := TList.Create;
  FReadOnlyProp := false;
  FCount := 0;
  FOwnerCtl := nil;
end;

constructor TiphSSHChannelList.Create(ReadOnly: Boolean);
begin
  inherited Create;
  FItems := TList.Create;
  FReadOnlyProp := ReadOnly;
  FCount := 0;
  FOwnerCtl := nil;
end;

constructor TiphSSHChannelList.Create(Owner: TObject; ReadOnly: Boolean); 
begin
  inherited Create;
  FItems := TList.Create;
  FReadOnlyProp := ReadOnly;
  FCount := 0;
  FOwnerCtl := Owner;
end;

destructor TiphSSHChannelList.Destroy; 
begin
  ClearList;
  FreeAndNil(FItems);
  inherited;
end;

procedure TiphSSHChannelList.ClearList;
var
  Inst : TiphSSHChannel;
  I: integer;
begin
  try
    for I := 0 to FItems.Count - 1 do
    begin
      Inst := TiphSSHChannel(FItems[I]);
      {$ifndef AUTOREFCOUNT}
      Inst.Free;
      {$else}
      Inst.__ObjRelease;
      Inst.DisposeOf;
      {$endif};
    end;
  finally
    FItems.Clear;
  end;
end;

function TiphSSHChannelList.IsFree: boolean;
begin
  // This method tells the requestor whether the collection object is freestanding or bound
  Result := FOwnerCtl = nil;
end;

procedure TiphSSHChannelList.SyncCount;
var
  Inst : TiphSSHChannel;
  I: integer;
begin
  // This method updates the number of objects held by the collection. It is only relevant for bound collections.
  if not IsFree then
  begin
    FCount := CtlGetCount();
    while FItems.Count < FCount do
    begin
      Inst := CreateElemInstance(FItems.Count);
      FItems.Add(Inst);
      {$ifdef AUTOREFCOUNT}
      Inst.__ObjAddRef;
      {$endif}
    end;
  end;
end;

procedure TiphSSHChannelList.InternalSetCount(Value: integer);
var
  Inst : TiphSSHChannel;
begin
  // This method updates the number of elements in the underlying list. 
  // For both free and bound collections, it will verify that the list has sufficient capacity.
  while FItems.Count < Value do
  begin
    Inst := CreateElemInstance(FItems.Count);
    FItems.Add(Inst);
    {$ifdef AUTOREFCOUNT}
    Inst.__ObjAddRef;
    {$endif}
  end;

  if not IsFree then
    CtlSetCount(Value);
  FCount := Value;
end;

procedure TiphSSHChannelList.InternalSetItem(Index: integer; Value: TiphSSHChannel);
begin
  // This method assigns (effectively copies) an item to collection.
  // It is expected that the boundaries have been checked before this method was called.
  TiphSSHChannel(FItems[Index]).Assign(Value); // Assign, if needed, will redirect any setters to the underlying control's methods
end;

procedure TiphSSHChannelList.Assign(Source: TPersistent);
var
  i: integer;
  Src: TiphSSHChannelList;
begin
  if Source is TiphSSHChannelList then
  begin
    raise Exception.Create('Operation prohibited: Assign is not supported by hash table collections');
    
  end
  else
    inherited;
end;

function TiphSSHChannelList.Clone: TiphSSHChannelList;
begin
  raise Exception.Create('Operation prohibited: Cloning is not supported by hash table collections');

end;

function TiphSSHChannelList.GetCount: Integer;
begin
  SyncCount;
  Result := FCount;
end;

function TiphSSHChannelList.GetItem(Idx: integer): TiphSSHChannel;
begin
  SyncCount;  
  OptionallyAdjustItemList(Idx + 1);

  
  Result := TiphSSHChannel(FItems[Idx]); 
end;

procedure TiphSSHChannelList.SetItem(Idx: integer; Value: TiphSSHChannel);
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OptionallyAdjustItemList(Idx + 1);

  InternalSetItem(Idx, Value);
end;

function TiphSSHChannelList.GetIsReadOnly: boolean;
begin
  Result := FReadOnlyProp;
end;

function TiphSSHChannelList.GetIsFixedSize: boolean;
begin
  Result := false;
end;

function TiphSSHChannelList.GetSyncRoot: TObject;
begin
  raise Exception.Create('Not implemented');
end;

function TiphSSHChannelList.GetIsSynchronized: boolean;
begin
  Result := false;
end;

function TiphSSHChannelList.Add(Value: TiphSSHChannel): Integer;
var
  OldCount: integer;
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OldCount := GetCount();
  InternalSetCount(OldCount + 1);
  InternalSetItem(OldCount, Value);
  Result := OldCount;
end;

function TiphSSHChannelList.Add(): Integer; 
var
  OldCount: integer;
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OldCount := GetCount();
  InternalSetCount(OldCount + 1);
  Result := OldCount;
end;

procedure TiphSSHChannelList.Remove(value: TiphSSHChannel);
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  raise Exception.Create('Not implemented');
end;

function TiphSSHChannelList.Contains(value: TiphSSHChannel): Boolean;
begin
  raise Exception.Create('Not implemented');
end;

procedure TiphSSHChannelList.Add(Index : integer); 
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  
  OptionallyAdjustItemList(Index + 1);
end;

procedure TiphSSHChannelList.Add(Index : integer; value: TiphSSHChannel); 
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;

  OptionallyAdjustItemList(Index + 1);
  InternalSetItem(Index, Value);
end;


procedure TiphSSHChannelList.Clear();
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  InternalSetCount(0);
end;

function TiphSSHChannelList.CtlGetCount: integer;
begin
  ; // to be overridden in derived classes
end;

procedure TiphSSHChannelList.CtlSetCount(Value: integer);
begin
  ; // to be overridden in derived classes
end;

function TiphSSHChannelList.CreateElemInstance(Index: integer): TiphSSHChannel; 
begin
  Result := TiphSSHChannel.Create(FOwnerCtl, FReadOnlyProp);
  Result.Index := Index;
end;

procedure TiphSSHChannelList.OptionallyAdjustItemList(MaxCount : integer);
const
  HT_MAX_LEN = 1048576;
var
  Inst : TiphSSHChannel;
begin
  // Adjusting our internal list to allow for indices 
  // outside of the collection limits, which can
  // happen for hash table collections.
  if MaxCount > HT_MAX_LEN then // DoS prevention
    raise Exception.Create('Operation prohibited: too many items');
    
  while FItems.Count < MaxCount do
  begin
    Inst := CreateElemInstance(FItems.Count);
    FItems.Add(Inst);
    {$ifdef AUTOREFCOUNT}
    Inst.__ObjAddRef;
    {$endif}
  end;
end;



//////////////////////////////////////////////////////////////////////////////////////////
// TiphSSHConnection type


constructor TiphSSHConnection.Create(Owner: TObject; ReadOnly: boolean);
begin
  inherited Create;
  FOwnerCtl := Owner;
  FReadOnlyProp := ReadOnly;
  FIndex := -1;
end;

destructor TiphSSHConnection.Destroy;
begin
  inherited;
end;

procedure TiphSSHConnection.Assign(Source: TPersistent);
var
  i: integer;
  Src: TiphSSHConnection;
begin
  // Copy contents using setters and getters. No owner or index is copied: this is simply an assignment
  // of a collection of fields to another object, not a creation of a full copy.
              if Source is TiphSSHConnection then
  begin

    SetPropConnected(TiphSSHConnection(Source).GetPropConnected);

    SetPropConnectionId(TiphSSHConnection(Source).GetPropConnectionId);

    SetPropLocalAddress(TiphSSHConnection(Source).GetPropLocalAddress);

    SetPropRemoteHost(TiphSSHConnection(Source).GetPropRemoteHost);

    SetPropRemotePort(TiphSSHConnection(Source).GetPropRemotePort);

    SetPropTimeout(TiphSSHConnection(Source).GetPropTimeout);

  end
  else
    inherited;
end;

function TiphSSHConnection.Clone: TiphSSHConnection;
begin
  // Always clone to a freestanding instance of the base class
  Result := TiphSSHConnection.Create(nil, FReadOnlyProp);
  Result.Assign(Self);
end;

function TiphSSHConnection.GetPropConnected: Boolean;
begin
  Result := Boolean(FConnected);
end;

procedure TiphSSHConnection.SetPropConnected(Value: Boolean);
begin
  FConnected := Value;
end;

function TiphSSHConnection.GetPropConnectionId: String;
begin
  Result := String(FConnectionId);
end;

procedure TiphSSHConnection.SetPropConnectionId(Value: String);
begin
  FConnectionId := Value;
end;

function TiphSSHConnection.GetPropLocalAddress: String;
begin
  Result := String(FLocalAddress);
end;

procedure TiphSSHConnection.SetPropLocalAddress(Value: String);
begin
  FLocalAddress := Value;
end;

function TiphSSHConnection.GetPropRemoteHost: String;
begin
  Result := String(FRemoteHost);
end;

procedure TiphSSHConnection.SetPropRemoteHost(Value: String);
begin
  FRemoteHost := Value;
end;

function TiphSSHConnection.GetPropRemotePort: Integer;
begin
  Result := Integer(FRemotePort);
end;

procedure TiphSSHConnection.SetPropRemotePort(Value: Integer);
begin
  FRemotePort := Value;
end;

function TiphSSHConnection.GetPropTimeout: Integer;
begin
  Result := Integer(FTimeout);
end;

procedure TiphSSHConnection.SetPropTimeout(Value: Integer);
begin
  FTimeout := Value;
end;


//////////////////////////////////////////////////////////////////////////////////////////
// TiphSSHConnectionList type
constructor TiphSSHConnectionList.Create();
begin
  inherited Create;
  FItems := TList.Create;
  FReadOnlyProp := false;
  FCount := 0;
  FOwnerCtl := nil;
end;

constructor TiphSSHConnectionList.Create(ReadOnly: Boolean);
begin
  inherited Create;
  FItems := TList.Create;
  FReadOnlyProp := ReadOnly;
  FCount := 0;
  FOwnerCtl := nil;
end;

constructor TiphSSHConnectionList.Create(Owner: TObject; ReadOnly: Boolean); 
begin
  inherited Create;
  FItems := TList.Create;
  FReadOnlyProp := ReadOnly;
  FCount := 0;
  FOwnerCtl := Owner;
end;

destructor TiphSSHConnectionList.Destroy; 
begin
  ClearList;
  FreeAndNil(FItems);
  inherited;
end;

procedure TiphSSHConnectionList.ClearList;
var
  Inst : TiphSSHConnection;
  I: integer;
begin
  try
    for I := 0 to FItems.Count - 1 do
    begin
      Inst := TiphSSHConnection(FItems[I]);
      {$ifndef AUTOREFCOUNT}
      Inst.Free;
      {$else}
      Inst.__ObjRelease;
      Inst.DisposeOf;
      {$endif};
    end;
  finally
    FItems.Clear;
  end;
end;

function TiphSSHConnectionList.IsFree: boolean;
begin
  // This method tells the requestor whether the collection object is freestanding or bound
  Result := FOwnerCtl = nil;
end;

procedure TiphSSHConnectionList.SyncCount;
var
  Inst : TiphSSHConnection;
  I: integer;
begin
  // This method updates the number of objects held by the collection. It is only relevant for bound collections.
  if not IsFree then
  begin
    FCount := CtlGetCount();
    while FItems.Count < FCount do
    begin
      Inst := CreateElemInstance(FItems.Count);
      FItems.Add(Inst);
      {$ifdef AUTOREFCOUNT}
      Inst.__ObjAddRef;
      {$endif}
    end;
  end;
end;

procedure TiphSSHConnectionList.InternalSetCount(Value: integer);
var
  Inst : TiphSSHConnection;
begin
  // This method updates the number of elements in the underlying list. 
  // For both free and bound collections, it will verify that the list has sufficient capacity.
  while FItems.Count < Value do
  begin
    Inst := CreateElemInstance(FItems.Count);
    FItems.Add(Inst);
    {$ifdef AUTOREFCOUNT}
    Inst.__ObjAddRef;
    {$endif}
  end;

  if not IsFree then
    CtlSetCount(Value);
  FCount := Value;
end;

procedure TiphSSHConnectionList.InternalSetItem(Index: integer; Value: TiphSSHConnection);
begin
  // This method assigns (effectively copies) an item to collection.
  // It is expected that the boundaries have been checked before this method was called.
  TiphSSHConnection(FItems[Index]).Assign(Value); // Assign, if needed, will redirect any setters to the underlying control's methods
end;

procedure TiphSSHConnectionList.Assign(Source: TPersistent);
var
  i: integer;
  Src: TiphSSHConnectionList;
begin
  if Source is TiphSSHConnectionList then
  begin
    raise Exception.Create('Operation prohibited: Assign is not supported by hash table collections');
    
  end
  else
    inherited;
end;

function TiphSSHConnectionList.Clone: TiphSSHConnectionList;
begin
  raise Exception.Create('Operation prohibited: Cloning is not supported by hash table collections');

end;

function TiphSSHConnectionList.GetCount: Integer;
begin
  SyncCount;
  Result := FCount;
end;

function TiphSSHConnectionList.GetItem(Idx: integer): TiphSSHConnection;
begin
  SyncCount;  
  OptionallyAdjustItemList(Idx + 1);

  
  Result := TiphSSHConnection(FItems[Idx]); 
end;

procedure TiphSSHConnectionList.SetItem(Idx: integer; Value: TiphSSHConnection);
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OptionallyAdjustItemList(Idx + 1);

  InternalSetItem(Idx, Value);
end;

function TiphSSHConnectionList.GetIsReadOnly: boolean;
begin
  Result := FReadOnlyProp;
end;

function TiphSSHConnectionList.GetIsFixedSize: boolean;
begin
  Result := false;
end;

function TiphSSHConnectionList.GetSyncRoot: TObject;
begin
  raise Exception.Create('Not implemented');
end;

function TiphSSHConnectionList.GetIsSynchronized: boolean;
begin
  Result := false;
end;

function TiphSSHConnectionList.Add(Value: TiphSSHConnection): Integer;
var
  OldCount: integer;
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OldCount := GetCount();
  InternalSetCount(OldCount + 1);
  InternalSetItem(OldCount, Value);
  Result := OldCount;
end;

function TiphSSHConnectionList.Add(): Integer; 
var
  OldCount: integer;
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OldCount := GetCount();
  InternalSetCount(OldCount + 1);
  Result := OldCount;
end;

procedure TiphSSHConnectionList.Remove(value: TiphSSHConnection);
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  raise Exception.Create('Not implemented');
end;

function TiphSSHConnectionList.Contains(value: TiphSSHConnection): Boolean;
begin
  raise Exception.Create('Not implemented');
end;

procedure TiphSSHConnectionList.Add(Index : integer); 
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  
  OptionallyAdjustItemList(Index + 1);
end;

procedure TiphSSHConnectionList.Add(Index : integer; value: TiphSSHConnection); 
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;

  OptionallyAdjustItemList(Index + 1);
  InternalSetItem(Index, Value);
end;


procedure TiphSSHConnectionList.Clear();
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  InternalSetCount(0);
end;

function TiphSSHConnectionList.CtlGetCount: integer;
begin
  ; // to be overridden in derived classes
end;

procedure TiphSSHConnectionList.CtlSetCount(Value: integer);
begin
  ; // to be overridden in derived classes
end;

function TiphSSHConnectionList.CreateElemInstance(Index: integer): TiphSSHConnection; 
begin
  Result := TiphSSHConnection.Create(FOwnerCtl, FReadOnlyProp);
  Result.Index := Index;
end;

procedure TiphSSHConnectionList.OptionallyAdjustItemList(MaxCount : integer);
const
  HT_MAX_LEN = 1048576;
var
  Inst : TiphSSHConnection;
begin
  // Adjusting our internal list to allow for indices 
  // outside of the collection limits, which can
  // happen for hash table collections.
  if MaxCount > HT_MAX_LEN then // DoS prevention
    raise Exception.Create('Operation prohibited: too many items');
    
  while FItems.Count < MaxCount do
  begin
    Inst := CreateElemInstance(FItems.Count);
    FItems.Add(Inst);
    {$ifdef AUTOREFCOUNT}
    Inst.__ObjAddRef;
    {$endif}
  end;
end;



//////////////////////////////////////////////////////////////////////////////////////////
// TiphSSHPlexOperation type


constructor TiphSSHPlexOperation.Create(Owner: TObject; ReadOnly: boolean);
begin
  inherited Create;
  FOwnerCtl := Owner;
  FReadOnlyProp := ReadOnly;
  FIndex := -1;
end;

destructor TiphSSHPlexOperation.Destroy;
begin
  inherited;
end;

procedure TiphSSHPlexOperation.Assign(Source: TPersistent);
var
  i: integer;
  Src: TiphSSHPlexOperation;
begin
  // Copy contents using setters and getters. No owner or index is copied: this is simply an assignment
  // of a collection of fields to another object, not a creation of a full copy.
    if Source is TiphSSHPlexOperation then
  begin

    SetPropOperationId(TiphSSHPlexOperation(Source).GetPropOperationId);

  end
  else
    inherited;
end;

function TiphSSHPlexOperation.Clone: TiphSSHPlexOperation;
begin
  // Always clone to a freestanding instance of the base class
  Result := TiphSSHPlexOperation.Create(nil, FReadOnlyProp);
  Result.Assign(Self);
end;

function TiphSSHPlexOperation.GetPropOperationId: String;
begin
  Result := String(FOperationId);
end;

procedure TiphSSHPlexOperation.SetPropOperationId(Value: String);
begin
  FOperationId := Value;
end;


//////////////////////////////////////////////////////////////////////////////////////////
// TiphSSHPlexOperationList type
constructor TiphSSHPlexOperationList.Create();
begin
  inherited Create;
  FItems := TList.Create;
  FReadOnlyProp := false;
  FCount := 0;
  FOwnerCtl := nil;
end;

constructor TiphSSHPlexOperationList.Create(ReadOnly: Boolean);
begin
  inherited Create;
  FItems := TList.Create;
  FReadOnlyProp := ReadOnly;
  FCount := 0;
  FOwnerCtl := nil;
end;

constructor TiphSSHPlexOperationList.Create(Owner: TObject; ReadOnly: Boolean); 
begin
  inherited Create;
  FItems := TList.Create;
  FReadOnlyProp := ReadOnly;
  FCount := 0;
  FOwnerCtl := Owner;
end;

destructor TiphSSHPlexOperationList.Destroy; 
begin
  ClearList;
  FreeAndNil(FItems);
  inherited;
end;

procedure TiphSSHPlexOperationList.ClearList;
var
  Inst : TiphSSHPlexOperation;
  I: integer;
begin
  try
    for I := 0 to FItems.Count - 1 do
    begin
      Inst := TiphSSHPlexOperation(FItems[I]);
      {$ifndef AUTOREFCOUNT}
      Inst.Free;
      {$else}
      Inst.__ObjRelease;
      Inst.DisposeOf;
      {$endif};
    end;
  finally
    FItems.Clear;
  end;
end;

function TiphSSHPlexOperationList.IsFree: boolean;
begin
  // This method tells the requestor whether the collection object is freestanding or bound
  Result := FOwnerCtl = nil;
end;

procedure TiphSSHPlexOperationList.SyncCount;
var
  Inst : TiphSSHPlexOperation;
  I: integer;
begin
  // This method updates the number of objects held by the collection. It is only relevant for bound collections.
  if not IsFree then
  begin
    FCount := CtlGetCount();
    while FItems.Count < FCount do
    begin
      Inst := CreateElemInstance(FItems.Count);
      FItems.Add(Inst);
      {$ifdef AUTOREFCOUNT}
      Inst.__ObjAddRef;
      {$endif}
    end;
  end;
end;

procedure TiphSSHPlexOperationList.InternalSetCount(Value: integer);
var
  Inst : TiphSSHPlexOperation;
begin
  // This method updates the number of elements in the underlying list. 
  // For both free and bound collections, it will verify that the list has sufficient capacity.
  while FItems.Count < Value do
  begin
    Inst := CreateElemInstance(FItems.Count);
    FItems.Add(Inst);
    {$ifdef AUTOREFCOUNT}
    Inst.__ObjAddRef;
    {$endif}
  end;

  if not IsFree then
    CtlSetCount(Value);
  FCount := Value;
end;

procedure TiphSSHPlexOperationList.InternalSetItem(Index: integer; Value: TiphSSHPlexOperation);
begin
  // This method assigns (effectively copies) an item to collection.
  // It is expected that the boundaries have been checked before this method was called.
  TiphSSHPlexOperation(FItems[Index]).Assign(Value); // Assign, if needed, will redirect any setters to the underlying control's methods
end;

procedure TiphSSHPlexOperationList.Assign(Source: TPersistent);
var
  i: integer;
  Src: TiphSSHPlexOperationList;
begin
  if Source is TiphSSHPlexOperationList then
  begin
    raise Exception.Create('Operation prohibited: Assign is not supported by hash table collections');
    
  end
  else
    inherited;
end;

function TiphSSHPlexOperationList.Clone: TiphSSHPlexOperationList;
begin
  raise Exception.Create('Operation prohibited: Cloning is not supported by hash table collections');

end;

function TiphSSHPlexOperationList.GetCount: Integer;
begin
  SyncCount;
  Result := FCount;
end;

function TiphSSHPlexOperationList.GetItem(Idx: integer): TiphSSHPlexOperation;
begin
  SyncCount;  
  OptionallyAdjustItemList(Idx + 1);

  
  Result := TiphSSHPlexOperation(FItems[Idx]); 
end;

procedure TiphSSHPlexOperationList.SetItem(Idx: integer; Value: TiphSSHPlexOperation);
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OptionallyAdjustItemList(Idx + 1);

  InternalSetItem(Idx, Value);
end;

function TiphSSHPlexOperationList.GetIsReadOnly: boolean;
begin
  Result := FReadOnlyProp;
end;

function TiphSSHPlexOperationList.GetIsFixedSize: boolean;
begin
  Result := false;
end;

function TiphSSHPlexOperationList.GetSyncRoot: TObject;
begin
  raise Exception.Create('Not implemented');
end;

function TiphSSHPlexOperationList.GetIsSynchronized: boolean;
begin
  Result := false;
end;

function TiphSSHPlexOperationList.Add(Value: TiphSSHPlexOperation): Integer;
var
  OldCount: integer;
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OldCount := GetCount();
  InternalSetCount(OldCount + 1);
  InternalSetItem(OldCount, Value);
  Result := OldCount;
end;

function TiphSSHPlexOperationList.Add(): Integer; 
var
  OldCount: integer;
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OldCount := GetCount();
  InternalSetCount(OldCount + 1);
  Result := OldCount;
end;

procedure TiphSSHPlexOperationList.Remove(value: TiphSSHPlexOperation);
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  raise Exception.Create('Not implemented');
end;

function TiphSSHPlexOperationList.Contains(value: TiphSSHPlexOperation): Boolean;
begin
  raise Exception.Create('Not implemented');
end;

procedure TiphSSHPlexOperationList.Add(Index : integer); 
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  
  OptionallyAdjustItemList(Index + 1);
end;

procedure TiphSSHPlexOperationList.Add(Index : integer; value: TiphSSHPlexOperation); 
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;

  OptionallyAdjustItemList(Index + 1);
  InternalSetItem(Index, Value);
end;


procedure TiphSSHPlexOperationList.Clear();
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  InternalSetCount(0);
end;

function TiphSSHPlexOperationList.CtlGetCount: integer;
begin
  ; // to be overridden in derived classes
end;

procedure TiphSSHPlexOperationList.CtlSetCount(Value: integer);
begin
  ; // to be overridden in derived classes
end;

function TiphSSHPlexOperationList.CreateElemInstance(Index: integer): TiphSSHPlexOperation; 
begin
  Result := TiphSSHPlexOperation.Create(FOwnerCtl, FReadOnlyProp);
  Result.Index := Index;
end;

procedure TiphSSHPlexOperationList.OptionallyAdjustItemList(MaxCount : integer);
const
  HT_MAX_LEN = 1048576;
var
  Inst : TiphSSHPlexOperation;
begin
  // Adjusting our internal list to allow for indices 
  // outside of the collection limits, which can
  // happen for hash table collections.
  if MaxCount > HT_MAX_LEN then // DoS prevention
    raise Exception.Create('Operation prohibited: too many items');
    
  while FItems.Count < MaxCount do
  begin
    Inst := CreateElemInstance(FItems.Count);
    FItems.Add(Inst);
    {$ifdef AUTOREFCOUNT}
    Inst.__ObjAddRef;
    {$endif}
  end;
end;



//////////////////////////////////////////////////////////////////////////////////////////
// TiphSSHPrompt type

constructor TiphSSHPrompt.Create();
begin
  inherited Create;
  FIndex := -1;
  FOwnerCtl := nil;
  FReadOnlyProp := false;
end;

constructor TiphSSHPrompt.Create(valPrompt: String; valEcho: Boolean);
begin
  inherited Create;
  FIndex := -1;
  FOwnerCtl := nil;
  FReadOnlyProp := false;
  FPrompt := valPrompt;

  FEcho := valEcho;

end;


constructor TiphSSHPrompt.Create(Owner: TObject; ReadOnly: boolean);
begin
  inherited Create;
  FOwnerCtl := Owner;
  FReadOnlyProp := ReadOnly;
  FIndex := -1;
end;

destructor TiphSSHPrompt.Destroy;
begin
  inherited;
end;

procedure TiphSSHPrompt.Assign(Source: TPersistent);
var
  i: integer;
  Src: TiphSSHPrompt;
begin
  // Copy contents using setters and getters. No owner or index is copied: this is simply an assignment
  // of a collection of fields to another object, not a creation of a full copy.
      if Source is TiphSSHPrompt then
  begin

    SetPropEcho(TiphSSHPrompt(Source).GetPropEcho);

    SetPropPrompt(TiphSSHPrompt(Source).GetPropPrompt);

  end
  else
    inherited;
end;

function TiphSSHPrompt.Clone: TiphSSHPrompt;
begin
  // Always clone to a freestanding instance of the base class
  Result := TiphSSHPrompt.Create(nil, FReadOnlyProp);
  Result.Assign(Self);
end;

function TiphSSHPrompt.GetPropEcho: Boolean;
begin
  Result := Boolean(FEcho);
end;

procedure TiphSSHPrompt.SetPropEcho(Value: Boolean);
begin
  FEcho := Value;
end;

function TiphSSHPrompt.GetPropPrompt: String;
begin
  Result := String(FPrompt);
end;

procedure TiphSSHPrompt.SetPropPrompt(Value: String);
begin
  FPrompt := Value;
end;


//////////////////////////////////////////////////////////////////////////////////////////
// TiphSSHPromptList type
constructor TiphSSHPromptList.Create();
begin
  inherited Create;
  FItems := TList.Create;
  FReadOnlyProp := false;
  FCount := 0;
  FOwnerCtl := nil;
end;

constructor TiphSSHPromptList.Create(ReadOnly: Boolean);
begin
  inherited Create;
  FItems := TList.Create;
  FReadOnlyProp := ReadOnly;
  FCount := 0;
  FOwnerCtl := nil;
end;

constructor TiphSSHPromptList.Create(Owner: TObject; ReadOnly: Boolean); 
begin
  inherited Create;
  FItems := TList.Create;
  FReadOnlyProp := ReadOnly;
  FCount := 0;
  FOwnerCtl := Owner;
end;

destructor TiphSSHPromptList.Destroy; 
begin
  ClearList;
  FreeAndNil(FItems);
  inherited;
end;

procedure TiphSSHPromptList.ClearList;
var
  Inst : TiphSSHPrompt;
  I: integer;
begin
  try
    for I := 0 to FItems.Count - 1 do
    begin
      Inst := TiphSSHPrompt(FItems[I]);
      {$ifndef AUTOREFCOUNT}
      Inst.Free;
      {$else}
      Inst.__ObjRelease;
      Inst.DisposeOf;
      {$endif};
    end;
  finally
    FItems.Clear;
  end;
end;

function TiphSSHPromptList.IsFree: boolean;
begin
  // This method tells the requestor whether the collection object is freestanding or bound
  Result := FOwnerCtl = nil;
end;

procedure TiphSSHPromptList.SyncCount;
var
  Inst : TiphSSHPrompt;
  I: integer;
begin
  // This method updates the number of objects held by the collection. It is only relevant for bound collections.
  if not IsFree then
  begin
    FCount := CtlGetCount();
    while FItems.Count < FCount do
    begin
      Inst := CreateElemInstance(FItems.Count);
      FItems.Add(Inst);
      {$ifdef AUTOREFCOUNT}
      Inst.__ObjAddRef;
      {$endif}
    end;
  end;
end;

procedure TiphSSHPromptList.InternalSetCount(Value: integer);
var
  Inst : TiphSSHPrompt;
begin
  // This method updates the number of elements in the underlying list. 
  // For both free and bound collections, it will verify that the list has sufficient capacity.
  while FItems.Count < Value do
  begin
    Inst := CreateElemInstance(FItems.Count);
    FItems.Add(Inst);
    {$ifdef AUTOREFCOUNT}
    Inst.__ObjAddRef;
    {$endif}
  end;

  if not IsFree then
    CtlSetCount(Value);
  FCount := Value;
end;

procedure TiphSSHPromptList.InternalSetItem(Index: integer; Value: TiphSSHPrompt);
begin
  // This method assigns (effectively copies) an item to collection.
  // It is expected that the boundaries have been checked before this method was called.
  TiphSSHPrompt(FItems[Index]).Assign(Value); // Assign, if needed, will redirect any setters to the underlying control's methods
end;

procedure TiphSSHPromptList.Assign(Source: TPersistent);
var
  i: integer;
  Src: TiphSSHPromptList;
begin
  if Source is TiphSSHPromptList then
  begin
    Src := TiphSSHPromptList(Source);
    InternalSetCount(Src.GetCount);
    for i := 0 to Src.GetCount - 1 do
      InternalSetItem(i, Src.GetItem(i));
      
  end
  else
    inherited;
end;

function TiphSSHPromptList.Clone: TiphSSHPromptList;
begin
  Result := TiphSSHPromptList.Create(FReadOnlyProp); // When cloning, always creating a freestanding object
  Result.Assign(Self);
  
end;

function TiphSSHPromptList.GetCount: Integer;
begin
  SyncCount;
  Result := FCount;
end;

function TiphSSHPromptList.GetItem(Idx: integer): TiphSSHPrompt;
begin
  SyncCount;  
  
  Result := TiphSSHPrompt(FItems[Idx]); 
end;

procedure TiphSSHPromptList.SetItem(Idx: integer; Value: TiphSSHPrompt);
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  
  if (Idx < 0) or (Idx >= GetCount) then
    raise Exception.Create('Index out of bounds');

  InternalSetItem(Idx, Value);
end;

function TiphSSHPromptList.GetIsReadOnly: boolean;
begin
  Result := FReadOnlyProp;
end;

function TiphSSHPromptList.GetIsFixedSize: boolean;
begin
  Result := false;
end;

function TiphSSHPromptList.GetSyncRoot: TObject;
begin
  raise Exception.Create('Not implemented');
end;

function TiphSSHPromptList.GetIsSynchronized: boolean;
begin
  Result := false;
end;

function TiphSSHPromptList.Add(Value: TiphSSHPrompt): Integer;
var
  OldCount: integer;
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OldCount := GetCount();
  InternalSetCount(OldCount + 1);
  InternalSetItem(OldCount, Value);
  Result := OldCount;
end;

function TiphSSHPromptList.Add(): Integer; 
var
  OldCount: integer;
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OldCount := GetCount();
  InternalSetCount(OldCount + 1);
  Result := OldCount;
end;

procedure TiphSSHPromptList.Remove(value: TiphSSHPrompt);
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  raise Exception.Create('Not implemented');
end;

function TiphSSHPromptList.Contains(value: TiphSSHPrompt): Boolean;
begin
  raise Exception.Create('Not implemented');
end;

function TiphSSHPromptList.IndexOf(value: TiphSSHPrompt): Integer;
begin
  raise Exception.Create('Not implemented');
end;

procedure TiphSSHPromptList.Insert(Idx: Integer; value: TiphSSHPrompt);
var
  OldCount: integer;
  I: integer;
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OldCount := GetCount();
  if (Idx < 0) or (Idx >= OldCount) then
    raise Exception.Create('Index out of bounds');
  InternalSetCount(OldCount + 1);
  for I := OldCount downto Idx + 1 do
    InternalSetItem(I, GetItem(i - 1));
  InternalSetItem(Idx, value);
end;

procedure TiphSSHPromptList.RemoveAt(idx: Integer);
var
  I: integer;
  OldCount: integer;
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OldCount := GetCount();
  if (Idx < 0) or (Idx >= OldCount) then
    raise Exception.Create('Index out of bounds');
  // Only copying the needed subset of elements, without cleaning them up
  for I := Idx to OldCount - 2 do
    InternalSetItem(I, GetItem(I + 1));
  InternalSetCount(OldCount - 1);
end;


procedure TiphSSHPromptList.Clear();
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  InternalSetCount(0);
end;

function TiphSSHPromptList.CtlGetCount: integer;
begin
  ; // to be overridden in derived classes
end;

procedure TiphSSHPromptList.CtlSetCount(Value: integer);
begin
  ; // to be overridden in derived classes
end;

function TiphSSHPromptList.CreateElemInstance(Index: integer): TiphSSHPrompt; 
begin
  Result := TiphSSHPrompt.Create(FOwnerCtl, FReadOnlyProp);
  Result.Index := Index;
end;



//////////////////////////////////////////////////////////////////////////////////////////
// TiphSSHRTChannel type


constructor TiphSSHRTChannel.Create(Owner: TObject; ReadOnly: boolean);
begin
  inherited Create;
  FOwnerCtl := Owner;
  FReadOnlyProp := ReadOnly;
  FIndex := -1;
end;

destructor TiphSSHRTChannel.Destroy;
begin
  inherited;
end;

procedure TiphSSHRTChannel.Assign(Source: TPersistent);
var
  i: integer;
  Src: TiphSSHRTChannel;
begin
  // Copy contents using setters and getters. No owner or index is copied: this is simply an assignment
  // of a collection of fields to another object, not a creation of a full copy.
                      if Source is TiphSSHRTChannel then
  begin

    SetPropBytesSent(TiphSSHRTChannel(Source).GetPropBytesSent);

    SetPropChannelId(TiphSSHRTChannel(Source).GetPropChannelId);

    SetPropConnectedAddress(TiphSSHRTChannel(Source).GetPropConnectedAddress);

    SetPropConnectedPort(TiphSSHRTChannel(Source).GetPropConnectedPort);

    SetPropDataToSendB(TiphSSHRTChannel(Source).GetPropDataToSendB);
    SetPropForwardingHost(TiphSSHRTChannel(Source).GetPropForwardingHost);

    SetPropForwardingPort(TiphSSHRTChannel(Source).GetPropForwardingPort);

    SetPropOriginAddress(TiphSSHRTChannel(Source).GetPropOriginAddress);

    SetPropOriginPort(TiphSSHRTChannel(Source).GetPropOriginPort);

    SetPropService(TiphSSHRTChannel(Source).GetPropService);

  end
  else
    inherited;
end;

function TiphSSHRTChannel.Clone: TiphSSHRTChannel;
begin
  // Always clone to a freestanding instance of the base class
  Result := TiphSSHRTChannel.Create(nil, FReadOnlyProp);
  Result.Assign(Self);
end;

function TiphSSHRTChannel.GetPropBytesSent: Integer;
begin
  Result := Integer(FBytesSent);
end;

procedure TiphSSHRTChannel.SetPropBytesSent(Value: Integer);
begin
  FBytesSent := Value;
end;

function TiphSSHRTChannel.GetPropChannelId: String;
begin
  Result := String(FChannelId);
end;

procedure TiphSSHRTChannel.SetPropChannelId(Value: String);
begin
  FChannelId := Value;
end;

function TiphSSHRTChannel.GetPropConnectedAddress: String;
begin
  Result := String(FConnectedAddress);
end;

procedure TiphSSHRTChannel.SetPropConnectedAddress(Value: String);
begin
  FConnectedAddress := Value;
end;

function TiphSSHRTChannel.GetPropConnectedPort: Integer;
begin
  Result := Integer(FConnectedPort);
end;

procedure TiphSSHRTChannel.SetPropConnectedPort(Value: Integer);
begin
  FConnectedPort := Value;
end;

function TiphSSHRTChannel.GetPropDataToSend: String;
begin
  Result := {$IFDEF UNICODE}TEncoding.UTF8.GetString{$else}string{$endif}(FDataToSend);
end;

procedure TiphSSHRTChannel.SetPropDataToSend(Value: String);
begin
  FDataToSend := {$IFDEF UNICODE}TEncoding.UTF8.GetBytes{$else}TBytes{$endif}(Value);
end;

function  TiphSSHRTChannel.GetPropDataToSendB : TBytes; 
begin
  Result := FDataToSend;
end;

procedure TiphSSHRTChannel.SetPropDataToSendB(Value: TBytes); 
begin
  FDataToSend := Value;
end;

function TiphSSHRTChannel.GetPropForwardingHost: String;
begin
  Result := String(FForwardingHost);
end;

procedure TiphSSHRTChannel.SetPropForwardingHost(Value: String);
begin
  FForwardingHost := Value;
end;

function TiphSSHRTChannel.GetPropForwardingPort: Integer;
begin
  Result := Integer(FForwardingPort);
end;

procedure TiphSSHRTChannel.SetPropForwardingPort(Value: Integer);
begin
  FForwardingPort := Value;
end;

function TiphSSHRTChannel.GetPropOriginAddress: String;
begin
  Result := String(FOriginAddress);
end;

procedure TiphSSHRTChannel.SetPropOriginAddress(Value: String);
begin
  FOriginAddress := Value;
end;

function TiphSSHRTChannel.GetPropOriginPort: Integer;
begin
  Result := Integer(FOriginPort);
end;

procedure TiphSSHRTChannel.SetPropOriginPort(Value: Integer);
begin
  FOriginPort := Value;
end;

function TiphSSHRTChannel.GetPropService: String;
begin
  Result := String(FService);
end;

procedure TiphSSHRTChannel.SetPropService(Value: String);
begin
  FService := Value;
end;


//////////////////////////////////////////////////////////////////////////////////////////
// TiphSSHRTChannelList type
constructor TiphSSHRTChannelList.Create();
begin
  inherited Create;
  FItems := TList.Create;
  FReadOnlyProp := false;
  FCount := 0;
  FOwnerCtl := nil;
end;

constructor TiphSSHRTChannelList.Create(ReadOnly: Boolean);
begin
  inherited Create;
  FItems := TList.Create;
  FReadOnlyProp := ReadOnly;
  FCount := 0;
  FOwnerCtl := nil;
end;

constructor TiphSSHRTChannelList.Create(Owner: TObject; ReadOnly: Boolean); 
begin
  inherited Create;
  FItems := TList.Create;
  FReadOnlyProp := ReadOnly;
  FCount := 0;
  FOwnerCtl := Owner;
end;

destructor TiphSSHRTChannelList.Destroy; 
begin
  ClearList;
  FreeAndNil(FItems);
  inherited;
end;

procedure TiphSSHRTChannelList.ClearList;
var
  Inst : TiphSSHRTChannel;
  I: integer;
begin
  try
    for I := 0 to FItems.Count - 1 do
    begin
      Inst := TiphSSHRTChannel(FItems[I]);
      {$ifndef AUTOREFCOUNT}
      Inst.Free;
      {$else}
      Inst.__ObjRelease;
      Inst.DisposeOf;
      {$endif};
    end;
  finally
    FItems.Clear;
  end;
end;

function TiphSSHRTChannelList.IsFree: boolean;
begin
  // This method tells the requestor whether the collection object is freestanding or bound
  Result := FOwnerCtl = nil;
end;

procedure TiphSSHRTChannelList.SyncCount;
var
  Inst : TiphSSHRTChannel;
  I: integer;
begin
  // This method updates the number of objects held by the collection. It is only relevant for bound collections.
  if not IsFree then
  begin
    FCount := CtlGetCount();
    while FItems.Count < FCount do
    begin
      Inst := CreateElemInstance(FItems.Count);
      FItems.Add(Inst);
      {$ifdef AUTOREFCOUNT}
      Inst.__ObjAddRef;
      {$endif}
    end;
  end;
end;

procedure TiphSSHRTChannelList.InternalSetCount(Value: integer);
var
  Inst : TiphSSHRTChannel;
begin
  // This method updates the number of elements in the underlying list. 
  // For both free and bound collections, it will verify that the list has sufficient capacity.
  while FItems.Count < Value do
  begin
    Inst := CreateElemInstance(FItems.Count);
    FItems.Add(Inst);
    {$ifdef AUTOREFCOUNT}
    Inst.__ObjAddRef;
    {$endif}
  end;

  if not IsFree then
    CtlSetCount(Value);
  FCount := Value;
end;

procedure TiphSSHRTChannelList.InternalSetItem(Index: integer; Value: TiphSSHRTChannel);
begin
  // This method assigns (effectively copies) an item to collection.
  // It is expected that the boundaries have been checked before this method was called.
  TiphSSHRTChannel(FItems[Index]).Assign(Value); // Assign, if needed, will redirect any setters to the underlying control's methods
end;

procedure TiphSSHRTChannelList.Assign(Source: TPersistent);
var
  i: integer;
  Src: TiphSSHRTChannelList;
begin
  if Source is TiphSSHRTChannelList then
  begin
    raise Exception.Create('Operation prohibited: Assign is not supported by hash table collections');
    
  end
  else
    inherited;
end;

function TiphSSHRTChannelList.Clone: TiphSSHRTChannelList;
begin
  raise Exception.Create('Operation prohibited: Cloning is not supported by hash table collections');

end;

function TiphSSHRTChannelList.GetCount: Integer;
begin
  SyncCount;
  Result := FCount;
end;

function TiphSSHRTChannelList.GetItem(Idx: integer): TiphSSHRTChannel;
begin
  SyncCount;  
  OptionallyAdjustItemList(Idx + 1);

  
  Result := TiphSSHRTChannel(FItems[Idx]); 
end;

procedure TiphSSHRTChannelList.SetItem(Idx: integer; Value: TiphSSHRTChannel);
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OptionallyAdjustItemList(Idx + 1);

  InternalSetItem(Idx, Value);
end;

function TiphSSHRTChannelList.GetIsReadOnly: boolean;
begin
  Result := FReadOnlyProp;
end;

function TiphSSHRTChannelList.GetIsFixedSize: boolean;
begin
  Result := false;
end;

function TiphSSHRTChannelList.GetSyncRoot: TObject;
begin
  raise Exception.Create('Not implemented');
end;

function TiphSSHRTChannelList.GetIsSynchronized: boolean;
begin
  Result := false;
end;

function TiphSSHRTChannelList.Add(Value: TiphSSHRTChannel): Integer;
var
  OldCount: integer;
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OldCount := GetCount();
  InternalSetCount(OldCount + 1);
  InternalSetItem(OldCount, Value);
  Result := OldCount;
end;

function TiphSSHRTChannelList.Add(): Integer; 
var
  OldCount: integer;
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  OldCount := GetCount();
  InternalSetCount(OldCount + 1);
  Result := OldCount;
end;

procedure TiphSSHRTChannelList.Remove(value: TiphSSHRTChannel);
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  raise Exception.Create('Not implemented');
end;

function TiphSSHRTChannelList.Contains(value: TiphSSHRTChannel): Boolean;
begin
  raise Exception.Create('Not implemented');
end;

procedure TiphSSHRTChannelList.Add(Index : integer); 
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;
  
  OptionallyAdjustItemList(Index + 1);
end;

procedure TiphSSHRTChannelList.Add(Index : integer; value: TiphSSHRTChannel); 
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  SyncCount;

  OptionallyAdjustItemList(Index + 1);
  InternalSetItem(Index, Value);
end;


procedure TiphSSHRTChannelList.Clear();
begin
  if FReadOnlyProp then
    raise Exception.Create('Operation prohibited: list is read-only.');
  InternalSetCount(0);
end;

function TiphSSHRTChannelList.CtlGetCount: integer;
begin
  ; // to be overridden in derived classes
end;

procedure TiphSSHRTChannelList.CtlSetCount(Value: integer);
begin
  ; // to be overridden in derived classes
end;

function TiphSSHRTChannelList.CreateElemInstance(Index: integer): TiphSSHRTChannel; 
begin
  Result := TiphSSHRTChannel.Create(FOwnerCtl, FReadOnlyProp);
  Result.Index := Index;
end;

procedure TiphSSHRTChannelList.OptionallyAdjustItemList(MaxCount : integer);
const
  HT_MAX_LEN = 1048576;
var
  Inst : TiphSSHRTChannel;
begin
  // Adjusting our internal list to allow for indices 
  // outside of the collection limits, which can
  // happen for hash table collections.
  if MaxCount > HT_MAX_LEN then // DoS prevention
    raise Exception.Create('Operation prohibited: too many items');
    
  while FItems.Count < MaxCount do
  begin
    Inst := CreateElemInstance(FItems.Count);
    FItems.Add(Inst);
    {$ifdef AUTOREFCOUNT}
    Inst.__ObjAddRef;
    {$endif}
  end;
end;



end.
