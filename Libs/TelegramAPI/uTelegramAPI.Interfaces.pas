unit uTelegramAPI.Interfaces;

interface

uses uConsts, System.SysUtils, uClassMessageDTO;

type
  ITelegramAPI = interface
    ['{11C947AD-6E74-47BF-8056-1EAA96D3A925}']
    function SetBotToken(AToken: String): ITelegramAPI;
    function SetUserID(AUserID: String): ITelegramAPI;
    function SendFile(AFileName: String): ITelegramAPI;
    function SendMsg(AMsg: String): ITelegramAPI;
    function SendMsgWithButtons(AMsg: String; AButtons: TTelegramButtons)
      : ITelegramAPI;
    function OnError(AValue: TProcErrorException): ITelegramAPI;
    function GetUpdates(var AValue: TChatMessageDTOList): ITelegramAPI;
    function GetResult(): String;
    function SendLocation(ALatitude, ALongitude: String): ITelegramAPI;
  end;

implementation

end.
