﻿unit uTelegramAPI;

interface

uses
  uTelegramAPI.Interfaces, uConsts, System.Net.HttpClientComponent,
  System.SysUtils, System.Classes, System.Net.Mime, System.JSON,
  uClassMessageDTO, Rest.JSON, Rest.JSON.Types, System.Generics.Collections;

// UnixToDateTime(1605808194) DateUtils

type
  TTelegramAPI = Class(TInterfacedObject, ITelegramAPI)
  private
    FHTTPClient: TNetHTTPClient;
    FBotToken: String;
    FUserID: String;
    FResult: String;
    FProcErrorException: TProcErrorException;
    function _POST(AUrl: String; AData: TStrings): String; overload;
    function _POST(AUrl: String; AData: TMultipartFormData): String; overload;
    function _GET(AUrl: String): String;
    function GetURL(APath: String = ''): String;
    function Ready(): Boolean;
  public
    constructor Create();
    destructor Destroy(); override;
    class function New(): ITelegramAPI;
    function SetBotToken(AToken: String): ITelegramAPI;
    function SetUserID(AUserID: String): ITelegramAPI;
    function SendFile(AFileName: String): ITelegramAPI;
    function SendMsg(AMsg: String): ITelegramAPI;
    function SendMsgWithButtons(AMsg: String; AButtons: TTelegramButtons)
      : ITelegramAPI;
    function OnError(AValue: TProcErrorException): ITelegramAPI;
    function GetResult: String;
    function GetUpdates(var AValue: TChatMessageDTOList): ITelegramAPI;
    function SendLocation(ALatitude, ALongitude: String): ITelegramAPI;
  end;

const
  urlBase = 'https://api.telegram.org/{BOT_TOKEN}';

implementation

{ TTelegramAPI }

constructor TTelegramAPI.Create;
begin
  FHTTPClient := TNetHTTPClient.Create(nil);
  FHTTPClient.ConnectionTimeout := 5000;
  FHTTPClient.ResponseTimeout := 5000;
end;

destructor TTelegramAPI.Destroy;
begin
  FreeAndNil(FHTTPClient);
  inherited;
end;

class function TTelegramAPI.New: ITelegramAPI;
begin
  Result := Self.Create;
end;

function TTelegramAPI.OnError(AValue: TProcErrorException): ITelegramAPI;
begin
  Result := Self;
  FProcErrorException := AValue;
end;

function TTelegramAPI.Ready: Boolean;
begin
  Result := True;

  if FBotToken.IsEmpty then
  begin
    if Assigned(FProcErrorException) then
      FProcErrorException(Exception.Create('BotToken is Empty!'));
    Result := False;
  end
  else if FUserID.IsEmpty then
  begin
    if Assigned(FProcErrorException) then
      FProcErrorException(Exception.Create('UserID is Empty!'));
    Result := False;
  end;
end;

function TTelegramAPI.GetResult: String;
begin
  Result := FResult;
end;

function TTelegramAPI.GetUpdates(var AValue: TChatMessageDTOList): ITelegramAPI;
var
  lArrJSON: TJSONArray;
  I: Byte;
begin
  Result := Self;

  if FBotToken.IsEmpty then
  begin
    if Assigned(FProcErrorException) then
      FProcErrorException(Exception.Create('BotToken is Empty!'));
    Exit;
  end;

  try
    FResult := _GET(GetURL('/getUpdates'));

    lArrJSON := ((TJSONObject.ParseJSONValue(FResult) as TJSONObject)
      .GetValue('result') as TJSONArray);

    if lArrJSON.Count <= 0 then Exit;

    for I := 0 to Pred(lArrJSON.Count) do
      AValue.Add(TJSON.JsonToObject<TChatMessageDTO>(lArrJSON.Items[I].ToJSON));

    lArrJSON.Destroy;
  except
    on E: Exception do
    begin
      if Assigned(FProcErrorException) then
        FProcErrorException(E);
    end;
  end;
end;

function TTelegramAPI.GetURL(APath: String = ''): String;
begin
  Result := EmptyStr;

  try
    Result := urlBase.Replace('{BOT_TOKEN}', FBotToken + APath);
  except
  end;
end;

function TTelegramAPI.SendFile(AFileName: String): ITelegramAPI;
var
  pData: TMultipartFormData;
begin
  Result := Self;
  FHTTPClient.ContentType := 'multipart/form-data';

  pData := TMultipartFormData.Create;
  pData.AddFile('document', AFileName);
  pData.AddField('chat_id', FUserID);

  FResult := _POST(GetURL('/sendDocument'), pData);
end;

function TTelegramAPI.SendLocation(ALatitude, ALongitude: String): ITelegramAPI;
var
  pData: TStrings;
begin
  Result := Self;

  FHTTPClient.ContentType := 'application/json';

  pData := TStringList.Create;
  pData.AddPair('chat_id', FUserID);
  pData.AddPair('latitude', ALatitude);
  pData.AddPair('longitude', ALongitude);

  FResult := _POST(GetURL('/sendLocation'), pData);
end;

function TTelegramAPI.SendMsg(AMsg: String): ITelegramAPI;
var
  pData: TStrings;
begin
  Result := Self;

  FHTTPClient.ContentType := 'application/json';

  pData := TStringList.Create;
  try
    pData.AddPair('chat_id', FUserID);
    pData.AddPair('text', AMsg);
    pData.AddPair('parse_mode', 'HTML');

    FResult := _POST(GetURL('/sendMessage'), pData);
  finally
    pData.Free;
  end;
end;

function TTelegramAPI.SendMsgWithButtons(AMsg: String; AButtons: TTelegramButtons): ITelegramAPI;
var
  lData: TStrings;
  lJsonArr: TJSONArray;
  lButton: TTelegramButton;
begin
  Result := Self;

  if AButtons.Count <= 0 then
    Exit;

  lJsonArr := TJSONArray.Create;

  for lButton in AButtons do
  begin
    lJsonArr.AddElement(TJSONObject.Create.AddPair('text', lButton.Key)
      .AddPair('url', lButton.Value));
  end;

  FHTTPClient.ContentType := 'application/json';

  lData := TStringList.Create;
  lData.AddPair('chat_id', FUserID);
  lData.AddPair('text', AMsg);
  lData.AddPair('reply_markup', '{"inline_keyboard":[' +
    lJsonArr.ToJSON + ']}');

  FResult := _POST(GetURL('/sendMessage'), lData);
end;

function TTelegramAPI.SetBotToken(AToken: String): ITelegramAPI;
begin
  Result := Self;
  FBotToken := 'bot' + AToken;
end;

function TTelegramAPI.SetUserID(AUserID: String): ITelegramAPI;
begin
  Result := Self;
  FUserID := AUserID;
end;

function TTelegramAPI._GET(AUrl: String): String;
begin
  Result := EmptyStr;

  try
    Result := FHTTPClient.Get(AUrl).ContentAsString(TEncoding.UTF8);
  except
    on E: Exception do
    begin
      if Assigned(FProcErrorException) then
        FProcErrorException(E);
    end;
  end;
end;

function TTelegramAPI._POST(AUrl: String; AData: TMultipartFormData): String;
begin
  Result := EmptyStr;

  if not Ready() then
    Exit;

  try
    Result := FHTTPClient.Post(AUrl, AData).ContentAsString(TEncoding.UTF8);
  except
    on E: Exception do
    begin
      if Assigned(FProcErrorException) then
        FProcErrorException(E);
    end;
  end;
end;

function TTelegramAPI._POST(AUrl: String; AData: TStrings): String;
begin
  Result := EmptyStr;

  if not Ready() then
    Exit;

  try
    Result := FHTTPClient.Post(AUrl, AData).ContentAsString(TEncoding.UTF8);
  except
    on E: Exception do
    begin
      if Assigned(FProcErrorException) then
        FProcErrorException(E);
    end;
  end;
end;

end.
