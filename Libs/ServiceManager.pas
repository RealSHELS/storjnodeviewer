unit ServiceManager;

interface

uses
  System.SysUtils, System.Classes,
  Winapi.WinSvc, Winapi.Windows, Winapi.Messages;

type
  TServiceEntry = class(TCollectionItem)
  private
    FServiceName, FDisplayName: String;
    FServiceType, FCurrentState, FControlsAccepted, FWin32ExitCode,
      FServiceSpecificExitCode, FCheckPoint, FWaitHint: DWORD;

    procedure SetStatusFields(const AStatus: TServiceStatus);
  public
    property ServiceName: String read FServiceName;
    property DisplayName: String read FDisplayName;
    property ServiceType: DWORD read FServiceType;
    property CurrentState: DWORD read FCurrentState;
    property ControlsAccepted: DWORD read FControlsAccepted;
    property Win32ExitCode: DWORD read FWin32ExitCode;
    property ServiceSpecificExitCode: DWORD read FServiceSpecificExitCode;
    property CheckPoint: DWORD read FCheckPoint;
    property WaitHint: DWORD read FWaitHint;
  end;

  TServiceEntries = class(TOwnedCollection)
  private
    function GetService(Index: Integer): TServiceEntry;
  public
    function FindService(const AServiceName: String): TServiceEntry;
    function ServiceByName(ServiceName: String): TServiceEntry;
    property Items[index: Integer]: TServiceEntry read GetService; default;
  end;

  { Record used in
    registerservice,
    configservice or
    queryserviceconfig
  }

  TServiceStartupType = (sstAutoDelayed, sstAuto, sstManual, sstDisabled);

  TServiceDescriptor = record
  private
    StartType: DWORD;
    IsDelayed: Boolean;
    FStartupType: TServiceStartupType;

    procedure SetStartupType(AType: TServiceStartupType);
  public
    Name: String;
    DisplayName: String;
    DesiredAccess: DWORD;
    ServiceType: DWORD;
    ErrorControl: DWORD;
    CommandLine: String;
    LoadOrderGroup: String;
    TagID: DWORD;
    Dependencies: String; // Separated by slash signs (/)
    UserName: String;
    Password: String;
    property StartupType: TServiceStartupType read FStartupType
      write SetStartupType;

    function GetStartupTypeAsString: String;
  end;

  TServiceManager = class(TComponent)
  private
    { Private declarations }
    FReconnect: Boolean;
    FMachineName: String;
    FAccess: DWORD;
    FHandle: THandle;
    FDBLock: SC_LOCK;
    FServices: TServiceEntries;
    FAfterRefresh: TNotifyEvent;
    FAfterConnect: TNotifyEvent;
    FRefreshOnConnect: Boolean;
    FBeforeDisConnect: TNotifyEvent;
    function GetConnected: Boolean;
    procedure SetConnected(const Value: Boolean);
    procedure SetMachineName(const Value: string);
  protected
    { Protected declarations }
    procedure Loaded; override;
    Procedure SMError(const Msg: String);
    Procedure CheckConnected(const Msg: String);
    Procedure DoBeforeDisConnect; virtual;
    Procedure DoAfterConnect; virtual;
    Procedure DoAfterRefresh; virtual;
  public
    { Public declarations }
    Constructor Create(AOwner: TComponent); override;
    Destructor Destroy; override;
    Procedure ClearServices;
    Procedure Refresh;
    Procedure Connect;
    Procedure Disconnect;
    function GetServiceHandle(const AServiceName: String;
      SAccess: DWORD): THandle;
    procedure ContinueService(SHandle: THandle); overload;
    procedure ContinueService(const AServiceName: String); overload;
    procedure StartService(SHandle: THandle; Args: TStrings); overload;
    procedure StartService(const AServiceName: String;
      Args: TStrings = nil); overload;
    procedure StopService(const ServiceName: String;
      StopDependent: Boolean = false); overload;
    procedure StopService(SHandle: THandle;
      StopDependent: Boolean = false); overload;
    procedure PauseService(SHandle: THandle); overload;
    procedure PauseService(const AServiceName: String); overload;
    procedure CustomControlService(ServiceName: String;
      ControlCode: DWORD); overload;
    procedure CustomControlService(SHandle: THandle;
      ControlCode: DWORD); overload;
    procedure ListDependentServices(SHandle: THandle; ServiceState: DWORD;
      List: TStrings); overload;
    procedure ListDependentServices(ServiceName: String; ServiceState: DWORD;
      List: TStrings); overload;
    Procedure LockServiceDatabase;
    Procedure UnlockServiceDatabase;
    procedure QueryServiceConfig(SHandle: THandle;
      Var Config: TServiceDescriptor); overload;
    procedure QueryServiceConfig(const AServiceName: String;
      var AConfig: TServiceDescriptor); overload;
    Function RegisterService(Var Desc: TServiceDescriptor): THandle;
    procedure SetStartupType(ServiceName: String; StartupType: DWORD); overload;
    procedure SetStartupType(SHandle: THandle; StartupType: DWORD); overload;
    Procedure UnregisterService(const AServiceName: String);
    procedure ConfigService(SHandle: THandle;
      Config: TServiceDescriptor); overload;
    procedure ConfigService(ServiceName: string;
      Config: TServiceDescriptor); overload;
    procedure RefreshServiceStatus(const AServiceName: String);
    procedure GetServiceStatus(SHandle: THandle;
      Var Status: TServiceStatus); overload;
    procedure GetServiceStatus(ServiceName: String;
      Var Status: TServiceStatus); overload;
    Property Handle: THandle Read FHandle;
    Property Acces: DWORD read FAccess Write FAccess;
    Property Services: TServiceEntries Read FServices;
  published
    { Published declarations }
    Property Connected: Boolean Read GetConnected Write SetConnected;
    Property MachineName: string Read FMachineName Write SetMachineName;
    Property RefreshOnConnect: Boolean Read FRefreshOnConnect
      Write FRefreshOnConnect;
    Property AfterRefresh: TNotifyEvent Read FAfterRefresh Write FAfterRefresh;
    Property AfterConnect: TNotifyEvent Read FAfterConnect Write FAfterConnect;
    Property BeforeDisConnect: TNotifyEvent Read FBeforeDisConnect
      Write FBeforeDisConnect;
  end;

  EServiceManager = Class(Exception);

const
  ServiceTypes: array [0 .. 3] of DWORD = (SERVICE_FILE_SYSTEM_DRIVER,
    SERVICE_KERNEL_DRIVER, SERVICE_WIN32_OWN_PROCESS,
    SERVICE_WIN32_SHARE_PROCESS);
  StartErrors: array [0 .. 3] of DWORD = (SERVICE_ERROR_IGNORE,
    SERVICE_ERROR_NORMAL, SERVICE_ERROR_SEVERE, SERVICE_ERROR_CRITICAL);

function ServiceTypeToString(AType: DWORD): String;
function ServiceStateToString(AState: DWORD): String;
function ControlsAcceptedToString(AValue: DWORD): String;
function IsInteractiveService(AType: DWORD): Boolean;

implementation

resourceString
  SErrConnected =
    'Operation not permitted while connected to Service Control Manager';
  SErrNotConnected = 'Not connected to Service control manager. Cannot %s';
  SErrInvalidControlCode = 'Invalid custom control code : %d';
  SQueryServiceList = 'Query service list';
  SActive = 'Active';
  SInactive = 'Inactive';
  SStopped = 'Stopped';
  SStartPending = 'Start pending';
  SStopPending = 'Stop pending';
  SRunning = 'Running';
  SContinuePending = 'Continue pending';
  SPausePending = 'Pause pending';
  SPaused = 'Paused';
  SUnknownState = 'Unknown State (%d)';
  SUnknownType = 'Unknown type (%d)';
  SStop = 'Stop';
  SPauseContinue = 'Pause/continue';
  SShutDown = 'Shutdown';
  SDeviceDriver = 'Device driver';
  SFileSystemDriver = 'Filesystem driver';
  SAdapter = 'Adapter';
  SRecognizer = 'Recognizer';
  SService = 'Service';
  SSHaredService = 'Service (shared)';
  SErrServiceNotFound = 'Service "%s" not found.';
  SStartupType_SERVICE_AUTO_START = 'Auto';
  SStartupType_SERVICE_DEMAND_START = 'Manual';
  SStartupType_SERVICE_DISABLED = 'Disabled';

function ServiceTypeToString(AType: DWORD): String;
begin
  case (AType and $FF) of
    SERVICE_KERNEL_DRIVER:
      Result := SDeviceDriver;
    SERVICE_FILE_SYSTEM_DRIVER:
      Result := SFileSystemDriver;
    SERVICE_ADAPTER:
      Result := SAdapter;
    SERVICE_RECOGNIZER_DRIVER:
      Result := SRecognizer;
    SERVICE_WIN32_OWN_PROCESS:
      Result := SService;
    SERVICE_WIN32_SHARE_PROCESS:
      Result := SSHaredService;
  else
    Result := Format(SUnknownType, [AType]);
  end;
end;

function ServiceStateToString(AState: DWORD): String;
begin
  case AState of
    SERVICE_STOPPED:
      Result := SStopped;
    SERVICE_START_PENDING:
      Result := SStartPending;
    SERVICE_STOP_PENDING:
      Result := SStopPending;
    SERVICE_RUNNING:
      Result := SRunning;
    SERVICE_CONTINUE_PENDING:
      Result := SContinuePending;
    SERVICE_PAUSE_PENDING:
      Result := SPausePending;
    SERVICE_PAUSED:
      Result := SPaused;
  else
    Result := Format(SUnknownState, [AState]);
  end;
end;

function ControlsAcceptedToString(AValue: DWORD): String;
  procedure AddToResult(S: String);
  begin
    if (Result = '') then
      Result := S
    else
      Result := Result + ',' + S
  end;

begin
  Result := '';
  if (AValue and SERVICE_ACCEPT_STOP) <> 0 then
    AddToResult(SStop);
  if (AValue and SERVICE_ACCEPT_PAUSE_CONTINUE) <> 0 then
    AddToResult(SPauseContinue);
  if (AValue and SERVICE_ACCEPT_SHUTDOWN) <> 0 then
    AddToResult(SShutDown)
end;

function IsInteractiveService(AType: DWORD): Boolean;
begin
  Result := (AType and SERVICE_INTERACTIVE_PROCESS) <> 0;
end;

{ TServiceManager }

procedure TServiceManager.CheckConnected(const Msg: String);
begin
  if not Connected then
    SMError(Format(SErrNotConnected, [Msg]));
end;

procedure TServiceManager.ClearServices;
begin
  FServices.Clear;
end;

procedure TServiceManager.Connect;
var
  P: PChar;
begin
  if (FHandle = 0) then
  begin
    P := nil;
    if (MachineName <> '') then
      P := PChar(MachineName);
    FHandle := OpenSCManager(P, nil, FAccess);
    if (FHandle = 0) then
      RaiseLastOSError;
    DoAfterConnect;
    if RefreshOnConnect then
      Refresh;
  end;
end;

constructor TServiceManager.Create(AOwner: TComponent);
begin
  inherited;
  FServices := TServiceEntries.Create(Self, TServiceEntry);
  FAccess := SC_MANAGER_ALL_ACCESS;
end;

destructor TServiceManager.Destroy;
begin
  FServices.Free;
  inherited;
end;

procedure TServiceManager.Disconnect;
begin
  if (FHandle <> 0) then
  begin
    DoBeforeDisConnect;
    CloseServiceHandle(FHandle);
    FHandle := 0;
  end;
end;

function TServiceManager.GetConnected: Boolean;
begin
  Result := (Handle <> 0);
end;

procedure TServiceManager.Refresh;
type
  TSvcA = array [0 .. 4096] of TEnumServiceStatus;
var
  BytesNeeded, ServicesReturned, ResumeHandle: DWORD;
  E: TServiceEntry;
  i: Integer;
  ServicesArr: TSvcA;
begin
  ClearServices;
  CheckConnected(SQueryServiceList);
  BytesNeeded := 0;
  ServicesReturned := 0;
  ResumeHandle := 0;

  if not EnumServicesStatus(FHandle, SERVICE_WIN32, SERVICE_STATE_ALL,
    @ServicesArr[0], SizeOf(ServicesArr), BytesNeeded, ServicesReturned,
    ResumeHandle) then
    RaiseLastOSError;

  for i := 1 to ServicesReturned do
  begin
    E := FServices.Add as TServiceEntry;
    E.FServiceName := StrPas(ServicesArr[i].lpServiceName);
    E.FDisplayName := StrPas(ServicesArr[i].lpDisplayName);
    E.SetStatusFields(ServicesArr[i].ServiceStatus);
  end;

  DoAfterRefresh;
end;

procedure TServiceManager.SetConnected(const Value: Boolean);
begin
  if (([csLoading, csDesigning] * ComponentState) <> []) then
    FReconnect := Value
  else if Value <> GetConnected then
    if Value then
      Connect
    else
      Disconnect;
end;

procedure TServiceManager.Loaded;
begin
  inherited;
  if FReconnect then
    Connect;
end;

procedure TServiceManager.SetMachineName(const Value: string);
begin
  if Connected then
    SMError(SErrConnected);
  FMachineName := Value;
end;

procedure TServiceManager.SMError(const Msg: String);
begin
  raise EServiceManager.Create(Msg);
end;

procedure TServiceManager.DoAfterConnect;
begin
  if Assigned(FAfterConnect) then
    FAfterConnect(Self);
end;

procedure TServiceManager.DoAfterRefresh;
begin
  if Assigned(FAfterRefresh) then
    FAfterRefresh(Self);
end;

procedure TServiceManager.DoBeforeDisConnect;
begin
  if Assigned(FBeforeDisConnect) then
    FBeforeDisConnect(Self);
end;

function AllocDependencyList(const S: String): PChar;
var
  i, L: Integer;
begin
  Result := nil;
  If (S <> '') then
  begin
    // Double Null terminated list of null-terminated strings.
    L := Length(S);
    Getmem(Result, L + 3);
    Move(S[1], Result^, L + 1); // Move terminating null as well.
    Result[L + 1] := #0;
    Result[L + 2] := #0;
    for i := 0 to L - 1 do
      if Result[i] = '/' then // Change / to #0.
        Result[i] := #0;
  end;
end;

function TServiceManager.RegisterService(var Desc: TServiceDescriptor): THandle;
var
  PDep, PLO, PUser, PPWd: PChar; // We need Nil for some things.
  N, D: String;
begin
  with Desc do
  begin
    N := Name;
    D := DisplayName;
    if (LoadOrderGroup = '') then
      PLO := nil
    else
      PLO := PChar(LoadOrderGroup);
    PPWd := nil;
    PUser := nil;
    if (UserName <> '') then
    begin
      PUser := PChar(UserName);
      if (Password <> '') then
        PPWd := PChar(Password);
    end;
    PDep := AllocDependencyList(Dependencies);
    try
      Result := CreateService(Self.Handle, PChar(N), PChar(D), DesiredAccess,
        ServiceType, StartType, ErrorControl, PChar(CommandLine), PLO, nil,
        PDep, PUser, PPWd);
      If (Result = 0) then
        RaiseLastOSError;
    finally
      if PDep <> nil then
        FreeMem(PDep);
    end;
  end;
end;

procedure TServiceManager.ListDependentServices(ServiceName: String;
  ServiceState: DWORD; List: TStrings);
var
  H: THandle;
begin
  H := OpenService(Handle, PChar(ServiceName), SERVICE_ENUMERATE_DEPENDENTS);
  try
    ListDependentServices(H, ServiceState, List);
  finally
    CloseServiceHandle(H);
  end;
end;

procedure TServiceManager.ListDependentServices(SHandle: THandle;
  ServiceState: DWORD; List: TStrings);
var
  P, E: PEnumServiceStatus;
  i, BytesNeeded, Count: DWORD;
begin
  P := nil;
  List.Clear;
  // If call succeeds with size 0, then there are no dependent services...
  if not EnumDependentServices(SHandle, ServiceState, P, 0, BytesNeeded, Count)
  then
  begin
    if (GetLastError <> ERROR_MORE_DATA) then
      RaiseLastOSError;
    Getmem(P, BytesNeeded);
    try
      if not EnumDependentServices(SHandle, ServiceState, P, BytesNeeded,
        BytesNeeded, Count) Then
        RaiseLastOSError;
      E := P;
      for i := 0 to Count - 1 do
      begin
        List.Add(StrPas(E^.lpServiceName));
        PChar(E) := PChar(E) + SizeOf(TEnumServiceStatus);
      end;
    finally
      FreeMem(P);
    end;
  end;
end;

procedure TServiceManager.StopService(SHandle: THandle;
  StopDependent: Boolean = false);
var
  i: Integer;
  List: TStrings;
  Status: TServiceStatus;
begin
  if not QueryServiceStatus(SHandle, Status) then
    RaiseLastOSError;
  if not(Status.dwCurrentState = SERVICE_STOPPED) then
  begin
    if StopDependent then
    begin
      List := TStringList.Create;
      try
        ListDependentServices(SHandle, SERVICE_ACTIVE, List);
        for i := 0 to List.Count - 1 do
          StopService(List[i], false); // Do not recurse !!
      finally
        List.Free;
      end;
    end;
    if not ControlService(SHandle, SERVICE_CONTROL_STOP, Status) then
      RaiseLastOSError;
  end;
end;

procedure TServiceManager.StopService(const ServiceName: String;
  StopDependent: Boolean = false);
var
  H: THandle;
  A: DWORD;
begin
  A := SERVICE_STOP or SERVICE_QUERY_STATUS;
  if StopDependent then
    A := A or SERVICE_ENUMERATE_DEPENDENTS;
  H := OpenService(Handle, PChar(ServiceName), A);
  try
    StopService(H, StopDependent);
  finally
    CloseServiceHandle(H);
  end;
end;

function TServiceManager.GetServiceHandle(const AServiceName: String;
  SAccess: DWORD): THandle;
begin
  Result := OpenService(Handle, PChar(AServiceName), SAccess);
  if (Result = 0) then
    RaiseLastOSError;
end;

procedure TServiceManager.UnregisterService(const AServiceName: String);
var
  H: THandle;
begin
  StopService(AServiceName, True);
  H := GetServiceHandle(AServiceName, SERVICE_STOP or
    SERVICE_QUERY_STATUS or _DELETE);
  try
    if not DeleteService(H) then
      RaiseLastOSError;
  finally
    CloseServiceHandle(H);
  end;
end;

procedure TServiceManager.PauseService(SHandle: THandle);
var
  Status: TServiceStatus;
begin
  if not ControlService(SHandle, SERVICE_CONTROL_PAUSE, Status) then
    RaiseLastOSError;
end;

procedure TServiceManager.PauseService(const AServiceName: String);
var
  H: THandle;
begin
  H := GetServiceHandle(AServiceName, SERVICE_PAUSE_CONTINUE);
  try
    PauseService(H);
  finally
    CloseServiceHandle(H);
  end;
end;

Procedure TServiceManager.ContinueService(SHandle: THandle);
var
  Status: TServiceStatus;
begin
  if not ControlService(SHandle, SERVICE_CONTROL_CONTINUE, Status) then
    RaiseLastOSError;
end;

procedure TServiceManager.ContinueService(const AServiceName: String);
var
  H: THandle;
begin
  H := GetServiceHandle(AServiceName, SERVICE_PAUSE_CONTINUE);
  try
    ContinueService(H);
  finally
    CloseServiceHandle(H);
  end;
end;

function StringsToPCharList(List: TStrings): PPChar;
var
  i: Integer;
  S: String;
begin
  i := (List.Count) + 1;
  Getmem(Result, i * SizeOf(PChar));
  PPCharArray(Result)^[List.Count] := nil;
  for i := 0 to List.Count - 1 do
  begin
    S := List[i];
    PPCharArray(Result)^[i] := StrNew(PChar(S));
  end;
end;

procedure FreePCharList(List: PPChar);
var
  i: Integer;
begin
  i := 0;
  while PPCharArray(List)[i] <> nil do
  begin
    StrDispose(PPCharArray(List)[i]);
    Inc(i);
  end;
  FreeMem(List);
end;

procedure TServiceManager.StartService(SHandle: THandle; Args: TStrings);
var
  Argc: DWORD;
  PArgs: PPChar;
  SD: TServiceDescriptor;
begin
  QueryServiceConfig(SHandle, SD);
  if SD.StartupType = sstDisabled then
    Exit;

  if (Args = nil) or (Args.Count = 0) then
  begin
    Argc := 0;
    PArgs := nil;
  end
  else
  begin
    Argc := Args.Count;
    PArgs := StringsToPCharList(Args);
  end;

  try
    if not Winapi.WinSvc.StartService(SHandle, Argc, PArgs^) then
      RaiseLastOSError;
  finally
    if (PArgs <> nil) then
      FreePCharList(PArgs);
  end;
end;

Procedure TServiceManager.StartService(const AServiceName: String;
  Args: TStrings = nil);
var
  H: THandle;
begin
  H := GetServiceHandle(AServiceName, SERVICE_ALL_ACCESS);
  try
    StartService(H, Args);
  finally
    CloseServiceHandle(H);
  end;
end;

procedure TServiceManager.LockServiceDatabase;
begin
  FDBLock := Winapi.WinSvc.LockServiceDatabase(Handle);
  if FDBLock = nil then
    RaiseLastOSError;
end;

procedure TServiceManager.UnlockServiceDatabase;
begin
  if (FDBLock <> nil) then
  begin
    try
      if not Winapi.WinSvc.UnlockServiceDatabase(FDBLock) then
        RaiseLastOSError;
    finally
      FDBLock := nil;
    end;
  end;
end;

procedure TServiceManager.QueryServiceConfig(SHandle: THandle;
  Var Config: TServiceDescriptor);
var
  SvcCfg: PQueryServiceConfig;
  BytesNeeded: DWORD;
  IsDelayedRec: SERVICE_DELAYED_AUTO_START_INFO;
begin
  Winapi.WinSvc.QueryServiceConfig(SHandle, Nil, 0, BytesNeeded);
  If (GetLastError <> ERROR_INSUFFICIENT_BUFFER) then
    RaiseLastOSError;
  Getmem(SvcCfg, BytesNeeded);
  Try
    if not Winapi.WinSvc.QueryServiceConfig(SHandle, SvcCfg, BytesNeeded,
      BytesNeeded) then
      RaiseLastOSError;
    With Config, SvcCfg^ do
    begin
      Password := '';
      Name := '';
      DesiredAccess := 0;
      ErrorControl := dwErrorControl;
      ServiceType := dwServiceType;
      StartType := dwStartType;
      TagID := dwTagID;
      CommandLine := lpBinaryPathName;
      LoadOrderGroup := lpLoadOrderGroup;
      Dependencies := lpDependencies;
      UserName := lpServiceStartName;
      DisplayName := lpDisplayName;
    end;
  Finally
    FreeMem(SvcCfg, BytesNeeded);
  end;

  if Config.StartType = SERVICE_AUTO_START then
  begin
    if not Winapi.WinSvc.QueryServiceConfig2(SHandle,
      SERVICE_CONFIG_DELAYED_AUTO_START_INFO, @IsDelayedRec,
      SizeOf(IsDelayedRec), @BytesNeeded) then
      RaiseLastOSError;

    Config.IsDelayed := IsDelayedRec.fDelayedAutostart;
  end
  else
    Config.IsDelayed := false;

  case Config.StartType of
    SERVICE_AUTO_START:
      begin
        if Config.IsDelayed then
          Config.FStartupType := sstAutoDelayed
        else
          Config.FStartupType := sstAuto;
      end;
    SERVICE_DEMAND_START:
      Config.FStartupType := sstManual;
    SERVICE_DISABLED:
      Config.FStartupType := sstDisabled;
  end;
end;

procedure TServiceManager.QueryServiceConfig(const AServiceName: String;
  var AConfig: TServiceDescriptor);
var
  H: THandle;
begin
  H := GetServiceHandle(AServiceName, SERVICE_QUERY_CONFIG);
  try
    QueryServiceConfig(H, AConfig);
  finally
    CloseServiceHandle(H);
  end;
end;

procedure TServiceManager.SetStartupType(ServiceName: String;
  StartupType: DWORD);
var
  H: THandle;
begin
  H := GetServiceHandle(ServiceName, SERVICE_CHANGE_CONFIG);
  try
    SetStartupType(H, StartupType);
  finally
    CloseServiceHandle(H);
  end;
end;

procedure TServiceManager.SetStartupType(SHandle: THandle; StartupType: DWORD);
begin
  if not ChangeServiceConfig(SHandle, SERVICE_NO_CHANGE, StartupType,
    SERVICE_NO_CHANGE, nil, nil, nil, nil, nil, nil, nil) then
    RaiseLastOSError;
end;

procedure TServiceManager.ConfigService(SHandle: THandle;
  Config: TServiceDescriptor);
type
  SERVICE_DELAYED_AUTO_START_INFO2 = record
    fDelayedAutostart: Integer;
  end;

  function SToPchar(var S: String): PChar;
  begin
    if (S = '') then
      Result := nil
    else
      Result := PChar(S);
  end;

Var
  PDep, PLO, PUser, PPWd, PCmd, PDisp: PChar; // We need Nil for some things.
  D: String;
  IsDelayedRec: SERVICE_DELAYED_AUTO_START_INFO2;
begin
  With Config do
  begin
    PCmd := SToPchar(CommandLine);
    D := DisplayName;
    PDisp := SToPchar(D);
    PLO := SToPchar(LoadOrderGroup);
    PUser := SToPchar(UserName);
    PPWd := SToPchar(Password);
    PDep := AllocDependencyList(Dependencies);
    try
      if not ChangeServiceConfig(SHandle, ServiceType, StartType, ErrorControl,
        PCmd, PLO, nil, PDep, PUser, PPWd, PDisp) then
        RaiseLastOSError;

      if Config.IsDelayed then
        IsDelayedRec.fDelayedAutostart := 1
      else
        IsDelayedRec.fDelayedAutostart := 0;
      if not ChangeServiceConfig2(SHandle,
        SERVICE_CONFIG_DELAYED_AUTO_START_INFO, @IsDelayedRec) then
        RaiseLastOSError;
    finally
      if PDep <> nil then
        FreeMem(PDep);
    end;
  end;
end;

procedure TServiceManager.GetServiceStatus(SHandle: THandle;
  Var Status: TServiceStatus);
begin
  if not QueryServiceStatus(SHandle, Status) then
    RaiseLastOSError;
end;

procedure TServiceManager.GetServiceStatus(ServiceName: String;
  Var Status: TServiceStatus);
var
  H: THandle;
begin
  H := GetServiceHandle(ServiceName, SERVICE_QUERY_STATUS);
  Try
    GetServiceStatus(H, Status);
  Finally
    CloseServiceHandle(H);
  end;
end;

procedure TServiceManager.RefreshServiceStatus(const AServiceName: String);
var
  Status: TServiceStatus;
  SE: TServiceEntry;
begin
  SE := Services.ServiceByName(AServiceName);
  GetServiceStatus(AServiceName, Status);
  SE.SetStatusFields(Status);
end;

procedure TServiceManager.ConfigService(ServiceName: String;
  Config: TServiceDescriptor);
var
  H: THandle;
begin
  H := GetServiceHandle(ServiceName, SERVICE_CHANGE_CONFIG);
  try
    ConfigService(H, Config);
  finally
    CloseServiceHandle(H);
  end;
end;

procedure TServiceManager.CustomControlService(ServiceName: String;
  ControlCode: DWORD);
var
  H: THandle;
begin
  H := GetServiceHandle(ServiceName, SERVICE_USER_DEFINED_CONTROL);
  try
    CustomControlService(H, ControlCode);
  finally
    CloseServiceHandle(H);
  end;
end;

procedure TServiceManager.CustomControlService(SHandle: THandle;
  ControlCode: DWORD);
var
  Status: TServiceStatus;
begin
  if (ControlCode < 128) or (ControlCode > 255) then
    raise EServiceManager.CreateFmt(SErrInvalidControlCode, [ControlCode]);
  if not ControlService(SHandle, ControlCode, Status) then
    RaiseLastOSError;
end;

{ TServiceEntries }

function TServiceEntries.FindService(const AServiceName: String): TServiceEntry;
var
  i: Integer;
begin
  i := Count - 1;
  Result := nil;

  while (i >= 0) and (Result = nil) do
    if SameText(Items[i].ServiceName, AServiceName) then
      Result := Items[i]
    else
      i := i - 1;
end;

function TServiceEntries.GetService(Index: Integer): TServiceEntry;
begin
  Result := inherited Items[Index] as TServiceEntry;
end;

function TServiceEntries.ServiceByName(ServiceName: String): TServiceEntry;
begin
  Result := FindService(ServiceName);
  if Result = nil then
    raise EServiceManager.CreateFmt(SErrServiceNotFound, [ServiceName]);
end;

{ TServiceEntry }

procedure TServiceEntry.SetStatusFields(const AStatus: TServiceStatus);
begin
  with AStatus do
  begin
    FServiceType := dwServiceType;
    FCurrentState := dwCurrentState;
    FControlsAccepted := dwControlsAccepted;
    FWin32ExitCode := dwWin32ExitCode;
    FServiceSpecificExitCode := dwServiceSpecificExitCode;
    FCheckPoint := dwCheckPoint;
    FWaitHint := dwWaitHint;
  end;
end;

{ TServiceDescriptor }

procedure TServiceDescriptor.SetStartupType(AType: TServiceStartupType);
begin
  IsDelayed := false;
  case AType of
    sstAuto:
      StartType := SERVICE_AUTO_START;
    sstAutoDelayed:
      begin
        IsDelayed := True;
        StartType := SERVICE_AUTO_START;
      end;
    sstManual:
      StartType := SERVICE_DEMAND_START;
    sstDisabled:
      StartType := SERVICE_DISABLED;
  end;
  FStartupType := AType;
end;

function TServiceDescriptor.GetStartupTypeAsString: String;
begin
  Result := '';
  case FStartupType of
    sstAutoDelayed:
      Result := 'Auto (delayed start)';
    sstAuto:
      Result := 'Auto';
    sstManual:
      Result := 'Manual';
    sstDisabled:
      Result := 'Disabled';
  end;
end;

end.
