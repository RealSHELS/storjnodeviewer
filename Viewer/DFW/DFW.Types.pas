unit DFW.Types;

interface

uses
  System.SysUtils, System.IOUtils, System.Types,
  System.Generics.Defaults, System.Generics.Collections,
  {$IFDEF MSWINDOWS}
  System.Classes,
  WinAPI.Windows,
  {$ENDIF}
  FireDac.Comp.Client,
  Data.DB;

type
  EDFWException = class(Exception);

  TSQLFile = record
    FileName: String;
    FileVersion: Integer;
    IsFile: Boolean;
  end;

  IDFW_DB_Worker = interface
  ['{30830504-C7D3-400C-9CF6-2042D5CFDE7E}']
    function GetVersion: Integer;
    procedure SetVersion(AVersion: Integer);
  end;

  TDFW_DB_Worker_Base = class abstract (TInterfacedObject, IDFW_DB_Worker)
  private
    FQuery: TFDQuery;

    function IsTableExists: Boolean; virtual; abstract;
    procedure CreateTable;
  public
    function GetVersion: Integer;
    procedure SetVersion(AVersion: Integer);

    constructor Create(AConnection: TFDCustomConnection);
    destructor Destroy; override;
  end;

  TDFW_DB_Worker_PG = class(TDFW_DB_Worker_Base)
  private
    function IsTableExists: Boolean; override;
  end;

  TDFW_DB_Worker_MSSQL = class(TDFW_DB_Worker_Base)
  private
    function IsTableExists: Boolean; override;
  end;

  TDFW_DB_WOrker_SQLite = class(TDFW_DB_Worker_Base)
  private
    function IsTableExists: Boolean; override;
  end;

  IScriptGetter = interface
    function GetScripts: TList<TSQLFile>;
  end;

  TFileScriptGetter = class (TInterfacedObject, IScriptGetter)
  private
    FPath: String;

    function GetVersionFromFileName(AFileName: String): Integer;
  public
    constructor Create(const APath: String = '');
    function GetScripts: TList<TSQLFile>;
  end;

  {$IFDEF MSWINDOWS}
  TResourceScriptGetter = class (TInterfacedObject, IScriptGetter)
  private
    function GetScriptFromResource(const AResName: String): String;
    function GetVersionFromFileName(AFileName: String): Integer;
  public
    function GetScripts: TList<TSQLFile>;
  end;
  {$ENDIF}

implementation

{$IFDEF MSWINDOWS}
var
  GStrList: TStringList;
{$ENDIF}

{ TDFW_DB_Worker_Base }

constructor TDFW_DB_Worker_Base.Create(AConnection: TFDCustomConnection);
begin
  FQuery := TFDQuery.Create(nil);
  FQuery.Connection := AConnection;
end;

procedure TDFW_DB_Worker_Base.CreateTable;
begin
  FQuery.ExecSQL('CREATE TABLE dfw(version int not null default 0)');
  FQuery.ExecSQL('insert into dfw (version) values (:version)', [0]);
end;

destructor TDFW_DB_Worker_Base.Destroy;
begin
  FQuery.Free;
  inherited;
end;

function TDFW_DB_Worker_Base.GetVersion: Integer;
begin
  Result := 0;
  if IsTableExists then begin
    FQuery.Open('select version from dfw');
    Result := FQuery.FieldByName('version').AsInteger;
  end else
    CreateTable;
end;

procedure TDFW_DB_Worker_Base.SetVersion(AVersion: Integer);
begin
  FQuery.ExecSQL('update dfw set version = :version', [AVersion]);
end;

{ TDFW_DB_Worker_PG }

function TDFW_DB_Worker_PG.IsTableExists: Boolean;
begin
  FQuery.Open('SELECT to_regclass(:table)', ['dfw']);
  Result := FQuery.FieldByName('to_regclass').AsInteger <> 0;
end;

{ TDFW_DB_Worker_MSSQL }

function TDFW_DB_Worker_MSSQL.IsTableExists: Boolean;
begin
  FQuery.Open('SELECT * FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = ' + QuotedStr('dfw'));
  Result := not FQuery.IsEmpty;
end;

{ TDFW_DB_WOrker_SQLite }

function TDFW_DB_WOrker_SQLite.IsTableExists: Boolean;
begin
  FQuery.Open('SELECT name FROM sqlite_master WHERE type=:type AND name=:table', ['table', 'dfw']);
  Result := not FQuery.IsEmpty;
end;

{ TFileScriptGetter }

constructor TFileScriptGetter.Create(const APath: String = '');
begin
  if APath.IsEmpty then
    FPath := ExtractFilePath(ParamStr(0)) + 'dfw'
  else
    FPath := APath;

  FPath := IncludeTrailingPathDelimiter(FPath);
end;

function TFileScriptGetter.GetScripts: TList<TSQLFile>;
var
  ScriptFileArray: TStringDynArray;
  Item: String;
  SQLFile: TSQLFile;
  Comparison: TComparison<TSQLFile>;
begin
  Result := TList<TSQLFile>.Create;
  try
    ScriptFileArray := TDirectory.GetFiles(FPath, 'V*.sql');

    if Length(ScriptFileArray) = 0 then
      Exit;

    for Item in ScriptFileArray do begin
      SQLFile.FileName := Item;
      SQLFile.FileVersion := GetVersionFromFileName(Item);
      SQLFile.IsFile := True;
      Result.Add(SQLFile);
    end;
    Comparison := function(const Left, Right: TSQLFile): Integer
                  begin
                    Result := Left.FileVersion - Right.FileVersion;
                  end;
    Result.Sort(TComparer<TSQLFile>.Construct(Comparison));
  except
    Result.Free;
  end;
end;

function TFileScriptGetter.GetVersionFromFileName(AFileName: String): Integer;
begin
  AFileName := ExtractFileName(AFileName);
  Delete(AFileName, 1, 1);
  AFileName := AFileName.Replace(ExtractFileExt(AFileName), '', [rfIgnoreCase]);
  Result := StrToIntDef(AFileName, -1);
end;

{$IFDEF MSWINDOWS}
{ TResourceScriptGetter }
function EnumResNameProc(lpszName: PChar; lParam: integer; lpszType: PChar; hModule: Cardinal): BOOL;
begin
  GStrList.Add(lpszName);
  Result := True;
end;

function TResourceScriptGetter.GetScriptFromResource(const AResName: String): String;
var
  Res: TResourceStream;
  StrList: TStringList;
begin
  Result := '';

  StrList := nil;
  Res := TResourceStream.Create(HInstance, AResName, RT_RCDATA);
  try
    StrList := TStringList.Create;
    StrList.LoadFromStream(Res);
    Result := StrList.Text;
  finally
    StrList.Free;
    Res.Free;
  end;
end;

function TResourceScriptGetter.GetVersionFromFileName(AFileName: String): Integer;
begin
  //implemented for ResName mask V*_SQL
  Delete(AFileName, Low(AFileName), 1);
  AFileName := AFileName.Replace('_SQL', '', [rfIgnoreCase]);
  Result := StrToIntDef(AFileName, -1);
end;

function TResourceScriptGetter.GetScripts: TList<TSQLFile>;
var
  Item: String;
  SQLFile: TSQLFile;
  Comparison: TComparison<TSQLFile>;
begin
  Result := TList<TSQLFile>.Create;
  try
    GStrList := TStringList.Create;
    try
      EnumResourceNames(0, RT_RCDATA, @EnumResNameProc, 0);

      for Item in GStrList do begin
        SQLFile.FileVersion := GetVersionFromFileName(Item);
        if SQLFile.FileVersion < 0 then
          Continue;
        SQLFile.FileName := GetScriptFromResource(Item);
        SQLFile.IsFile := False;
        Result.Add(SQLFile);
      end;
      Comparison := function(const Left, Right: TSQLFile): Integer
                    begin
                      Result := Left.FileVersion - Right.FileVersion;
                    end;
      Result.Sort(TComparer<TSQLFile>.Construct(Comparison));
    finally
      GStrList.Free;
    end;
  except
    Result.Free;
  end;
end;
{$ENDIF}

end.
