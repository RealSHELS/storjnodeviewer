unit DFW;

interface

uses
  System.SysUtils, System.Generics.Collections,
  DFW.Types,
  FireDac.Comp.Client, FireDac.Comp.Script, Firedac.Comp.ScriptCommands;

  procedure CheckDBUpdates(AConnection: TFDCustomConnection; const APath: String = ''); overload;
  procedure CheckDBUpdates(AConnection: TFDCustomConnection; AScriptGetter: IScriptGetter); overload;

implementation

function RunScript(AFDScript: TFDScript; const ASQLFile: TSQLFile): Boolean;
begin
  AFDScript.SQLScriptFileName := '';
  AFDScript.SQLScripts.Clear;

  if ASQLFile.IsFile then
    AFDScript.SQLScriptFileName := ASQLFile.FileName
  else
    AFDScript.SQLScripts.Add.SQL.Text := ASQLFile.FileName;

  Result := AFDScript.ValidateAll;
  if Result then
    Result := AFDScript.ExecuteAll;
end;

function GetDFW_DB_Worker(AConnection: TFDCustomConnection): IDFW_DB_Worker;
begin
  Result := nil;
  if SameText(AConnection.DriverName, 'PG') then
    Result := TDFW_DB_Worker_PG.Create(AConnection)
  else if SameText(AConnection.DriverName, 'MSSQL') then
    Result := TDFW_DB_Worker_MSSQL.Create(AConnection)
  else if SameText(AConnection.DriverName, 'SQLite') then
    Result := TDFW_DB_Worker_SQLite.Create(AConnection);
end;

procedure CheckDBUpdates(AConnection: TFDCustomConnection; AScriptGetter: IScriptGetter);
var
  List: TList<TSQLFile>;
  DFWVersion: Integer;
  FScript: TFDScript;
  i: integer;
  DFW_DB_Worker: IDFW_DB_Worker;
begin
  if not AConnection.Connected then
    Exit;

  DFW_DB_Worker := GetDFW_DB_Worker(AConnection);
  if DFW_DB_Worker = nil then
    Exit;

  List := nil;
  FScript := TFDScript.Create(nil);
  try
    FScript.Connection := AConnection;
    List := AScriptGetter.GetScripts;
    if not Assigned(List) or (List.Count = 0) then
      Exit;

    DFWVersion := DFW_DB_Worker.GetVersion;
    for i:=0 to List.Count - 1 do
      if (DFWVersion < List[i].FileVersion) then begin
        AConnection.StartTransaction;
        try
          if RunScript(FScript, List[i]) then
            DFW_DB_Worker.SetVersion(List[i].FileVersion)
          else
            raise EDFWException.Create('[EDFWException] Error(s) occured in update local database at script #' + List[i].FileVersion.ToString);

          AConnection.Commit;
        except
          AConnection.Rollback;
          raise;
        end;
      end;
  finally
    FScript.Free;
    List.Free;
  end;
end;

procedure CheckDBUpdates(AConnection: TFDCustomConnection; const APath: String = '');
begin
  CheckDBUpdates(AConnection, TFileScriptGetter.Create(APath));
end;

end.
