program StorjNodeViewer;

{$R *.dres}

uses
  System.SysUtils,
  System.IOUtils,
  System.Classes,
  Vcl.Forms,
  Vcl.Themes,
  Vcl.Styles,
  Winapi.Windows,
  ufrmMain in 'View\Form\ufrmMain.pas' {frmMain},
  DFW in 'DFW\DFW.pas',
  DFW.Types in 'DFW\DFW.Types.pas',
  ufrmTreeSettings in 'View\Form\ufrmTreeSettings.pas' {frmSettings},
  ufrmDailyDataTree in 'View\Form\ufrmDailyDataTree.pas' {frmDailyDataTree},
  uVersionGetter in '..\Shared\uVersionGetter.pas',
  ufrmChangeLog in 'View\Form\ufrmChangeLog.pas' {frmChangeLog},
  AMR.AutoObject in '..\Libs\AutoManagementRecords\AMR.AutoObject.pas',
  ufrmCheckNeighbours in 'View\Form\ufrmCheckNeighbours.pas' {frmCheckNeighbours},
  ufrmSettings in 'View\Form\ufrmSettings.pas' {frmSetting},
  ufrmFinancialDataTree in 'View\Form\ufrmFinancialDataTree.pas' {frmFinancialData},
  ufrmGridView in 'View\Form\ufrmGridView.pas' {frmGridView},
  uDM in 'uDM.pas' {DM: TDataModule},
  ufrmGlobalLog in 'View\Form\ufrmGlobalLog.pas' {frmGlobalLog},
  uframeButtons in 'View\Frame\uframeButtons.pas' {frameButtons: TFrame},
  uNotifyCenter in '..\Libs\uNotifyCenter.pas',
  ufrmHealthTree in 'View\Form\ufrmHealthTree.pas' {frmHealthTree},
  uframeStatusBar in 'View\Frame\uframeStatusBar.pas' {frameStatusBar: TFrame},
  uUptimeRobotAPI in 'uUptimeRobotAPI.pas',
  ufrmNeighboursTree in 'View\Form\ufrmNeighboursTree.pas' {frmNeighboursTree},
  Storj.Consts in 'Storj\Storj.Consts.pas',
  Storj.CurrencyGetter in 'Storj\Storj.CurrencyGetter.pas',
  Storj.InfoGetter in 'Storj\Storj.InfoGetter.pas',
  Storj.NeighboursGetter in 'Storj\Storj.NeighboursGetter.pas',
  Storj.StorjNetworkDataGetter in 'Storj\Storj.StorjNetworkDataGetter.pas',
  Storj.TotalEarnedGetter in 'Storj\Storj.TotalEarnedGetter.pas',
  Storj.Registry in 'Storj\Storj.Registry.pas',
  Storj.Types in 'Storj\Storj.Types.pas',
  Storj.Settings in 'Storj\Storj.Settings.pas',
  ufrmDailyGridView in 'View\Form\ufrmDailyGridView.pas' {frmDailyGridView},
  uframePCRebooterSettings in 'View\Frame\uframePCRebooterSettings.pas' {framePCRebooterSettings: TFrame},
  PCRebooter.ForWebserver in 'Storj\PCRebooter.ForWebserver.pas',
  ufrmPCRebooterAction in 'View\Form\ufrmPCRebooterAction.pas' {frmPCRebooterAction},
  uframePCRebootersStatus in 'View\Frame\uframePCRebootersStatus.pas' {framePCRebootersStatus: TFrame},
  ufrmChart in 'View\Form\ufrmChart.pas' {frmChart},
  Storj.HistoricalDataGetter in 'Storj\Storj.HistoricalDataGetter.pas',
  Storj.CurrentMonthDataGetter in 'Storj\Storj.CurrentMonthDataGetter.pas',
  ufrmRestartUsedDiffer in 'View\Form\ufrmRestartUsedDiffer.pas' {frmRestartUsedDiffer},
  Storj.Common in '..\Shared\Storj.Common.pas',
  ufrmLocalHistory in 'View\Form\ufrmLocalHistory.pas' {frmLocalHistory},
  ufrmDashboard in 'View\Form\ufrmDashboard.pas' {frmDashboard},
  uClassMessageDTO in '..\Libs\TelegramAPI\uClassMessageDTO.pas',
  uConsts in '..\Libs\TelegramAPI\uConsts.pas',
  uTelegramAPI.Interfaces in '..\Libs\TelegramAPI\uTelegramAPI.Interfaces.pas',
  uTelegramAPI in '..\Libs\TelegramAPI\uTelegramAPI.pas',
  Storj.Telegram.Sender in 'Storj\Storj.Telegram.Sender.pas',
  Storj.Email.Sender in 'Storj\Storj.Email.Sender.pas',
  ufrmSSHSettings in 'View\Form\ufrmSSHSettings.pas' {frmSSHSettings},
  Storj.LockBoxAES256PasswordTransformer in 'Storj\Storj.LockBoxAES256PasswordTransformer.pas',
  Storj.Fabrica in 'Storj\Storj.Fabrica.pas',
  ufrmSSHCommandExecute in 'View\Form\ufrmSSHCommandExecute.pas' {frmSSHCommandExecute};

{$R *.res}
const
  TEXT_FOR_MUTEX = 'StorjNodeViewerAppMutex';

begin
  {$IFDEF RELEASE}
  if OpenMutex(MUTEX_ALL_ACCESS, False, TEXT_FOR_MUTEX) <> 0 then
    Exit;

  CreateMutex(nil, False, TEXT_FOR_MUTEX);
  {$ENDIF}

  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  {$IFDEF DEBUG}
    ReportMemoryLeaksOnShutdown := True;
  {$ENDIF}

  for var FileName in ['sqlite3.dll', 'libeay32.dll', 'ssleay32.dll', 'ipworksssh22.dll'] do begin
    var FullFileName := ExtractFilePath(Application.ExeName) + FileName;
    if not TFile.Exists(FullFileName) then begin
      var ResStream := TResourceStream.Create(HInstance, FileName.Replace('.', '_'), RT_RCDATA);
      try
        ResStream.Position := 0;
        ResStream.SaveToFile(FullFileName);
      finally
        ResStream.Free;
      end;
    end;
  end;

  Application.CreateForm(TDM, DM);
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TfrmTreeSettings, frmTreeSettings);
  Application.CreateForm(TfrmDailyDataTree, frmDailyDataTree);
  Application.CreateForm(TfrmChangeLog, frmChangeLog);
  Application.CreateForm(TfrmCheckNeighbours, frmCheckNeighbours);
  Application.CreateForm(TfrmSetting, frmSetting);
  Application.CreateForm(TfrmFinancialData, frmFinancialData);
  Application.CreateForm(TfrmGridView, frmGridView);
  Application.CreateForm(TfrmGlobalLog, frmGlobalLog);
  Application.CreateForm(TfrmHealthTree, frmHealthTree);
  Application.CreateForm(TfrmNeighboursTree, frmNeighboursTree);
  Application.CreateForm(TfrmDailyGridView, frmDailyGridView);
  Application.CreateForm(TfrmPCRebooterAction, frmPCRebooterAction);
  Application.CreateForm(TfrmChart, frmChart);
  Application.CreateForm(TfrmRestartUsedDiffer, frmRestartUsedDiffer);
  Application.CreateForm(TfrmLocalHistory, frmLocalHistory);
  Application.CreateForm(TfrmDashboard, frmDashboard);
  Application.CreateForm(TfrmSSHSettings, frmSSHSettings);
  Application.CreateForm(TfrmSSHCommandExecute, frmSSHCommandExecute);
  DM.LoadNodesData(TGetInfoMode.gimStandart, TLoadSourceMode.lsmManual);

  Application.Run;
end.
