object DM: TDM
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 587
  Width = 819
  object mtblNodeData: TFDMemTable
    Active = True
    FieldDefs = <
      item
        Name = 'Title'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Address'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Port'
        DataType = ftInteger
      end
      item
        Name = 'TBM'
        DataType = ftFloat
      end
      item
        Name = 'Ingress'
        DataType = ftFloat
      end
      item
        Name = 'Egress'
        DataType = ftFloat
      end
      item
        Name = 'RepairIngress'
        DataType = ftFloat
      end
      item
        Name = 'RepairEgress'
        DataType = ftFloat
      end
      item
        Name = 'AllocatedSpace'
        DataType = ftInteger
      end
      item
        Name = 'FreeSpace'
        DataType = ftInteger
      end
      item
        Name = 'UsedSpace'
        DataType = ftInteger
      end
      item
        Name = 'PayoutDirty'
        DataType = ftCurrency
        Precision = 19
      end
      item
        Name = 'PayoutCleanly'
        DataType = ftCurrency
        Precision = 19
      end
      item
        Name = 'EstimatedDirty'
        DataType = ftCurrency
        Precision = 19
      end
      item
        Name = 'EstimatedCleanly'
        DataType = ftCurrency
        Precision = 19
      end
      item
        Name = 'Version'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Efficiency'
        DataType = ftCurrency
        Precision = 19
      end
      item
        Name = 'HDDVolume'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NodeID'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Trash'
        DataType = ftInteger
      end
      item
        Name = 'FullPath'
        DataType = ftString
        Size = 1000
      end
      item
        Name = 'TotalBandwidth'
        DataType = ftFloat
      end
      item
        Name = 'DailyEgress'
        DataType = ftFloat
      end
      item
        Name = 'DailyIngress'
        DataType = ftFloat
      end
      item
        Name = 'DailyRepairIngress'
        DataType = ftFloat
      end
      item
        Name = 'DailyRepairEgress'
        DataType = ftFloat
      end
      item
        Name = 'DailyTotalBandwidth'
        DataType = ftFloat
      end
      item
        Name = 'UptimeMinutes'
        DataType = ftInteger
      end
      item
        Name = 'Wallet'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'DailyTBMonth'
        DataType = ftFloat
      end
      item
        Name = 'EfficiencyByTBM'
        DataType = ftCurrency
        Precision = 19
      end>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 135
    Top = 68
    object mtblNodeDataTitle: TStringField
      FieldName = 'Title'
      Size = 100
    end
    object mtblNodeDataAddress: TStringField
      FieldName = 'Address'
      Size = 100
    end
    object mtblNodeDataPort: TIntegerField
      FieldName = 'Port'
    end
    object mtblNodeDataTBM: TFloatField
      FieldName = 'TBM'
    end
    object mtblNodeDataIngress: TFloatField
      FieldName = 'Ingress'
    end
    object mtblNodeDataEgress: TFloatField
      FieldName = 'Egress'
    end
    object mtblNodeDataRepairIngress: TFloatField
      FieldName = 'RepairIngress'
    end
    object mtblNodeDataRepairEgress: TFloatField
      FieldName = 'RepairEgress'
    end
    object mtblNodeDataAllocatedSpace: TIntegerField
      FieldName = 'AllocatedSpace'
    end
    object mtblNodeDataFreeSpace: TIntegerField
      FieldName = 'FreeSpace'
    end
    object mtblNodeDataUsedSpace: TIntegerField
      FieldName = 'UsedSpace'
    end
    object mtblNodeDataPayoutDirty: TCurrencyField
      FieldName = 'PayoutDirty'
    end
    object mtblNodeDataPayoutCleanly: TCurrencyField
      FieldName = 'PayoutCleanly'
    end
    object mtblNodeDataEstimatedDirty: TCurrencyField
      FieldName = 'EstimatedDirty'
    end
    object mtblNodeDataEstimatedCleanly: TCurrencyField
      FieldName = 'EstimatedCleanly'
    end
    object mtblNodeDataVersion: TStringField
      FieldName = 'Version'
    end
    object mtblNodeDataEfficiency: TCurrencyField
      FieldName = 'Efficiency'
    end
    object mtblNodeDataHDDVolume: TStringField
      FieldName = 'HDDVolume'
    end
    object mtblNodeDataNodeID: TStringField
      FieldName = 'NodeID'
      Size = 100
    end
    object mtblNodeDataTrash: TIntegerField
      FieldName = 'Trash'
    end
    object mtblNodeDataFullPath: TStringField
      FieldName = 'FullPath'
      Size = 1000
    end
    object mtblNodeDataTotalBandwidth: TFloatField
      FieldName = 'TotalBandwidth'
    end
    object mtblNodeDataDailyEgress: TFloatField
      FieldName = 'DailyEgress'
    end
    object mtblNodeDataDailyIngress: TFloatField
      FieldName = 'DailyIngress'
    end
    object mtblNodeDataDailyRepairIngress: TFloatField
      FieldName = 'DailyRepairIngress'
    end
    object mtblNodeDataDailyRepairEgress: TFloatField
      FieldName = 'DailyRepairEgress'
    end
    object mtblNodeDataDailyTotalBandwidth: TFloatField
      FieldName = 'DailyTotalBandwidth'
    end
    object mtblNodeDataUptimeMinutes: TIntegerField
      FieldName = 'UptimeMinutes'
    end
    object mtblNodeDataWallet: TStringField
      FieldName = 'Wallet'
      Size = 255
    end
    object mtblNodeDataDailyTBMonth: TFloatField
      FieldName = 'DailyTBMonth'
    end
    object mtblNodeDataEfficiencyByTBM: TCurrencyField
      FieldName = 'EfficiencyByTBM'
    end
  end
  object mtblChartHistoricalData: TFDMemTable
    Active = True
    FieldDefs = <
      item
        Name = 'Category'
        DataType = ftDateTime
      end
      item
        Name = 'Title'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Address'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Port'
        DataType = ftInteger
      end
      item
        Name = 'RecID'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'ParentRecID'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Payment'
        DataType = ftCurrency
        Precision = 19
      end
      item
        Name = 'TBM'
        DataType = ftFloat
      end
      item
        Name = 'Ingress'
        DataType = ftFloat
      end
      item
        Name = 'Egress'
        DataType = ftFloat
      end
      item
        Name = 'RepairIngress'
        DataType = ftFloat
      end
      item
        Name = 'RepairEgress'
        DataType = ftFloat
      end
      item
        Name = 'Efficiency'
        DataType = ftCurrency
        Precision = 19
      end
      item
        Name = 'Level'
        DataType = ftInteger
      end
      item
        Name = 'TotalEgress'
        DataType = ftFloat
      end
      item
        Name = 'FullPath'
        DataType = ftString
        Size = 255
      end>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 688
    Top = 280
    object mtblChartHistoricalDataCategory: TDateTimeField
      FieldName = 'Category'
    end
    object mtblChartHistoricalDataTitle: TStringField
      FieldName = 'Title'
      Size = 255
    end
    object mtblChartHistoricalDataAddress: TStringField
      FieldName = 'Address'
      Size = 255
    end
    object mtblChartHistoricalDataPort: TIntegerField
      FieldName = 'Port'
    end
    object mtblChartHistoricalDataRecID: TStringField
      FieldName = 'RecID'
      Size = 255
    end
    object mtblChartHistoricalDataParentRecID: TStringField
      FieldName = 'ParentRecID'
      Size = 255
    end
    object mtblChartHistoricalDataPayment: TCurrencyField
      FieldName = 'Payment'
      DisplayFormat = '#0.00 $'
    end
    object mtblChartHistoricalDataTBM: TFloatField
      FieldName = 'TBM'
      DisplayFormat = '#0.00 $'
    end
    object mtblChartHistoricalDataIngress: TFloatField
      FieldName = 'Ingress'
    end
    object mtblChartHistoricalDataEgress: TFloatField
      FieldName = 'Egress'
    end
    object mtblChartHistoricalDataRepairIngress: TFloatField
      FieldName = 'RepairIngress'
    end
    object mtblChartHistoricalDataRepairEgress: TFloatField
      FieldName = 'RepairEgress'
    end
    object mtblChartHistoricalDataEfficiency: TCurrencyField
      FieldName = 'Efficiency'
    end
    object mtblChartHistoricalDataLevel: TIntegerField
      FieldName = 'Level'
    end
    object mtblChartHistoricalDataTotalEgress: TFloatField
      FieldName = 'TotalEgress'
    end
    object mtblChartHistoricalDataFullPath: TStringField
      FieldName = 'FullPath'
      Size = 255
    end
  end
  object mtblChartData: TFDMemTable
    FieldDefs = <>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 536
    Top = 208
  end
  object mtblTreeNodeData: TFDMemTable
    Active = True
    FieldDefs = <
      item
        Name = 'RecID'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'ParentRecID'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Title'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Address'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Port'
        DataType = ftInteger
      end
      item
        Name = 'Level'
        DataType = ftInteger
      end
      item
        Name = 'FullPath'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'IsError'
        DataType = ftBoolean
      end>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 120
    Top = 144
    object mtblTreeNodeDataRecID: TStringField
      FieldName = 'RecID'
      Size = 255
    end
    object mtblTreeNodeDataParentRecID: TStringField
      FieldName = 'ParentRecID'
      Size = 255
    end
    object mtblTreeNodeDataTitle: TStringField
      FieldName = 'Title'
      Size = 255
    end
    object mtblTreeNodeDataAddress: TStringField
      FieldName = 'Address'
      Size = 255
    end
    object mtblTreeNodeDataPort: TIntegerField
      FieldName = 'Port'
    end
    object mtblTreeNodeDataLevel: TIntegerField
      FieldName = 'Level'
    end
    object mtblTreeNodeDataFullPath: TStringField
      FieldName = 'FullPath'
      Size = 255
    end
    object mtblTreeNodeDataIsError: TBooleanField
      FieldName = 'IsError'
    end
  end
  object dtsrcChartData: TDataSource
    DataSet = mtblChartData
    Left = 536
    Top = 288
  end
  object dtsrcNodeData: TDataSource
    DataSet = mtblNodeData
    Left = 48
    Top = 72
  end
  object mtblChartCurrentMonthData: TFDMemTable
    Active = True
    FieldDefs = <
      item
        Name = 'Category'
        DataType = ftDateTime
      end
      item
        Name = 'Title'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Address'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'Port'
        DataType = ftInteger
      end
      item
        Name = 'RecID'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'ParentRecID'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'TBM'
        DataType = ftFloat
      end
      item
        Name = 'Ingress'
        DataType = ftFloat
      end
      item
        Name = 'Egress'
        DataType = ftFloat
      end
      item
        Name = 'RepairIngress'
        DataType = ftFloat
      end
      item
        Name = 'RepairEgress'
        DataType = ftFloat
      end
      item
        Name = 'Level'
        DataType = ftInteger
      end
      item
        Name = 'TotalEgress'
        DataType = ftFloat
      end
      item
        Name = 'TotalIngress'
        DataType = ftFloat
      end
      item
        Name = 'TotalBandwidth'
        DataType = ftFloat
      end
      item
        Name = 'FullPath'
        DataType = ftString
        Size = 255
      end>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 680
    Top = 208
    object DateTimeField1: TDateTimeField
      FieldName = 'Category'
    end
    object StringField1: TStringField
      FieldName = 'Title'
      Size = 255
    end
    object StringField2: TStringField
      FieldName = 'Address'
      Size = 255
    end
    object IntegerField1: TIntegerField
      FieldName = 'Port'
    end
    object StringField3: TStringField
      FieldName = 'RecID'
      Size = 255
    end
    object StringField4: TStringField
      FieldName = 'ParentRecID'
      Size = 255
    end
    object FloatField1: TFloatField
      FieldName = 'TBM'
      DisplayFormat = '#0.00 $'
    end
    object FloatField2: TFloatField
      FieldName = 'Ingress'
    end
    object FloatField3: TFloatField
      FieldName = 'Egress'
    end
    object FloatField4: TFloatField
      FieldName = 'RepairIngress'
    end
    object FloatField5: TFloatField
      FieldName = 'RepairEgress'
    end
    object IntegerField2: TIntegerField
      FieldName = 'Level'
    end
    object FloatField6: TFloatField
      FieldName = 'TotalEgress'
    end
    object mtblChartCurrentMonthDataTotalIngress: TFloatField
      FieldName = 'TotalIngress'
    end
    object mtblChartCurrentMonthDataTotalBandwidth: TFloatField
      FieldName = 'TotalBandwidth'
    end
    object mtblChartCurrentMonthDataFullPath: TStringField
      FieldName = 'FullPath'
      Size = 255
    end
  end
  object mtblRestartUsedDiffer: TFDMemTable
    Active = True
    FieldDefs = <
      item
        Name = 'Title'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'NewUsed'
        DataType = ftInteger
      end
      item
        Name = 'OldUsed'
        DataType = ftInteger
      end
      item
        Name = 'NewUptime'
        DataType = ftInteger
      end
      item
        Name = 'OldUptime'
        DataType = ftInteger
      end
      item
        Name = 'OldTrash'
        DataType = ftInteger
      end
      item
        Name = 'NewTrash'
        DataType = ftInteger
      end>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 72
    Top = 224
    object mtblRestartUsedDifferTitle: TStringField
      FieldName = 'Title'
      Size = 255
    end
    object mtblRestartUsedDifferNewUsed: TIntegerField
      FieldName = 'NewUsed'
    end
    object mtblRestartUsedDifferOldUsed: TIntegerField
      FieldName = 'OldUsed'
    end
    object mtblRestartUsedDifferNewUptime: TIntegerField
      FieldName = 'NewUptime'
    end
    object mtblRestartUsedDifferOldUptime: TIntegerField
      FieldName = 'OldUptime'
    end
    object mtblRestartUsedDifferOldTrash: TIntegerField
      FieldName = 'OldTrash'
    end
    object mtblRestartUsedDifferNewTrash: TIntegerField
      FieldName = 'NewTrash'
    end
  end
  object dtsrcRestartUsedDiffer: TDataSource
    DataSet = mtblRestartUsedDiffer
    Left = 72
    Top = 280
  end
  object FDConnection: TFDConnection
    Params.Strings = (
      'Database=Viewer.db'
      'DriverID=SQLite')
    AfterConnect = FDConnectionAfterConnect
    Left = 560
    Top = 32
  end
  object qryRestartUsedDifferHistory: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'select * from RestartUsedDifferHistory')
    Left = 696
    Top = 32
  end
  object dtsrcRestartUsedDifferHistory: TDataSource
    DataSet = qryRestartUsedDifferHistory
    Left = 696
    Top = 96
  end
  object timerPCRebooters: TTimer
    Interval = 300000
    OnTimer = timerPCRebootersTimer
    Left = 264
    Top = 32
  end
  object qryLocalHistoryDates: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      
        'select LOCAL_HISTORY_ID, STRFTIME('#39'%Y-%m-%d'#39', CREATION_DT) as Da' +
        'te '
      'from LOCAL_HISTORY'
      'order by CREATION_DT desc')
    Left = 504
    Top = 440
  end
  object dtsrcLocalHistoryDates: TDataSource
    DataSet = qryLocalHistoryDates
    Left = 504
    Top = 512
  end
  object mtblLocalHistoryGrid: TFDMemTable
    Active = True
    FieldDefs = <
      item
        Name = 'Title'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Address'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Port'
        DataType = ftInteger
      end
      item
        Name = 'TBM'
        DataType = ftFloat
      end
      item
        Name = 'Ingress'
        DataType = ftFloat
      end
      item
        Name = 'Egress'
        DataType = ftFloat
      end
      item
        Name = 'RepairIngress'
        DataType = ftFloat
      end
      item
        Name = 'RepairEgress'
        DataType = ftFloat
      end
      item
        Name = 'AllocatedSpace'
        DataType = ftInteger
      end
      item
        Name = 'FreeSpace'
        DataType = ftInteger
      end
      item
        Name = 'UsedSpace'
        DataType = ftInteger
      end
      item
        Name = 'PayoutDirty'
        DataType = ftCurrency
        Precision = 19
      end
      item
        Name = 'PayoutCleanly'
        DataType = ftCurrency
        Precision = 19
      end
      item
        Name = 'EstimatedDirty'
        DataType = ftCurrency
        Precision = 19
      end
      item
        Name = 'EstimatedCleanly'
        DataType = ftCurrency
        Precision = 19
      end
      item
        Name = 'Version'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Efficiency'
        DataType = ftCurrency
        Precision = 19
      end
      item
        Name = 'HDDVolume'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NodeID'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Trash'
        DataType = ftInteger
      end
      item
        Name = 'FullPath'
        DataType = ftString
        Size = 1000
      end
      item
        Name = 'TotalBandwidth'
        DataType = ftFloat
      end
      item
        Name = 'DailyEgress'
        DataType = ftFloat
      end
      item
        Name = 'DailyIngress'
        DataType = ftFloat
      end
      item
        Name = 'DailyRepairIngress'
        DataType = ftFloat
      end
      item
        Name = 'DailyRepairEgress'
        DataType = ftFloat
      end
      item
        Name = 'DailyTotalBandwidth'
        DataType = ftFloat
      end
      item
        Name = 'UptimeMinutes'
        DataType = ftInteger
      end
      item
        Name = 'Wallet'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'DailyTBMonth'
        DataType = ftFloat
      end>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 647
    Top = 444
    object StringField5: TStringField
      FieldName = 'Title'
      Size = 100
    end
    object StringField6: TStringField
      FieldName = 'Address'
      Size = 100
    end
    object IntegerField3: TIntegerField
      FieldName = 'Port'
    end
    object FloatField7: TFloatField
      FieldName = 'TBM'
    end
    object FloatField8: TFloatField
      FieldName = 'Ingress'
    end
    object FloatField9: TFloatField
      FieldName = 'Egress'
    end
    object FloatField10: TFloatField
      FieldName = 'RepairIngress'
    end
    object FloatField11: TFloatField
      FieldName = 'RepairEgress'
    end
    object IntegerField4: TIntegerField
      FieldName = 'AllocatedSpace'
    end
    object IntegerField5: TIntegerField
      FieldName = 'FreeSpace'
    end
    object IntegerField6: TIntegerField
      FieldName = 'UsedSpace'
    end
    object CurrencyField1: TCurrencyField
      FieldName = 'PayoutDirty'
    end
    object CurrencyField2: TCurrencyField
      FieldName = 'PayoutCleanly'
    end
    object CurrencyField3: TCurrencyField
      FieldName = 'EstimatedDirty'
    end
    object CurrencyField4: TCurrencyField
      FieldName = 'EstimatedCleanly'
    end
    object StringField7: TStringField
      FieldName = 'Version'
    end
    object CurrencyField5: TCurrencyField
      FieldName = 'Efficiency'
    end
    object StringField8: TStringField
      FieldName = 'HDDVolume'
    end
    object StringField9: TStringField
      FieldName = 'NodeID'
      Size = 100
    end
    object IntegerField7: TIntegerField
      FieldName = 'Trash'
    end
    object StringField10: TStringField
      FieldName = 'FullPath'
      Size = 1000
    end
    object FloatField12: TFloatField
      FieldName = 'TotalBandwidth'
    end
    object FloatField13: TFloatField
      FieldName = 'DailyEgress'
    end
    object FloatField14: TFloatField
      FieldName = 'DailyIngress'
    end
    object FloatField15: TFloatField
      FieldName = 'DailyRepairIngress'
    end
    object FloatField16: TFloatField
      FieldName = 'DailyRepairEgress'
    end
    object FloatField17: TFloatField
      FieldName = 'DailyTotalBandwidth'
    end
    object IntegerField8: TIntegerField
      FieldName = 'UptimeMinutes'
    end
    object StringField11: TStringField
      FieldName = 'Wallet'
      Size = 255
    end
    object FloatField18: TFloatField
      FieldName = 'DailyTBMonth'
    end
  end
  object dtsrcLocalHistoryGrid: TDataSource
    DataSet = mtblLocalHistoryGrid
    Left = 648
    Top = 512
  end
  object tmrReloadNodes: TTimer
    Enabled = False
    OnTimer = tmrReloadNodesTimer
    Left = 384
    Top = 40
  end
  object mtblDashboardSpace: TFDMemTable
    Active = True
    FieldDefs = <
      item
        Name = 'Caption'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Value'
        DataType = ftInteger
      end>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvPersistent, rvSilentMode]
    ResourceOptions.Persistent = True
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 64
    Top = 472
    Content = {
      4144425310000000B0010000FF00010001FF02FF030400240000006D00740062
      006C00440061007300680062006F006100720064005300700061006300650005
      00240000006D00740062006C00440061007300680062006F0061007200640053
      007000610063006500060000000000070000080032000000090000FF0AFF0B04
      000E000000430061007000740069006F006E0005000E00000043006100700074
      0069006F006E000C00010000000E000D000F0064000000100001110001120001
      13000114000115000116000E000000430061007000740069006F006E00170064
      000000FEFF0B04000A000000560061006C007500650005000A00000056006100
      6C00750065000C00020000000E00180010000111000112000113000114000115
      000116000A000000560061006C0075006500FEFEFF19FEFF1AFEFF1BFF1C1D00
      00000000FF1E00000A0000005573656420537061636501002C010000FEFEFF1C
      1D0001000000FF1E0000050000005472617368010008000000FEFEFF1C1D0002
      000000FF1E00000A00000046726565205370616365010064000000FEFEFEFEFE
      FF1FFEFF20210009000000FF22FEFEFE0E004D0061006E006100670065007200
      1E00550070006400610074006500730052006500670069007300740072007900
      12005400610062006C0065004C006900730074000A005400610062006C006500
      08004E0061006D006500140053006F0075007200630065004E0061006D006500
      0A0054006100620049004400240045006E0066006F0072006300650043006F00
      6E00730074007200610069006E00740073001E004D0069006E0069006D007500
      6D0043006100700061006300690074007900180043006800650063006B004E00
      6F0074004E0075006C006C00140043006F006C0075006D006E004C0069007300
      74000C0043006F006C0075006D006E00100053006F0075007200630065004900
      440018006400740041006E007300690053007400720069006E00670010004400
      61007400610054007900700065000800530069007A0065001400530065006100
      720063006800610062006C006500120041006C006C006F0077004E0075006C00
      6C000800420061007300650014004F0041006C006C006F0077004E0075006C00
      6C0012004F0049006E0055007000640061007400650010004F0049006E005700
      68006500720065001A004F0072006900670069006E0043006F006C004E006100
      6D006500140053006F007500720063006500530069007A0065000E0064007400
      49006E007400330032001C0043006F006E00730074007200610069006E007400
      4C00690073007400100056006900650077004C006900730074000E0052006F00
      77004C00690073007400060052006F0077000A0052006F007700490044001000
      4F0072006900670069006E0061006C001800520065006C006100740069006F00
      6E004C006900730074001C0055007000640061007400650073004A006F007500
      72006E0061006C001200530061007600650050006F0069006E0074000E004300
      680061006E00670065007300}
    object mtblDashboardSpaceCaption: TStringField
      FieldName = 'Caption'
      Size = 100
    end
    object mtblDashboardSpaceValue: TIntegerField
      FieldName = 'Value'
    end
  end
  object dtsrcDashboardSpace: TDataSource
    DataSet = mtblDashboardSpace
    Left = 64
    Top = 528
  end
  object mtblChartLocalHistory: TFDMemTable
    Active = True
    FieldDefs = <
      item
        Name = 'Title'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Address'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Port'
        DataType = ftInteger
      end
      item
        Name = 'TBM'
        DataType = ftFloat
      end
      item
        Name = 'Ingress'
        DataType = ftFloat
      end
      item
        Name = 'Egress'
        DataType = ftFloat
      end
      item
        Name = 'RepairIngress'
        DataType = ftFloat
      end
      item
        Name = 'RepairEgress'
        DataType = ftFloat
      end
      item
        Name = 'AllocatedSpace'
        DataType = ftInteger
      end
      item
        Name = 'FreeSpace'
        DataType = ftInteger
      end
      item
        Name = 'UsedSpace'
        DataType = ftInteger
      end
      item
        Name = 'PayoutDirty'
        DataType = ftCurrency
        Precision = 19
      end
      item
        Name = 'PayoutCleanly'
        DataType = ftCurrency
        Precision = 19
      end
      item
        Name = 'EstimatedDirty'
        DataType = ftCurrency
        Precision = 19
      end
      item
        Name = 'EstimatedCleanly'
        DataType = ftCurrency
        Precision = 19
      end
      item
        Name = 'Version'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Efficiency'
        DataType = ftCurrency
        Precision = 19
      end
      item
        Name = 'NodeID'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Trash'
        DataType = ftInteger
      end
      item
        Name = 'FullPath'
        DataType = ftString
        Size = 1000
      end
      item
        Name = 'TotalBandwidth'
        DataType = ftFloat
      end
      item
        Name = 'DailyEgress'
        DataType = ftFloat
      end
      item
        Name = 'DailyIngress'
        DataType = ftFloat
      end
      item
        Name = 'DailyRepairIngress'
        DataType = ftFloat
      end
      item
        Name = 'DailyRepairEgress'
        DataType = ftFloat
      end
      item
        Name = 'DailyTotalBandwidth'
        DataType = ftFloat
      end
      item
        Name = 'UptimeMinutes'
        DataType = ftInteger
      end
      item
        Name = 'DailyTBMonth'
        DataType = ftFloat
      end
      item
        Name = 'Category'
        DataType = ftDateTime
      end>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 687
    Top = 348
    object StringField12: TStringField
      FieldName = 'Title'
      Size = 100
    end
    object StringField13: TStringField
      FieldName = 'Address'
      Size = 100
    end
    object IntegerField9: TIntegerField
      FieldName = 'Port'
    end
    object FloatField19: TFloatField
      FieldName = 'TBM'
    end
    object FloatField20: TFloatField
      FieldName = 'Ingress'
    end
    object FloatField21: TFloatField
      FieldName = 'Egress'
    end
    object FloatField22: TFloatField
      FieldName = 'RepairIngress'
    end
    object FloatField23: TFloatField
      FieldName = 'RepairEgress'
    end
    object IntegerField10: TIntegerField
      FieldName = 'AllocatedSpace'
    end
    object IntegerField11: TIntegerField
      FieldName = 'FreeSpace'
    end
    object IntegerField12: TIntegerField
      FieldName = 'UsedSpace'
    end
    object CurrencyField6: TCurrencyField
      FieldName = 'PayoutDirty'
    end
    object CurrencyField7: TCurrencyField
      FieldName = 'PayoutCleanly'
    end
    object CurrencyField8: TCurrencyField
      FieldName = 'EstimatedDirty'
    end
    object CurrencyField9: TCurrencyField
      FieldName = 'EstimatedCleanly'
    end
    object StringField14: TStringField
      FieldName = 'Version'
    end
    object CurrencyField10: TCurrencyField
      FieldName = 'Efficiency'
    end
    object StringField16: TStringField
      FieldName = 'NodeID'
      Size = 100
    end
    object IntegerField13: TIntegerField
      FieldName = 'Trash'
    end
    object StringField17: TStringField
      FieldName = 'FullPath'
      Size = 1000
    end
    object FloatField24: TFloatField
      FieldName = 'TotalBandwidth'
    end
    object FloatField25: TFloatField
      FieldName = 'DailyEgress'
    end
    object FloatField26: TFloatField
      FieldName = 'DailyIngress'
    end
    object FloatField27: TFloatField
      FieldName = 'DailyRepairIngress'
    end
    object FloatField28: TFloatField
      FieldName = 'DailyRepairEgress'
    end
    object FloatField29: TFloatField
      FieldName = 'DailyTotalBandwidth'
    end
    object IntegerField14: TIntegerField
      FieldName = 'UptimeMinutes'
    end
    object FloatField30: TFloatField
      FieldName = 'DailyTBMonth'
    end
    object mtblChartLocalHistoryCategory: TDateTimeField
      FieldName = 'Category'
    end
  end
  object tmrNodesBuffer: TTimer
    Enabled = False
    Interval = 1500
    OnTimer = tmrNodesBufferTimer
    Left = 264
    Top = 104
  end
end
