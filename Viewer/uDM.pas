unit uDM;

interface

uses
  System.SysUtils, System.Classes, System.Generics.Collections, System.JSON,
  Storj.Types, Storj.Consts, Storj.InfoGetter, uVersionGetter,
  System.Threading, uNotifyCenter, Storj.StorjNetworkDataGetter,
  Storj.Settings, Storj.TotalEarnedGetter, Storj.NeighboursGetter,
  Storj.Registry, System.DateUtils, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  Storj.HistoricalDataGetter, System.StrUtils, System.Variants,
  Storj.CurrentMonthDataGetter, FireDAC.UI.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait,
  FireDAC.Phys.SQLite, FireDAC.Phys.SQLiteDef, FireDAC.Stan.ExprFuncs,
  FireDAC.Phys.SQLiteWrapper.Stat, DFW, DFW.Types, FireDAC.DApt, Vcl.ExtCtrls,
  PCRebooter.ForWebserver, FireDAC.Stan.StorageBin,
  Storj.Telegram.Sender, Storj.Email.Sender, Storj.Fabrica;

type
  TDM = class(TDataModule)
    mtblNodeData: TFDMemTable;
    mtblNodeDataTitle: TStringField;
    mtblNodeDataAddress: TStringField;
    mtblNodeDataPort: TIntegerField;
    mtblNodeDataTBM: TFloatField;
    mtblNodeDataIngress: TFloatField;
    mtblNodeDataEgress: TFloatField;
    mtblNodeDataRepairIngress: TFloatField;
    mtblNodeDataRepairEgress: TFloatField;
    mtblNodeDataAllocatedSpace: TIntegerField;
    mtblNodeDataFreeSpace: TIntegerField;
    mtblNodeDataUsedSpace: TIntegerField;
    mtblNodeDataPayoutDirty: TCurrencyField;
    mtblNodeDataPayoutCleanly: TCurrencyField;
    mtblNodeDataEstimatedDirty: TCurrencyField;
    mtblNodeDataEstimatedCleanly: TCurrencyField;
    mtblNodeDataVersion: TStringField;
    mtblNodeDataEfficiency: TCurrencyField;
    mtblNodeDataHDDVolume: TStringField;
    mtblNodeDataNodeID: TStringField;
    mtblNodeDataTrash: TIntegerField;
    mtblNodeDataFullPath: TStringField;
    mtblNodeDataTotalBandwidth: TFloatField;
    mtblNodeDataDailyEgress: TFloatField;
    mtblNodeDataDailyIngress: TFloatField;
    mtblNodeDataDailyRepairIngress: TFloatField;
    mtblNodeDataDailyRepairEgress: TFloatField;
    mtblNodeDataDailyTotalBandwidth: TFloatField;
    mtblChartHistoricalData: TFDMemTable;
    mtblChartHistoricalDataCategory: TDateTimeField;
    mtblChartHistoricalDataTitle: TStringField;
    mtblChartHistoricalDataAddress: TStringField;
    mtblChartHistoricalDataPort: TIntegerField;
    mtblChartHistoricalDataRecID: TStringField;
    mtblChartHistoricalDataParentRecID: TStringField;
    mtblChartHistoricalDataPayment: TCurrencyField;
    mtblChartHistoricalDataTBM: TFloatField;
    mtblChartHistoricalDataIngress: TFloatField;
    mtblChartHistoricalDataEgress: TFloatField;
    mtblChartHistoricalDataRepairIngress: TFloatField;
    mtblChartHistoricalDataRepairEgress: TFloatField;
    mtblChartData: TFDMemTable;
    mtblChartHistoricalDataEfficiency: TCurrencyField;
    mtblTreeNodeData: TFDMemTable;
    mtblTreeNodeDataRecID: TStringField;
    mtblTreeNodeDataParentRecID: TStringField;
    mtblTreeNodeDataTitle: TStringField;
    mtblTreeNodeDataAddress: TStringField;
    mtblTreeNodeDataPort: TIntegerField;
    dtsrcChartData: TDataSource;
    mtblTreeNodeDataLevel: TIntegerField;
    mtblChartHistoricalDataLevel: TIntegerField;
    mtblChartHistoricalDataTotalEgress: TFloatField;
    dtsrcNodeData: TDataSource;
    mtblChartCurrentMonthData: TFDMemTable;
    DateTimeField1: TDateTimeField;
    StringField1: TStringField;
    StringField2: TStringField;
    IntegerField1: TIntegerField;
    StringField3: TStringField;
    StringField4: TStringField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    FloatField3: TFloatField;
    FloatField4: TFloatField;
    FloatField5: TFloatField;
    IntegerField2: TIntegerField;
    FloatField6: TFloatField;
    mtblChartCurrentMonthDataTotalIngress: TFloatField;
    mtblChartCurrentMonthDataTotalBandwidth: TFloatField;
    mtblNodeDataUptimeMinutes: TIntegerField;
    mtblRestartUsedDiffer: TFDMemTable;
    dtsrcRestartUsedDiffer: TDataSource;
    mtblRestartUsedDifferTitle: TStringField;
    mtblRestartUsedDifferNewUsed: TIntegerField;
    mtblRestartUsedDifferOldUsed: TIntegerField;
    mtblRestartUsedDifferNewUptime: TIntegerField;
    mtblRestartUsedDifferOldUptime: TIntegerField;
    mtblTreeNodeDataFullPath: TStringField;
    mtblChartCurrentMonthDataFullPath: TStringField;
    mtblChartHistoricalDataFullPath: TStringField;
    mtblRestartUsedDifferOldTrash: TIntegerField;
    mtblRestartUsedDifferNewTrash: TIntegerField;
    FDConnection: TFDConnection;
    qryRestartUsedDifferHistory: TFDQuery;
    dtsrcRestartUsedDifferHistory: TDataSource;
    mtblNodeDataWallet: TStringField;
    timerPCRebooters: TTimer;
    mtblNodeDataDailyTBMonth: TFloatField;
    qryLocalHistoryDates: TFDQuery;
    dtsrcLocalHistoryDates: TDataSource;
    mtblLocalHistoryGrid: TFDMemTable;
    StringField5: TStringField;
    StringField6: TStringField;
    IntegerField3: TIntegerField;
    FloatField7: TFloatField;
    FloatField8: TFloatField;
    FloatField9: TFloatField;
    FloatField10: TFloatField;
    FloatField11: TFloatField;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    IntegerField6: TIntegerField;
    CurrencyField1: TCurrencyField;
    CurrencyField2: TCurrencyField;
    CurrencyField3: TCurrencyField;
    CurrencyField4: TCurrencyField;
    StringField7: TStringField;
    CurrencyField5: TCurrencyField;
    StringField8: TStringField;
    StringField9: TStringField;
    IntegerField7: TIntegerField;
    StringField10: TStringField;
    FloatField12: TFloatField;
    FloatField13: TFloatField;
    FloatField14: TFloatField;
    FloatField15: TFloatField;
    FloatField16: TFloatField;
    FloatField17: TFloatField;
    IntegerField8: TIntegerField;
    StringField11: TStringField;
    FloatField18: TFloatField;
    dtsrcLocalHistoryGrid: TDataSource;
    tmrReloadNodes: TTimer;
    mtblDashboardSpace: TFDMemTable;
    dtsrcDashboardSpace: TDataSource;
    mtblDashboardSpaceCaption: TStringField;
    mtblDashboardSpaceValue: TIntegerField;
    mtblNodeDataEfficiencyByTBM: TCurrencyField;
    mtblChartLocalHistory: TFDMemTable;
    StringField12: TStringField;
    StringField13: TStringField;
    IntegerField9: TIntegerField;
    FloatField19: TFloatField;
    FloatField20: TFloatField;
    FloatField21: TFloatField;
    FloatField22: TFloatField;
    FloatField23: TFloatField;
    IntegerField10: TIntegerField;
    IntegerField11: TIntegerField;
    IntegerField12: TIntegerField;
    CurrencyField6: TCurrencyField;
    CurrencyField7: TCurrencyField;
    CurrencyField8: TCurrencyField;
    CurrencyField9: TCurrencyField;
    StringField14: TStringField;
    CurrencyField10: TCurrencyField;
    StringField16: TStringField;
    IntegerField13: TIntegerField;
    StringField17: TStringField;
    FloatField24: TFloatField;
    FloatField25: TFloatField;
    FloatField26: TFloatField;
    FloatField27: TFloatField;
    FloatField28: TFloatField;
    FloatField29: TFloatField;
    IntegerField14: TIntegerField;
    FloatField30: TFloatField;
    mtblChartLocalHistoryCategory: TDateTimeField;
    tmrNodesBuffer: TTimer;
    mtblTreeNodeDataIsError: TBooleanField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure FDConnectionAfterConnect(Sender: TObject);
    procedure timerPCRebootersTimer(Sender: TObject);
    procedure tmrReloadNodesTimer(Sender: TObject);
    procedure tmrNodesBufferTimer(Sender: TObject);
  private
    fNodesJArray: TJSONArray;
    fNodesJArrayActiveOnly: TJSONArray;
    fIsPausedRefresh: Boolean;
    fChartNodeCount: Integer;
    fChartTotalNodeCount: Integer;
    fNodeCount: Integer;
    fSuccessNodeCount: Integer;
    fErrorNodeCount: Integer;
    fTotalNodeCount: Integer;
    fChartWorker: IChartWorker;
    fNotifiedOfflineNodes: TStringList;
    fNotifiedErrorNodes: TStringList;
    fNodesBuffer: TNodeDataArray;
    fLoadSourceMode: TLoadSourceMode;

    function GetNodesList: TNodesList;
    function GetNodesFromBranch(aJArr: TJSONArray; const aParentPath: String = ''): TNodesList;
    procedure NodeDataCallback(ANodeData: TNodeData);
    procedure ErrorNodeDataCallback(ANode: TNode; E: Exception);
    procedure AsyncPause;
    procedure DoFinishReloadNodes;
    procedure DoCleanNodesBuffer;
    procedure SetNodeBoolValueByFieldInJArrayActiveOnly(const aNode: TNode; aValue: Boolean; aField: String);
    function GetJNode(aJArr: TJSONArray; const aFullPath: String): TJSONObject;
    procedure RemoveNonActiveFromJArray(aJArray: TJSONArray);

    procedure ProcessDataForNotify;

    procedure SaveDataToHistory;
    procedure GetChartLocalHistory;

    procedure NodeTotalEarnedDataCallback(ANodeData: TNodeData);
    procedure ErrorTotalEarnedNodeDataCallback(ANode: TNode; E: Exception);

    procedure FillJArrFullPath(aJArr: TJSONArray; const aParentPath: String);

    procedure NodeChartDataCallback(aData: TArray<TChartNodeData>);
    procedure ErrorChartNodeDataCallback(ANode: TNode; E: Exception);
    procedure MakeAndFillHistoricalChartData(const aIsUseCache: Boolean = False);
    procedure FillChartParents(aDataSet: TDataSet);
    procedure CalcChartParents(aDataSet: TFDMemTable);

    procedure AddNodeToDataSet(aDataSet: TDataSet; aJArr: TJSONArray; const aParentPath: String; aLevel: Integer);
//    procedure AddSatelliteField(const aFieldName: String);

    procedure NeighboursDataCallback(const aData: TNeighboursData);
    procedure NeighboursErrorCallback(const aHost: String; E: Exception);

    procedure OnGetStorjGithubVersion(AVersion: String);
    procedure OnGetStorjIOVersion(AStorjIOVersion: TStorjIOVersion);
    procedure OnGetStorjNetworkData(const aData: TStorjNetworkData);

    procedure PCRebooterEchoCallback(aRebooter: TPCRebooterSettings; aRes: Boolean);
    procedure PCRebooterEchoCallbackLogged(aRebooter: TPCRebooterSettings; aRes: Boolean);

    function GetIsLoading: Boolean;
    function GetNodeCaptionForLog(const aNode: TNode): String;

    procedure ProcessDataForDashboard;

    procedure OnCommandSettingsChanged(const aValue: TValue);
  public
    procedure UpdateLastestStorjVersion;
    procedure UpdateStorjNetworkData;
    procedure LoadHistoricalNodesData(aChartWorker: IChartWorker);
    procedure LoadNodesData(const aGetInfoMode: TGetInfoMode; const aLoadSourceMode: TLoadSourceMode);
    procedure LoadNodeData(const aNode: TNode);
    procedure LoadTotalEarned;
    procedure ReloadFromJSON;
    procedure Log(const aStr: String);

    procedure UpdatePCRebooters;
    procedure UpdatePCRebooter(const aRebooter: TPCRebooterSettings);

    procedure LocateDifferHistoryFromCurrent;

    procedure ReloadLocalHistoryDates;
    function LoadHistoryData(aID: Integer): TLocalHistoryData;

    procedure LoadNeighbours;
    function IsCanLoadNeighbours: Boolean;
    function GetFormattedNeighboursLastUpdate: String;

    function GetSSHSettings(const aFullPath: String): TSSHSettings;
    procedure UpsertSSHSettings(aSSHSettings: TSSHSettings);

    function GenGUID: String;
    function GetNodesJArrayActiveOnly: TJSONArray;

    property IsLoading: Boolean read GetIsLoading;
//  public const
//    SATELLITE_COLUMN_NAME_PREFIX = 'Satellite_';
  end;

var
  DM: TDM;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TDM }

procedure TDM.AddNodeToDataSet(aDataSet: TDataSet; aJArr: TJSONArray; const aParentPath: String; aLevel: Integer);
var
  i: integer;
  JObj: TJSONObject;
  JValue: TJSONValue;
  Port: String;
begin
  for i := 0 to aJArr.Count - 1 do begin
    JObj := aJArr.Items[i] as TJSONObject;
    if not JObj.GetValue<Boolean>('IsActive') then
        Continue;

    if (JObj.GetValue<String>('Port', '').Length > 0) or (aDataSet.Name = mtblTreeNodeData.Name) then begin
      aDataSet.Append;
      aDataSet['Title'] := JObj.GetValue<String>('Title');
      aDataSet['Address'] := JObj.GetValue<String>('Address', '');
      aDataSet['FullPath'] := JObj.GetValue<String>('FullPath', '');

      Port := JObj.GetValue<String>('Port', '');
      if Port.Length > 0 then
        aDataSet['Port'] := Port;

      if aDataSet.Name = mtblNodeData.Name then begin
        aDataSet['HDDVolume'] := JObj.GetValue<String>('HDDVolume', '');
      end else begin
        aDataSet['ParentRecID'] := aParentPath;
        aDataSet['RecID'] := JObj.GetValue<String>('FullPath');
        aDataSet['Level'] := aLevel;
        aDataSet['IsError'] := False;
      end;

      aDataSet.Post;
    end;

    JValue := JObj.GetValue<TJSONArray>('Childs', nil);
    if JValue <> nil then
      AddNodeToDataSet(aDataSet, JValue as TJSONArray, JObj.GetValue<String>('FullPath'), aLevel + 1);
  end;
end;

//procedure TDM.AddSatelliteField(const aFieldName: String);
//var
//  TmpMemTable: TFDMemTable;
//  Field : TField;
//begin
//  TmpMemTable := TFDMemTable.Create(nil);
//  try
//    TmpMemTable.CopyDataSet(mtblNodeData, [coStructure, coRestart, coAppend]);
//
//    mtblNodeData.Close;
//    Field := TIntegerField.Create(Self);
//    Field.FieldName := aFieldName;
//    Field.DataSet := mtblNodeData;
//
//    mtblNodeData.CreateDataSet;
//
//    if TmpMemTable.RecordCount > 0 then
//      mtblNodeData.CopyDataSet(TmpMemTable, [coRestart, coAppend]);
//  finally
//    TmpMemTable.Free;
//  end;
//end;

//this procedure call in TTask as async
procedure TDM.AsyncPause;
begin
  Sleep(5000);
  TThread.Queue(nil, procedure
                     begin
                       fIsPausedRefresh := false;
                     end
                );
end;

procedure TDM.CalcChartParents(aDataSet: TFDMemTable);
var
  ParentRecIDList: TList<String>;
  CategoryList: TList<TDateTime>;
  ParentRecID: String;
  Category: TDateTime;

  Payment: Currency;
  TBM, Egress, RepairEgress, TotalEgress: Double;
  Ingress, RepairIngress, TotalIngress, TotalBandiwdth: Double;
  ChartDisplaySettings: TChartDisplaySettings;
begin
  ChartDisplaySettings := fChartWorker.GetChartDisplaySettings;

  CategoryList := nil;
  ParentRecIDList := TList<String>.Create;
  try
    CategoryList := TList<TDateTime>.Create;

    aDataSet.FormatOptions.SortOptions := [soDescending];
    aDataSet.IndexFieldNames := 'Level';
    aDataSet.First;
    while not aDataSet.Eof do begin
      ParentRecID := aDataSet.FieldByName('ParentRecID').AsString;
      Category := aDataSet.FieldByName('Category').asDateTime;

      if (ParentRecID.Length > 0) and not ParentRecIDList.Contains(ParentRecID) then
        ParentRecIDList.Add(ParentRecID);

      if not CategoryList.Contains(Category) then
        CategoryList.Add(Category);

      aDataSet.Next;
    end;

    if ParentRecIDList.Count > 0 then
      for Category in CategoryList do begin
        for ParentRecID in ParentRecIDList do begin
          aDataSet.Filtered := False;
          aDataSet.Filter := 'ParentRecID = ' + QuotedStr(ParentRecID) +
            ' and Category = ' + QuotedStr(FormatDateTime('yyyy-mm-dd', Category));
          aDataSet.Filtered := True;

          Payment := 0;
          TBM := 0;
          Egress := 0; RepairEgress := 0; TotalEgress := 0;
          Ingress := 0; RepairIngress := 0; TotalIngress := 0;
          TotalBandiwdth := 0;
          aDataSet.First;
          while not aDataSet.Eof do begin
            TBM := TBM + aDataSet.FieldByName('TBM').AsFloat;
            Egress := Egress + aDataSet.FieldByName('Egress').AsFloat;
            RepairEgress := RepairEgress + aDataSet.FieldByName('RepairEgress').AsFloat;
            TotalEgress := TotalEgress + aDataSet.FieldByName('TotalEgress').AsFloat;

            case ChartDisplaySettings.ChartDataType of
              TChartDataType.cdtHistorical:
                  Payment := Payment + aDataSet.FieldByName('Payment').AsCurrency;
              TChartDataType.cdtCurrentMonth: begin
                Ingress := Ingress + aDataSet.FieldByName('Ingress').AsFloat;
                RepairIngress := RepairIngress + aDataSet.FieldByName('RepairIngress').AsFloat;
                TotalIngress := TotalIngress + aDataSet.FieldByName('TotalIngress').AsFloat;

              TotalBandiwdth := TotalBandiwdth + aDataSet.FieldByName('TotalBandwidth').AsFloat;
              end;
            end;

            aDataSet.Next;
          end;

          aDataSet.Filtered := False;
          if aDataSet.Locate('Category;RecID', VarArrayOf([Category, ParentRecID])) then begin
            aDataSet.Edit;
            aDataSet['TBM'] := TBM;
            aDataSet['Egress'] := Egress;
            aDataSet['RepairEgress'] := RepairEgress;
            aDataSet['TotalEgress'] := TotalEgress;

            case ChartDisplaySettings.ChartDataType of
              TChartDataType.cdtHistorical: begin
                aDataSet['Payment'] := Payment;
                if TBM > 0 then
                  aDataSet['Efficiency'] := Payment / TBM;
              end;
              TChartDataType.cdtCurrentMonth: begin
                aDataSet['Ingress'] := Ingress;
                aDataSet['RepairIngress'] := RepairIngress;
                aDataSet['TotalIngress'] := TotalIngress;
                aDataSet['TotalBandwidth'] := TotalBandiwdth;
              end;
            end;

            aDataSet.Post;
          end;

        end;
      end;

    aDataSet.Filtered := False;
  finally
    ParentRecIDList.Free;
    CategoryList.Free;
  end;
end;

procedure TDM.DataModuleCreate(Sender: TObject);
begin
  fIsPausedRefresh := False;
  fLoadSourceMode := TLoadSourceMode.lsmManual;
  fNodesBuffer := [];

  fNotifiedErrorNodes := TStringList.Create;
  fNotifiedOfflineNodes := TStringList.Create;

  FDConnection.Connected := True;
  qryRestartUsedDifferHistory.Active := True;

  TNotifyCenter.Instance.Subscribe(COMMAND_SETTING_CHANGED, OnCommandSettingsChanged);
  OnCommandSettingsChanged(nil);
end;

procedure TDM.DataModuleDestroy(Sender: TObject);
begin
  if Assigned(fNodesJArray) then
    fNodesJArray.Free;

  if Assigned(fNodesJArrayActiveOnly) then
    fNodesJArrayActiveOnly.Free;

  fNotifiedErrorNodes.Free;
  fNotifiedOfflineNodes.Free;

  TNotifyCenter.Instance.UnSubscribe(COMMAND_SETTING_CHANGED, OnCommandSettingsChanged);
end;

procedure TDM.DoCleanNodesBuffer;
begin
  tmrNodesBuffer.Enabled := False;
  tmrNodesBuffer.Enabled := True;

  TNotifyCenter.Instance.SendMessage(COMMAND_SHOW_NODE_DATA_GROUP, TValue.From<TNodeDataArray>(fNodesBuffer));
  fNodesBuffer := [];
end;

procedure TDM.DoFinishReloadNodes;
var
  AfterGetNodeData: TAfterGetNodeData;
begin
  AfterGetNodeData.TotalCount := fTotalNodeCount;
  AfterGetNodeData.SuccessCount := fSuccessNodeCount;
  AfterGetNodeData.ErrorCount := fErrorNodeCount;
  TNotifyCenter.Instance.SendMessage(COMMAND_AFTER_GET_NODE_DATA, TValue.From<TAfterGetNodeData>(AfterGetNodeData));
  SaveDataToHistory;
  ProcessDataForDashboard;

//  {$IFDEF DEBUG}
//    ProcessDataForNotify;
//  {$ELSE}
    if fLoadSourceMode = TLoadSourceMode.lsmAuto then begin
      try
        ProcessDataForNotify;
      except
        on E: Exception do
          Log('Error at send notify: ' + E.Message);
      end;
    end;
  //{$ENDIF}
end;

procedure TDM.ErrorChartNodeDataCallback(ANode: TNode; E: Exception);
begin
  fChartWorker.IncProgress;

  fChartWorker.Log('Error: ' + GetNodeCaptionForLog(ANode));

  fChartNodeCount := fChartNodeCount + 1;
  if fChartNodeCount = fChartTotalNodeCount then
    MakeAndFillHistoricalChartData;
end;

procedure TDM.ErrorNodeDataCallback(ANode: TNode; E: Exception);
var
  LogMessage: String;
begin
  LogMessage := GetNodeCaptionForLog(ANode) + ' ' + ANode.Address + ':' + ANode.Port.ToString + ' - Error get data - ' + E.Message;
  Log(DateTimeToStr(Now) + ' ' + LogMessage);

  SetNodeBoolValueByFieldInJArrayActiveOnly(aNode, True, 'IsError');

  fNodeCount := fNodeCount + 1;
  fErrorNodeCount := fErrorNodeCount + 1;
  if fNodeCount = fTotalNodeCount then
    DoFinishReloadNodes;
end;

procedure TDM.ErrorTotalEarnedNodeDataCallback(ANode: TNode; E: Exception);
begin
  Log(DateTimeToStr(Now) + ' ' + GetNodeCaptionForLog(ANode) + ' ' + ANode.Address + ':' + ANode.Port.ToString + ' - Error get total earned data - ' + E.Message);
end;

procedure TDM.FDConnectionAfterConnect(Sender: TObject);
begin
  DFW.CheckDBUpdates(FDConnection, TResourceScriptGetter.Create);
end;

procedure TDM.FillChartParents(aDataSet: TDataSet);
var
  CategoryList: TList<TDateTime>;
  DT: TDateTime;
begin
  CategoryList := TList<TDateTime>.Create;
  try
    aDataSet.First;
    while not aDataSet.Eof do begin
      DT := aDataSet.FieldByName('Category').AsDateTime;
      if not CategoryList.Contains(DT) then
        CategoryList.Add(DT);

      aDataSet.Next;
    end;

    for DT in CategoryList do begin
      mtblTreeNodeData.First;
      while not mtblTreeNodeData.Eof do begin
        if mtblTreeNodeData.FieldByName('Port').IsNull then begin
          aDataSet.Append;
          aDataSet['Category'] := DT;
          aDataSet['Title'] := mtblTreeNodeData['Title'];
          aDataSet['Address'] := '';
          aDataSet['RecID'] := mtblTreeNodeData['RecID'];
          aDataSet['ParentRecID'] := mtblTreeNodeData['ParentRecID'];
          aDataSet['Level'] := mtblTreeNodeData['Level'];
          aDataSet['FullPath'] := mtblTreeNodeData['FullPath'];
          aDataSet.Post;
        end;
        mtblTreeNodeData.Next;
      end;
    end;
  finally
    CategoryList.Free;
  end;
end;

procedure TDM.FillJArrFullPath(aJArr: TJSONArray; const aParentPath: String);
begin
  for var i := 0 to aJArr.Count - 1 do begin
    var JObj := TJSONObject(aJArr[i]);

    if aParentPath.IsEmpty then
      TJSONObject(aJarr[i]).AddPair('FullPath', JObj.GetValue<String>('Title'))
    else
      TJSONObject(aJarr[i]).AddPair('FullPath', aParentPath + ' - ' + JObj.GetValue<String>('Title'));

   var JValue := JObj.GetValue<TJSONArray>('Childs', nil);
   if JValue <> nil then
     FillJArrFullPath(TJSONArray(JValue), JObj.GetValue<String>('FullPath'));
  end;
end;

procedure TDM.UpdateLastestStorjVersion;
begin
  GetGithubLatestVersion(OnGetStorjGithubVersion);
  GetStorjIOLatestVersion(OnGetStorjIOVersion);
end;

procedure TDM.UpdatePCRebooter(const aRebooter: TPCRebooterSettings);
begin
  SendEchoAsync(aRebooter, PCRebooterEchoCallbackLogged);
end;

procedure TDM.UpdatePCRebooters;
begin
  timerPCRebootersTimer(nil);
end;

procedure TDM.UpdateStorjNetworkData;
begin
  GetStorjNetworkDataAsync(OnGetStorjNetworkData);
end;

procedure TDM.UpsertSSHSettings(aSSHSettings: TSSHSettings);
begin
  var PasswordTransformer := GetPasswordTransformer('LockBoxAES256');

  var Q := TFDQuery.Create(nil);
  try
    Q.Connection := FDConnection;
    Q.Open('select * from SSH_SETTINGS where FULLPATH = :FULLPATH', [aSSHSettings.FullPath]);

    if Q.IsEmpty then begin
      Q.SQL.Text := 'insert into SSH_SETTINGS (CREATION_DT, LASTMODIFIED_DT, FULLPATH, HOST, PORT, USERNAME, PASSWORD, COMMANDS, LAST_SELECTED_INDEXES) ' +
                                      'values (:CREATION_DT, :LASTMODIFIED_DT, :FULLPATH, :HOST, :PORT, :USERNAME, :PASSWORD, :COMMANDS, :LAST_SELECTED_INDEXES)';
      Q.ParamByName('CREATION_DT').Value := TDateTime.NowUTC;
    end else
      Q.SQL.Text := 'update SSH_SETTINGS set LASTMODIFIED_DT = :LASTMODIFIED_DT, HOST = :HOST, PORT = :PORT, USERNAME = :USERNAME, ' +
                    'PASSWORD = :PASSWORD, COMMANDS = :COMMANDS, LAST_SELECTED_INDEXES = :LAST_SELECTED_INDEXES ' +
                    'where FULLPATH = :FULLPATH';

    Q.ParamByName('LASTMODIFIED_DT').Value := TDateTime.NowUTC;
    Q.ParamByName('FULLPATH').Value := aSSHSettings.FullPath;
    Q.ParamByName('HOST').Value := aSSHSettings.Host;
    Q.ParamByName('PORT').Value := aSSHSettings.Port;
    Q.ParamByName('USERNAME').Value := aSSHSettings.UserName;
    Q.ParamByName('PASSWORD').Value := PasswordTransformer.Encypt(aSSHSettings.Password);
    Q.ParamByName('COMMANDS').Value := aSSHSettings.Commands;
    Q.ParamByName('LAST_SELECTED_INDEXES').Value := aSSHSettings.LastSelectedIndexes;

    Q.ExecSQL;
  finally
    Q.Free;
  end;
end;

function TDM.GenGUID: String;
var
  Uid: TGuid;
  Res: HResult;
begin
  Result := '';

  Res := CreateGuid(Uid);
  if Res = S_OK then
    Result := GuidToString(Uid);

  Result := Result.Replace('{', '');
  Result := Result.Replace('}', '');
  Result := Result.Replace('-', '');
end;

procedure TDM.GetChartLocalHistory;
  procedure AddNodesFromArray(aInnerJSONArr: TJSONArray; aDate: TDate);
  begin

    for var i := 0 to aInnerJSONArr.Count - 1 do begin
      var JObj := aInnerJSONArr[i] as TJSONObject;

      mtblChartLocalHistory.Append;
      mtblChartLocalHistory['Category'] := aDate;
      mtblChartLocalHistory['Title'] := JObj.GetValue<String>('Title', '');
      mtblChartLocalHistory['Address'] := JObj.GetValue<String>('Address', '');
      if JObj.GetValue<String>('Port', '').Length > 0 then
        mtblChartLocalHistory['Port'] := JObj.GetValue<Integer>('Port', 0);
      mtblChartLocalHistory['TBM'] := JObj.GetValue<Double>('TBM', 0.0);
      mtblChartLocalHistory['Ingress'] := JObj.GetValue<Double>('Ingress', 0.0);
      mtblChartLocalHistory['Egress'] := JObj.GetValue<Double>('Egress', 0.0);
      mtblChartLocalHistory['RepairIngress'] := JObj.GetValue<Double>('RepairIngress', 0.0);
      mtblChartLocalHistory['RepairEgress'] := JObj.GetValue<Double>('RepairEgress', 0.0);
      mtblChartLocalHistory['AllocatedSpace'] := JObj.GetValue<Integer>('AllocatedSpace', 0);
      mtblChartLocalHistory['FreeSpace'] := JObj.GetValue<Integer>('FreeSpace', 0);
      mtblChartLocalHistory['UsedSpace'] := JObj.GetValue<Integer>('UsedSpace', 0);
      mtblChartLocalHistory['PayoutDirty'] := JObj.GetValue<Currency>('PayoutDirty', 0);
      mtblChartLocalHistory['PayoutCleanly'] := JObj.GetValue<Currency>('PayoutCleanly', 0);
      mtblChartLocalHistory['EstimatedDirty'] := JObj.GetValue<Currency>('EstimatedDirty', 0);
      mtblChartLocalHistory['EstimatedCleanly'] := JObj.GetValue<Currency>('EstimatedCleanly', 0);
      mtblChartLocalHistory['Version'] := JObj.GetValue<String>('Version', '');
      mtblChartLocalHistory['Efficiency'] := JObj.GetValue<Currency>('Efficiency', 0);
      mtblChartLocalHistory['NodeID'] := JObj.GetValue<String>('NodeID', '');
      mtblChartLocalHistory['Trash'] := JObj.GetValue<Integer>('Trash', 0);
      mtblChartLocalHistory['FullPath'] := JObj.GetValue<String>('FullPath', '');
      mtblChartLocalHistory['TotalBandwidth'] := JObj.GetValue<Currency>('TotalBandwidth', 0);
      mtblChartLocalHistory['DailyEgress'] := JObj.GetValue<Double>('DailyEgress', 0.0);
      mtblChartLocalHistory['DailyIngress'] := JObj.GetValue<Double>('DailyIngress', 0.0);
      mtblChartLocalHistory['DailyRepairIngress'] := JObj.GetValue<Double>('DailyRepairIngress', 0.0);
      mtblChartLocalHistory['DailyRepairEgress'] := JObj.GetValue<Double>('DailyRepairEgress', 0.0);
      mtblChartLocalHistory['DailyTotalBandwidth'] := JObj.GetValue<Double>('DailyTotalBandwidth', 0.0);
      mtblChartLocalHistory['DailyTBMonth'] := JObj.GetValue<Double>('DailyTBMonth', 0.0);
      mtblChartLocalHistory['UptimeMinutes'] := JObj.GetValue<Integer>('UptimeMinutes', 0);
      mtblChartLocalHistory.Post;

      var Childs := JObj.GetValue<TJSONArray>('Childs', nil);
      if Assigned(Childs) then
        AddNodesFromArray(Childs, aDate);
    end;
  end;

begin
  var DisplaySettings := fChartWorker.GetChartDisplaySettings;

  var Q := TFDQuery.Create(nil);
  try
    Q.Connection := FDConnection;
    Q.Open('select * from LOCAL_HISTORY where Date(CREATION_DT) >= Date(:FromDT) and Date(CREATION_DT) <= Date(:ToDT)',
            [DisplaySettings.DateFrom, DisplaySettings.DateTo]);
    Q.FetchAll;
    Q.First;
    while not Q.Eof do begin
      var JSONArr := TJSONObject.ParseJSONValue(Q.FieldByName('Data').AsString) as TJSONArray;
      var Date := TDate(Q.FieldByName('CREATION_DT').AsDateTime);
      if Assigned(JSONArr) then begin
        try
          AddNodesFromArray(JSONArr, Date);
        finally
          JSONArr.Free;
        end;
      end;
      Q.Next;
    end;
  finally
    Q.Free;
  end;

  MakeAndFillHistoricalChartData(True);
end;

function TDM.GetFormattedNeighboursLastUpdate: String;
var
  Date: TDateTime;
begin
  Date := GetNeighboursLastUpdated;
  if IncYear(Now, -2) > Date then
    Result := 'no date'
  else
    Result := FormatDateTime('hh:nn:ss dd.mm.yyyy', Date);
end;

function TDM.GetIsLoading: Boolean;
begin
  Result := fTotalNodeCount <> fNodeCount;
end;

function TDM.GetNodeCaptionForLog(const aNode: TNode): String;
begin
  if GetSettings.IsUseTreePathForLog then
    Result := aNode.FullPath
  else
    Result := aNode.Title;
end;

function TDM.GetNodesFromBranch(aJArr: TJSONArray; const aParentPath: String = ''): TNodesList;
var
  i: integer;
  JObj: TJSONObject;
  JValue: TJSONValue;
  Node: TNode;
begin
  Result := [];

  if not Assigned(aJArr) then
    Exit;

  for i := 0 to aJArr.Count - 1 do begin
    JObj := aJArr.Items[i] as TJSONObject;
    if not JObj.GetValue<Boolean>('IsActive') then
        Continue;

    if JObj.GetValue<String>('Port', '').Length > 0 then begin
      Node.Title := JObj.GetValue<String>('Title');
      Node.Address := JObj.GetValue<String>('Address', '');
      Node.Port := JObj.GetValue<Integer>('Port', 0);
      Node.FullPath := JObj.GetValue<String>('FullPath', '');
      if Node.FullPath.IsEmpty then begin
        if aParentPath.IsEmpty then
          Node.FullPath := Node.Title
        else
          Node.FullPath := aParentPath + ' - ' + Node.Title;
      end;
      Node.IsUseInAdditionalStats := true;
      Result := Result + [Node];
    end;

    JValue := JObj.GetValue<TJSONArray>('Childs', nil);
    if JValue <> nil then
      Result := Result + GetNodesFromBranch(JValue as TJSONArray, Node.FullPath);
  end;
end;

function TDM.GetNodesJArrayActiveOnly: TJSONArray;
begin
  Result := fNodesJArrayActiveOnly;
end;

function TDM.GetNodesList: TNodesList;
begin
  Result := GetNodesFromBranch(fNodesJArray);
end;

function TDM.GetSSHSettings(const aFullPath: String): TSSHSettings;
begin
  Result.FullPath := aFullPath;

  var PasswordTransformer := GetPasswordTransformer('LockBoxAES256');

  var Q := TFDQuery.Create(nil);
  try
    Q.Connection := FDConnection;
    Q.Open('select * from SSH_SETTINGS where FULLPATH = :FULLPATH', [aFullPath]);

    if Q.IsEmpty then
      Exit;

    Result.Host := Q.FieldByName('HOST').AsString;
    Result.Port := Q.FieldByName('PORT').AsInteger;
    Result.UserName := Q.FieldByName('USERNAME').AsString;
    Result.Password := PasswordTransformer.Decrypt(Q.FieldByName('PASSWORD').AsString);
    Result.Commands := Q.FieldByName('COMMANDS').AsString;
    Result.LastSelectedIndexes := Q.FieldByName('LAST_SELECTED_INDEXES').AsString;
  finally
    Q.Free;
  end;
end;

procedure TDM.NodeChartDataCallback(aData: TArray<TChartNodeData>);
var
  Node: TChartNodeData;
  ChartDisplaySettings: TChartDisplaySettings;
  DataSet: TDataSet;
begin
  ChartDisplaySettings := fChartWorker.GetChartDisplaySettings;

  DataSet := nil;

  case ChartDisplaySettings.ChartDataType of
    cdtHistorical: DataSet := mtblChartHistoricalData;
    cdtCurrentMonth: DataSet := mtblChartCurrentMonthData;
  end;

  if not Assigned(DataSet) then
    Exit;

  for Node in aData do begin
    DataSet.Append;
    DataSet['Category'] := Node.DT;
    DataSet['Title'] := Node.Node.Title;
    DataSet['Address'] := Node.Node.Address;
    DataSet['Port'] := Node.Node.Port;
    DataSet['FullPath'] := Node.Node.FullPath;

    if mtblTreeNodeData.Locate('Title', Node.Node.Title) then begin
      DataSet['RecID'] := mtblTreeNodeData['RecID'];
      DataSet['ParentRecID'] := mtblTreeNodeData['ParentRecID'];
      DataSet['Level'] := mtblTreeNodeData['Level'];
    end;

    DataSet['TBM'] := Node.TBMonth;
    DataSet['Egress'] := Node.Egress;
    DataSet['RepairEgress'] := Node.RepairEgress;
    DataSet['TotalEgress'] := Node.TotalEgress;

    case ChartDisplaySettings.ChartDataType of
      cdtHistorical: begin
        DataSet['Payment'] := Node.Payment;
        DataSet['Efficiency'] := Node.Efficiency;
      end;
      cdtCurrentMonth: begin
        DataSet['Ingress'] := Node.Ingress;
        DataSet['RepairIngress'] := Node.RepairIngress;
        DataSet['TotalIngress'] := Node.TotalINgress;
        DataSet['TotalBandwidth'] := Node.TotalBandwidth;
      end;
    cdtLocalHistory: ;
  end;

    DataSet.Post;
  end;

  fChartWorker.IncProgress;

  fChartNodeCount := fChartNodeCount + 1;
  if fChartNodeCount = fChartTotalNodeCount then
    MakeAndFillHistoricalChartData;
end;

function TDM.IsCanLoadNeighbours: Boolean;
begin
  {$IFDEF DEBUG}
    Result := True;
  {$ELSE}
    var LastUpdatedDate := GetNeighboursLastUpdated;
    Result := IncHour(LastUpdatedDate, 2) < Now;
  {$ENDIF}
end;

procedure TDM.NeighboursDataCallback(const aData: TNeighboursData);
begin
  TNotifyCenter.Instance.SendMessage(COMMAND_NEIGHBOURS_DATA, TValue.From<TNeighboursData>(aData));
end;

procedure TDM.NeighboursErrorCallback(const aHost: String; E: Exception);
begin
  Log(DateTimeToStr(Now) + ' ' + aHost + ' - Error get data - ' + E.Message);
end;

procedure TDM.NodeDataCallback(ANodeData: TNodeData);
var
  RecNo: Integer;
begin
  if GetSettings.IsUseNodeRefreshBuffer then begin
    fNodesBuffer := fNodesBuffer + [ANodeData];
//    if Length(fNodesBuffer) = 15 then
//      DoCleanNodesBuffer;
  end else begin
    TNotifyCenter.Instance.SendMessage(COMMAND_SHOW_NODE_DATA, TValue.From<TNodeData>(ANodeData));
  end;

  SetNodeBoolValueByFieldInJArrayActiveOnly(ANodeData.Node, False, 'IsError');
  SetNodeBoolValueByFieldInJArrayActiveOnly(ANodeData.Node, not ANodeData.IsOnline, 'IsOffline');

  mtblNodeData.DisableControls;
  try
//    for var Satellite in ANodeData.Satellites do
//      if mtblNodeData.FieldDefList.Find(SATELLITE_COLUMN_NAME_PREFIX + Satellite.Title) = nil then
//        AddSatelliteField(SATELLITE_COLUMN_NAME_PREFIX + Satellite.Title);

    RecNo := mtblNodeData.RecNo;

    if mtblNodeData.Locate('Address;Port', VarArrayOf([ANodeData.Node.Address, ANodeData.Node.Port]), []) then begin
       //check for Restart Used Differ
       var Used := mtblNodeData.FieldByName('UsedSpace').AsInteger;
       var Uptime := mtblNodeData.FieldByName('UptimeMinutes').AsInteger;
       if (Used > 0) and (Uptime > 0) and (Used > (ANodeData.DiskSpace.Used + 1)) and (Uptime > ANodeData.UptimeMinutes) then begin
         mtblRestartUsedDiffer.Append;
         mtblRestartUsedDiffer['Title'] := ANodeData.Node.Title;
         mtblRestartUsedDiffer['OldUsed'] := Used;
         mtblRestartUsedDiffer['NewUsed'] := ANodeData.DiskSpace.Used;
         mtblRestartUsedDiffer['OldUptime'] := Uptime;
         mtblRestartUsedDiffer['NewUptime'] := ANodeData.UptimeMinutes;
         mtblRestartUsedDiffer['OldTrash'] := mtblNodeData['Trash'];
         mtblRestartUsedDiffer['NewTrash'] := ANodeData.DiskSpace.Trash;
         mtblRestartUsedDiffer.Post;

         qryRestartUsedDifferHistory.DisableControls;
         try
           qryRestartUsedDifferHistory.Append;
           qryRestartUsedDifferHistory['NodeID'] := ANodeData.Node.NodeID;
           qryRestartUsedDifferHistory['Title'] := ANodeData.Node.Title;
           qryRestartUsedDifferHistory['OldUsed'] := Used;
           qryRestartUsedDifferHistory['NewUsed'] := ANodeData.DiskSpace.Used;
           qryRestartUsedDifferHistory['OldUptime'] := Uptime;
           qryRestartUsedDifferHistory['NewUptime'] := ANodeData.UptimeMinutes;
           qryRestartUsedDifferHistory['OldTrash'] := mtblNodeData['Trash'];
           qryRestartUsedDifferHistory['NewTrash'] := ANodeData.DiskSpace.Trash;
           qryRestartUsedDifferHistory['CreationDT'] := Now;
           qryRestartUsedDifferHistory.Post;
         finally
           qryRestartUsedDifferHistory.EnableControls;
         end;

         TNotifyCenter.Instance.SendMessage(COMMAND_RESTART_USED_DIFFER_COUNT, mtblRestartUsedDiffer.RecordCount);
       end;

       mtblNodeData.Edit;
       mtblNodeData['Egress'] := ANodeData.Egress;
       mtblNodeData['Ingress'] := ANodeData.Ingress;
       mtblNodeData['RepairIngress'] := ANodeData.RepairIngress;
       mtblNodeData['RepairEgress'] := ANodeData.RepairEgress;
       mtblNodeData['TotalBandwidth'] := ANodeData.TotalBandwidth;
       mtblNodeData['TBM'] := ANodeData.TBMonth;
       mtblNodeData['PayoutCleanly'] := ANodeData.ClearlyPayout;
       mtblNodeData['PayoutDirty'] := ANodeData.DirtyPayout;
       mtblNodeData['EstimatedDirty'] := ANodeData.EstimatedPayoutDirty;
       mtblNodeData['EstimatedCleanly'] := ANodeData.EstimatedPayoutCleanly;
       mtblNodeData['Version'] := ANodeData.Version;
       mtblNodeData['AllocatedSpace'] := ANodeData.DiskSpace.Available;
       mtblNodeData['UsedSpace'] := ANodeData.DiskSpace.Used;
       mtblNodeData['FreeSpace'] := ANodeData.DiskSpace.Free;
       mtblNodeData['Efficiency'] := ANodeData.EfficiencyByUsed;
       mtblNodeData['EfficiencyByTBM'] := ANodeData.EfficiencyByTBM;
       mtblNodeData['NodeID'] := ANodeData.Node.NodeID;
       mtblNodeData['Trash'] := ANodeData.DiskSpace.Trash;
       mtblNodeData['UptimeMinutes'] := ANodeData.UptimeMinutes;
       mtblNodeData['Wallet'] := ANodeData.Wallet;

       mtblNodeData['DailyIngress'] := ANodeData.DailyData.Ingress;
       mtblNodeData['DailyEgress'] := ANodeData.DailyData.Egress;
       mtblNodeData['DailyRepairIngress'] := ANodeData.DailyData.RepairIngress;
       mtblNodeData['DailyRepairEgress'] := ANodeData.DailyData.RepairEgress;
       mtblNodeData['DailyTotalBandwidth'] := ANodeData.DailyData.TotalBandwidth;
       mtblNodeData['DailyTBMonth'] := ANodeData.DailyData.TBMonth;

       //for var Satellite in ANodeData.Satellites do
          // mtblNodeData[SATELLITE_COLUMN_NAME_PREFIX + Satellite.Title] := Satellite.StorageUsed;

       mtblNodeData.Post;
    end;

    mtblNodeData.RecNo := RecNo;
  finally
    mtblNodeData.EnableControls;
  end;

  if not ANodeData.IsOnline then
    Log(DateTimeToStr(Now) + ' ' + GetNodeCaptionForLog(ANodeData.Node) + ' ' + ANodeData.Node.Address + ':' + ANodeData.Node.Port.ToString +
          ' is offline');

  for var Satellite in ANodeData.Satellites do begin
    if Satellite.Health.AuditPerc > 0 then begin
      if GetSettings.IsLogHealthAuditRed and (Satellite.Health.AuditPerc < GetSettings.HealthAuditRed) then
        Log(DateTimeToStr(Now) + ' ' + GetNodeCaptionForLog(ANodeData.Node) + ' ' + ANodeData.Node.Address + ':' + ANodeData.Node.Port.ToString +
            ' on satellite ' + Satellite.Title + ' have red audit value: ' + CurrToStr(Satellite.Health.AuditPerc) + '%')
      else if GetSettings.IsLogHealthAuditYellow and (Satellite.Health.AuditPerc < GetSettings.HealthAuditYellow)
              and (Satellite.Health.AuditPerc > GetSettings.HealthAuditRed) then
        Log(DateTimeToStr(Now) + ' ' + GetNodeCaptionForLog(ANodeData.Node) + ' ' + ANodeData.Node.Address + ':' + ANodeData.Node.Port.ToString +
            ' on satellite ' + Satellite.Title + ' have yellow audit value: ' + CurrToStr(Satellite.Health.AuditPerc) + '%');
    end;

    if Satellite.Health.OnlinePerc > 0 then begin
      if GetSettings.IsLogHealthOnlineRed and (Satellite.Health.OnlinePerc < GetSettings.HealthOnlineRed) then
        Log(DateTimeToStr(Now) + ' ' + GetNodeCaptionForLog(ANodeData.Node) + ' ' + ANodeData.Node.Address + ':' + ANodeData.Node.Port.ToString +
            ' on satellite ' + Satellite.Title + ' have red online value: ' + CurrToStr(Satellite.Health.OnlinePerc) + '%')
      else if GetSettings.IsLogHealthOnlineYellow and (Satellite.Health.OnlinePerc < GetSettings.HealthOnlineYellow)
              and (Satellite.Health.OnlinePerc > GetSettings.HealthOnlineRed) then
        Log(DateTimeToStr(Now) + ' ' + GetNodeCaptionForLog(ANodeData.Node) + ' ' + ANodeData.Node.Address + ':' + ANodeData.Node.Port.ToString +
            ' on satellite ' + Satellite.Title + ' have yellow online value: ' + CurrToStr(Satellite.Health.OnlinePerc) + '%');
    end;

    if Satellite.Health.SuspensionPerc > 0 then begin
      if GetSettings.IsLogHealthSuspensionRed and (Satellite.Health.SuspensionPerc < GetSettings.HealthSuspensionRed) then
        Log(DateTimeToStr(Now) + ' ' + GetNodeCaptionForLog(ANodeData.Node) + ' ' + ANodeData.Node.Address + ':' + ANodeData.Node.Port.ToString +
            ' on satellite ' + Satellite.Title + ' have red suspension value: ' + CurrToStr(Satellite.Health.SuspensionPerc) + '%')
      else if GetSettings.IsLogHealthSuspensionYellow and (Satellite.Health.SuspensionPerc < GetSettings.HealthSuspensionYellow)
              and (Satellite.Health.SuspensionPerc > GetSettings.HealthSuspensionRed) then
        Log(DateTimeToStr(Now) + ' ' + GetNodeCaptionForLog(ANodeData.Node) + ' ' + ANodeData.Node.Address + ':' + ANodeData.Node.Port.ToString +
            ' on satellite ' + Satellite.Title + ' have yellow suspension value: ' + CurrToStr(Satellite.Health.SuspensionPerc) + '%');
    end;
  end;

  fNodeCount := fNodeCount + 1;
  fSuccessNodeCount := fSuccessNodeCount + 1;
  if fNodeCount = fTotalNodeCount then
    DoFinishReloadNodes;
end;

procedure TDM.NodeTotalEarnedDataCallback(ANodeData: TNodeData);
begin
  TNotifyCenter.Instance.SendMessage(COMMAND_RECEIVE_TOTAL_EARNED, TValue.From<TNodeData>(ANodeData));
end;

procedure TDM.OnCommandSettingsChanged(const aValue: TValue);
begin
  tmrReloadNodes.Enabled := GetSettings.IsEnableReloadNodesTimer;
  tmrReloadNodes.Interval := GetSettings.ReloadNodesTimerMinutes * 1000 * 60;

  tmrNodesBuffer.Enabled := GetSettings.IsUseNodeRefreshBuffer;
end;

procedure TDM.OnGetStorjGithubVersion(AVersion: String);
begin
  TNotifyCenter.Instance.SendMessage(COMMAND_STORJ_GITHUB_VERSION, AVersion);
end;

procedure TDM.OnGetStorjIOVersion(AStorjIOVersion: TStorjIOVersion);
begin
  TNotifyCenter.Instance.SendMessage(COMMAND_STORJ_IO_VERSION, TValue.From<TStorjIOVersion>(AStorjIOVersion));
end;

procedure TDM.OnGetStorjNetworkData(const aData: TStorjNetworkData);
begin
  TNotifyCenter.Instance.SendMessage(COMMAND_STORJ_NETWORK_DATA, TValue.From<TStorjNetworkData>(aData));
end;

procedure TDM.PCRebooterEchoCallback(aRebooter: TPCRebooterSettings; aRes: Boolean);
var
  RebooterSettingsResult: TPCRebooterSettingsEchoResult;
begin
  RebooterSettingsResult.Settings := aRebooter;
  RebooterSettingsResult.IsOnline := aRes;

  TNotifyCenter.Instance.SendMessage(COMMAND_PCREBOOTER_ECHO_RESULT, TValue.From<TPCRebooterSettingsEchoResult>(RebooterSettingsResult));
end;

procedure TDM.PCRebooterEchoCallbackLogged(aRebooter: TPCRebooterSettings; aRes: Boolean);
var
  RebooterSettingsResult: TPCRebooterSettingsEchoResult;
  Text: String;
begin
  RebooterSettingsResult.Settings := aRebooter;
  RebooterSettingsResult.IsOnline := aRes;

  if aRes then
    Text := 'online'
  else
    Text := 'offline';

  Log(DateTimeToStr(Now) + ' PCRebooter ' + aRebooter.Name + ' is ' + Text);

  TNotifyCenter.Instance.SendMessage(COMMAND_PCREBOOTER_ECHO_RESULT, TValue.From<TPCRebooterSettingsEchoResult>(RebooterSettingsResult));
end;

procedure TDM.ProcessDataForDashboard;
var
  UsedSpace, Trash, FreeSpace: Integer;
begin
  UsedSpace := 0; Trash := 0; FreeSpace := 0;
  mtblNodeData.DisableControls;
  try
    mtblNodeData.First;
    while not mtblNodeData.Eof do
    begin
      UsedSpace := UsedSpace + mtblNodeData.FieldByName('UsedSpace').AsInteger;
      Trash := Trash + mtblNodeData.FieldByName('Trash').AsInteger;
      FreeSpace := FreeSpace + mtblNodeData.FieldByName('FreeSpace').AsInteger;

      mtblNodeData.Next;
    end;
  finally
    mtblNodeData.EnableControls;
  end;

  mtblDashboardSpace.DisableControls;
  try
    mtblDashboardSpace.Close;
    mtblDashboardSpace.Open;

    mtblDashboardSpace.Append;
    mtblDashboardSpace['Caption'] := 'Used Space';
    mtblDashboardSpace['Value'] := UsedSpace;
    mtblDashboardSpace.Post;

    mtblDashboardSpace.Append;
    mtblDashboardSpace['Caption'] := 'Trash';
    mtblDashboardSpace['Value'] := Trash;
    mtblDashboardSpace.Post;

    mtblDashboardSpace.Append;
    mtblDashboardSpace['Caption'] := 'Free Space';
    mtblDashboardSpace['Value'] := FreeSpace;
    mtblDashboardSpace.Post;
  finally
    mtblDashboardSpace.EnableControls;
  end;
end;

procedure TDM.ProcessDataForNotify;
  function CalcChildsBoolValueByField(aJObj: TJSONObject; aField: String): Boolean;
  begin
    var innerJArr := aJObj.GetValue<TJSONArray>('Childs', nil);
    if Assigned(innerJArr) then begin
      Result := True;
      for var j := 0 to innerJArr.Count - 1 do begin
        var innerJObj := innerJArr.Items[j] as TJSONObject;
        var innerBoolValue := CalcChildsBoolValueByField(InnerJObj, aField);

        var innerPair := innerJObj.Get(aField);
        if Assigned(innerPair) then
          innerPair.JsonValue := TJSONBool.Create(innerBoolValue)
        else
          innerJObj.AddPair(aField, innerBoolValue);

        Result := Result and innerBoolValue;
      end;
    end else
      Result := aJObj.GetValue<Boolean>(aField, False);
  end;

  function GetMessageByBoolField(aJArr: TJSONArray; aField, aText: String; aStrList, aStrListSecond: TStringList): String;
  begin
    Result := '';
    for var i := 0 to aJArr.Count - 1 do begin
      var JObj := aJArr.Items[i] as TJSONObject;
      var BoolValue := JObj.GetValue<Boolean>(aField, False);
      var FullPath := JObj.GetValue<String>('FullPath', '');

      if BoolValue then begin
        var NodeName := '';
        if GetSettings.IsUseTreePathForLog or JObj.GetValue<String>('Port', '').IsEmpty then
          NodeName := JObj.GetValue<String>('FullPath', '')
        else
          NodeName := JObj.GetValue<String>('Title', '');

        if (aStrList.IndexOf(FullPath) < 0) and ((aStrListSecond = nil) or (aStrListSecond.IndexOf(FullPath) < 0)) then begin
          var Msg := NodeName + ' ' + aText;
          Result := Result + Msg + sLineBreak;
          aStrList.Add(FullPath);
        end;

        Continue;
      end;

      var Childs := JObj.GetValue<TJSONArray>('Childs', nil);
      if Assigned(Childs) then
        Result := Result + GetMessageByBoolField(Childs, aField, aText, aStrList, aStrListSecond);
    end;
  end;

  function GetNotErrorAnymoreMessage(aJArr: TJSONArray; aField, aText: String; aStrList: TStringList): String;
  begin
    Result := '';
    for var i := aStrList.Count - 1 downto 0 do begin
      var JNode := GetJNode(aJArr, aStrList[i]);
      var FullPath := JNode.GetValue<String>('FullPath', '');
      if not JNode.GetValue<Boolean>(aField, False) then begin
        var NodeName := '';
        if GetSettings.IsUseTreePathForLog or JNode.GetValue<String>('Port', '').IsEmpty then
          NodeName := JNode.GetValue<String>('FullPath', '')
        else
          NodeName := JNode.GetValue<String>('Title', '');

        var Msg := NodeName + ' ' + aText;
        Result := Result + Msg + sLineBreak;
        aStrList.Delete(i);
      end;
    end;
  end;
begin
//  {$IFDEF DEBUG}
//    var StrList := TStringList.Create;
//    try
//      StrList.Text := fNodesJArrayActiveOnly.Format;
//      StrList.SaveToFile(TDateTime.Now.TimeToMilliseconds.ToString + '_DEBUG_fNodesJArrayActiveOnly1.txt');
//    finally
//      StrList.Free;
//    end;
//  {$ENDIF}
  //1. calculate parent IsError by childs, in fNodesJArrayActiveOnly
  for var Field in ['IsError', 'IsOffline'] do begin
    for var i := 0 to fNodesJArrayActiveOnly.Count - 1 do begin
      var JObj := fNodesJArrayActiveOnly.Items[i] as TJSONObject;

      var BoolValue := CalcChildsBoolValueByField(JObj, Field);
      var Pair := JObj.Get(Field);
      if Assigned(Pair) then
        Pair.JsonValue := TJSONBool.Create(BoolValue)
      else
        JObj.AddPair(Field, BoolValue);
    end;
  end;

//  {$IFDEF DEBUG}
//    var StrList2 := TStringList.Create;
//    try
//      StrList2.Text := fNodesJArrayActiveOnly.Format;
//      StrList2.SaveToFile(TDateTime.Now.TimeToMilliseconds.ToString + '_DEBUG_fNodesJArrayActiveOnly2.txt');
//    finally
//      StrList2.Free;
//    end;
//  {$ENDIF}

  //2. get IsError nodes, from fNodesJArrayActiveOnly
  var MessageForNotifyError := GetMessageByBoolField(fNodesJArrayActiveOnly, 'IsError', 'have a error at loading data', fNotifiedErrorNodes, nil);
  var MessageForNotifyOffline := GetMessageByBoolField(fNodesJArrayActiveOnly, 'IsOffline', 'is offline', fNotifiedOfflineNodes, fNotifiedErrorNodes);

  //3. get nodes that is not error anymore
  var MessageForNotifyNotErrorAnymore := GetNotErrorAnymoreMessage(fNodesJArrayActiveOnly, 'IsError', 'now is ok', fNotifiedErrorNodes);
  var MessageForNotifyNotOfflineAnymore := GetNotErrorAnymoreMessage(fNodesJArrayActiveOnly, 'IsOffline', 'now is online', fNotifiedOfflineNodes);

  var NotifySettings := GetSettings.NotifySettings;

  if NotifySettings.TelegramSettings.IsEnable then begin
    var MessageForNotify := '';
    if not MessageForNotifyError.IsEmpty then
      MessageForNotify := '<b>' + MessageForNotifyError.Trim + '</b>' + sLineBreak;

    if not MessageForNotifyOffline.IsEmpty then
      MessageForNotify := MessageForNotify + '<b>' + MessageForNotifyOffline.Trim + '</b>' + sLineBreak;

    if not MessageForNotifyNotErrorAnymore.IsEmpty then
      MessageForNotify := MessageForNotify + '<i>' + MessageForNotifyNotErrorAnymore.Trim + '</i>' + sLineBreak;

    if not MessageForNotifyNotOfflineAnymore.IsEmpty then
      MessageForNotify := MessageForNotify + '<i>' + MessageForNotifyNotOfflineAnymore.Trim + '</i>';

    MessageForNotify := MessageForNotify.Trim;
    if not MessageForNotify.IsEmpty then
      SendTelegramMessage(NotifySettings.TelegramSettings.Token, NotifySettings.TelegramSettings.UserID, MessageForNotify);
  end;

  if NotifySettings.EmailSettings.IsEnable then begin
    var MessageForNotify := '';
    if not MessageForNotifyError.IsEmpty then
      MessageForNotify := '<b>' + MessageForNotifyError.Trim + '</b>' + sLineBreak;

    if not MessageForNotifyOffline.IsEmpty then
      MessageForNotify := MessageForNotify + '<b>' + MessageForNotifyOffline.Trim + '</b>' + sLineBreak;

    if not MessageForNotifyNotErrorAnymore.IsEmpty then
      MessageForNotify := MessageForNotify + '<i>' + MessageForNotifyNotErrorAnymore.Trim + '</i>' + sLineBreak;

    if not MessageForNotifyNotOfflineAnymore.IsEmpty then
      MessageForNotify := MessageForNotify + '<i>' + MessageForNotifyNotOfflineAnymore.Trim + '</i>';

    MessageForNotify := MessageForNotify.Trim;
    if not MessageForNotify.IsEmpty then
      SendEmailAsync(NotifySettings.EmailSettings.Email, MessageForNotify, NotifySettings.EmailSettings.Login, NotifySettings.EmailSettings.Password);
  end;
end;

procedure TDM.ReloadFromJSON;
begin
  if not FileExists(TreeJSONFileName) then
    Exit;

  var StrList := TStringList.Create;
  try
    StrList.LoadFromFile(TreeJSONFileName);
    var JArr := TJSONObject.ParseJSONValue(StrList.Text) as TJSONArray;
    if not Assigned(JArr) then
      Exit;

    if (JArr.Count > 0) and JArr[0].GetValue<String>('FullPath', '').IsEmpty then
      FillJArrFullPath(JArr, '');

    if Assigned(fNodesJArray) then
      FreeAndNil(fNodesJArray);

    fNodesJArray := JArr;

    if Assigned(fNodesJArrayActiveOnly) then
      FreeAndNil(fNodesJArrayActiveOnly);

    fNodesJArrayActiveOnly := fNodesJArray.Clone as TJSONArray;
    RemoveNonActiveFromJArray(fNodesJArrayActiveOnly);

    fNotifiedErrorNodes.Clear;
    fNotifiedOfflineNodes.Clear;

    var DataSetArr := [mtblNodeData, mtblTreeNodeData];
    for var DataSet in DataSetArr do begin
      DataSet.DisableControls;
      try
        DataSet.EmptyDataSet;

        AddNodeToDataSet(DataSet, JArr, '', 1);
      finally
        DataSet.EnableControls;
      end;
    end;

    TNotifyCenter.Instance.SendMessage(COMMAND_LOAD_FROM_JSON, TValue.From<TJSONArray>(JArr));
  finally
    StrList.Free;
  end;
end;

procedure TDM.ReloadLocalHistoryDates;
begin
  qryLocalHistoryDates.Close;
  qryLocalHistoryDates.Open;
  qryLocalHistoryDates.FetchAll;
end;

procedure TDM.RemoveNonActiveFromJArray(aJArray: TJSONArray);
begin
  for var i := aJArray.Count - 1 downto 0 do begin
    var JObj := aJArray.Items[i] as TJSONObject;
    if not JObj.GetValue<Boolean>('IsActive', true) then begin
      aJArray.Remove(i).Free;
      Continue;
    end;

    var childs := JObj.GetValue<TJSONArray>('Childs', nil);
    if Assigned(childs) then
      RemoveNonActiveFromJArray(childs);
  end;
end;

function TDM.GetJNode(aJArr: TJSONArray; const aFullPath: String): TJSONObject;
  begin
    for var i := 0 to aJArr.Count - 1 do begin
      var JObj := aJArr.Items[i] as TJSONObject;
      if JObj.GetValue<String>('FullPath', '') = aFullPath then
        Exit(JObj);

      var childs := JObj.GetValue<TJSONArray>('Childs', nil);
      if Assigned(childs) then begin
        Result := GetJNode(childs, aFullPath);
        if Assigned(Result) then
          Exit;
      end;
    end;
    Result := nil;
  end;

procedure TDM.SaveDataToHistory;
  procedure CalcParent(aInnerJObj: TJSONObject);
  var
    PayoutDirty, PayoutCleanly, EstimatedDirty, EstimatedCleanly, Efficiency: Currency;
  begin
    if not Assigned(aInnerJObj) then Exit;

    var childs := aInnerJObj.GetValue<TJSONArray>('Childs', nil);
    if not Assigned(childs) then Exit;

    var TBM := 0.0; var Ingress := 0.0; var Egress := 0.0;
    var RepairEgress := 0.0; var RepairIngress := 0.0;
    var AllocatedSpace := 0; var UsedSpace := 0; var FreeSpace := 0;
    PayoutDirty := 0; PayoutCleanly := 0; EstimatedDirty := 0; EstimatedCleanly := 0;
    var Trash := 0; var TotalBandwidth := 0.0;
    var DailyEgress := 0.0; var DailyIngress := 0.0;
    var DailyRepairIngress := 0.0; var DailyRepairEgress := 0.0;
    var DailyTotalBandwidth := 0.0; var DailyTBMonth := 0.0;
    for var i := 0 to childs.Count - 1 do begin
      var JInnerObj := childs.Items[i] as TJSONObject;
      if JInnerObj.FindValue('TBM') = nil then
        CalcParent(JInnerObj);

      TBM := TBM + JInnerObj.GetValue<Double>('TBM', 0.0);
      Ingress := Ingress + JInnerObj.GetValue<Double>('Ingress', 0.0);
      Egress := Egress + JInnerObj.GetValue<Double>('Egress', 0.0);
      RepairIngress := RepairIngress + JInnerObj.GetValue<Double>('RepairIngress', 0.0);
      RepairEgress := RepairEgress + JInnerObj.GetValue<Double>('RepairEgress', 0.0);
      AllocatedSpace := AllocatedSpace + JInnerObj.GetValue<Integer>('AllocatedSpace', 0);
      UsedSpace := UsedSpace + JInnerObj.GetValue<Integer>('UsedSpace', 0);
      FreeSpace := FreeSpace + JInnerObj.GetValue<Integer>('FreeSpace', 0);
      PayoutDirty := PayoutDirty + JInnerObj.GetValue<Currency>('PayoutDirty', 0);
      PayoutCleanly := PayoutCleanly + JInnerObj.GetValue<Currency>('PayoutCleanly', 0);
      EstimatedDirty := EstimatedDirty + JInnerObj.GetValue<Currency>('EstimatedDirty', 0);
      EstimatedCleanly := EstimatedCleanly + JInnerObj.GetValue<Currency>('EstimatedCleanly', 0);
      Trash := Trash + JInnerObj.GetValue<Integer>('Trash', 0);
      TotalBandwidth := TotalBandwidth + JInnerObj.GetValue<Double>('TotalBandwidth', 0.0);
      DailyEgress := DailyEgress + JInnerObj.GetValue<Double>('DailyEgress', 0.0);
      DailyIngress := DailyIngress + JInnerObj.GetValue<Double>('DailyIngress', 0.0);
      DailyRepairIngress := DailyRepairIngress + JInnerObj.GetValue<Double>('DailyRepairIngress', 0.0);
      DailyRepairEgress := DailyRepairEgress + JInnerObj.GetValue<Double>('DailyRepairEgress', 0.0);
      DailyTotalBandwidth := DailyTotalBandwidth + JInnerObj.GetValue<Double>('DailyTotalBandwidth', 0.0);
      DailyTBMonth := DailyTBMonth + JInnerObj.GetValue<Double>('DailyTBMonth', 0.0);
    end;

    Efficiency := 0;
    if UsedSpace > 0 then
      Efficiency := EstimatedDirty / UsedSpace * 1000;

    aInnerJObj.AddPair('TBM', TBM);
    aInnerJObj.AddPair('Ingress', Ingress);
    aInnerJObj.AddPair('Egress', Egress);
    aInnerJObj.AddPair('RepairIngress', RepairIngress);
    aInnerJObj.AddPair('RepairEgress', RepairEgress);
    aInnerJObj.AddPair('AllocatedSpace', AllocatedSpace);
    aInnerJObj.AddPair('UsedSpace', UsedSpace);
    aInnerJObj.AddPair('FreeSpace', FreeSpace);
    aInnerJObj.AddPair('PayoutDirty', PayoutDirty);
    aInnerJObj.AddPair('PayoutCleanly', PayoutCleanly);
    aInnerJObj.AddPair('EstimatedDirty', EstimatedDirty);
    aInnerJObj.AddPair('EstimatedCleanly', EstimatedCleanly);
    aInnerJObj.AddPair('Trash', Trash);
    aInnerJObj.AddPair('TotalBandwidth', TotalBandwidth);
    aInnerJObj.AddPair('DailyEgress', DailyEgress);
    aInnerJObj.AddPair('DailyIngress', DailyIngress);
    aInnerJObj.AddPair('DailyRepairIngress', DailyRepairIngress);
    aInnerJObj.AddPair('DailyRepairEgress', DailyRepairEgress);
    aInnerJObj.AddPair('DailyTotalBandwidth', DailyTotalBandwidth);
    aInnerJObj.AddPair('DailyTBMonth', DailyTBMonth);
    aInnerJObj.AddPair('Efficiency', Efficiency);
  end;

  procedure SaveToDB(const aJSON: String);
  begin
    var Query := TFDQuery.Create(nil);
    try
      Query.Connection := FDConnection;

      Query.SQL.Text := 'SELECT * FROM LOCAL_HISTORY WHERE DATE(CREATION_DT) = :Value';
      Query.ParamByName('Value').AsDate := TDateTime.NowUTC.GetDate;
      Query.Open;

      try
        if not Query.IsEmpty then begin
          var LocalHistoryID := Query.FieldByName('LOCAL_HISTORY_ID').AsInteger;
          Query.SQL.Text := 'UPDATE LOCAL_HISTORY SET CREATION_DT = :CREATION_DT, DATA = :DATA where LOCAL_HISTORY_ID = :ID';
          Query.ParamByName('ID').Value := LocalHistoryID;
        end else
          Query.SQL.Text := 'INSERT INTO LOCAL_HISTORY (CREATION_DT, DATA) VALUES (:CREATION_DT, :DATA)';

        Query.ParamByName('CREATION_DT').Value := TDateTime.NowUTC;
        Query.ParamByName('DATA').Value := aJSON;
        Query.ExecSQL;
      finally
        Query.Close;
      end;
    finally
      Query.Free;
    end;
  end;

var
  NodesArray: TJSONArray;
begin
  NodesArray := fNodesJArrayActiveOnly.Clone as TJSONArray;
  try
    mtblNodeData.DisableControls;
    try
      mtblNodeData.First;
      while not mtblNodeData.Eof do begin
        var FullPath := mtblNodeData.FieldByName('FullPath').AsString;
        var JObj := GetJNode(NodesArray, FullPath);
        if not Assigned(JObj) then begin
          mtblNodeData.Next;
          Continue;
        end;

        JObj.AddPair('TBM', mtblNodeData.FieldByName('TBM').AsFloat);
        JObj.AddPair('Ingress', mtblNodeData.FieldByName('Ingress').AsFloat);
        JObj.AddPair('Egress', mtblNodeData.FieldByName('Egress').AsFloat);
        JObj.AddPair('RepairIngress', mtblNodeData.FieldByName('RepairIngress').AsFloat);
        JObj.AddPair('RepairEgress', mtblNodeData.FieldByName('RepairEgress').AsFloat);
        JObj.AddPair('AllocatedSpace', mtblNodeData.FieldByName('AllocatedSpace').AsInteger);
        JObj.AddPair('FreeSpace', mtblNodeData.FieldByName('FreeSpace').AsInteger);
        JObj.AddPair('UsedSpace', mtblNodeData.FieldByName('UsedSpace').AsInteger);
        JObj.AddPair('PayoutDirty', mtblNodeData.FieldByName('PayoutDirty').AsCurrency);
        JObj.AddPair('PayoutCleanly', mtblNodeData.FieldByName('PayoutCleanly').AsCurrency);
        JObj.AddPair('EstimatedDirty', mtblNodeData.FieldByName('EstimatedDirty').AsCurrency);
        JObj.AddPair('EstimatedCleanly', mtblNodeData.FieldByName('EstimatedCleanly').AsCurrency);
        JObj.AddPair('Version', mtblNodeData.FieldByName('Version').AsString);
        JObj.AddPair('Efficiency', mtblNodeData.FieldByName('Efficiency').AsCurrency);
        JObj.AddPair('HDDVolume', mtblNodeData.FieldByName('HDDVolume').AsString);
        JObj.AddPair('NodeID', mtblNodeData.FieldByName('NodeID').AsString);
        JObj.AddPair('Trash', mtblNodeData.FieldByName('Trash').AsInteger);
        JObj.AddPair('FullPath', mtblNodeData.FieldByName('FullPath').AsString);
        JObj.AddPair('TotalBandwidth', mtblNodeData.FieldByName('TotalBandwidth').AsFloat);
        JObj.AddPair('DailyEgress', mtblNodeData.FieldByName('DailyEgress').AsFloat);
        JObj.AddPair('DailyIngress', mtblNodeData.FieldByName('DailyIngress').AsFloat);
        JObj.AddPair('DailyRepairIngress', mtblNodeData.FieldByName('DailyRepairIngress').AsFloat);
        JObj.AddPair('DailyRepairEgress', mtblNodeData.FieldByName('DailyRepairEgress').AsFloat);
        JObj.AddPair('DailyTotalBandwidth', mtblNodeData.FieldByName('DailyTotalBandwidth').AsFloat);
        JObj.AddPair('UptimeMinutes', mtblNodeData.FieldByName('UptimeMinutes').AsInteger);
        JObj.AddPair('Wallet', mtblNodeData.FieldByName('Wallet').AsString);
        JObj.AddPair('DailyTBMonth', mtblNodeData.FieldByName('DailyTBMonth').AsFloat);

        mtblNodeData.Next;
      end;
    finally
      mtblNodeData.EnableControls;
    end;

    for var i := 0 to NodesArray.Count - 1 do begin
      var JObj := NodesArray.Items[i] as TJSONObject;
       CalcParent(JObj);
    end;

    SaveToDB(NodesArray.ToString);

    //Log(NodesArray.ToString);
  finally
    NodesArray.Free;
  end;
end;

procedure TDM.SetNodeBoolValueByFieldInJArrayActiveOnly(const aNode: TNode; aValue: Boolean; aField: String);
begin
  var JObj := GetJNode(fNodesJArrayActiveOnly, aNode.FullPath);
  if not Assigned(JObj) then
    Exit;

  var Pair := JObj.Get(aField);

  if Assigned(Pair) then
    Pair.JsonValue := TJSONBool.Create(aValue)
  else
    JObj.AddPair(aField, aValue);
end;

procedure TDM.timerPCRebootersTimer(Sender: TObject);
begin
  if GetSettings.IsEnabledPCRebooter then
    for var PCRebooter in GetSettings.PCRebooters do
      if PCRebooter.IsActive then
        SendEchoAsync(PCRebooter, PCRebooterEchoCallback);
end;

procedure TDM.tmrNodesBufferTimer(Sender: TObject);
begin
  if Length(fNodesBuffer) > 0 then
    DoCleanNodesBuffer;
end;

procedure TDM.tmrReloadNodesTimer(Sender: TObject);
begin
  if not IsLoading then
    LoadNodesData(TGetInfoMode.gimStandart, TLoadSourceMode.lsmAuto);
end;

procedure TDM.LoadHistoricalNodesData(aChartWorker: IChartWorker);
var
  ChartDisplaySettings: TChartDisplaySettings;
  DataSet: TFDMemTable;
begin
  fChartWorker := aChartWorker;

  ChartDisplaySettings := aChartWorker.GetChartDisplaySettings;

  DataSet := nil;
  case ChartDisplaySettings.ChartDataType of
    cdtHistorical:  DataSet := mtblChartHistoricalData;
    cdtCurrentMonth: DataSet := mtblChartCurrentMonthData;
    cdtLocalHistory: DataSet := mtblChartLocalHistory;
  end;

  if not Assigned(DataSet) then
    Exit;

  if ChartDisplaySettings.IsUseCache and (DataSet.RecordCount > 0) then begin
    MakeAndFillHistoricalChartData(DataSet.RecordCount > 0);
    Exit;
  end;

  DataSet.EmptyDataSet;

  if ChartDisplaySettings.ChartDataType = TChartDataType.cdtLocalHistory then begin
    GetChartLocalHistory;
  end else begin
    fChartNodeCount := 0;
    var NodesList := GetNodesList;

    fChartTotalNodeCount := Length(NodesList);

    fChartWorker.SetProgressData(fChartTotalNodeCount);

    for var Node in NodesList do
      case ChartDisplaySettings.ChartDataType of
        cdtHistorical: GetHistoricalNodeData(Node, NodeChartDataCallback, ErrorChartNodeDataCallback);
        cdtCurrentMonth: GetCurrentMonthNodeData(Node, NodeChartDataCallback, ErrorChartNodeDataCallback);
      end;
  end;
end;

function TDM.LoadHistoryData(aID: Integer): TLocalHistoryData;
  procedure AddJArrayIntoGrid(aJArr: TJSONArray);
  begin
    for var i := 0 to aJArr.Count -1 do begin
      var JObj := aJArr.Items[i] as TJSONObject;

      if not JObj.GetValue<String>('Title', '').IsEmpty and not JObj.GetValue<String>('Port', '').IsEmpty then begin
        mtblLocalHistoryGrid.Append;
        mtblLocalHistoryGrid['Title'] := JObj.GetValue<String>('Title', '');
        mtblLocalHistoryGrid['Address'] := JObj.GetValue<String>('Address', '');
        mtblLocalHistoryGrid['Port'] := JObj.GetValue<Integer>('Port', 0);
        mtblLocalHistoryGrid['TBM'] := JObj.GetValue<Double>('TBM', 0.0);
        mtblLocalHistoryGrid['Ingress'] := JObj.GetValue<Double>('Ingress', 0.0);
        mtblLocalHistoryGrid['Egress'] := JObj.GetValue<Double>('Egress', 0.0);
        mtblLocalHistoryGrid['RepairIngress'] := JObj.GetValue<Double>('RepairIngress', 0.0);
        mtblLocalHistoryGrid['RepairEgress'] := JObj.GetValue<Double>('RepairEgress', 0.0);
        mtblLocalHistoryGrid['AllocatedSpace'] := JObj.GetValue<Integer>('AllocatedSpace', 0);
        mtblLocalHistoryGrid['FreeSpace'] := JObj.GetValue<Integer>('FreeSpace', 0);
        mtblLocalHistoryGrid['UsedSpace'] := JObj.GetValue<Integer>('UsedSpace', 0);
        mtblLocalHistoryGrid['PayoutDirty'] := JObj.GetValue<Currency>('PayoutDirty', 0);
        mtblLocalHistoryGrid['PayoutCleanly'] := JObj.GetValue<Currency>('PayoutCleanly', 0);
        mtblLocalHistoryGrid['EstimatedDirty'] := JObj.GetValue<Currency>('EstimatedDirty', 0);
        mtblLocalHistoryGrid['EstimatedCleanly'] := JObj.GetValue<Currency>('EstimatedCleanly', 0);
        mtblLocalHistoryGrid['Version'] := JObj.GetValue<String>('Version', '');
        mtblLocalHistoryGrid['Efficiency'] := JObj.GetValue<Currency>('Efficiency', 0);
        mtblLocalHistoryGrid['HDDVolume'] := JObj.GetValue<String>('HDDVolume', '');
        mtblLocalHistoryGrid['NodeID'] := JObj.GetValue<String>('NodeID', '');
        mtblLocalHistoryGrid['Trash'] := JObj.GetValue<Integer>('Trash', 0);
        mtblLocalHistoryGrid['FullPath'] := JObj.GetValue<String>('FullPath', '');
        mtblLocalHistoryGrid['TotalBandwidth'] := JObj.GetValue<Currency>('TotalBandwidth', 0);
        mtblLocalHistoryGrid['DailyEgress'] := JObj.GetValue<Double>('DailyEgress', 0.0);
        mtblLocalHistoryGrid['DailyIngress'] := JObj.GetValue<Double>('DailyEgress', 0.0);
        mtblLocalHistoryGrid['DailyRepairIngress'] := JObj.GetValue<Double>('DailyRepairIngress', 0.0);
        mtblLocalHistoryGrid['DailyRepairEgress'] := JObj.GetValue<Double>('DailyRepairEgress', 0.0);
        mtblLocalHistoryGrid['DailyTotalBandwidth'] := JObj.GetValue<Double>('DailyTotalBandwidth', 0.0);
        mtblLocalHistoryGrid['DailyTBMonth'] := JObj.GetValue<Double>('DailyTBMonth', 0.0);
        mtblLocalHistoryGrid['Wallet'] := JObj.GetValue<String>('NodeID', '');
        mtblLocalHistoryGrid['UptimeMinutes'] := JObj.GetValue<Integer>('UptimeMinutes', 0);
        mtblLocalHistoryGrid.Post;
      end;

      var Childs := JObj.GetValue<TJSONArray>('Childs', nil);
      if Assigned(Childs) then
        AddJArrayIntoGrid(Childs);
    end;
  end;

begin
  var Q := TFDQuery.Create(nil);
  try
    Q.Connection := FDConnection;
    Q.Open('select * from LOCAL_HISTORY where LOCAL_HISTORY_ID = :ID', [aID]);
    if not Q.IsEmpty then begin
      Result.CreationDT := Q.FieldByName('CREATION_DT').AsDateTime;
      Result.Data := Q.FieldByName('Data').AsString;
    end;
  finally
    Q.Free;
  end;

  mtblLocalHistoryGrid.DisableControls;
  try
    mtblLocalHistoryGrid.Close;
    mtblLocalHistoryGrid.Open;

    if Result.Data.IsEmpty then
      Exit;

    var JArr := TJSONValue.ParseJSONValue(Result.Data) as TJSONArray;
    try
      if not Assigned(JArr) or (JArr.Count = 0) then
        Exit;

      AddJArrayIntoGrid(JArr);
    finally
      JArr.Free;
    end;
  finally
    mtblLocalHistoryGrid.EnableControls;
  end;
end;

procedure TDM.LoadNeighbours;
var
  StrList: TStringList;
begin
  if not Assigned(fNodesJArray) then
    ReloadFromJSON;

  var NodesList := GetNodesList;

  SetNeighboursLastUpdated(Now);

  StrList := TStringList.Create;
  try
    for var Node in NodesList do
      if StrList.IndexOf(Node.Address) < 0 then begin
        GetNeighboursASync(Node.Address, NeighboursDataCallback, NeighboursErrorCallback);
        StrList.Add(Node.Address);
      end;
  finally
    StrList.Free;
  end;
end;

procedure TDM.LoadNodeData(const aNode: TNode);
begin
  GetNodeData(aNode, NodeDataCallback, ErrorNodeDataCallback, TGetInfoMode.gimStandart);
end;

procedure TDM.LoadNodesData(const aGetInfoMode: TGetInfoMode; const aLoadSourceMode: TLoadSourceMode);
var
  Data: TBeforeStartGetNodeData;
begin
  if fIsPausedRefresh then
    Exit;

  if not Assigned(fNodesJArray) then
    ReloadFromJSON;

  fIsPausedRefresh := true;
  TTask.Run(AsyncPause).Start;

  UpdateLastestStorjVersion;
  UpdateStorjNetworkData;

  var NodesList := GetNodesList;

  fTotalNodeCount := Length(NodesList);
  fNodeCount := 0;
  fSuccessNodeCount := 0;
  fErrorNodeCount := 0;

  Data.TotalCount := fTotalNodeCount;
  if fTotalNodeCount > 0 then
    TNotifyCenter.Instance.SendMessage(COMMAND_BEFORE_START_GET_NODE, TValue.From<TBeforeStartGetNodeData>(Data));

  mtblRestartUsedDiffer.EmptyDataSet;
  TNotifyCenter.Instance.SendMessage(COMMAND_RESTART_USED_DIFFER_COUNT, 0);

  fLoadSourceMode := aLoadSourceMode;
  for var Node in NodesList do
    GetNodeData(Node, NodeDataCallback, ErrorNodeDataCallback, aGetInfoMode);
end;

procedure TDM.LoadTotalEarned;
begin
  if not Assigned(fNodesJArray) then
    ReloadFromJSON;

  var NodesList := GetNodesList;

  for var Node in NodesList do
    GetTotalEarned(Node, NodeTotalEarnedDataCallback, ErrorTotalEarnedNodeDataCallback);
end;

procedure TDM.LocateDifferHistoryFromCurrent;
var
  Title: String;
begin
  Title := mtblRestartUsedDiffer.FieldByName('Title').AsString;

  qryRestartUsedDifferHistory.Locate('Title', Title, []);
end;

procedure TDM.Log(const aStr: String);
begin
  TNotifyCenter.Instance.SendMessage(COMMAND_LOG, aStr);
end;

procedure TDM.MakeAndFillHistoricalChartData(const aIsUseCache: Boolean = False);
var
  FullPath: String;
  DT: TDateTime;
  ChartDisplaySettings: TChartDisplaySettings;
  Fields: TArray<String>;
  IsNeedAddRecord: Boolean;
  DataSet: TFDMemTable;
begin
  ChartDisplaySettings := fChartWorker.GetChartDisplaySettings;

  DataSet := nil;
  case ChartDisplaySettings.ChartDataType of
    cdtHistorical: DataSet := mtblChartHistoricalData;
    cdtCurrentMonth: DataSet := mtblChartCurrentMonthData;
    cdtLocalHistory: DataSet := mtblChartLocalHistory;
  end;

  if not Assigned(DataSet) then
    Exit;

  if not aIsUseCache and (ChartDisplaySettings.ChartDataType <> TChartDataType.cdtLocalHistory) then begin
    FillChartParents(DataSet);
    CalcChartParents(DataSet);
  end;

  Fields := fChartWorker.GetDisplayFields;

  mtblChartData.DisableControls;
  try
    mtblChartData.Active := False;
    mtblChartData.FieldDefs.Clear;
    mtblChartData.FieldDefs.Add('Category', TFieldType.ftDateTime);
    for var Item in Fields do
      mtblChartData.FieldDefs.Add(Item, ChartDisplaySettings.FieldType);

    mtblChartData.Active := True;

    DataSet.First;
    while not DataSet.Eof do begin
      FullPath := DataSet.FieldByName('FullPath').AsString;
      if MatchStr(FullPath, Fields) then begin
        DT := DataSet.FieldByName('Category').AsDateTime;
        IsNeedAddRecord := True;
        if (ChartDisplaySettings.ChartDataType = TChartDataType.cdtHistorical) and ((DT < ChartDisplaySettings.DateFrom) or (DT > ChartDisplaySettings.DateTo)) then
          IsNeedAddRecord := False;

        if IsNeedAddRecord then begin
          if not mtblChartData.Locate('Category', DT) then begin
            mtblChartData.Append;
            mtblChartData['Category'] := DT;
          end else
            mtblChartData.Edit;

          mtblChartData[FullPath] := DataSet[ChartDisplaySettings.FieldName];
          mtblChartData.Post;
        end;
      end;

      DataSet.Next;
    end;

  finally
    mtblChartData.EnableControls;
  end;

  fChartWorker.DisplayChart;
end;

end.
