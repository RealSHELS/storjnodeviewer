unit Storj.Email.Sender;

interface

uses
  System.SysUtils, System.Classes, System.Threading,
  IdSMTP, IdMessage, IdSSLOpenSSL, IdExplicitTLSClientServerBase;

  procedure SendEmail(const aReceiver, aMessage, aLogin, aPwd: String);
  procedure SendEmailAsync(const aReceiver, aMessage, aLogin, aPwd: String);

implementation

procedure SendEmail(const aReceiver, aMessage, aLogin, aPwd: String);
var
  SMTP: TIdSMTP;
  Msg: TIdMessage;
  IO: TIdSSLIOHandlerSocketOpenSSL;
begin
  SMTP := TIdSMTP.Create(nil);
  try
    SMTP.Host := 'smtp.gmail.com';
    SMTP.Port := 587;

    SMTP.AuthType := TIdSMTPAuthenticationType.satDefault;
    SMTP.Username := aLogin;
    SMTP.Password := aPwd;

    IO := TIdSSLIOHandlerSocketOpenSSL.Create(SMTP);
    IO.SSLOptions.SSLVersions := [sslvTLSv1, sslvTLSv1_1, sslvTLSv1_2];
    SMTP.IOHandler := IO;
    SMTP.UseTLS := TIdUseTLS.utUseExplicitTLS;

    SMTP.Connect;

    if SMTP.Connected then begin
      Msg := TIdMessage.Create(SMTP);

      Msg.From.Address := 'StorjNodeViewer';
      Msg.Recipients.EMailAddresses := aReceiver;
      Msg.Body.Text := aMessage;
      Msg.ContentType := 'text/html';
      Msg.CharSet := 'UTF-8';
      Msg.Subject := 'Notify from StorjNodeViewer';

      SMTP.Send(Msg);
    end;
  finally
    SMTP.Free;
  end;
end;

procedure SendEmailAsync(const aReceiver, aMessage, aLogin, aPwd: String);
var
  Proc: TProc;
begin
  Proc := procedure
          begin
            try
              SendEmail(aReceiver, aMessage, aLogin, aPwd);
            except
            end;
          end;

  TTask.Create(Proc).Start;
end;

end.
