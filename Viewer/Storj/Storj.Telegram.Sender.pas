unit Storj.Telegram.Sender;

interface

uses
  Storj.Settings,
  uTelegramAPI.Interfaces, uTelegramAPI;

  procedure SendTelegramMessage(const aToken, aUserID, aMessage: String);

implementation

procedure SendTelegramMessage(const aToken, aUserID, aMessage: String);
var
  Telegram: ITelegramAPI;
begin
  Telegram := TTelegramAPI.New;
  Telegram.SetUserID(aUserID).SetBotToken(aToken);
  Telegram.SendMsg(aMessage);
end;

end.
