unit PCRebooter.ForWebserver;

interface

uses
  System.Threading,
  System.Net.HTTPClient,
  System.SysUtils,
  System.Classes,
  System.JSON,
  System.Math,
  Storj.Types,
  Storj.Consts,
  System.Generics.Collections,
  Storj.Settings;

  procedure SendEchoAsync(aRebooter: TPCRebooterSettings; aCallback: TProc<TPCRebooterSettings, Boolean>);
  function SendEcho(aRebooter: TPCRebooterSettings): Boolean;

 // procedure SendRebootAsync();
  function SendReboot(aRebooter: TPCRebooterSettings; aNumber: Integer): Boolean;

implementation

procedure SendEchoAsync(aRebooter: TPCRebooterSettings; aCallback: TProc<TPCRebooterSettings, Boolean>);
var
  Proc: TProc;
begin
  Proc :=
    procedure
    begin
      var res := SendEcho(aRebooter);
      TThread.Queue(nil,
                 procedure
                 begin
                   if Assigned(ACallback) then
                    ACallback(aRebooter, res);
                 end);
    end;

  TTask.Create(Proc).Start;
end;

function SendEcho(aRebooter: TPCRebooterSettings): Boolean;
var
  Resp: IHTTPResponse;
begin
  try
    var HTTP := THTTPClient.Create;
    try
      HTTP.SendTimeout := 1000;
      HTTP.ConnectionTimeout := 1000;
      HTTP.ResponseTimeout := 1000;
      HTTP.CustomHeaders['Authorization'] := aRebooter.AuthKey;

      Resp := HTTP.Get('http://' + aRebooter.URL + '/echo');

      Result := Resp.StatusCode = 201;
    finally
      HTTP.Free;
    end;
  except
    Result := False;
  end;
end;

function SendReboot(aRebooter: TPCRebooterSettings; aNumber: Integer): Boolean;
var
  Resp: IHTTPResponse;
begin
  try
    var HTTP := THTTPClient.Create;
    try
      HTTP.SendTimeout := 1000;
      HTTP.ConnectionTimeout := 1000;
      HTTP.ResponseTimeout := 1000;
      HTTP.CustomHeaders['Authorization'] := aRebooter.AuthKey;

      Resp := HTTP.Get('http://' + aRebooter.URL + '/rebootpc?n=' + aNumber.ToString);

      Result := Resp.StatusCode = 202;
    finally
      HTTP.Free;
    end;
  except
    Result := False;
  end;
end;

end.
