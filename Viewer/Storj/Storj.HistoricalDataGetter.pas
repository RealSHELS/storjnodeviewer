unit Storj.HistoricalDataGetter;

interface

uses
  System.Threading,
  System.Net.HTTPClient,
  System.SysUtils,
  System.Classes,
  System.JSON,
  System.Math,
  Storj.Types,
  Storj.Consts,
  System.Generics.Collections;

  procedure GetHistoricalNodeData(aNode: TNode; ACallback: TChartDataCallback; AErrorCallback: TErrorCallback);

implementation

uses
  System.DateUtils, WinAPI.Windows, System.Diagnostics,
  Storj.Settings;

procedure GetHistoricalNodeData(aNode: TNode; ACallback: TChartDataCallback; AErrorCallback: TErrorCallback);
var
  Proc: TProc;
begin
  Proc := procedure
          var
            NodeData: TChartNodeData;
            HTTP: THTTPClient;
            Resp: IHTTPResponse;
            JObj: TJSONObject;
            JArr: TJSONArray;
            i: integer;
            E: Exception;
            SW: TStopWatch;
            Result: TArray<TChartNodeData>;
            DT: TDateTime;
            FS: TFormatSettings;
          begin
            SW := TStopWatch.StartNew;

            try
              HTTP := THTTPClient.Create;
              try
                HTTP.ResponseTimeout := GetSettings.Timeout;
                HTTP.ConnectionTimeout := GetSettings.Timeout;
                HTTP.SendTimeout := GetSettings.Timeout;
                Result := [];

                Resp := HTTP.Get('http://' + ANode.Address + ':' + ANode.Port.ToString +
                  '/api/heldamount/paystubs/2000-01/' + FormatDateTime('yyyy-mm', Now));

                JArr := TJSONObject.ParseJSONValue(Resp.ContentAsString) as TJSONArray;
                if not Assigned(JArr) then begin
                  TThread.Queue(nil,
                     procedure
                     begin
                       if Assigned(ACallback) then
                         ACallback(Result);
                     end);
                  Exit;
                end;

                try
                  FS := TFormatSettings.Create;
                  FS.DateSeparator := '-';
                  FS.ShortDateFormat := 'yyyy-mm-dd';
                  FS.TimeSeparator := ':';
                  FS.ShortTimeFormat := 'hh:nn';
                  FS.LongTimeFormat := 'hh:nn:ss';


                  for i := 0 to JArr.Count -1 do begin
                    JObj := JArr.Items[i] as TJSONObject;

                    DT := StrToDate(JObj.GetValue('period').Value + '-01', FS);

                    if (Length(Result) = 0) or (Result[Length(Result) - 1].DT <> DT) then begin
                      NodeData := TChartNodeData.Create;
                      NodeData.Node := aNode;
                      NodeData.DT := DT;
                      Result := Result + [NodeData];
                    end else
                      NodeData := Result[Length(Result) - 1];

                    NodeData.Payment := NodeData.Payment + JObj.GetValue<Integer>('paid', 0) / 1000000;
                    NodeData.TBMonth := NodeData.TBMonth + JObj.GetValue<Double>('usageAtRest', 0) / TERABYTE;
                    NodeData.Egress := NodeData.Egress + JObj.GetValue<Int64>('usageGet', 0) / GIGABYTE;
                    NodeData.RepairEgress := NodeData.RepairEgress + JObj.GetValue<Int64>('usageGetRepair', 0) / GIGABYTE;
                    NodeData.TotalEgress := NodeData.Egress + NodeData.RepairEgress;

                    if NodeData.TBMonth > 0 then
                      NodeData.Efficiency := NodeData.Payment / NodeData.TBMonth;

                    Result[Length(Result) - 1] := NodeData;
                  end;
                finally
                  JArr.Free;
                end;
              finally
                HTTP.Free;
              end;

              SW.Stop;

              TThread.Queue(nil,
                 procedure
                 begin
                   if Assigned(ACallback) then
                     ACallback(Result);
                 end);
            except
              SW.Stop;
              E := Exception(AcquireExceptionObject);
              try
                TThread.Synchronize(nil,
                 procedure
                 begin
                   AErrorCallback(aNode, E);
                 end);
              finally
                E.Free;
              end;
            end;
          end;

  TTask.Create(Proc).Start;
end;

end.
