unit Storj.NeighboursGetter;

interface

uses
  System.Threading,
  System.Net.HTTPClient,
  System.SysUtils,
  System.Classes,
  System.JSON,
  System.Math,
  Storj.Types,
  Storj.Consts,
  System.Generics.Collections,
  System.RegularExpressions,
  System.Net.URLClient;

  function GetNeighboursSync(aHost: String): Integer;
  procedure GetNeighboursASync(const aHost: String; ACallback: TNeighboursCallback; AErrorCallback: TNeighboursErrorCallback);

implementation

uses
  VCL.Dialogs, System.DateUtils,
  Storj.Settings;

function IsLocalHost(aText: String): Boolean;
begin
  Result := AText.ToLower.Equals('localhost');
end;

function IsURL(aText: String): Boolean;
begin
  var ipRegExp := '\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b';

  Result := not TRegEx.IsMatch(aText, ipRegExp);
end;

function GetIPByDNS(const aURL: String): String;
var
  Resp: IHTTPResponse;
  JStr: String;
begin
  Result := '';

  var HTTP := THTTPClient.Create;
  try
    HTTP.CustomHeaders['Accept'] := 'application/dns-json';
    Resp := HTTP.Get('https://cloudflare-dns.com/dns-query?name=' + aURL + '&type=A');
    JStr := Resp.ContentAsString;
    var JSON := TJSONObject.ParseJSONValue(JStr) as TJSONObject;
    if not Assigned(JSON) then
        Exit;
    try
      if Assigned(JSON.Values['Status']) and (TJSONNumber(JSON.Values['Status']).AsInt = 0) then begin
        var JArr := JSON.Values['Answer'] as TJSONArray;
        if JArr.Count > 0 then
          Result := (JArr[0] as TJSONObject).Values['data'].Value;
      end;
    finally
      JSON.Free;
    end;
  finally
    HTTP.Free;
  end;
end;

function IsLocalIP(aIP: String): Boolean;
begin
  Result := aIP.StartsWith('192.168.') or aIP.StartsWith('10.') or aIP.StartsWith('127.0.');
end;

function GetExternalIP: String;
begin
  var HTTP := THTTPClient.Create;
  try
    Result := HTTP.Get('http://ipinfo.io/ip').ContentAsString;
  finally
    HTTP.Free;
  end;
end;

function GetNeighboursCount(aIP: String): Integer;
begin
  Result := 0;

  var HTTP := THTTPClient.Create;
  try
    if GetSettings.IsUseProxyForGetNeighbours then begin
      var Proxy := TProxySettings.Create('134.209.67.109', 26000);
      HTTP.ProxySettings := Proxy;
    end;

    var JStr := HTTP.Get('http://storjnet.info/api/neighbors/' + aIP).ContentAsString;
    var JSON := TJSONObject.ParseJSONValue(JStr) as TJSONObject;
    if not Assigned(JSON) then
      Exit;

    try
      if Assigned(JSON.Values['ok']) and TJSONBool(JSON.Values['ok']).AsBoolean then
        Result := TJSONNumber(TJSONObject(JSON.Values['result']).Values['count']).AsInt;
    finally
      JSON.Free;
    end;
  finally
    HTTP.Free;
  end;
end;

function GetNeighboursSync(aHost: String): Integer;
begin
  if aHost.Trim.IsEmpty then
    raise Exception.Create('IP/URL is empty');

  if IsLocalHost(aHost) then
    aHost := '127.0.0.1';

  if IsURL(aHost) then
    aHost := GetIPByDNS(aHost);

  if IsLocalIP(aHost) then
    aHost := GetExternalIP;

  Result := GetNeighboursCount(aHost);
end;

procedure GetNeighboursASync(const aHost: String; ACallback: TNeighboursCallback; AErrorCallback: TNeighboursErrorCallback);
var
  Proc: TProc;
begin
  Proc := procedure
          var
            NodeData: TNeighboursData;
            IP: String;
            E: Exception;
          begin
            NodeData.Host := aHost;
            NodeData.Neighbours := 0;
            IP := aHost;

            try
              if IP.Trim.IsEmpty then
                raise Exception.Create('IP/URL is empty');

              if IsLocalHost(IP) then
                IP := '127.0.0.1';

              if IsURL(IP) then
                IP := GetIPByDNS(IP);

              if IsLocalIP(IP) then
                IP := GetExternalIP;

              NodeData.Subnet := IP.Substring(0, IP.LastIndexOf('.') + 1) + 'X';

              NodeData.Neighbours := GetNeighboursCount(IP);

              TThread.Queue(nil,
                 procedure
                 begin
                   if Assigned(ACallback) then
                     ACallback(NodeData);
                 end);
            except
              E := Exception(AcquireExceptionObject);
              try
                TThread.Synchronize(nil,
                 procedure
                 begin
                   AErrorCallback(aHost, E);
                 end);
              finally
                E.Free;
              end;
            end;
          end;

  TTask.Create(Proc).Start;
end;

end.
