unit Storj.StorjNetworkDataGetter;

interface

uses
  System.Threading,
  System.Net.HTTPClient,
  System.SysUtils,
  System.Classes,
  System.JSON,
  System.Math,
  Storj.Types,
  Storj.Consts,
  System.Generics.Collections;

  procedure GetStorjNetworkDataAsync(aCallback: TStorjNetworkDataCallback);
  function GetStorjNetworkData: TStorjNetworkData;

implementation

procedure GetStorjNetworkDataAsync(aCallback: TStorjNetworkDataCallback);
var
  Proc: TProc;
begin
  Proc :=
    procedure
    begin
      var res := GetStorjNetworkData;
      TThread.Queue(nil,
                 procedure
                 begin
                   if Assigned(aCallback) then
                    aCallback(res);
                 end);
    end;

  TTask.Create(Proc).Start;
end;

function GetStorjNetworkData: TStorjNetworkData;
var
  Resp: IHTTPResponse;
  JSON: TJSONObject;
  i: integer;
  StorageTotalBytes: Int64;
begin
  Result.UsedSpace := 0;

  JSON := nil;
  try
    var HTTP := THTTPClient.Create;
    try
      HTTP.SendTimeout := 1000;
      HTTP.ConnectionTimeout := 1000;
      HTTP.ResponseTimeout := 1000;

      Resp := HTTP.Get('https://stats.storjshare.io/data.json');
      JSON := TJSONObject.ParseJSONValue(Resp.ContentAsString) as TJSONObject;

      for i := 0 to JSON.Count - 1 do begin
        var JObj := JSON.Pairs[i].JsonValue as TJSONObject;
        StorageTotalBytes := 0;
        if not JObj.Values['storage_total_bytes'].Null then
          StorageTotalBytes := JObj.GetValue<Int64>('storage_total_bytes', 0);

        var SatelliteUsed := Round(StorageTotalBytes / GIGABYTE);
        Result.UsedSpace := Result.UsedSpace + SatelliteUsed;
      end;
    finally
      JSON.Free;
      HTTP.Free;
    end;
  except

  end;
end;

end.
