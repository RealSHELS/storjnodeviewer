unit Storj.CurrentMonthDataGetter;

interface

uses
  System.Threading,
  System.Net.HTTPClient,
  System.SysUtils,
  System.Classes,
  System.JSON,
  System.Math,
  Storj.Types,
  Storj.Consts,
  System.Generics.Collections;

  procedure GetCurrentMonthNodeData(aNode: TNode; ACallback: TChartDataCallback; AErrorCallback: TErrorCallback);

implementation

uses
  System.DateUtils, WinAPI.Windows, System.Diagnostics,
  Storj.Settings;

procedure GetCurrentMonthNodeData(aNode: TNode; ACallback: TChartDataCallback; AErrorCallback: TErrorCallback);
var
  Proc: TProc;
begin
  Proc := procedure
          var
            NodeData: TChartNodeData;
            E: Exception;
            Result: TArray<TChartNodeData>;
            DT: TDateTime;
          begin
            var SW := TStopWatch.StartNew;

            try
              var HTTP := THTTPClient.Create;
              try
                HTTP.ResponseTimeout := GetSettings.Timeout;
                HTTP.ConnectionTimeout := GetSettings.Timeout;
                HTTP.SendTimeout := GetSettings.Timeout;

                var Resp := HTTP.Get('http://' + ANode.Address + ':' + ANode.Port.ToString + '/api/sno/satellites');

                var JSON := TJSONObject.ParseJSONValue(Resp.ContentAsString) as TJSONObject;
                if not Assigned(JSON) then
                  Exit;

                try
                  var FS := TFormatSettings.Create;
                  FS.DateSeparator := '-';
                  FS.ShortDateFormat := 'yyyy-mm-dd';
                  FS.TimeSeparator := ':';
                  FS.ShortTimeFormat := 'hh:nn';
                  FS.LongTimeFormat := 'hh:nn:ss';

                  Result := [];
                  var JArrBandwidthDaily := JSON.GetValue('bandwidthDaily') as TJSONArray;
                  var JArrStorageDaily := JSON.GetValue('storageDaily') as TJSONArray;
                  if Assigned(JArrBandwidthDaily) then begin
                    for var i := 0 to JArrBandwidthDaily.Count -1 do begin
                      var JObjBandwidthDaily := JArrBandwidthDaily.Items[i] as TJSONObject;

                      var strDT := JObjBandwidthDaily.GetValue('intervalStart').Value;
                      strDT := strDT.Split(['T'])[0];
                      DT := StrToDate(strDT, FS);

                      if (Length(Result) = 0) or (Result[Length(Result) - 1].DT <> DT) then begin
                        NodeData := TChartNodeData.Create;
                        NodeData.Node := aNode;
                        NodeData.DT := DT;
                        Result := Result + [NodeData];
                      end else
                        NodeData := Result[Length(Result) - 1];

                      if Assigned(JArrStorageDaily) and (i < JArrStorageDaily.Count) then begin
                        var JObjStorageDaily := JArrStorageDaily.Items[i] as TJSONObject;
                        NodeData.TBMonth := NodeData.TBMonth + JObjStorageDaily.GetValue<Double>('atRestTotalBytes', 0) / TERABYTE;
                      end else
                        NodeData.TBMonth := 0;

                      NodeData.Egress := NodeData.Egress + JObjBandwidthDaily.GetValue<Int64>('egress.usage', 0) / GIGABYTE;
                      NodeData.RepairEgress := NodeData.RepairEgress + JObjBandwidthDaily.GetValue<Int64>('egress.repair', 0) / GIGABYTE;
                      NodeData.TotalEgress := NodeData.Egress + NodeData.RepairEgress;

                      NodeData.Ingress := NodeData.Ingress + JObjBandwidthDaily.GetValue<Int64>('ingress.usage', 0) / GIGABYTE;
                      NodeData.RepairIngress := NodeData.RepairIngress + JObjBandwidthDaily.GetValue<Int64>('ingress.repair', 0) / GIGABYTE;
                      NodeData.TotalIngress := NodeData.Ingress + NodeData.RepairIngress;

                      NodeData.TotalBandwidth := NodeData.TotalEgress + NodeData.TotalIngress;

                      Result[Length(Result) - 1] := NodeData;
                    end;
                  end;

                finally
                  JSON.Free;
                end;
              finally
                HTTP.Free;
              end;

              SW.Stop;

              TThread.Queue(nil,
                 procedure
                 begin
                   if Assigned(ACallback) then
                     ACallback(Result);
                 end);
            except
              SW.Stop;
              E := Exception(AcquireExceptionObject);
              try
                TThread.Synchronize(nil,
                 procedure
                 begin
                   AErrorCallback(aNode, E);
                 end);
              finally
                E.Free;
              end;
            end;
          end;

  TTask.Create(Proc).Start;
end;

end.
