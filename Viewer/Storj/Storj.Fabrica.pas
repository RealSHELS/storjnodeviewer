unit Storj.Fabrica;

interface

uses
  Storj.Types,
  Storj.LockBoxAES256PasswordTransformer;

  function GetPasswordTransformer(const aName: String): IPasswordTransformer;

implementation

function GetPasswordTransformer(const aName: String): IPasswordTransformer;
begin
  Result := nil;
  if aName = 'LockBoxAES256' then
    Result := TLockBoxAES256PasswordTransformer.Create;

  if Result = nil then
    raise EFabricaUnknownName.Create('Password transformer "' + aName + '" not found');
end;

end.
