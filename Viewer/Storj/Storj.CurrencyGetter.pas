unit Storj.CurrencyGetter;

interface

uses
  System.Threading,
  System.Net.HTTPClient,
  System.SysUtils,
  System.Classes,
  System.JSON,
  System.Math,
  Storj.Types,
  Storj.Consts,
  System.Generics.Collections;

  procedure GetCurrenciesRatesAsync(ACallback: TCurrenciesRatesCallback);
  function GetCurrenciesRates: TCurrencyRates;

implementation

procedure GetCurrenciesRatesAsync(ACallback: TCurrenciesRatesCallback);
var
  Proc: TProc;
begin
  Proc :=
    procedure
    begin
      var res := GetCurrenciesRates;
      TThread.Queue(nil,
                 procedure
                 begin
                   if Assigned(ACallback) then
                    ACallback(res);
                 end);
    end;

  TTask.Create(Proc).Start;
end;

function GetCurrenciesRates: TCurrencyRates;
var
  Resp: IHTTPResponse;
  JSON: TJSONObject;
begin
  Result := TCurrencyRates.Create(Now);

  JSON := nil;
  try
    var HTTP := THTTPClient.Create;
    try
      HTTP.SendTimeout := 1000;
      HTTP.ConnectionTimeout := 1000;
      HTTP.ResponseTimeout := 1000;

      Resp := HTTP.Get('http://realshels.zapto.org:3000/api/rates');
      JSON := TJSONObject.ParseJSONValue(Resp.ContentAsString) as TJSONObject;
      var JRates := JSON.Values['rates'] as TJSONObject;
      for var Currency in CURRENCIES do begin
        var JRate := JRates.Values[Currency] as TJSONObject;
        var Rate := 1 / JRate.GetValue<Double>('rate');
        Result.RatesToUSD.Add(Currency, Rate);
      end;
    finally
      JSON.Free;
      HTTP.Free;
    end;
  except

  end;
end;

end.
