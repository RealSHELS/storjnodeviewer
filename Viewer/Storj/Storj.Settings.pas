unit Storj.Settings;

interface

uses
  System.SysUtils, System.JSON, System.Classes, System.Generics.Collections,
  VCL.Forms, VCL.Dialogs;

type
  TPCRebooterSettings = record
    IsActive: Boolean;
    Name: String;
    URL: String;
    Nums: Integer;
    PCNames: String;
    AuthKey: String;
  end;

  TPCRebooterSettingsEchoResult = record
    Settings: TPCRebooterSettings;
    IsOnline: Boolean;
  end;

  TTelegramNotifySettings = record
    IsEnable: Boolean;
    UserID: String;
    Token: String;

    class operator Initialize(out aDest: TTelegramNotifySettings);

    function ToJSON: TJSONObject;
    class function FromJSON(aJObj: TJSONObject): TTelegramNotifySettings; static;
  end;

  TEmailNotifySettings = record
    IsEnable: Boolean;
    Email: String;
    Login: String;
    Password: String;

    class operator Initialize(out aDest: TEmailNotifySettings);

    function ToJSON: TJSONObject;
    class function FromJSON(aJObj: TJSONObject): TEmailNotifySettings; static;
  end;

  TNotifySettings = record
    EmailSettings: TEmailNotifySettings;
    TelegramSettings: TTelegramNotifySettings;

    class operator Initialize(out aDest: TNotifySettings);

    function ToJSON: TJSONObject;
    class function FromJSON(aJObj: TJSONObject): TNotifySettings; static;
  end;

  TSettings = record
    IsColoredFreeSpace: Boolean;
    RedColorValue: Integer;
    YellowColorValue: Integer;
    Timeout: Integer;
    IsMainFormFullScreen: Boolean;

    IsColoredTBMDeviation: Boolean;
    LessThanDeviationPercent: Integer;
    MoreThanDeviationPercent: Integer;

    HealthAuditRed: Integer;
    HealthAuditYellow: Integer;
    HealthOnlineRed: Integer;
    HealthOnlineYellow: Integer;
    HealthSuspensionRed: Integer;
    HealthSuspensionYellow: Integer;

    IsLogHealthAuditRed: Boolean;
    IsLogHealthAuditYellow: Boolean;
    IsLogHealthOnlineRed: Boolean;
    IsLogHealthOnlineYellow: Boolean;
    IsLogHealthSuspensionRed: Boolean;
    IsLogHealthSuspensionYellow: Boolean;

    IsDisplayHealthHotData: Boolean;
    IsDisplayStorjNetUsed: Boolean;

    OnlineRangeMinutes: Integer;

    IsEnabledPCRebooter: Boolean;
    PCRebooters: TArray<TPCRebooterSettings>;

    IsEnableReloadNodesTimer: Boolean;
    ReloadNodesTimerMinutes: Integer;

    IsUseTreePathForLog: Boolean;
    IsUseProxyForGetNeighbours: Boolean;
    IsUseNodeRefreshBuffer: Boolean;

    NotifySettings: TNotifySettings;

    class operator Initialize(out ADest: TSettings);
  end;

  function GetSettings: TSettings;
  procedure LoadSettings;
  procedure SaveSettings(aSettings: TSettings);

implementation

uses
  WinAPI.Windows;

var
  Settings: TSettings;

const
  GLOBAL_SETTINGS_FILENAME = 'GlobalSettings.json';

function GetStringFromResource(const AResName: String): String;
var
  Res: TResourceStream;
  StrList: TStringList;
begin
  Result := '';

  StrList := nil;
  Res := TResourceStream.Create(HInstance, AResName, RT_RCDATA);
  try
    StrList := TStringList.Create;
    StrList.LoadFromStream(Res);
    Result := StrList.Text;
  finally
    StrList.Free;
    Res.Free;
  end;
end;

class operator TSettings.Initialize(out ADest: TSettings);
begin
  ADest.IsColoredFreeSpace := True;
  ADest.RedColorValue := 150;
  ADest.YellowColorValue := 700;
  ADest.Timeout := 10000;
  aDest.IsMainFormFullScreen := False;

  ADest.IsColoredTBMDeviation := True;
  ADest.LessThanDeviationPercent := 10;
  ADest.MoreThanDeviationPercent := 0;

  ADest.HealthAuditRed := 97;
  ADest.HealthAuditYellow := 99;
  ADest.HealthOnlineRed := 80;
  ADest.HealthOnlineYellow := 90;
  ADest.HealthSuspensionRed := 80;
  ADest.HealthSuspensionYellow := 90;

  ADest.IsLogHealthAuditRed := false;
  ADest.IsLogHealthAuditYellow := false;
  ADest.IsLogHealthOnlineRed := false;
  ADest.IsLogHealthOnlineYellow := false;
  ADest.IsLogHealthSuspensionRed := false;
  ADest.IsLogHealthSuspensionYellow := false;

  ADest.IsDisplayHealthHotData := true;
  ADest.IsDisplayStorjNetUsed := false;

  ADest.OnlineRangeMinutes := 5;

  ADest.IsEnabledPCRebooter := false;
  ADest.PCRebooters := [];

  ADest.IsEnableReloadNodesTimer := False;
  ADest.ReloadNodesTimerMinutes := 30;

  ADest.IsUseTreePathForLog := False;
  ADest.IsUseProxyForGetNeighbours := False;
  ADest.IsUseNodeRefreshBuffer := False;
end;

function GetSettings: TSettings;
begin
  Result := Settings;
end;

procedure LoadSettings;
var
  JArr: TJSONArray;
  i: integer;
  PCRebooter: TPCRebooterSettings;
  JObj: TJSONObject;
begin
  var Path := ExtractFilePath(Application.ExeName);

  if not FileExists(Path + GLOBAL_SETTINGS_FILENAME) then
    Exit;

  var StrList := TStringList.Create;
  try
    StrList.LoadFromFile(GLOBAL_SETTINGS_FILENAME);

    var JSON := TJSONObject.ParseJSONValue(StrList.Text);
    if not Assigned(JSON) then
      Exit;

    try
      Settings.IsColoredFreeSpace := JSON.GetValue<Boolean>('IsColoredFreeSpace', Settings.IsColoredFreeSpace);
      Settings.RedColorValue := JSON.GetValue<Integer>('RedColorValue', Settings.RedColorValue);
      Settings.YellowColorValue := JSON.GetValue<Integer>('YellowColorValue', Settings.YellowColorValue);
      Settings.Timeout := JSON.GetValue<Integer>('Timeout', Settings.Timeout);
      Settings.IsMainFormFullScreen := JSON.GetValue<Boolean>('IsMainFormFullScreen', Settings.IsMainFormFullScreen);

      Settings.IsColoredTBMDeviation := JSON.GetValue<Boolean>('IsColoredTBMDeviation', Settings.IsColoredTBMDeviation);
      Settings.LessThanDeviationPercent := JSON.GetValue<Integer>('LessThanDeviationPercent', Settings.LessThanDeviationPercent);
      Settings.MoreThanDeviationPercent := JSON.GetValue<Integer>('MoreThanDeviationPercent', Settings.MoreThanDeviationPercent);

      Settings.HealthAuditRed := JSON.GetValue<Integer>('HealthAuditRed', Settings.HealthAuditRed);
      Settings.HealthAuditYellow := JSON.GetValue<Integer>('HealthAuditYellow', Settings.HealthAuditYellow);
      Settings.HealthOnlineRed := JSON.GetValue<Integer>('HealthOnlineRed', Settings.HealthOnlineRed);
      Settings.HealthOnlineYellow := JSON.GetValue<Integer>('HealthOnlineYellow', Settings.HealthOnlineYellow);
      Settings.HealthSuspensionRed := JSON.GetValue<Integer>('HealthSuspensionRed', Settings.HealthSuspensionRed);
      Settings.HealthSuspensionYellow := JSON.GetValue<Integer>('HealthSuspensionYellow', Settings.HealthSuspensionYellow);

      Settings.IsLogHealthAuditRed := JSON.GetValue<Boolean>('IsLogHealthAuditRed', Settings.IsLogHealthAuditRed);
      Settings.IsLogHealthAuditYellow := JSON.GetValue<Boolean>('IsLogHealthAuditYellow', Settings.IsLogHealthAuditYellow);
      Settings.IsLogHealthOnlineRed := JSON.GetValue<Boolean>('IsLogHealthOnlineRed', Settings.IsLogHealthOnlineRed);
      Settings.IsLogHealthOnlineYellow := JSON.GetValue<Boolean>('IsLogHealthOnlineYellow', Settings.IsLogHealthOnlineYellow);
      Settings.IsLogHealthSuspensionRed := JSON.GetValue<Boolean>('IsLogHealthSuspensionRed', Settings.IsLogHealthSuspensionRed);
      Settings.IsLogHealthSuspensionYellow := JSON.GetValue<Boolean>('IsLogHealthSuspensionYellow', Settings.IsLogHealthSuspensionYellow);

      Settings.IsDisplayHealthHotData := JSON.GetValue<Boolean>('IsDisplayHealthHotData', Settings.IsDisplayHealthHotData);
      Settings.IsDisplayStorjNetUsed := JSON.GetValue<Boolean>('IsDisplayStorjNetUsed', Settings.IsDisplayStorjNetUsed);

      Settings.OnlineRangeMinutes := JSON.GetValue<Integer>('OnlineRangeMinutes', Settings.OnlineRangeMinutes);

      Settings.IsEnableReloadNodesTimer := JSON.GetValue<Boolean>('IsEnableReloadNodesTimer', Settings.IsEnableReloadNodesTimer);
      Settings.ReloadNodesTimerMinutes := JSON.GetValue<Integer>('ReloadNodesTimerMinutes', Settings.ReloadNodesTimerMinutes);

      Settings.IsUseTreePathForLog := JSON.GetValue<Boolean>('IsUseTreePathForLog', Settings.IsUseTreePathForLog);
      Settings.IsUseProxyForGetNeighbours := JSON.GetValue<Boolean>('IsUseProxyForGetNeighbours', Settings.IsUseProxyForGetNeighbours);
      Settings.IsUseNodeRefreshBuffer := JSON.GetValue<Boolean>('IsUseNodeRefreshBuffer', Settings.IsUseNodeRefreshBuffer);

      JObj := JSON.GetValue<TJSONObject>('NotifySettings', nil);
      if Assigned(JObj) then
        Settings.NotifySettings := TNotifySettings.FromJSON(JObj);
      //PCRebooter
      Settings.IsEnabledPCRebooter := JSON.GetValue<Boolean>('IsEnabledPCRebooter', Settings.IsEnabledPCRebooter);
      Settings.PCRebooters := [];

      JArr := JSON.FindValue('PCRebooters') as TJSONArray;
      if Assigned(JArr) then
        for i := 0 to JArr.Count - 1 do begin
          PCRebooter.IsActive := TJSONObject(JArr.Items[i]).GetValue<Boolean>('IsActive');
          PCRebooter.Name := TJSONObject(JArr.Items[i]).GetValue<String>('Name');
          PCRebooter.URL := TJSONObject(JArr.Items[i]).GetValue<String>('URL');
          PCRebooter.Nums := TJSONObject(JArr.Items[i]).GetValue<Integer>('Nums');
          PCRebooter.PCNames := TJSONObject(JArr.Items[i]).GetValue<String>('PCNames', '');
          PCRebooter.AuthKey := TJSONObject(JArr.Items[i]).GetValue<String>('AuthKey');

          Settings.PCRebooters := Settings.PCRebooters + [PCRebooter];
        end;
    finally
      JSON.Free;
    end;
  finally
    StrList.Free;
  end;
end;

procedure SaveSettings(aSettings: TSettings);
var
  JArr: TJSONArray;
  JObj: TJSONObject;
  PCRebooter: TPCRebooterSettings;
begin
  var JSON := TJSONObject.Create;
  try
    JSON.AddPair('IsColoredFreeSpace', TJSONBool.Create(aSettings.IsColoredFreeSpace));
    JSON.AddPair('RedColorValue', TJSONNumber.Create(aSettings.RedColorValue));
    JSON.AddPair('YellowColorValue', TJSONNumber.Create(aSettings.YellowColorValue));
    JSON.AddPair('Timeout', TJSONNumber.Create(aSettings.Timeout));
    JSON.AddPair('IsMainFormFullScreen', TJSONBool.Create(aSettings.IsMainFormFullScreen));

    JSON.AddPair('IsColoredTBMDeviation', TJSONBool.Create(aSettings.IsColoredTBMDeviation));
    JSON.AddPair('LessThanDeviationPercent', TJSONNumber.Create(aSettings.LessThanDeviationPercent));
    JSON.AddPair('MoreThanDeviationPercent', TJSONNumber.Create(aSettings.MoreThanDeviationPercent));

    JSON.AddPair('HealthAuditRed', TJSONNumber.Create(aSettings.HealthAuditRed));
    JSON.AddPair('HealthAuditYellow', TJSONNumber.Create(aSettings.HealthAuditYellow));
    JSON.AddPair('HealthOnlineRed', TJSONNumber.Create(aSettings.HealthOnlineRed));
    JSON.AddPair('HealthOnlineYellow', TJSONNumber.Create(aSettings.HealthOnlineYellow));
    JSON.AddPair('HealthSuspensionRed', TJSONNumber.Create(aSettings.HealthSuspensionRed));
    JSON.AddPair('HealthSuspensionYellow', TJSONNumber.Create(aSettings.HealthSuspensionYellow));
    JSON.AddPair('IsDisplayHealthHotData', TJSONBool.Create(aSettings.IsDisplayHealthHotData));
    JSON.AddPair('IsDisplayStorjNetUsed', TJSONBool.Create(aSettings.IsDisplayStorjNetUsed));

    JSON.AddPair('IsLogHealthAuditRed', TJSONBool.Create(aSettings.IsLogHealthAuditRed));
    JSON.AddPair('IsLogHealthAuditYellow', TJSONBool.Create(aSettings.IsLogHealthAuditYellow));
    JSON.AddPair('IsLogHealthOnlineRed', TJSONBool.Create(aSettings.IsLogHealthOnlineRed));
    JSON.AddPair('IsLogHealthOnlineYellow', TJSONBool.Create(aSettings.IsLogHealthOnlineYellow));
    JSON.AddPair('IsLogHealthSuspensionRed', TJSONBool.Create(aSettings.IsLogHealthSuspensionRed));
    JSON.AddPair('IsLogHealthSuspensionYellow', TJSONBool.Create(aSettings.IsLogHealthSuspensionYellow));

    JSON.AddPair('OnlineRangeMinutes', TJSONNumber.Create(aSettings.OnlineRangeMinutes));

    JSON.AddPair('IsEnableReloadNodesTimer', TJSONBool.Create(aSettings.IsEnableReloadNodesTimer));
    JSON.AddPair('ReloadNodesTimerMinutes', TJSONNumber.Create(aSettings.ReloadNodesTimerMinutes));

    JSON.AddPair('IsUseTreePathForLog', TJSONBool.Create(aSettings.IsUseTreePathForLog));
    JSON.AddPair('IsUseProxyForGetNeighbours', TJSONBool.Create(aSettings.IsUseProxyForGetNeighbours));
    JSON.AddPair('IsUseNodeRefreshBuffer', TJSONBool.Create(aSettings.IsUseNodeRefreshBuffer));

    //PCRebooter
    JSON.AddPair('IsEnabledPCRebooter', TJSONBool.Create(aSettings.IsEnabledPCRebooter));
    JArr := TJSONArray.Create;
    for PCRebooter in aSettings.PCRebooters do begin
      JObj := TJSONObject.Create;

      JObj.AddPair('IsActive', TJSONBool.Create(PCRebooter.IsActive));
      JObj.AddPair('Name', TJSONString.Create(PCRebooter.Name));
      JObj.AddPair('URL', TJSONString.Create(PCRebooter.URL));
      JObj.AddPair('Nums', TJSONNumber.Create(PCRebooter.Nums));
      JObj.AddPair('PCNames', TJSONString.Create(PCRebooter.PCNames));
      JObj.AddPair('AuthKey', TJSONString.Create(PCRebooter.AuthKey));

      JArr.Add(JObj);
    end;
    JSON.AddPair('PCRebooters', JArr);

    JSON.AddPair('NotifySettings', aSettings.NotifySettings.ToJSON);

    var StrList := TStringList.Create;
    try
      var Path := ExtractFilePath(Application.ExeName);

      StrList.Text := JSON.ToString;
      StrList.SaveToFile(Path + GLOBAL_SETTINGS_FILENAME);
    finally
      StrList.Free;
    end;
  finally
    JSON.Free;
  end;

  Settings := aSettings;
end;

{ TEmailNotifySettings }

class function TEmailNotifySettings.FromJSON(aJObj: TJSONObject): TEmailNotifySettings;
begin
  Result.IsEnable := aJObj.GetValue<Boolean>('IsEnable', False);
  Result.Email := aJObj.GetValue<String>('Email', '');
end;

class operator TEmailNotifySettings.Initialize(out aDest: TEmailNotifySettings);
begin
  aDest.IsEnable := False;
  aDest.Email := '';
  aDest.Login := '';
  aDest.Password := '';

  var StrJSONKeys := GetStringFromResource('Keys');
  if not StrJSONKeys.IsEmpty then begin
    var JSONKeys := TJSONObject.ParseJSONValue(StrJSONKeys) as TJSONObject;
    if Assigned(JSONKeys) then begin
      try
        aDest.Login := JSONKeys.GetValue<String>('email.login', '');
        aDest.Password := JSONKeys.GetValue<String>('email.password', '');
      finally
        JSONKeys.Free;
      end;
    end;
  end;
end;

function TEmailNotifySettings.ToJSON: TJSONObject;
begin
  Result := TJSONObject.Create;
  Result.AddPair('IsEnable', TJSONBool.Create(Self.IsEnable));
  Result.AddPair('Email', Self.Email);
end;

{ TTelegramNotifySettings }

class function TTelegramNotifySettings.FromJSON(aJObj: TJSONObject): TTelegramNotifySettings;
begin
  Result.IsEnable := aJObj.GetValue<Boolean>('IsEnable', False);
  Result.UserID := aJObj.GetValue<String>('UserID', '');
end;

class operator TTelegramNotifySettings.Initialize(out aDest: TTelegramNotifySettings);
begin
  aDest.IsEnable := False;
  aDest.UserID := '';
  aDest.Token := '';

  var StrJSONKeys := GetStringFromResource('Keys');
  if not StrJSONKeys.IsEmpty then begin
    var JSONKeys := TJSONObject.ParseJSONValue(StrJSONKeys) as TJSONObject;
    if Assigned(JSONKeys) then begin
      try
        aDest.Token := JSONKeys.GetValue<String>('telegram.botToken', '');
      finally
        JSONKeys.Free;
      end;
    end;
  end;
end;

function TTelegramNotifySettings.ToJSON: TJSONObject;
begin
  Result := TJSONObject.Create;
  Result.AddPair('IsEnable', TJSONBool.Create(Self.IsEnable));
  Result.AddPair('UserID', Self.UserID);
end;

{ TNotifySettings }

class function TNotifySettings.FromJSON(aJObj: TJSONObject): TNotifySettings;
var
  JObj: TJSONObject;
begin
  JObj := aJObj.GetValue<TJSONObject>('EmailSettings', nil);
  if Assigned(JObj) then
    Result.EmailSettings := TEmailNotifySettings.FromJSON(JObj);

  JObj := aJObj.GetValue<TJSONObject>('TelegramSettings', nil);
  if Assigned(JObj) then
    Result.TelegramSettings := TTelegramNotifySettings.FromJSON(JObj);
end;

class operator TNotifySettings.Initialize(out aDest: TNotifySettings);
begin

end;

function TNotifySettings.ToJSON: TJSONObject;
begin
  Result := TJSONObject.Create;
  Result.AddPair('EmailSettings', Self.EmailSettings.ToJSON);
  Result.AddPair('TelegramSettings', Self.TelegramSettings.ToJSON);
end;

initialization
  LoadSettings;

end.
