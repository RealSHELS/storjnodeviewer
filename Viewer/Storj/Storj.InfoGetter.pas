unit Storj.InfoGetter;

interface

uses
  System.Threading,
  System.Net.HTTPClient,
  System.SysUtils,
  System.Classes,
  System.JSON,
  System.Math,
  Storj.Types,
  Storj.Consts,
  System.Generics.Collections;

  procedure GetNodeData(ANode: TNode; ACallback: TDataCallback; AErrorCallback: TErrorCallback; aGetInfoMode: TGetInfoMode);

implementation

uses
  VCL.Dialogs, System.DateUtils, WinAPI.Windows, System.Diagnostics,
  Storj.Settings;

procedure SetBaseNodeData(const AResponseText: String; var ANodedata: TNodeData);
var
  JSON: TJSONObject;
  i: integer;
  LastPinged, NowUTC, StartedAt: TDateTime;
  MinBetween: Integer;
begin
  JSON := TJSONObject.ParseJSONValue(AResponseText) as TJSONObject;
  if not Assigned(JSON) then
    Exit;

  try
    ANodeData.Version := JSON.Values['version'].Value;
    ANodeData.Wallet := JSON.Values['wallet'].Value;
    ANodeData.Node.NodeID := JSON.Values['nodeID'].Value;

    ANodedata.DiskSpace.Used := Round(JSON.GetValue<Int64>('diskSpace.used') / GIGABYTE);
    ANodedata.DiskSpace.Available := Round(JSON.GetValue<Int64>('diskSpace.available') / GIGABYTE);
    ANodedata.DiskSpace.Overused := Round(JSON.GetValue<Int64>('diskSpace.overused') / GIGABYTE);
    ANodedata.DiskSpace.Trash := Round(JSON.GetValue<Int64>('diskSpace.trash') / GIGABYTE);
    ANodedata.DiskSpace.Free := ANodedata.DiskSpace.Available - ANodedata.DiskSpace.Used - ANodedata.DiskSpace.Trash;
    LastPinged := JSON.GetValue<TDateTime>('lastPinged');
    StartedAt := JSON.GetValue<TDateTime>('startedAt');
    NowUTC := TDateTime.NowUTC;
    MinBetween := MinutesBetween(LastPinged, NowUTC);
    ANodeData.UptimeMinutes := MinutesBetween(StartedAt, NowUTC);
    ANodedata.IsOnline := MinBetween < GetSettings.OnlineRangeMinutes;

    for i := 0 to TJSONArray(JSON.Values['satellites']).Count - 1 do begin
      var Satellite := TSatellite.Create;
      Satellite.Title := TJSONObject(TJSONArray(JSON.Values['satellites']).Items[i]).Values['url'].Value;
      Satellite.ID := TJSONObject(TJSONArray(JSON.Values['satellites']).Items[i]).Values['id'].Value;
      //Satellite.StorageUsed := Round(TJSONObject(TJSONArray(JSON.Values['satellites']).Items[i]).GetValue<Int64>('currentStorageUsed') / GIGABYTE);
      Satellite.Health.IsSuspended := not TJSONObject(TJSONArray(JSON.Values['satellites']).Items[i]).Values['suspended'].Null;
      Satellite.Health.IsDisqualified := not TJSONObject(TJSONArray(JSON.Values['satellites']).Items[i]).Values['disqualified'].Null;
      ANodedata.Satellites := ANodedata.Satellites + [Satellite];
    end;
  finally
    JSON.Free;
  end;
end;

procedure SetBaseNodeDailyData(const AResponseText: String; var ANodeDailyData: TNodeDailyData);
var
  JSON: TJSONObject;
  i: integer;
begin
  JSON := TJSONObject.ParseJSONValue(AResponseText) as TJSONObject;
  if not Assigned(JSON) then
    Exit;

  try
    for i := 0 to TJSONArray(JSON.Values['satellites']).Count - 1 do begin
      var Satellite := TSatellite.Create;
      Satellite.Title := TJSONObject(TJSONArray(JSON.Values['satellites']).Items[i]).Values['url'].Value;
      Satellite.ID := TJSONObject(TJSONArray(JSON.Values['satellites']).Items[i]).Values['id'].Value;
      ANodeDailyData.Satellites := ANodeDailyData.Satellites + [Satellite];
    end;
  finally
    JSON.Free;
  end;
end;

procedure SetNodePayoutData(const AResponseText: String; var ANodedata: TNodeData);
var
  Payout, Held: Currency;
  JSON: TJSONObject;
  HoursNow, HoursMonth: Int64;
  Date, Date2, NowUTC: TDateTime;
  Year, Month, Day: Word;
begin
  JSON := TJSONObject.ParseJSONValue(AResponseText) as TJSONObject;
  if not Assigned(JSON) then
    Exit;

  try
    Payout := TJSONObject(JSON.Values['currentMonth']).GetValue<Currency>('payout', 0) / 100;
    Held := TJSONObject(JSON.Values['currentMonth']).GetValue<Currency>('held', 0) / 100;
    ANodedata.ClearlyPayout := Payout;
    ANodedata.DirtyPayout := Payout + Held;
    NowUTC := TDateTime.NowUTC;
    DecodeDate(NowUTC, Year, Month, Day);
    Date := EncodeDateTime(Year, Month, 1, 0, 0, 0, 0);
    HoursNow := HoursBetween(NowUTC, Date);
    if Month < 12 then
      Date2 := EncodeDateTime(Year, Month + 1, 1, 0, 0, 0, 0)
    else
      Date2 := EncodeDateTime(Year + 1, 1, 1, 0, 0, 0, 0);
    HoursMonth := HoursBetween(Date, Date2);
    ANodeData.EstimatedPayoutDirty := 0;
    ANodedata.EstimatedPayoutCleanly := 0;
    if HoursNow > 0 then begin
      ANodeData.EstimatedPayoutDirty := (ANodeData.DirtyPayout / HoursNow) * HoursMonth;
      ANodedata.EstimatedPayoutCleanly := (ANodeData.ClearlyPayout / HoursNow) * HoursMonth;
    end;
  finally
    JSON.Free;
  end;
end;

function GetHealth(aJValue: TJSONValue; const aSatelliteID: String; aOldHealth: THealthData): THealthData;
begin
  Result.OnlinePerc := 100;
  Result.AuditPerc := 100;
  Result.SuspensionPerc := 100;
  Result.IsSuspended := aOldHealth.IsSuspended;
  Result.IsDisqualified := aOldHealth.IsDisqualified;

  try
    if not Assigned(aJValue) or (aJValue is TJSONNull) then
      Exit;

    Result := THealthData.Create;
    Result.OnlinePerc := FloatToCurr(aJValue.GetValue<Double>('onlineScore') * 100);
    Result.AuditPerc := FloatToCurr(aJValue.GetValue<Double>('auditScore') * 100);
    Result.SuspensionPerc := FloatToCurr(aJValue.GetValue<Double>('suspensionScore') * 100);
  except
    on E: Exception do
      raise EInfoGetterException.Create('Error at GetHealth at satellite ' + aSatelliteID + ': ' + E.Message);
  end;
end;

function GetAuditHistoryValue(AJValue: TJSONValue; const ASatelliteID: String): Integer;
var
  JArr: TJSONArray;
  JObj: TJSONObject;
  i: integer;
begin
  Result := 0;

  try
    if not Assigned(TJSONObject(AJValue).Values['windows']) or (TJSONObject(AJValue).Values['windows'] is TJSONNull) then
      Exit;

    JArr := TJSONObject(AJValue).Values['windows'] as TJSONArray;
    for i := 0  to JArr.Count - 1 do begin
      JObj := JArr.Items[i] as TJSONObject;
      Result := Result + JObj.GetValue<Integer>('totalCount');
    end;
  except
    on E: Exception do
      raise EInfoGetterException.Create('Error at GetAuditHistoryValue at satellite ' + ASatelliteID + ': ' + E.Message);
  end;
end;

procedure GetNodeData(ANode: TNode; ACallback: TDataCallback; AErrorCallback: TErrorCallback; aGetInfoMode: TGetInfoMode);
var
  Proc: TProc;
begin
  Proc := procedure
          var
            HTTP: THTTPClient;
            Resp: IHTTPResponse;
            JSON, JObj: TJSONObject;
            i, j: integer;
            Satellite: TSatellite;
            Egress, RepEgress, Ingress, RepIngress: Int64;
            E: Exception;
            SW: TStopWatch;
          begin
            var NodeDailyData := TNodeDailyData.Create;
            var NodeData := TNodeData.Create;
            NodeData.Node := ANode;

            if NodeData.Node.Title = 'HomeVNode1' then
              Sleep(0);

            SW := TStopWatch.StartNew;

            try
              HTTP := THTTPClient.Create;
              try
                HTTP.ResponseTimeout := GetSettings.Timeout;
                HTTP.ConnectionTimeout := GetSettings.Timeout;
                HTTP.SendTimeout := GetSettings.Timeout;
                Resp := HTTP.Get('http://' + ANode.Address + ':' + ANode.Port.ToString + '/api/sno/');
                SetBaseNodeData(Resp.ContentAsString, NodeData);
                SetBaseNodeDailyData(Resp.ContentAsString, NodeDailyData);

                Resp := HTTP.Get('http://' + ANode.Address + ':' + ANode.Port.ToString + '/api/sno/estimated-payout');
                SetNodePayoutData(Resp.ContentAsString, NodeData);

                if aGetInfoMode = TGetInfoMode.gimStandart then begin
                  for i := 0 to Length(NodeData.Satellites) - 1 do begin
                    Satellite := NodeData.Satellites[i];
                    if Satellite.Health.IsDisqualified then
                      Continue;

                    Resp := HTTP.Get('http://' + ANode.Address + ':' + ANode.Port.ToString + '/api/sno/satellite/' + Satellite.ID);
                    JSON := TJSONObject.ParseJSONValue(Resp.ContentAsString) as TJSONObject;
                    if not Assigned(JSON) then
                      Continue;

                    var JStr := JSON.ToString;
                    try
                      if JSON.Values['audit'] <> nil then
                        Satellite.Audit := TJSONObject(JSON.Values['audit']).GetValue<Integer>('successCount', -1);
                      if (Satellite.Audit = -1) and Assigned(JSON.Values['auditHistory']) then
                        Satellite.Audit := GetAuditHistoryValue(JSON.Values['auditHistory'], Satellite.ID);
                      Satellite.TBMonth := RoundTo(JSON.GetValue<Double>('averageUsageBytes', 0) / TERABYTE, -3);

                      Satellite.Health := GetHealth(JSON.Values['audits'], Satellite.ID, Satellite.Health);

                      Egress := 0; RepEgress := 0; Ingress := 0; RepIngress := 0;
                      if (Assigned(JSON.Values['bandwidthDaily']) and not (JSON.Values['bandwidthDaily'] is TJSONNull)) then begin
                        for j := 0 to TJSONArray(JSON.Values['bandwidthDaily']).Count - 1 do begin
                          JObj := TJSONObject(TJSONArray(JSON.Values['bandwidthDaily']).Items[j]);
                          Egress := Egress + TJSONObject(JObj.Values['egress']).GetValue<Int64>('usage', 0);
                          RepEgress := RepEgress + TJSONObject(JObj.Values['egress']).GetValue<Int64>('repair', 0);

                          Ingress := Ingress + TJSONObject(JObj.Values['ingress']).GetValue<Int64>('usage', 0);
                          RepIngress := RepIngress + TJSONObject(JObj.Values['ingress']).GetValue<Int64>('repair', 0);
                        end;

                        JObj := TJSONObject(TJSONArray(JSON.Values['bandwidthDaily']).Items[TJSONArray(JSON.Values['bandwidthDaily']).Count - 1]);
                        if Assigned(JObj) then begin
                          NodeDailyData.Satellites[i].Egress := RoundTo(TJSONObject(JObj.Values['egress']).GetValue<Int64>('usage', 0) / GIGABYTE, -3);
                          NodeDailyData.Satellites[i].RepairEgress := RoundTo(TJSONObject(JObj.Values['egress']).GetValue<Int64>('repair', 0) / GIGABYTE, -3);
                          NodeDailyData.Satellites[i].Ingress := RoundTo(TJSONObject(JObj.Values['ingress']).GetValue<Int64>('usage', 0) / GIGABYTE, -3);
                          NodeDailyData.Satellites[i].RepairIngress := RoundTo(TJSONObject(JObj.Values['ingress']).GetValue<Int64>('repair', 0) / GIGABYTE, -3);
                          NodeDailyData.Satellites[i].TotalBandwidth := NodeDailyData.Satellites[i].Egress + NodeDailyData.Satellites[i].RepairEgress +
                            NodeDailyData.Satellites[i].Ingress + NodeDailyData.Satellites[i].RepairIngress;
                        end;
                      end;

                      if Assigned(JSON.Values['storageDaily']) and not (JSON.Values['storageDaily'] is TJSONNull) and (TJSONArray(JSON.Values['storageDaily']).Count > 0) then begin
                        JObj := TJSONObject(TJSONArray(JSON.Values['storageDaily']).Items[TJSONArray(JSON.Values['storageDaily']).Count - 1]);
                        if Assigned(JObj) then begin
                          NodeDailyData.Satellites[i].TBMonth := RoundTo(JObj.GetValue<Double>('atRestTotalBytes', 0) / TERABYTE, -3);
                        end;
                      end;
                    finally
                      JSON.Free;
                    end;

                    Satellite.Egress := RoundTo(Egress / GIGABYTE, -3);
                    Satellite.RepairEgress := RoundTo(RepEgress / GIGABYTE, -3);
                    Satellite.Ingress := RoundTo(Ingress / GIGABYTE, -3);
                    Satellite.RepairIngress := RoundTo(RepIngress / GIGABYTE, -3);
                    Satellite.TotalBandwidth := Satellite.Egress + Satellite.RepairEgress +
                                                Satellite.Ingress + Satellite.RepairIngress;
                    NodeData.Satellites[i] := Satellite;
                  end;
                end else begin
                  Resp := HTTP.Get('http://' + ANode.Address + ':' + ANode.Port.ToString + '/api/sno/satellites/');
                  JSON := TJSONObject.ParseJSONValue(Resp.ContentAsString) as TJSONObject;
                  if Assigned(JSON) then begin
                    try
                      NodeData.TBMonth := RoundTo(JSON.GetValue<Double>('averageUsageBytes', 0) / TERABYTE, -3);
                      var BandwidthDaily := JSON.Values['bandwidthDaily'] as TJSONArray;

                      var TodayBandwithDaily := BandwidthDaily[BandwidthDaily.Count - 1] as TJSONObject;
                      NodeDailyData.Egress := RoundTo(TodayBandwithDaily.GetValue<Int64>('egress.usage', 0) / GIGABYTE, -3);
                      NodeDailyData.RepairEgress := RoundTo(TodayBandwithDaily.GetValue<Int64>('egress.repair', 0) / GIGABYTE, -3);
                      NodeDailyData.Ingress := RoundTo(TodayBandwithDaily.GetValue<Int64>('ingress.usage', 0) / GIGABYTE, -3);
                      NodeDailyData.RepairIngress := RoundTo(TodayBandwithDaily.GetValue<Int64>('ingress.repair', 0) / GIGABYTE, -3);

                      for i := 0 to BandwidthDaily.Count - 1 do begin
                        NodeData.Egress := NodeData.Egress + RoundTo(BandwidthDaily[i].GetValue<Int64>('egress.usage', 0) / GIGABYTE, -3);
                        NodeData.RepairEgress := NodeData.RepairEgress + RoundTo(BandwidthDaily[i].GetValue<Int64>('egress.repair', 0) / GIGABYTE, -3);
                        NodeData.Ingress := NodeData.Ingress + RoundTo(BandwidthDaily[i].GetValue<Int64>('ingress.usage', 0) / GIGABYTE, -3);
                        NodeData.RepairIngress := NodeData.RepairIngress + RoundTo(BandwidthDaily[i].GetValue<Int64>('ingress.repair', 0) / GIGABYTE, -3);
                      end;

                      NodeDailyData.TotalBandwidth := NodeDailyData.Egress + NodeDailyData.RepairEgress + NodeDailyData.Ingress + NodeDailyData.RepairIngress;
                      NodeData.TotalBandwidth := NodeData.Egress + NodeData.RepairEgress + NodeData.Ingress + NodeData.RepairIngress;

                      var StorageDaily := JSON.Values['storageDaily'] as TJSONArray;
                      var TodayStorageDaily := StorageDaily[StorageDaily.Count - 1] as TJSONObject;
                      NodeDailyData.TBMonth := RoundTo(TodayStorageDaily.GetValue<Double>('atRestTotalBytes', 0) / TERABYTE, -3);
                    finally
                      JSON.Free;
                    end;
                  end;
                end;

                for Satellite in NodeData.Satellites do begin
                  NodeData.Egress := NodeData.Egress + Satellite.Egress;
                  NodeData.RepairEgress := NodeData.RepairEgress + Satellite.RepairEgress;
                  NodeData.Ingress := NodeData.Ingress + Satellite.Ingress;
                  NodeData.RepairIngress := NodeData.RepairIngress + Satellite.RepairIngress;
                  NodeData.TotalBandwidth := NodeData.TotalBandwidth + Satellite.TotalBandwidth;
                  NodeData.TBMonth := NodeData.TBMonth + Satellite.TBMonth;
                end;

                for Satellite in NodeDailyData.Satellites do begin
                  NodeDailyData.Egress := NodeDailyData.Egress + Satellite.Egress;
                  NodeDailyData.Ingress := NodeDailyData.Ingress + Satellite.Ingress;
                  NodeDailyData.RepairEgress := NodeDailyData.RepairEgress + Satellite.RepairEgress;
                  NodeDailyData.RepairIngress := NodeDailyData.RepairIngress + Satellite.RepairIngress;
                  NodeDailyData.TotalBandwidth := NodeDailyData.TotalBandwidth + Satellite.TotalBandwidth;
                  NodeDailyData.TBMonth := NodeDailyData.TBMonth + Satellite.TBMonth;
                end;
              finally
                HTTP.Free;
              end;

              NodeData.DailyData := NodeDailyData;
              if NodeData.DiskSpace.Used > 0 then
                NodeData.EfficiencyByUsed := NodeData.EstimatedPayoutDirty / NodeData.DiskSpace.Used * 1000
              else
                NodeData.EfficiencyByUsed := 0;

              if NodeData.TBMonth > 0 then
                NodeData.EfficiencyByTBM := NodeData.EstimatedPayoutDirty / NodeData.TBMonth
              else
                NodeData.EfficiencyByTBM := 0;


              SW.Stop;
              NodeData.WorkTimeMs := SW.ElapsedMilliseconds;
              TThread.Queue(nil,
                 procedure
                 begin
                   if Assigned(ACallback) then
                     ACallback(NodeData);
                 end);
            except
              SW.Stop;
              E := Exception(AcquireExceptionObject);
              try
                TThread.Synchronize(nil,
                 procedure
                 begin
                   AErrorCallback(NodeData.Node, E);
                 end);
              finally
                E.Free;
              end;
            end;
          end;

  TTask.Create(Proc).Start;
end;

end.
