unit Storj.Registry;

interface

uses
  System.SysUtils,
  System.DateUtils,
  System.Win.Registry;

  procedure SetNeighboursLastUpdated(const aDateTime: TDateTime);
  function GetNeighboursLastUpdated: TDateTime;

implementation

procedure SetNeighboursLastUpdated(const aDateTime: TDateTime);
var
  R: TRegistry;
begin
  R := TRegistry.Create;
  try
    if not R.OpenKey('Software\RealSHELS\StorjNodeViewer', True) then
      RaiseLastOSError;

    R.WriteDateTime('NeighboursLastUpdated', aDateTime);
  finally
    R.Free;
  end;
end;

function GetNeighboursLastUpdated: TDateTime;
var
  R: TRegistry;
begin
  R := TRegistry.Create;
  try
    if R.OpenKey('Software\RealSHELS\StorjNodeViewer', False) then
      Result := R.ReadDateTime('NeighboursLastUpdated')
    else
      Result := IncYear(Now, -10);
  finally
    R.Free;
  end;
end;

end.
