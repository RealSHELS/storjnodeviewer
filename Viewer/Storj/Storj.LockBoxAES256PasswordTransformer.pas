unit Storj.LockBoxAES256PasswordTransformer;

interface

uses
  System.SysUtils,
  Storj.Types,
  uTPLb_CryptographicLibrary, uTPLb_Codec;

type
  TLockBoxAES256PasswordTransformer = class(TInterfacedObject, IPasswordTransformer)
  private const
    AES_KEY = 'bNIDuYY7bOs$fu79YTG8HI8r5=CQw}J$';
  public
    function Encypt(const aPassword: String): String;
    function Decrypt(const aEncryptedPassword: String): String;
  end;

implementation

{ TLockBoxAES256PasswordTransformer }

function TLockBoxAES256PasswordTransformer.Decrypt(const aEncryptedPassword: String): String;
var
  CryptoLib: TCryptographicLibrary;
  Codec: TCodec;
  DecryptedStr: String;
begin
  CryptoLib := TCryptographicLibrary.Create(nil);
  Codec := TCodec.Create(nil);
  try
    Codec.CryptoLibrary := CryptoLib;
    Codec.StreamCipherId := 'native.StreamToBlock';
    Codec.BlockCipherId := 'native.AES-256';
    Codec.ChainModeId := 'native.CBC';

    Codec.Reset;
    Codec.Password := AES_KEY;
    Codec.DecryptString(DecryptedStr, aEncryptedPassword, TEncoding.UTF8);
  finally
    CryptoLib.Free;
    Codec.Free;
  end;

  Result := DecryptedStr;
end;

function TLockBoxAES256PasswordTransformer.Encypt(const aPassword: String): String;
var
  CryptoLib: TCryptographicLibrary;
  Codec: TCodec;
  EncryptedStr: String;
begin
  CryptoLib := TCryptographicLibrary.Create(nil);
  Codec := TCodec.Create(nil);
  try
    Codec.CryptoLibrary := CryptoLib;
    Codec.StreamCipherId := 'native.StreamToBlock';
    Codec.BlockCipherId := 'native.AES-256';
    Codec.ChainModeId := 'native.CBC';

    Codec.Reset;
    Codec.Password := AES_KEY;
    Codec.EncryptString(aPassword, EncryptedStr, TEncoding.UTF8);
  finally
    CryptoLib.Free;
    Codec.Free;
  end;

  Result := EncryptedStr;
end;

end.
