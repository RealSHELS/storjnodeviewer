unit Storj.TotalEarnedGetter;

interface

uses
  System.Threading,
  System.Net.HTTPClient,
  System.SysUtils,
  System.Classes,
  System.JSON,
  System.Math,
  Storj.Types,
  Storj.Consts,
  System.Generics.Collections;

  procedure GetTotalEarned(ANode: TNode; ACallback: TDataCallback; AErrorCallback: TErrorCallback);

implementation

uses
  VCL.Dialogs, System.DateUtils,
  Storj.Settings;

procedure GetTotalEarned(ANode: TNode; ACallback: TDataCallback; AErrorCallback: TErrorCallback);
var
  Proc: TProc;
begin
  Proc := procedure
          var
            NodeData: TNodeData;
            HTTP: THTTPClient;
            Resp: IHTTPResponse;
            JObj: TJSONObject;
            E: Exception;
            TotalCount: Integer;
          begin
            NodeData.Node := ANode;
            NodeData.TotalEarned := 0;

            try
              HTTP := THTTPClient.Create;
              try
                HTTP.ResponseTimeout := GetSettings.Timeout;
                HTTP.ConnectionTimeout := GetSettings.Timeout;
                HTTP.SendTimeout := GetSettings.Timeout;
                var requestTo := FormatDatetime('yyyy-MM', Now);
                Resp := HTTP.Get('http://' + ANode.Address + ':' + ANode.Port.ToString + '/api/heldamount/paystubs/2000-01/' + requestTo);

                var JArr := TJSONObject.ParseJSONValue(Resp.ContentAsString) as TJSONArray;
                try
                  TotalCount :=0;

                  if Assigned(JArr) then begin
                    for var i := 0 to JArr.Count - 1 do begin
                      JObj := JArr.Items[i] as TJSONObject;
                      TotalCount := TotalCount + JObj.GetValue<Integer>('paid', 0);
                    end;
                  end;
                finally
                  JArr.Free;
                end;

                NodeData.TotalEarned := TotalCount / 1000000; //1 million
              finally
                HTTP.Free;
              end;

              TThread.Queue(nil,
                 procedure
                 begin
                   if Assigned(ACallback) then
                     ACallback(NodeData);
                 end);
            except
              E := Exception(AcquireExceptionObject);
              try
                TThread.Synchronize(nil,
                 procedure
                 begin
                   AErrorCallback(NodeData.Node, E);
                 end);
              finally
                E.Free;
              end;
            end;
          end;

  TTask.Create(Proc).Start;
end;

end.
