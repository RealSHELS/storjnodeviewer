unit Storj.Types;

interface

uses
  System.SysUtils,
  System.Generics.Collections,
  System.JSON,
  Data.DB;

type
  IPasswordTransformer = interface
    function Encypt(const aPassword: String): String;
    function Decrypt(const aEncryptedPassword: String): String;
  end;

  EInfoGetterException = class(Exception);
  EFabricaUnknownName = class(Exception);

  TGetInfoMode = (gimStandart, gimLight);
  TLoadSourceMode = (lsmManual, lsmAuto);

  TNode = record
    Title: String;
    FullPath: String;
    Address: String;
    Port: Integer;
    NodeID: String;
    IsUseInAdditionalStats: Boolean;

    class function Create: TNode; static;
  end;

  TDiskSpace = record
    Used: Integer;
    Available: Integer;
    Free: Integer;
    Overused: Integer;
    Trash: Integer;

    class function Create: TDiskSpace; static;
  end;

  THealthData = record
    OnlinePerc: Currency;
    AuditPerc: Currency;
    SuspensionPerc: Currency;
    IsSuspended: Boolean;
    IsDisqualified: Boolean;

    class function Create: THealthData; static;
  end;

  TSatellite = record
    ID: String;
    Title: String;
    //StorageUsed: Integer;
    Audit: Integer;
    Egress: Double;
    RepairEgress: Double;
    Ingress: Double;
    RepairIngress: Double;
    TotalBandwidth: Double;
    TBMonth: Double;
    Health: THealthData;

    class function Create: TSatellite; static;
  end;

  TNodeDailyData = record
    Egress: Double;
    RepairEgress: Double;
    Ingress: Double;
    RepairIngress: Double;
    TotalBandwidth: Double;
    TBMonth: Double;
    Satellites: array of TSatellite;

    class function Create: TNodeDailyData; static;
  end;

  TNodeData = record
    Node: TNode;
    IsOnline: Boolean;
    UptimeMinutes: Integer;
    WorkTimeMs: Integer;
    DiskSpace: TDiskSpace;
    Version: String;
    TBMonth: Double;
    ClearlyPayout: Currency;
    DirtyPayout: Currency;
    EstimatedPayoutDirty: Currency;
    EstimatedPayoutCleanly: Currency;
    Egress: Double;
    RepairEgress: Double;
    Ingress: Double;
    RepairIngress: Double;
    TotalBandwidth: Double;
    Satellites: array of TSatellite;
    DailyData: TNodeDailyData;
    EfficiencyByUsed: Currency;
    EfficiencyByTBM: CUrrency;
    TotalEarned: Currency;
    Wallet: String;

    class function Create: TNodeData; static;
  end;

  TNodeDataArray = TArray<TNodeData>;

  TCurrencyRates = class
  public
    Date: TDate;
    RatesToUSD: TDictionary<String, Currency>;

    constructor Create(aDate: TDate);
    destructor Destroy; override;
  end;

  TStorjNetworkData = record
    UsedSpace: Int64;
  end;

  TBeforeStartGetNodeData = record
    TotalCount: Integer;
  end;

  TAfterGetNodeData = record
    TotalCount: Integer;
    SuccessCount: Integer;
    ErrorCount: Integer;
  end;

  TNeighboursData = record
    Host: String;
    Neighbours: Integer;
    Subnet: String;
  end;

  TChartNodeData = record
    Node: TNode;
    DT: TDateTime;
    TBMonth: Double;
    Payment: Currency;
    Egress: Double;
    RepairEgress: Double;
    TotalEgress: Double;
    Ingress: Double;
    RepairIngress: Double;
    TotalIngress: Double;
    TotalBandwidth: Double;
    Efficiency: Currency;

    class function Create: TChartNodeData; static;
  end;

  TChartDataType = (cdtHistorical, cdtCurrentMonth, cdtLocalHistory);

  TChartDisplaySettings = record
    Title: String;
    FieldType: TFieldType;
    FieldName: String;
    ChartDataType: TChartDataType;
    DateFrom: TDateTime;
    DateTo: TDateTime;
    ValueCaptionFormat: String;
    ValueCaptionFormatDxChart: String;
    IsUseCache: Boolean;
  end;

  TChartDisplayField = record
    Title: String;
    FullPath: String;
  end;

  TLocalHistoryData = record
    CreationDT: TDatetime;
    Data: String;
  end;

  TArrayChartDisplayField = TArray<TChartDisplayField>;

  IChartWorker = interface
    procedure DisplayChart;
    procedure SetProgressData(const aNodeCount: Integer);
    procedure IncProgress;
    function GetChartDisplaySettings: TChartDisplaySettings;
    function GetDisplayFields: TArray<String>;
    function GetDisplayFieldsFullData: TArrayChartDisplayField;
    procedure Log(const aText: String);
  end;

  TArrayChartDisplayFieldHelper = record helper for TArrayChartDisplayField
    function GetFullPathArray: TArray<String>;
  end;

  TSSHSettings = record
    FullPath: String;
    Host: String;
    Port: Integer;
    UserName: String;
    Password: String;
    Commands: String;
    LastSelectedIndexes: String;

    class operator Initialize(out aDest: TSSHSettings);
  end;

  TDataCallback = reference to procedure (ANodeData: TNodeData);
  TChartDataCallback = reference to procedure (aData: TArray<TChartNodeData>);
  TErrorCallback = reference to procedure (ANode: TNode; E: Exception);
  TCurrenciesRatesCallback = reference to procedure (aRates: TCurrencyRates);
  TStorjNetworkDataCallback = reference to procedure (const aData: TStorjNetworkData);
  TNodesList = TArray<TNode>;
  TNeighboursCallback = reference to procedure (const aData: TNeighboursData);
  TNeighboursErrorCallback = reference to procedure (const aHost: String; E: Exception);

implementation

{ TCurrencyRates }

constructor TCurrencyRates.Create(aDate: TDate);
begin
  Date := aDate;
  RatesToUSD := TDictionary<String, Currency>.Create;
end;

destructor TCurrencyRates.Destroy;
begin
  RatesToUSD.Free;
  inherited;
end;

{ THistoricalNodeData }

class function TChartNodeData.Create: TChartNodeData;
begin
  Result.Node.Title := '';
  Result.Node.Address := '';
  Result.Node.Port := 0;
  Result.Node.NodeID := '';
  Result.Node.IsUseInAdditionalStats := false;

  Result.DT := Now;
  Result.TBMonth := 0;
  Result.Payment := 0;
  Result.Egress := 0;
  Result.RepairEgress := 0;
  Result.Ingress := 0;
  Result.RepairIngress := 0;
  Result.TotalBandwidth := 0;
  Result.Efficiency := 0;
end;

{ TArrayChartDisplayFieldHelper }

function TArrayChartDisplayFieldHelper.GetFullPathArray: TArray<String>;
begin
  Result := [];
  for var Item in Self do
    Result := Result + [Item.FullPath];
end;

{ TNode }

class function TNode.Create: TNode;
begin
  Result.Title := '';
  Result.FullPath := '';
  Result.Address := '';
  Result.Port := 0;
  Result.NodeID := '';
  Result.IsUseInAdditionalStats := False;
end;

{ TDiskSpace }

class function TDiskSpace.Create: TDiskSpace;
begin
  Result.Used := 0;
  Result.Available := 0;
  Result.Free := 0;
  Result.Overused := 0;
  Result.Trash := 0;
end;

{ THealthData }

class function THealthData.Create: THealthData;
begin
  Result.OnlinePerc := 0;
  Result.AuditPerc := 0;
  Result.SuspensionPerc := 0;
  Result.IsSuspended := False;
  Result.IsDisqualified := False;
end;

{ TSatellite }

class function TSatellite.Create: TSatellite;
begin
  Result.ID := '';
  Result.Title := '';
  //Result.StorageUsed := 0;
  Result.Audit := 0;
  Result.Egress := 0;
  Result.RepairEgress := 0;
  Result.Ingress := 0;
  Result.RepairIngress := 0;
  Result.TotalBandwidth := 0;
  Result.TBMonth := 0;
  Result.Health := THealthData.Create;
end;

{ TNodeDailyData }

class function TNodeDailyData.Create: TNodeDailyData;
begin
  Result.Egress := 0;
  Result.RepairEgress := 0;
  Result.Ingress := 0;
  Result.RepairIngress := 0;
  Result.TotalBandwidth := 0;
  Result.TBMonth := 0;
  Result.Satellites := [];
end;

{ TNodeData }

class function TNodeData.Create: TNodeData;
begin
  Result.Node := TNode.Create;
  Result.IsOnline := False;
  Result.UptimeMinutes := 0;
  Result.WorkTimeMs := 0;
  Result.DiskSpace := TDiskSpace.Create;
  Result.Version := '';
  Result.TBMonth := 0;
  Result.ClearlyPayout := 0;
  Result.DirtyPayout := 0;
  Result.EstimatedPayoutDirty := 0;
  Result.EstimatedPayoutCleanly := 0;
  Result.Egress := 0;
  Result.RepairEgress := 0;
  Result.Ingress := 0;
  Result.RepairIngress := 0;
  Result.TotalBandwidth := 0;
  Result.Satellites := [];
  Result.DailyData := TNodeDailyData.Create;
  Result.EfficiencyByUsed := 0;
  Result.EfficiencyByTBM := 0;
  Result.TotalEarned := 0;
  Result.Wallet := '';
end;

{ TSSHSettings }

class operator TSSHSettings.Initialize(out aDest: TSSHSettings);
begin
  aDest.FullPath := '';
  aDest.Host := '';
  aDest.Port := 22;
  aDest.UserName := '';
  aDest.Password := '';
  aDest.Commands := '';
  aDest.LastSelectedIndexes := '';
end;

end.
