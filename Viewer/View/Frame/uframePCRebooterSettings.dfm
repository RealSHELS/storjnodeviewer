object framePCRebooterSettings: TframePCRebooterSettings
  Left = 0
  Top = 0
  Width = 197
  Height = 195
  TabOrder = 0
  object dxLayoutControl: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 197
    Height = 195
    Align = alClient
    ParentBackground = True
    TabOrder = 0
    Transparent = True
    object chkIsActive: TcxCheckBox
      Left = 22
      Top = 21
      Caption = 'Active'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 0
    end
    object edtName: TcxTextEdit
      Left = 75
      Top = 44
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 1
      Width = 100
    end
    object edtURL: TcxTextEdit
      Left = 75
      Top = 71
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 2
      Width = 100
    end
    object numNums: TcxCurrencyEdit
      Left = 75
      Top = 125
      EditValue = 4.000000000000000000
      Properties.DisplayFormat = '0'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 4
      Width = 100
    end
    object edtPCNames: TcxTextEdit
      Left = 75
      Top = 152
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 5
      Width = 100
    end
    object txtAuthKey: TcxTextEdit
      Left = 75
      Top = 98
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 3
      Width = 100
    end
    object dxLayoutControlGroup_Root: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avClient
      Offsets.Top = -7
      ButtonOptions.Buttons = <>
      Hidden = True
      ShowBorder = False
      Index = -1
    end
    object laychkOsActive: TdxLayoutItem
      Parent = laygrpSameAsRoot
      CaptionOptions.Text = 'cxCheckBox1'
      CaptionOptions.Visible = False
      Control = chkIsActive
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 82
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object layedtName: TdxLayoutItem
      Parent = laygrpSameAsRoot
      CaptionOptions.Text = 'Name'
      Control = edtName
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object layedtURL: TdxLayoutItem
      Parent = laygrpSameAsRoot
      CaptionOptions.Text = 'URL/IP'
      Control = edtURL
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object laynumNums: TdxLayoutItem
      Parent = laygrpSameAsRoot
      CaptionOptions.Text = 'Nums'
      Control = numNums
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 4
    end
    object layedtPCNames: TdxLayoutItem
      Parent = laygrpSameAsRoot
      CaptionOptions.Text = 'PC Names'
      Control = edtPCNames
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 5
    end
    object lattxtAuthKey: TdxLayoutItem
      Parent = laygrpSameAsRoot
      CaptionOptions.Text = 'Auth Key'
      Control = txtAuthKey
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 3
    end
    object laygrpSameAsRoot: TdxLayoutGroup
      Parent = dxLayoutControlGroup_Root
      CaptionOptions.Text = 'PCRebooter'
      ButtonOptions.Buttons = <>
      ItemIndex = 5
      Index = 0
    end
  end
end
