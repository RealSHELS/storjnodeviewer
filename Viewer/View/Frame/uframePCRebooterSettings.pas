unit uframePCRebooterSettings;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxSkinBasic,
  dxSkiniMaginary, dxSkinSeven, dxSkinSevenClassic, cxClasses,
  dxLayoutContainer, dxLayoutControl, dxLayoutcxEditAdapters, cxContainer,
  cxEdit, cxCheckBox, cxTextEdit, cxCurrencyEdit,
  Storj.Settings;

type
  TframePCRebooterSettings = class(TFrame)
    dxLayoutControlGroup_Root: TdxLayoutGroup;
    dxLayoutControl: TdxLayoutControl;
    chkIsActive: TcxCheckBox;
    laychkOsActive: TdxLayoutItem;
    edtName: TcxTextEdit;
    layedtName: TdxLayoutItem;
    edtURL: TcxTextEdit;
    layedtURL: TdxLayoutItem;
    numNums: TcxCurrencyEdit;
    laynumNums: TdxLayoutItem;
    edtPCNames: TcxTextEdit;
    layedtPCNames: TdxLayoutItem;
    txtAuthKey: TcxTextEdit;
    lattxtAuthKey: TdxLayoutItem;
    laygrpSameAsRoot: TdxLayoutGroup;
  private
    { Private declarations }
  public
    procedure SetSettingsToUI(const aSettings: TPCRebooterSettings);
    function GetSettingsFromUI: TPCRebooterSettings;

    function IsActive: Boolean;
  end;

implementation

{$R *.dfm}

{ TframePCRebooterSettings }

function TframePCRebooterSettings.GetSettingsFromUI: TPCRebooterSettings;
begin
  Result.IsActive := chkIsActive.Checked;
  Result.Name := edtName.Text;
  Result.URL := edtURL.Text;
  Result.Nums := Round(numNums.Value);
  Result.PCNames := edtPCNames.Text;
  Result.AuthKey := txtAuthKey.Text;
end;

function TframePCRebooterSettings.IsActive: Boolean;
begin
  Result := chkIsActive.Checked;
end;

procedure TframePCRebooterSettings.SetSettingsToUI(const aSettings: TPCRebooterSettings);
begin
  chkIsActive.Checked := aSettings.IsActive;
  edtName.Text := aSettings.Name;
  edtURL.Text := aSettings.URL;
  numNums.Value := aSettings.Nums;
  edtPCNames.Text := aSettings.PCNames;
  txtAuthKey.Text := aSettings.AuthKey;
end;

end.
