unit uframePCRebootersStatus;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  dxSkinsCore, dxSkinBasic, dxSkiniMaginary, dxSkinSeven, dxSkinSevenClassic,
  cxLabel, Vcl.Menus, Vcl.StdCtrls, cxButtons, cxCheckBox,
  uNotifyCenter, Storj.Consts, Storj.Settings, uDM,
  dxSkinWXI, dxSkinOffice2013LightGray, dxSkinOffice2019Black,
  dxSkinOffice2019DarkGray, dxSkinOffice2019White, dxSkinWhiteprint,
  System.Threading;

type
  TframePCRebootersStatus = class(TFrame)
    pnl: TFlowPanel;
    chkSource: TcxCheckBox;
    procedure chkMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    procedure CommandSettingsChanged(const aData: TValue);
    procedure CommandEchoResult(const aData: TValue);
  public
    constructor Create(AOwnder: TComponent); override;
    destructor Destroy; override;
  end;

implementation

{$R *.dfm}

{ TframePCRebootersStatus }

procedure TframePCRebootersStatus.chkMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Button = TMouseButton.mbRight then begin
    for var PCRebooter in GetSettings.PCRebooters do
      if PCRebooter.IsActive and (PCRebooter.Name = TcxCheckBox(Sender).Caption) then
        DM.UpdatePCRebooter(PCRebooter);
  end;
end;

procedure TframePCRebootersStatus.CommandEchoResult(const aData: TValue);
var
  i: integer;
  Res: TPCRebooterSettingsEchoResult;
begin
  Res := aData.AsType<TPCRebooterSettingsEchoResult>;
  for i := 0 to pnl.ControlCount - 1 do
    if (pnl.Controls[i] is TcxCheckBox) and (TcxCheckBox(pnl.Controls[i]).Caption = Res.Settings.Name) then
      TcxCheckBox(pnl.Controls[i]).Checked := Res.IsOnline;
end;

procedure TframePCRebootersStatus.CommandSettingsChanged(const aData: TValue);
var
  i: integer;
  chk: TcxCheckBox;
begin
  i := 0;
  while i < pnl.ControlCount do
    if (pnl.Controls[i] is TcxCheckBox) and (TcxCheckBox(pnl.Controls[i]).Name <> 'chkSource') then
      pnl.Controls[i].Free
    else
      i := i + 1;

  if GetSettings.IsEnabledPCRebooter then begin
    Self.Visible := true;

    for var PCRebooter in GetSettings.PCRebooters do
      if PCRebooter.IsActive then begin
        chk := TcxCheckBox.Create(pnl);
        chk.Parent := pnl;
        chk.Caption := PCRebooter.Name;
        chk.EditValue := null;
        chk.Properties.ReadOnly := true;

        chk.Properties.GlyphCount := 3;
        chk.Properties.Glyph := chkSource.Properties.Glyph;

        chk.OnMouseDown := chkMouseDown;
      end;

      DM.UpdatePCRebooters;
  end else begin
    Self.Visible := false;
  end;
end;

constructor TframePCRebootersStatus.Create(AOwnder: TComponent);
begin
  inherited;

  TNotifyCenter.Instance.Subscribe(COMMAND_SETTING_CHANGED, CommandSettingsChanged);
  TNotifyCenter.Instance.Subscribe(COMMAND_PCREBOOTER_ECHO_RESULT, CommandEchoResult);

  TTask.Run(
    procedure
    begin
      Sleep(100);
      TThread.Queue(nil, procedure begin CommandSettingsChanged(nil); end);
    end
  ).Start;
end;

destructor TframePCRebootersStatus.Destroy;
begin
  TNotifyCenter.Instance.Unsubscribe(COMMAND_SETTING_CHANGED, CommandSettingsChanged);
  TNotifyCenter.Instance.Unsubscribe(COMMAND_PCREBOOTER_ECHO_RESULT, CommandEchoResult);

  inherited;
end;

end.
