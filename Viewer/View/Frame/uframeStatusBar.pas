unit uframeStatusBar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxSkinBasic,
  dxSkiniMaginary, dxSkinSeven, dxSkinSevenClassic, cxClasses,
  dxLayoutContainer, dxLayoutControl, dxStatusBar, dxLayoutControlAdapters,
  Vcl.Menus, Vcl.StdCtrls, cxButtons, uNotifyCenter, Storj.Consts,
  uVersionGetter, Storj.Types, System.JSON, dxSkinOffice2013LightGray,
  dxSkinOffice2019Black, dxSkinOffice2019DarkGray, dxSkinOffice2019White,
  dxSkinWhiteprint, dxSkinWXI;

type
  TframeStatusBar = class(TFrame)
    dxLayoutControlGroup_Root: TdxLayoutGroup;
    dxLayoutControl: TdxLayoutControl;
    layStatusBar: TdxLayoutItem;
    StatusBar: TdxStatusBar;
    laybtnStorjVersionRefresh: TdxLayoutItem;
    btnStorjVersionRefresh: TcxButton;
    procedure btnStorjVersionRefreshClick(Sender: TObject);
  private
    FFinishedCount: Integer;

    procedure CommandStorjGithubVersion(const aValue: TValue);
    procedure CommandStorjIOVersion(const aValue: TValue);
    procedure CommandBeforeStartGetNodeData(const aData: TValue);
    procedure CommandShowNodeData(const aData: TValue);
    procedure CommandShowNodeDataGroup(const aData: TValue);
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
  end;

implementation

{$R *.dfm}

uses
  uDM;

procedure TframeStatusBar.btnStorjVersionRefreshClick(Sender: TObject);
begin
  DM.UpdateLastestStorjVersion;
end;

procedure TframeStatusBar.CommandStorjGithubVersion(const aValue: TValue);
begin
  StatusBar.Panels[3].Text := 'Github v: ' + aValue.AsString;
end;

procedure TframeStatusBar.CommandStorjIOVersion(const aValue: TValue);
begin
  StatusBar.Panels[4].Text := 'Storj.io v: ' + aValue.AsType<TStorjIOVersion>.Version +
                              ' Cursor: ' + aValue.AsType<TStorjIOVersion>.Cursor;
end;

constructor TframeStatusBar.Create(aOwner: TComponent);
begin
  inherited;

  FFinishedCount := 0;

  TNotifyCenter.Instance.Subscribe(COMMAND_STORJ_GITHUB_VERSION, CommandStorjGithubVersion);
  TNotifyCenter.Instance.Subscribe(COMMAND_STORJ_IO_VERSION, CommandStorjIOVersion);
  TNotifyCenter.Instance.Subscribe(COMMAND_BEFORE_START_GET_NODE, CommandBeforeStartGetNodeData);
  TNotifyCenter.Instance.Subscribe(COMMAND_SHOW_NODE_DATA, CommandShowNodeData);
  TNotifyCenter.Instance.Subscribe(COMMAND_SHOW_NODE_DATA_GROUP, CommandShowNodeDataGroup);
end;

destructor TframeStatusBar.Destroy;
begin
  TNotifyCenter.Instance.UnSubscribe(COMMAND_STORJ_GITHUB_VERSION, CommandStorjGithubVersion);
  TNotifyCenter.Instance.UnSubscribe(COMMAND_STORJ_IO_VERSION, CommandStorjIOVersion);
  TNotifyCenter.Instance.UnSubscribe(COMMAND_BEFORE_START_GET_NODE, CommandBeforeStartGetNodeData);
  TNotifyCenter.Instance.UnSubscribe(COMMAND_SHOW_NODE_DATA, CommandShowNodeData);
  TNotifyCenter.Instance.UnSubscribe(COMMAND_SHOW_NODE_DATA_GROUP, CommandShowNodeDataGroup);

  inherited;
end;

procedure TframeStatusBar.CommandBeforeStartGetNodeData(const aData: TValue);
var
  Data: TBeforeStartGetNodeData;
begin
  Data := aData.AsType<TBeforeStartGetNodeData>;

  StatusBar.Panels[0].Text := 'Last time: ' + DateTimeToStr(Now);
  FFinishedCount := 0;
  StatusBar.Panels[2].Text := 'Finished count: ' + FFinishedCount.ToString;

  StatusBar.Panels[1].Text := 'Total Nodes: ' + Data.TotalCount.ToString;
end;

procedure TframeStatusBar.CommandShowNodeData(const aData: TValue);
begin
  var NodeData := aData.AsType<TNodeData>;

  if NodeData.Node.IsUseInAdditionalStats then begin
    FFinishedCount := FFinishedCount + 1;
    StatusBar.Panels[2].Text := 'Finished count: ' + FFinishedCount.ToString;
  end;
end;

procedure TframeStatusBar.CommandShowNodeDataGroup(const aData: TValue);
begin
  var NodeDataArray := aData.AsType<TNodeDataArray>;

  if Length(NodeDataArray) > 0 then
    for var NodeData in NodeDataArray do
      if NodeData.Node.IsUseInAdditionalStats then
        FFinishedCount := FFinishedCount + 1;

  StatusBar.Panels[2].Text := 'Finished count: ' + FFinishedCount.ToString;
end;

end.
