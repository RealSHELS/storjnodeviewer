object frameStatusBar: TframeStatusBar
  Left = 0
  Top = 0
  Width = 1102
  Height = 40
  TabOrder = 0
  object dxLayoutControl: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 1102
    Height = 40
    Align = alClient
    TabOrder = 0
    object StatusBar: TdxStatusBar
      Left = 36
      Top = 10
      Width = 1056
      Height = 20
      Panels = <
        item
          PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
          MinWidth = 180
          Width = 180
        end
        item
          PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
          MinWidth = 130
          Width = 130
        end
        item
          PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
          MinWidth = 130
          Width = 130
        end
        item
          PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
          Width = 140
        end
        item
          PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
          Width = 150
        end>
      PaintStyle = stpsFlat
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'iMaginary'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
    end
    object btnStorjVersionRefresh: TcxButton
      Left = 10
      Top = 10
      Width = 20
      Height = 20
      Hint = 'Refresh storj version'
      Caption = 'btnStorjVersionRefresh'
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'iMaginary'
      OptionsImage.Glyph.SourceDPI = 96
      OptionsImage.Glyph.SourceHeight = 18
      OptionsImage.Glyph.SourceWidth = 18
      OptionsImage.Glyph.Data = {
        3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D225554
        462D38223F3E0D0A3C7376672076657273696F6E3D22312E31222069643D224C
        617965725F312220786D6C6E733D22687474703A2F2F7777772E77332E6F7267
        2F323030302F7376672220786D6C6E733A786C696E6B3D22687474703A2F2F77
        77772E77332E6F72672F313939392F786C696E6B2220783D223070782220793D
        22307078222076696577426F783D2230203020333220333222207374796C653D
        22656E61626C652D6261636B67726F756E643A6E657720302030203332203332
        3B2220786D6C3A73706163653D227072657365727665223E262331333B262331
        303B3C7374796C6520747970653D22746578742F637373223E2E477265656E7B
        66696C6C3A233033394332333B7D3C2F7374796C653E0D0A3C7061746820636C
        6173733D22477265656E2220643D224D31362C3236632D332E332C302D362E32
        2D312E362D382D346C342D3448362E32682D3448327631306C332E322D332E32
        43372E372C32382C31312E362C33302C31362C333063372E312C302C31322E39
        2D352E322C31332E382D3132682D3420202623393B4332342E392C32322E362C
        32302E382C32362C31362C32367A222F3E0D0A3C7061746820636C6173733D22
        477265656E2220643D224D32362E382C372E324332342E332C342C32302E342C
        322C31362C3243382E392C322C332E312C372E322C322E322C3134683463302E
        392D342E362C352D382C392E382D3863332E332C302C362E322C312E362C382C
        346C2D342C3468352E386834483330563420202623393B4C32362E382C372E32
        7A222F3E0D0A3C2F7376673E0D0A}
      PaintStyle = bpsGlyph
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnStorjVersionRefreshClick
    end
    object dxLayoutControlGroup_Root: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avClient
      Hidden = True
      LayoutDirection = ldHorizontal
      Padding.AssignedValues = [lpavBottom]
      ShowBorder = False
      Index = -1
    end
    object layStatusBar: TdxLayoutItem
      Parent = dxLayoutControlGroup_Root
      AlignHorz = ahClient
      Padding.AssignedValues = [lpavBottom]
      Control = StatusBar
      ControlOptions.OriginalHeight = 20
      ControlOptions.OriginalWidth = 1035
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object laybtnStorjVersionRefresh: TdxLayoutItem
      Parent = dxLayoutControlGroup_Root
      Padding.AssignedValues = [lpavRight]
      CaptionOptions.Visible = False
      Control = btnStorjVersionRefresh
      ControlOptions.OriginalHeight = 20
      ControlOptions.OriginalWidth = 20
      ControlOptions.ShowBorder = False
      Index = 0
    end
  end
end
