unit uframeButtons;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxSkinBasic,
  dxSkiniMaginary, dxSkinSeven, dxSkinSevenClassic, cxClasses,
  dxLayoutContainer, dxLayoutControl, dxLayoutControlAdapters, Vcl.Menus,
  Vcl.StdCtrls, cxButtons, uNotifyCenter, Storj.Consts, dxLayoutcxEditAdapters,
  cxContainer, cxEdit, cxLabel, Storj.Types, System.JSON,
  uframePCRebootersStatus, dxSkinOffice2013LightGray, dxSkinOffice2019DarkGray,
  dxSkinOffice2019White, dxSkinWXI, dxUIAdorners, dxSkinOffice2019Black,
  dxSkinWhiteprint;

type
  TframeButtons = class(TFrame)
    dxLayoutControlGroup_Root: TdxLayoutGroup;
    dxLayoutControl: TdxLayoutControl;
    laybtnSettings: TdxLayoutItem;
    btnSettings: TcxButton;
    laybtnChangeLog: TdxLayoutItem;
    btnChangeLog: TcxButton;
    laybtnShowGlobalLog: TdxLayoutItem;
    btnShowGlobalLog: TcxButton;
    laybtnShowGridView: TdxLayoutItem;
    btnShowGridView: TcxButton;
    laybtnShowFinancialData: TdxLayoutItem;
    btnShowFinancialData: TcxButton;
    laybtnShowCheckNeighboursForm: TdxLayoutItem;
    btnShowCheckNeighboursForm: TcxButton;
    laybtnShowDaily: TdxLayoutItem;
    btnShowDaily: TcxButton;
    laybtnResetSorting: TdxLayoutItem;
    btnResetSorting: TcxButton;
    laybtnRefresh: TdxLayoutItem;
    btnRefresh: TcxButton;
    laybtnTreeSettings: TdxLayoutItem;
    btnTreeSettings: TcxButton;
    lblDisqualified: TcxLabel;
    laylblDisqualified: TdxLayoutItem;
    lblSuspended: TcxLabel;
    laylblSuspended: TdxLayoutItem;
    lblAuditRed: TcxLabel;
    laylblAuditRed: TdxLayoutItem;
    lblAuditYellow: TcxLabel;
    laylblAuditYellow: TdxLayoutItem;
    layHealthData: TdxLayoutGroup;
    layAuditHealth: TdxLayoutGroup;
    laygrpMainHealth: TdxLayoutGroup;
    btnShowHealthData: TcxButton;
    laybtnShowHealthData: TdxLayoutItem;
    lblSuspensionRed: TcxLabel;
    laylblSuspensionRed: TdxLayoutItem;
    lblSuspensionYellow: TcxLabel;
    laylblSuspensionYellow: TdxLayoutItem;
    lblOnlineRed: TcxLabel;
    laylblOnlineRed: TdxLayoutItem;
    lblOnlineYellow: TcxLabel;
    laylblOnlineYellow: TdxLayoutItem;
    laySuspensionHealth: TdxLayoutGroup;
    layOnlineHealth: TdxLayoutGroup;
    lblWholeSNUsed: TcxLabel;
    laylblWholeSNUsed: TdxLayoutItem;
    btnExportData: TcxButton;
    laybtnExportData: TdxLayoutItem;
    btnShowNeighboursTree: TcxButton;
    laybtnShowNeighboursTree: TdxLayoutItem;
    btnShowDailyGridView: TcxButton;
    laybtnShowDailyGridView: TdxLayoutItem;
    btnRebootPC: TcxButton;
    laybtnRebootPC: TdxLayoutItem;
    dxLayoutItem5: TdxLayoutItem;
    framePCRebootersStatus1: TframePCRebootersStatus;
    btnOpenChart: TcxButton;
    laybtnOpenChart: TdxLayoutItem;
    btnRestartUsedDiffer: TcxButton;
    laybtnRestartUsedDiffer: TdxLayoutItem;
    dxUIAdornerManager: TdxUIAdornerManager;
    badgeREDCount: TdxBadge;
    btnLightRefresh: TcxButton;
    laybtnLightRefresh: TdxLayoutItem;
    btnOpenLocalHistory: TcxButton;
    laybtnOpenLocalHistory: TdxLayoutItem;
    btnDashboard: TcxButton;
    laybtnDashboard: TdxLayoutItem;
    btnOpenSSHExecute: TcxButton;
    laybtnOpenSSHExecute: TdxLayoutItem;
    procedure btnTreeSettingsClick(Sender: TObject);
    procedure btnResetSortingClick(Sender: TObject);
    procedure btnRefreshClick(Sender: TObject);
    procedure btnShowDailyClick(Sender: TObject);
    procedure btnShowCheckNeighboursFormClick(Sender: TObject);
    procedure btnShowFinancialDataClick(Sender: TObject);
    procedure btnShowGridViewClick(Sender: TObject);
    procedure btnShowGlobalLogClick(Sender: TObject);
    procedure btnChangeLogClick(Sender: TObject);
    procedure btnSettingsClick(Sender: TObject);
    procedure btnShowHealthDataClick(Sender: TObject);
    procedure btnExportDataClick(Sender: TObject);
    procedure btnShowNeighboursTreeClick(Sender: TObject);
    procedure btnShowDailyGridViewClick(Sender: TObject);
    procedure btnRebootPCClick(Sender: TObject);
    procedure btnOpenChartClick(Sender: TObject);
    procedure btnRestartUsedDifferClick(Sender: TObject);
    procedure btnLightRefreshClick(Sender: TObject);
    procedure btnOpenLocalHistoryClick(Sender: TObject);
    procedure btnDashboardClick(Sender: TObject);
    procedure btnOpenSSHExecuteClick(Sender: TObject);
  private
    FAuditRedCountNode: Integer;
    FAuditYellowCountNode: Integer;
    FOnlineRedCountNode: Integer;
    FOnlineYellowCountNode: Integer;
    FSuspensionRedCountNode: Integer;
    FSuspensionYellowCountNode: Integer;
    FDisqualifiedCountNode: Integer;
    FSuspendedCountNode: Integer;

    FAuditRedCountSatellite: Integer;
    FAuditYellowCountSatellite: Integer;
    FOnlineRedCountSatellite: Integer;
    FOnlineYellowCountSatellite: Integer;
    FSuspensionRedCountSatellite: Integer;
    FSuspensionYellowCountSatellite: Integer;
    FDisqualifiedCountSatellite: Integer;
    FSuspendedCountSatellite: Integer;

    FStorjNetworkData: TStorjNetworkData;
    FNodesUsedGB: Integer;

    procedure SetToEmptyHealthHotData;
    procedure DisplayHealthHotData;
    procedure DisplayStorjNetworkData;
    procedure CalculateHotData(const aNodeData: TNodeData);

    procedure CommandSettingsChanged(const aData: TValue);
    procedure CommandStorjNetworkData(const aData: TValue);
    procedure CommandBeforeStartGetNodeData(const aData: TValue);
    procedure CommandShowNodeData(const aData: TValue);
    procedure CommandShowNodeDataGroup(const aData: TValue);
    procedure CommandDifferCount(const aData: TValue);
    procedure CommandAfterGetNodeData(const aData: Tvalue);
  public
    constructor Create(AOwnder: TComponent); override;
    destructor Destroy; override;
  end;

implementation

{$R *.dfm}

uses
  uDM, Storj.Settings;

procedure TframeButtons.btnChangeLogClick(Sender: TObject);
begin
  TNotifyCenter.Instance.SendMessage(COMMAND_SHOW_CHANGE_LOG, nil);
end;

procedure TframeButtons.btnDashboardClick(Sender: TObject);
begin
  TNotifyCenter.Instance.SendMessage(COMMAND_SHOW_DASHBOARD, nil);
end;

procedure TframeButtons.btnExportDataClick(Sender: TObject);
begin
  TNotifyCenter.Instance.SendMessage(COMMAND_EXPORT_DATA, nil);
end;

procedure TframeButtons.btnLightRefreshClick(Sender: TObject);
begin
  DM.LoadNodesData(TGetInfoMode.gimLight, TLoadSourceMode.lsmManual);
end;

procedure TframeButtons.btnOpenChartClick(Sender: TObject);
begin
  TNotifyCenter.Instance.SendMessage(COMMAND_SHOW_CHART, nil);
end;

procedure TframeButtons.btnOpenLocalHistoryClick(Sender: TObject);
begin
  TNotifyCenter.Instance.SendMessage(COMMAND_SHOW_LOCAL_HISTORY, nil);
end;

procedure TframeButtons.btnOpenSSHExecuteClick(Sender: TObject);
begin
  TNotifyCenter.Instance.SendMessage(COMMAND_SHOW_SSH_COMMAND_EXECUTE, nil);
end;

procedure TframeButtons.btnRebootPCClick(Sender: TObject);
begin
  TNotifyCenter.Instance.SendMessage(COMMAND_SHOW_PCREBOOTER, nil);
end;

procedure TframeButtons.btnRefreshClick(Sender: TObject);
begin
  DM.LoadNodesData(TGetInfoMode.gimStandart, TLoadSourceMode.lsmManual);
end;

procedure TframeButtons.btnResetSortingClick(Sender: TObject);
begin
  TNotifyCenter.Instance.SendMessage(COMMAND_RESET_SORTING, nil);
end;

procedure TframeButtons.btnRestartUsedDifferClick(Sender: TObject);
begin
  TNotifyCenter.Instance.SendMessage(COMMAND_SHOW_RESTART_USED_DIFFER, nil);
end;

procedure TframeButtons.btnSettingsClick(Sender: TObject);
begin
  TNotifyCenter.Instance.SendMessage(COMMAND_SHOW_SETTINGS, nil);
end;

procedure TframeButtons.btnShowCheckNeighboursFormClick(Sender: TObject);
begin
  TNotifyCenter.Instance.SendMessage(COMMAND_SHOW_CHECK_NEIGHBOURS, '');
end;

procedure TframeButtons.btnShowDailyClick(Sender: TObject);
begin
  TNotifyCenter.Instance.SendMessage(COMMAND_SHOW_DAILY_DATA_TREE, nil);
end;

procedure TframeButtons.btnShowDailyGridViewClick(Sender: TObject);
begin
  TNotifyCenter.Instance.SendMessage(COMMAND_SHOW_DAILY_DATA_GRID, nil);
end;

procedure TframeButtons.btnShowFinancialDataClick(Sender: TObject);
begin
  TNotifyCenter.Instance.SendMessage(COMMAND_SHOW_FINANCIAL_DATA_TREE, nil);
end;

procedure TframeButtons.btnShowGlobalLogClick(Sender: TObject);
begin
  TNotifyCenter.Instance.SendMessage(COMMAND_SHOW_GLOBAL_LOG, nil);
end;

procedure TframeButtons.btnShowGridViewClick(Sender: TObject);
begin
  TNotifyCenter.Instance.SendMessage(COMMAND_SHOW_GRID_VIEW, nil);
end;

procedure TframeButtons.btnShowHealthDataClick(Sender: TObject);
begin
  TNotifyCenter.Instance.SendMessage(COMMAND_SHOW_HEALTH_TREE, nil);
end;

procedure TframeButtons.btnShowNeighboursTreeClick(Sender: TObject);
begin
  TNotifyCenter.Instance.SendMessage(COMMAND_SHOW_NEIGHBOURS_TREE, nil);
end;

procedure TframeButtons.btnTreeSettingsClick(Sender: TObject);
begin
  TNotifyCenter.Instance.SendMessage(COMMAND_SHOW_TREE_SETTINGS, nil);
end;

procedure TframeButtons.CalculateHotData(const aNodeData: TNodeData);
begin
  var DisqualifiedCountSatellite := 0;
  var SuspendedCountSatellite := 0;
  var AuditRedCountSatellite := 0;
  var AuditYellowCountSatellite := 0;
  var OnlineRedCountSatellite := 0;
  var OnlineYellowCountSatellite := 0;
  var SuspensionRedCountSatellite := 0;
  var SuspensionYellowCountSatellite := 0;

  for var Satellite in aNodeData.Satellites do begin
    if Satellite.Health.IsDisqualified then begin
      DisqualifiedCountSatellite := DisqualifiedCountSatellite + 1;
      Continue;
    end;

    if Satellite.Health.IsSuspended then
      SuspendedCountSatellite := SuspendedCountSatellite + 1;

    if Satellite.Health.AuditPerc > 0 then begin
      if Satellite.Health.AuditPerc < GetSettings.HealthAuditRed then
        AuditRedCountSatellite := AuditRedCountSatellite + 1
      else if Satellite.Health.AuditPerc < GetSettings.HealthAuditYellow then
        AuditYellowCountSatellite := AuditYellowCountSatellite + 1;
    end;

    if Satellite.Health.OnlinePerc > 0 then begin
      if Satellite.Health.OnlinePerc < GetSettings.HealthOnlineRed then
        OnlineRedCountSatellite := OnlineRedCountSatellite + 1
      else if Satellite.Health.OnlinePerc < GetSettings.HealthOnlineYellow then
        OnlineYellowCountSatellite := OnlineYellowCountSatellite + 1;
    end;

    if Satellite.Health.SuspensionPerc > 0 then begin
      if Satellite.Health.SuspensionPerc < GetSettings.HealthSuspensionRed then
        SuspensionRedCountSatellite := SuspensionRedCountSatellite + 1
      else if Satellite.Health.SuspensionPerc < GetSettings.HealthSuspensionYellow then
        SuspensionYellowCountSatellite := SuspensionYellowCountSatellite + 1;
    end;
  end;

  FDisqualifiedCountSatellite := FDisqualifiedCountSatellite + DisqualifiedCountSatellite;
  if DisqualifiedCountSatellite > 0 then
    FDisqualifiedCountNode := FDisqualifiedCountNode + 1;

  FSuspendedCountSatellite := FSuspendedCountSatellite + SuspendedCountSatellite;
  if SuspendedCountSatellite > 0 then
    FSuspendedCountNode := FSuspendedCountNode + 1;

  FAuditRedCountSatellite := FAuditRedCountSatellite + AuditRedCountSatellite;
  FAuditYellowCountSatellite := FAuditYellowCountSatellite + AuditYellowCountSatellite;
  if AuditRedCountSatellite > 0 then
    FAuditRedCountNode := FAuditRedCountNode + 1;
  if AuditYellowCountSatellite > 0 then
    FAuditYellowCountNode := FAuditYellowCountNode + 1;

  FOnlineRedCountSatellite := FOnlineRedCountSatellite + OnlineRedCountSatellite;
  FOnlineYellowCountSatellite := FOnlineYellowCountSatellite + OnlineYellowCountSatellite;
  if OnlineRedCountSatellite > 0 then
    FOnlineRedCountNode := FOnlineRedCountNode + 1;
  if OnlineYellowCountSatellite > 0 then
    FOnlineYellowCountNode := FOnlineYellowCountNode + 1;

  FSuspensionRedCountSatellite := FSuspensionRedCountSatellite + SuspensionRedCountSatellite;
  FSuspensionYellowCountSatellite := FSuspensionYellowCountSatellite + SuspensionYellowCountSatellite;
  if SuspensionRedCountSatellite > 0 then
    FSuspensionRedCountNode := FSuspensionRedCountNode + 1;
  if SuspensionYellowCountSatellite > 0 then
    FSuspensionYellowCountNode := FSuspensionYellowCountNode + 1;

  FNodesUsedGB := FNodesUsedGB + aNodeData.DiskSpace.Used;
end;

procedure TframeButtons.CommandAfterGetNodeData(const aData: Tvalue);
begin
  laybtnRefresh.Enabled := True;
  laybtnLightRefresh.Enabled := True;
  laybtnSettings.Enabled := True;
  laybtnTreeSettings.Enabled := True;
  if laybtnResetSorting.Visible then
    laybtnResetSorting.Enabled := True;
  if laybtnExportData.Visible then
    laybtnExportData.Enabled := True;
end;

procedure TframeButtons.CommandSettingsChanged(const aData: TValue);
begin
  laygrpMainHealth.Visible := GetSettings.IsDisplayHealthHotData;
  laylblWholeSNUsed.Visible := GetSettings.IsDisplayStorjNetUsed;

  btnRebootPC.Visible := GetSettings.IsEnabledPCRebooter;
end;

procedure TframeButtons.CommandStorjNetworkData(const aData: TValue);
begin
  FStorjNetworkData := aData.AsType<TStorjNetworkData>;
end;

constructor TframeButtons.Create(AOwnder: TComponent);
begin
  inherited;

  SetToEmptyHealthHotData;

  TNotifyCenter.Instance.Subscribe(COMMAND_SETTING_CHANGED, CommandSettingsChanged);
  TNotifyCenter.Instance.Subscribe(COMMAND_STORJ_NETWORK_DATA, CommandStorjNetworkData);
  TNotifyCenter.Instance.Subscribe(COMMAND_BEFORE_START_GET_NODE, CommandBeforeStartGetNodeData);
  TNotifyCenter.Instance.Subscribe(COMMAND_SHOW_NODE_DATA, CommandShowNodeData);
  TNotifyCenter.Instance.Subscribe(COMMAND_SHOW_NODE_DATA_GROUP, CommandShowNodeDataGroup);
  TNotifyCenter.Instance.Subscribe(COMMAND_RESTART_USED_DIFFER_COUNT, CommandDifferCount);
  TNotifyCenter.Instance.Subscribe(COMMAND_AFTER_GET_NODE_DATA, CommandAfterGetNodeData);

  CommandSettingsChanged(nil);

  var YellowColor := RGB(153, 153, 0);
  lblOnlineYellow.Style.TextColor := YellowColor;
  lblSuspended.Style.TextColor := YellowColor;
  lblAuditYellow.Style.TextColor := YellowColor;
  lblSuspensionYellow.Style.TextColor := YellowColor;
end;

destructor TframeButtons.Destroy;
begin
  TNotifyCenter.Instance.Unsubscribe(COMMAND_SETTING_CHANGED, CommandSettingsChanged);
  TNotifyCenter.Instance.Unsubscribe(COMMAND_STORJ_NETWORK_DATA, CommandStorjNetworkData);
  TNotifyCenter.Instance.Unsubscribe(COMMAND_BEFORE_START_GET_NODE, CommandBeforeStartGetNodeData);
  TNotifyCenter.Instance.UnSubscribe(COMMAND_SHOW_NODE_DATA, CommandShowNodeData);
  TNotifyCenter.Instance.UnSubscribe(COMMAND_SHOW_NODE_DATA_GROUP, CommandShowNodeDataGroup);
  TNotifyCenter.Instance.Unsubscribe(COMMAND_RESTART_USED_DIFFER_COUNT, CommandDifferCount);
  TNotifyCenter.Instance.Unsubscribe(COMMAND_AFTER_GET_NODE_DATA, CommandAfterGetNodeData);

  inherited;
end;

procedure TframeButtons.DisplayHealthHotData;
begin
  lblDisqualified.Caption :=  Format('Disqualified: %d/%d', [FDisqualifiedCountNode, FDisqualifiedCountSatellite]);
  lblSuspended.Caption := Format('Suspended: %d/%d', [FSuspendedCountNode, FSuspendedCountSatellite]);

  lblAuditRed.Caption := Format('Audit: %d/%d', [FAuditRedCountNode, FAuditRedCountSatellite]);
  lblAuditYellow.Caption := Format('Audit: %d/%d', [FAuditYellowCountNode, FAuditYellowCountSatellite]);

  lblOnlineRed.Caption := Format('Online: %d/%d', [FOnlineRedCountNode, FOnlineRedCountSatellite]);
  lblOnlineYellow.Caption := Format('Online: %d/%d', [FOnlineYellowCountNode, FOnlineYellowCountSatellite]);

  lblSuspensionRed.Caption := Format('Suspension: %d/%d', [FSuspensionRedCountNode, FSuspensionRedCountSatellite]);
  lblSuspensionYellow.Caption := Format('Suspension: %d/%d', [FSuspensionYellowCountNode, FSuspensionYellowCountSatellite]);
end;

procedure TframeButtons.DisplayStorjNetworkData;
var
  PB, Perc: Double;
begin
  PB := FStorjNetworkData.UsedSpace / 1000000;
  Perc := 0;
  if FStorjNetworkData.UsedSpace > 0 then
    Perc := FNodesUsedGB / FStorjNetworkData.UsedSpace * 100;

  lblWholeSNUsed.Caption := Format('Storj net used: %3.3f PB (%2.4f %%)', [PB, Perc]);
end;

procedure TframeButtons.CommandBeforeStartGetNodeData(const aData: TValue);
begin
  FStorjNetworkData.UsedSpace := 0;
  FNodesUsedGB := 0;

  laybtnRefresh.Enabled := False;
  laybtnLightRefresh.Enabled := False;
  laybtnSettings.Enabled := False;
  laybtnTreeSettings.Enabled := False;
  if laybtnResetSorting.Visible then
    laybtnResetSorting.Enabled := False;
  if laybtnExportData.Visible then
    laybtnExportData.Enabled := False;

  SetToEmptyHealthHotData;
  DisplayHealthHotData;
end;

procedure TframeButtons.CommandDifferCount(const aData: TValue);
begin
  badgeREDCount.Text := aData.AsInteger.ToString;
  badgeREDCount.Visible := aData.AsInteger > 0;
end;

procedure TframeButtons.SetToEmptyHealthHotData;
begin
  FAuditRedCountNode := 0;
  FAuditYellowCountNode := 0;
  FOnlineRedCountNode := 0;
  FOnlineYellowCountNode := 0;
  FSuspensionRedCountNode := 0;
  FSuspensionYellowCountNode := 0;
  FDisqualifiedCountNode := 0;
  FSuspendedCountNode := 0;

  FAuditRedCountSatellite := 0;
  FAuditYellowCountSatellite := 0;
  FOnlineRedCountSatellite := 0;
  FOnlineYellowCountSatellite := 0;
  FSuspensionRedCountSatellite := 0;
  FSuspensionYellowCountSatellite := 0;
  FDisqualifiedCountSatellite := 0;
  FSuspendedCountSatellite := 0;
end;

procedure TframeButtons.CommandShowNodeData(const aData: TValue);
begin
  var NodeData := aData.AsType<TNodeData>;

  if not NodeData.Node.IsUseInAdditionalStats then
    Exit;

  CalculateHotData(NodeData);

  DisplayHealthHotData;
  DisplayStorjNetworkData;
end;

procedure TframeButtons.CommandShowNodeDataGroup(const aData: TValue);
begin
  var NodeDataArray := aData.AsType<TNodeDataArray>;

  for var NodeData in NodeDataArray do
    if NodeData.Node.IsUseInAdditionalStats then
      CalculateHotData(NodeData);

  DisplayHealthHotData;
  DisplayStorjNetworkData;
end;

end.
