object frmSSHCommandExecute: TfrmSSHCommandExecute
  Left = 0
  Top = 0
  Caption = 'SSH Command Execute'
  ClientHeight = 598
  ClientWidth = 605
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  TextHeight = 15
  object dxLayoutControl: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 605
    Height = 598
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 601
    ExplicitHeight = 597
    object comboFullPath: TcxComboBox
      Left = 61
      Top = 12
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownRows = 20
      Properties.OnChange = comboFullPathPropertiesChange
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 0
      Width = 532
    end
    object lblSelectedHost: TcxLabel
      Left = 12
      Top = 42
      Style.HotTrack = False
      Style.TransparentBorder = False
      Transparent = True
    end
    object btnExecute: TcxButton
      Left = 518
      Top = 42
      Width = 75
      Height = 25
      Caption = 'Execute'
      TabOrder = 2
      OnClick = btnExecuteClick
    end
    object chklistCommands: TcxCheckListBox
      Left = 12
      Top = 95
      Width = 581
      Height = 221
      Items = <>
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = cbs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 3
    end
    object mmoLog: TcxMemo
      Left = 12
      Top = 344
      Properties.ScrollBars = ssVertical
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 4
      Height = 242
      Width = 581
    end
    object dxLayoutControlGroup_Root: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avClient
      Hidden = True
      ItemIndex = 1
      ShowBorder = False
      Index = -1
    end
    object laycomboFullPath: TdxLayoutItem
      Parent = dxLayoutControlGroup_Root
      CaptionOptions.Text = 'FullPath'
      Control = comboFullPath
      ControlOptions.OriginalHeight = 23
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object laylblSelctedHost: TdxLayoutItem
      Parent = grpTopControls
      AlignHorz = ahClient
      CaptionOptions.Text = 'cxLabel1'
      CaptionOptions.Visible = False
      Control = lblSelectedHost
      ControlOptions.OriginalHeight = 15
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object laybtnExecute: TdxLayoutItem
      Parent = grpTopControls
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnExecute
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object laychklistCommands: TdxLayoutItem
      Parent = dxLayoutControlGroup_Root
      AlignVert = avClient
      CaptionOptions.Text = 'Commands'
      CaptionOptions.Layout = clTop
      Control = chklistCommands
      ControlOptions.OriginalHeight = 97
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object laymmoLog: TdxLayoutItem
      Parent = dxLayoutControlGroup_Root
      CaptionOptions.Text = 'Log'
      CaptionOptions.Layout = clTop
      Control = mmoLog
      ControlOptions.OriginalHeight = 242
      ControlOptions.OriginalWidth = 185
      ControlOptions.ShowBorder = False
      Index = 3
    end
    object grpTopControls: TdxLayoutGroup
      Parent = dxLayoutControlGroup_Root
      CaptionOptions.Text = 'New Group'
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 1
    end
  end
end
