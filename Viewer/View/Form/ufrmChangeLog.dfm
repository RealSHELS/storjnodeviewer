object frmChangeLog: TfrmChangeLog
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Change log'
  ClientHeight = 323
  ClientWidth = 567
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  TextHeight = 13
  object mmoChangeLog: TMemo
    Left = 0
    Top = 0
    Width = 567
    Height = 293
    Align = alClient
    Lines.Strings = (
      'v0.9.3'
      ' - Remove "Columns used by satellite" feature from GridView'
      ' - Change get currency data to own server'
      ' - Renew chart control'
      ' - Improve formating data in columns in TreeView'
      'v0.9.2.1'
      ' - Remove used space by satellite'
      'v0.9.2'
      ' - Added feature to execute predefined commands on VPS by SSH'
      ' - Fix bug when empty node list'
      'v0.9.1'
      ' - Added in chart display by LocalHistory'
      
        ' - Fix calculation clean profit in Financial Tree when cost is n' +
        'ot set'
      ' - Added notify by telegram and email'
      
        ' - For Used Differfeature add calculate only node with differenc' +
        'e more than 1 gb'
      ' - Fix total summary in DailyGridView'
      ' - Added info for feedback'
      'v0.9.0'
      
        ' - Added setting to set is main window open in full screen at ap' +
        'p start'
      ' - Added save data to local history, and display it'
      ' - Added timer for auto refresh'
      ' - Added ability display in log TreePath instead node title'
      
        ' - Added Efficiency by TBM columns in GridView and Financial tre' +
        'e'
      ' - Added using proxy for get neighbours'
      'v0.8.1'
      ' - Improve loading data for disqualified satellite'
      ' - fix duplicating PCRebooter online checking in each frame'
      ' - added empty init value for data'
      ' - in Daily views added current day TBMonth'
      ' - Added TB*Month by satellites'
      ' - Added "Light Refresh" '
      'v0.8.0'
      ' - Improve coloring trees'
      ' - In GridView context menu change items ordering'
      ' - Added Restart Used Differ'
      ' - Move to new IDE and libs version'
      ' - in Chart some UI improvements'
      ' - Other UI improvements'
      ' - Added Wallet column into GridView as hidden column'
      'v0.7.0'
      ' - Changed old TB*M calculating to Avg. TB*M'
      ' - Added Historical Charts'
      ' - In GridView added Daily data as hidden columns'
      ' - Added Current Month Charts'
      ' - In GridView added new column Uptime'
      
        ' - Added Avg. TB*M to Used space deviation treshold settings and' +
        ' coloring'
      'v0.6.3'
      ' - Added column Free to Daily GridView'
      ' - Added satellites used space data in GridView as columns'
      'v0.6.2'
      ' - Added saving/loading GridView grid column settings'
      ' - Added TreePath column to GridView, hidden by default'
      ' - Added right-click context menu into GridView'
      'v.0.6.1'
      ' - Added feature PCRebooter direct server'
      ' - Reduce time limit for check neighbours to 2 hours'
      ' - Add Comment column to neighbours tree'
      'v.0.6.0.1'
      ' - Fix data in Daily grid view'
      'v0.6.0'
      ' - Add settings to display or not Storj net used'
      ' - A little rework UI in settings'
      ' - Added Trash for display and for calculate Free space'
      ' - Not allow run second app copy'
      ' - Added Daily GridView'
      ' - Added display own neighbours for node in Neighbours tree'
      ' - Added informing for Offline node in log'
      'v0.5.0'
      ' - Added export GridView to Excel (.xslx)'
      ' - Added refresh single node data by right click context menu'
      ' - Fix not correct check for IsSuspended and IsDisqualified'
      ' - Addapt old TB*h (up to 1.66) to TB*d (from 1.67)'
      ' - Make to start app without any additional files'
      
        ' - Add to Financial data tree column with node total earned, act' +
        'ivated by context menu'
      ' - Add new form with display neighbours for all nodes'
      'v0.4.3'
      ' - Added info about storj network used and our percent of it'
      ' - Fixed display satellite daily data'
      ' - Add settings and log yellow/red nodes'
      'v0.4.2'
      ' - Remove column Health from main tree'
      ' - Added Health tree data in separate form'
      
        ' - In main toolbar added health hot data with settings to turn o' +
        'ff it'
      ' - When close main form add confirmation ask to close app'
      
        ' - Added protection from double clicking/starting refresh nodes ' +
        'data'
      ' - Refactor StatusBar and added it to GridView'
      'v0.4.1'
      ' - Refactor toolbar with buttons and added it in GridView'
      ' - Added Efficiency column for GridView'
      ' - Added setting HDD Volume and display it in GridView'
      ' - In GridView added column  NodeID, by default is hidden'
      'v.0.4.0'
      
        ' - Fix bug with lost data when saving tree with active search te' +
        'xt'
      
        ' - Added autobackup tree settings to Tree.json.bak file before e' +
        'ach saving'
      ' - Added Comment column to Financial Data Tree'
      ' - Added sorting in Daily Data tree'
      ' - Added GridView'
      ' - Added Health column in main tree and settings for it'
      ' - Added global log viewer'
      'v.0.3'
      ' - Added currency and cost settings'
      ' - Added financial form'
      ' - Move Efficiency column from main tree'
      'v.0.2.4'
      ' - Added Comment column in tree settings'
      'v.0.2.3'
      ' - Added Efficiency column'
      'v0.2.2'
      '  - Added search in tree settings'
      '  - Back sorting by Title column'
      'v0.2.1'
      '  - Fixed bug in sort column'
      '  - Added seetings timeout for get data from node'
      '  - Fix some typo'
      'v0.2.0'
      '  - Reworking sorting column'
      '  - Added AutoExpand tree setting'
      '  - Added check for neighbours'
      '  - Added settings'
      '  - Added colored Free Space cell depends on settings')
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object lblFeedback: TdxFormattedLabel
    Left = 0
    Top = 293
    Align = alBottom
    Caption = 
      'Feedback is crucial! If you have any suggestions, observations, ' +
      'proposals, or information about errors, '#13#10'please feel free to wr' +
      'ite at [URL=mailto:StorjNodeViewer@gmail.com]StorjNodeViewer@gma' +
      'il.com[/URL]. Your feedback helps improve this product!'
    Properties.Alignment.Horz = taCenter
    Properties.Alignment.Vert = taVCenter
    AnchorX = 284
    AnchorY = 308
  end
end
