object frmLocalHistory: TfrmLocalHistory
  Left = 0
  Top = 0
  Caption = 'Local History'
  ClientHeight = 559
  ClientWidth = 1054
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  TextHeight = 13
  object dxLayoutControl: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 1054
    Height = 559
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 1050
    ExplicitHeight = 558
    object GridView: TcxGrid
      Left = 10000
      Top = 10000
      Width = 998
      Height = 449
      TabOrder = 4
      Visible = False
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'SevenClassic'
      object GridViewDBTableView: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        FindPanel.DisplayMode = fpdmAlways
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = DM.dtsrcLocalHistoryGrid
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            FieldName = 'Egress'
            Column = columnGridEgress
          end
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            FieldName = 'Ingress'
            Column = columnGridIngress
          end
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            FieldName = 'RepairEgress'
            Column = columnGridRepairEgress
          end
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            FieldName = 'RepairIngress'
            Column = columnGridRepairIngress
          end
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            FieldName = 'TotalBandwidth'
            Column = columnGridTotalBandwidth
          end
          item
            Kind = skSum
            FieldName = 'TBM'
            Column = columnGridTBM
          end
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            FieldName = 'DailyRepairEgress'
            Column = columnGridDailyEgress
          end
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            FieldName = 'DailyIngress'
            Column = columnGridDailyIngress
          end
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            FieldName = 'DailyRepairEgress'
            Column = columnGridDailyRepairEgress
          end
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            FieldName = 'DailyRepairIngress'
            Column = columnGridDailyRepairIngress
          end
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            FieldName = 'DailyTotalBandwidth'
            Column = columnGridDailyTotalBandwidth
          end
          item
            Kind = skSum
            FieldName = 'DailyTBMonth'
            Column = columnGridDailyTBMonth
          end
          item
            Format = '### ##0 GB'
            Kind = skSum
            FieldName = 'AllocatedSpace'
            Column = columnGridAllocatedSpace
          end
          item
            Format = '### ##0 GB'
            Kind = skSum
            FieldName = 'FreeSpace'
            Column = columnGridFreeSpace
          end
          item
            Format = '### ##0 GB'
            Kind = skSum
            FieldName = 'UsedSpace'
            Column = columnGridUsedSpace
          end
          item
            Format = '### ##0 GB'
            Kind = skSum
            FieldName = 'Trash'
            Column = columnGridTrash
          end
          item
            Format = ',0.00 $;-,0.00 $'
            Kind = skSum
            FieldName = 'PayoutDirty'
            Column = columnGridPayoutDirty
          end
          item
            Format = ',0.00 $;-,0.00 $'
            Kind = skSum
            FieldName = 'PayoutCleanly'
            Column = columnGridPayoutCleanly
          end
          item
            Format = ',0.00 $;-,0.00 $'
            Kind = skSum
            FieldName = 'EstimatedCleanly'
            Column = columnGridEstimatedCleanly
          end
          item
            Format = ',0.00 $;-,0.00 $'
            Kind = skSum
            FieldName = 'EstimatedDirty'
            Column = columnGridEstimatedDirty
          end
          item
            Format = 'Count: ##'
            Kind = skCount
            FieldName = 'Title'
            Column = columnGridTitle
          end>
        DataController.Summary.SummaryGroups = <>
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.Footer = True
        object columnGridTitle: TcxGridDBColumn
          DataBinding.FieldName = 'Title'
          Options.Editing = False
          Width = 156
        end
        object columnGridAddress: TcxGridDBColumn
          DataBinding.FieldName = 'Address'
          Options.Editing = False
          Width = 197
        end
        object columnGridPort: TcxGridDBColumn
          DataBinding.FieldName = 'Port'
          PropertiesClassName = 'TcxLabelProperties'
          Properties.Alignment.Horz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 160
        end
        object columnGridTBM: TcxGridDBColumn
          Caption = 'Avg. TB*M'
          DataBinding.FieldName = 'TBM'
          PropertiesClassName = 'TcxLabelProperties'
          Properties.Alignment.Horz = taCenter
          OnCustomDrawCell = columnGridTBMCustomDrawCell
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 162
        end
        object columnGridIngress: TcxGridDBColumn
          DataBinding.FieldName = 'Ingress'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 159
        end
        object columnGridEgress: TcxGridDBColumn
          DataBinding.FieldName = 'Egress'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 162
        end
        object columnGridRepairIngress: TcxGridDBColumn
          Caption = 'Repair Ingress'
          DataBinding.FieldName = 'RepairIngress'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object columnGridRepairEgress: TcxGridDBColumn
          Caption = 'Repair Egress'
          DataBinding.FieldName = 'RepairEgress'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object columnGridAllocatedSpace: TcxGridDBColumn
          Caption = 'Allocated Space'
          DataBinding.FieldName = 'AllocatedSpace'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = '### ##0 GB'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object columnGridFreeSpace: TcxGridDBColumn
          Caption = 'Free Space'
          DataBinding.FieldName = 'FreeSpace'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = '### ##0 GB'
          OnCustomDrawCell = columnGridFreeSpaceCustomDrawCell
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object columnGridUsedSpace: TcxGridDBColumn
          Caption = 'Used Space'
          DataBinding.FieldName = 'UsedSpace'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = '### ##0 GB'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object columnGridPayoutDirty: TcxGridDBColumn
          Caption = 'Payout Dirty'
          DataBinding.FieldName = 'PayoutDirty'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 $;-,0.00 $'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object columnGridPayoutCleanly: TcxGridDBColumn
          Caption = 'Payout Cleanly'
          DataBinding.FieldName = 'PayoutCleanly'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 $;-,0.00 $'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object columnGridEstimatedDirty: TcxGridDBColumn
          Caption = 'Estimated Dirty'
          DataBinding.FieldName = 'EstimatedDirty'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 $;-,0.00 $'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object columnGridEstimatedCleanly: TcxGridDBColumn
          Caption = 'Estimated Cleanly'
          DataBinding.FieldName = 'EstimatedCleanly'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 $;-,0.00 $'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object columnGridVersion: TcxGridDBColumn
          DataBinding.FieldName = 'Version'
          Options.Editing = False
        end
        object columnGridEfficiency: TcxGridDBColumn
          DataBinding.FieldName = 'Efficiency'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 $;-,0.00 $'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object columnGridHDDVolume: TcxGridDBColumn
          Caption = 'HDD Volume'
          DataBinding.FieldName = 'HDDVolume'
          PropertiesClassName = 'TcxLabelProperties'
          Properties.Alignment.Horz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object columnGridNodeID: TcxGridDBColumn
          DataBinding.FieldName = 'NodeID'
          Options.Editing = False
        end
        object columnGridTrash: TcxGridDBColumn
          DataBinding.FieldName = 'Trash'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = '### ##0 GB'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object columnGridFullPath: TcxGridDBColumn
          Caption = 'Tree Path'
          DataBinding.FieldName = 'FullPath'
          Options.Editing = False
        end
        object columnGridTotalBandwidth: TcxGridDBColumn
          Caption = 'Total Bandwidth'
          DataBinding.FieldName = 'TotalBandwidth'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object columnGridDailyEgress: TcxGridDBColumn
          Caption = 'Daily Egress'
          DataBinding.FieldName = 'DailyEgress'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object columnGridDailyIngress: TcxGridDBColumn
          Caption = 'Daily Ingress'
          DataBinding.FieldName = 'DailyIngress'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object columnGridDailyRepairIngress: TcxGridDBColumn
          Caption = 'Daily Repair Ingress'
          DataBinding.FieldName = 'DailyRepairIngress'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object columnGridDailyRepairEgress: TcxGridDBColumn
          Caption = 'Daily Repair Egress'
          DataBinding.FieldName = 'DailyRepairEgress'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object columnGridDailyTotalBandwidth: TcxGridDBColumn
          Caption = 'Daily Total Bandwidth'
          DataBinding.FieldName = 'DailyTotalBandwidth'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object columnGridUptimeMinutes: TcxGridDBColumn
          Caption = 'Uptime Minutes'
          DataBinding.FieldName = 'UptimeMinutes'
          PropertiesClassName = 'TcxLabelProperties'
          Properties.Alignment.Horz = taCenter
          OnGetDataText = columnGridUptimeMinutesGetDataText
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object columnGridWallet: TcxGridDBColumn
          DataBinding.FieldName = 'Wallet'
          Options.Editing = False
        end
        object columnGridDailyTBMonth: TcxGridDBColumn
          Caption = 'Daily TB*M'
          DataBinding.FieldName = 'DailyTBMonth'
          PropertiesClassName = 'TcxLabelProperties'
          Properties.Alignment.Horz = taCenter
          Visible = False
          OnCustomDrawCell = columnGridTBMCustomDrawCell
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
      end
      object GridViewLevel1: TcxGridLevel
        GridView = GridViewDBTableView
      end
    end
    object DailyGridView: TcxGrid
      Left = 10000
      Top = 10000
      Width = 998
      Height = 449
      TabOrder = 6
      Visible = False
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'SevenClassic'
      object DailyGridViewDBTableView: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = DM.dtsrcLocalHistoryGrid
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
            Kind = skSum
            FieldName = 'DailyTBMonth'
            Column = columnDailyGridTBMonth
          end
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            FieldName = 'DailyEgress'
            Column = columnDailyGridEgress
          end
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            FieldName = 'DailyIngress'
            Column = columnDailyGridIngress
          end
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            FieldName = 'DailyRepairIngress'
            Column = columnDailyGridRepairIngress
          end
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            FieldName = 'DailyRepairEgress'
            Column = columnDailyGridRepairEgress
          end
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            FieldName = 'DailyTotalBandwidth'
            Column = columnDailyGridTotalBandwidth
          end>
        DataController.Summary.SummaryGroups = <>
        OptionsView.ColumnAutoWidth = True
        OptionsView.Footer = True
        object columnDailyTitle: TcxGridDBColumn
          DataBinding.FieldName = 'Title'
          Options.Editing = False
          Width = 154
        end
        object columnDailyAddress: TcxGridDBColumn
          DataBinding.FieldName = 'Address'
          Options.Editing = False
          Width = 107
        end
        object columnDailyPort: TcxGridDBColumn
          DataBinding.FieldName = 'Port'
          PropertiesClassName = 'TcxLabelProperties'
          Properties.Alignment.Horz = taCenter
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 70
        end
        object columnDailyGridTBMonth: TcxGridDBColumn
          Caption = 'TB * M'
          DataBinding.FieldName = 'DailyTBMonth'
          PropertiesClassName = 'TcxLabelProperties'
          Properties.Alignment.Horz = taCenter
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 110
        end
        object columnDailyGridEgress: TcxGridDBColumn
          Caption = 'Egress'
          DataBinding.FieldName = 'DailyEgress'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 112
        end
        object columnDailyGridIngress: TcxGridDBColumn
          Caption = 'Ingress'
          DataBinding.FieldName = 'DailyIngress'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 109
        end
        object columnDailyGridRepairIngress: TcxGridDBColumn
          Caption = 'Repair Ingress'
          DataBinding.FieldName = 'DailyRepairIngress'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 113
        end
        object columnDailyGridRepairEgress: TcxGridDBColumn
          Caption = 'Repair Egress'
          DataBinding.FieldName = 'DailyRepairEgress'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 110
        end
        object columnDailyGridTotalBandwidth: TcxGridDBColumn
          Caption = 'Total Bandwidth'
          DataBinding.FieldName = 'DailyTotalBandwidth'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 111
        end
      end
      object DailyGridViewLevel1: TcxGridLevel
        GridView = DailyGridViewDBTableView
      end
    end
    object TreeList: TcxTreeList
      Left = 24
      Top = 75
      Width = 1006
      Height = 460
      Bands = <
        item
        end>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'SevenClassic'
      Navigator.Buttons.CustomButtons = <>
      OptionsSelection.CellSelect = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.GridLines = tlglBoth
      ParentFont = False
      ScrollbarAnnotations.CustomAnnotations = <>
      TabOrder = 3
      OnCustomDrawDataCell = TreeListCustomDrawDataCell
      object clmTreeTitle: TcxTreeListColumn
        Caption.Text = 'Title'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 0
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmTreeAddress: TcxTreeListColumn
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taCenter
        Caption.AlignHorz = taCenter
        Caption.Text = 'Address'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 1
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmTreePort: TcxTreeListColumn
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taCenter
        Caption.AlignHorz = taCenter
        Caption.Text = 'Port'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 2
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmTreeEgress: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Egress'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 3
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmTreeIngress: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Ingress'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 4
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmTreeRepairEgress: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Repair Egress'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 5
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmTreeRepairIngress: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Repair Ingress'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 6
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmTreeTotalBandwidth: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Total Bandwidth'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 7
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmTreeTBMonth: TcxTreeListColumn
        PropertiesClassName = 'TcxLabelProperties'
        Properties.Alignment.Horz = taCenter
        Properties.Alignment.Vert = taVCenter
        Caption.AlignHorz = taCenter
        Caption.Text = 'Avg. TB*M'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 8
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmTreeAllocatedSpace: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = ',0 GB;-,0 GB'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Allocated Space'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 9
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmTreeUsedSpace: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = ',0 GB;-,0 GB'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Used Space'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 10
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmTreeTrash: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = ',0 GB;-,0 GB'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Trash'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 11
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmTreeFreeSpace: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = ',0 GB;-,0 GB'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Free Space'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 12
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmTreePayoutDirty: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = ',0.00 $;-,0.00 $'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Payout Dirty'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 13
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmTreePayoutCleanly: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = ',0.00 $;-,0.00 $'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Payout Cleanly'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 14
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmTreeEstimatedDirtyPayout: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = ',0.00 $;-,0.00 $'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Estimated Dirty'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 15
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmTreeEstimatedCleanlyPayout: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = ',0.00 $;-,0.00 $'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Estimated Cleanly'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 16
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmTreeVersion: TcxTreeListColumn
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taCenter
        Caption.AlignHorz = taCenter
        Caption.Text = 'Version'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 17
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
    object DailyTreeList: TcxTreeList
      Left = 10000
      Top = 10000
      Width = 1006
      Height = 460
      Bands = <
        item
        end>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'SevenClassic'
      Navigator.Buttons.CustomButtons = <>
      OptionsSelection.CellSelect = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.GridLines = tlglBoth
      ParentFont = False
      ScrollbarAnnotations.CustomAnnotations = <>
      TabOrder = 5
      Visible = False
      object clmDailyTreeTitle: TcxTreeListColumn
        Caption.Text = 'Title'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 0
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmDailyTreeAddress: TcxTreeListColumn
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taCenter
        Caption.AlignHorz = taCenter
        Caption.Text = 'Address'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 1
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmDailyTreePort: TcxTreeListColumn
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taCenter
        Caption.AlignHorz = taCenter
        Caption.Text = 'Port'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 2
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmDailyTreeTBMonth: TcxTreeListColumn
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taCenter
        Caption.AlignHorz = taCenter
        Caption.Text = 'TB*M'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 3
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmDailyTreeEgress: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Egress'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 4
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmDailyTreeIngress: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Ingress'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 5
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmDailyTreeRepairEgress: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Repair Egress'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 6
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmDailyTreeRepairIngress: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Repair Ingress'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 7
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmDailyTreeTotalBandwidth: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Total Bandwidth'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 8
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
    object btnDisplay: TcxButton
      Left = 226
      Top = 10
      Width = 75
      Height = 25
      Caption = 'Display'
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'iMaginary'
      TabOrder = 1
      OnClick = btnDisplayClick
    end
    object comboDates: TcxLookupComboBox
      Left = 75
      Top = 10
      AutoSize = False
      Properties.KeyFieldNames = 'LOCAL_HISTORY_ID'
      Properties.ListColumns = <
        item
          FieldName = 'Date'
        end>
      Properties.ListSource = DM.dtsrcLocalHistoryDates
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 0
      Height = 25
      Width = 145
    end
    object lblDateSavedAt: TcxLabel
      Left = 307
      Top = 16
      Style.HotTrack = False
      Style.TransparentBorder = False
      Transparent = True
    end
    object dxLayoutControlGroup_Root: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avClient
      Hidden = True
      ShowBorder = False
      Index = -1
    end
    object laygrpTop: TdxLayoutGroup
      Parent = dxLayoutControlGroup_Root
      CaptionOptions.Text = 'New Group'
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 0
    end
    object laygrpClient: TdxLayoutGroup
      Parent = dxLayoutControlGroup_Root
      AlignVert = avClient
      CaptionOptions.Text = 'New Group'
      LayoutDirection = ldTabbed
      ShowBorder = False
      Index = 1
    end
    object layGridView: TdxLayoutItem
      Parent = laygrpClient
      AlignVert = avClient
      CaptionOptions.Text = 'Grid View'
      CaptionOptions.Visible = False
      Control = GridView
      ControlOptions.OriginalHeight = 200
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object layDailyGridView: TdxLayoutItem
      Parent = laygrpClient
      AlignVert = avClient
      CaptionOptions.Text = 'Daily Grid View'
      CaptionOptions.Visible = False
      Control = DailyGridView
      ControlOptions.OriginalHeight = 200
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 3
    end
    object layTreeList: TdxLayoutItem
      Parent = laygrpClient
      AlignVert = avClient
      CaptionOptions.Text = 'Tree View'
      CaptionOptions.Visible = False
      Control = TreeList
      ControlOptions.OriginalHeight = 150
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object layDailyTreeList: TdxLayoutItem
      Parent = laygrpClient
      AlignVert = avClient
      CaptionOptions.Text = 'Daily Tree View'
      CaptionOptions.Visible = False
      Control = DailyTreeList
      ControlOptions.OriginalHeight = 150
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object dxLayoutItem2: TdxLayoutItem
      Parent = laygrpTop
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnDisplay
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object laycomboDates: TdxLayoutItem
      Parent = laygrpTop
      AlignVert = avClient
      CaptionOptions.Text = 'Saved dates'
      Control = comboDates
      ControlOptions.OriginalHeight = 23
      ControlOptions.OriginalWidth = 145
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutItem1: TdxLayoutItem
      Parent = laygrpTop
      AlignVert = avCenter
      CaptionOptions.Text = 'cxLabel1'
      CaptionOptions.Visible = False
      Control = lblDateSavedAt
      ControlOptions.OriginalHeight = 13
      ControlOptions.OriginalWidth = 3
      ControlOptions.ShowBorder = False
      Index = 2
    end
  end
  object cxGridPopupMenu: TcxGridPopupMenu
    Grid = GridView
    PopupMenus = <>
    Left = 384
    Top = 256
  end
end
