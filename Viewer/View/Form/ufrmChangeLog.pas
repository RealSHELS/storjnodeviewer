unit ufrmChangeLog;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  Storj.Consts, uNotifyCenter, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore, dxSkinBasic,
  dxSkiniMaginary, dxSkinSeven, dxSkinSevenClassic, dxSkinWXI, dxFormattedLabel,
  dxSkinOffice2013LightGray, dxSkinOffice2019Black, dxSkinOffice2019DarkGray,
  dxSkinOffice2019White, dxSkinWhiteprint;

type
  TfrmChangeLog = class(TForm)
    mmoChangeLog: TMemo;
    lblFeedback: TdxFormattedLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    procedure CommandShow(const aData: TValue);
  public
    { Public declarations }
  end;

var
  frmChangeLog: TfrmChangeLog;

implementation

{$R *.dfm}

procedure TfrmChangeLog.CommandShow(const aData: TValue);
begin
  Self.Show;
end;

procedure TfrmChangeLog.FormCreate(Sender: TObject);
begin
  TNotifyCenter.Instance.Subscribe(COMMAND_SHOW_CHANGE_LOG, CommandShow);
end;

procedure TfrmChangeLog.FormDestroy(Sender: TObject);
begin
  TNotifyCenter.Instance.UnSubscribe(COMMAND_SHOW_CHANGE_LOG, CommandShow);
end;

end.
