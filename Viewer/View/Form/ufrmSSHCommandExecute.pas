unit ufrmSSHCommandExecute;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBasic, dxSkiniMaginary, dxSkinSeven,
  dxSkinSevenClassic, dxSkinWXI, cxClasses, dxLayoutContainer, dxLayoutControl,
  dxLayoutcxEditAdapters, dxLayoutControlAdapters, cxContainer, cxEdit,
  Vcl.Menus, cxCheckBox, cxCustomListBox, cxCheckListBox, Vcl.StdCtrls,
  cxButtons, cxTextEdit, cxLabel, cxMaskEdit, cxDropDownEdit,
  Storj.Consts, uNotifyCenter, Storj.Types, uDM, cxMemo, System.JSON,
  System.Generics.Collections, System.UITypes, dxSkinOffice2013LightGray,
  dxSkinOffice2019DarkGray, dxSkinOffice2019White;

type
  TfrmSSHCommandExecute = class(TForm)
    dxLayoutControlGroup_Root: TdxLayoutGroup;
    dxLayoutControl: TdxLayoutControl;
    comboFullPath: TcxComboBox;
    laycomboFullPath: TdxLayoutItem;
    lblSelectedHost: TcxLabel;
    laylblSelctedHost: TdxLayoutItem;
    btnExecute: TcxButton;
    laybtnExecute: TdxLayoutItem;
    chklistCommands: TcxCheckListBox;
    laychklistCommands: TdxLayoutItem;
    mmoLog: TcxMemo;
    laymmoLog: TdxLayoutItem;
    grpTopControls: TdxLayoutGroup;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure comboFullPathPropertiesChange(Sender: TObject);
    procedure btnExecuteClick(Sender: TObject);
  private
    FSSHSettings: TSSHSettings;

    procedure OnCommandShow(const aValue: TValue);
    procedure iphSExecSSHServerAuthentication(Sender: TObject;
                        HostKey: string; HostKeyB: TArray<System.Byte>; const Fingerprint, KeyAlgorithm, CertSubject, CertIssuer,
                        Status: string; var Accept: Boolean);
    procedure iphSExec1Stdout(Sender: TObject; Text: String; TextB: TArray<Byte>);
    procedure iphSExec1Stderr(Sender: TObject; Text: String; TextB: TArray<Byte>);
  public
    { Public declarations }
  end;

var
  frmSSHCommandExecute: TfrmSSHCommandExecute;

implementation

{$R *.dfm}

uses
  iphcore, iphtypes, iphsexec;

procedure TfrmSSHCommandExecute.btnExecuteClick(Sender: TObject);
var
  SelectedIndexes: String;
begin
  SelectedIndexes := '';
  for var i := 0 to chklistCommands.Items.Count - 1 do
    if chklistCommands.Items[i].Checked then
      SelectedIndexes := SelectedIndexes + i.ToString + ',';

  if not SelectedIndexes.IsEmpty then
    SelectedIndexes.Remove(SelectedIndexes.Length);

  if SelectedIndexes.IsEmpty then begin
    MessageDlg('No commands selected', TMsgDlgType.mtError, [mbOk], 0);
    Exit;
  end;

  var ipSSHExec := TiphSExec.Create(nil);
  try
    ipSSHExec.OnSSHServerAuthentication := iphSExecSSHServerAuthentication;
    ipSSHExec.OnStdout := iphSExec1Stdout;
    ipSSHExec.OnStderr := iphSExec1Stderr;

    ipSSHExec.SSHUser := FSSHSettings.UserName;
    ipSSHExec.SSHPassword := FSSHSettings.Password;
    ipSSHExec.SSHLogon(FSSHSettings.Host, FSSHSettings.Port);

    for var i := 0 to chklistCommands.Items.Count - 1 do begin
      var Item := chklistCommands.Items[i];
      if Item.Checked then begin
        mmoLog.Lines.Add('Start execute command: ' + Item.Text);
        ipSSHExec.Execute(Item.Text);
        mmoLog.Lines.Add('Execution done');
      end;
    end;

    ipSSHExec.SSHLogoff;
  finally
    ipSSHExec.Free;
  end;

  FSSHSettings.LastSelectedIndexes := SelectedIndexes;
  DM.UpsertSSHSettings(FSSHSettings);
end;

procedure TfrmSSHCommandExecute.comboFullPathPropertiesChange(Sender: TObject);
var
  SSHSettings: TSSHSettings;
begin
  if comboFullPath.ItemIndex >= 0 then begin
    var FullPath := comboFullPath.Properties.Items[comboFullPath.ItemIndex];
    FSSHSettings := DM.GetSSHSettings(FullPath);
  end else
    FSSHSettings := SSHSettings;

  chklistCommands.Clear;
  lblSelectedHost.Caption := 'Selected host: ' + FSSHSettings.Host;
  var LastSelectedIndex := ',' + FSSHSettings.LastSelectedIndexes + ',';
  var StrList := TStringList.Create;
  try
    StrList.Text := FSSHSettings.Commands;
    for var i := 0 to StrList.Count - 1 do begin
      var Item := chklistCommands.Items.Add;
      Item.Text := StrList[i];
      Item.Checked := LastSelectedIndex.Contains(',' + i.ToString + ',');
    end;
  finally
    StrList.Free;
  end;
end;

procedure TfrmSSHCommandExecute.FormCreate(Sender: TObject);
begin
  TNotifyCenter.Instance.Subscribe(COMMAND_SHOW_SSH_COMMAND_EXECUTE, OnCommandShow);
end;

procedure TfrmSSHCommandExecute.FormDestroy(Sender: TObject);
begin
  TNotifyCenter.Instance.UnSubscribe(COMMAND_SHOW_SSH_COMMAND_EXECUTE, OnCommandShow);
end;

procedure TfrmSSHCommandExecute.iphSExec1Stderr(Sender: TObject; Text: String; TextB: TArray<Byte>);
begin
  mmoLog.Lines.Add('Error: ' + StringReplace(Text, #10, #13#10, [rfReplaceAll]));
end;

procedure TfrmSSHCommandExecute.iphSExec1Stdout(Sender: TObject; Text: String; TextB: TArray<Byte>);
begin
  mmoLog.Lines.Add('Output: ' + StringReplace(Text, #10, #13#10, [rfReplaceAll]));
end;

procedure TfrmSSHCommandExecute.iphSExecSSHServerAuthentication(Sender: TObject;
  HostKey: string; HostKeyB: TArray<System.Byte>; const Fingerprint,
  KeyAlgorithm, CertSubject, CertIssuer, Status: string; var Accept: Boolean);
begin
  Accept := True;
end;

procedure TfrmSSHCommandExecute.OnCommandShow(const aValue: TValue);
  procedure AddJNodeToCombo(aJArray: TJSONArray; const aPreselectedFullPath: String);
  begin
    for var i := 0 to aJArray.Count -1 do begin
      var JObj := aJArray.Items[i] as TJSONObject;
      var FullPath := JObj.GetValue<String>('FullPath', '');

      var Index := comboFullPath.Properties.Items.Add(FullPath);
      if (aPreselectedFullPath = FullPath) and not aPreselectedFullPath.IsEmpty then
        comboFullPath.ItemIndex := Index;

      var Childs := JObj.GetValue<TJSONArray>('Childs', nil);
      if Childs <> nil then
        AddJNodeToCombo(Childs, aPreselectedFullPath);
    end;
  end;

begin
  var PreSelectedFullPath := '';
  if not aValue.IsEmpty then
    PreSelectedFullPath := aValue.AsString;

  comboFullPath.Properties.Items.Clear;
  var JNodesArray := DM.GetNodesJArrayActiveOnly;
  if Assigned(JNodesArray) then
    AddJNodeToCombo(JNodesArray, PreSelectedFullPath);

  Show;
end;

end.
