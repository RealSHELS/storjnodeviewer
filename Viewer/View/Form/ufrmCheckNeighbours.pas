unit ufrmCheckNeighbours;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, System.Net.HttpClient, Vcl.StdCtrls, System.JSON,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
  dxSkinBasic,
  dxSkiniMaginary,
  cxClasses, dxLayoutContainer, dxLayoutControl,
  dxLayoutcxEditAdapters, dxLayoutControlAdapters, cxContainer, cxEdit,
  Vcl.Menus, cxLabel, cxButtons, cxTextEdit, System.UItypes,
  System.Generics.Collections, cxImage,
  dxGDIPlusClasses, Vcl.ExtCtrls, dxSkinSeven, dxSkinSevenClassic,
  Storj.Consts, uNotifyCenter, dxSkinWXI;

type
  TfrmCheckNeighbours = class(TForm)
    dxLayoutControlGroup_Root: TdxLayoutGroup;
    dxLayoutControl: TdxLayoutControl;
    edtURL: TcxTextEdit;
    layedtURL: TdxLayoutItem;
    btnAction: TcxButton;
    laybtnAction: TdxLayoutItem;
    lblInfo: TcxLabel;
    laylblInfo: TdxLayoutItem;
    dxLayoutGroup: TdxLayoutGroup;
    lblPoweredBy: TcxLabel;
    laylblPoweredBy: TdxLayoutItem;
    linkStorjNetInfo: TLinkLabel;
    laylinkStorjNetInfo: TdxLayoutItem;
    laygrpPoweredBy: TdxLayoutGroup;
    linkCloudflare: TLinkLabel;
    laylinkCloudflare: TdxLayoutItem;
    procedure btnActionClick(Sender: TObject);
    procedure linkStorjNetInfoLinkClick(Sender: TObject; const Link: string; LinkType: TSysLinkType);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    procedure CommandShow(const aData: TValue);
  public
    procedure ShowForEmpty;
    procedure ShowForText(aText: String);
  end;

var
  frmCheckNeighbours: TfrmCheckNeighbours;

implementation

{$R *.dfm}

uses
  Storj.Common, Storj.NeighboursGetter;

{ TfrmCheckNeighbours }

procedure TfrmCheckNeighbours.btnActionClick(Sender: TObject);
begin
  var Text := edtURL.Text;

  try
    var Count := GetNeighboursSync(Text);

    lblInfo.Caption := 'Subnet: ' + Text + #13 +
                       'Total nodes in subnet: ' + Count.ToString;
  except
    on E: Exception do
      MessageDlg(E.Message, mtError, [mbOk], 0);
  end;
end;

procedure TfrmCheckNeighbours.CommandShow(const aData: TValue);
begin
  lblInfo.Caption := '';
  edtURL.Text := aData.AsString;
  Show;
  if not aData.AsString.IsEmpty then
    btnActionClick(nil);
end;

procedure TfrmCheckNeighbours.FormCreate(Sender: TObject);
begin
  TNotifyCenter.Instance.Subscribe(COMMAND_SHOW_CHECK_NEIGHBOURS, CommandShow);
end;

procedure TfrmCheckNeighbours.FormDestroy(Sender: TObject);
begin
  TNotifyCenter.Instance.UnSubscribe(COMMAND_SHOW_CHECK_NEIGHBOURS, CommandShow);
end;

procedure TfrmCheckNeighbours.linkStorjNetInfoLinkClick(Sender: TObject;
  const Link: string; LinkType: TSysLinkType);
begin
  OpenURL(Link);
end;

procedure TfrmCheckNeighbours.ShowForEmpty;
begin
  lblInfo.Caption := '';
  edtURL.Text := '';
  Show;
end;

procedure TfrmCheckNeighbours.ShowForText(aText: String);
begin
  lblInfo.Caption := '';
  edtURL.Text := aText;
  Show;
  btnActionClick(nil);
end;

end.
