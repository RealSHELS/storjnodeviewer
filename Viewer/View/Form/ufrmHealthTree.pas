unit ufrmHealthTree;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Storj.Consts, uNotifyCenter, System.JSON, Storj.Types, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxSkinBasic,
  dxSkiniMaginary, dxSkinSeven, dxSkinSevenClassic, cxClasses,
  dxLayoutContainer, dxLayoutControl, Vcl.ComCtrls, cxContainer, cxEdit,
  cxTreeView, cxCustomData, cxStyles, dxScrollbarAnnotations, cxTL, cxTextEdit,
  cxCurrencyEdit, cxTLdxBarBuiltInMenu, cxInplaceContainer,
  System.Generics.Collections, Storj.Settings, System.StrUtils, cxCheckBox,
  dxSkinWXI, cxFilter;

type
  TfrmHealthTree = class(TForm)
    dxLayoutControlGroup_Root: TdxLayoutGroup;
    dxLayoutControl: TdxLayoutControl;
    dxLayoutItem1: TdxLayoutItem;
    treeHealthData: TcxTreeList;
    clmTitle: TcxTreeListColumn;
    clmAddress: TcxTreeListColumn;
    clmPort: TcxTreeListColumn;
    clmSatellite: TcxTreeListColumn;
    clmAudit: TcxTreeListColumn;
    clmOnline: TcxTreeListColumn;
    clmSuspension: TcxTreeListColumn;
    clmIsSuspended: TcxTreeListColumn;
    clmIsDisqualified: TcxTreeListColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure treeHealthDataCustomDrawDataCell(Sender: TcxCustomTreeList;
      ACanvas: TcxCanvas; AViewInfo: TcxTreeListEditCellViewInfo;
      var ADone: Boolean);
    procedure clmIsSuspendedGetDisplayText(Sender: TcxTreeListColumn;
      ANode: TcxTreeListNode; var Value: string);
  private
    procedure CommandShow(const aData: TValue);
    procedure CommandShowNodeData(const aData: TValue);
    procedure CommandShowNodeDataGroup(const aData: TValue);
    procedure CommandLoadFromJSON(const aData: TValue);

    procedure SetJArrTreeNodes(AJArr: TJSONArray; AParentNode: TcxTreeListNode);
    function GetValueForColoring(const aColorType, aColumnName: String): Integer;
  public
    { Public declarations }
  end;

var
  frmHealthTree: TfrmHealthTree;

implementation

{$R *.dfm}

uses
  uDM;

procedure TfrmHealthTree.CommandShow(const aData: TValue);
begin
  Self.Show;
end;

procedure TfrmHealthTree.FormCreate(Sender: TObject);
begin
  TNotifyCenter.Instance.Subscribe(COMMAND_SHOW_HEALTH_TREE, CommandShow);
  TNotifyCenter.Instance.Subscribe(COMMAND_SHOW_NODE_DATA, CommandShowNodeData);
  TNotifyCenter.Instance.Subscribe(COMMAND_SHOW_NODE_DATA_GROUP, CommandShowNodeDataGroup);
  TNotifyCenter.Instance.Subscribe(COMMAND_LOAD_FROM_JSON, CommandLoadFromJSON);
end;

procedure TfrmHealthTree.FormDestroy(Sender: TObject);
begin
  TNotifyCenter.Instance.UnSubscribe(COMMAND_SHOW_HEALTH_TREE, CommandShow);
  TNotifyCenter.Instance.UnSubscribe(COMMAND_SHOW_NODE_DATA, CommandShowNodeData);
  TNotifyCenter.Instance.UnSubscribe(COMMAND_SHOW_NODE_DATA_GROUP, CommandShowNodeDataGroup);
  TNotifyCenter.Instance.UnSubscribe(COMMAND_LOAD_FROM_JSON, CommandLoadFromJSON);
end;

function TfrmHealthTree.GetValueForColoring(const aColorType, aColumnName: String): Integer;
begin
  if aColumnName = 'clmAudit' then begin
    if aColorType = 'Red' then
      Result := GetSettings.HealthAuditRed
    else
      Result := GetSettings.HealthAuditYellow;
  end else if aColumnName = 'clmOnline' then begin
     if aColorType = 'Red' then
      Result := GetSettings.HealthOnlineRed
    else
      Result := GetSettings.HealthOnlineYellow;
  end else begin
    if aColorType = 'Red' then
      Result := GetSettings.HealthSuspensionRed
    else
      Result := GetSettings.HealthSuspensionYellow;
  end;
end;

procedure TfrmHealthTree.clmIsSuspendedGetDisplayText(Sender: TcxTreeListColumn;
  ANode: TcxTreeListNode; var Value: string);
begin
  Value := '';
end;

procedure TfrmHealthTree.CommandLoadFromJSON(const aData: TValue);
begin
  var JArr := aData.AsObject as TJSONArray;
  if not Assigned(JArr) then
    Exit;

  treeHealthData.BeginUpdate;
  try
    treeHealthData.Clear;
    SetJArrTreeNodes(JArr, treeHealthData.Root);
  finally
    treeHealthData.EndUpdate;
  end;
end;

procedure TfrmHealthTree.SetJArrTreeNodes(AJArr: TJSONArray; AParentNode: TcxTreeListNode);
var
  i: integer;
  JObj: TJSONObject;
  JValue: TJSONValue;
  Node: TcxTreeListNode;
begin
  for i := 0 to AJarr.Count - 1 do begin
    JObj := AJArr.Items[i] as TJSONObject;
    if not JObj.GetValue<Boolean>('IsActive') then
      Continue;

    Node := AParentNode.AddChild;
    Node.Values[clmTitle.Position.ColIndex] := JObj.GetValue<String>('Title');
    Node.Values[clmAddress.Position.ColIndex] := JObj.GetValue<String>('Address', '');
    Node.Values[clmPort.Position.ColIndex] := JObj.GetValue<String>('Port', '');

    JValue := JObj.GetValue<TJSONArray>('Childs', nil);
    if JValue <> nil then
      SetJArrTreeNodes(JValue as TJSONArray, Node);

    if JObj.GetValue<Boolean>('AutoExpand', False) then
      Node.Expanded := True;
  end;
end;

procedure TfrmHealthTree.CommandShowNodeData(const aData: TValue);
  function GetNode(AParentNode: TcxTreeListNode; ANode: TNode): TcxTreeListNode;
    var
      i: integer;
    begin
      for i := 0 to AParentNode.Count - 1 do begin
        Result := AParentNode.Items[i];
        if (Result.Values[clmAddress.Position.ColIndex] = ANode.Address) and
           (Result.Values[clmPort.Position.ColIndex] = ANode.Port) then
          Exit(Result);

        if Result.HasChildren then
          Result := GetNode(Result, ANode)
        else
          Result := nil;
        if Result <> nil then
          Exit(Result);
      end;

      Result := nil;
    end;

var
  Node, SatNode: TcxTreeListNode;
  Satellite: TSatellite;
  NodeData: TNodeData;
begin
  NodeData := aData.AsType<TNodeData>;

  treeHealthData.BeginUpdate;
  try
    Node := GetNode(treeHealthData.Root, NodeData.Node);
    if Node = nil then
      Node := treeHealthData.Root.AddChild;

    Node.Values[clmTitle.Position.ColIndex] := NodeData.Node.Title;
    Node.Values[clmAddress.Position.ColIndex] := NodeData.Node.Address;
    Node.Values[clmPort.Position.ColIndex] := NodeData.Node.Port;

    Node.DeleteChildren;
    for Satellite in NodeData.Satellites do begin
      SatNode := Node.AddChild;
      SatNode.Values[clmSatellite.Position.ColIndex] := Satellite.Title;
      SatNode.Values[clmAudit.Position.ColIndex] := Satellite.Health.AuditPerc;
      SatNode.Values[clmOnline.Position.ColIndex] := Satellite.Health.OnlinePerc;
      SatNode.Values[clmSuspension.Position.ColIndex] := Satellite.Health.SuspensionPerc;
      SatNode.Values[clmIsSuspended.Position.ColIndex] := Satellite.Health.IsSuspended;
      SatNode.Values[clmIsDisqualified.Position.ColIndex] := Satellite.Health.IsDisqualified;
    end;

    //RecalcParent(Node.Parent);

    treeHealthData.FullExpand;
  finally
    treeHealthData.EndUpdate;
  end;
end;

procedure TfrmHealthTree.CommandShowNodeDataGroup(const aData: TValue);
begin
  var NodeDataArray := aData.AsType<TNodeDataArray>;

  treeHealthData.BeginUpdate;
  try
    for var NodeData in NodeDataArray do
      CommandShowNodeData(TValue.From<TNodeData>(NodeData));
  finally
    treeHealthData.EndUpdate;
  end;
end;

procedure TfrmHealthTree.treeHealthDataCustomDrawDataCell(
  Sender: TcxCustomTreeList; ACanvas: TcxCanvas;
  AViewInfo: TcxTreeListEditCellViewInfo; var ADone: Boolean);
begin
  if MatchText(AViewInfo.Column.Name, ['clmAudit', 'clmOnline', 'clmSuspension', 'clmIsSuspended', 'clmIsDisqualified']) then begin
    var Value := AViewInfo.DisplayValue;
    if Value = null then
      Exit;

    var strValue := VarToStr(Value);

    if MatchText(AViewInfo.Column.Name, ['clmAudit', 'clmOnline', 'clmSuspension']) then
    begin
      strValue := strValue + ' %';

      ACanvas.Brush.Color := $d4f9d4;
      if Value < GetValueForColoring('Red', AViewInfo.Column.Name) then
        ACanvas.Brush.Color := RGB(255, 236, 230)
      else if Value < GetValueForColoring('Yellow', AViewInfo.Column.Name) then
        ACanvas.Brush.Color := RGB(255, 246, 189);
    end else begin
      strValue := '';
       ACanvas.Brush.Color := $d4f9d4;

      if (AViewInfo.Column.Name = 'clmIsSuspended') and Boolean(Value) then
         ACanvas.Brush.Color := RGB(255, 246, 189)
      else if (AViewInfo.Column.Name = 'clmIsDisqualified') and Boolean(Value) then
         ACanvas.Brush.Color := RGB(255, 236, 230);
    end;
  end;
end;

end.
