object frmGlobalLog: TfrmGlobalLog
  Left = 0
  Top = 0
  Caption = 'Global Log'
  ClientHeight = 545
  ClientWidth = 1099
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxLayoutControl1: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 1099
    Height = 545
    Align = alClient
    TabOrder = 0
    object mmoGlobalLog: TcxMemo
      Left = 10
      Top = 10
      Properties.ReadOnly = True
      Properties.ScrollBars = ssVertical
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 0
      Height = 494
      Width = 1079
    end
    object dxStatusBar: TdxStatusBar
      Left = 46
      Top = 510
      Width = 1043
      Height = 25
      Panels = <
        item
          PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
          Text = 'Log file size: 0 B'
        end>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
    end
    object btnDeleteLogFile: TcxButton
      Left = 10
      Top = 510
      Width = 30
      Height = 25
      Hint = 'Delete log file and clear log'
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'iMaginary'
      OptionsImage.Glyph.SourceDPI = 96
      OptionsImage.Glyph.SourceHeight = 20
      OptionsImage.Glyph.SourceWidth = 20
      OptionsImage.Glyph.Data = {
        3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D225554
        462D38223F3E0D0A3C7376672076657273696F6E3D22312E31222069643D224C
        617965725F312220786D6C6E733D22687474703A2F2F7777772E77332E6F7267
        2F323030302F7376672220786D6C6E733A786C696E6B3D22687474703A2F2F77
        77772E77332E6F72672F313939392F786C696E6B2220783D223070782220793D
        22307078222076696577426F783D223020302033322033322220656E61626C65
        2D6261636B67726F756E643D226E6577203020302033322033322220786D6C3A
        73706163653D227072657365727665223E262331333B262331303B3C70617468
        20643D224D31362C31392E31222F3E0D0A3C7061746820643D224D31322E392C
        3136222F3E0D0A3C7061746820643D224D31362C31322E39222F3E0D0A3C7061
        746820643D224D31392E312C3136222F3E0D0A3C673E0D0A09093C7061746820
        643D224D372C323863302C312E312C302E392C322C322C3268313463312E312C
        302C322D302E392C322D32563848375632387A204D32302C313263302D302E36
        2C302E342D312C312D3173312C302E342C312C3176313463302C302E362D302E
        342C312D312C31732D312D302E342D312D315631327A20202623393B2623393B
        204D31352C313263302D302E362C302E342D312C312D3173312C302E342C312C
        3176313463302C302E362D302E342C312D312C31732D312D302E342D312D3156
        31327A204D31302C313263302D302E352C302E352D312C312D3173312C302E35
        2C312C3176313463302C302E352D302E352C312D312C3120202623393B262339
        3B732D312D302E352D312D315631327A222F3E0D0A09093C7061746820643D22
        4D32362C35682D37563463302D312E312D302E392D322D322D32682D32632D31
        2E312C302D322C302E392D322C327631483643352E342C352C352C352E342C35
        2C36763168323256364332372C352E342C32362E362C352C32362C357A204D31
        382C35682D34563420202623393B2623393B63302D302E362C302E342D312C31
        2D31683263302E362C302C312C302E342C312C3156357A222F3E0D0A093C2F67
        3E0D0A3C2F7376673E0D0A}
      PaintStyle = bpsGlyph
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnDeleteLogFileClick
    end
    object dxLayoutControl1Group_Root: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avClient
      ButtonOptions.Buttons = <>
      Hidden = True
      ItemIndex = 1
      ShowBorder = False
      Index = -1
    end
    object layMemo: TdxLayoutItem
      Parent = dxLayoutControl1Group_Root
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'cxMemo1'
      CaptionOptions.Visible = False
      Control = mmoGlobalLog
      ControlOptions.OriginalHeight = 89
      ControlOptions.OriginalWidth = 185
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object layStatusBar: TdxLayoutItem
      Parent = laygrpBottom
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'dxStatusBar1'
      CaptionOptions.Visible = False
      Control = dxStatusBar
      ControlOptions.OriginalHeight = 20
      ControlOptions.OriginalWidth = 42
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object layButtonDeleteLogFile: TdxLayoutItem
      Parent = laygrpBottom
      AlignHorz = ahLeft
      AlignVert = avClient
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnDeleteLogFile
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 30
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object laygrpBottom: TdxLayoutGroup
      Parent = dxLayoutControl1Group_Root
      CaptionOptions.Text = 'New Group'
      CaptionOptions.Visible = False
      ButtonOptions.Buttons = <>
      ItemIndex = 1
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 1
    end
  end
  object Timer: TTimer
    Interval = 3600000
    OnTimer = TimerTimer
    Left = 536
    Top = 168
  end
end
