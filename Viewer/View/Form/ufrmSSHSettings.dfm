object frmSSHSettings: TfrmSSHSettings
  Left = 0
  Top = 0
  Caption = 'SSH Settings'
  ClientHeight = 461
  ClientWidth = 898
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  TextHeight = 15
  object dxLayoutControl: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 898
    Height = 461
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 894
    ExplicitHeight = 460
    object comboFullPath: TcxComboBox
      Left = 75
      Top = 33
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownRows = 20
      Properties.OnChange = comboFullPathPropertiesChange
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 0
      Width = 540
    end
    object edtHost: TcxTextEdit
      Left = 709
      Top = 33
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 2
      Width = 163
    end
    object edtUsername: TcxTextEdit
      Left = 709
      Top = 93
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 4
      Text = 'root'
      Width = 163
    end
    object edtPassword: TcxTextEdit
      Left = 709
      Top = 123
      Properties.EchoMode = eemPassword
      Properties.ShowPasswordRevealButton = True
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 5
      Width = 163
    end
    object mmoCommands: TcxMemo
      Left = 26
      Top = 84
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 1
      Height = 351
      Width = 589
    end
    object btnSave: TcxButton
      Left = 797
      Top = 410
      Width = 75
      Height = 25
      Caption = 'Save'
      TabOrder = 6
      OnClick = btnSaveClick
    end
    object edtPort: TcxCurrencyEdit
      Left = 709
      Top = 63
      EditValue = 22.000000000000000000
      Properties.DecimalPlaces = 0
      Properties.DisplayFormat = '0'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 3
      Width = 163
    end
    object dxLayoutControlGroup_Root: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avClient
      Hidden = True
      ShowBorder = False
      Index = -1
    end
    object laycomboFullPath: TdxLayoutItem
      Parent = laygrpLeftSide
      CaptionOptions.Text = 'FullPath'
      Control = comboFullPath
      ControlOptions.OriginalHeight = 23
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object layedtHost: TdxLayoutItem
      Parent = laygrpRightSide
      CaptionOptions.Text = 'Host/IP'
      Control = edtHost
      ControlOptions.OriginalHeight = 23
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object layedtUsername: TdxLayoutItem
      Parent = laygrpRightSide
      AlignHorz = ahClient
      AlignVert = avTop
      CaptionOptions.Text = 'Username'
      Control = edtUsername
      ControlOptions.OriginalHeight = 23
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object layedtPassword: TdxLayoutItem
      Parent = laygrpRightSide
      CaptionOptions.Text = 'Password'
      Control = edtPassword
      ControlOptions.OriginalHeight = 23
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 3
    end
    object laymmoCommands: TdxLayoutItem
      Parent = laygrpLeftSide
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'Commands'
      CaptionOptions.Layout = clTop
      Control = mmoCommands
      ControlOptions.OriginalHeight = 89
      ControlOptions.OriginalWidth = 185
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object laybtnSave: TdxLayoutItem
      Parent = laygrpRightSide
      AlignHorz = ahRight
      AlignVert = avBottom
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnSave
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 4
    end
    object laygrpRightSide: TdxLayoutGroup
      Parent = laygrpContent
      AlignHorz = ahRight
      SizeOptions.Width = 250
      ItemIndex = 1
      Index = 1
    end
    object laygrpLeftSide: TdxLayoutGroup
      Parent = laygrpContent
      AlignHorz = ahClient
      Index = 0
    end
    object laygrpContent: TdxLayoutGroup
      Parent = dxLayoutControlGroup_Root
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'New Group'
      ItemIndex = 1
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 0
    end
    object layedtPort: TdxLayoutItem
      Parent = laygrpRightSide
      CaptionOptions.Text = 'Port'
      Control = edtPort
      ControlOptions.OriginalHeight = 23
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
  end
end
