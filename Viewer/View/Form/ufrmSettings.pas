unit ufrmSettings;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBasic, dxSkiniMaginary, dxSkinSeven,
  dxSkinSevenClassic, cxClasses, dxLayoutContainer, dxLayoutControl,
  dxLayoutcxEditAdapters, dxLayoutControlAdapters, cxContainer, cxEdit,
  Vcl.Menus, Vcl.StdCtrls, cxButtons, cxCheckBox, cxTextEdit, cxCurrencyEdit,
  Storj.Settings, System.UItypes, Storj.Consts, uNotifyCenter,
  uframePCRebooterSettings, Vcl.ExtCtrls, cxScrollBox, cxGroupBox, cxRadioGroup,
  dxSkinWXI, dxSkinOffice2013LightGray, dxSkinOffice2019Black,
  dxSkinOffice2019DarkGray, dxSkinOffice2019White, dxSkinWhiteprint, cxMemo,
  dxScreenTip, dxCustomHint, cxHint, dxGDIPlusClasses, cxImage;

type
  TfrmSetting = class(TForm)
    dxLayoutControlGroup_Root: TdxLayoutGroup;
    dxLayoutControl: TdxLayoutControl;
    laygrpColoredFreeSpaceValues: TdxLayoutGroup;
    chkColoredFreeSpace: TcxCheckBox;
    laychkColoredFreeSpace: TdxLayoutItem;
    btnSave: TcxButton;
    laybtnSave: TdxLayoutItem;
    btnCancel: TcxButton;
    laybtnCancel: TdxLayoutItem;
    laycurRedValue: TdxLayoutItem;
    curYellowValue: TcxCurrencyEdit;
    laycurYellowValue: TdxLayoutItem;
    laygrpBottomControls: TdxLayoutGroup;
    curRedValue: TcxCurrencyEdit;
    curTimeout: TcxCurrencyEdit;
    laycurTimeout: TdxLayoutItem;
    edtHealthSuspensionRed: TcxCurrencyEdit;
    layedtHealthSuspensionRed: TdxLayoutItem;
    edtHealthSuspensionYellow: TcxCurrencyEdit;
    layedtHealthSuspensionYellow: TdxLayoutItem;
    edtHealthOnlineYellow: TcxCurrencyEdit;
    layedtHealthOnlineYellow: TdxLayoutItem;
    edtHealthOnlineRed: TcxCurrencyEdit;
    layedtHealthOnlineRed: TdxLayoutItem;
    edtHealthAuditYellow: TcxCurrencyEdit;
    layedtHealthAuditYellow: TdxLayoutItem;
    edtHealthAuditRed: TcxCurrencyEdit;
    layedtHealthAuditRed: TdxLayoutItem;
    laygrpHealthTab: TdxLayoutGroup;
    laygrpHealthAudit: TdxLayoutGroup;
    laygrpHealthOnline: TdxLayoutGroup;
    laygrpHealthSuspension: TdxLayoutGroup;
    chkHealthHotData: TcxCheckBox;
    layDisplayHealthHotData: TdxLayoutItem;
    laygrpSuspensionPercent: TdxLayoutGroup;
    chkSuspensionRedLog: TcxCheckBox;
    laychkSuspensionRedLog: TdxLayoutItem;
    chkSuspensionYellowLog: TcxCheckBox;
    laychkSuspensionYellowLog: TdxLayoutItem;
    laygrpSuspensionLogChecks: TdxLayoutGroup;
    chkHealthOnlineRedLog: TcxCheckBox;
    laychkHealthOnlineRedLog: TdxLayoutItem;
    chkHealthOnlineYellowLog: TcxCheckBox;
    laychkHealthOnlineYellowLog: TdxLayoutItem;
    chkHealthAuditRedLog: TcxCheckBox;
    laychkHealthAuditRedLog: TdxLayoutItem;
    chkHealthAuditYellowLog: TcxCheckBox;
    laychkHealthAuditYellowLog: TdxLayoutItem;
    laygrpHealthOnlinePercent: TdxLayoutGroup;
    laygrpHealthOnlineLogCheck: TdxLayoutGroup;
    laygrpHealthAuditPercent: TdxLayoutGroup;
    laygrpHealthAuditLogCheck: TdxLayoutGroup;
    chkIsDisplayStorjNetUsed: TcxCheckBox;
    laychkIsDisplayStorjNetUsed: TdxLayoutItem;
    laygrpMain: TdxLayoutGroup;
    laygrpCommonTab: TdxLayoutGroup;
    numOnlineRangeMinutes: TcxCurrencyEdit;
    laynumOnlineRangeMinutes: TdxLayoutItem;
    laygrpPCRebooterSettings: TdxLayoutGroup;
    chkIsEnabledPCRebooter: TcxCheckBox;
    laychkIsEnabledPCRebooter: TdxLayoutItem;
    btnAddPCRebooter: TcxButton;
    laybtnAddPCRebooter: TdxLayoutItem;
    btnRemovePCRebooter: TcxButton;
    laybtnRemovePCRebooter: TdxLayoutItem;
    laygrpPCRebooterFuncBtns: TdxLayoutGroup;
    laygrpPCRebooterItems: TdxLayoutGroup;
    scrollPCRebooters: TScrollBox;
    layscrollPCRebooters: TdxLayoutItem;
    pnlPCRebooters: TFlowPanel;
    chkColoredTBM: TcxCheckBox;
    laychkColoredTBM: TdxLayoutItem;
    edtLowerDeviationTreshold: TcxCurrencyEdit;
    layedtLowerDeviationTreshold: TdxLayoutItem;
    edtHigherDeviationTreshold: TcxCurrencyEdit;
    layedtHigherDeviationTreshold: TdxLayoutItem;
    laygrpColoredTBMSettings: TdxLayoutGroup;
    chkMainFormFullScreen: TcxCheckBox;
    laychkMainFormFullScreen: TdxLayoutItem;
    chkReloadNodesTimer: TcxCheckBox;
    laychkReloadNodesTimer: TdxLayoutItem;
    edtReloadNodesTimerMinutes: TcxCurrencyEdit;
    layedtReloadNodesTimerMinutes: TdxLayoutItem;
    chkUseTreePathForLog: TcxCheckBox;
    dxLayoutItem1: TdxLayoutItem;
    chkUseProxyForGetNeightbours: TcxCheckBox;
    dxLayoutItem2: TdxLayoutItem;
    laygrpNotifyTab: TdxLayoutGroup;
    chkNotifyTelegramEnable: TcxCheckBox;
    laychkNotifyTelegramEnable: TdxLayoutItem;
    chkNotifyEmailEnable: TcxCheckBox;
    laychkNotifyEmailEnable: TdxLayoutItem;
    edtNotifyEmail: TcxTextEdit;
    layedtNotifyEmail: TdxLayoutItem;
    laygrpNotifyTelegram: TdxLayoutGroup;
    laygrpNotifyEmail: TdxLayoutGroup;
    chkUseNodeRefreshBuffer: TcxCheckBox;
    laychkUseNodeRefreshBuffer: TdxLayoutItem;
    edtNotifyTelegramUserID: TcxTextEdit;
    layedtNotifyTelegramUserID: TdxLayoutItem;
    cxHintStyleController: TcxHintStyleController;
    imgTelegramNotifyHelp: TImage;
    layimgTelegramNotifyHelp: TdxLayoutItem;
    grpNotifyTelegramSettings: TdxLayoutGroup;
    grpNotifyEmailSettings: TdxLayoutGroup;
    imgEmailNotifyHelp: TImage;
    layimgEmailNotifyHelp: TdxLayoutItem;
    procedure chkColoredFreeSpacePropertiesEditValueChanged(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnAddPCRebooterClick(Sender: TObject);
    procedure btnRemovePCRebooterClick(Sender: TObject);
    procedure scrollPCRebootersMouseWheelDown(Sender: TObject;
      Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure scrollPCRebootersMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure chkIsEnabledPCRebooterPropertiesEditValueChanged(Sender: TObject);
    procedure chkColoredTBMPropertiesEditValueChanged(Sender: TObject);
    procedure chkReloadNodesTimerPropertiesEditValueChanged(Sender: TObject);
  private
    function IsValid: Boolean;

    procedure SettingsToUI(const aSettings: TSettings);
    function UIToSettings: TSettings;

    function CreateNewFramePCRebooter: TframePCRebooterSettings;

    procedure CommandShow(const aData: TValue);

    procedure CommandBeforeStartGetNodeData(const aData: TValue);
    procedure CommandAfterGetNodeData(const aData: TValue);
  public

  end;

var
  frmSetting: TfrmSetting;

implementation

{$R *.dfm}

uses
  uDM;

procedure TfrmSetting.btnAddPCRebooterClick(Sender: TObject);
begin
  CreateNewFramePCRebooter;
end;

procedure TfrmSetting.btnCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmSetting.btnRemovePCRebooterClick(Sender: TObject);
var
  index: integer;
begin
  index := 0;

  while index < pnlPCRebooters.ControlCount do begin
    if not TframePCRebooterSettings(pnlPCRebooters.Controls[index]).IsActive then
      TframePCRebooterSettings(pnlPCRebooters.Controls[index]).Free
    else
      index := index + 1;
  end;
end;

procedure TfrmSetting.btnSaveClick(Sender: TObject);
begin
  if not IsValid then
    Exit;

  SaveSettings(UIToSettings);
  Close;
  TNotifyCenter.Instance.SendMessage(COMMAND_SETTING_CHANGED, nil);
end;

procedure TfrmSetting.chkColoredFreeSpacePropertiesEditValueChanged(Sender: TObject);
begin
  laygrpColoredFreeSpaceValues.Enabled := chkColoredFreeSpace.Checked;
end;

procedure TfrmSetting.chkColoredTBMPropertiesEditValueChanged(Sender: TObject);
begin
  laygrpColoredTBMSettings.Enabled := chkColoredTBM.Checked;
end;

procedure TfrmSetting.chkIsEnabledPCRebooterPropertiesEditValueChanged(Sender: TObject);
begin
  btnAddPCRebooter.Enabled := chkIsEnabledPCRebooter.Checked;
  btnRemovePCRebooter.Enabled := chkIsEnabledPCRebooter.Checked;
  scrollPCRebooters.Enabled := chkIsEnabledPCRebooter.Checked;
end;

procedure TfrmSetting.chkReloadNodesTimerPropertiesEditValueChanged(Sender: TObject);
begin
  layedtReloadNodesTimerMinutes.Enabled := chkReloadNodesTimer.Checked;
end;

procedure TfrmSetting.CommandAfterGetNodeData(const aData: TValue);
begin
  laybtnSave.Enabled := True;
end;

procedure TfrmSetting.CommandBeforeStartGetNodeData(const aData: TValue);
begin
  laybtnSave.Enabled := False;
end;

procedure TfrmSetting.CommandShow(const aData: TValue);
begin
  Self.Show;
end;

function TfrmSetting.CreateNewFramePCRebooter: TframePCRebooterSettings;
begin
  Result := TframePCRebooterSettings.Create(Self);
  Result.Parent := pnlPCRebooters;
  Result.Name := 'f' + DM.GenGUID;
end;

procedure TfrmSetting.FormCreate(Sender: TObject);
begin
  TNotifyCenter.Instance.Subscribe(COMMAND_SHOW_SETTINGS, CommandShow);
  TNotifyCenter.Instance.Subscribe(COMMAND_BEFORE_START_GET_NODE, CommandBeforeStartGetNodeData);
  TNotifyCenter.Instance.Subscribe(COMMAND_AFTER_GET_NODE_DATA, CommandAfterGetNodeData);
end;

procedure TfrmSetting.FormDestroy(Sender: TObject);
begin
  TNotifyCenter.Instance.UnSubscribe(COMMAND_SHOW_SETTINGS, CommandShow);
  TNotifyCenter.Instance.UnSubscribe(COMMAND_BEFORE_START_GET_NODE, CommandBeforeStartGetNodeData);
  TNotifyCenter.Instance.UnSubscribe(COMMAND_AFTER_GET_NODE_DATA, CommandAfterGetNodeData);
end;

procedure TfrmSetting.FormShow(Sender: TObject);
begin
  var Settings := GetSettings;
  SettingsToUI(Settings);
end;

function TfrmSetting.IsValid: Boolean;
begin
  Result := True;

//  if curRedValue.Value >= curYellowValue.Value then begin
//    MessageDlg('', mtError, [mbOk], 0);
//    Exit(False);
//  end;

  var RefreshInterval := Round(edtReloadNodesTimerMinutes.Value);
  if (RefreshInterval < 5) or (RefreshInterval > 1440) then begin //for timer
    MessageDlg('The refresh timer interval can only be set within the range of 5 minutes to 1440 minutes (1 day)', mtError, [mbOk], 0);
    Exit(False);
  end;
end;

procedure TfrmSetting.scrollPCRebootersMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  TScrollBox(Sender).VertScrollBar.Position := TScrollBox(Sender).VertScrollBar.Position + 3;
end;

procedure TfrmSetting.scrollPCRebootersMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  TScrollBox(Sender).VertScrollBar.Position := TScrollBox(Sender).VertScrollBar.Position - 3;
end;

procedure TfrmSetting.SettingsToUI(const aSettings: TSettings);
var
  PCRebooter: TPCRebooterSettings;
  i: integer;
begin
  chkColoredFreeSpace.Checked := aSettings.IsColoredFreeSpace;
  curRedValue.Value := aSettings.RedColorValue;
  curYellowValue.Value := aSettings.YellowColorValue;
  curTimeout.Value := aSettings.Timeout;
  chkMainFormFullScreen.Checked := aSettings.IsMainFormFullScreen;

  chkColoredTBM.Checked := aSettings.IsColoredTBMDeviation;
  edtLowerDeviationTreshold.Value := aSettings.LessThanDeviationPercent;
  edtHigherDeviationTreshold.Value := aSettings.MoreThanDeviationPercent;

  edtHealthAuditRed.Value := aSettings.HealthAuditRed;
  edtHealthAuditYellow.Value := aSettings.HealthAuditYellow;
  edtHealthOnlineRed.Value := aSettings.HealthOnlineRed;
  edtHealthOnlineYellow.Value := aSettings.HealthOnlineYellow;
  edtHealthSuspensionRed.Value := aSettings.HealthSuspensionRed;
  edtHealthSuspensionYellow.Value := aSettings.HealthSuspensionYellow;

  chkHealthAuditRedLog.Checked := aSettings.IsLogHealthAuditRed;
  chkHealthAuditYellowLog.Checked := aSettings.IsLogHealthAuditYellow;
  chkHealthOnlineRedLog.Checked := aSettings.IsLogHealthOnlineRed;
  chkHealthOnlineYellowLog.Checked := aSettings.IsLogHealthOnlineYellow;
  chkSuspensionYellowLog.Checked := aSettings.IsLogHealthSuspensionYellow;
  chkSuspensionRedLog.Checked := aSettings.IsLogHealthSuspensionRed;

  chkHealthHotData.Checked := aSettings.IsDisplayHealthHotData;
  chkIsDisplayStorjNetUsed.Checked := aSettings.IsDisplayStorjNetUsed;

  chkColoredFreeSpacePropertiesEditValueChanged(chkColoredFreeSpace);
  chkColoredTBMPropertiesEditValueChanged(chkColoredTBM);

  numOnlineRangeMinutes.Value := aSettings.OnlineRangeMinutes;

  chkReloadNodesTimer.Checked := aSettings.IsEnableReloadNodesTimer;
  edtReloadNodesTimerMinutes.Value := aSettings.ReloadNodesTimerMinutes;
  layedtReloadNodesTimerMinutes.Enabled := chkReloadNodesTimer.Checked;

  chkUseTreePathForLog.Checked := aSettings.IsUseTreePathForLog;
  chkUseProxyForGetNeightbours.Checked := aSettings.IsUseProxyForGetNeighbours;
  chkUseNodeRefreshBuffer.Checked := aSettings.IsUseNodeRefreshBuffer;

  chkNotifyTelegramEnable.Checked := aSettings.NotifySettings.TelegramSettings.IsEnable;
  edtNotifyTelegramUserID.Text := aSettings.NotifySettings.TelegramSettings.UserID;

  chkNotifyEmailEnable.Checked := aSettings.NotifySettings.EmailSettings.IsEnable;
  edtNotifyEmail.Text := aSettings.NotifySettings.EmailSettings.Email;

  //PCRebooter
  chkIsEnabledPCRebooter.Checked := aSettings.IsEnabledPCRebooter;
  chkIsEnabledPCRebooterPropertiesEditValueChanged(nil);

  i := 0;
  while i < pnlPCRebooters.ControlCount do
    if pnlPCRebooters.Controls[i] is TframePCRebooterSettings then
      TframePCRebooterSettings(pnlPCRebooters.Controls[i]).Free
    else
      i := i + 1;

  for PCRebooter in aSettings.PCRebooters do begin
    var frame := CreateNewFramePCRebooter;
    frame.SetSettingsToUI(PCRebooter);
  end;
end;

function TfrmSetting.UIToSettings: TSettings;
var
  PCRebooters: TArray<TPCRebooterSettings>;
  i: integer;
begin
  if not IsValid then
    Exit;

  Result.IsColoredFreeSpace := chkColoredFreeSpace.Checked;
  Result.RedColorValue := Round(curRedValue.Value);
  Result.YellowColorValue := Round(curYellowValue.Value);
  Result.Timeout := Round(curTimeout.Value);
  Result.IsMainFormFullScreen := chkMainFormFullScreen.Checked;

  Result.IsColoredTBMDeviation := chkColoredTBM.Checked;
  Result.LessThanDeviationPercent := Round(edtLowerDeviationTreshold.Value);
  Result.MoreThanDeviationPercent := Round(edtHigherDeviationTreshold.Value);

  Result.HealthAuditRed := Round(edtHealthAuditRed.Value);
  Result.HealthAuditYellow := Round(edtHealthAuditYellow.Value);
  Result.HealthOnlineRed := Round(edtHealthOnlineRed.Value);
  Result.HealthOnlineYellow := Round(edtHealthOnlineYellow.Value);
  Result.HealthSuspensionRed := Round(edtHealthSuspensionRed.Value);
  Result.HealthSuspensionYellow := Round(edtHealthSuspensionYellow.Value);

  Result.IsLogHealthAuditRed := chkHealthAuditRedLog.Checked;
  Result.IsLogHealthAuditYellow := chkHealthAuditYellowLog.Checked;
  Result.IsLogHealthOnlineRed := chkHealthOnlineRedLog.Checked;
  Result.IsLogHealthOnlineYellow := chkHealthOnlineYellowLog.Checked;
  Result.IsLogHealthSuspensionRed := chkSuspensionRedLog.Checked;
  Result.IsLogHealthSuspensionYellow := chkSuspensionYellowLog.Checked;

  Result.IsDisplayHealthHotData := chkHealthHotData.Checked;
  Result.IsDisplayStorjNetUsed := chkIsDisplayStorjNetUsed.Checked;

  Result.IsEnableReloadNodesTimer := chkReloadNodesTimer.Checked;
  Result.ReloadNodesTimerMinutes := Round(edtReloadNodesTimerMinutes.Value);

  Result.IsUseTreePathForLog := chkUseTreePathForLog.Checked;
  Result.IsUseProxyForGetNeighbours := chkUseProxyForGetNeightbours.Checked;
  Result.IsUseNodeRefreshBuffer := chkUseNodeRefreshBuffer.Checked;

  Result.NotifySettings.EmailSettings.IsEnable := chkNotifyEmailEnable.Checked;
  Result.NotifySettings.EmailSettings.Email := edtNotifyEmail.Text;

  Result.NotifySettings.TelegramSettings.IsEnable := chkNotifyTelegramEnable.Checked;
  Result.NotifySettings.TelegramSettings.UserID := edtNotifyTelegramUserID.Text;

  Result.OnlineRangeMinutes := Round(numOnlineRangeMinutes.Value);

  //PCRebooter
  Result.IsEnabledPCRebooter := chkIsEnabledPCRebooter.Checked;

  PCRebooters := [];
  for i:= 0 to pnlPCRebooters.ControlCount - 1 do
    if pnlPCRebooters.Controls[i] is TframePCRebooterSettings then
      PCRebooters := PCRebooters + [TframePCRebooterSettings(pnlPCRebooters.Controls[i]).GetSettingsFromUI];

  Result.PCRebooters := PCRebooters;
end;

end.
