object frmChart: TfrmChart
  Left = 0
  Top = 0
  Caption = 'Chart'
  ClientHeight = 563
  ClientWidth = 1121
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  TextHeight = 13
  object dxLayoutControl: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 1121
    Height = 563
    Align = alClient
    TabOrder = 0
    object TreeList: TcxTreeList
      Left = -236
      Top = 10
      Width = 250
      Height = 405
      Bands = <
        item
        end>
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'SevenClassic'
      Navigator.Buttons.CustomButtons = <>
      OptionsBehavior.ChangeDelay = 1000
      OptionsView.ColumnAutoWidth = True
      PopupMenu = popmenuTreeChart
      ScrollbarAnnotations.CustomAnnotations = <>
      TabOrder = 0
      object clmTitle: TcxTreeListColumn
        Caption.AlignHorz = taCenter
        Caption.Text = 'Title'
        Options.Editing = False
        Options.Sorting = False
        Width = 167
        Position.ColIndex = 0
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmAddress: TcxTreeListColumn
        Visible = False
        Caption.Text = 'Address'
        Width = 100
        Position.ColIndex = 1
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmPort: TcxTreeListColumn
        Visible = False
        Caption.Text = 'Port'
        Width = 100
        Position.ColIndex = 2
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmDisplayChart: TcxTreeListColumn
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.ValueGrayed = 'False'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Display Chart'
        Options.Sorting = False
        Width = 81
        Position.ColIndex = 3
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmFullPath: TcxTreeListColumn
        VisibleForExpressionEditor = bFalse
        Visible = False
        Width = 100
        Position.ColIndex = 4
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
    object comboChartDataType: TcxComboBox
      Left = 116
      Top = 19
      Properties.DropDownListStyle = lsFixedList
      Properties.DropDownRows = 20
      Properties.Items.Strings = (
        'Historical Payment'
        'Historical Efficiency'
        'Historical Avg. TB*Month')
      Properties.OnEditValueChanged = comboChartDataTypePropertiesEditValueChanged
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 5
      Text = 'Historical Payment'
      Width = 175
    end
    object ProgressBar: TcxProgressBar
      Left = 34
      Top = 55
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.TransparentBorder = False
      TabOrder = 11
      Width = 1077
    end
    object gridChart: TcxGrid
      Left = 34
      Top = 82
      Width = 1077
      Height = 199
      TabOrder = 12
      object gridChartView: TcxGridDBChartView
        Categories.DataBinding.FieldName = 'Category'
        Categories.SortOrder = soAscending
        Categories.OnGetValueDisplayText = gridChartViewCategoriesGetValueDisplayText
        DataController.DataSource = DM.dtsrcChartData
        DiagramLine.Active = True
        DiagramLine.Values.MarkerStyle = cmsCircle
        object gridChartViewSeries1: TcxGridDBChartSeries
          DataBinding.FieldName = 'Total'
          DisplayText = '123'
          ValueCaptionFormat = '0.00 $'
        end
      end
      object gridChartLevel1: TcxGridLevel
        GridView = gridChartView
      end
    end
    object LeftRightSplitter: TcxSplitter
      Left = 20
      Top = 10
      Width = 8
      Height = 526
      Cursor = crSizeWE
      AlignSplitter = salRight
      AllowHotZoneDrag = False
      ResizeUpdate = True
      Control = TreeList
    end
    object btnLoadAndDisplay: TcxButton
      Left = 663
      Top = 17
      Width = 128
      Height = 25
      Caption = 'Load && Display'
      TabOrder = 8
      OnClick = btnLoadAndDisplayClick
    end
    object mmoLog: TcxMemo
      Left = -236
      Top = 447
      Properties.ScrollBars = ssVertical
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 3
      Height = 89
      Width = 250
    end
    object chkUseCache: TcxCheckBox
      Left = 797
      Top = 21
      Caption = 'Use Last Loaded Data'
      State = cbsChecked
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 9
      Transparent = True
    end
    object lblLog: TcxLabel
      Left = -210
      Top = 424
      Caption = 'Log'
      Style.HotTrack = False
      Style.TransparentBorder = False
      Transparent = True
    end
    object btnClearLog: TcxButton
      Left = -236
      Top = 421
      Width = 20
      Height = 20
      Hint = 'Clear log'
      Caption = 'btnClearLog'
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'iMaginary'
      OptionsImage.Glyph.SourceDPI = 96
      OptionsImage.Glyph.SourceHeight = 16
      OptionsImage.Glyph.SourceWidth = 16
      OptionsImage.Glyph.Data = {
        3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D225554
        462D38223F3E0D0A3C7376672076657273696F6E3D22312E31222069643D22D0
        A1D0BBD0BED0B95F322220786D6C6E733D22687474703A2F2F7777772E77332E
        6F72672F323030302F7376672220786D6C6E733A786C696E6B3D22687474703A
        2F2F7777772E77332E6F72672F313939392F786C696E6B2220783D2230707822
        20793D22307078222076696577426F783D223020302033322033322220737479
        6C653D22656E61626C652D6261636B67726F756E643A6E657720302030203332
        2033323B2220786D6C3A73706163653D227072657365727665223E262331333B
        262331303B3C7374796C6520747970653D22746578742F6373732220786D6C3A
        73706163653D227072657365727665223E2E426C61636B7B66696C6C3A233732
        373237323B7D262331333B262331303B2623393B2E59656C6C6F777B66696C6C
        3A234646423131353B7D3C2F7374796C653E0D0A3C706F6C79676F6E20636C61
        73733D2259656C6C6F772220706F696E74733D22382C323220362C3330203236
        2C33302032342C323220222F3E0D0A3C7061746820636C6173733D22426C6163
        6B2220643D224D32302C3136682D32563463302D312E312D302E392D322D322D
        32732D322C302E392D322C32763132682D32632D322E322C302D342C312E382D
        342C346831364332342C31372E382C32322E322C31362C32302C31367A222F3E
        0D0A3C2F7376673E0D0A}
      PaintStyle = bpsGlyph
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnClearLogClick
    end
    object dxChartControl: TdxChartControl
      Left = 34
      Top = 287
      Width = 1077
      Height = 249
      Titles = <>
      ToolTips.CrosshairOptions.StickyLines = Crosshair
      ToolTips.DefaultMode = Crosshair
      object dxChartControlXYDiagram: TdxChartXYDiagram
        Appearance.Margins.All = 2
        Axes.AxisX.CrosshairLabels.TextFormat = 'ADD'
        Axes.AxisX.Gridlines.Visible = True
        Axes.AxisX.Range.SideMarginMax = 3.000000000000000000
        object dxChartControl1XYSeries1: TdxChartXYSeries
          Caption = 'Total'
          DataBindingType = 'DB'
          DataBinding.DataSource = DM.dtsrcChartData
          ViewType = 'Line'
          View.Markers.Visible = True
          View.ValueLabels.TextFormat = 'Aw'
          ColorSchemeIndex = 0
        end
      end
    end
    object radiogrpSerieCaptionType: TcxRadioGroup
      Left = 926
      Top = 10
      Alignment = alCenterCenter
      Properties.Items = <
        item
          Caption = 'Title'
        end
        item
          Caption = 'Full Path'
        end>
      ItemIndex = 0
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.TransparentBorder = False
      StyleDisabled.BorderStyle = ebs3D
      TabOrder = 10
      Transparent = True
      Height = 39
      Width = 185
    end
    object datePickerFrom: TDatePicker
      Left = 328
      Top = 17
      Height = 25
      Date = 45276.000000000000000000
      DateFormat = 'yyyy-mm'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Segoe UI'
      Font.Style = []
      TabOrder = 6
    end
    object datePickerTo: TDatePicker
      Left = 505
      Top = 17
      Height = 25
      Date = 45276.000000000000000000
      DateFormat = 'yyyy-mm'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Segoe UI'
      Font.Style = []
      TabOrder = 7
    end
    object dxLayoutControlGroup_Root: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avClient
      Hidden = True
      ItemIndex = 2
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = -1
    end
    object layTree: TdxLayoutItem
      Parent = laygrpLeftPanel
      AlignVert = avClient
      CaptionOptions.Text = 'cxTreeList1'
      CaptionOptions.Visible = False
      CaptionOptions.Layout = clTop
      Control = TreeList
      ControlOptions.OriginalHeight = 150
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object laycomboChartDataType: TdxLayoutItem
      Parent = laygrpTopPanelButtons
      AlignVert = avCenter
      CaptionOptions.Text = 'Chart data type'
      Control = comboChartDataType
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 175
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object layProgressBar: TdxLayoutItem
      Parent = laygrpTopPanel
      CaptionOptions.Text = 'cxProgressBar1'
      CaptionOptions.Visible = False
      Control = ProgressBar
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object layChart: TdxLayoutItem
      Parent = laygrpRightPanel
      AlignHorz = ahClient
      AlignVert = avClient
      Visible = False
      CaptionOptions.Text = 'cxGrid1'
      CaptionOptions.Visible = False
      Control = gridChart
      ControlOptions.OriginalHeight = 200
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object laygrpRightPanel: TdxLayoutGroup
      Parent = dxLayoutControlGroup_Root
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'New Group'
      CaptionOptions.Visible = False
      AllowFloating = True
      ShowBorder = False
      Index = 2
    end
    object laygrpTopPanel: TdxLayoutGroup
      Parent = laygrpRightPanel
      CaptionOptions.Text = 'New Group'
      CaptionOptions.Visible = False
      ShowBorder = False
      Index = 0
    end
    object layLeftRightSplitter: TdxLayoutItem
      Parent = dxLayoutControlGroup_Root
      AlignHorz = ahLeft
      AlignVert = avClient
      CaptionOptions.Text = 'cxSplitter1'
      CaptionOptions.Visible = False
      Control = LeftRightSplitter
      ControlOptions.AutoColor = True
      ControlOptions.OriginalHeight = 100
      ControlOptions.OriginalWidth = 8
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object laybtnLoadAndDisplay: TdxLayoutItem
      Parent = laygrpTopPanelButtons
      AlignVert = avCenter
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnLoadAndDisplay
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 128
      ControlOptions.ShowBorder = False
      Index = 3
    end
    object laygrpTopPanelButtons: TdxLayoutGroup
      Parent = laygrpTopPanel
      CaptionOptions.Text = 'New Group'
      CaptionOptions.Visible = False
      ItemIndex = 1
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 0
    end
    object laymmoLog: TdxLayoutItem
      Parent = laygrpLeftPanel
      CaptionOptions.Text = 'Log'
      CaptionOptions.Visible = False
      CaptionOptions.Layout = clTop
      Control = mmoLog
      ControlOptions.OriginalHeight = 89
      ControlOptions.OriginalWidth = 185
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object laygrpLeftPanel: TdxLayoutGroup
      Parent = dxLayoutControlGroup_Root
      AlignHorz = ahLeft
      AlignVert = avClient
      CaptionOptions.Text = 'New Group'
      AllowFloating = True
      ItemIndex = 1
      ShowBorder = False
      Index = 0
    end
    object laychkUseCache: TdxLayoutItem
      Parent = laygrpTopPanelButtons
      AlignVert = avCenter
      CaptionOptions.Text = 'cxCheckBox1'
      CaptionOptions.Visible = False
      Control = chkUseCache
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 123
      ControlOptions.ShowBorder = False
      Index = 4
    end
    object laylblLog: TdxLayoutItem
      Parent = dxLayoutGroup1
      AlignVert = avCenter
      CaptionOptions.Text = 'cxLabel1'
      CaptionOptions.Visible = False
      CaptionOptions.Layout = clTop
      Control = lblLog
      ControlOptions.OriginalHeight = 13
      ControlOptions.OriginalWidth = 17
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object laybtnClearLog: TdxLayoutItem
      Parent = dxLayoutGroup1
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      CaptionOptions.Layout = clTop
      Control = btnClearLog
      ControlOptions.OriginalHeight = 20
      ControlOptions.OriginalWidth = 20
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutGroup1: TdxLayoutGroup
      Parent = laygrpLeftPanel
      CaptionOptions.Text = 'New Group'
      ItemIndex = 1
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 1
    end
    object dxLayoutItem1: TdxLayoutItem
      Parent = laygrpRightPanel
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'dxChartControl1'
      CaptionOptions.Visible = False
      Control = dxChartControl
      ControlOptions.OriginalHeight = 250
      ControlOptions.OriginalWidth = 300
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object layradiogrpSerieCaptionType: TdxLayoutItem
      Parent = laygrpTopPanelButtons
      Padding.AssignedValues = [lpavTop]
      CaptionOptions.Text = 'cxRadioGroup1'
      CaptionOptions.Visible = False
      Control = radiogrpSerieCaptionType
      ControlOptions.AutoColor = True
      ControlOptions.OriginalHeight = 39
      ControlOptions.OriginalWidth = 185
      ControlOptions.ShowBorder = False
      Index = 5
    end
    object laydatePickerFrom: TdxLayoutItem
      Parent = laygrpTopPanelButtons
      AlignVert = avCenter
      CaptionOptions.Text = 'From'
      Control = datePickerFrom
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 150
      Index = 1
    end
    object laydatePickerTo: TdxLayoutItem
      Parent = laygrpTopPanelButtons
      AlignVert = avCenter
      CaptionOptions.Text = 'To'
      Control = datePickerTo
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 150
      Index = 2
    end
  end
  object popmenuTreeChart: TPopupMenu
    Left = 416
    Top = 240
    object SelectAllChilds: TMenuItem
      Tag = 1
      Caption = 'Select Childs'
      OnClick = SelectAllChildsClick
    end
    object UnselectAllChilds: TMenuItem
      Caption = 'Unselect Childs'
      OnClick = SelectAllChildsClick
    end
  end
end
