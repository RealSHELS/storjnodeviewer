unit ufrmChart;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBasic, dxSkiniMaginary, dxSkinSeven,
  dxSkinSevenClassic, cxClasses, dxLayoutContainer, dxLayoutControl,
  cxCustomData, cxStyles, dxScrollbarAnnotations, cxTL, cxTLdxBarBuiltInMenu,
  cxInplaceContainer, dxLayoutcxEditAdapters, cxContainer, cxEdit, cxFilter,
  cxData, cxDataStorage, cxNavigator, dxDateRanges, Data.DB, cxDBData,
  cxGridLevel, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxProgressBar, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxSplitter, cxGridChartView, cxGridDBChartView,
  dxLayoutControlAdapters, Vcl.ComCtrls, dxCore, cxDateUtils, Vcl.Menus,
  Vcl.StdCtrls, cxButtons, cxCalendar,
  Storj.Consts, uNotifyCenter, uDM, Storj.Types, System.DateUtils, cxMemo,
  cxCheckBox, System.JSON, System.Generics.Collections, System.UITypes, cxLabel,
  dxSkinWXI, cxGeometry, cxVariants, dxCustomData, cxCustomCanvas,
  dxCoreGraphics, dxChartCore, dxChartData, dxChartLegend, dxChartSimpleDiagram,
  dxChartXYDiagram, dxChartXYSeriesLineView, dxChartXYSeriesAreaView,
  dxChartMarkers, dxChartXYSeriesBarView, dxCoreClasses, dxChartControl,
  dxChartDBData, cxGroupBox, cxRadioGroup, Vcl.WinXPickers,
  dxSkinOffice2013LightGray, dxSkinOffice2019Black, dxSkinOffice2019DarkGray,
  dxSkinOffice2019White, dxSkinWhiteprint;

type
  TfrmChart = class(TForm, IChartWorker)
    dxLayoutControlGroup_Root: TdxLayoutGroup;
    dxLayoutControl: TdxLayoutControl;
    TreeList: TcxTreeList;
    layTree: TdxLayoutItem;
    comboChartDataType: TcxComboBox;
    laycomboChartDataType: TdxLayoutItem;
    ProgressBar: TcxProgressBar;
    layProgressBar: TdxLayoutItem;
    gridChartLevel1: TcxGridLevel;
    gridChart: TcxGrid;
    layChart: TdxLayoutItem;
    laygrpRightPanel: TdxLayoutGroup;
    laygrpTopPanel: TdxLayoutGroup;
    LeftRightSplitter: TcxSplitter;
    layLeftRightSplitter: TdxLayoutItem;
    gridChartView: TcxGridDBChartView;
    btnLoadAndDisplay: TcxButton;
    laybtnLoadAndDisplay: TdxLayoutItem;
    gridChartViewSeries1: TcxGridDBChartSeries;
    laygrpTopPanelButtons: TdxLayoutGroup;
    mmoLog: TcxMemo;
    laymmoLog: TdxLayoutItem;
    laygrpLeftPanel: TdxLayoutGroup;
    chkUseCache: TcxCheckBox;
    laychkUseCache: TdxLayoutItem;
    clmTitle: TcxTreeListColumn;
    clmAddress: TcxTreeListColumn;
    clmPort: TcxTreeListColumn;
    clmDisplayChart: TcxTreeListColumn;
    lblLog: TcxLabel;
    laylblLog: TdxLayoutItem;
    btnClearLog: TcxButton;
    laybtnClearLog: TdxLayoutItem;
    dxLayoutGroup1: TdxLayoutGroup;
    dxChartControl: TdxChartControl;
    dxLayoutItem1: TdxLayoutItem;
    dxChartControlXYDiagram: TdxChartXYDiagram;
    dxChartControl1XYSeries1: TdxChartXYSeries;
    clmFullPath: TcxTreeListColumn;
    radiogrpSerieCaptionType: TcxRadioGroup;
    layradiogrpSerieCaptionType: TdxLayoutItem;
    datePickerFrom: TDatePicker;
    laydatePickerFrom: TdxLayoutItem;
    datePickerTo: TDatePicker;
    laydatePickerTo: TdxLayoutItem;
    popmenuTreeChart: TPopupMenu;
    SelectAllChilds: TMenuItem;
    UnselectAllChilds: TMenuItem;
    procedure btnLoadAndDisplayClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure gridChartViewCategoriesGetValueDisplayText(Sender: TObject;
      const AValue: Variant; var ADisplayText: string);
    procedure btnClearLogClick(Sender: TObject);
    procedure comboChartDataTypePropertiesEditValueChanged(Sender: TObject);
    procedure SelectAllChildsClick(Sender: TObject);
  private
    function GetAllChartDisplaySettings: TArray<TChartDisplaySettings>;
    procedure SetJArrTreeNodes(AJArr: TJSONArray; AParentNode: TcxTreeListNode);

    function GetCheckedTreeNodes(aParentNode: TcxTreeListNode): TArrayChartDisplayField;
  public
    procedure OnCommandShow(const aData: TValue);
    procedure CommandLoadFromJSON(const aData: TValue);

    //IChartWorker
    procedure DisplayChart;
    procedure SetProgressData(const aNodeCount: Integer);
    procedure IncProgress;
    function GetChartDisplaySettings: TChartDisplaySettings;
    function GetDisplayFields: TArray<String>;
    function GetDisplayFieldsFullData: TArrayChartDisplayField;
    procedure Log(const aText: String);
  end;

var
  frmChart: TfrmChart;

implementation

{$R *.dfm}

procedure TfrmChart.btnClearLogClick(Sender: TObject);
begin
  mmoLog.Lines.Clear;
end;

procedure TfrmChart.btnLoadAndDisplayClick(Sender: TObject);
begin
  var Arr := GetDisplayFields;
  if Length(Arr) = 0  then begin
    MessageDlg('No one tree node selected for display chart', mtError, [mbOk], 0);
    Exit;
  end;

  DM.LoadHistoricalNodesData(Self);
end;

procedure TfrmChart.comboChartDataTypePropertiesEditValueChanged(Sender: TObject);
begin
  if comboChartDataType.ItemIndex < 0 then
    Exit;

  var DisplaySettings := GetAllChartDisplaySettings[comboChartDataType.ItemIndex];

  case DisplaySettings.ChartDataType of
    cdtHistorical: begin
      laydatePickerFrom.Visible := True;
      laydatePickerTo.Visible := True;

      datePickerFrom.DateFormat := 'yyyy-mm';
      datePickerTo.DateFormat := 'yyyy-mm';
    end;
    cdtCurrentMonth: begin
      laydatePickerFrom.Visible := False;
      laydatePickerTo.Visible := False;
    end;
    cdtLocalHistory: begin
      laydatePickerFrom.Visible := True;
      laydatePickerTo.Visible := True;

      datePickerFrom.DateFormat := 'yyyy-mm-dd';
      datePickerTo.DateFormat := 'yyyy-mm-dd';
    end;
  end;
end;

procedure TfrmChart.CommandLoadFromJSON(const aData: TValue);
begin
  var JArr := aData.AsObject as TJSONArray;

  if not Assigned(JArr) then
    Exit;

  TreeList.BeginUpdate;
  try
    TreeList.Clear;
    SetJArrTreeNodes(JArr, TreeList.Root);
  finally
    TreeList.EndUpdate;
  end;
end;

procedure TfrmChart.DisplayChart;
var
  ChartDisplaySettings: TChartDisplaySettings;
begin
  ChartDisplaySettings := GetChartDisplaySettings;

//  gridChartView.BeginUpdate;
//  try
//    gridChartView.ClearSeries;
//    for var Item in GetDisplayFieldsFullData do begin
//      var Serie := gridChartView.CreateSeries;
//      Serie.DataBinding.FieldName := Item.FullPath;
//      if radiogrpSerieCaptionType.ItemIndex = 0 then
//        Serie.DisplayText := Item.Title
//      else
//        Serie.DisplayText := Item.FullPath;
//
//      Serie.ValueCaptionFormat := ChartDisplaySettings.ValueCaptionFormat;
//    end;
//  finally
//    gridChartView.EndUpdate;
//  end;

  dxChartControlXYDiagram.BeginUpdate;
  try
    dxChartControlXYDiagram.DeleteAllSeries;
    for var Item in GetDisplayFieldsFullData do begin
      var Serie := dxChartControlXYDiagram.AddSeries;
      Serie.DataBindingType := 'DB';
      Serie.ViewType := 'Line';
      Serie.SortBy := TdxChartSeriesSortBy.Argument;
      Serie.SortOrder := TdxSortOrder.soAscending;
      Serie.DataBindingClass := TdxChartSeriesDBDataBinding;
      (Serie.DataBinding as TdxChartSeriesDBDataBinding).DataSource := DM.dtsrcChartData;
      (Serie.DataBinding as TdxChartSeriesDBDataBinding).ArgumentField.FieldName := 'Category';
      (Serie.DataBinding as TdxChartSeriesDBDataBinding).ValueField.FieldName := Item.FullPath;
      TdxChartXYSeriesLineView(Serie.View).Markers.Visible := True;
      dxChartControlXYDiagram.Axes.AxisY.ValueLabels.TextFormat := ChartDisplaySettings.ValueCaptionFormatDxChart;

      if radiogrpSerieCaptionType.ItemIndex = 0 then
        Serie.Caption := Item.Title
      else
        Serie.Caption := Item.FullPath;

      var DTFormat := 'yyyy-mm-dd';
      if GetChartDisplaySettings.ChartDataType = TChartDataType.cdtHistorical then
         DTFormat := 'yyyy-mm';

      dxChartControlXYDiagram.Axes.AxisX.ValueLabels.TextFormat := '{V:' + DTFormat + '}';
      dxChartControlXYDiagram.Axes.AxisX.CrosshairLabels.TextFormat := '{V:' + DTFormat + '}';
      Serie.ToolTips.PointToolTipFormat := '{S} for {A:' + DTFormat + '} is ' + ChartDisplaySettings.ValueCaptionFormatDxChart;
    end;
  finally
    dxChartControlXYDiagram.EndUpdate;
  end;

  laygrpTopPanelButtons.Enabled := True;
  laygrpLeftPanel.Enabled := True;
end;

procedure TfrmChart.FormCreate(Sender: TObject);
var
  ArrChartDisplaySettings: TArray<TChartDisplaySettings>;
begin
  TNotifyCenter.Instance.Subscribe(COMMAND_SHOW_CHART, OnCommandShow);
  TNotifyCenter.Instance.Subscribe(COMMAND_LOAD_FROM_JSON, CommandLoadFromJSON);

  //gridChartView.ClearSeries;

  datePickerFrom.Date := IncYear(Now, -4);
  datePickerTo.Date := Now;

  comboChartDataType.Properties.Items.Clear;
  ArrChartDisplaySettings := GetAllChartDisplaySettings;
  if Length(ArrChartDisplaySettings) > 0 then begin
    for var Item in ArrChartDisplaySettings do
      comboChartDataType.Properties.Items.Add(Item.Title);

    comboChartDataType.ItemIndex := 0;
  end;
end;

procedure TfrmChart.FormDestroy(Sender: TObject);
begin
  TNotifyCenter.Instance.Unsubscribe(COMMAND_SHOW_CHART, OnCommandShow);
  TNotifyCenter.Instance.Unsubscribe(COMMAND_LOAD_FROM_JSON, CommandLoadFromJSON);
end;

function TfrmChart.GetAllChartDisplaySettings: TArray<TChartDisplaySettings>;
var
  Value: TChartDisplaySettings;
begin
  Result := [];

  Value.Title := 'Historical - Payment';
  Value.FieldType := TFieldType.ftFloat;
  Value.FieldName := 'Payment';
  Value.ChartDataType := TChartDataType.cdtHistorical;
  Value.ValueCaptionFormat := '0.00 $';
  Value.ValueCaptionFormatDxChart := '{V:0.00} $';
  Result := Result + [Value];

  Value.Title := 'Historical - Avg. TB*M';
  Value.FieldType := TFieldType.ftFloat;
  Value.FieldName := 'TBM';
  Value.ChartDataType := TChartDataType.cdtHistorical;
  Value.ValueCaptionFormat := '0.### TB*M';
  Value.ValueCaptionFormatDxChart := '{V:0.###} TB*M';
  Result := Result + [Value];

  Value.Title := 'Historical - Efficiency';
  Value.FieldType := TFieldType.ftFloat;
  Value.FieldName := 'Efficiency';
  Value.ChartDataType := TChartDataType.cdtHistorical;
  Value.ValueCaptionFormat := '0.00 $/TB';
  Value.ValueCaptionFormatDxChart := '{V:0.00} $/TB';
  Result := Result + [Value];

  Value.Title := 'Historical - Egress';
  Value.FieldType := TFieldType.ftFloat;
  Value.FieldName := 'Egress';
  Value.ChartDataType := TChartDataType.cdtHistorical;
  Value.ValueCaptionFormat := '# ##0 GB';
  Value.ValueCaptionFormatDxChart := '{V:# ##0} GB';
  Result := Result + [Value];

  Value.Title := 'Historical - Repair Egress';
  Value.FieldType := TFieldType.ftFloat;
  Value.FieldName := 'RepairEgress';
  Value.ChartDataType := TChartDataType.cdtHistorical;
  Value.ValueCaptionFormat := '# ##0 GB';
  Value.ValueCaptionFormatDxChart := '{V:# ##0} GB';
  Result := Result + [Value];

  Value.Title := 'Historical - Total Egress';
  Value.FieldType := TFieldType.ftFloat;
  Value.FieldName := 'TotalEgress';
  Value.ChartDataType := TChartDataType.cdtHistorical;
  Value.ValueCaptionFormat := '# ##0 GB';
  Value.ValueCaptionFormatDxChart := '{V:# ##0} GB';
  Result := Result + [Value];

  Value.Title := 'Current Month - Egress';
  Value.FieldType := TFieldType.ftFloat;
  Value.FieldName := 'Egress';
  Value.ChartDataType := TChartDataType.cdtCurrentMonth;
  Value.ValueCaptionFormat := '# ##0 GB';
  Value.ValueCaptionFormatDxChart := '{V:# ##0} GB';
  Result := Result + [Value];

  Value.Title := 'Current Month - Repair Egress';
  Value.FieldType := TFieldType.ftFloat;
  Value.FieldName := 'RepairEgress';
  Value.ChartDataType := TChartDataType.cdtCurrentMonth;
  Value.ValueCaptionFormat := '# ##0 GB';
  Value.ValueCaptionFormatDxChart := '{V:# ##0} GB';
  Result := Result + [Value];

  Value.Title := 'Current Month - Total Egress';
  Value.FieldType := TFieldType.ftFloat;
  Value.FieldName := 'TotalEgress';
  Value.ChartDataType := TChartDataType.cdtCurrentMonth;
  Value.ValueCaptionFormat := '# ##0 GB';
  Value.ValueCaptionFormatDxChart := '{V:# ##0} GB';
  Result := Result + [Value];

  Value.Title := 'Current Month - Avg. TB*M';
  Value.FieldType := TFieldType.ftFloat;
  Value.FieldName := 'TBM';
  Value.ChartDataType := TChartDataType.cdtCurrentMonth;
  Value.ValueCaptionFormat := '0.### TB*M';
  Value.ValueCaptionFormatDxChart := '{V:0.###} TB*M';
  Result := Result + [Value];

  Value.Title := 'Current Month - Ingress';
  Value.FieldType := TFieldType.ftFloat;
  Value.FieldName := 'Ingress';
  Value.ChartDataType := TChartDataType.cdtCurrentMonth;
  Value.ValueCaptionFormat := '# ##0 GB';
  Value.ValueCaptionFormatDxChart := '{V:# ##0} GB';
  Result := Result + [Value];

  Value.Title := 'Current Month - Repair Ingress';
  Value.FieldType := TFieldType.ftFloat;
  Value.FieldName := 'RepairIngress';
  Value.ChartDataType := TChartDataType.cdtCurrentMonth;
  Value.ValueCaptionFormat := '# ##0 GB';
  Value.ValueCaptionFormatDxChart := '{V:# ##0} GB';
  Result := Result + [Value];

  Value.Title := 'Current Month - Total Ingress';
  Value.FieldType := TFieldType.ftFloat;
  Value.FieldName := 'TotalIngress';
  Value.ChartDataType := TChartDataType.cdtCurrentMonth;
  Value.ValueCaptionFormat := '# ##0 GB';
  Value.ValueCaptionFormatDxChart := '{V:# ##0} GB';
  Result := Result + [Value];

  Value.Title := 'Current Month - Total Bandwidth';
  Value.FieldType := TFieldType.ftFloat;
  Value.FieldName := 'TotalBandwidth';
  Value.ChartDataType := TChartDataType.cdtCurrentMonth;
  Value.ValueCaptionFormat := '# ##0 GB';
  Value.ValueCaptionFormatDxChart := '{V:# ##0} GB';
  Result := Result + [Value];

  Value.Title := 'Local History - Avg. TB*M';
  Value.FieldType := TFieldType.ftFloat;
  Value.FieldName := 'TBM';
  Value.ChartDataType := TChartDataType.cdtLocalHistory;
  Value.ValueCaptionFormat := '0.### TB*M';
  Value.ValueCaptionFormatDxChart := '{V:0.###} TB*M';
  Result := Result + [Value];

  Value.Title := 'Local History - Ingress';
  Value.FieldType := TFieldType.ftFloat;
  Value.FieldName := 'Ingress';
  Value.ChartDataType := TChartDataType.cdtLocalHistory;
  Value.ValueCaptionFormat := '### ##0.## GB';
  Value.ValueCaptionFormatDxChart := '{V:### ##0.##} GB';
  Result := Result + [Value];

  Value.Title := 'Local History - Egress';
  Value.FieldType := TFieldType.ftFloat;
  Value.FieldName := 'Egress';
  Value.ChartDataType := TChartDataType.cdtLocalHistory;
  Value.ValueCaptionFormat := '### ##0.## GB';
  Value.ValueCaptionFormatDxChart := '{V:### ##0.##} GB';
  Result := Result + [Value];

  Value.Title := 'Local History - Repair Ingress';
  Value.FieldType := TFieldType.ftFloat;
  Value.FieldName := 'RepairIngress';
  Value.ChartDataType := TChartDataType.cdtLocalHistory;
  Value.ValueCaptionFormat := '### ##0.## GB';
  Value.ValueCaptionFormatDxChart := '{V:### ##0.##} GB';
  Result := Result + [Value];

  Value.Title := 'Local History - Repair Egress';
  Value.FieldType := TFieldType.ftFloat;
  Value.FieldName := 'RepairEgress';
  Value.ChartDataType := TChartDataType.cdtLocalHistory;
  Value.ValueCaptionFormat := '### ##0.## GB';
  Value.ValueCaptionFormatDxChart := '{V:### ##0.##} GB';
  Result := Result + [Value];

  Value.Title := 'Local History - Total Bandwidth';
  Value.FieldType := TFieldType.ftFloat;
  Value.FieldName := 'TotalBandwidth';
  Value.ChartDataType := TChartDataType.cdtLocalHistory;
  Value.ValueCaptionFormat := '### ##0.## GB';
  Value.ValueCaptionFormatDxChart := '{V:### ##0.##} GB';
  Result := Result + [Value];

  Value.Title := 'Local History - Allocated Space';
  Value.FieldType := TFieldType.ftInteger;
  Value.FieldName := 'AllocatedSpace';
  Value.ChartDataType := TChartDataType.cdtLocalHistory;
  Value.ValueCaptionFormat := '# ##0 GB';
  Value.ValueCaptionFormatDxChart := '{V:# ##0} GB';
  Result := Result + [Value];

  Value.Title := 'Local History - Free Space';
  Value.FieldType := TFieldType.ftInteger;
  Value.FieldName := 'FreeSpace';
  Value.ChartDataType := TChartDataType.cdtLocalHistory;
  Value.ValueCaptionFormat := '# ##0 GB';
  Value.ValueCaptionFormatDxChart := '{V:# ##0} GB';
  Result := Result + [Value];

  Value.Title := 'Local History - Used Space';
  Value.FieldType := TFieldType.ftInteger;
  Value.FieldName := 'UsedSpace';
  Value.ChartDataType := TChartDataType.cdtLocalHistory;
  Value.ValueCaptionFormat := '# ##0 GB';
  Value.ValueCaptionFormatDxChart := '{V:# ##0} GB';
  Result := Result + [Value];

  Value.Title := 'Local History - Trash';
  Value.FieldType := TFieldType.ftInteger;
  Value.FieldName := 'Trash';
  Value.ChartDataType := TChartDataType.cdtLocalHistory;
  Value.ValueCaptionFormat := '# ##0 GB';
  Value.ValueCaptionFormatDxChart := '{V:# ##0} GB';
  Result := Result + [Value];

  Value.Title := 'Local History - Payout Dirty';
  Value.FieldType := TFieldType.ftFloat;
  Value.FieldName := 'PayoutDirty';
  Value.ChartDataType := TChartDataType.cdtLocalHistory;
  Value.ValueCaptionFormat := '0.00 $';
  Value.ValueCaptionFormatDxChart := '{V:0.00} $';
  Result := Result + [Value];

  Value.Title := 'Local History - Payout Cleanly';
  Value.FieldType := TFieldType.ftFloat;
  Value.FieldName := 'PayoutCleanly';
  Value.ChartDataType := TChartDataType.cdtLocalHistory;
  Value.ValueCaptionFormat := '0.00 $';
  Value.ValueCaptionFormatDxChart := '{V:0.00} $';
  Result := Result + [Value];

  Value.Title := 'Local History - Estimated Dirty';
  Value.FieldType := TFieldType.ftFloat;
  Value.FieldName := 'EstimatedDirty';
  Value.ChartDataType := TChartDataType.cdtLocalHistory;
  Value.ValueCaptionFormat := '0.00 $';
  Value.ValueCaptionFormatDxChart := '{V:0.00} $';
  Result := Result + [Value];

  Value.Title := 'Local History - Estimated Cleanly';
  Value.FieldType := TFieldType.ftFloat;
  Value.FieldName := 'EstimatedCleanly';
  Value.ChartDataType := TChartDataType.cdtLocalHistory;
  Value.ValueCaptionFormat := '0.00 $';
  Value.ValueCaptionFormatDxChart := '{V:0.00} $';
  Result := Result + [Value];

  Value.Title := 'Local History - Efficiency';
  Value.FieldType := TFieldType.ftFloat;
  Value.FieldName := 'Efficiency';
  Value.ChartDataType := TChartDataType.cdtLocalHistory;
  Value.ValueCaptionFormat := '0.00 $/TB';
  Value.ValueCaptionFormatDxChart := '{V:0.00} $/TB';
  Result := Result + [Value];

  Value.Title := 'Local History - Daily Ingress';
  Value.FieldType := TFieldType.ftFloat;
  Value.FieldName := 'DailyIngress';
  Value.ChartDataType := TChartDataType.cdtLocalHistory;
  Value.ValueCaptionFormat := '### ##0.## GB';
  Value.ValueCaptionFormatDxChart := '{V:### ##0.##} GB';
  Result := Result + [Value];

  Value.Title := 'Local History - Daily Egress';
  Value.FieldType := TFieldType.ftFloat;
  Value.FieldName := 'DailyEgress';
  Value.ChartDataType := TChartDataType.cdtLocalHistory;
  Value.ValueCaptionFormat := '### ##0.## GB';
  Value.ValueCaptionFormatDxChart := '{V:### ##0.##} GB';
  Result := Result + [Value];

  Value.Title := 'Local History - Daily Repair Ingress';
  Value.FieldType := TFieldType.ftFloat;
  Value.FieldName := 'DailyRepairIngress';
  Value.ChartDataType := TChartDataType.cdtLocalHistory;
  Value.ValueCaptionFormat := '### ##0.## GB';
  Value.ValueCaptionFormatDxChart := '{V:### ##0.##} GB';
  Result := Result + [Value];

  Value.Title := 'Local History - Daily Repair Egress';
  Value.FieldType := TFieldType.ftFloat;
  Value.FieldName := 'DailyRepairEgress';
  Value.ChartDataType := TChartDataType.cdtLocalHistory;
  Value.ValueCaptionFormat := '### ##0.## GB';
  Value.ValueCaptionFormatDxChart := '{V:### ##0.##} GB';
  Result := Result + [Value];

  Value.Title := 'Local History - Daily Total Bandwidth';
  Value.FieldType := TFieldType.ftFloat;
  Value.FieldName := 'DailyTotalBandwidth';
  Value.ChartDataType := TChartDataType.cdtLocalHistory;
  Value.ValueCaptionFormat := '### ##0.## GB';
  Value.ValueCaptionFormatDxChart := '{V:### ##0.##} GB';
  Result := Result + [Value];
end;

function TfrmChart.GetChartDisplaySettings: TChartDisplaySettings;
begin
  Result := GetAllChartDisplaySettings[comboChartDataType.ItemIndex];

  case Result.ChartDataType of
    cdtHistorical: begin
      Result.DateFrom := StartOfTheMonth(datePickerFrom.Date);
      Result.DateTo := StartOfTheMonth(datePickerTo.Date);

      Result.IsUseCache := chkUseCache.Checked;
    end;
    cdtCurrentMonth: begin
      Result.IsUseCache := chkUseCache.Checked;
    end;
    cdtLocalHistory: begin
      Result.DateFrom := datePickerFrom.Date;
      Result.DateTo := datePickerTo.Date;

      Result.IsUseCache := False;
    end;
  end;
end;

function TfrmChart.GetCheckedTreeNodes(aParentNode: TcxTreeListNode): TArray<TChartDisplayField>;
var
  i: integer;
  Field: TChartDisplayField;
begin
  Result := [];

  for i := 0 to aParentNode.Count - 1 do begin
    if Boolean(aParentNode.Items[i].Values[clmDisplayChart.Position.ColIndex]) then begin
      Field.Title := aParentNode.Items[i].Values[clmTitle.Position.ColIndex];
      Field.FullPath := aParentNode.Items[i].Values[clmFullPath.Position.ColIndex];
      Result := Result + [Field];
    end;

    if aParentNode.Items[i].Count > 0 then
      Result := Result + GetCheckedTreeNodes(aParentNode.Items[i]);
  end;
end;

function TfrmChart.GetDisplayFields: TArray<String>;
begin
  Result := GetDisplayFieldsFullData.GetFullPathArray;
end;

function TfrmChart.GetDisplayFieldsFullData: TArrayChartDisplayField;
begin
  Result := GetCheckedTreeNodes(TreeList.Root);
end;

procedure TfrmChart.gridChartViewCategoriesGetValueDisplayText(Sender: TObject;
  const AValue: Variant; var ADisplayText: string);
begin
  if GetChartDisplaySettings.ChartDataType = TChartDataType.cdtHistorical then
    ADisplayText := FormatDateTime('yyyy-mm', AValue)
  else
    ADisplayText := FormatDateTime('yyyy-mm-dd', AValue);
end;

procedure TfrmChart.IncProgress;
begin
  ProgressBar.Position := ProgressBar.Position + 1;
end;

procedure TfrmChart.Log(const aText: String);
begin
  mmoLog.Lines.Add(aText);
end;

procedure TfrmChart.OnCommandShow(const aData: TValue);
begin
  Show;
end;

procedure TfrmChart.SelectAllChildsClick(Sender: TObject);
begin
  var IsSelectChilds := TMenuItem(Sender).Tag = 1;
  if TreeList.FocusedNode.HasChildren then
    for var i := 0 to TreeList.FocusedNode.Count - 1 do
      TreeList.FocusedNode.Items[i].Values[clmDisplayChart.Position.ColIndex] := IsSelectChilds;
end;

procedure TfrmChart.SetJArrTreeNodes(aJArr: TJSONArray; AParentNode: TcxTreeListNode);
var
  i: integer;
  JObj: TJSONObject;
  JValue: TJSONValue;
  Node: TcxTreeListNode;
begin
  for i := 0 to aJarr.Count - 1 do begin
    JObj := aJArr.Items[i] as TJSONObject;
    if not JObj.GetValue<Boolean>('IsActive') then
      Continue;

    Node := AParentNode.AddChild;
    Node.Values[clmTitle.Position.ColIndex] := JObj.GetValue<String>('Title');
    Node.Values[clmAddress.Position.ColIndex] := JObj.GetValue<String>('Address');
    Node.Values[clmPort.Position.ColIndex] := JObj.GetValue<String>('Port');
    Node.Values[clmDisplayChart.Position.ColIndex] := False;
    Node.Values[clmFullPath.Position.ColIndex] := JObj.GetValue<String>('FullPath', '');

    JValue := JObj.GetValue<TJSONArray>('Childs', nil);
    if JValue <> nil then
      SetJArrTreeNodes(JValue as TJSONArray, Node);

    if JObj.GetValue<Boolean>('AutoExpand', False) then
      Node.Expanded := True;
  end;
end;

procedure TfrmChart.SetProgressData(const aNodeCount: Integer);
begin
  laygrpTopPanelButtons.Enabled := False;
  laygrpLeftPanel.Enabled := False;

  //gridChartView.ClearSeries;
  dxChartControlXYDiagram.DeleteAllSeries;

  ProgressBar.Position := 0;
  ProgressBar.Properties.Min := 0;
  ProgressBar.Properties.Max := aNodeCount;

  Application.ProcessMessages;
end;

end.
