unit ufrmFinancialDataTree;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBasic, dxSkiniMaginary, dxSkinSeven,
  dxSkinSevenClassic, cxClasses, dxLayoutContainer, dxLayoutControl,
  cxCustomData, cxStyles, dxScrollbarAnnotations, cxTL, cxTLdxBarBuiltInMenu,
  cxInplaceContainer, cxTextEdit, cxCheckBox,
  System.JSON, System.Generics.Collections, cxDropDownEdit,
  Storj.Types, cxCurrencyEdit, uNotifyCenter, Vcl.Menus, dxSkinWXI, cxFilter;

type
  TfrmFinancialData = class(TForm)
    dxLayoutControlGroup_Root: TdxLayoutGroup;
    dxLayoutControl: TdxLayoutControl;
    dxLayoutGroupTop: TdxLayoutGroup;
    dxLayoutGroupClient: TdxLayoutGroup;
    TreeListFinancial: TcxTreeList;
    layTreeList: TdxLayoutItem;
    clmTitle: TcxTreeListColumn;
    clmPayoutDirty: TcxTreeListColumn;
    clmPayoutCleanly: TcxTreeListColumn;
    clmEstimatedDirty: TcxTreeListColumn;
    clmEstimatedCleanly: TcxTreeListColumn;
    clmEfficiencyByUsed: TcxTreeListColumn;
    clmUSDCost: TcxTreeListColumn;
    clmCleanProfit: TcxTreeListColumn;
    clmEstimatedCleanProfit: TcxTreeListColumn;
    clmAddress: TcxTreeListColumn;
    clmPort: TcxTreeListColumn;
    clmUsedSpace: TcxTreeListColumn;
    clmCurrency: TcxTreeListColumn;
    clmCost: TcxTreeListColumn;
    clmComment: TcxTreeListColumn;
    clmTotalEarned: TcxTreeListColumn;
    PopupMenu: TPopupMenu;
    RefreshTotalEarned1: TMenuItem;
    clmEfficiencyByTBM: TcxTreeListColumn;
    clmTBM: TcxTreeListColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure RefreshTotalEarned1Click(Sender: TObject);
  private
    FRates: TCurrencyRates;

    procedure SetJArrTreeNodes(AJArr: TJSONArray; AParentNode: TcxTreeListNode);
    procedure RecalcParent(AParentNode: TcxTreeListNode);
    function VarToCurrency(aValue: Variant): Currency;

    procedure CommandShow(const aData: TValue);
    procedure CommandReceiveTotalEarned(const aData: TValue);
    procedure CommandShowNodeData(const aData: TValue);
    procedure CommandShowNodeDataGroup(const aData: TValue);
    procedure CommandLoadFromJSON(const aData: TValue);

    function GetNode(AParentNode: TcxTreeListNode; ANode: TNode): TcxTreeListNode;
    procedure RecalcParentTotalEarned(AParentNode: TcxTreeListNode);
  public
  end;

var
  frmFinancialData: TfrmFinancialData;

implementation

{$R *.dfm}

uses
  uDM, Storj.CurrencyGetter, Storj.Consts;

{ TfrmFinancialData }

procedure TfrmFinancialData.CommandReceiveTotalEarned(const aData: TValue);
var
  Node: TcxTreeListNode;
  NodeData: TNodeData;
begin
  if not aData.IsType<TNodeData> then
    Exit;

  NodeData := aData.AsType<TNodeData>;

  TreeListFinancial.BeginUpdate;
  try
    Node := GetNode(TreeListFinancial.Root, NodeData.Node);
    if Node = nil then
      Node := TreeListFinancial.Root.AddChild;

    Node.Values[clmTotalEarned.Position.ColIndex] := NodeData.TotalEarned;

    RecalcParentTotalEarned(Node.Parent);
  finally
    TreeListFinancial.EndUpdate;
  end;
end;

procedure TfrmFinancialData.CommandShow(const aData: TValue);
begin
  Self.Show;
end;

procedure TfrmFinancialData.FormCreate(Sender: TObject);
begin
  FRates := GetCurrenciesRates;

  TNotifyCenter.Instance.Subscribe(COMMAND_SHOW_FINANCIAL_DATA_TREE, CommandShow);
  TNotifyCenter.Instance.Subscribe(COMMAND_RECEIVE_TOTAL_EARNED, CommandReceiveTotalEarned);
  TNotifyCenter.Instance.Subscribe(COMMAND_SHOW_NODE_DATA, CommandShowNodeData);
  TNotifyCenter.Instance.Subscribe(COMMAND_SHOW_NODE_DATA_GROUP, CommandShowNodeDataGroup);
  TNotifyCenter.Instance.Subscribe(COMMAND_LOAD_FROM_JSON, CommandLoadFromJSON);
end;

procedure TfrmFinancialData.FormDestroy(Sender: TObject);
begin
  FRates.Free;

  TNotifyCenter.Instance.UnSubscribe(COMMAND_SHOW_FINANCIAL_DATA_TREE, CommandShow);
  TNotifyCenter.Instance.UnSubscribe(COMMAND_RECEIVE_TOTAL_EARNED, CommandReceiveTotalEarned);
  TNotifyCenter.Instance.UnSubscribe(COMMAND_SHOW_NODE_DATA, CommandShowNodeData);
  TNotifyCenter.Instance.UnSubscribe(COMMAND_SHOW_NODE_DATA_GROUP, CommandShowNodeDataGroup);
  TNotifyCenter.Instance.UnSubscribe(COMMAND_LOAD_FROM_JSON, CommandLoadFromJSON);
end;

function TfrmFinancialData.GetNode(AParentNode: TcxTreeListNode; ANode: TNode): TcxTreeListNode;
var
  i: integer;
begin
  for i := 0 to AParentNode.Count - 1 do begin
    Result := AParentNode.Items[i];
    if (Result.Values[clmAddress.Position.ColIndex] = ANode.Address) and
       (Result.Values[clmPort.Position.ColIndex] = ANode.Port) then
      Exit(Result);

    if Result.HasChildren then
      Result := GetNode(Result, ANode)
    else
      Result := nil;
    if Result <> nil then
      Exit(Result);
  end;

  Result := nil;
end;

procedure TfrmFinancialData.CommandLoadFromJSON(const aData: TValue);
begin
  var JArr := aData.AsObject as TJSONArray;

  if not Assigned(JArr) then
    Exit;

  TreeListFinancial.BeginUpdate;
  try
    TreeListFinancial.Clear;
    SetJArrTreeNodes(JArr, TreeListFinancial.Root);
  finally
    TreeListFinancial.EndUpdate;
  end;
end;

procedure TfrmFinancialData.RecalcParent(AParentNode: TcxTreeListNode);
var
  i: integer;
  UsedSpace: Integer;
  ClearPayout, DirtyPayout, EstimatedDirtyPayout, EstimatedCleanlyPayout: Currency;
  USDCost, CleanProfit, EstimatedCleanProfit, TBM: Currency;
  InUSD: Variant;
begin
  if TreeListFinancial.Root.Equals(AParentNode) then
    Exit;

  UsedSpace := 0;
  ClearPayout := 0;
  DirtyPayout := 0;
  EstimatedDirtyPayout := 0;
  EstimatedCleanlyPayout := 0;
  USDCost := 0; CleanProfit := 0;
  EstimatedCleanProfit := 0;
  TBM := 0;
  for i := 0 to AParentNode.Count - 1 do begin
    if AParentNode.Items[i].Values[clmUsedSpace.Position.ColIndex] <> null then
      UsedSpace := UsedSpace + AParentNode.Items[i].Values[clmUsedSpace.Position.ColIndex];

    if AParentNode.Items[i].Values[clmPayoutDirty.Position.ColIndex] <> null then
      DirtyPayout := DirtyPayout + AParentNode.Items[i].Values[clmPayoutDirty.Position.ColIndex];

    if AParentNode.Items[i].Values[clmPayoutCleanly.Position.ColIndex] <> null then
      ClearPayout := ClearPayout + AParentNode.Items[i].Values[clmPayoutCleanly.Position.ColIndex];

    if AParentNode.Items[i].Values[clmEstimatedDirty.Position.ColIndex] <> null then
      EstimatedDirtyPayout := EstimatedDirtyPayout + AParentNode.Items[i].Values[clmEstimatedDirty.Position.ColIndex];

    if AParentNode.Items[i].Values[clmEstimatedCleanly.Position.ColIndex] <> null then
      EstimatedCleanlyPayout := EstimatedCleanlyPayout + AParentNode.Items[i].Values[clmEstimatedCleanly.Position.ColIndex];

    if AParentNode.Items[i].Values[clmUSDCost.Position.ColIndex] <> null then
      USDCost := USDCost + AParentNode.Items[i].Values[clmUSDCost.Position.ColIndex];

    if AParentNode.Items[i].Values[clmCleanProfit.Position.ColIndex] <> null then
      CleanProfit := CleanProfit + AParentNode.Items[i].Values[clmCleanProfit.Position.ColIndex];

    if AParentNode.Items[i].Values[clmEstimatedCleanProfit.Position.ColIndex] <> null then
      EstimatedCleanProfit := EstimatedCleanProfit + AParentNode.Items[i].Values[clmEstimatedCleanProfit.Position.ColIndex];

    if AParentNode.Items[i].Values[clmTBM.Position.ColIndex] <> null then
      TBM := TBM + AParentNode.Items[i].Values[clmTBM.Position.ColIndex];
  end;

  AParentNode.Values[clmPayoutDirty.Position.ColIndex] := DirtyPayout;
  AParentNode.Values[clmPayoutCleanly.Position.ColIndex] := ClearPayout;
  AParentNode.Values[clmEstimatedDirty.Position.ColIndex] := EstimatedDirtyPayout;
  AParentNode.Values[clmEstimatedCleanly.Position.ColIndex] := EstimatedCleanlyPayout;
  AParentNode.Values[clmUsedSpace.Position.ColIndex] := UsedSpace;
  AParentNode.Values[clmTBM.Position.ColIndex] := TBM;

  if CleanProfit <> 0 then
    AParentNode.Values[clmCleanProfit.Position.ColIndex] := CleanProfit
  else
    AParentNode.Values[clmCleanProfit.Position.ColIndex] := null;

  if EstimatedCleanProfit <> 0 then
    AParentNode.Values[clmEstimatedCleanProfit.Position.ColIndex] := EstimatedCleanProfit
  else
    AParentNode.Values[clmEstimatedCleanProfit.Position.ColIndex] := null;

  if USDCost > 0 then
    AParentNode.Values[clmUSDCost.Position.ColIndex] := USDCost
  else begin
    var Cost := AParentNode.Values[clmCost.Position.ColIndex];
    var Currency := AParentNode.Values[clmCurrency.Position.ColIndex];
    InUSD := null;
    if (VarToStr(Cost) <> '') and (VarToStr(Currency) <> '') then
      if FRates.RatesToUSD.ContainsKey(Currency) then
        InUSD := FRates.RatesToUSD[Currency] * VarToCurrency(Cost)
      else
        InUSD := 0;

    AParentNode.Values[clmUSDCost.Position.ColIndex] := InUSD;
    if InUSD = null then
      InUSD := 0;

    AParentNode.Values[clmCleanProfit.Position.ColIndex] := ClearPayout - InUSD;
    AParentNode.Values[clmEstimatedCleanProfit.Position.ColIndex] := EstimatedCleanlyPayout - InUSD;
  end;

  if UsedSpace > 0 then
    AParentNode.Values[clmEfficiencyByUsed.Position.ColIndex] := EstimatedDirtyPayout / UsedSpace * 1000
  else
    AParentNode.Values[clmEfficiencyByUsed.Position.ColIndex] := 0;

  if TBM > 0 then
    AParentNode.Values[clmEfficiencyByTBM.Position.ColIndex] := EstimatedDirtyPayout / TBM
  else
    AParentNode.Values[clmEfficiencyByTBM.Position.ColIndex] := 0;

  RecalcParent(AParentNode.Parent);
end;

procedure TfrmFinancialData.RecalcParentTotalEarned(AParentNode: TcxTreeListNode);
var
  i: integer;
  TotalEarned: Currency;
begin
  if TreeListFinancial.Root.Equals(AParentNode) then
    Exit;

  TotalEarned := 0;
  for i := 0 to AParentNode.Count - 1 do begin
    if AParentNode.Items[i].Values[clmTotalEarned.Position.ColIndex] <> null then
      TotalEarned := TotalEarned + AParentNode.Items[i].Values[clmTotalEarned.Position.ColIndex];
  end;

  AParentNode.Values[clmTotalEarned.Position.ColIndex] := TotalEarned;

  RecalcParentTotalEarned(AParentNode.Parent);
end;

procedure TfrmFinancialData.RefreshTotalEarned1Click(Sender: TObject);
begin
  DM.LoadTotalEarned;
end;

procedure TfrmFinancialData.SetJArrTreeNodes(AJArr: TJSONArray; AParentNode: TcxTreeListNode);
var
  i: integer;
  JObj: TJSONObject;
  JValue: TJSONValue;
  Node: TcxTreeListNode;
begin
  for i := 0 to AJarr.Count - 1 do begin
    JObj := AJArr.Items[i] as TJSONObject;
    if not JObj.GetValue<Boolean>('IsActive') then
      Continue;

    Node := AParentNode.AddChild;
    Node.Values[clmTitle.Position.ColIndex] := JObj.GetValue<String>('Title');
    Node.Values[clmAddress.Position.ColIndex] := JObj.GetValue<String>('Address', '');
    Node.Values[clmPort.Position.ColIndex] := JObj.GetValue<String>('Port', '');
    Node.Values[clmCurrency.Position.ColIndex] := JObj.GetValue<String>('Currency', '');
    Node.Values[clmCost.Position.ColIndex] := JObj.GetValue<String>('Cost', '');
    Node.Values[clmComment.Position.ColIndex] := JObj.GetValue<String>('Comment', '');
    Node.Values[clmTotalEarned.Position.ColIndex] := null;

    JValue := JObj.GetValue<TJSONArray>('Childs', nil);
    if JValue <> nil then
      SetJArrTreeNodes(JValue as TJSONArray, Node);

    if JObj.GetValue<Boolean>('AutoExpand', False) then
      Node.Expanded := True;
  end;
end;

procedure TfrmFinancialData.CommandShowNodeData(const aData: TValue);
var
  Node: TcxTreeListNode;
  USDCost: Variant;
  NodeData: TNodeData;
begin
  NodeData := aData.AsType<TNodeData>;

  if FRates.Date <> TDate(Now) then begin
    FRates.Free;
    FRates := GetCurrenciesRates;
  end;

  TreeListFinancial.BeginUpdate;
  try
    Node := GetNode(TreeListFinancial.Root, NodeData.Node);
    if Node = nil then
      Node := TreeListFinancial.Root.AddChild;

    Node.Values[clmTitle.Position.ColIndex] := NodeData.Node.Title;
    Node.Values[clmAddress.Position.ColIndex] := NodeData.Node.Address;
    Node.Values[clmPort.Position.ColIndex] := NodeData.Node.Port;
    Node.Values[clmPayoutDirty.Position.ColIndex] := NodeData.DirtyPayout;
    Node.Values[clmPayoutCleanly.Position.ColIndex] := NodeData.ClearlyPayout;
    Node.Values[clmEstimatedDirty.Position.ColIndex] := NodeData.EstimatedPayoutDirty;
    Node.Values[clmEstimatedCleanly.Position.ColIndex] := NodeData.EstimatedPayoutCleanly;
    Node.Values[clmUsedSpace.Position.ColIndex] := NodeData.DiskSpace.Used;
    Node.Values[clmEfficiencyByUsed.Position.ColIndex] := NodeData.EfficiencyByUsed;
    Node.Values[clmEfficiencyByTBM.Position.ColIndex] := NodeData.EfficiencyByTBM;
    Node.Values[clmTBM.Position.ColIndex] := NodeData.TBMonth;

    var Cost := Node.Values[clmCost.Position.ColIndex];
    var Currency := Node.Values[clmCurrency.Position.ColIndex];
    USDCost := null;
    if (VarToStr(Cost) <> '') and (VarToStr(Currency) <> '') and FRates.RatesToUSD.ContainsKey(Currency) then
      USDCost := FRates.RatesToUSD[Currency] * VarToCurrency(Cost);

    Node.Values[clmUSDCost.Position.ColIndex] := USDCost;

    if USDCost <> null then begin
      Node.Values[clmCleanProfit.Position.ColIndex] := NodeData.ClearlyPayout - USDCost;
      Node.Values[clmEstimatedCleanProfit.Position.ColIndex] := NodeData.EstimatedPayoutCleanly - USDCost;
    end else begin
      Node.Values[clmCleanProfit.Position.ColIndex] := null;
      Node.Values[clmEstimatedCleanProfit.Position.ColIndex] := null;
    end;

    RecalcParent(Node.Parent);
  finally
    TreeListFinancial.EndUpdate;
  end;
end;

procedure TfrmFinancialData.CommandShowNodeDataGroup(const aData: TValue);
begin
  var NodeDataArray := aData.AsType<TNodeDataArray>;

  if Length(NodeDataArray) = 0 then
    Exit;

  TreeListFinancial.BeginUpdate;
  try
    for var NodeData in NodeDataArray do
      CommandShowNodeData(TValue.From<TNodeData>(NodeData));
  finally
    TreeListFinancial.EndUpdate;
  end;
end;

function TfrmFinancialData.VarToCurrency(aValue: Variant): Currency;
var
  Str: String;
  FS: TFormatSettings;
begin
  Result := 0;

  Str := VarToStr(aValue);
  if Str = '' then
    Exit;

  Str := Str.Replace(',', '.');
  FS.DecimalSeparator := '.';
  Result := StrToCurr(Str, FS);
end;

end.
