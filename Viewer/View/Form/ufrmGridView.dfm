object frmGridView: TfrmGridView
  Left = 0
  Top = 0
  Caption = 'Grid View'
  ClientHeight = 543
  ClientWidth = 1274
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  TextHeight = 13
  object dxLayoutControl: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 1274
    Height = 543
    Align = alClient
    TabOrder = 0
    object gridNodeData: TcxGrid
      Left = 10
      Top = 42
      Width = 1254
      Height = 458
      TabOrder = 1
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'SevenClassic'
      object gridNodeDataDBTableView: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        FilterBox.Position = fpTop
        FilterBox.Visible = fvAlways
        FindPanel.DisplayMode = fpdmAlways
        ScrollbarAnnotations.Active = True
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = DM.dtsrcNodeData
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            FieldName = 'AllocatedSpace'
            Column = clmAllocatedSpace
          end
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            FieldName = 'UsedSpace'
            Column = clmUsedSpace
          end
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            FieldName = 'FreeSpace'
            Column = clmFreeSpace
          end
          item
            Kind = skSum
            FieldName = 'TBM'
            Column = clmTBM
          end
          item
            Format = ',0.00 $;-,0.00 $'
            Kind = skSum
            FieldName = 'PayoutDirty'
            Column = clmPayoutDirty
          end
          item
            Format = ',0.00 $;-,0.00 $'
            Kind = skSum
            FieldName = 'PayoutCleanly'
            Column = clmPayoutCleanly
          end
          item
            Format = ',0.00 $;-,0.00 $'
            Kind = skSum
            FieldName = 'EstimatedDirty'
            Column = clmEstimatedDirty
          end
          item
            Format = ',0.00 $;-,0.00 $'
            Kind = skSum
            FieldName = 'EstimatedCleanly'
            Column = clmEstimatedCleanly
          end
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            FieldName = 'Egress'
            Column = clmEgress
          end
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            FieldName = 'Ingress'
            Column = clmIngress
          end
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            FieldName = 'RepairEgress'
            Column = clmRepairEgress
          end
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            FieldName = 'RepairIngress'
            Column = clmRepairIngress
          end
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            FieldName = 'TotalBandwidth'
            Column = clmTotalBandwitdth
          end
          item
            Format = 'Count: ##'
            Kind = skCount
            FieldName = 'Title'
            Column = clmTitle
          end
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            Column = clmDailyEgress
          end
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            Column = clmDailyRepairEgress
          end
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            Column = clmDailyIngress
          end
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            Column = clmDailyRepairIngress
          end
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            Column = clmDailyTotalBandwidth
          end
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            FieldName = 'Trash'
            Column = clmTrash
          end
          item
            Kind = skSum
            FieldName = 'DailyTBMonth'
            Column = clmDailyTBM
          end>
        DataController.Summary.SummaryGroups = <>
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.Footer = True
        object clmTitle: TcxGridDBColumn
          DataBinding.FieldName = 'Title'
          Width = 36
        end
        object clmAddress: TcxGridDBColumn
          DataBinding.FieldName = 'Address'
          Width = 29
        end
        object clmPort: TcxGridDBColumn
          DataBinding.FieldName = 'Port'
          PropertiesClassName = 'TcxLabelProperties'
          Properties.Alignment.Horz = taCenter
          HeaderAlignmentHorz = taCenter
          Width = 29
        end
        object clmNodeID: TcxGridDBColumn
          DataBinding.FieldName = 'NodeID'
          Visible = False
          VisibleForExpressionEditor = bTrue
          VisibleForEditForm = bTrue
        end
        object clmEgress: TcxGridDBColumn
          DataBinding.FieldName = 'Egress'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Width = 28
        end
        object clmIngress: TcxGridDBColumn
          DataBinding.FieldName = 'Ingress'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Width = 29
        end
        object clmRepairIngress: TcxGridDBColumn
          Caption = 'Repair Ingress'
          DataBinding.FieldName = 'RepairIngress'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Width = 29
        end
        object clmRepairEgress: TcxGridDBColumn
          Caption = 'Repair Egress'
          DataBinding.FieldName = 'RepairEgress'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Width = 29
        end
        object clmTotalBandwitdth: TcxGridDBColumn
          Caption = 'Total bandwidth'
          DataBinding.FieldName = 'TotalBandwidth'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Width = 29
        end
        object clmTBM: TcxGridDBColumn
          Caption = 'Avg. TB*M'
          DataBinding.FieldName = 'TBM'
          PropertiesClassName = 'TcxLabelProperties'
          Properties.Alignment.Horz = taCenter
          OnCustomDrawCell = clmTBMCustomDrawCell
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Width = 28
        end
        object clmEfficiencyByUsed: TcxGridDBColumn
          Caption = 'Efficiency by Used'
          DataBinding.FieldName = 'Efficiency'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 $;-,0.00 $'
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 29
        end
        object clmAllocatedSpace: TcxGridDBColumn
          Caption = 'Allocated space'
          DataBinding.FieldName = 'AllocatedSpace'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = '### ##0 GB'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 29
        end
        object clmUsedSpace: TcxGridDBColumn
          Caption = 'Used space'
          DataBinding.FieldName = 'UsedSpace'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = '### ##0 GB'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 29
        end
        object clmTrash: TcxGridDBColumn
          DataBinding.FieldName = 'Trash'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = '### ##0 GB'
          Properties.ReadOnly = True
          HeaderAlignmentHorz = taCenter
          Width = 29
        end
        object clmFreeSpace: TcxGridDBColumn
          Caption = 'Free space'
          DataBinding.FieldName = 'FreeSpace'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = '### ##0 GB'
          OnCustomDrawCell = clmFreeSpaceCustomDrawCell
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 29
        end
        object clmPayoutDirty: TcxGridDBColumn
          Caption = 'Payout dirty'
          DataBinding.FieldName = 'PayoutDirty'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 $;-,0.00 $'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 28
        end
        object clmPayoutCleanly: TcxGridDBColumn
          Caption = 'Payout cleanly'
          DataBinding.FieldName = 'PayoutCleanly'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 $;-,0.00 $'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 29
        end
        object clmEstimatedDirty: TcxGridDBColumn
          Caption = 'Estimated dirty'
          DataBinding.FieldName = 'EstimatedDirty'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 $;-,0.00 $'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 29
        end
        object clmEstimatedCleanly: TcxGridDBColumn
          Caption = 'Estimated cleanly'
          DataBinding.FieldName = 'EstimatedCleanly'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 $;-,0.00 $'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 29
        end
        object clmHDDVolume: TcxGridDBColumn
          Caption = 'HDD Volume'
          DataBinding.FieldName = 'HDDVolume'
          PropertiesClassName = 'TcxLabelProperties'
          Properties.Alignment.Horz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 163
        end
        object clmVersion: TcxGridDBColumn
          DataBinding.FieldName = 'Version'
          PropertiesClassName = 'TcxLabelProperties'
          Properties.Alignment.Horz = taCenter
          Properties.Alignment.Vert = taVCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 387
        end
        object clmFullTreePath: TcxGridDBColumn
          Caption = 'Tree path'
          DataBinding.FieldName = 'FullPath'
          Visible = False
          BestFitMaxWidth = 100
          Options.Editing = False
          Width = 329
        end
        object clmDailyEgress: TcxGridDBColumn
          Caption = 'Daily Egress'
          DataBinding.FieldName = 'DailyEgress'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
          Visible = False
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object clmDailyRepairEgress: TcxGridDBColumn
          Caption = 'Daily Repair Egress'
          DataBinding.FieldName = 'DailyRepairEgress'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
          Visible = False
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object clmDailyIngress: TcxGridDBColumn
          Caption = 'Daily Ingress'
          DataBinding.FieldName = 'DailyIngress'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
          Visible = False
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object clmDailyRepairIngress: TcxGridDBColumn
          Caption = 'Daily Repair Ingress'
          DataBinding.FieldName = 'DailyRepairIngress'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
          Visible = False
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object clmDailyTotalBandwidth: TcxGridDBColumn
          Caption = 'Daily Total Bandwidth'
          DataBinding.FieldName = 'DailyTotalBandwidth'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
          Visible = False
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object clmUptime: TcxGridDBColumn
          Caption = 'Uptime'
          DataBinding.FieldName = 'UptimeMinutes'
          PropertiesClassName = 'TcxLabelProperties'
          Properties.Alignment.Horz = taCenter
          OnGetDataText = clmUptimeGetDataText
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Options.SortByDisplayText = isbtOff
        end
        object clmWallet: TcxGridDBColumn
          DataBinding.FieldName = 'Wallet'
          Visible = False
          Options.Editing = False
        end
        object clmDailyTBM: TcxGridDBColumn
          Caption = 'Daily TB*M'
          DataBinding.FieldName = 'DailyTBMonth'
          PropertiesClassName = 'TcxLabelProperties'
          Properties.Alignment.Horz = taCenter
          Visible = False
          OnCustomDrawCell = clmTBMCustomDrawCell
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object clmEfficiencyByTBM: TcxGridDBColumn
          Caption = 'Efficiency by TBM'
          DataBinding.FieldName = 'EfficiencyByTBM'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 $;-,0.00 $'
          Options.Editing = False
        end
      end
      object gridNodeDataLevel1: TcxGridLevel
        GridView = gridNodeDataDBTableView
      end
    end
    inline frameButtons: TframeButtons
      Left = 5
      Top = 0
      Width = 1264
      Height = 42
      VertScrollBar.Visible = False
      TabOrder = 0
      ExplicitLeft = 5
      ExplicitWidth = 1264
      inherited dxLayoutControl: TdxLayoutControl
        Width = 1264
        ExplicitWidth = 1264
        inherited btnSettings: TcxButton
          Left = 1242
          Top = 5
          ExplicitLeft = 1242
          ExplicitTop = 5
        end
        inherited btnChangeLog: TcxButton
          Left = 1206
          Top = 5
          ExplicitLeft = 1206
          ExplicitTop = 5
        end
        inherited btnShowGlobalLog: TcxButton
          Left = 1170
          Top = 5
          ExplicitLeft = 1170
          ExplicitTop = 5
        end
        inherited btnShowGridView: TcxButton
          Left = 257
          Top = 5
          ExplicitLeft = 257
          ExplicitTop = 5
        end
        inherited btnShowFinancialData: TcxButton
          Left = 221
          Top = 5
          ExplicitLeft = 221
          ExplicitTop = 5
        end
        inherited btnShowCheckNeighboursForm: TcxButton
          Left = 185
          Top = 5
          ExplicitLeft = 185
          ExplicitTop = 5
        end
        inherited btnShowDaily: TcxButton
          Left = 149
          Top = 5
          ExplicitLeft = 149
          ExplicitTop = 5
        end
        inherited btnResetSorting: TcxButton
          Left = 113
          Top = 5
          ExplicitLeft = 113
          ExplicitTop = 5
        end
        inherited btnRefresh: TcxButton
          Left = 41
          Top = 5
          ExplicitLeft = 41
          ExplicitTop = 5
        end
        inherited btnTreeSettings: TcxButton
          Left = 77
          Top = 5
          ExplicitLeft = 77
          ExplicitTop = 5
        end
        inherited lblDisqualified: TcxLabel
          Left = 861
          Top = 5
          ExplicitLeft = 861
          ExplicitTop = 5
          ExplicitHeight = 13
        end
        inherited lblSuspended: TcxLabel
          Left = 861
          Top = 24
          ExplicitLeft = 861
          ExplicitTop = 24
          ExplicitHeight = 13
        end
        inherited lblAuditRed: TcxLabel
          Left = 947
          Top = 5
          ExplicitLeft = 947
          ExplicitTop = 5
          ExplicitHeight = 13
        end
        inherited lblAuditYellow: TcxLabel
          Left = 947
          Top = 24
          ExplicitLeft = 947
          ExplicitTop = 24
          ExplicitHeight = 13
        end
        inherited btnShowHealthData: TcxButton
          Left = 293
          Top = 5
          ExplicitLeft = 293
          ExplicitTop = 5
        end
        inherited lblSuspensionRed: TcxLabel
          Left = 1084
          Top = 5
          ExplicitLeft = 1084
          ExplicitTop = 5
          ExplicitHeight = 13
        end
        inherited lblSuspensionYellow: TcxLabel
          Left = 1084
          Top = 24
          ExplicitLeft = 1084
          ExplicitTop = 24
        end
        inherited lblOnlineRed: TcxLabel
          Left = 1008
          Top = 5
          ExplicitLeft = 1008
          ExplicitTop = 5
          ExplicitHeight = 13
        end
        inherited lblOnlineYellow: TcxLabel
          Left = 1008
          Top = 24
          ExplicitLeft = 1008
          ExplicitTop = 24
          ExplicitHeight = 13
        end
        inherited lblWholeSNUsed: TcxLabel
          Left = 706
          Top = 5
          ExplicitLeft = 706
          ExplicitTop = 5
          ExplicitWidth = 149
          ExplicitHeight = 13
        end
        inherited btnExportData: TcxButton
          Left = 329
          Top = 5
          ExplicitLeft = 329
          ExplicitTop = 5
        end
        inherited btnShowNeighboursTree: TcxButton
          Left = 365
          Top = 5
          ExplicitLeft = 365
          ExplicitTop = 5
        end
        inherited btnShowDailyGridView: TcxButton
          Left = 401
          Top = 5
          ExplicitLeft = 401
          ExplicitTop = 5
        end
        inherited btnRebootPC: TcxButton
          Left = 437
          Top = 5
          ExplicitLeft = 437
          ExplicitTop = 5
        end
        inherited framePCRebootersStatus1: TframePCRebootersStatus
          Left = 653
          Top = 3
          Height = 34
          ExplicitLeft = 653
          ExplicitTop = 3
          ExplicitHeight = 34
          inherited pnl: TFlowPanel
            Height = 34
            StyleElements = [seFont, seClient, seBorder]
            ExplicitHeight = 34
          end
        end
        inherited btnOpenChart: TcxButton
          Left = 473
          Top = 5
          ExplicitLeft = 473
          ExplicitTop = 5
        end
        inherited btnRestartUsedDiffer: TcxButton
          Left = 509
          Top = 5
          ExplicitLeft = 509
          ExplicitTop = 5
        end
        inherited btnLightRefresh: TcxButton
          Left = 5
          Top = 5
          ExplicitLeft = 5
          ExplicitTop = 5
        end
        inherited btnOpenLocalHistory: TcxButton
          Left = 545
          Top = 5
          ExplicitLeft = 545
          ExplicitTop = 5
        end
        inherited btnDashboard: TcxButton
          Left = 581
          Top = 5
          ExplicitLeft = 581
          ExplicitTop = 5
        end
        inherited btnOpenSSHExecute: TcxButton
          Left = 617
          Top = 5
          ExplicitLeft = 617
          ExplicitTop = 5
        end
        inherited laylblDisqualified: TdxLayoutItem
          ControlOptions.OriginalHeight = 13
        end
        inherited laylblSuspended: TdxLayoutItem
          ControlOptions.OriginalHeight = 13
        end
        inherited laylblAuditRed: TdxLayoutItem
          ControlOptions.OriginalHeight = 13
        end
        inherited laylblAuditYellow: TdxLayoutItem
          ControlOptions.OriginalHeight = 13
        end
        inherited laylblSuspensionRed: TdxLayoutItem
          ControlOptions.OriginalHeight = 13
        end
        inherited laylblOnlineRed: TdxLayoutItem
          ControlOptions.OriginalHeight = 13
        end
        inherited laylblOnlineYellow: TdxLayoutItem
          ControlOptions.OriginalHeight = 13
        end
        inherited laylblWholeSNUsed: TdxLayoutItem
          ControlOptions.OriginalHeight = 13
          ControlOptions.OriginalWidth = 149
        end
      end
      inherited dxUIAdornerManager: TdxUIAdornerManager
        inherited badgeREDCount: TdxBadge
          Font.Height = -11
          Font.Name = 'Tahoma'
        end
      end
    end
    inline frameStatusBar: TframeStatusBar
      Left = 0
      Top = 500
      Width = 1274
      Height = 40
      TabOrder = 2
      ExplicitTop = 500
      ExplicitWidth = 1274
      inherited dxLayoutControl: TdxLayoutControl
        Width = 1274
        ExplicitWidth = 1274
        inherited StatusBar: TdxStatusBar
          Width = 1228
          ExplicitWidth = 1228
        end
        inherited dxLayoutControlGroup_Root: TdxLayoutGroup
          ItemIndex = 1
        end
      end
    end
    object dxLayoutControlGroup_Root: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Visible = False
      ButtonOptions.Visible = False
      Hidden = True
      ItemIndex = 2
      Padding.Top = -5
      Padding.AssignedValues = [lpavTop]
      ShowBorder = False
      UseIndent = False
      Index = -1
    end
    object layGrid: TdxLayoutItem
      Parent = dxLayoutControlGroup_Root
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Visible = False
      Control = gridNodeData
      ControlOptions.OriginalHeight = 511
      ControlOptions.OriginalWidth = 1034
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object layTopFrameButtons: TdxLayoutItem
      Parent = dxLayoutControlGroup_Root
      Padding.Left = -5
      Padding.Right = -5
      Padding.Top = -5
      Padding.AssignedValues = [lpavBottom, lpavLeft, lpavRight, lpavTop]
      Control = frameButtons
      ControlOptions.AutoColor = True
      ControlOptions.OriginalHeight = 42
      ControlOptions.OriginalWidth = 1082
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object layframeStatusBar: TdxLayoutItem
      Parent = dxLayoutControlGroup_Root
      Padding.Bottom = -7
      Padding.Left = -10
      Padding.Right = -10
      Padding.AssignedValues = [lpavBottom, lpavLeft, lpavRight, lpavTop]
      Control = frameStatusBar
      ControlOptions.AutoColor = True
      ControlOptions.OriginalHeight = 40
      ControlOptions.OriginalWidth = 1102
      ControlOptions.ShowBorder = False
      Index = 2
    end
  end
  object cxGridPopupMenu: TcxGridPopupMenu
    Grid = gridNodeData
    PopupMenus = <
      item
        GridView = gridNodeDataDBTableView
        HitTypes = [gvhtCell]
        Index = 0
        PopupMenu = popMenuForNode
      end>
    Left = 208
    Top = 224
  end
  object SaveDialogExcel: TSaveDialog
    DefaultExt = 'xlsx'
    Filter = 'Excel file|*.xlsx'
    Left = 688
    Top = 216
  end
  object popMenuForNode: TPopupMenu
    Left = 232
    Top = 320
    object popmenuitemOpenInBrowser: TMenuItem
      Caption = 'Open node in browser'
      OnClick = popmenuitemOpenInBrowserClick
    end
    object popmenuitemCheckNeighbours: TMenuItem
      Caption = 'Check neighbours'
      OnClick = popmenuitemCheckNeighboursClick
    end
    object menuItemDisplayUsedbysatellites: TMenuItem
      Caption = 'Display "Used" by satellites'
      Visible = False
      OnClick = menuItemDisplayUsedbysatellitesClick
    end
    object popmenuitemRefreshNode: TMenuItem
      Caption = 'Refresh node'
      OnClick = popmenuitemRefreshNodeClick
    end
    object ExecuteSSH1: TMenuItem
      Caption = 'Execute SSH'
      OnClick = ExecuteSSH1Click
    end
  end
end
