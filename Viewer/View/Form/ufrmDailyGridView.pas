unit ufrmDailyGridView;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBasic, dxSkiniMaginary, dxSkinSeven,
  dxSkinSevenClassic, cxClasses, dxLayoutContainer, dxLayoutControl, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  dxDateRanges, dxScrollbarAnnotations, Data.DB, cxDBData,
  cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid,
  uNotifyCenter, Storj.Consts, System.JSON, Storj.Types,
  System.Generics.Collections, dxBarBuiltInMenu, cxGridCustomPopupMenu,
  cxGridPopupMenu, cxLabel, cxCurrencyEdit, dxSkinOffice2013LightGray,
  dxSkinOffice2019Black, dxSkinOffice2019DarkGray, dxSkinOffice2019White,
  dxSkinWhiteprint, uDM, dxSkinWXI;

type
  TfrmDailyGridView = class(TForm)
    dxLayoutControlGroup_Root: TdxLayoutGroup;
    dxLayoutControl: TdxLayoutControl;
    gridDailyNodeDataDBTableView1: TcxGridDBTableView;
    gridDailyNodeDataLevel1: TcxGridLevel;
    gridDailyNodeData: TcxGrid;
    laygridDailyNodeData: TdxLayoutItem;
    clmTitle: TcxGridDBColumn;
    clmAddress: TcxGridDBColumn;
    clmPort: TcxGridDBColumn;
    clmEgress: TcxGridDBColumn;
    clmIngress: TcxGridDBColumn;
    clmRepairEgress: TcxGridDBColumn;
    clmRepairIngress: TcxGridDBColumn;
    clmTotalBandwidth: TcxGridDBColumn;
    cxGridPopupMenu: TcxGridPopupMenu;
    clmFreeSpace: TcxGridDBColumn;
    clmTBMonth: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    procedure CommandShow(const aData: TValue);
  public
    { Public declarations }
  end;

var
  frmDailyGridView: TfrmDailyGridView;

implementation

{$R *.dfm}

{ TfrmDailyGridView }

procedure TfrmDailyGridView.CommandShow(const aData: TValue);
begin
  Self.Show;
end;

procedure TfrmDailyGridView.FormCreate(Sender: TObject);
begin
  TNotifyCenter.Instance.Subscribe(COMMAND_SHOW_DAILY_DATA_GRID, CommandShow);
end;

procedure TfrmDailyGridView.FormDestroy(Sender: TObject);
begin
  TNotifyCenter.Instance.UnSubscribe(COMMAND_SHOW_DAILY_DATA_GRID, CommandShow);
end;

end.
