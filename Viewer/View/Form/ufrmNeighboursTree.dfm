object frmNeighboursTree: TfrmNeighboursTree
  Left = 0
  Top = 0
  Caption = 'Neighbours Tree'
  ClientHeight = 505
  ClientWidth = 799
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  TextHeight = 13
  object dxLayoutControl: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 799
    Height = 505
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 795
    ExplicitHeight = 504
    object treeNeightbours: TcxTreeList
      Left = 10
      Top = 48
      Width = 779
      Height = 447
      Bands = <
        item
        end>
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'SevenClassic'
      Navigator.Buttons.CustomButtons = <>
      OptionsBehavior.ChangeDelay = 1000
      OptionsView.ColumnAutoWidth = True
      ScrollbarAnnotations.CustomAnnotations = <>
      TabOrder = 3
      OnCustomDrawDataCell = treeNeightboursCustomDrawDataCell
      object clmTitle: TcxTreeListColumn
        Caption.Text = 'Title'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 0
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmAddress: TcxTreeListColumn
        Caption.Text = 'Address'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 1
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmPort: TcxTreeListColumn
        PropertiesClassName = 'TcxLabelProperties'
        Properties.Alignment.Horz = taCenter
        Properties.Alignment.Vert = taVCenter
        Caption.AlignHorz = taCenter
        Caption.Text = 'Port'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 2
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmNeighbours: TcxTreeListColumn
        PropertiesClassName = 'TcxLabelProperties'
        Properties.Alignment.Horz = taCenter
        Properties.Alignment.Vert = taVCenter
        Caption.AlignHorz = taCenter
        Caption.Text = 'Subnet nodes count'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 3
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmOwnNeighbours: TcxTreeListColumn
        Caption.Text = 'Own neightbours'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 4
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmSubnet: TcxTreeListColumn
        Visible = False
        Caption.Text = 'Subnet'
        Width = 100
        Position.ColIndex = 5
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmOwnNeighboursCount: TcxTreeListColumn
        Visible = False
        Width = 100
        Position.ColIndex = 6
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmComment: TcxTreeListColumn
        Caption.Text = 'Comment'
        Width = 100
        Position.ColIndex = 7
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
    object btnRefresh: TcxButton
      Left = 10
      Top = 10
      Width = 30
      Height = 30
      Caption = 'btnRefresh'
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'iMaginary'
      OptionsImage.Glyph.SourceDPI = 96
      OptionsImage.Glyph.SourceHeight = 25
      OptionsImage.Glyph.SourceWidth = 25
      OptionsImage.Glyph.Data = {
        3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D225554
        462D38223F3E0D0A3C7376672076657273696F6E3D22312E31222069643D22D0
        A1D0BBD0BED0B95F312220786D6C6E733D22687474703A2F2F7777772E77332E
        6F72672F323030302F7376672220786D6C6E733A786C696E6B3D22687474703A
        2F2F7777772E77332E6F72672F313939392F786C696E6B2220783D2230707822
        20793D22307078222076696577426F783D223020302033322033322220737479
        6C653D22656E61626C652D6261636B67726F756E643A6E657720302030203332
        2033323B2220786D6C3A73706163653D227072657365727665223E262331333B
        262331303B3C7374796C6520747970653D22746578742F637373223E2E477265
        656E7B66696C6C3A233033394332333B7D3C2F7374796C653E0D0A3C67206964
        3D22D0A1D0BBD0BED0B95F32223E0D0A09093C706174682069643D2252656672
        6573682220636C6173733D22477265656E2220643D224D33302C313456346C2D
        332E322C332E324332342E332C342C32302E342C322C31362C3243382E332C32
        2C322C382E332C322C313673362E332C31342C31342C313463372E322C302C31
        332E322D352E352C31332E392D31322E3520202623393B2623393B6C2D342D30
        2E35632D302E352C352E312D342E382C392D392E392C39632D352E352C302D31
        302D342E352D31302D313063302D352E352C342E352D31302C31302D31306333
        2E332C302C362E322C312E362C382C346C2D342C344833307A222F3E0D0A093C
        2F673E0D0A3C2F7376673E0D0A}
      PaintStyle = bpsGlyph
      TabOrder = 0
      OnClick = btnRefreshClick
    end
    object txtCommonInfo: TcxLabel
      Left = 46
      Top = 29
      Caption = 
        'Neighbours info may be updated no more often than once every 2 h' +
        'ours'
      Style.HotTrack = False
      Style.TransparentBorder = False
      Transparent = True
    end
    object txtLastUpdateInfo: TcxLabel
      Left = 46
      Top = 10
      Caption = 'Last update: no data'
      Style.HotTrack = False
      Style.TransparentBorder = False
      Transparent = True
    end
    object dxLayoutControlGroup_Root: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avClient
      Hidden = True
      ShowBorder = False
      Index = -1
    end
    object laygrpTopToolbar: TdxLayoutGroup
      Parent = dxLayoutControlGroup_Root
      CaptionOptions.Text = 'New Group'
      ItemIndex = 1
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 0
    end
    object layNeighboursTree: TdxLayoutItem
      Parent = dxLayoutControlGroup_Root
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'cxTreeList1'
      CaptionOptions.Visible = False
      Control = treeNeightbours
      ControlOptions.OriginalHeight = 150
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object laybtnRefresh: TdxLayoutItem
      Parent = laygrpTopToolbar
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnRefresh
      ControlOptions.OriginalHeight = 30
      ControlOptions.OriginalWidth = 30
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object laytxtCommonInfo: TdxLayoutItem
      Parent = laygrpTextInfo
      SizeOptions.Width = 500
      CaptionOptions.Text = 'cxLabel1'
      CaptionOptions.Visible = False
      Control = txtCommonInfo
      ControlOptions.OriginalHeight = 13
      ControlOptions.OriginalWidth = 46
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object laytxtLastUpdateInfo: TdxLayoutItem
      Parent = laygrpTextInfo
      SizeOptions.Width = 500
      CaptionOptions.Text = 'cxLabel2'
      CaptionOptions.Visible = False
      Control = txtLastUpdateInfo
      ControlOptions.OriginalHeight = 13
      ControlOptions.OriginalWidth = 46
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object laygrpTextInfo: TdxLayoutGroup
      Parent = laygrpTopToolbar
      CaptionOptions.Text = 'New Group'
      ItemIndex = 1
      ShowBorder = False
      Index = 1
    end
  end
end
