unit ufrmDailyDataTree;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters, cxClasses,
  dxLayoutContainer, dxLayoutControl, cxCustomData, cxStyles, cxTL,
  cxTLdxBarBuiltInMenu, cxInplaceContainer, cxTextEdit,
  Storj.Types, System.JSON, cxCurrencyEdit, System.Generics.Collections,
  dxSkinBasic, dxCore,
  dxSkiniMaginary,
  dxSkinSeven,
  dxSkinSevenClassic,
  dxScrollbarAnnotations, cxCheckBox, cxDropDownEdit,
  System.StrUtils, Storj.Consts, uNotifyCenter, dxSkinWXI, cxFilter, cxLabel;

type
  TfrmDailyDataTree = class(TForm)
    dxLayoutControl1Group_Root: TdxLayoutGroup;
    dxLayoutControl1: TdxLayoutControl;
    TreeListDaily: TcxTreeList;
    layTreeListDaily: TdxLayoutItem;
    clmTitle: TcxTreeListColumn;
    clmAddress: TcxTreeListColumn;
    clmPort: TcxTreeListColumn;
    clmSatellite: TcxTreeListColumn;
    clmEgress: TcxTreeListColumn;
    clmIngress: TcxTreeListColumn;
    clmRepairEgress: TcxTreeListColumn;
    clmRepairIngress: TcxTreeListColumn;
    clmTotalBandwidth: TcxTreeListColumn;
    clmDailyTBMonth: TcxTreeListColumn;
    procedure TreeListDailyCompare(Sender: TcxCustomTreeList; ANode1,
      ANode2: TcxTreeListNode; var ACompare: Integer);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    procedure SetJArrTreeNodes(AJArr: TJSONArray; AParentNode: TcxTreeListNode);
    procedure RecalcParent(AParentNode: TcxTreeListNode);

    procedure ShowCommand(const aData: TValue);
    procedure CommandShowNodeData(const aData: TValue);
    procedure CommandShowNodeDataGroup(const aData: TValue);
    procedure CommandLoadFromJSON(const aData: TValue);
  public
  end;

var
  frmDailyDataTree: TfrmDailyDataTree;

implementation

{$R *.dfm}

uses
  uDM;

{ TfrmDailyDataTree }

procedure TfrmDailyDataTree.FormCreate(Sender: TObject);
begin
  TNotifyCenter.Instance.Subscribe(COMMAND_SHOW_DAILY_DATA_TREE, ShowCommand);
  TNotifyCenter.Instance.Subscribe(COMMAND_SHOW_NODE_DATA, CommandShowNodeData);
  TNotifyCenter.Instance.Subscribe(COMMAND_SHOW_NODE_DATA_GROUP, CommandShowNodeDataGroup);
  TNotifyCenter.Instance.Subscribe(COMMAND_LOAD_FROM_JSON, CommandLoadFromJSON);
end;

procedure TfrmDailyDataTree.FormDestroy(Sender: TObject);
begin
  TNotifyCenter.Instance.UnSubscribe(COMMAND_SHOW_DAILY_DATA_TREE, ShowCommand);
  TNotifyCenter.Instance.UnSubscribe(COMMAND_SHOW_NODE_DATA, CommandShowNodeData);
  TNotifyCenter.Instance.UnSubscribe(COMMAND_SHOW_NODE_DATA_GROUP, CommandShowNodeDataGroup);
  TNotifyCenter.Instance.UnSubscribe(COMMAND_LOAD_FROM_JSON, CommandLoadFromJSON);
end;

procedure TfrmDailyDataTree.CommandLoadFromJSON(const aData: TValue);
begin
  var JArr := AData.AsObject as TJSONArray;

  if not Assigned(JArr) then
    Exit;

  TreeListDaily.BeginUpdate;
  try
    TreeListDaily.Clear;
    SetJArrTreeNodes(JArr, TreeListDaily.Root);
  finally
    TreeListDaily.EndUpdate;
  end;
end;

procedure TfrmDailyDataTree.SetJArrTreeNodes(AJArr: TJSONArray; AParentNode: TcxTreeListNode);
var
  i: integer;
  JObj: TJSONObject;
  JValue: TJSONValue;
  Node: TcxTreeListNode;
begin
  for i := 0 to AJarr.Count - 1 do begin
    JObj := AJArr.Items[i] as TJSONObject;
    if not JObj.GetValue<Boolean>('IsActive') then
      Continue;

    Node := AParentNode.AddChild;
    Node.Values[clmTitle.Position.ColIndex] := JObj.GetValue<String>('Title');
    Node.Values[clmAddress.Position.ColIndex] := JObj.GetValue<String>('Address');
    Node.Values[clmPort.Position.ColIndex] := JObj.GetValue<String>('Port');

    JValue := JObj.GetValue<TJSONArray>('Childs', nil);
    if JValue <> nil then
      SetJArrTreeNodes(JValue as TJSONArray, Node);

    if JObj.GetValue<Boolean>('AutoExpand', False) then
      Node.Expanded := True;
  end;
end;

procedure TfrmDailyDataTree.ShowCommand(const aData: TValue);
begin
  Self.Show;
end;

procedure TfrmDailyDataTree.CommandShowNodeData(const aData: TValue);
  function GetNode(AParentNode: TcxTreeListNode; ANode: TNode): TcxTreeListNode;
  var
    i: integer;
  begin
    for i := 0 to AParentNode.Count - 1 do begin
      Result := AParentNode.Items[i];
      if (Result.Values[clmAddress.Position.ColIndex] = ANode.Address) and
         (Result.Values[clmPort.Position.ColIndex] = ANode.Port) then
        Exit(Result);

      if Result.HasChildren then
        Result := GetNode(Result, ANode)
      else
        Result := nil;
      if Result <> nil then
        Exit(Result);
    end;

    Result := nil;
  end;

var
  Satellite: TSatellite;
  Node, SatNode: TcxTreeListNode;
  NodeData: TNodeData;
begin
  NodeData := aData.AsType<TNodeData>;

  TreeListDaily.BeginUpdate;
  try
    Node := GetNode(TreeListDaily.Root, NodeData.Node);
    if Node = nil then
      Node := TreeListDaily.Root.AddChild;

    Node.Values[clmTitle.Position.ColIndex] := NodeData.Node.Title;
    Node.Values[clmAddress.Position.ColIndex] := NodeData.Node.Address;
    Node.Values[clmPort.Position.ColIndex] := NodeData.Node.Port;
    Node.Values[clmEgress.Position.ColIndex] := NodeData.DailyData.Egress;
    Node.Values[clmIngress.Position.ColIndex] := NodeData.DailyData.Ingress;
    Node.Values[clmRepairEgress.Position.ColIndex] := NodeData.DailyData.RepairEgress;
    Node.Values[clmRepairIngress.Position.ColIndex] := NodeData.DailyData.RepairIngress;
    Node.Values[clmTotalBandwidth.Position.ColIndex] := NodeData.DailyData.TotalBandwidth;
    Node.Values[clmDailyTBMonth.Position.ColIndex] := NodeData.DailyData.TBMonth;
    Node.DeleteChildren;
    for Satellite in NodeData.DailyData.Satellites do begin
      SatNode := Node.AddChild;
      SatNode.Values[clmSatellite.Position.ColIndex] := Satellite.Title;
      SatNode.Values[clmEgress.Position.ColIndex] := Satellite.Egress;
      SatNode.Values[clmIngress.Position.ColIndex] := Satellite.Ingress;
      SatNode.Values[clmRepairEgress.Position.ColIndex] := Satellite.RepairEgress;
      SatNode.Values[clmRepairIngress.Position.ColIndex] := Satellite.RepairIngress;
      SatNode.Values[clmTotalBandwidth.Position.ColIndex] := Satellite.TotalBandwidth;
      SatNode.Values[clmDailyTBMonth.Position.ColIndex] := Satellite.TBMonth;
    end;

    RecalcParent(Node.Parent);
  finally
    TreeListDaily.EndUpdate;
  end;
end;

procedure TfrmDailyDataTree.CommandShowNodeDataGroup(const aData: TValue);
begin
  var NodeDataArray := aData.AsType<TNodeDataArray>;

  if Length(NodeDataArray) = 0 then
    Exit;

  TreeListDaily.BeginUpdate;
  try
    for var NodeData in NodeDataArray do
      CommandShowNodeData(TValue.From<TNodeData>(NodeData));
  finally
    TreeListDaily.EndUpdate;
  end;
end;

procedure TfrmDailyDataTree.TreeListDailyCompare(Sender: TcxCustomTreeList;
  ANode1, ANode2: TcxTreeListNode; var ACompare: Integer);
var
  V1, V2: Variant;
  Column: TcxTreeListColumn;
begin
  Column := TreeListDaily.HitTest.HitColumn;

  if Column = nil then
    Exit;

  ACompare := 0;
  if MatchText(Column.Name, ['clmPort', 'clmEgress', 'clmIngress', 'clmRepairIngress', 'clmRepairEgress',
                             'clmTotalBandwidth']) then begin
    if (ANode1.Values[Column.Position.ColIndex] <> null) then
      V1 := ANode1.Values[Column.Position.ColIndex]
    else
      V1 := 0;

    if (ANode2.Values[Column.Position.ColIndex] <> null) then
      V2 := ANode2.Values[Column.Position.ColIndex]
    else
      V2 := 0;

    if VarToStr(V1).Trim.Length = 0 then
      V1 := 0;

    if VarToStr(V2).Trim.Length = 0 then
      V2 := 0;

    var D1: Double := V1;
    var D2: Double := V2;
    if D1 > D2 then
      ACompare := 1
    else if D1 < D2 then
      ACompare := -1;
  end else if MatchText(Column.Name, ['clmTitle', 'clmAddress', 'clmSatellite']) then begin
    if (ANode1.Values[Column.Position.ColIndex] <> null) then
      V1 := ANode1.Values[Column.Position.ColIndex]
    else
      V1 := '';

    if (ANode2.Values[Column.Position.ColIndex] <> null) then
      V2 := ANode2.Values[Column.Position.ColIndex]
    else
      V2 := '';

    ACompare := CompareText(V1, V2);
  end;

  if Column.SortOrder = TdxSortOrder.soDescending then
    ACompare := -ACompare;
end;

procedure TfrmDailyDataTree.RecalcParent(AParentNode: TcxTreeListNode);
var
  i: integer;
  Egress, RepairEgress, Ingress, RepairIngress, TotalBandwidth, TBMonth: Double;
begin
  if TreeListDaily.Root.Equals(AParentNode) then
    Exit;

  Egress := 0; RepairEgress := 0;
  Ingress := 0; RepairIngress := 0;
  TotalBandwidth := 0; TBMonth := 0;
  for i := 0 to AParentNode.Count - 1 do begin
    if AParentNode.Items[i].Values[clmEgress.Position.ColIndex] <> null then
      Egress := Egress + AParentNode.Items[i].Values[clmEgress.Position.ColIndex];

    if AParentNode.Items[i].Values[clmIngress.Position.ColIndex] <> null then
      Ingress := Ingress + AParentNode.Items[i].Values[clmIngress.Position.ColIndex];

    if AParentNode.Items[i].Values[clmRepairEgress.Position.ColIndex] <> null then
      RepairEgress := RepairEgress + AParentNode.Items[i].Values[clmRepairEgress.Position.ColIndex];

    if AParentNode.Items[i].Values[clmRepairIngress.Position.ColIndex] <> null then
      RepairIngress := RepairIngress + AParentNode.Items[i].Values[clmRepairIngress.Position.ColIndex];

    if AParentNode.Items[i].Values[clmTotalBandwidth.Position.ColIndex] <> null then
      TotalBandwidth := TotalBandwidth + AParentNode.Items[i].Values[clmTotalBandwidth.Position.ColIndex];

    if AParentNode.Items[i].Values[clmDailyTBMonth.Position.ColIndex] <> null then
      TBMonth := TBMonth + AParentNode.Items[i].Values[clmDailyTBMonth.Position.ColIndex];
  end;

  AParentNode.Values[clmEgress.Position.ColIndex] := Egress;
  AParentNode.Values[clmIngress.Position.ColIndex] := Ingress;
  AParentNode.Values[clmRepairEgress.Position.ColIndex] := RepairEgress;
  AParentNode.Values[clmRepairIngress.Position.ColIndex] := RepairIngress;
  AParentNode.Values[clmTotalBandwidth.Position.ColIndex] := TotalBandwidth;
  AParentNode.Values[clmDailyTBMonth.Position.ColIndex] := TBMonth;

  RecalcParent(AParentNode.Parent);
end;

end.
