unit ufrmGridView;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBasic, dxSkiniMaginary, dxSkinSeven,
  dxSkinSevenClassic, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, dxDateRanges, dxScrollbarAnnotations, Data.DB, cxDBData,
  cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid,
  Storj.Types, System.JSON, System.Generics.Collections,
  cxCurrencyEdit, cxLabel, dxBarBuiltInMenu, cxGridCustomPopupMenu,
  cxGridPopupMenu, dxLayoutContainer, dxLayoutControl, uframeButtons,
  uNotifyCenter, uframeStatusBar, cxGridExportLink, System.UITypes, Vcl.Menus,
  dxSkinOffice2013LightGray, dxSkinOffice2019DarkGray, dxSkinOffice2019White,
  cxDataControllerConditionalFormattingRulesManagerDialog, dxSkinWXI,
  dxSkinOffice2019Black, dxSkinWhiteprint;

type
  TfrmGridView = class(TForm)
    cxGridPopupMenu: TcxGridPopupMenu;
    dxLayoutControlGroup_Root: TdxLayoutGroup;
    dxLayoutControl: TdxLayoutControl;
    layGrid: TdxLayoutItem;
    gridNodeData: TcxGrid;
    gridNodeDataDBTableView: TcxGridDBTableView;
    clmTitle: TcxGridDBColumn;
    clmAddress: TcxGridDBColumn;
    clmPort: TcxGridDBColumn;
    clmEgress: TcxGridDBColumn;
    clmIngress: TcxGridDBColumn;
    clmRepairIngress: TcxGridDBColumn;
    clmRepairEgress: TcxGridDBColumn;
    clmTotalBandwitdth: TcxGridDBColumn;
    clmTBM: TcxGridDBColumn;
    clmAllocatedSpace: TcxGridDBColumn;
    clmUsedSpace: TcxGridDBColumn;
    clmFreeSpace: TcxGridDBColumn;
    clmPayoutDirty: TcxGridDBColumn;
    clmPayoutCleanly: TcxGridDBColumn;
    clmEstimatedDirty: TcxGridDBColumn;
    clmEstimatedCleanly: TcxGridDBColumn;
    clmVersion: TcxGridDBColumn;
    gridNodeDataLevel1: TcxGridLevel;
    clmEfficiencyByUsed: TcxGridDBColumn;
    clmHDDVolume: TcxGridDBColumn;
    clmNodeID: TcxGridDBColumn;
    layTopFrameButtons: TdxLayoutItem;
    frameButtons: TframeButtons;
    layframeStatusBar: TdxLayoutItem;
    frameStatusBar: TframeStatusBar;
    SaveDialogExcel: TSaveDialog;
    clmTrash: TcxGridDBColumn;
    clmFullTreePath: TcxGridDBColumn;
    popMenuForNode: TPopupMenu;
    popmenuitemOpenInBrowser: TMenuItem;
    popmenuitemCheckNeighbours: TMenuItem;
    popmenuitemRefreshNode: TMenuItem;
    menuItemDisplayUsedbysatellites: TMenuItem;
    clmDailyEgress: TcxGridDBColumn;
    clmDailyRepairEgress: TcxGridDBColumn;
    clmDailyIngress: TcxGridDBColumn;
    clmDailyRepairIngress: TcxGridDBColumn;
    clmDailyTotalBandwidth: TcxGridDBColumn;
    clmUptime: TcxGridDBColumn;
    clmWallet: TcxGridDBColumn;
    clmDailyTBM: TcxGridDBColumn;
    clmEfficiencyByTBM: TcxGridDBColumn;
    ExecuteSSH1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure clmFreeSpaceCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);

    procedure LoadSettingsFromJSON;
    procedure SaveŅolumnToJSON;
    procedure popmenuitemOpenInBrowserClick(Sender: TObject);
    procedure popmenuitemCheckNeighboursClick(Sender: TObject);
    procedure popmenuitemRefreshNodeClick(Sender: TObject);
    procedure menuItemDisplayUsedbysatellitesClick(Sender: TObject);
    procedure clmUptimeGetDataText(Sender: TcxCustomGridTableItem;
      ARecordIndex: Integer; var AText: string);
    procedure clmTBMCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure ExecuteSSH1Click(Sender: TObject);
  private
    procedure CommandShow(const aData: TValue);
    procedure CommandRepaintNodes(const aData: TValue);
    procedure CommandExportData(const aData: TValue);
  public
  end;

var
  frmGridView: TfrmGridView;

implementation

{$R *.dfm}

uses
  uDM, Storj.Settings, Storj.Consts, uVersionGetter, Storj.Common;

{ TfrmGridView }

procedure TfrmGridView.clmFreeSpaceCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  FreeSpace: Variant;
begin
  if Assigned(AViewInfo) and GetSettings.IsColoredFreeSpace then
  begin
    FreeSpace := AViewInfo.Value;
    if FreeSpace = null then
      FreeSpace := 0;

    var color := $d4f9d4;
    if FreeSpace < GetSettings.RedColorValue then
      color := RGB(255, 236, 230)
    else if FreeSpace < GetSettings.YellowColorValue then
      color := RGB(255, 246, 189);

     ACanvas.Brush.Color := color;
   end;
end;

procedure TfrmGridView.SaveŅolumnToJSON;
var
  StrList: TStringList;
  JSON, RootJSON: TJSONObject;
  JArr: TJSONArray;
  i: integer;
begin
  var Path := ExtractFilePath(Application.ExeName);

  RootJSON := TJSONObject.Create;
  try
    JArr := TJSONArray.Create;
    for i := 0 to gridNodeDataDBTableView.ColumnCount - 1 do begin
      JSON := TJSONObject.Create;
      JSON.AddPair('FieldName', gridNodeDataDBTableView.Columns[i].DataBinding.FieldName);
      JSON.AddPair('ColumnVisible', TJSONBool.Create(gridNodeDataDBTableView.Columns[i].Visible));
      JSON.AddPair('ColumnWidth', TJSONNumber.Create(gridNodeDataDBTableView.Columns[i].Width));
      JSON.AddPair('ColumnIndex', TJSONNumber.Create(gridNodeDataDBTableView.Columns[i].Index));

      JArr.AddElement(JSON);
    end;

    RootJSON.AddPair('MainGrid', JArr);

    StrList := TStringList.Create;
    try
      StrList.Text := RootJSON.ToString;
      StrList.SaveToFile(Path + 'GridViewSettings.json');
    finally
      StrList.Free;
    end;
  finally
    RootJSON.Free;
  end;
end;

procedure TfrmGridView.LoadSettingsFromJSON;
var
  StrList: TStringList;
  RootJSON, JSON: TJSONObject;
  JArr: TJSONArray;
  i: integer;
  Path: String;
  Column: TcxGridDBColumn;
begin
  Path := ExtractFilePath(Application.ExeName);

  if not FileExists(Path + 'GridViewSettings.json') then
    Exit;

  StrList := TStringList.Create;
  try
    StrList.LoadFromFile(Path + 'GridViewSettings.json');

    RootJSON := TJSONObject.ParseJSONValue(StrList.Text) as TJSONObject;
    try
      JArr := RootJSON.Values['MainGrid'] as TJSONArray;
      for i := 0 to JArr.Count - 1 do begin
        JSON := JArr.Items[i] as TJSONObject;

        Column := gridNodeDataDBTableView.GetColumnByFieldName(JSON.Values['FieldName'].Value);
        if Column <> nil then begin
          Column.Visible := JSON.GetValue<Boolean>('ColumnVisible');
          Column.Width := JSON.GetValue<Integer>('ColumnWidth');
          Column.Index := JSON.GetValue<Integer>('ColumnIndex');
        end;
      end;
    finally
      RootJSON.Free;
    end;
  finally
    StrList.Free;
  end;
end;

procedure TfrmGridView.menuItemDisplayUsedbysatellitesClick(Sender: TObject);
//var
//  Column: TcxGridDBColumn;
//  i: integer;
begin
//  TMenuItem(Sender).Checked := not TMenuItem(Sender).Checked;
//
//  if TMenuItem(Sender).Checked then begin
//    for i := 0 to DM.dtsrcNodeData.DataSet.FieldDefList.Count - 1 do
//      if DM.dtsrcNodeData.DataSet.FieldDefList[i].Name.StartsWith(DM.SATELLITE_COLUMN_NAME_PREFIX) then begin
//        Column := gridNodeDataDBTableView.CreateColumn;
//        Column.DataBinding.FieldName := DM.dtsrcNodeData.DataSet.FieldDefList[i].Name;
//        Column.Caption := DM.dtsrcNodeData.DataSet.FieldDefList[i].Name.Replace(DM.SATELLITE_COLUMN_NAME_PREFIX, '');
//        Column.PropertiesClass := TcxCurrencyEditProperties;
//        TcxCurrencyEditProperties(Column.Properties).DisplayFormat := '### ##0 GB';
//        TcxCurrencyEditProperties(Column.Properties).Alignment.Horz := TAlignment.taCenter;
//
//        Column.Summary.FooterKind := skSum;
//        Column.Summary.FooterFormat := '### ##0 GB';
//      end;
//  end else begin
//    i := 0;
//    while i < gridNodeDataDBTableView.ColumnCount do
//      if gridNodeDataDBTableView.Columns[i].DataBinding.FieldName.StartsWith(DM.SATELLITE_COLUMN_NAME_PREFIX) then
//        gridNodeDataDBTableView.Columns[i].Destroy
//      else
//        i := i + 1;
//  end;
end;

procedure TfrmGridView.clmTBMCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
var
  TBM, UsedSpace: Variant;
begin
  if Assigned(AViewInfo) and GetSettings.IsColoredTBMDeviation then
  begin
    TBM := AViewInfo.Value;
    if TBM = null then
      TBM := 0;

    UsedSpace := AViewInfo.GridRecord.Values[clmUsedSpace.Index];
    if UsedSpace = null then
      UsedSpace := 0;

    if UsedSpace > 0 then begin
      var Ratio: Double := (TBM * 1000) / UsedSpace * 100;

      var IsDeviationTresholdedForYellow := Ratio < (100 - GetSettings.LessThanDeviationPercent);
      var IsDeviationTresholdedForRed := Ratio > (100 + GetSettings.MoreThanDeviationPercent);

      var color := $d4f9d4;
      if IsDeviationTresholdedForRed then
        color := RGB(255, 236, 230)
      else if IsDeviationTresholdedForYellow then
        color := RGB(255, 246, 189);

      ACanvas.Brush.Color := color;
    end;
   end;
end;

procedure TfrmGridView.clmUptimeGetDataText(Sender: TcxCustomGridTableItem; ARecordIndex: Integer; var AText: string);
var
  AView: TcxGridDBTableView;
  UptimeMinutes: Integer;
begin
  AView := Sender.GridView as TcxGridDBTableView;
  if VarIsNull(AView.DataController.Values[ARecordIndex, clmUptime.Index]) then
    UptimeMinutes := 0
  else
    UptimeMinutes := AView.DataController.Values[ARecordIndex, clmUptime.Index];

  AText := UptimeMinutesToStr(UptimeMinutes);
end;

procedure TfrmGridView.CommandExportData(const aData: TValue);
begin
  if SaveDialogExcel.Execute then begin
    ExportGridToXLSX(SaveDialogExcel.FileName, gridNodeData);
    MessageDlg('Grid node data successfully saved into xlsx file', mtInformation, [mbOk], 0);
  end;
end;

procedure TfrmGridView.CommandRepaintNodes(const aData: TValue);
begin
  gridNodeDataDBTableView.ViewChanged;
end;

procedure TfrmGridView.CommandShow(const aData: TValue);
begin
  Self.Show;
end;

procedure TfrmGridView.ExecuteSSH1Click(Sender: TObject);
begin
  var FullPath := DM.dtsrcNodeData.DataSet.FieldByName('FullPath').AsString;

  TNotifyCenter.Instance.SendMessage(COMMAND_SHOW_SSH_COMMAND_EXECUTE, FullPath);
end;

procedure TfrmGridView.FormCreate(Sender: TObject);
begin
  LoadSettingsFromJSON;

  TNotifyCenter.Instance.Subscribe(COMMAND_SHOW_GRID_VIEW, CommandShow);
  TNotifyCenter.Instance.Subscribe(COMMAND_SETTING_CHANGED, CommandRepaintNodes);
  TNotifyCenter.Instance.Subscribe(COMMAND_EXPORT_DATA, CommandExportData);
end;

procedure TfrmGridView.FormDestroy(Sender: TObject);
begin
  SaveŅolumnToJSON;

  TNotifyCenter.Instance.UnSubscribe(COMMAND_SHOW_GRID_VIEW, CommandShow);
  TNotifyCenter.Instance.UnSubscribe(COMMAND_SETTING_CHANGED, CommandRepaintNodes);
  TNotifyCenter.Instance.UnSubscribe(COMMAND_EXPORT_DATA, CommandExportData);
end;

procedure TfrmGridView.popmenuitemCheckNeighboursClick(Sender: TObject);
begin
  var Addr := DM.dtsrcNodeData.DataSet.FieldByName('Address').AsString;

  if Addr.IsEmpty then begin
    MessageDlg('Please, select your node with address', mtError, [mbOk], 0);
    Exit;
  end;

  TNotifyCenter.Instance.SendMessage(COMMAND_SHOW_CHECK_NEIGHBOURS, Addr);
end;

procedure TfrmGridView.popmenuitemOpenInBrowserClick(Sender: TObject);
var
  Address, Port: String;
begin
  Address := DM.dtsrcNodeData.DataSet.FieldByName('Address').AsString;
  Port := DM.dtsrcNodeData.DataSet.FieldByName('Port').AsString;

  if Address.IsEmpty or Port.IsEmpty then begin
    MessageDlg('Please, select your node with address and port', mtError, [mbOk], 0);
    Exit;
  end;

  OpenURL('http://' + Address + ':' + Port);
end;

procedure TfrmGridView.popmenuitemRefreshNodeClick(Sender: TObject);
var
  Node: TNode;
begin
  var Addr := DM.dtsrcNodeData.DataSet.FieldByName('Address').AsString;

  if Addr.IsEmpty then begin
    MessageDlg('Please, select your node with address', mtError, [mbOk], 0);
    Exit;
  end;

  Node.Address := Addr;
  Node.Title := DM.dtsrcNodeData.DataSet.FieldByName('Title').AsString;
  Node.Port := DM.dtsrcNodeData.DataSet.FieldByName('Port').AsInteger;
  Node.IsUseInAdditionalStats := false;

  DM.LoadNodeData(Node);
end;

end.
