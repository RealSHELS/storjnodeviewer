object frmDashboard: TfrmDashboard
  Left = 0
  Top = 0
  Caption = 'Dashboard'
  ClientHeight = 442
  ClientWidth = 649
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  TextHeight = 15
  object dxLayoutControl: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 649
    Height = 442
    Align = alClient
    TabOrder = 0
    ExplicitLeft = 248
    ExplicitTop = 128
    ExplicitWidth = 300
    ExplicitHeight = 250
    object chartSpace: TdxChartControl
      Left = 12
      Top = 12
      Width = 625
      Height = 250
      Titles = <>
      object chartSpaceSimpleDiagram: TdxChartSimpleDiagram
        Appearance.Margins.All = 0
        OnGetValueLabelDrawParameters = chartSpaceSimpleDiagramGetValueLabelDrawParameters
        OnGetTotalLabelDrawParameters = chartSpaceSimpleDiagramGetTotalLabelDrawParameters
        object chartSpaceSimpleSeries: TdxChartSimpleSeries
          Caption = 'Value'
          DataBindingType = 'DB'
          DataBinding.DataSource = DM.dtsrcDashboardSpace
          DataBinding.ArgumentField.FieldName = 'Caption'
          DataBinding.ValueField.FieldName = 'Value'
          ViewType = 'Doughnut'
          View.Appearance.Margins.All = 0
          View.SweepDirection = Counterclockwise
          View.TotalLabel.Appearance.FontOptions.Bold = True
          View.TotalLabel.Appearance.FontOptions.Size = 9
          View.ValueLabels.Appearance.Padding.All = 0
          View.ValueLabels.LineLength = 3.000000000000000000
          View.ValueLabels.Visible = True
          View.ValueLabels.Position = TwoColumns
          ShowInLegend = None
          ColorSchemeIndex = 0
        end
      end
    end
    object dxLayoutControlGroup_Root: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avClient
      Hidden = True
      ShowBorder = False
      Index = -1
    end
    object laychartSpace: TdxLayoutItem
      Parent = dxLayoutControlGroup_Root
      CaptionOptions.Text = 'dxChartControl1'
      CaptionOptions.Visible = False
      Control = chartSpace
      ControlOptions.OriginalHeight = 250
      ControlOptions.OriginalWidth = 300
      ControlOptions.ShowBorder = False
      Index = 0
    end
  end
end
