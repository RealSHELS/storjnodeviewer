unit ufrmRestartUsedDiffer;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBasic, dxSkiniMaginary, dxSkinSeven,
  dxSkinSevenClassic, dxSkinWXI, cxClasses, dxLayoutContainer, dxLayoutControl,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  dxDateRanges, dxScrollbarAnnotations, Data.DB, cxDBData, cxGridLevel,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, Storj.Consts, uNotifyCenter, uDM, cxCurrencyEdit, cxLabel, cxCalendar,
  dxSkinOffice2013LightGray, dxSkinOffice2019Black, dxSkinOffice2019DarkGray,
  dxSkinOffice2019White, dxSkinWhiteprint;

type
  TfrmRestartUsedDiffer = class(TForm)
    dxLayoutControlGroup_Root: TdxLayoutGroup;
    dxLayoutControl: TdxLayoutControl;
    GridDBTableView: TcxGridDBTableView;
    GridLevel1: TcxGridLevel;
    Grid: TcxGrid;
    layGrid: TdxLayoutItem;
    clmTitle: TcxGridDBColumn;
    clmNewUsed: TcxGridDBColumn;
    clmOldUsed: TcxGridDBColumn;
    clmNewUptime: TcxGridDBColumn;
    clmOldUptime: TcxGridDBColumn;
    clmOldTrash: TcxGridDBColumn;
    clmNewTrash: TcxGridDBColumn;
    GridLevel2: TcxGridLevel;
    GridDBTableViewHistory: TcxGridDBTableView;
    clmHistoryNewTrash: TcxGridDBColumn;
    clmHistoryOldTrash: TcxGridDBColumn;
    clmHistoryNewUsed: TcxGridDBColumn;
    clmHistoryOldUsed: TcxGridDBColumn;
    clmHistoryNewUptime: TcxGridDBColumn;
    clmHistoryOldUptime: TcxGridDBColumn;
    clmHistoryCreationDT: TcxGridDBColumn;
    clmHistoryTitle: TcxGridDBColumn;
    clmHistoryNodeID: TcxGridDBColumn;
    clmHistoryUsedDiff: TcxGridDBColumn;
    clmUsedDiff: TcxGridDBColumn;
    procedure clmNewUptimeGetDataText(Sender: TcxCustomGridTableItem;
      ARecordIndex: Integer; var AText: string);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure GridDBTableViewCellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure FormShow(Sender: TObject);
  private
    procedure CommandShow(const aData: TValue);
  public
    { Public declarations }
  end;

var
  frmRestartUsedDiffer: TfrmRestartUsedDiffer;

implementation

uses
  Storj.Common;

{$R *.dfm}

procedure TfrmRestartUsedDiffer.clmNewUptimeGetDataText(Sender: TcxCustomGridTableItem; ARecordIndex: Integer; var AText: string);
var
  AView: TcxGridDBTableView;
  UptimeMinutes: Integer;
begin
  AView := Sender.GridView as TcxGridDBTableView;
  if VarIsNull(AView.DataController.Values[ARecordIndex, Sender.Index]) then
    UptimeMinutes := 0
  else
    UptimeMinutes := AView.DataController.Values[ARecordIndex, Sender.Index];

  AText := UptimeMinutesToStr(UptimeMinutes);
end;

procedure TfrmRestartUsedDiffer.CommandShow(const aData: TValue);
begin
  Self.Show;
end;

procedure TfrmRestartUsedDiffer.FormCreate(Sender: TObject);
begin
  TNotifyCenter.Instance.Subscribe(COMMAND_SHOW_RESTART_USED_DIFFER, CommandShow);
end;

procedure TfrmRestartUsedDiffer.FormDestroy(Sender: TObject);
begin
  TNotifyCenter.Instance.Unsubscribe(COMMAND_SHOW_RESTART_USED_DIFFER, CommandShow);
end;

procedure TfrmRestartUsedDiffer.FormShow(Sender: TObject);
begin
  GridLevel1.Active := True;
end;

procedure TfrmRestartUsedDiffer.GridDBTableViewCellDblClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
  DM.LocateDifferHistoryFromCurrent;
  GridLevel2.Active := True;
end;

end.
