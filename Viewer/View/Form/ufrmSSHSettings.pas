unit ufrmSSHSettings;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBasic, dxSkiniMaginary, dxSkinSeven,
  dxSkinSevenClassic, dxSkinWXI, cxClasses, dxLayoutContainer, dxLayoutControl,
  dxLayoutcxEditAdapters, dxLayoutControlAdapters, cxContainer, cxEdit,
  Vcl.Menus, Vcl.StdCtrls, cxButtons, cxMemo, cxCurrencyEdit, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, Vcl.NumberBox, Storj.Consts, uNotifyCenter, uDM,
  System.JSON, System.Generics.Collections, Storj.Types, System.UITypes,
  dxSkinOffice2013LightGray, dxSkinOffice2019DarkGray, dxSkinOffice2019White;

type
  TfrmSSHSettings = class(TForm)
    dxLayoutControlGroup_Root: TdxLayoutGroup;
    dxLayoutControl: TdxLayoutControl;
    comboFullPath: TcxComboBox;
    laycomboFullPath: TdxLayoutItem;
    edtHost: TcxTextEdit;
    layedtHost: TdxLayoutItem;
    edtUsername: TcxTextEdit;
    layedtUsername: TdxLayoutItem;
    edtPassword: TcxTextEdit;
    layedtPassword: TdxLayoutItem;
    mmoCommands: TcxMemo;
    laymmoCommands: TdxLayoutItem;
    btnSave: TcxButton;
    laybtnSave: TdxLayoutItem;
    laygrpRightSide: TdxLayoutGroup;
    laygrpLeftSide: TdxLayoutGroup;
    laygrpContent: TdxLayoutGroup;
    edtPort: TcxCurrencyEdit;
    layedtPort: TdxLayoutItem;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure comboFullPathPropertiesChange(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
  private
    function IsValidInput: Boolean;

    procedure OnCommandShow(const aValue: TValue);
  public
    { Public declarations }
  end;

var
  frmSSHSettings: TfrmSSHSettings;

implementation

{$R *.dfm}

procedure TfrmSSHSettings.btnSaveClick(Sender: TObject);
var
  SSHSettings: TSSHSettings;
begin
  if not IsValidInput then
    Exit;

  SSHSettings.FullPath := comboFullPath.Properties.Items[comboFullPath.ItemIndex];
  SSHSettings.Host := Trim(edtHost.Text);
  SSHSettings.Port := Round(edtPort.Value);
  SSHSettings.UserName := Trim(edtUsername.Text);
  SSHSettings.Password := Trim(edtPassword.Text);
  SSHSettings.Commands := Trim(mmoCommands.Text);

  DM.UpsertSSHSettings(SSHSettings);

  MessageDlg('SSH settings saved', TMsgDlgType.mtInformation, [mbOk], 0);
end;

procedure TfrmSSHSettings.comboFullPathPropertiesChange(Sender: TObject);
var
  SSHSettings: TSSHSettings;
begin
  if comboFullPath.ItemIndex >= 0 then begin
    var FullPath := comboFullPath.Properties.Items[comboFullPath.ItemIndex];
    SSHSettings := DM.GetSSHSettings(FullPath);
  end;

  edtHost.Text := SSHSettings.Host;
  edtPort.Value := SSHSettings.Port;
  edtUsername.Text := SSHSettings.UserName;
  edtPassword.Text := SSHSettings.Password;
  mmoCommands.Text := SSHSettings.Commands;
end;

procedure TfrmSSHSettings.FormCreate(Sender: TObject);
begin
  TNotifyCenter.Instance.Subscribe(COMMAND_SHOW_SSH_SETTINGS, OnCommandShow);
end;

procedure TfrmSSHSettings.FormDestroy(Sender: TObject);
begin
  TNotifyCenter.Instance.UnSubscribe(COMMAND_SHOW_SSH_SETTINGS, OnCommandShow);
end;

function TfrmSSHSettings.IsValidInput: Boolean;
begin
  var ErrorMessage := '';
  Result := True;

  if comboFullPath.ItemIndex < 0 then begin
    ErrorMessage := 'FullPath is not selected' + sLineBreak;
    Result := False;
  end;

  if Trim(edtHost.Text).IsEmpty then begin
    ErrorMessage := ErrorMessage + 'Host is empty' + sLineBreak;
    Result := False;
  end;

  if Round(edtPort.Value) < 1 then begin
    ErrorMessage := ErrorMessage + 'Port is not correct' + sLineBreak;
    Result := False;
  end;

  if Trim(edtUserName.Text).IsEmpty then begin
    ErrorMessage := ErrorMessage + 'Username is empty' + sLineBreak;
    Result := False;
  end;

  if Trim(edtPassword.Text).IsEmpty then begin
    ErrorMessage := ErrorMessage + 'Password is empty' + sLineBreak;
    Result := False;
  end;

  ErrorMessage := ErrorMessage.Trim;

  if not ErrorMessage.IsEmpty then
    MessageDlg(ErrorMessage, TMsgDlgType.mtError, [mbOk], 0);
end;

procedure TfrmSSHSettings.OnCommandShow(const aValue: TValue);
  procedure AddJNodeToCombo(aJArray: TJSONArray);
  begin
    for var i := 0 to aJArray.Count -1 do begin
      var JObj := aJArray.Items[i] as TJSONObject;
      var FullPath := JObj.GetValue<String>('FullPath', '');

      comboFullPath.Properties.Items.Add(FullPath);

      var Childs := JObj.GetValue<TJSONArray>('Childs', nil);
      if Childs <> nil then
        AddJNodeToCombo(Childs);
    end;
  end;

begin
  comboFullPath.Properties.Items.Clear;
  var JNodesArray := DM.GetNodesJArrayActiveOnly;
  AddJNodeToCombo(JNodesArray);

  Show;
end;

end.
