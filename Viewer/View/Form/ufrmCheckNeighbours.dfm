object frmCheckNeighbours: TfrmCheckNeighbours
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Check neighbours'
  ClientHeight = 120
  ClientWidth = 363
  Color = clGrayText
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  TextHeight = 13
  object dxLayoutControl: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 363
    Height = 120
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 371
    ExplicitHeight = 121
    object edtURL: TcxTextEdit
      Left = 48
      Top = 12
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 0
      TextHint = '1.2.3.4'
      Width = 277
    end
    object btnAction: TcxButton
      Left = 331
      Top = 10
      Width = 26
      Height = 25
      Hint = 'GO!'
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'iMaginary'
      OptionsImage.Glyph.SourceDPI = 96
      OptionsImage.Glyph.SourceHeight = 16
      OptionsImage.Glyph.SourceWidth = 16
      OptionsImage.Glyph.Data = {
        3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D225554
        462D38223F3E0D0A3C7376672076657273696F6E3D22312E31222069643D224C
        617965725F312220786D6C6E733D22687474703A2F2F7777772E77332E6F7267
        2F323030302F7376672220786D6C6E733A786C696E6B3D22687474703A2F2F77
        77772E77332E6F72672F313939392F786C696E6B2220783D223070782220793D
        22307078222076696577426F783D2230203020333220333222207374796C653D
        22656E61626C652D6261636B67726F756E643A6E657720302030203332203332
        3B2220786D6C3A73706163653D227072657365727665223E262331333B262331
        303B3C7374796C6520747970653D22746578742F637373223E2E426C61636B7B
        66696C6C3A233732373237323B7D3C2F7374796C653E0D0A3C706F6C79676F6E
        20636C6173733D22426C61636B2220706F696E74733D2232382C31362031382C
        362031382C313220342C313220342C32302031382C32302031382C323620222F
        3E0D0A3C2F7376673E0D0A}
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnActionClick
    end
    object lblInfo: TcxLabel
      Left = 10
      Top = 41
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -16
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.IsFontAssigned = True
      Transparent = True
    end
    object lblPoweredBy: TcxLabel
      Left = 10
      Top = 95
      Caption = 'Powered by'
      Style.HotTrack = False
      Style.TransparentBorder = False
      Transparent = True
    end
    object linkStorjNetInfo: TLinkLabel
      Left = 108
      Top = 92
      Width = 137
      Height = 19
      Caption = '<a href="http://storjnet.info">storjnet.info</a>'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 4
      UseVisualStyle = True
      OnLinkClick = linkStorjNetInfoLinkClick
    end
    object linkCloudflare: TLinkLabel
      Left = 251
      Top = 92
      Width = 106
      Height = 19
      Caption = '<a href="http://cloudflare.com">cloudflare.com</a>'
      Color = clBtnFace
      ParentColor = False
      TabOrder = 5
      UseVisualStyle = True
      OnLinkClick = linkStorjNetInfoLinkClick
    end
    object dxLayoutControlGroup_Root: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avClient
      Hidden = True
      ItemIndex = 2
      ShowBorder = False
      Index = -1
    end
    object layedtURL: TdxLayoutItem
      Parent = dxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avCenter
      CaptionOptions.Text = 'IP/URL'
      Control = edtURL
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object laybtnAction: TdxLayoutItem
      Parent = dxLayoutGroup
      AlignVert = avCenter
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnAction
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 26
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object laylblInfo: TdxLayoutItem
      Parent = dxLayoutControlGroup_Root
      AlignHorz = ahClient
      CaptionOptions.Text = 'cxLabel1'
      CaptionOptions.Visible = False
      Control = lblInfo
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 46
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutGroup: TdxLayoutGroup
      Parent = dxLayoutControlGroup_Root
      CaptionOptions.Text = 'New Group'
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 0
    end
    object laylblPoweredBy: TdxLayoutItem
      Parent = laygrpPoweredBy
      AlignHorz = ahClient
      AlignVert = avCenter
      CaptionOptions.Text = 'cxLabel1'
      CaptionOptions.Visible = False
      Control = lblPoweredBy
      ControlOptions.OriginalHeight = 13
      ControlOptions.OriginalWidth = 46
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object laylinkStorjNetInfo: TdxLayoutItem
      Parent = laygrpPoweredBy
      AlignHorz = ahClient
      AlignVert = avCenter
      CaptionOptions.Text = 'LinkLabel1'
      CaptionOptions.Visible = False
      Control = linkStorjNetInfo
      ControlOptions.AutoColor = True
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 68
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object laygrpPoweredBy: TdxLayoutGroup
      Parent = dxLayoutControlGroup_Root
      AlignVert = avBottom
      CaptionOptions.Text = 'New Group'
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 2
    end
    object laylinkCloudflare: TdxLayoutItem
      Parent = laygrpPoweredBy
      AlignHorz = ahClient
      AlignVert = avCenter
      CaptionOptions.Text = 'LinkLabel1'
      CaptionOptions.Visible = False
      Control = linkCloudflare
      ControlOptions.AutoColor = True
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 53
      ControlOptions.ShowBorder = False
      Index = 2
    end
  end
end
