object frmFinancialData: TfrmFinancialData
  Left = 0
  Top = 0
  Caption = 'Financial'
  ClientHeight = 529
  ClientWidth = 1085
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  TextHeight = 13
  object dxLayoutControl: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 1085
    Height = 529
    Align = alClient
    TabOrder = 0
    object TreeListFinancial: TcxTreeList
      Left = 10
      Top = 46
      Width = 1069
      Height = 474
      Bands = <
        item
        end>
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'SevenClassic'
      Navigator.Buttons.CustomButtons = <>
      OptionsBehavior.ChangeDelay = 1000
      OptionsData.Editing = False
      OptionsData.Deleting = False
      OptionsView.ColumnAutoWidth = True
      PopupMenu = PopupMenu
      ScrollbarAnnotations.CustomAnnotations = <>
      TabOrder = 0
      object clmTitle: TcxTreeListColumn
        Caption.AlignVert = vaTop
        Caption.Text = 'Title'
        Width = 100
        Position.ColIndex = 0
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmAddress: TcxTreeListColumn
        Caption.AlignVert = vaTop
        Caption.Text = 'Address'
        Width = 100
        Position.ColIndex = 1
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmPort: TcxTreeListColumn
        Caption.AlignVert = vaTop
        Caption.Text = 'Port'
        Width = 100
        Position.ColIndex = 2
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmPayoutDirty: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = ',0.00 $;-,0.00 $'
        Caption.AlignHorz = taCenter
        Caption.AlignVert = vaTop
        Caption.Text = 'Payout dirty'
        Width = 100
        Position.ColIndex = 3
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmPayoutCleanly: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = ',0.00 $;-,0.00 $'
        Caption.AlignHorz = taCenter
        Caption.AlignVert = vaTop
        Caption.Text = 'Payout cleanly'
        Width = 100
        Position.ColIndex = 4
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmEstimatedDirty: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = ',0.00 $;-,0.00 $'
        Caption.AlignHorz = taCenter
        Caption.AlignVert = vaTop
        Caption.Text = 'Estimated dirty'
        Width = 100
        Position.ColIndex = 5
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmEstimatedCleanly: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = ',0.00 $;-,0.00 $'
        Caption.AlignHorz = taCenter
        Caption.AlignVert = vaTop
        Caption.Text = 'Estimated cleanly'
        Width = 100
        Position.ColIndex = 6
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmEfficiencyByUsed: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = ',0.00 $;-,0.00 $'
        Caption.AlignHorz = taCenter
        Caption.AlignVert = vaTop
        Caption.Text = 'Efficiency by Used'
        Width = 100
        Position.ColIndex = 7
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmEfficiencyByTBM: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = ',0.00 $;-,0.00 $'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Efficiency by TBM'
        Options.Editing = False
        Width = 100
        Position.ColIndex = 8
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmUSDCost: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = ',0.00 $;-,0.00 $'
        Caption.AlignHorz = taCenter
        Caption.AlignVert = vaTop
        Caption.Text = 'USD cost'
        Width = 100
        Position.ColIndex = 9
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmCleanProfit: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = ',0.00 $;-,0.00 $'
        Caption.AlignHorz = taCenter
        Caption.AlignVert = vaTop
        Caption.Text = 'Clean profit'
        Width = 100
        Position.ColIndex = 10
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmEstimatedCleanProfit: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = ',0.00 $;-,0.00 $'
        Caption.AlignHorz = taCenter
        Caption.AlignVert = vaTop
        Caption.Text = 'Estimated clean profit'
        Width = 100
        Position.ColIndex = 11
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmUsedSpace: TcxTreeListColumn
        Visible = False
        Caption.AlignVert = vaTop
        Caption.Text = 'Used space'
        Width = 100
        Position.ColIndex = 12
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmCurrency: TcxTreeListColumn
        Visible = False
        Caption.AlignVert = vaTop
        Caption.Text = 'Currency'
        Width = 100
        Position.ColIndex = 13
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmCost: TcxTreeListColumn
        Visible = False
        Caption.AlignVert = vaTop
        Caption.Text = 'Cost'
        Width = 100
        Position.ColIndex = 14
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmTotalEarned: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = ',0.00 $;-,0.00 $'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Total Earned'
        Width = 100
        Position.ColIndex = 15
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmComment: TcxTreeListColumn
        Caption.AlignVert = vaTop
        Caption.Text = 'Comment'
        Options.Sorting = False
        Width = 100
        Position.ColIndex = 16
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmTBM: TcxTreeListColumn
        VisibleForExpressionEditor = bFalse
        Visible = False
        Options.Editing = False
        Width = 100
        Position.ColIndex = 17
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
    object dxLayoutControlGroup_Root: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avClient
      Hidden = True
      ShowBorder = False
      Index = -1
    end
    object dxLayoutGroupTop: TdxLayoutGroup
      Parent = dxLayoutControlGroup_Root
      CaptionOptions.Text = 'New Group'
      Visible = False
      Index = 0
    end
    object dxLayoutGroupClient: TdxLayoutGroup
      Parent = dxLayoutControlGroup_Root
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'New Group'
      ShowBorder = False
      Index = 1
    end
    object layTreeList: TdxLayoutItem
      Parent = dxLayoutGroupClient
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'cxTreeList1'
      CaptionOptions.Visible = False
      Control = TreeListFinancial
      ControlOptions.OriginalHeight = 150
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 0
    end
  end
  object PopupMenu: TPopupMenu
    Left = 608
    Top = 288
    object RefreshTotalEarned1: TMenuItem
      Caption = 'Refresh TotalEarned'
      OnClick = RefreshTotalEarned1Click
    end
  end
end
