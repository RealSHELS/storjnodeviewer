unit ufrmNeighboursTree;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBasic, dxSkiniMaginary, dxSkinSeven,
  dxSkinSevenClassic, cxClasses, dxLayoutContainer, dxLayoutControl,
  dxLayoutControlAdapters, dxLayoutcxEditAdapters, cxCustomData, cxStyles,
  dxScrollbarAnnotations, cxTL, cxTLdxBarBuiltInMenu, Vcl.Menus, cxContainer,
  cxEdit, cxLabel, Vcl.StdCtrls, cxButtons, cxInplaceContainer,
  uNotifyCenter, Storj.Consts, Storj.Types, System.JSON, cxTextEdit,
  uDM, System.Generics.Collections, System.UITypes, dxSkinWXI, cxFilter;

type
  TfrmNeighboursTree = class(TForm)
    dxLayoutControlGroup_Root: TdxLayoutGroup;
    dxLayoutControl: TdxLayoutControl;
    laygrpTopToolbar: TdxLayoutGroup;
    treeNeightbours: TcxTreeList;
    layNeighboursTree: TdxLayoutItem;
    btnRefresh: TcxButton;
    laybtnRefresh: TdxLayoutItem;
    txtCommonInfo: TcxLabel;
    laytxtCommonInfo: TdxLayoutItem;
    txtLastUpdateInfo: TcxLabel;
    laytxtLastUpdateInfo: TdxLayoutItem;
    clmTitle: TcxTreeListColumn;
    clmAddress: TcxTreeListColumn;
    clmPort: TcxTreeListColumn;
    clmNeighbours: TcxTreeListColumn;
    laygrpTextInfo: TdxLayoutGroup;
    clmOwnNeighbours: TcxTreeListColumn;
    clmSubnet: TcxTreeListColumn;
    clmOwnNeighboursCount: TcxTreeListColumn;
    clmComment: TcxTreeListColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnRefreshClick(Sender: TObject);
    procedure FormShow(Sender: TObject);

    procedure ProcessSubnetOwnNeighbours(const aSubnet: String);
    procedure treeNeightboursCustomDrawDataCell(Sender: TcxCustomTreeList;
      ACanvas: TcxCanvas; AViewInfo: TcxTreeListEditCellViewInfo;
      var ADone: Boolean);
  private
    procedure CommandShow(const aData: TValue);
    procedure CommandGetData(const aData: TValue);
    procedure CommandLoadFromJSON(const aData: TValue);

    procedure SetJArrTreeNodes(AJArr: TJSONArray; AParentNode: TcxTreeListNode);
    procedure DisplayLastUpdatedInfo;
  public
  end;

var
  frmNeighboursTree: TfrmNeighboursTree;

implementation

{$R *.dfm}

procedure TfrmNeighboursTree.CommandGetData(const aData: TValue);
  procedure SetNodeNeighbours(AParentNode: TcxTreeListNode; iData: TNeighboursData);
  var
    i: integer;
    CurNode: TcxTreeListNode;
  begin
    for i := 0 to AParentNode.Count - 1 do begin
      CurNode := AParentNode.Items[i];
      if not VarToStr(CurNode.Values[clmAddress.Position.ColIndex]).IsEmpty and (CurNode.Values[clmAddress.Position.ColIndex] = iData.Host) then begin
        CurNode.Values[clmNeighbours.Position.ColIndex] := iData.Neighbours;
        CurNode.Values[clmSubnet.Position.ColIndex] := iData.Subnet;
      end;

      if CurNode.HasChildren then
        SetNodeNeighbours(CurNode, iData);
    end;
  end;

var
  Data: TNeighboursData;
begin
  Data := aData.AsType<TNeighboursData>;

  treeNeightbours.BeginUpdate;
  try
    SetNodeNeighbours(treeNeightbours.Root, Data);

    ProcessSubnetOwnNeighbours(Data.Subnet);
  finally
    treeNeightbours.EndUpdate;
  end;
end;

procedure TfrmNeighboursTree.CommandShow(const aData: TValue);
begin
  Show;
end;

procedure TfrmNeighboursTree.DisplayLastUpdatedInfo;
begin
  txtLastUpdateInfo.Caption := 'Last update: ' + DM.GetFormattedNeighboursLastUpdate;
end;

procedure TfrmNeighboursTree.btnRefreshClick(Sender: TObject);
begin
  if DM.IsCanLoadNeighbours then begin
    DM.LoadNeighbours;
    DisplayLastUpdatedInfo;
  end else
    MessageDlg('You can''t refresh neighbours too often', mtError, [mbOk], 0);
end;

procedure TfrmNeighboursTree.FormCreate(Sender: TObject);
begin
  TNotifyCenter.Instance.Subscribe(COMMAND_SHOW_NEIGHBOURS_TREE, CommandShow);
  TNotifyCenter.Instance.Subscribe(COMMAND_NEIGHBOURS_DATA, CommandGetData);
  TNotifyCenter.Instance.Subscribe(COMMAND_LOAD_FROM_JSON, CommandLoadFromJSON);
end;

procedure TfrmNeighboursTree.FormDestroy(Sender: TObject);
begin
  TNotifyCenter.Instance.Unsubscribe(COMMAND_SHOW_NEIGHBOURS_TREE, CommandShow);
  TNotifyCenter.Instance.Unsubscribe(COMMAND_NEIGHBOURS_DATA, CommandGetData);
  TNotifyCenter.Instance.UnSubscribe(COMMAND_LOAD_FROM_JSON, CommandLoadFromJSON);
end;

procedure TfrmNeighboursTree.FormShow(Sender: TObject);
begin
  DisplayLastUpdatedInfo;
end;

procedure TfrmNeighboursTree.ProcessSubnetOwnNeighbours(const aSubnet: String);
  function GetOwnNeighbours(const iSubnet: String; AParentNode: TcxTreeListNode): TArray<String>;
  var
    i: integer;
  begin
    Result := [];
    for i := 0 to AParentNode.Count - 1 do begin
      if AParentNode.Items[i].Values[clmSubnet.Position.ColIndex] = iSubnet then
        Result := Result + [AParentNode.Items[i].Values[clmTitle.Position.ColIndex]];

      if AParentNode.Items[i].HasChildren then
        Result := Result + GetOwnNeighbours(iSubnet, AParentNode.Items[i]);
    end;
  end;

  procedure SetOwnNeightbours(const iSubnet, iOwnNeighboursData: String; iOwnNeighboursCount: Integer; AParentNode: TcxTreeListNode);
  var
    i: integer;
    OwnTitle, Data: String;
  begin
    for i := 0 to AParentNode.Count - 1 do begin
      if AParentNode.Items[i].Values[clmSubnet.Position.ColIndex] = iSubnet then begin
        OwnTitle := AParentNode.Items[i].Values[clmTitle.Position.ColIndex];
        Data := iOwnNeighboursData.Replace(OwnTitle + '; ', '');
        AParentNode.Items[i].Values[clmOwnNeighbours.Position.ColIndex] := Data;
        AParentNode.Items[i].Values[clmOwnNeighboursCount.Position.ColIndex] := iOwnNeighboursCount;
      end;

      if AParentNode.Items[i].HasChildren then
        SetOwnNeightbours(iSubnet, iOwnNeighboursData, iOwnNeighboursCount, AParentNode.Items[i]);
    end;
  end;

var
  OwnNeighboursData: String;
begin
  var Arr := GetOwnNeighbours(aSubnet, treeNeightbours.Root);

  OwnNeighboursData := '';
  if Length(arr) > 1 then
    for var item in arr do
      OwnNeighboursData := OwnNeighboursData + item + '; ';

  SetOwnNeightbours(aSubnet, OwnNeighboursData, Length(arr), treeNeightbours.Root);
end;

procedure TfrmNeighboursTree.CommandLoadFromJSON(const aData: TValue);
begin
  var JArr := aData.AsObject as TJSONArray;
  if not Assigned(JArr) then
    Exit;

  treeNeightbours.BeginUpdate;
  try
    treeNeightbours.Clear;
    SetJArrTreeNodes(JArr, treeNeightbours.Root);
//    treeNeightbours.FullExpand;
  finally
    treeNeightbours.EndUpdate;
  end;
end;

procedure TfrmNeighboursTree.SetJArrTreeNodes(AJArr: TJSONArray; AParentNode: TcxTreeListNode);
var
  i: integer;
  JObj: TJSONObject;
  JValue: TJSONValue;
  Node: TcxTreeListNode;
begin
  for i := 0 to AJarr.Count - 1 do begin
    JObj := AJArr.Items[i] as TJSONObject;
    if not JObj.GetValue<Boolean>('IsActive') then
      Continue;

    Node := AParentNode.AddChild;
    Node.Values[clmTitle.Position.ColIndex] := JObj.GetValue<String>('Title');
    Node.Values[clmAddress.Position.ColIndex] := JObj.GetValue<String>('Address');
    Node.Values[clmPort.Position.ColIndex] := JObj.GetValue<String>('Port');
    Node.Values[clmNeighbours.Position.ColIndex] := null;
    Node.Values[clmComment.Position.ColIndex] := JObj.GetValue<String>('Comment');

    JValue := JObj.GetValue<TJSONArray>('Childs', nil);
    if JValue <> nil then
      SetJArrTreeNodes(JValue as TJSONArray, Node);

    Node.Expanded := True;
  end;
end;

procedure TfrmNeighboursTree.treeNeightboursCustomDrawDataCell(
  Sender: TcxCustomTreeList; ACanvas: TcxCanvas;
  AViewInfo: TcxTreeListEditCellViewInfo; var ADone: Boolean);
begin
  if (AViewInfo.Column.Name = 'clmNeighbours') then begin
    var OwnCount := AViewInfo.Node.Values[clmOwnNeighboursCount.Position.ColIndex];
    var Value := AViewInfo.DisplayValue;

    if (not VarIsNull(OwnCount)) and (not VarIsNull(Value)) then begin
      var color := $d4f9d4;

      if OwnCount < Value then
        color := RGB(255, 236, 230);

      ACanvas.FillRect(AViewInfo.ContentRect, color);
      ACanvas.DrawTexT(Value, AViewInfo.BoundsRect, TAlignment.taCenter, TcxAlignmentVert.vaCenter, false, true);
      ADone := True;
    end;
  end;
end;

end.
