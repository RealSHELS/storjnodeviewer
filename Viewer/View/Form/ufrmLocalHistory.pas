unit ufrmLocalHistory;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uNotifyCenter, Storj.Consts, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxSkinBasic,
  dxSkiniMaginary, dxSkinSeven, dxSkinSevenClassic, dxSkinWXI, cxClasses,
  dxLayoutContainer, dxLayoutControl, dxLayoutcxEditAdapters, Vcl.ComCtrls,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  dxDateRanges, dxScrollbarAnnotations, Data.DB, cxDBData, cxGridLevel,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, cxContainer, cxTreeView, cxTL, cxTLdxBarBuiltInMenu,
  cxInplaceContainer, dxLayoutControlAdapters, Vcl.Menus, Vcl.StdCtrls,
  cxButtons, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxDBEdit, uDM,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, Vcl.DBCtrls, Storj.Types,
  System.JSON, Storj.Settings, Storj.Common, cxLabel, cxCurrencyEdit,
  dxBarBuiltInMenu, cxGridCustomPopupMenu, cxGridPopupMenu,
  System.Generics.Collections;

type
  TfrmLocalHistory = class(TForm)
    dxLayoutControlGroup_Root: TdxLayoutGroup;
    dxLayoutControl: TdxLayoutControl;
    laygrpTop: TdxLayoutGroup;
    laygrpClient: TdxLayoutGroup;
    GridViewDBTableView: TcxGridDBTableView;
    GridViewLevel1: TcxGridLevel;
    GridView: TcxGrid;
    layGridView: TdxLayoutItem;
    DailyGridViewDBTableView: TcxGridDBTableView;
    DailyGridViewLevel1: TcxGridLevel;
    DailyGridView: TcxGrid;
    layDailyGridView: TdxLayoutItem;
    TreeList: TcxTreeList;
    layTreeList: TdxLayoutItem;
    DailyTreeList: TcxTreeList;
    layDailyTreeList: TdxLayoutItem;
    btnDisplay: TcxButton;
    dxLayoutItem2: TdxLayoutItem;
    comboDates: TcxLookupComboBox;
    laycomboDates: TdxLayoutItem;
    columnGridTitle: TcxGridDBColumn;
    columnGridAddress: TcxGridDBColumn;
    columnGridPort: TcxGridDBColumn;
    columnGridTBM: TcxGridDBColumn;
    columnGridIngress: TcxGridDBColumn;
    columnGridEgress: TcxGridDBColumn;
    columnGridRepairIngress: TcxGridDBColumn;
    columnGridRepairEgress: TcxGridDBColumn;
    columnGridAllocatedSpace: TcxGridDBColumn;
    columnGridFreeSpace: TcxGridDBColumn;
    columnGridUsedSpace: TcxGridDBColumn;
    columnGridPayoutDirty: TcxGridDBColumn;
    columnGridPayoutCleanly: TcxGridDBColumn;
    columnGridEstimatedDirty: TcxGridDBColumn;
    columnGridEstimatedCleanly: TcxGridDBColumn;
    columnGridVersion: TcxGridDBColumn;
    columnGridEfficiency: TcxGridDBColumn;
    columnGridHDDVolume: TcxGridDBColumn;
    columnGridNodeID: TcxGridDBColumn;
    columnGridTrash: TcxGridDBColumn;
    columnGridFullPath: TcxGridDBColumn;
    columnGridTotalBandwidth: TcxGridDBColumn;
    columnGridDailyEgress: TcxGridDBColumn;
    columnGridDailyIngress: TcxGridDBColumn;
    columnGridDailyRepairIngress: TcxGridDBColumn;
    columnGridDailyRepairEgress: TcxGridDBColumn;
    columnGridDailyTotalBandwidth: TcxGridDBColumn;
    columnGridUptimeMinutes: TcxGridDBColumn;
    columnGridWallet: TcxGridDBColumn;
    columnGridDailyTBMonth: TcxGridDBColumn;
    columnDailyGridEgress: TcxGridDBColumn;
    columnDailyGridIngress: TcxGridDBColumn;
    columnDailyGridRepairIngress: TcxGridDBColumn;
    columnDailyGridRepairEgress: TcxGridDBColumn;
    columnDailyGridTotalBandwidth: TcxGridDBColumn;
    columnDailyGridTBMonth: TcxGridDBColumn;
    cxGridPopupMenu: TcxGridPopupMenu;
    columnDailyTitle: TcxGridDBColumn;
    columnDailyAddress: TcxGridDBColumn;
    columnDailyPort: TcxGridDBColumn;
    lblDateSavedAt: TcxLabel;
    dxLayoutItem1: TdxLayoutItem;
    clmTreeTitle: TcxTreeListColumn;
    clmTreeAddress: TcxTreeListColumn;
    clmTreePort: TcxTreeListColumn;
    clmTreeEgress: TcxTreeListColumn;
    clmTreeIngress: TcxTreeListColumn;
    clmTreeRepairEgress: TcxTreeListColumn;
    clmTreeRepairIngress: TcxTreeListColumn;
    clmTreeTotalBandwidth: TcxTreeListColumn;
    clmTreeTBMonth: TcxTreeListColumn;
    clmTreeAllocatedSpace: TcxTreeListColumn;
    clmTreeUsedSpace: TcxTreeListColumn;
    clmTreeTrash: TcxTreeListColumn;
    clmTreeFreeSpace: TcxTreeListColumn;
    clmTreePayoutDirty: TcxTreeListColumn;
    clmTreePayoutCleanly: TcxTreeListColumn;
    clmTreeEstimatedDirtyPayout: TcxTreeListColumn;
    clmTreeEstimatedCleanlyPayout: TcxTreeListColumn;
    clmTreeVersion: TcxTreeListColumn;
    clmDailyTreeTitle: TcxTreeListColumn;
    clmDailyTreeAddress: TcxTreeListColumn;
    clmDailyTreePort: TcxTreeListColumn;
    clmDailyTreeTBMonth: TcxTreeListColumn;
    clmDailyTreeEgress: TcxTreeListColumn;
    clmDailyTreeIngress: TcxTreeListColumn;
    clmDailyTreeRepairEgress: TcxTreeListColumn;
    clmDailyTreeRepairIngress: TcxTreeListColumn;
    clmDailyTreeTotalBandwidth: TcxTreeListColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnDisplayClick(Sender: TObject);
    procedure columnGridFreeSpaceCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure columnGridTBMCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure columnGridUptimeMinutesGetDataText(Sender: TcxCustomGridTableItem;
      ARecordIndex: Integer; var AText: string);
    procedure TreeListCustomDrawDataCell(Sender: TcxCustomTreeList;
      ACanvas: TcxCanvas; AViewInfo: TcxTreeListEditCellViewInfo;
      var ADone: Boolean);
  private

    procedure AddTreeNodes(aParent: TcxTreeListNode; aJArr: TJSONArray);
    procedure AddDailyTreeNodes(aParent: TcxTreeListNode; aJArr: TJSONArray);
    procedure LoadSettingsFromJSONForGridView;
    procedure LoadTreeColumnFromJSON;

    procedure OnCommandShowLocalHistory(const aValue: TValue);
  public

  end;

var
  frmLocalHistory: TfrmLocalHistory;

implementation

{$R *.dfm}

{ TfrmLocalHistory }

procedure TfrmLocalHistory.AddDailyTreeNodes(aParent: TcxTreeListNode; aJArr: TJSONArray);
begin
  for var i := 0 to aJArr.Count - 1 do begin
    var JObj := aJArr.Items[i] as TJSONObject;
    var TreeNode := aParent.AddChild;

    TreeNode.Values[clmDailyTreeTitle.Position.ColIndex] := JObj.GetValue<String>('Title', '');
    TreeNode.Values[clmDailyTreeAddress.Position.ColIndex] := JObj.GetValue<String>('Address', '');
    if not JObj.GetValue<String>('Port', '').IsEmpty then
      TreeNode.Values[clmDailyTreePort.Position.ColIndex] := JObj.GetValue<Integer>('Port', 0);

    TreeNode.Values[clmDailyTreeEgress.Position.ColIndex] := JObj.GetValue<Double>('DailyEgress', 0);
    TreeNode.Values[clmDailyTreeIngress.Position.ColIndex] := JObj.GetValue<Double>('DailyIngress', 0);
    TreeNode.Values[clmDailyTreeRepairEgress.Position.ColIndex] := JObj.GetValue<Double>('DailyRepairEgress', 0);
    TreeNode.Values[clmDailyTreeRepairIngress.Position.ColIndex] := JObj.GetValue<Double>('DailyRepairIngress', 0);
    TreeNode.Values[clmDailyTreeTotalBandwidth.Position.ColIndex] := JObj.GetValue<Double>('DailyTotalBandwidth', 0);

    TreeNode.Values[clmDailyTreeTBMonth.Position.ColIndex] := JObj.GetValue<Double>('DailyTBMonth', 0);

    var Childs := JObj.GetValue<TJSONArray>('Childs', nil);
    if Assigned(Childs) then
      AddDailyTreeNodes(TreeNode, Childs);

    TreeNode.Expanded := JObj.GetValue<Boolean>('AutoExpand', False);
  end;
end;

procedure TfrmLocalHistory.AddTreeNodes(aParent: TcxTreeListNode; aJArr: TJSONArray);
begin
  for var i := 0 to aJArr.Count - 1 do begin
    var JObj := aJArr.Items[i] as TJSONObject;
    var TreeNode := aParent.AddChild;

    TreeNode.Values[clmTreeTitle.Position.ColIndex] := JObj.GetValue<String>('Title', '');
    TreeNode.Values[clmTreeAddress.Position.ColIndex] := JObj.GetValue<String>('Address', '');
    if not JObj.GetValue<String>('Port', '').IsEmpty then
      TreeNode.Values[clmTreePort.Position.ColIndex] := JObj.GetValue<Integer>('Port', 0);

    TreeNode.Values[clmTreeEgress.Position.ColIndex] := JObj.GetValue<Double>('Egress', 0);
    TreeNode.Values[clmTreeIngress.Position.ColIndex] := JObj.GetValue<Double>('Ingress', 0);
    TreeNode.Values[clmTreeRepairEgress.Position.ColIndex] := JObj.GetValue<Double>('RepairEgress', 0);
    TreeNode.Values[clmTreeRepairIngress.Position.ColIndex] := JObj.GetValue<Double>('RepairIngress', 0);
    TreeNode.Values[clmTreeTotalBandwidth.Position.ColIndex] := JObj.GetValue<Double>('TotalBandwidth', 0);

    TreeNode.Values[clmTreeTBMonth.Position.ColIndex] := JObj.GetValue<Double>('TBM', 0);
    TreeNode.Values[clmTreeVersion.Position.ColIndex] := JObj.GetValue<String>('Version', '');

    TreeNode.Values[clmTreeAllocatedSpace.Position.ColIndex] := JObj.GetValue<Integer>('AllocatedSpace', 0);
    TreeNode.Values[clmTreeUsedSpace.Position.ColIndex] := JObj.GetValue<Integer>('UsedSpace', 0);
    TreeNode.Values[clmTreeTrash.Position.ColIndex] := JObj.GetValue<Integer>('Trash', 0);
    TreeNode.Values[clmTreeFreeSpace.Position.ColIndex] := JObj.GetValue<Integer>('FreeSpace', 0);

    TreeNode.Values[clmTreePayoutDirty.Position.ColIndex] := JObj.GetValue<Currency>('PayoutDirty', 0);
    TreeNode.Values[clmTreePayoutCleanly.Position.ColIndex] := JObj.GetValue<Currency>('PayoutCleanly', 0);
    TreeNode.Values[clmTreeEstimatedDirtyPayout.Position.ColIndex] := JObj.GetValue<Currency>('EstimatedDirty', 0);
    TreeNode.Values[clmTreeEstimatedCleanlyPayout.Position.ColIndex] := JObj.GetValue<Currency>('EstimatedCleanly', 0);

    var Childs := JObj.GetValue<TJSONArray>('Childs', nil);
    if Assigned(Childs) then
      AddTreeNodes(TreeNode, Childs);

    TreeNode.Expanded := JObj.GetValue<Boolean>('AutoExpand', False);
  end;
end;

procedure TfrmLocalHistory.btnDisplayClick(Sender: TObject);
var
  ID: Integer;
  LocalHistoryData: TLocalHistoryData;
begin
  ID := comboDates.EditValue;

  TreeList.BeginUpdate;
  DailyTreeList.BeginUpdate;
  try
    TreeList.Root.DeleteChildren;
    DailyTreeList.Root.DeleteChildren;

    LocalHistoryData := DM.LoadHistoryData(ID);

    lblDateSavedAt.Caption := 'Date stats saved at: ' + DateTimeToStr(LocalHistoryData.CreationDT) + ' (UTC)';

    var JArr := TJSONValue.ParseJSONValue(LocalHistoryData.Data) as TJSONArray;
    try
      if Assigned(JArr) and (JArr.Count > 0) then begin
        AddTreeNodes(TreeList.Root, JArr);
        AddDailyTreeNodes(DailyTreeList.Root, JArr);
      end;
    finally
      JArr.Free;
    end;
  finally
    TreeList.EndUpdate;
    DailyTreeList.EndUpdate;
  end;
end;

procedure TfrmLocalHistory.columnGridFreeSpaceCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  FreeSpace: Variant;
begin
  if Assigned(AViewInfo) and GetSettings.IsColoredFreeSpace then
  begin
    FreeSpace := AViewInfo.Value;
    if FreeSpace = null then
      FreeSpace := 0;

    var color := $d4f9d4;
    if FreeSpace < GetSettings.RedColorValue then
      color := RGB(255, 236, 230)
    else if FreeSpace < GetSettings.YellowColorValue then
      color := RGB(255, 246, 189);

     ACanvas.Brush.Color := color;
   end;
end;

procedure TfrmLocalHistory.columnGridTBMCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  TBM, UsedSpace: Variant;
begin
  if Assigned(AViewInfo) and GetSettings.IsColoredTBMDeviation then
  begin
    TBM := AViewInfo.Value;
    if TBM = null then
      TBM := 0;

    UsedSpace := AViewInfo.GridRecord.Values[columnGridUsedSpace.Index];
    if UsedSpace = null then
      UsedSpace := 0;

    if UsedSpace > 0 then begin
      var Ratio: Double := (TBM * 1000) / UsedSpace * 100;

      var IsDeviationTresholdedForYellow := Ratio < (100 - GetSettings.LessThanDeviationPercent);
      var IsDeviationTresholdedForRed := Ratio > (100 + GetSettings.MoreThanDeviationPercent);

      var color := $d4f9d4;
      if IsDeviationTresholdedForRed then
        color := RGB(255, 236, 230)
      else if IsDeviationTresholdedForYellow then
        color := RGB(255, 246, 189);

      ACanvas.Brush.Color := color;
    end;
   end;
end;

procedure TfrmLocalHistory.columnGridUptimeMinutesGetDataText(
  Sender: TcxCustomGridTableItem; ARecordIndex: Integer; var AText: string);
var
  AView: TcxGridDBTableView;
  UptimeMinutes: Integer;
begin
  AView := Sender.GridView as TcxGridDBTableView;
  if VarIsNull(AView.DataController.Values[ARecordIndex, columnGridUptimeMinutes.Index]) then
    UptimeMinutes := 0
  else
    UptimeMinutes := AView.DataController.Values[ARecordIndex, columnGridUptimeMinutes.Index];

  AText := UptimeMinutesToStr(UptimeMinutes);
end;

procedure TfrmLocalHistory.FormCreate(Sender: TObject);
begin
  LoadSettingsFromJSONForGridView;
  LoadTreeColumnFromJSON;

  TNotifyCenter.Instance.Subscribe(COMMAND_SHOW_LOCAL_HISTORY, OnCommandShowLocalHistory);
end;

procedure TfrmLocalHistory.FormDestroy(Sender: TObject);
begin
  TNotifyCenter.Instance.UnSubscribe(COMMAND_SHOW_LOCAL_HISTORY, OnCommandShowLocalHistory);
end;

procedure TfrmLocalHistory.FormShow(Sender: TObject);
begin
  DM.ReloadLocalHistoryDates;
end;

procedure TfrmLocalHistory.LoadSettingsFromJSONForGridView;
var
  StrList: TStringList;
  RootJSON, JSON: TJSONObject;
  JArr: TJSONArray;
  i: integer;
  Path: String;
  Column: TcxGridDBColumn;
begin
  Path := ExtractFilePath(Application.ExeName);

  if not FileExists(Path + 'GridViewSettings.json') then
    Exit;

  StrList := TStringList.Create;
  try
    StrList.LoadFromFile(Path + 'GridViewSettings.json');

    RootJSON := TJSONObject.ParseJSONValue(StrList.Text) as TJSONObject;
    try
      JArr := RootJSON.Values['MainGrid'] as TJSONArray;
      for i := 0 to JArr.Count - 1 do begin
        JSON := JArr.Items[i] as TJSONObject;

        Column := GridViewDBTableView.GetColumnByFieldName(JSON.Values['FieldName'].Value);
        if Column <> nil then begin
          Column.Visible := JSON.GetValue<Boolean>('ColumnVisible');
          Column.Width := JSON.GetValue<Integer>('ColumnWidth');
          Column.Index := JSON.GetValue<Integer>('ColumnIndex');
        end;
      end;
    finally
      RootJSON.Free;
    end;
  finally
    StrList.Free;
  end;
end;

procedure TfrmLocalHistory.LoadTreeColumnFromJSON;
var
  StrList: TStringList;
  RootJSON, JSON: TJSONObject;
  JArr: TJSONArray;
  i: integer;
  Column: TcxTreeListColumn;
  Path: String;
  Width: Integer;
begin
  Path := ExtractFilePath(Application.ExeName);

  if not FileExists(Path + 'Settings.json') then
    Exit;

  StrList := TStringList.Create;
  try
    StrList.LoadFromFile(Path + 'Settings.json');

    RootJSON := TJSONObject.ParseJSONValue(StrList.Text) as TJSONObject;
    try
      JArr := RootJSON.Values['MainTree'] as TJSONArray;
      for i := 0 to JArr.Count - 1 do begin
        JSON := JArr.Items[i] as TJSONObject;

        Column := TreeList.ColumnByName(JSON.Values['ColumnName'].Value.Replace('clm', 'clmTree'));
        if Column <> nil then begin
          Column.Visible := JSON.GetValue<Boolean>('ColumnVisible');
          Width := JSON.GetValue<Integer>('ColumnWidth', 0);
          if Width > 0 then
            Column.Width := Width;
        end;
      end;
    finally
      RootJSON.Free;
    end;
  finally
    StrList.Free;
  end;
end;

procedure TfrmLocalHistory.OnCommandShowLocalHistory(const aValue: TValue);
begin
  Self.Show;
end;

procedure TfrmLocalHistory.TreeListCustomDrawDataCell(Sender: TcxCustomTreeList;
  ACanvas: TcxCanvas; AViewInfo: TcxTreeListEditCellViewInfo;
  var ADone: Boolean);
begin
  if (AViewInfo.Column.Name = 'clmTreeFreeSpace') and GetSettings.IsColoredFreeSpace then begin
    var Value := AViewInfo.DisplayValue;
    if Value <> null then begin
      ACanvas.Brush.Color := $d4f9d4;
      if Value < GetSettings.RedColorValue then
        ACanvas.Brush.Color := RGB(255, 236, 230)
      else if Value < GetSettings.YellowColorValue then
        ACanvas.Brush.Color := RGB(255, 246, 189);
    end;
  end;

  if (AViewInfo.Column.Name = 'clmTreeTBMonth') and GetSettings.IsColoredTBMDeviation then begin
    var Value := AViewInfo.DisplayValue;
    if Value <> null then begin
      var vUsedSpace := AViewInfo.Node.Values[clmTreeUsedSpace.Position.ColIndex];
      var UsedSpace := 0;
      if vUsedSpace <> null then
        UsedSpace := vUsedSpace;

      if UsedSpace > 0 then begin
        var Ratio: Double := (Value * 1000) / UsedSpace * 100;

        var IsDeviationTresholdedForYellow := Ratio < (100 - GetSettings.LessThanDeviationPercent);
        var IsDeviationTresholdedForRed := Ratio > (100 + GetSettings.MoreThanDeviationPercent);

        ACanvas.Brush.Color := $d4f9d4;
        if IsDeviationTresholdedForRed then
          ACanvas.Brush.Color := RGB(255, 236, 230)
        else if IsDeviationTresholdedForYellow then
          ACanvas.Brush.Color := RGB(255, 246, 189);
      end;
    end;
  end;
end;

end.
