﻿unit ufrmMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxCustomData, cxStyles, cxTL, cxTextEdit,
  cxTLdxBarBuiltInMenu, dxSkinsCore, cxInplaceContainer,
  cxDataControllerConditionalFormattingRulesManagerDialog, cxCurrencyEdit,
  cxClasses, dxSkinsDefaultPainters, dxLayoutLookAndFeels, dxSkinsForm,
  dxLayoutContainer, dxLayoutControl, Vcl.Menus, dxLayoutControlAdapters,
  Vcl.StdCtrls, cxButtons, dxSkiniMaginary, cxProgressBar, dxSkinSevenClassic,
  Storj.Types, Storj.InfoGetter, dxLayoutcxEditAdapters, cxContainer, cxEdit,
  cxLabel, cxMaskEdit, cxCheckBox, System.JSON, dxStatusBar,
  Vcl.ComCtrls, cxMemo, Vcl.ExtCtrls, uVersionGetter,
  System.UITypes, dxCore, System.StrUtils, System.Generics.Collections,
  dxSkinBasic, dxSkinSeven,
  dxScrollbarAnnotations, dxSkinOffice2013LightGray, dxSkinOffice2019DarkGray,
  dxSkinOffice2019White, cxDropDownEdit, uframeButtons, uNotifyCenter,
  uframeStatusBar, dxSkinOffice2019Black, dxSkinWhiteprint, dxSkinWXI, cxFilter;

type
  TcxTreeListAccess = class (TcxTreeList);

  TfrmMain = class(TForm)
    dxLayoutControlGroup_Root: TdxLayoutGroup;
    dxLayoutControl: TdxLayoutControl;
    layTreeList: TdxLayoutItem;
    TreeList: TcxTreeList;
    clmTitle: TcxTreeListColumn;
    clmAddress: TcxTreeListColumn;
    clmPort: TcxTreeListColumn;
    clmSatellite: TcxTreeListColumn;
    clmAudits: TcxTreeListColumn;
    clmEgress: TcxTreeListColumn;
    clmIngress: TcxTreeListColumn;
    clmRepairEgress: TcxTreeListColumn;
    clmRepairIngress: TcxTreeListColumn;
    clmTotalBandwidth: TcxTreeListColumn;
    clmTBMonth: TcxTreeListColumn;
    clmAllocatedSpace: TcxTreeListColumn;
    clmUsedSpace: TcxTreeListColumn;
    clmPayoutDirty: TcxTreeListColumn;
    clmPayoutCleanly: TcxTreeListColumn;
    mmoLog: TcxMemo;
    laymmoLog: TdxLayoutItem;
    clmFreeSpace: TcxTreeListColumn;
    laygrpBottomLogs: TdxLayoutGroup;
    mmoFinishedNodes: TcxMemo;
    laymmoFinishedNodes: TdxLayoutItem;
    clmVersion: TcxTreeListColumn;
    treePopupMenu: TPopupMenu;
    dxLayoutGroup1: TdxLayoutGroup;
    btnClearLog: TcxButton;
    laybtnClearLog: TdxLayoutItem;
    dxLayoutGroup2: TdxLayoutGroup;
    clmEstimatedDirtyPayout: TcxTreeListColumn;
    clmEstimatedCleanlyPayout: TcxTreeListColumn;
    T1: TMenuItem;
    T2: TMenuItem;
    layFrameButtons: TdxLayoutItem;
    frameButtons: TframeButtons;
    layframeStatusBar: TdxLayoutItem;
    frameStatusBar: TframeStatusBar;
    Refreshnode1: TMenuItem;
    clmTrash: TcxTreeListColumn;
    btnNodeExecuteSSH: TMenuItem;
    clmFullPath: TcxTreeListColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnClearLogClick(Sender: TObject);
    procedure T1Click(Sender: TObject);
    procedure TreeListCustomDrawDataCell(Sender: TcxCustomTreeList;
      ACanvas: TcxCanvas; AViewInfo: TcxTreeListEditCellViewInfo;
      var ADone: Boolean);
    procedure T2Click(Sender: TObject);
    procedure TreeListCompare(Sender: TcxCustomTreeList; ANode1, ANode2: TcxTreeListNode; var ACompare: Integer);
    procedure FormDestroy(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure Refreshnode1Click(Sender: TObject);
    procedure btnNodeExecuteSSHClick(Sender: TObject);
  private
    procedure RecalcParent(AParentNode: TcxTreeListNode);
    procedure SetJArrTreeNodes(AJArr: TJSONArray; AParentNode: TcxTreeListNode);

    procedure SaveTreeColumnToJSON;
    procedure LoadTreeColumnFromJSON;

    procedure ResetSorting(const Data: TValue);
    procedure CommandRepaintNodes(const aData: TValue);
    procedure CommandBeforeStartGetNodeData(const aData: TValue);
    procedure CommandAfterGetNodeData(const aData: TValue);
    procedure CommandLog(const aData: TValue);
    procedure CommandShowNodeData(const aData: TValue);
    procedure CommandShowNodeDataGroup(const aData: TValue);
    procedure CommandLoadFromJSON(const aData: TValue);
  public
  end;

  //create updater
  //create installer

  //R&D cxDBTreeList

  //need fix in Avg.TBM calc, dates

  //tree view historical data 'on date'
  //effeciency by TBM to LocalHistory
  //effciency by used to efficiency by tbm
  //dashborad?
  // -- total allocated - used - free - trash
  // -- % разных версий нод
  // -- daily total
  // -- efficiency by used space and efficiency by TBM
  // -- % total difference between used and TBM
  // -- last time check counts: total nodes count, succesfull, error, offline
  //many charts by tabs?
  //LocalHistory to remote
  // -- LocalHistory restore from remote
  //avg node Profitable by estimated cleanly
  //% used to tb*m
  //% ingress to best ingress IP
  //Comparison data
  //colored "old/NotUpdated/Error" nodes in grid view and daily grid view and dashboard
  //colored "refreshed" updatime nodes in grid view and daily grid view and dashboard
  //uptime robot api

  //make multilang. add Ukr lang

  //chain to copy between disk and VM
  //target to store with progress and estimated date
  //advanced treeview

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

uses
  uDM, Storj.Settings, Storj.Consts, Storj.Common;

procedure TfrmMain.CommandRepaintNodes(const aData: TValue);
begin
  TcxTreeListAccess(TreeList).RefreshFields;
end;

procedure TfrmMain.btnClearLogClick(Sender: TObject);
begin
  mmoLog.Lines.Clear;
end;

procedure TfrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SaveTreeColumnToJSON;
end;

procedure TfrmMain.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := MessageDlg('Are you sure want close app?', mtConfirmation, [mbYes, mbNo], 0) = mrYes;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
//  dxSkinController.NativeStyle := False;
//  dxSkinController.SkinName := 'iMaginary'; 'SevenClassic'

  LoadTreeColumnFromJSON;

  TNotifyCenter.Instance.Subscribe(COMMAND_RESET_SORTING, ResetSorting);
  TNotifyCenter.Instance.Subscribe(COMMAND_SETTING_CHANGED, CommandRepaintNodes);
  TNotifyCenter.Instance.Subscribe(COMMAND_BEFORE_START_GET_NODE, CommandBeforeStartGetNodeData);
  TNotifyCenter.Instance.Subscribe(COMMAND_LOG, CommandLog);
  TNotifyCenter.Instance.Subscribe(COMMAND_SHOW_NODE_DATA, CommandShowNodeData);
  TNotifyCenter.Instance.Subscribe(COMMAND_SHOW_NODE_DATA_GROUP, CommandShowNodeDataGroup);
  TNotifyCenter.Instance.Subscribe(COMMAND_LOAD_FROM_JSON, CommandLoadFromJSON);
  TNotifyCenter.Instance.Subscribe(COMMAND_AFTER_GET_NODE_DATA, CommandAfterGetNodeData);

  TreeList.Clear;

  if GetSettings.IsMainFormFullScreen then
    Self.WindowState := TWindowState.wsMaximized;
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  TNotifyCenter.Instance.UnSubscribe(COMMAND_RESET_SORTING, ResetSorting);
  TNotifyCenter.Instance.UnSubscribe(COMMAND_SETTING_CHANGED, CommandRepaintNodes);
  TNotifyCenter.Instance.UnSubscribe(COMMAND_BEFORE_START_GET_NODE, CommandBeforeStartGetNodeData);
  TNotifyCenter.Instance.UnSubscribe(COMMAND_LOG, CommandLog);
  TNotifyCenter.Instance.UnSubscribe(COMMAND_SHOW_NODE_DATA, CommandShowNodeData);
  TNotifyCenter.Instance.UnSubscribe(COMMAND_SHOW_NODE_DATA_GROUP, CommandShowNodeDataGroup);
  TNotifyCenter.Instance.UnSubscribe(COMMAND_LOAD_FROM_JSON, CommandLoadFromJSON);
  TNotifyCenter.Instance.UnSubscribe(COMMAND_AFTER_GET_NODE_DATA, CommandAfterGetNodeData);
end;

procedure TfrmMain.CommandLoadFromJSON(const aData: TValue);
begin
  var JArr := aData.AsObject as TJSONArray;

  if not Assigned(JArr) then
    Exit;

  TreeList.BeginUpdate;
  try
    TreeList.Clear;
    SetJArrTreeNodes(JArr, TreeList.Root);
  finally
    TreeList.EndUpdate;
  end;
end;

procedure TfrmMain.LoadTreeColumnFromJSON;
var
  StrList: TStringList;
  RootJSON, JSON: TJSONObject;
  JArr: TJSONArray;
  i: integer;
  Column: TcxTreeListColumn;
  Path: String;
  Width: Integer;
begin
  Path := ExtractFilePath(Application.ExeName);

  if not FileExists(Path + 'Settings.json') then
    Exit;

  StrList := TStringList.Create;
  try
    StrList.LoadFromFile(Path + 'Settings.json');

    RootJSON := TJSONObject.ParseJSONValue(StrList.Text) as TJSONObject;
    try
      JArr := RootJSON.Values['MainTree'] as TJSONArray;
      for i := 0 to JArr.Count - 1 do begin
        JSON := JArr.Items[i] as TJSONObject;

        Column := TreeList.ColumnByName(JSON.Values['ColumnName'].Value);
        if Column <> nil then begin
          Column.Visible := JSON.GetValue<Boolean>('ColumnVisible');
          Width := JSON.GetValue<Integer>('ColumnWidth', 0);
          if Width > 0 then
            Column.Width := Width;
        end;
      end;
    finally
      RootJSON.Free;
    end;
  finally
    StrList.Free;
  end;
end;

procedure TfrmMain.CommandLog(const aData: TValue);
begin
  mmoLog.Lines.Add(aData.AsString);
end;

procedure TfrmMain.btnNodeExecuteSSHClick(Sender: TObject);
begin
  var FullPath := VarToStr(TreeList.FocusedNode.Values[clmFullPath.Position.ColIndex]);

  TNotifyCenter.Instance.SendMessage(COMMAND_SHOW_SSH_COMMAND_EXECUTE, FullPath);
end;

procedure TfrmMain.CommandAfterGetNodeData(const aData: TValue);
begin
  laybtnClearLog.Enabled := True;
end;

procedure TfrmMain.CommandBeforeStartGetNodeData(const aData: TValue);
begin
  mmoFinishedNodes.Lines.Clear;
  laybtnClearLog.Enabled := False;
end;

procedure TfrmMain.RecalcParent(AParentNode: TcxTreeListNode);
var
  i: integer;
  UsedSpace, AllocatedSpace, FreeSpace, Trash: Integer;
  ClearPayout, DirtyPayout, EstimatedDirtyPayout, EstimatedCleanlyPayout: Currency;
  Egress, RepairEgress, Ingress, RepairIngress, TotalBandwidth, TBMonth: Double;
begin
  if TreeList.Root.Equals(AParentNode) then
    Exit;

  TBMonth := 0;
  UsedSpace := 0;
  AllocatedSpace := 0;
  FreeSpace := 0;
  Trash := 0;
  ClearPayout := 0;
  DirtyPayout := 0;
  Egress := 0; RepairEgress := 0;
  Ingress := 0; RepairIngress := 0;
  TotalBandwidth := 0;
  EstimatedDirtyPayout := 0;
  EstimatedCleanlyPayout := 0;
  for i := 0 to AParentNode.Count - 1 do begin
    if AParentNode.Items[i].Values[clmUsedSpace.Position.ColIndex] <> null then
      UsedSpace := UsedSpace + AParentNode.Items[i].Values[clmUsedSpace.Position.ColIndex];

    if AParentNode.Items[i].Values[clmTrash.Position.ColIndex] <> null then
      Trash := Trash + AParentNode.Items[i].Values[clmTrash.Position.ColIndex];

    if AParentNode.Items[i].Values[clmAllocatedSpace.Position.ColIndex] <> null then
      AllocatedSpace := AllocatedSpace + AParentNode.Items[i].Values[clmAllocatedSpace.Position.ColIndex];

    if AParentNode.Items[i].Values[clmPayoutDirty.Position.ColIndex] <> null then
      DirtyPayout := DirtyPayout + AParentNode.Items[i].Values[clmPayoutDirty.Position.ColIndex];

    if AParentNode.Items[i].Values[clmPayoutCleanly.Position.ColIndex] <> null then
      ClearPayout := ClearPayout + AParentNode.Items[i].Values[clmPayoutCleanly.Position.ColIndex];

    if AParentNode.Items[i].Values[clmEgress.Position.ColIndex] <> null then
      Egress := Egress + AParentNode.Items[i].Values[clmEgress.Position.ColIndex];

    if AParentNode.Items[i].Values[clmIngress.Position.ColIndex] <> null then
      Ingress := Ingress + AParentNode.Items[i].Values[clmIngress.Position.ColIndex];

    if AParentNode.Items[i].Values[clmRepairEgress.Position.ColIndex] <> null then
      RepairEgress := RepairEgress + AParentNode.Items[i].Values[clmRepairEgress.Position.ColIndex];

    if AParentNode.Items[i].Values[clmRepairIngress.Position.ColIndex] <> null then
      RepairIngress := RepairIngress + AParentNode.Items[i].Values[clmRepairIngress.Position.ColIndex];

    if AParentNode.Items[i].Values[clmTotalBandwidth.Position.ColIndex] <> null then
      TotalBandwidth := TotalBandwidth + AParentNode.Items[i].Values[clmTotalBandwidth.Position.ColIndex];

    if AParentNode.Items[i].Values[clmFreeSpace.Position.ColIndex] <> null then
      FreeSpace := FreeSpace + AParentNode.Items[i].Values[clmFreeSpace.Position.ColIndex];

    if AParentNode.Items[i].Values[clmTBMonth.Position.ColIndex] <> null then
      TBMonth := TBMonth + AParentNode.Items[i].Values[clmTBMonth.Position.ColIndex];

    if AParentNode.Items[i].Values[clmEstimatedDirtyPayout.Position.ColIndex] <> null then
      EstimatedDirtyPayout := EstimatedDirtyPayout + AParentNode.Items[i].Values[clmEstimatedDirtyPayout.Position.ColIndex];

    if AParentNode.Items[i].Values[clmEstimatedCleanlyPayout.Position.ColIndex] <> null then
      EstimatedCleanlyPayout := EstimatedCleanlyPayout + AParentNode.Items[i].Values[clmEstimatedCleanlyPayout.Position.ColIndex];
  end;

  AParentNode.Values[clmAllocatedSpace.Position.ColIndex] := AllocatedSpace;
  AParentNode.Values[clmUsedSpace.Position.ColIndex] := UsedSpace;
  AParentNode.Values[clmPayoutDirty.Position.ColIndex] := DirtyPayout;
  AParentNode.Values[clmPayoutCleanly.Position.ColIndex] := ClearPayout;
  AParentNode.Values[clmEgress.Position.ColIndex] := Egress;
  AParentNode.Values[clmIngress.Position.ColIndex] := Ingress;
  AParentNode.Values[clmRepairEgress.Position.ColIndex] := RepairEgress;
  AParentNode.Values[clmRepairIngress.Position.ColIndex] := RepairIngress;
  AParentNode.Values[clmTotalBandwidth.Position.ColIndex] := TotalBandwidth;
  AParentNode.Values[clmFreeSpace.Position.ColIndex] := FreeSpace;
  AParentNode.Values[clmTBMonth.Position.ColIndex] := TBMonth;
  AParentNode.Values[clmEstimatedDirtyPayout.Position.ColIndex] := EstimatedDirtyPayout;
  AParentNode.Values[clmEstimatedCleanlyPayout.Position.ColIndex] := EstimatedCleanlyPayout;
  AParentNode.Values[clmTrash.Position.ColIndex] := Trash;

  RecalcParent(AParentNode.Parent);
end;

procedure TfrmMain.Refreshnode1Click(Sender: TObject);
var
  Node: TNode;
begin
  var Addr := VarToStr(TreeList.FocusedNode.Values[clmAddress.Position.ColIndex]);

  if Addr.IsEmpty then begin
    MessageDlg('Please, select your node with address', mtError, [mbOk], 0);
    Exit;
  end;

  Node.Address := Addr;
  Node.Title := VarToStr(TreeList.FocusedNode.Values[clmTitle.Position.ColIndex]);
  Node.Port := TreeList.FocusedNode.Values[clmPort.Position.ColIndex];
  Node.IsUseInAdditionalStats := false;

  DM.LoadNodeData(Node);
end;

procedure TfrmMain.ResetSorting(const Data: TValue);
begin
  if not TreeList.Sorted then
    Exit;

  TreeList.ClearSorting;
  TreeList.Clear;
  DM.ReloadFromJSON;
  DM.LoadNodesData(TGetInfoMode.gimStandart, TLoadSourceMode.lsmManual);
end;

procedure TfrmMain.SaveTreeColumnToJSON;
var
  StrList: TStringList;
  JSON, RootJSON: TJSONObject;
  JArr: TJSONArray;
  i: integer;
begin
  var Path := ExtractFilePath(Application.ExeName);

  RootJSON := TJSONObject.Create;
  try
    JArr := TJSONArray.Create;
    for i := 0 to TreeList.ColumnCount - 1 do begin
      JSON := TJSONObject.Create;
      JSON.AddPair('ColumnName', TreeList.Columns[i].Name);
      JSON.AddPair('ColumnVisible', TJSONBool.Create(TreeList.Columns[i].Visible));
      JSON.AddPair('ColumnWidth', TJSONNumber.Create(TreeList.Columns[i].Width));
      JArr.AddElement(JSON);
    end;

    RootJSON.AddPair('MainTree', JArr);

    StrList := TStringList.Create;
    try
      StrList.Text := RootJSON.ToString;
      StrList.SaveToFile(Path + 'Settings.json');
    finally
      StrList.Free;
    end;
  finally
    RootJSON.Free;
  end;
end;

procedure TfrmMain.SetJArrTreeNodes(AJArr: TJSONArray; AParentNode: TcxTreeListNode);
var
  i: integer;
  JObj: TJSONObject;
  JValue: TJSONValue;
  Node: TcxTreeListNode;
begin
  for i := 0 to AJarr.Count - 1 do begin
    JObj := AJArr.Items[i] as TJSONObject;
    if not JObj.GetValue<Boolean>('IsActive') then
      Continue;

    Node := AParentNode.AddChild;
    Node.Values[clmTitle.Position.ColIndex] := JObj.GetValue<String>('Title');
    Node.Values[clmAddress.Position.ColIndex] := JObj.GetValue<String>('Address');
    Node.Values[clmPort.Position.ColIndex] := JObj.GetValue<String>('Port');
    Node.Values[clmFullPath.Position.ColIndex] := JObj.GetValue<String>('FullPath', '');

    JValue := JObj.GetValue<TJSONArray>('Childs', nil);
    if JValue <> nil then
      SetJArrTreeNodes(JValue as TJSONArray, Node);

    if JObj.GetValue<Boolean>('AutoExpand', False) then
      Node.Expanded := True;
  end;
end;

procedure TfrmMain.CommandShowNodeData(const aData: TValue);
  function GetNode(AParentNode: TcxTreeListNode; ANode: TNode): TcxTreeListNode;
  var
    i: integer;
  begin
    for i := 0 to AParentNode.Count - 1 do begin
      Result := AParentNode.Items[i];
      if (Result.Values[clmAddress.Position.ColIndex] = ANode.Address) and
         (Result.Values[clmPort.Position.ColIndex] = ANode.Port) then
        Exit(Result);

      if Result.HasChildren then
        Result := GetNode(Result, ANode)
      else
        Result := nil;
      if Result <> nil then
        Exit(Result);
    end;

    Result := nil;
  end;

var
  Satellite: TSatellite;
  Node, SatNode: TcxTreeListNode;
  NodeData: TNodeData;
begin
  NodeData := aData.AsType<TNodeData>;

  mmoFinishedNodes.Lines.Add('Work time: ' + NodeData.WorkTimeMs.ToString + ' ms - ' +
                             NodeData.Node.Address + ':' + NodeData.Node.Port.ToString + ' - ' +
                             NodeData.Node.Title);
  TreeList.BeginUpdate;
  try
    Node := GetNode(TreeList.Root, NodeData.Node);
    if Node = nil then
      Node := TreeList.Root.AddChild;

    Node.Values[clmTitle.Position.ColIndex] := NodeData.Node.Title;
    Node.Values[clmAddress.Position.ColIndex] := NodeData.Node.Address;
    Node.Values[clmPort.Position.ColIndex] := NodeData.Node.Port;
    Node.Values[clmAllocatedSpace.Position.ColIndex] := NodeData.DiskSpace.Available;
    Node.Values[clmUsedSpace.Position.ColIndex] := NodeData.DiskSpace.Used;
    Node.Values[clmFreeSpace.Position.ColIndex] := NodeData.DiskSpace.Free;
    Node.Values[clmPayoutDirty.Position.ColIndex] := NodeData.DirtyPayout;
    Node.Values[clmPayoutCleanly.Position.ColIndex] := NodeData.ClearlyPayout;
    Node.Values[clmEgress.Position.ColIndex] := NodeData.Egress;
    Node.Values[clmIngress.Position.ColIndex] := NodeData.Ingress;
    Node.Values[clmRepairEgress.Position.ColIndex] := NodeData.RepairEgress;
    Node.Values[clmRepairIngress.Position.ColIndex] := NodeData.RepairIngress;
    Node.Values[clmTotalBandwidth.Position.ColIndex] := NodeData.TotalBandwidth;
    Node.Values[clmVersion.Position.ColIndex] := NodeData.Version;
    Node.Values[clmTBMonth.Position.ColIndex] := NodeData.TBMonth;
    Node.Values[clmEstimatedDirtyPayout.Position.ColIndex] := NodeData.EstimatedPayoutDirty;
    Node.Values[clmEstimatedCleanlyPayout.Position.ColIndex] := NodeData.EstimatedPayoutCleanly;
    Node.Values[clmTrash.Position.ColIndex] := NodeData.DiskSpace.Trash;

    Node.DeleteChildren;
    for Satellite in NodeData.Satellites do begin
      SatNode := Node.AddChild;
      SatNode.Values[clmSatellite.Position.ColIndex] := Satellite.Title;
      SatNode.Values[clmAudits.Position.ColIndex] := Satellite.Audit;
      SatNode.Values[clmEgress.Position.ColIndex] := Satellite.Egress;
      SatNode.Values[clmIngress.Position.ColIndex] := Satellite.Ingress;
      SatNode.Values[clmRepairEgress.Position.ColIndex] := Satellite.RepairEgress;
      SatNode.Values[clmRepairIngress.Position.ColIndex] := Satellite.RepairIngress;
      //SatNode.Values[clmUsedSpace.Position.ColIndex] := Satellite.StorageUsed;
      SatNode.Values[clmTotalBandwidth.Position.ColIndex] := Satellite.TotalBandwidth;
      SatNode.Values[clmTBMonth.Position.ColIndex] := Satellite.TBMonth;
    end;

    RecalcParent(Node.Parent);
  finally
    TreeList.EndUpdate;
  end;
end;

procedure TfrmMain.CommandShowNodeDataGroup(const aData: TValue);
begin
  var NodeDataArray := aData.AsType<TNodeDataArray>;

  if Length(NodeDataArray) = 0 then
    Exit;

  TreeList.BeginUpdate;
  mmoFinishedNodes.Lines.BeginUpdate;
  try
    for var NodeData in NodeDataArray do
      CommandShowNodeData(TValue.From<TNodeData>(NodeData));

    mmoFinishedNodes.InnerControl.Perform(EM_LINESCROLL, 0,  mmoFinishedNodes.Lines.Count);
  finally
    mmoFinishedNodes.Lines.EndUpdate;
    TreeList.EndUpdate;
  end;
end;

procedure TfrmMain.T1Click(Sender: TObject);
var
  Address, Port: String;
begin
  Address := VarToStr(TreeList.FocusedNode.Values[clmAddress.Position.ColIndex]);
  Port := VarToStr(TreeList.FocusedNode.Values[clmPort.Position.ColIndex]);

  if Address.IsEmpty or Port.IsEmpty then begin
    MessageDlg('Please, select your node with address and port', mtError, [mbOk], 0);
    Exit;
  end;

  OpenURL('http://' + Address + ':' + Port);
end;

procedure TfrmMain.T2Click(Sender: TObject);
begin
  var Addr := VarToStr(TreeList.FocusedNode.Values[clmAddress.Position.ColIndex]);

  if Addr.IsEmpty then begin
    MessageDlg('Please, select your node with address', mtError, [mbOk], 0);
    Exit;
  end;

  TNotifyCenter.Instance.SendMessage(COMMAND_SHOW_CHECK_NEIGHBOURS, Addr);
end;

procedure TfrmMain.TreeListCustomDrawDataCell(Sender: TcxCustomTreeList;
  ACanvas: TcxCanvas; AViewInfo: TcxTreeListEditCellViewInfo;
  var ADone: Boolean);
begin
  if (AViewInfo.Column.Name = 'clmFreeSpace') and GetSettings.IsColoredFreeSpace then begin
    var Value := AViewInfo.DisplayValue;
    if Value <> null then begin
      ACanvas.Brush.Color := $d4f9d4;
      if Value < GetSettings.RedColorValue then
        ACanvas.Brush.Color := RGB(255, 236, 230)
      else if Value < GetSettings.YellowColorValue then
        ACanvas.Brush.Color := RGB(255, 246, 189);
    end;
  end;

  if (AViewInfo.Column.Name = 'clmTBMonth') and GetSettings.IsColoredTBMDeviation then begin
    var Value := AViewInfo.DisplayValue;
    if Value <> null then begin
      var vUsedSpace := AViewInfo.Node.Values[clmUsedSpace.Position.ColIndex];
      var UsedSpace := 0;
      if vUsedSpace <> null then
        UsedSpace := vUsedSpace;

      if UsedSpace > 0 then begin
        var Ratio: Double := (Value * 1000) / UsedSpace * 100;

        var IsDeviationTresholdedForYellow := Ratio < (100 - GetSettings.LessThanDeviationPercent);
        var IsDeviationTresholdedForRed := Ratio > (100 + GetSettings.MoreThanDeviationPercent);

        ACanvas.Brush.Color := $d4f9d4;
        if IsDeviationTresholdedForRed then
          ACanvas.Brush.Color := RGB(255, 236, 230)
        else if IsDeviationTresholdedForYellow then
          ACanvas.Brush.Color := RGB(255, 246, 189);
      end;
    end;
  end;
end;

procedure TfrmMain.TreeListCompare(Sender: TcxCustomTreeList; ANode1,
  ANode2: TcxTreeListNode; var ACompare: Integer);
var
  V1, V2: Variant;
  Column: TcxTreeListColumn;
begin
  Column := TreeList.HitTest.HitColumn;

  if Column = nil then
    Exit;

  ACompare := 0;
  if MatchText(Column.Name, ['clmAudits', 'clmPort', 'clmEgress', 'clmIngress', 'clmRepairIngress', 'clmRepairEgress',
                             'clmTotalBandwidth', 'clmTBMonth', 'clmAllocatedSpace', 'clmUsedSpace', 'clmFreeSpace',
                             'clmPayoutDirty', 'clmPayoutCleanly', 'clmEstimatedDirtyPayout', 'clmEstimatedCleanlyPayout']) then begin
    if (ANode1.Values[Column.Position.ColIndex] <> null) then
      V1 := ANode1.Values[Column.Position.ColIndex]
    else
      V1 := 0;

    if (ANode2.Values[Column.Position.ColIndex] <> null) then
      V2 := ANode2.Values[Column.Position.ColIndex]
    else
      V2 := 0;

    if VarToStr(V1).Trim.Length = 0 then
      V1 := 0;

    if VarToStr(V2).Trim.Length = 0 then
      V2 := 0;

    var D1: Double := V1;
    var D2: Double := V2;
    if D1 > D2 then
      ACompare := 1
    else if D1 < D2 then
      ACompare := -1;
  end else if MatchText(Column.Name, ['clmTitle', 'clmAddress', 'clmSatellite', 'clmVersion']) then begin
    if (ANode1.Values[Column.Position.ColIndex] <> null) then
      V1 := ANode1.Values[Column.Position.ColIndex]
    else
      V1 := '';

    if (ANode2.Values[Column.Position.ColIndex] <> null) then
      V2 := ANode2.Values[Column.Position.ColIndex]
    else
      V2 := '';

    ACompare := CompareText(V1, V2);
  end;

  if Column.SortOrder = TdxSortOrder.soDescending then
    ACompare := -ACompare;
end;

end.
