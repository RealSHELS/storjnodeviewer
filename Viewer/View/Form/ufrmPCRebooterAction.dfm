object frmPCRebooterAction: TfrmPCRebooterAction
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Reboot PC'
  ClientHeight = 286
  ClientWidth = 178
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxLayoutControl: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 178
    Height = 286
    Align = alClient
    TabOrder = 0
    object comboPCRebooters: TcxComboBox
      Left = 60
      Top = 10
      Properties.DropDownListStyle = lsFixedList
      Properties.OnEditValueChanged = comboPCRebootersPropertiesEditValueChanged
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 0
      Width = 108
    end
    object pnlActionButtons: TFlowPanel
      Left = 10
      Top = 37
      Width = 158
      Height = 239
      TabOrder = 1
      object cxButton1: TcxButton
        AlignWithMargins = True
        Left = 4
        Top = 4
        Width = 141
        Height = 29
        Align = alClient
        Anchors = [akLeft, akTop, akRight]
        Caption = 'cxButton1'
        TabOrder = 0
      end
    end
    object dxLayoutControlGroup_Root: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avClient
      ButtonOptions.Buttons = <>
      Hidden = True
      ShowBorder = False
      Index = -1
    end
    object laycomboPCRebooters: TdxLayoutItem
      Parent = dxLayoutControlGroup_Root
      CaptionOptions.Text = 'Rebooter'
      Control = comboPCRebooters
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object laypnlActionButtons: TdxLayoutItem
      Parent = dxLayoutControlGroup_Root
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'FlowPanel1'
      CaptionOptions.Visible = False
      Control = pnlActionButtons
      ControlOptions.AutoColor = True
      ControlOptions.OriginalHeight = 41
      ControlOptions.OriginalWidth = 185
      ControlOptions.ShowBorder = False
      Index = 1
    end
  end
end
