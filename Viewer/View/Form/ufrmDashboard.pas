unit ufrmDashboard;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBasic, dxSkiniMaginary, dxSkinSeven,
  dxSkinSevenClassic, dxSkinWXI, cxClasses, dxLayoutContainer, dxLayoutControl,
  cxGeometry, cxVariants, dxCustomData, cxCustomCanvas, dxCoreGraphics,
  dxChartCore, dxChartData, dxChartLegend, dxChartSimpleDiagram,
  dxChartXYDiagram, dxChartXYSeriesLineView, dxChartXYSeriesAreaView,
  dxChartMarkers, dxChartXYSeriesBarView, dxCoreClasses, dxChartControl,
  uDM, dxChartDBData, Storj.Consts, uNotifyCenter;

type
  TfrmDashboard = class(TForm)
    dxLayoutControlGroup_Root: TdxLayoutGroup;
    dxLayoutControl: TdxLayoutControl;
    chartSpace: TdxChartControl;
    laychartSpace: TdxLayoutItem;
    chartSpaceSimpleDiagram: TdxChartSimpleDiagram;
    chartSpaceSimpleSeries: TdxChartSimpleSeries;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure chartSpaceSimpleDiagramGetValueLabelDrawParameters(
      Sender: TdxChartCustomDiagram;
      AArgs: TdxChartGetValueLabelDrawParametersEventArgs);
    procedure chartSpaceSimpleDiagramGetTotalLabelDrawParameters(
      Sender: TdxChartCustomDiagram;
      AArgs: TdxChartGetTotalLabelDrawParametersEventArgs);
  private
    procedure OnCommandShow(const aValue: TValue);
  public
    { Public declarations }
  end;

var
  frmDashboard: TfrmDashboard;

implementation

{$R *.dfm}

procedure TfrmDashboard.chartSpaceSimpleDiagramGetTotalLabelDrawParameters(
  Sender: TdxChartCustomDiagram;
  AArgs: TdxChartGetTotalLabelDrawParametersEventArgs);
begin
  AArgs.Text := 'Total: ' + sLineBreak + FormatFloat('# ### ###', AArgs.TotalValue) + ' GB';
end;

procedure TfrmDashboard.chartSpaceSimpleDiagramGetValueLabelDrawParameters(
  Sender: TdxChartCustomDiagram;
  AArgs: TdxChartGetValueLabelDrawParametersEventArgs);
var
  Total: Integer;
begin
  Total := 0;
  for var i := 0 to AArgs.SeriesPoint.Series.Points.Count - 1 do
    Total := Total + AArgs.SeriesPoint.Series.Points.Values[i];

  AArgs.Text := AArgs.SeriesPoint.Argument + ': ' + FormatFloat('# ### ###', AArgs.SeriesPoint.Value) + ' GB ('
    + FormatFloat('#0.## %', AArgs.SeriesPoint.Value / Total * 100) + ')';
end;

procedure TfrmDashboard.FormCreate(Sender: TObject);
begin
  TNotifyCenter.Instance.Subscribe(COMMAND_SHOW_DASHBOARD, OnCommandShow);
end;

procedure TfrmDashboard.FormDestroy(Sender: TObject);
begin
  TNotifyCenter.Instance.Unsubscribe(COMMAND_SHOW_DASHBOARD, OnCommandShow);
end;

procedure TfrmDashboard.OnCommandShow(const aValue: TValue);
begin
  Self.Show;
end;

end.
