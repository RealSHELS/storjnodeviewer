object frmSetting: TfrmSetting
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Settings'
  ClientHeight = 528
  ClientWidth = 286
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  TextHeight = 13
  object dxLayoutControl: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 286
    Height = 528
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 282
    ExplicitHeight = 527
    object chkColoredFreeSpace: TcxCheckBox
      Left = 24
      Top = 44
      Caption = 'Colored Free Space column'
      Properties.Alignment = taLeftJustify
      Properties.OnEditValueChanged = chkColoredFreeSpacePropertiesEditValueChanged
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 0
    end
    object btnSave: TcxButton
      Left = 65
      Top = 493
      Width = 75
      Height = 25
      Caption = 'Save'
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'iMaginary'
      TabOrder = 36
      OnClick = btnSaveClick
    end
    object btnCancel: TcxButton
      Left = 146
      Top = 493
      Width = 75
      Height = 25
      Caption = 'Cancel'
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'iMaginary'
      TabOrder = 37
      OnClick = btnCancelClick
    end
    object curRedValue: TcxCurrencyEdit
      Left = 159
      Top = 85
      EditValue = 200.000000000000000000
      Properties.DisplayFormat = '##0 GB'
      Properties.MaxValue = 999.000000000000000000
      Properties.MinValue = 1.000000000000000000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 1
      Width = 91
    end
    object curYellowValue: TcxCurrencyEdit
      Left = 159
      Top = 112
      EditValue = 550.000000000000000000
      Properties.DisplayFormat = '##0 GB'
      Properties.MaxValue = 999.000000000000000000
      Properties.MinValue = 1.000000000000000000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 2
      Width = 91
    end
    object curTimeout: TcxCurrencyEdit
      Left = 134
      Top = 258
      Properties.DisplayFormat = '### ##0 miliseconds'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 6
      Width = 128
    end
    object edtHealthSuspensionRed: TcxCurrencyEdit
      Left = 10000
      Top = 10000
      Hint = 'Red color when less than'
      EditValue = 75
      ParentShowHint = False
      Properties.DisplayFormat = '##0 %'
      ShowHint = True
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 23
      Visible = False
      Width = 69
    end
    object edtHealthSuspensionYellow: TcxCurrencyEdit
      Left = 10000
      Top = 10000
      Hint = 'Yellow color when less than'
      EditValue = 90
      ParentShowHint = False
      Properties.DisplayFormat = '##0 %'
      ShowHint = True
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 24
      Visible = False
      Width = 71
    end
    object edtHealthOnlineYellow: TcxCurrencyEdit
      Left = 10000
      Top = 10000
      Hint = 'Yellow color when less than'
      EditValue = 95
      ParentShowHint = False
      Properties.DisplayFormat = '##0 %'
      ShowHint = True
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 20
      Visible = False
      Width = 71
    end
    object edtHealthOnlineRed: TcxCurrencyEdit
      Left = 10000
      Top = 10000
      Hint = 'Red color when less than'
      EditValue = 80
      ParentShowHint = False
      Properties.DisplayFormat = '##0 %'
      ShowHint = True
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 19
      Visible = False
      Width = 69
    end
    object edtHealthAuditYellow: TcxCurrencyEdit
      Left = 10000
      Top = 10000
      Hint = 'Yellow color when less than'
      EditValue = 99
      ParentShowHint = False
      Properties.DisplayFormat = '##0 %'
      ShowHint = True
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 16
      Visible = False
      Width = 71
    end
    object edtHealthAuditRed: TcxCurrencyEdit
      Left = 10000
      Top = 10000
      Hint = 'Red color when less than'
      EditValue = 98
      ParentShowHint = False
      Properties.DisplayFormat = '##0 %'
      ShowHint = True
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 15
      Visible = False
      Width = 69
    end
    object chkHealthHotData: TcxCheckBox
      Left = 10000
      Top = 10000
      Caption = 'Display health hot data'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 27
      Visible = False
    end
    object chkSuspensionRedLog: TcxCheckBox
      Left = 10000
      Top = 10000
      Caption = 'Log red nodes'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 26
      Visible = False
    end
    object chkSuspensionYellowLog: TcxCheckBox
      Left = 10000
      Top = 10000
      Caption = 'Log yellow nodes'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 25
      Visible = False
    end
    object chkHealthOnlineRedLog: TcxCheckBox
      Left = 10000
      Top = 10000
      Caption = 'Log red nodes'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 22
      Visible = False
    end
    object chkHealthOnlineYellowLog: TcxCheckBox
      Left = 10000
      Top = 10000
      Caption = 'Log yellow nodes'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 21
      Visible = False
    end
    object chkHealthAuditRedLog: TcxCheckBox
      Left = 10000
      Top = 10000
      Caption = 'Log red nodes'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 18
      Visible = False
    end
    object chkHealthAuditYellowLog: TcxCheckBox
      Left = 10000
      Top = 10000
      Caption = 'Log yellow nodes'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 17
      Visible = False
    end
    object chkIsDisplayStorjNetUsed: TcxCheckBox
      Left = 24
      Top = 312
      Caption = 'Display Storj net used'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 8
    end
    object numOnlineRangeMinutes: TcxCurrencyEdit
      Left = 134
      Top = 285
      Properties.DisplayFormat = '### ##0 minutes'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 7
      Width = 128
    end
    object chkIsEnabledPCRebooter: TcxCheckBox
      Left = 10000
      Top = 10000
      Caption = 'Enable PCRebooter'
      Properties.OnEditValueChanged = chkIsEnabledPCRebooterPropertiesEditValueChanged
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 28
      Visible = False
    end
    object btnAddPCRebooter: TcxButton
      Left = 10000
      Top = 10000
      Width = 112
      Height = 25
      Caption = 'Add PCRebooter'
      TabOrder = 29
      Visible = False
      OnClick = btnAddPCRebooterClick
    end
    object btnRemovePCRebooter: TcxButton
      Left = 10000
      Top = 10000
      Width = 111
      Height = 25
      Caption = 'Remove non active'
      TabOrder = 30
      Visible = False
      OnClick = btnRemovePCRebooterClick
    end
    object scrollPCRebooters: TScrollBox
      Left = 10000
      Top = 10000
      Width = 225
      Height = 337
      BevelEdges = []
      BevelInner = bvNone
      BorderStyle = bsNone
      ParentBackground = True
      TabOrder = 31
      Visible = False
      OnMouseWheelDown = scrollPCRebootersMouseWheelDown
      OnMouseWheelUp = scrollPCRebootersMouseWheelUp
      object pnlPCRebooters: TFlowPanel
        Left = 0
        Top = 0
        Width = 225
        Height = 41
        Align = alTop
        AutoSize = True
        TabOrder = 0
      end
    end
    object chkColoredTBM: TcxCheckBox
      Left = 24
      Top = 151
      Caption = 'Colored TB*M by deviation'
      Properties.OnEditValueChanged = chkColoredTBMPropertiesEditValueChanged
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 3
    end
    object edtLowerDeviationTreshold: TcxCurrencyEdit
      Left = 159
      Top = 192
      EditValue = 10.000000000000000000
      Properties.AssignedValues.MinValue = True
      Properties.DecimalPlaces = 0
      Properties.DisplayFormat = '##0 %'
      Properties.MaxValue = 99.000000000000000000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 4
      Width = 91
    end
    object edtHigherDeviationTreshold: TcxCurrencyEdit
      Left = 159
      Top = 219
      EditValue = 0.000000000000000000
      Properties.AssignedValues.MinValue = True
      Properties.DecimalPlaces = 0
      Properties.DisplayFormat = '##0 %'
      Properties.MaxValue = 99.000000000000000000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 5
      Width = 91
    end
    object chkMainFormFullScreen: TcxCheckBox
      Left = 24
      Top = 335
      Caption = 'Main window in Full screen at app start'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 9
    end
    object chkReloadNodesTimer: TcxCheckBox
      Left = 24
      Top = 358
      Caption = 'Enable auto refresh nodes data by timer'
      Properties.OnEditValueChanged = chkReloadNodesTimerPropertiesEditValueChanged
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 10
    end
    object edtReloadNodesTimerMinutes: TcxCurrencyEdit
      Left = 134
      Top = 381
      Properties.DisplayFormat = '### ##0 minutes'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 11
      Width = 128
    end
    object chkUseTreePathForLog: TcxCheckBox
      Left = 24
      Top = 408
      Caption = 'Use TreePath for log instead node title'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 12
    end
    object chkUseProxyForGetNeightbours: TcxCheckBox
      Left = 24
      Top = 431
      Caption = 'Use proxy for get neightbours'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 13
    end
    object chkNotifyTelegramEnable: TcxCheckBox
      Left = 10000
      Top = 10000
      Hint = 
        '[B]Bold text[/B]'#13#10'[I]dsfsd[/I]'#13#10'[BACKCOLOR=#80FF00]dfsdf'#13#10'[/BACK' +
        'COLOR]'#13#10
      Caption = 'Enable notifications by telegram bot'
      ParentShowHint = False
      ShowHint = False
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 34
      Visible = False
    end
    object chkNotifyEmailEnable: TcxCheckBox
      Left = 10000
      Top = 10000
      Caption = 'Enable notifications by email'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 32
      Visible = False
    end
    object edtNotifyEmail: TcxTextEdit
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 33
      TextHint = 'username@gmail.com'
      Visible = False
      Width = 173
    end
    object chkUseNodeRefreshBuffer: TcxCheckBox
      Left = 24
      Top = 454
      Caption = 'Use node refresh buffer'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 14
    end
    object edtNotifyTelegramUserID: TcxTextEdit
      Left = 10000
      Top = 10000
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 35
      TextHint = '123456789'
      Visible = False
      Width = 173
    end
    object imgTelegramNotifyHelp: TImage
      Left = 10000
      Top = 10000
      Width = 20
      Height = 20
      Hint = 
        '[U]1.[/U] Open the Telegram app'#13#10'[U]2.[/U] In the search bar, ty' +
        'pe [B]@userinfobot[/B]'#13#10'[U]3.[/U] Select the bot and press the "' +
        '[B]Start[/B]" button'#13#10'[U]4.[/U] The bot will send your public da' +
        'ta'#13#10'[U]5.[/U] Copy your [B]ID[/B] and paste it into the [B]UserI' +
        'D[/B] field'#13#10'[U]6.[/U] Next, in the search bar, type [B]@StorjNo' +
        'deViewerBot[/B]'#13#10'[U]7.[/U] Select the bot and press the "[B]Star' +
        't[/B]" button'#13#10#13#10'That'#39's it, the settings are finished. Press "Sa' +
        've"'#13#10#13#10'[COLOR=#FF0000]Note that you need to enable "auto refresh' +
        ' nodes data by the timer" option in Common tab.'#13#10'The notificatio' +
        'ns works only during auto-refresh triggered by the timer.[/COLOR' +
        ']'
      ParentShowHint = False
      Picture.Data = {
        0D546478536D617274496D6167653C3F786D6C2076657273696F6E3D22312E30
        2220656E636F64696E673D225554462D38223F3E0D0A3C737667207665727369
        6F6E3D22312E31222069643D224C617965725F312220786D6C6E733D22687474
        703A2F2F7777772E77332E6F72672F323030302F7376672220786D6C6E733A78
        6C696E6B3D22687474703A2F2F7777772E77332E6F72672F313939392F786C69
        6E6B2220783D223070782220793D22307078222076696577426F783D22302030
        20333220333222207374796C653D22656E61626C652D6261636B67726F756E64
        3A6E6577203020302033322033323B2220786D6C3A73706163653D2270726573
        65727665223E262331333B262331303B3C7374796C6520747970653D22746578
        742F6373732220786D6C3A73706163653D227072657365727665223E2E426C61
        636B7B66696C6C3A233733373337343B7D262331333B262331303B2623393B2E
        59656C6C6F777B66696C6C3A234643423031423B7D262331333B262331303B26
        23393B2E477265656E7B66696C6C3A233132394334393B7D262331333B262331
        303B2623393B2E426C75657B66696C6C3A233338374342373B7D262331333B26
        2331303B2623393B2E5265647B66696C6C3A234430323132373B7D262331333B
        262331303B2623393B2E57686974657B66696C6C3A234646464646463B7D2623
        31333B262331303B2623393B2E7374307B6F7061636974793A302E353B7D2623
        31333B262331303B2623393B2E7374317B6F7061636974793A302E37353B7D26
        2331333B262331303B2623393B2E7374327B6F7061636974793A302E32353B7D
        262331333B262331303B2623393B2E7374337B646973706C61793A6E6F6E653B
        66696C6C3A233733373337343B7D3C2F7374796C653E0D0A3C7061746820636C
        6173733D22426C75652220643D224D31362C3243382E332C322C322C382E332C
        322C313673362E332C31342C31342C31347331342D362E332C31342D31345332
        332E372C322C31362C327A204D31362C3234632D312E312C302D322D302E392D
        322D3273302E392D322C322D3273322C302E392C322C3220202623393B533137
        2E312C32342C31362C32347A204D32302E372C31332E33632D302E322C302E34
        2D302E342C302E372D302E362C31632D302E332C302E332D302E352C302E352D
        302E382C302E37732D302E362C302E342D302E392C302E36632D302E332C302E
        322D302E352C302E342D302E372C302E3720202623393B632D302E322C302E33
        2D302E332C302E362D302E342C31563138682D322E37762D302E3963302D302E
        362C302E322D312C302E332D312E3463302E322D302E342C302E342D302E372C
        302E362D3163302E322D302E332C302E352D302E352C302E382D302E3773302E
        352D302E342C302E382D302E3620202623393B63302E322D302E322C302E342D
        302E342C302E362D302E3663302E312D302E322C302E322D302E352C302E322D
        302E3963302D302E362D302E322D312D302E352D312E33732D302E372D302E34
        2D312E332D302E34632D302E342C302D302E372C302E312D302E392C302E3220
        202623393B632D302E332C302E312D302E352C302E332D302E372C302E35732D
        302E332C302E352D302E342C302E384331342C31322E312C31342C31322E372C
        31342C3133682D3363302D302E372C302E312D312E362C302E342D322E316330
        2E322D302E362C302E362D312E312C312D312E3573302E392D302E382C312E35
        2D3120202623393B73312E332D302E342C322D302E3463312C302C312E382C30
        2E312C322E342C302E3473312E322C302E362C312E362C302E3963302E342C30
        2E342C302E372C302E382C302E382C312E3273302E332C302E382C302E332C31
        2E324332312C31322E342C32302E392C31322E392C32302E372C31332E337A22
        2F3E0D0A3C2F7376673E0D0A}
      Proportional = True
      ShowHint = True
      Stretch = True
      Transparent = True
      Visible = False
    end
    object imgEmailNotifyHelp: TImage
      Left = 10000
      Top = 10000
      Width = 20
      Height = 20
      Hint = 
        '[COLOR=#FF0000]Note that you need to enable "auto refresh nodes ' +
        'data by the timer" option in Common tab.'#13#10'The notifications work' +
        's only during auto-refresh triggered by the timer.[/COLOR]'
      ParentShowHint = False
      Picture.Data = {
        0D546478536D617274496D6167653C3F786D6C2076657273696F6E3D22312E30
        2220656E636F64696E673D225554462D38223F3E0D0A3C737667207665727369
        6F6E3D22312E31222069643D224C617965725F312220786D6C6E733D22687474
        703A2F2F7777772E77332E6F72672F323030302F7376672220786D6C6E733A78
        6C696E6B3D22687474703A2F2F7777772E77332E6F72672F313939392F786C69
        6E6B2220783D223070782220793D22307078222076696577426F783D22302030
        20333220333222207374796C653D22656E61626C652D6261636B67726F756E64
        3A6E6577203020302033322033323B2220786D6C3A73706163653D2270726573
        65727665223E262331333B262331303B3C7374796C6520747970653D22746578
        742F6373732220786D6C3A73706163653D227072657365727665223E2E426C61
        636B7B66696C6C3A233733373337343B7D262331333B262331303B2623393B2E
        59656C6C6F777B66696C6C3A234643423031423B7D262331333B262331303B26
        23393B2E477265656E7B66696C6C3A233132394334393B7D262331333B262331
        303B2623393B2E426C75657B66696C6C3A233338374342373B7D262331333B26
        2331303B2623393B2E5265647B66696C6C3A234430323132373B7D262331333B
        262331303B2623393B2E57686974657B66696C6C3A234646464646463B7D2623
        31333B262331303B2623393B2E7374307B6F7061636974793A302E353B7D2623
        31333B262331303B2623393B2E7374317B6F7061636974793A302E37353B7D26
        2331333B262331303B2623393B2E7374327B6F7061636974793A302E32353B7D
        262331333B262331303B2623393B2E7374337B646973706C61793A6E6F6E653B
        66696C6C3A233733373337343B7D3C2F7374796C653E0D0A3C7061746820636C
        6173733D22426C75652220643D224D31362C3243382E332C322C322C382E332C
        322C313673362E332C31342C31342C31347331342D362E332C31342D31345332
        332E372C322C31362C327A204D31362C3234632D312E312C302D322D302E392D
        322D3273302E392D322C322D3273322C302E392C322C3220202623393B533137
        2E312C32342C31362C32347A204D32302E372C31332E33632D302E322C302E34
        2D302E342C302E372D302E362C31632D302E332C302E332D302E352C302E352D
        302E382C302E37732D302E362C302E342D302E392C302E36632D302E332C302E
        322D302E352C302E342D302E372C302E3720202623393B632D302E322C302E33
        2D302E332C302E362D302E342C31563138682D322E37762D302E3963302D302E
        362C302E322D312C302E332D312E3463302E322D302E342C302E342D302E372C
        302E362D3163302E322D302E332C302E352D302E352C302E382D302E3773302E
        352D302E342C302E382D302E3620202623393B63302E322D302E322C302E342D
        302E342C302E362D302E3663302E312D302E322C302E322D302E352C302E322D
        302E3963302D302E362D302E322D312D302E352D312E33732D302E372D302E34
        2D312E332D302E34632D302E342C302D302E372C302E312D302E392C302E3220
        202623393B632D302E332C302E312D302E352C302E332D302E372C302E35732D
        302E332C302E352D302E342C302E384331342C31322E312C31342C31322E372C
        31342C3133682D3363302D302E372C302E312D312E362C302E342D322E316330
        2E322D302E362C302E362D312E312C312D312E3573302E392D302E382C312E35
        2D3120202623393B73312E332D302E342C322D302E3463312C302C312E382C30
        2E312C322E342C302E3473312E322C302E362C312E362C302E3963302E342C30
        2E342C302E372C302E382C302E382C312E3273302E332C302E382C302E332C31
        2E324332312C31322E342C32302E392C31322E392C32302E372C31332E337A22
        2F3E0D0A3C2F7376673E0D0A}
      ShowHint = True
      Stretch = True
      Visible = False
    end
    object dxLayoutControlGroup_Root: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avClient
      Hidden = True
      ShowBorder = False
      Index = -1
    end
    object laygrpColoredFreeSpaceValues: TdxLayoutGroup
      Parent = laygrpCommonTab
      CaptionOptions.Text = 'Free Space Column settings'
      Index = 1
    end
    object laychkColoredFreeSpace: TdxLayoutItem
      Parent = laygrpCommonTab
      CaptionOptions.Text = 'cxCheckBox1'
      CaptionOptions.Visible = False
      Control = chkColoredFreeSpace
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 80
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object laybtnSave: TdxLayoutItem
      Parent = laygrpBottomControls
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnSave
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object laybtnCancel: TdxLayoutItem
      Parent = laygrpBottomControls
      CaptionOptions.Text = 'cxButton2'
      CaptionOptions.Visible = False
      Control = btnCancel
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object laycurRedValue: TdxLayoutItem
      Parent = laygrpColoredFreeSpaceValues
      CaptionOptions.Text = 'Red color less than'
      Control = curRedValue
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object laycurYellowValue: TdxLayoutItem
      Parent = laygrpColoredFreeSpaceValues
      CaptionOptions.Text = 'Yellow color less than'
      Control = curYellowValue
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object laygrpBottomControls: TdxLayoutGroup
      Parent = dxLayoutControlGroup_Root
      AlignHorz = ahCenter
      AlignVert = avBottom
      CaptionOptions.Text = 'New Group'
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 1
    end
    object laycurTimeout: TdxLayoutItem
      Parent = laygrpCommonTab
      CaptionOptions.Text = 'Timeout'
      Control = curTimeout
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 4
    end
    object layedtHealthSuspensionRed: TdxLayoutItem
      Parent = laygrpSuspensionPercent
      AlignHorz = ahClient
      CaptionOptions.Hint = 'Red color when less than'
      CaptionOptions.Text = 'Red'
      Control = edtHealthSuspensionRed
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 50
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object layedtHealthSuspensionYellow: TdxLayoutItem
      Parent = laygrpSuspensionPercent
      AlignHorz = ahClient
      CaptionOptions.Hint = 'Yellow color when less than'
      CaptionOptions.Text = 'Yellow'
      Control = edtHealthSuspensionYellow
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 50
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object layedtHealthOnlineYellow: TdxLayoutItem
      Parent = laygrpHealthOnlinePercent
      AlignHorz = ahClient
      CaptionOptions.Hint = 'Yellow color when less than'
      CaptionOptions.Text = 'Yellow'
      Control = edtHealthOnlineYellow
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 50
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object layedtHealthOnlineRed: TdxLayoutItem
      Parent = laygrpHealthOnlinePercent
      AlignHorz = ahClient
      CaptionOptions.Hint = 'Red color when less than'
      CaptionOptions.Text = 'Red'
      Control = edtHealthOnlineRed
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 50
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object layedtHealthAuditYellow: TdxLayoutItem
      Parent = laygrpHealthAuditPercent
      AlignHorz = ahClient
      CaptionOptions.Hint = 'Yellow color when less than'
      CaptionOptions.Text = 'Yellow'
      Control = edtHealthAuditYellow
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 50
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object layedtHealthAuditRed: TdxLayoutItem
      Parent = laygrpHealthAuditPercent
      AlignHorz = ahClient
      CaptionOptions.Hint = 'Red color when less than'
      CaptionOptions.Text = 'Red'
      Control = edtHealthAuditRed
      ControlOptions.AutoControlAreaAlignment = False
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 50
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object laygrpHealthTab: TdxLayoutGroup
      Parent = laygrpMain
      CaptionOptions.Text = 'Health'
      Index = 1
    end
    object laygrpHealthAudit: TdxLayoutGroup
      Parent = laygrpHealthTab
      CaptionOptions.Text = 'Audit'
      Index = 0
    end
    object laygrpHealthOnline: TdxLayoutGroup
      Parent = laygrpHealthTab
      CaptionOptions.Text = 'Online'
      Index = 1
    end
    object laygrpHealthSuspension: TdxLayoutGroup
      Parent = laygrpHealthTab
      CaptionOptions.Text = 'Suspension'
      Index = 2
    end
    object layDisplayHealthHotData: TdxLayoutItem
      Parent = laygrpHealthTab
      CaptionOptions.Text = 'cxCheckBox1'
      CaptionOptions.Visible = False
      Control = chkHealthHotData
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 80
      ControlOptions.ShowBorder = False
      Index = 3
    end
    object laygrpSuspensionPercent: TdxLayoutGroup
      Parent = laygrpHealthSuspension
      AlignHorz = ahClient
      CaptionOptions.Text = 'New Group'
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 0
    end
    object laychkSuspensionRedLog: TdxLayoutItem
      Parent = laygrpSuspensionLogChecks
      CaptionOptions.Text = 'cxCheckBox1'
      CaptionOptions.Visible = False
      Control = chkSuspensionRedLog
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 82
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object laychkSuspensionYellowLog: TdxLayoutItem
      Parent = laygrpSuspensionLogChecks
      CaptionOptions.Text = 'cxCheckBox2'
      CaptionOptions.Visible = False
      Control = chkSuspensionYellowLog
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 82
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object laygrpSuspensionLogChecks: TdxLayoutGroup
      Parent = laygrpHealthSuspension
      CaptionOptions.Text = 'New Group'
      ShowBorder = False
      Index = 1
    end
    object laychkHealthOnlineRedLog: TdxLayoutItem
      Parent = laygrpHealthOnlineLogCheck
      CaptionOptions.Text = 'cxCheckBox1'
      CaptionOptions.Visible = False
      Control = chkHealthOnlineRedLog
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 82
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object laychkHealthOnlineYellowLog: TdxLayoutItem
      Parent = laygrpHealthOnlineLogCheck
      CaptionOptions.Text = 'cxCheckBox2'
      CaptionOptions.Visible = False
      Control = chkHealthOnlineYellowLog
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 82
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object laychkHealthAuditRedLog: TdxLayoutItem
      Parent = laygrpHealthAuditLogCheck
      CaptionOptions.Text = 'cxCheckBox3'
      CaptionOptions.Visible = False
      Control = chkHealthAuditRedLog
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 82
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object laychkHealthAuditYellowLog: TdxLayoutItem
      Parent = laygrpHealthAuditLogCheck
      CaptionOptions.Text = 'cxCheckBox4'
      CaptionOptions.Visible = False
      Control = chkHealthAuditYellowLog
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 82
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object laygrpHealthOnlinePercent: TdxLayoutGroup
      Parent = laygrpHealthOnline
      AlignHorz = ahClient
      CaptionOptions.Text = 'New Group'
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 0
    end
    object laygrpHealthOnlineLogCheck: TdxLayoutGroup
      Parent = laygrpHealthOnline
      CaptionOptions.Text = 'New Group'
      ShowBorder = False
      Index = 1
    end
    object laygrpHealthAuditPercent: TdxLayoutGroup
      Parent = laygrpHealthAudit
      AlignHorz = ahClient
      CaptionOptions.Text = 'New Group'
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 0
    end
    object laygrpHealthAuditLogCheck: TdxLayoutGroup
      Parent = laygrpHealthAudit
      CaptionOptions.Text = 'New Group'
      ShowBorder = False
      Index = 1
    end
    object laychkIsDisplayStorjNetUsed: TdxLayoutItem
      Parent = laygrpCommonTab
      CaptionOptions.Text = 'cxCheckBox1'
      CaptionOptions.Visible = False
      Control = chkIsDisplayStorjNetUsed
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 80
      ControlOptions.ShowBorder = False
      Index = 6
    end
    object laygrpMain: TdxLayoutGroup
      Parent = dxLayoutControlGroup_Root
      AlignVert = avClient
      CaptionOptions.Text = 'New Group'
      LayoutDirection = ldTabbed
      ShowBorder = False
      Index = 0
    end
    object laygrpCommonTab: TdxLayoutGroup
      Parent = laygrpMain
      CaptionOptions.Text = 'Common'
      ItemIndex = 10
      Index = 0
    end
    object laynumOnlineRangeMinutes: TdxLayoutItem
      Parent = laygrpCommonTab
      CaptionOptions.Text = 'Online range minutes'
      Control = numOnlineRangeMinutes
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 5
    end
    object laygrpPCRebooterSettings: TdxLayoutGroup
      Parent = laygrpMain
      AlignHorz = ahClient
      CaptionOptions.Text = 'PC Rebooter'
      Index = 2
    end
    object laychkIsEnabledPCRebooter: TdxLayoutItem
      Parent = laygrpPCRebooterSettings
      CaptionOptions.Text = 'cxCheckBox1'
      CaptionOptions.Visible = False
      Control = chkIsEnabledPCRebooter
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 80
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object laybtnAddPCRebooter: TdxLayoutItem
      Parent = laygrpPCRebooterFuncBtns
      AlignHorz = ahClient
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnAddPCRebooter
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object laybtnRemovePCRebooter: TdxLayoutItem
      Parent = laygrpPCRebooterFuncBtns
      AlignHorz = ahClient
      CaptionOptions.Text = 'cxButton2'
      CaptionOptions.Visible = False
      Control = btnRemovePCRebooter
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object laygrpPCRebooterFuncBtns: TdxLayoutGroup
      Parent = laygrpPCRebooterSettings
      AlignHorz = ahClient
      CaptionOptions.Text = 'New Group'
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 1
    end
    object laygrpPCRebooterItems: TdxLayoutGroup
      Parent = laygrpPCRebooterSettings
      CaptionOptions.Text = 'New Group'
      ShowBorder = False
      Index = 2
    end
    object layscrollPCRebooters: TdxLayoutItem
      Parent = laygrpPCRebooterSettings
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'ScrollBox1'
      CaptionOptions.Visible = False
      Control = scrollPCRebooters
      ControlOptions.AutoColor = True
      ControlOptions.OriginalHeight = 37
      ControlOptions.OriginalWidth = 185
      Index = 3
    end
    object laychkColoredTBM: TdxLayoutItem
      Parent = laygrpCommonTab
      CaptionOptions.Text = 'cxCheckBox1'
      CaptionOptions.Visible = False
      Control = chkColoredTBM
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 80
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object layedtLowerDeviationTreshold: TdxLayoutItem
      Parent = laygrpColoredTBMSettings
      CaptionOptions.Text = 'Less than Used treshold'
      Control = edtLowerDeviationTreshold
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object layedtHigherDeviationTreshold: TdxLayoutItem
      Parent = laygrpColoredTBMSettings
      CaptionOptions.Text = 'More than Used treshold'
      Control = edtHigherDeviationTreshold
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object laygrpColoredTBMSettings: TdxLayoutGroup
      Parent = laygrpCommonTab
      CaptionOptions.Text = 'TB*M Deviation Settings'
      Index = 3
    end
    object laychkMainFormFullScreen: TdxLayoutItem
      Parent = laygrpCommonTab
      CaptionOptions.Text = 'cxCheckBox1'
      CaptionOptions.Visible = False
      Control = chkMainFormFullScreen
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 7
    end
    object laychkReloadNodesTimer: TdxLayoutItem
      Parent = laygrpCommonTab
      CaptionOptions.Text = 'cxCheckBox1'
      CaptionOptions.Visible = False
      Control = chkReloadNodesTimer
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 8
    end
    object layedtReloadNodesTimerMinutes: TdxLayoutItem
      Parent = laygrpCommonTab
      CaptionOptions.Text = 'Refresh timer minutes'
      Control = edtReloadNodesTimerMinutes
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 9
    end
    object dxLayoutItem1: TdxLayoutItem
      Parent = laygrpCommonTab
      CaptionOptions.Text = 'cxCheckBox1'
      CaptionOptions.Visible = False
      Control = chkUseTreePathForLog
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 82
      ControlOptions.ShowBorder = False
      Index = 10
    end
    object dxLayoutItem2: TdxLayoutItem
      Parent = laygrpCommonTab
      CaptionOptions.Text = 'cxCheckBox1'
      CaptionOptions.Visible = False
      Control = chkUseProxyForGetNeightbours
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 11
    end
    object laygrpNotifyTab: TdxLayoutGroup
      Parent = laygrpMain
      CaptionOptions.Text = 'Notifications'
      Index = 3
    end
    object laychkNotifyTelegramEnable: TdxLayoutItem
      Parent = grpNotifyTelegramSettings
      AlignHorz = ahClient
      CaptionOptions.Text = 'cxCheckBox1'
      CaptionOptions.Visible = False
      Control = chkNotifyTelegramEnable
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 82
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object laychkNotifyEmailEnable: TdxLayoutItem
      Parent = grpNotifyEmailSettings
      AlignHorz = ahClient
      CaptionOptions.Text = 'cxCheckBox2'
      CaptionOptions.Visible = False
      Control = chkNotifyEmailEnable
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object layedtNotifyEmail: TdxLayoutItem
      Parent = laygrpNotifyEmail
      CaptionOptions.Text = 'Email'
      Control = edtNotifyEmail
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object laygrpNotifyTelegram: TdxLayoutGroup
      Parent = laygrpNotifyTab
      CaptionOptions.Text = 'Telegram'
      Index = 1
    end
    object laygrpNotifyEmail: TdxLayoutGroup
      Parent = laygrpNotifyTab
      CaptionOptions.Text = 'Email'
      Index = 0
    end
    object laychkUseNodeRefreshBuffer: TdxLayoutItem
      Parent = laygrpCommonTab
      Visible = False
      CaptionOptions.Text = 'cxCheckBox1'
      CaptionOptions.Visible = False
      Control = chkUseNodeRefreshBuffer
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 12
    end
    object layedtNotifyTelegramUserID: TdxLayoutItem
      Parent = laygrpNotifyTelegram
      CaptionOptions.Text = 'User ID'
      Control = edtNotifyTelegramUserID
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object layimgTelegramNotifyHelp: TdxLayoutItem
      Parent = grpNotifyTelegramSettings
      AlignHorz = ahRight
      CaptionOptions.Text = 'Image1'
      CaptionOptions.Visible = False
      Control = imgTelegramNotifyHelp
      ControlOptions.OriginalHeight = 20
      ControlOptions.OriginalWidth = 20
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object grpNotifyTelegramSettings: TdxLayoutGroup
      Parent = laygrpNotifyTelegram
      CaptionOptions.Text = 'New Group'
      ItemIndex = 1
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 0
    end
    object grpNotifyEmailSettings: TdxLayoutGroup
      Parent = laygrpNotifyEmail
      CaptionOptions.Text = 'New Group'
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 0
    end
    object layimgEmailNotifyHelp: TdxLayoutItem
      Parent = grpNotifyEmailSettings
      CaptionOptions.Text = 'Image1'
      CaptionOptions.Visible = False
      Control = imgEmailNotifyHelp
      ControlOptions.OriginalHeight = 20
      ControlOptions.OriginalWidth = 20
      ControlOptions.ShowBorder = False
      Index = 1
    end
  end
  object cxHintStyleController: TcxHintStyleController
    Global = False
    HintStyleClassName = 'TcxHintStyle'
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -12
    HintStyle.CaptionFont.Name = 'Segoe UI'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -12
    HintStyle.Font.Name = 'Segoe UI'
    HintStyle.Font.Style = []
    HintHidePause = 10000
    Left = 144
    Top = 264
  end
end
