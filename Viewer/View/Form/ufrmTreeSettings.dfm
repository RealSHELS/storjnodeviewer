object frmTreeSettings: TfrmTreeSettings
  Left = 0
  Top = 0
  Caption = 'Settings'
  ClientHeight = 544
  ClientWidth = 776
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  TextHeight = 13
  object dxLayoutControl1: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 776
    Height = 544
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 772
    ExplicitHeight = 543
    object btnSave: TcxButton
      Left = 310
      Top = 509
      Width = 75
      Height = 25
      Caption = 'Save'
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'iMaginary'
      TabOrder = 2
      OnClick = btnSaveClick
    end
    object btnCancel: TcxButton
      Left = 391
      Top = 509
      Width = 75
      Height = 25
      Caption = 'Cancel'
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'iMaginary'
      TabOrder = 3
      OnClick = btnCancelClick
    end
    object treeStructure: TcxTreeList
      Left = 10
      Top = 10
      Width = 756
      Height = 493
      Bands = <
        item
        end>
      DragCursor = crDrag
      DragMode = dmAutomatic
      FindPanel.DisplayMode = fpdmAlways
      FindPanel.Layout = fplCompact
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'SevenClassic'
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.First.Visible = False
      Navigator.Buttons.PriorPage.Visible = False
      Navigator.Buttons.Prior.Visible = False
      Navigator.Buttons.Next.Visible = False
      Navigator.Buttons.NextPage.Visible = False
      Navigator.Buttons.Last.Visible = False
      Navigator.Buttons.Append.Visible = True
      Navigator.Buttons.Edit.Visible = False
      Navigator.Buttons.Post.Visible = True
      Navigator.Buttons.Cancel.Visible = True
      Navigator.Buttons.Refresh.Visible = False
      Navigator.Buttons.SaveBookmark.Visible = False
      Navigator.Buttons.GotoBookmark.Visible = False
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      OptionsBehavior.ChangeDelay = 1000
      OptionsData.Appending = True
      OptionsData.Inserting = True
      OptionsView.ColumnAutoWidth = True
      OptionsView.DropNodeIndicator = True
      ScrollbarAnnotations.CustomAnnotations = <>
      TabOrder = 0
      OnDragDrop = treeStructureDragDrop
      OnDragOver = treeStructureDragOver
      Data = {
        00000500670200000F00000044617461436F6E74726F6C6C6572310900000012
        000000546378537472696E6756616C7565547970651200000054637853747269
        6E6756616C75655479706512000000546378537472696E6756616C7565547970
        6512000000546378537472696E6756616C756554797065120000005463785374
        72696E6756616C75655479706512000000546378537472696E6756616C756554
        79706512000000546378537472696E6756616C75655479706512000000546378
        537472696E6756616C75655479706512000000546378537472696E6756616C75
        655479706507000000445855464D540000030000003100320033000101010101
        010101445855464D540000030000003100310031000101010101010101445855
        464D540000030000003200320032000101010101010101445855464D54000003
        0000003500350035000101010101010101445855464D54000003000000320031
        0031000101010101010101445855464D54000003000000330033003300010101
        0101010101445855464D54000003000000340034003400010101010101010102
        00000000000000120A030000000000000000000000FFFFFFFFFFFFFFFFFFFFFF
        FF01000000080A0000000000000000FFFFFFFFFFFFFFFFFFFFFFFF0200000008
        080000000000000000FFFFFFFFFFFFFFFFFFFFFFFF03000000080A0000000000
        000000FFFFFFFFFFFFFFFFFFFFFFFF0400000012080200000000000000000000
        00FFFFFFFFFFFFFFFFFFFFFFFF0500000008080000000000000000FFFFFFFFFF
        FFFFFFFFFFFFFF0600000008080000000000000000FFFFFFFFFFFFFFFFFFFFFF
        FF1A0802000000}
      object clmTitle: TcxTreeListColumn
        Caption.Text = 'Title'
        Options.Sorting = False
        Width = 250
        Position.ColIndex = 0
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmAddress: TcxTreeListColumn
        Caption.Text = 'Address'
        Options.Sorting = False
        Width = 100
        Position.ColIndex = 1
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmPort: TcxTreeListColumn
        Caption.Text = 'Port'
        Options.Sorting = False
        Width = 100
        Position.ColIndex = 2
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmIsActive: TcxTreeListColumn
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.NullStyle = nssUnchecked
        Caption.AlignHorz = taCenter
        Caption.Text = 'Active'
        Options.Sorting = False
        Width = 58
        Position.ColIndex = 3
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmAutoExpand: TcxTreeListColumn
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.NullStyle = nssUnchecked
        Caption.AlignHorz = taCenter
        Caption.Text = 'Auto expand'
        Width = 100
        Position.ColIndex = 4
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmCost: TcxTreeListColumn
        Caption.AlignHorz = taCenter
        Caption.Text = 'Cost'
        Width = 100
        Position.ColIndex = 5
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmCurrency: TcxTreeListColumn
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.ClearKey = 46
        Properties.DropDownListStyle = lsFixedList
        Properties.Items.Strings = (
          'USD'
          'EUR'
          'UAH'
          'RUB')
        Caption.AlignHorz = taCenter
        Caption.Text = 'Currency'
        Width = 100
        Position.ColIndex = 6
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmComment: TcxTreeListColumn
        Caption.Text = 'Comment'
        Width = 100
        Position.ColIndex = 7
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmHDDVolume: TcxTreeListColumn
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.ClearKey = 46
        Properties.DropDownListStyle = lsFixedList
        Properties.Items.Strings = (
          '< 1 TB'
          '1 TB'
          '2 TB'
          '3 TB'
          '4 TB'
          '6 TB'
          '8 TB'
          '10 TB'
          '12 TB'
          '14 TB'
          '16 TB'
          '18 TB'
          '20 TB'
          '22 TB'
          '> 22 TB')
        Caption.Text = 'HDD Volume'
        Width = 100
        Position.ColIndex = 8
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
    object btnShowSSHSettings: TcxButton
      Left = 10
      Top = 509
      Width = 75
      Height = 25
      Caption = 'SSH Settings'
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'iMaginary'
      TabOrder = 1
      OnClick = btnShowSSHSettingsClick
    end
    object dxLayoutControl1Group_Root: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avClient
      Hidden = True
      ItemIndex = 1
      ShowBorder = False
      Index = -1
    end
    object laygrpMain: TdxLayoutGroup
      Parent = dxLayoutControl1Group_Root
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'New Group'
      ShowBorder = False
      Index = 0
    end
    object laygrpBottom: TdxLayoutGroup
      Parent = laygrpBottomButtons
      AlignHorz = ahCenter
      AlignVert = avBottom
      CaptionOptions.Text = 'New Group'
      CaptionOptions.Visible = False
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 1
    end
    object laybtnSave: TdxLayoutItem
      Parent = laygrpBottom
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnSave
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object laybtnCancel: TdxLayoutItem
      Parent = laygrpBottom
      CaptionOptions.Text = 'cxButton2'
      CaptionOptions.Visible = False
      Control = btnCancel
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object laytreeStructure: TdxLayoutItem
      Parent = laygrpMain
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'cxTreeList1'
      CaptionOptions.Visible = False
      Control = treeStructure
      ControlOptions.OriginalHeight = 415
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object laybtnShowSSHSettings: TdxLayoutItem
      Parent = laygrpBottomButtons
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnShowSSHSettings
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object laygrpBottomButtons: TdxLayoutGroup
      Parent = dxLayoutControl1Group_Root
      CaptionOptions.Text = 'New Group'
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 1
    end
  end
end
