object frmRestartUsedDiffer: TfrmRestartUsedDiffer
  Left = 0
  Top = 0
  Caption = 'Restart Used Differ'
  ClientHeight = 494
  ClientWidth = 830
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  TextHeight = 15
  object dxLayoutControl: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 830
    Height = 494
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 826
    ExplicitHeight = 493
    object Grid: TcxGrid
      Left = 12
      Top = 12
      Width = 806
      Height = 470
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'SevenClassic'
      RootLevelOptions.DetailTabsPosition = dtpTop
      object GridDBTableView: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        ScrollbarAnnotations.CustomAnnotations = <>
        OnCellDblClick = GridDBTableViewCellDblClick
        DataController.DataSource = DM.dtsrcRestartUsedDiffer
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsView.ColumnAutoWidth = True
        OptionsView.GroupByBox = False
        object clmTitle: TcxGridDBColumn
          DataBinding.FieldName = 'Title'
          Options.Editing = False
          Width = 148
        end
        object clmNewUsed: TcxGridDBColumn
          Caption = 'New Used'
          DataBinding.FieldName = 'NewUsed'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = '### ##0 GB'
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 133
        end
        object clmOldUsed: TcxGridDBColumn
          Caption = 'Old Used'
          DataBinding.FieldName = 'OldUsed'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = '### ##0 GB'
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 100
        end
        object clmUsedDiff: TcxGridDBColumn
          Caption = 'Used Diff'
          DataBinding.Expression = #1'[Old Used]-[New Used]'
          DataBinding.ValueType = 'Integer'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = '### ##0 GB'
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object clmNewTrash: TcxGridDBColumn
          Caption = 'New Trash'
          DataBinding.FieldName = 'NewTrash'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = '### ##0 GB'
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 96
        end
        object clmOldTrash: TcxGridDBColumn
          Caption = 'Old Trash'
          DataBinding.FieldName = 'OldTrash'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = '### ##0 GB'
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 99
        end
        object clmNewUptime: TcxGridDBColumn
          Caption = 'New Uptime'
          DataBinding.FieldName = 'NewUptime'
          PropertiesClassName = 'TcxLabelProperties'
          Properties.Alignment.Horz = taCenter
          OnGetDataText = clmNewUptimeGetDataText
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 131
        end
        object clmOldUptime: TcxGridDBColumn
          Caption = 'Old Uptime'
          DataBinding.FieldName = 'OldUptime'
          PropertiesClassName = 'TcxLabelProperties'
          Properties.Alignment.Horz = taCenter
          OnGetDataText = clmNewUptimeGetDataText
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 97
        end
      end
      object GridDBTableViewHistory: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        FilterBox.Position = fpTop
        FilterBox.Visible = fvAlways
        FindPanel.DisplayMode = fpdmAlways
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = DM.dtsrcRestartUsedDifferHistory
        DataController.Summary.DefaultGroupSummaryItems = <
          item
            Format = 'Total Used Diff: ### ##0 GB'
            Kind = skSum
            Column = clmHistoryUsedDiff
          end>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsView.ColumnAutoWidth = True
        object clmHistoryTitle: TcxGridDBColumn
          DataBinding.FieldName = 'Title'
          DataBinding.IsNullValueType = True
          Options.Editing = False
        end
        object clmHistoryNewUsed: TcxGridDBColumn
          Caption = 'New Used'
          DataBinding.FieldName = 'NewUsed'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = '### ##0 GB'
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object clmHistoryOldUsed: TcxGridDBColumn
          Caption = 'Old Used'
          DataBinding.FieldName = 'OldUsed'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = '### ##0 GB'
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object clmHistoryUsedDiff: TcxGridDBColumn
          Caption = 'Used Diff'
          DataBinding.Expression = #1'[Old Used]-[New Used]'
          DataBinding.ValueType = 'Integer'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = '### ##0 GB'
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object clmHistoryNewTrash: TcxGridDBColumn
          Caption = 'New Trash'
          DataBinding.FieldName = 'NewTrash'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = '### ##0 GB'
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object clmHistoryOldTrash: TcxGridDBColumn
          Caption = 'Old Trash'
          DataBinding.FieldName = 'OldTrash'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = '### ##0 GB'
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object clmHistoryNewUptime: TcxGridDBColumn
          Caption = 'New Uptime'
          DataBinding.FieldName = 'NewUptime'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxLabelProperties'
          Properties.Alignment.Horz = taCenter
          OnGetDataText = clmNewUptimeGetDataText
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object clmHistoryOldUptime: TcxGridDBColumn
          Caption = 'Old Uptime'
          DataBinding.FieldName = 'OldUptime'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxLabelProperties'
          Properties.Alignment.Horz = taCenter
          OnGetDataText = clmNewUptimeGetDataText
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object clmHistoryCreationDT: TcxGridDBColumn
          Caption = 'Creation DT'
          DataBinding.FieldName = 'CreationDT'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxDateEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = 'yyyy-mm-dd hh:nn:ss'
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          SortIndex = 0
          SortOrder = soDescending
        end
        object clmHistoryNodeID: TcxGridDBColumn
          DataBinding.FieldName = 'NodeID'
          DataBinding.IsNullValueType = True
          Visible = False
          GroupIndex = 0
          Options.Editing = False
        end
      end
      object GridLevel1: TcxGridLevel
        Caption = 'Current'
        GridView = GridDBTableView
      end
      object GridLevel2: TcxGridLevel
        Caption = 'History'
        GridView = GridDBTableViewHistory
      end
    end
    object dxLayoutControlGroup_Root: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avClient
      Hidden = True
      ShowBorder = False
      Index = -1
    end
    object layGrid: TdxLayoutItem
      Parent = dxLayoutControlGroup_Root
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'cxGrid1'
      CaptionOptions.Visible = False
      Control = Grid
      ControlOptions.OriginalHeight = 200
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 0
    end
  end
end
