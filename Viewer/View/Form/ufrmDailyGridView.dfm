object frmDailyGridView: TfrmDailyGridView
  Left = 0
  Top = 0
  Caption = 'Daily Grid View'
  ClientHeight = 455
  ClientWidth = 787
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  TextHeight = 13
  object dxLayoutControl: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 787
    Height = 455
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 783
    ExplicitHeight = 454
    object gridDailyNodeData: TcxGrid
      Left = 10
      Top = 10
      Width = 767
      Height = 435
      TabOrder = 0
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'SevenClassic'
      object gridDailyNodeDataDBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        FilterBox.Position = fpTop
        FilterBox.Visible = fvAlways
        FindPanel.DisplayMode = fpdmAlways
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = DM.dtsrcNodeData
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            FieldName = 'DailyEgress'
            Column = clmEgress
          end
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            FieldName = 'DailyIngress'
            Column = clmIngress
          end
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            FieldName = 'DailyRepairEgress'
            Column = clmRepairEgress
          end
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            FieldName = 'DailyRepairIngress'
            Column = clmRepairIngress
          end
          item
            Format = ',0.00 GB;-,0.00 GB'
            Kind = skSum
            FieldName = 'DailyTotalBandwidth'
            Column = clmTotalBandwidth
          end
          item
            Format = 'Count: ##'
            Kind = skCount
            FieldName = 'Title'
            Column = clmTitle
          end
          item
            Format = '### ##0 GB'
            Kind = skSum
            FieldName = 'FreeSpace'
            Column = clmFreeSpace
          end
          item
            Kind = skSum
            FieldName = 'DailyTBMonth'
            Column = clmTBMonth
          end>
        DataController.Summary.SummaryGroups = <>
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.Footer = True
        object clmTitle: TcxGridDBColumn
          DataBinding.FieldName = 'Title'
          Width = 167
        end
        object clmAddress: TcxGridDBColumn
          DataBinding.FieldName = 'Address'
          Width = 83
        end
        object clmPort: TcxGridDBColumn
          DataBinding.FieldName = 'Port'
          PropertiesClassName = 'TcxLabelProperties'
          Properties.Alignment.Horz = taCenter
          Properties.Alignment.Vert = taVCenter
          HeaderAlignmentHorz = taCenter
        end
        object clmTBMonth: TcxGridDBColumn
          Caption = 'TB * M'
          DataBinding.FieldName = 'DailyTBMonth'
          PropertiesClassName = 'TcxLabelProperties'
          Properties.Alignment.Horz = taCenter
          Properties.Alignment.Vert = taVCenter
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object clmEgress: TcxGridDBColumn
          Caption = 'Egress'
          DataBinding.FieldName = 'DailyEgress'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
        end
        object clmIngress: TcxGridDBColumn
          Caption = 'Ingress'
          DataBinding.FieldName = 'DailyIngress'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
        end
        object clmRepairEgress: TcxGridDBColumn
          Caption = 'Repair Egress'
          DataBinding.FieldName = 'DailyRepairEgress'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
        end
        object clmRepairIngress: TcxGridDBColumn
          Caption = 'Repair Ingress'
          DataBinding.FieldName = 'DailyRepairIngress'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
        end
        object clmTotalBandwidth: TcxGridDBColumn
          Caption = 'Total Bandwidth'
          DataBinding.FieldName = 'DailyTotalBandwidth'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0.00 GB;-,0.00 GB'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 111
        end
        object clmFreeSpace: TcxGridDBColumn
          Caption = 'Free space'
          DataBinding.FieldName = 'FreeSpace'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = ',0 GB;-,0 GB'
          FooterAlignmentHorz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
      end
      object gridDailyNodeDataLevel1: TcxGridLevel
        GridView = gridDailyNodeDataDBTableView1
      end
    end
    object dxLayoutControlGroup_Root: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avClient
      Hidden = True
      ShowBorder = False
      Index = -1
    end
    object laygridDailyNodeData: TdxLayoutItem
      Parent = dxLayoutControlGroup_Root
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'cxGrid1'
      CaptionOptions.Visible = False
      Control = gridDailyNodeData
      ControlOptions.OriginalHeight = 200
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 0
    end
  end
  object cxGridPopupMenu: TcxGridPopupMenu
    Grid = gridDailyNodeData
    PopupMenus = <>
    Left = 232
    Top = 192
  end
end
