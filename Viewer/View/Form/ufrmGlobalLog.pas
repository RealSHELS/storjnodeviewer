unit ufrmGlobalLog;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBasic, dxSkiniMaginary, dxSkinSeven,
  dxSkinSevenClassic, cxClasses, dxLayoutContainer, dxLayoutControl,
  dxLayoutcxEditAdapters, cxContainer, cxEdit, cxTextEdit, cxMemo,
  Storj.Types, System.IOUtils, dxLayoutControlAdapters, Vcl.Menus, Vcl.StdCtrls,
  cxButtons, dxStatusBar, Vcl.ExtCtrls, uNotifyCenter;

type
  TfrmGlobalLog = class(TForm)
    dxLayoutControl1Group_Root: TdxLayoutGroup;
    dxLayoutControl1: TdxLayoutControl;
    mmoGlobalLog: TcxMemo;
    layMemo: TdxLayoutItem;
    dxStatusBar: TdxStatusBar;
    layStatusBar: TdxLayoutItem;
    btnDeleteLogFile: TcxButton;
    layButtonDeleteLogFile: TdxLayoutItem;
    laygrpBottom: TdxLayoutGroup;
    Timer: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
    procedure btnDeleteLogFileClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure AddTextToFile(const aStr: String);
    procedure RefreshFileSizeInfo;

    procedure CommandShow(const aData: TValue);
    procedure CommandLog(const aData: TValue);
  public
  end;

var
  frmGlobalLog: TfrmGlobalLog;

implementation

{$R *.dfm}

uses
  uDM, Storj.Consts, System.UITypes;

{ TfrmGlobalLog }

function GetFileSize(const aFilename: String): Int64;
var
  info: TWin32FileAttributeData;
begin
  result := -1;

  if NOT GetFileAttributesEx(PWideChar(aFileName), GetFileExInfoStandard, @info) then
    EXIT;

  result := Int64(info.nFileSizeLow) or Int64(info.nFileSizeHigh shl 32);
end;

procedure TfrmGlobalLog.AddTextToFile(const aStr: String);
begin
  TFile.AppendAllText(LOG_FILENAME, #13#10 + aStr, TEncoding.UTF8);
end;

procedure TfrmGlobalLog.btnDeleteLogFileClick(Sender: TObject);
begin
  if MessageDlg('Are you sure want delete log file?', mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
    Exit;

  if TFile.Exists(LOG_FILENAME) then
    TFile.Delete(LOG_FILENAME);

  dxStatusBar.Panels[0].Text := 'Log file size: 0 B';
  mmoGlobalLog.Lines.Clear;
end;

procedure TfrmGlobalLog.CommandShow(const aData: TValue);
begin
  Self.Show;
end;

procedure TfrmGlobalLog.FormCreate(Sender: TObject);
begin
  if TFile.Exists(LOG_FILENAME) then begin
    mmoGlobalLog.Lines.LoadFromFile(LOG_FILENAME, TEncoding.UTF8);
    RefreshFileSizeInfo;
  end;

  TNotifyCenter.Instance.Subscribe(COMMAND_SHOW_GLOBAL_LOG, CommandShow);
  TNotifyCenter.Instance.Subscribe(COMMAND_LOG, CommandLog);
end;

procedure TfrmGlobalLog.FormDestroy(Sender: TObject);
begin
  TNotifyCenter.Instance.Unsubscribe(COMMAND_SHOW_GLOBAL_LOG, CommandShow);
  TNotifyCenter.Instance.Subscribe(COMMAND_LOG, CommandLog);
end;

procedure TfrmGlobalLog.FormShow(Sender: TObject);
begin
  SendMessage(mmoGlobalLog.Handle, EM_LINESCROLL, 0, mmoGlobalLog.Lines.Count);
end;

procedure TfrmGlobalLog.CommandLog(const aData: TValue);
begin
  mmoGlobalLog.Lines.Add(aData.AsString);
  AddTextToFile(aData.AsString);
end;

procedure TfrmGlobalLog.RefreshFileSizeInfo;
var
  FileSize: Int64;
begin
  FileSize := GetFileSize(LOG_FILENAME);

  var Arr := ['B', 'KB', 'MB', 'GB', 'TB'];
  var Index := 0;
  var Del := 1;
  var Value := FileSize;
  while Value > 1000 do begin
    Index := Index + 1;
    Del := Del * 1000;
    Value := Round(Value / 1000);
  end;

  dxStatusBar.Panels[0].Text := 'Log file size: ' + FormatFloat('#.00', FileSize/Del) + ' ' + arr[index];
end;

procedure TfrmGlobalLog.TimerTimer(Sender: TObject);
begin
  RefreshFileSizeInfo;
end;

end.
