object frmHealthTree: TfrmHealthTree
  Left = 0
  Top = 0
  Caption = 'Health Data'
  ClientHeight = 472
  ClientWidth = 943
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  TextHeight = 13
  object dxLayoutControl: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 943
    Height = 472
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 947
    ExplicitHeight = 473
    object treeHealthData: TcxTreeList
      Left = 10
      Top = 10
      Width = 931
      Height = 454
      Bands = <
        item
        end>
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'SevenClassic'
      Navigator.Buttons.CustomButtons = <>
      OptionsBehavior.ChangeDelay = 1000
      OptionsView.ColumnAutoWidth = True
      ScrollbarAnnotations.CustomAnnotations = <>
      TabOrder = 0
      OnCustomDrawDataCell = treeHealthDataCustomDrawDataCell
      object clmTitle: TcxTreeListColumn
        Caption.Text = 'Title'
        Options.Editing = False
        Options.Sorting = False
        Width = 103
        Position.ColIndex = 0
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmAddress: TcxTreeListColumn
        Caption.Text = 'Address'
        Options.Editing = False
        Options.Sorting = False
        Width = 103
        Position.ColIndex = 1
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmPort: TcxTreeListColumn
        Caption.Text = 'Port'
        Options.Editing = False
        Options.Sorting = False
        Width = 104
        Position.ColIndex = 2
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmSatellite: TcxTreeListColumn
        Caption.Text = 'Satellite'
        Options.Editing = False
        Options.Sorting = False
        Width = 103
        Position.ColIndex = 3
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmAudit: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = '##0.## %'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Audit'
        Options.Editing = False
        Options.Sorting = False
        Width = 103
        Position.ColIndex = 4
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmOnline: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = '##0.## %'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Online'
        Options.Editing = False
        Options.Sorting = False
        Width = 103
        Position.ColIndex = 5
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmSuspension: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = '##0.## %'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Suspension'
        Options.Editing = False
        Options.Sorting = False
        Width = 176
        Position.ColIndex = 6
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmIsSuspended: TcxTreeListColumn
        BestFitMaxWidth = 70
        Caption.AlignHorz = taCenter
        Caption.Text = 'Suspended'
        Options.Sizing = False
        Options.Editing = False
        Options.Sorting = False
        Width = 70
        Position.ColIndex = 7
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
        OnGetDisplayText = clmIsSuspendedGetDisplayText
      end
      object clmIsDisqualified: TcxTreeListColumn
        BestFitMaxWidth = 70
        Caption.AlignHorz = taCenter
        Caption.Text = 'Disqualified'
        Options.Sizing = False
        Options.Editing = False
        Options.Sorting = False
        Width = 70
        Position.ColIndex = 8
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
        OnGetDisplayText = clmIsSuspendedGetDisplayText
      end
    end
    object dxLayoutControlGroup_Root: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avClient
      Hidden = True
      ShowBorder = False
      Index = -1
    end
    object dxLayoutItem1: TdxLayoutItem
      Parent = dxLayoutControlGroup_Root
      AlignHorz = ahClient
      AlignVert = avClient
      Control = treeHealthData
      ControlOptions.OriginalHeight = 397
      ControlOptions.OriginalWidth = 804
      ControlOptions.ShowBorder = False
      Index = 0
    end
  end
end
