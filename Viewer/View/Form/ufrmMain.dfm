object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'StorjNodeViewer by RealSHELS v0.9.3'
  ClientHeight = 568
  ClientWidth = 1257
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Position = poDesigned
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  TextHeight = 13
  object dxLayoutControl: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 1257
    Height = 568
    Align = alClient
    TabOrder = 0
    object TreeList: TcxTreeList
      Left = 10
      Top = 43
      Width = 1237
      Height = 301
      Bands = <
        item
        end>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'SevenClassic'
      Navigator.Buttons.CustomButtons = <>
      OptionsBehavior.ChangeDelay = 1000
      OptionsSelection.CellSelect = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.FocusRect = False
      OptionsView.GridLines = tlglBoth
      ParentFont = False
      PopupMenu = treePopupMenu
      PopupMenus.ColumnHeaderMenu.UseBuiltInMenu = True
      PopupMenus.ColumnHeaderMenu.Items = [tlchmiSortAscending, tlchmiSortDescending, tlchmiClearSorting, tlchmiRemoveThisColumn, tlchmiFieldChooser, tlchmiHorzAlignment, tlchmiVertAlignment, tlchmiBestFit, tlchmiBestFitAllColumns]
      ScrollbarAnnotations.CustomAnnotations = <>
      TabOrder = 1
      OnCompare = TreeListCompare
      OnCustomDrawDataCell = TreeListCustomDrawDataCell
      Data = {
        00000500FF0400000F00000044617461436F6E74726F6C6C6572311500000012
        000000546378537472696E6756616C7565547970651200000054637853747269
        6E6756616C75655479706512000000546378537472696E6756616C7565547970
        6512000000546378537472696E6756616C756554797065120000005463785374
        72696E6756616C75655479706512000000546378537472696E6756616C756554
        79706512000000546378537472696E6756616C75655479706512000000546378
        537472696E6756616C75655479706512000000546378537472696E6756616C75
        655479706512000000546378537472696E6756616C7565547970651200000054
        6378537472696E6756616C75655479706512000000546378537472696E675661
        6C75655479706512000000546378537472696E6756616C756554797065120000
        00546378537472696E6756616C75655479706512000000546378537472696E67
        56616C75655479706512000000546378537472696E6756616C75655479706512
        000000546378537472696E6756616C7565547970651200000054637853747269
        6E6756616C75655479706512000000546378537472696E6756616C7565547970
        6512000000546378537472696E6756616C756554797065120000005463785374
        72696E6756616C75655479706505000000445855464D5400000400000048006F
        006D0065000101010101010101010101010101010101010101445855464D5400
        0003000000500043003100010101010101010101010101010101010101010144
        5855464D540000050000004E006F00640065003100000D000000310039003200
        2E003100360038002E0030002E00310031003200000500000031003400300030
        003200010100060000003200310039002C0033003600000400000031002C0034
        0038000005000000390030002C0035003300000400000030002C003700370000
        060000003300310032002C0031003400000500000032002C0036003600320000
        0400000032003800300030000004000000320037003900390001010004000000
        39002C0032003900000400000039002C003000320001010101445855464D5400
        010101001B000000730061006C0074006C0061006B0065002E00740061007200
        64006900670072006100640065002E0069006F003A0037003700370037000005
        0000003300330032003700370000060000003100390038002C00370039000001
        00000030000006000000340031002C00360037003400000500000030002C0031
        003500370000060000003200340030002C003600320001010101010101010101
        01445855464D5400010101001E00000061007300690061002D00650061007300
        74002D0031002E0074006100720064006900670072006100640065002E006900
        6F003A003700370037003700000200000039003900000400000038002C003700
        3400000400000030002C0033003500000500000036002C003300360035000005
        00000030002C003000330034000005000000310035002C003400390001010101
        0101010101010101000000000000000208010000000000000000000000FFFFFF
        FFFFFFFFFFFFFFFFFF010000000208010000000000000000000000FFFFFFFFFF
        FFFFFFFFFFFFFF020000001208020000000000000000000000FFFFFFFFFFFFFF
        FFFFFFFFFF0300000008080000000000000000FFFFFFFFFFFFFFFFFFFFFFFF04
        000000080A0000000000000000FFFFFFFFFFFFFFFFFFFFFFFF0A0801000000}
      object clmTitle: TcxTreeListColumn
        Caption.AlignHorz = taCenter
        Caption.Text = 'Title'
        Options.Editing = False
        Width = 143
        Position.ColIndex = 0
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmAddress: TcxTreeListColumn
        Caption.Text = 'Address'
        Options.Editing = False
        Options.Sorting = False
        Width = 54
        Position.ColIndex = 1
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmPort: TcxTreeListColumn
        Caption.Text = 'Port'
        Options.Editing = False
        Width = 53
        Position.ColIndex = 2
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmSatellite: TcxTreeListColumn
        Caption.Text = 'Satellite'
        Options.Editing = False
        Options.Sorting = False
        Width = 86
        Position.ColIndex = 3
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmAudits: TcxTreeListColumn
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taCenter
        Caption.AlignHorz = taCenter
        Caption.Text = 'Succesfull audits'
        Options.Editing = False
        Width = 53
        Position.ColIndex = 4
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmEgress: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = '### ##0.00 GB'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Egress'
        Options.Editing = False
        Width = 53
        Position.ColIndex = 5
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmIngress: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = '### ##0.00 GB'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Ingress'
        Options.Editing = False
        Width = 54
        Position.ColIndex = 6
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmRepairEgress: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = '### ##0.00 GB'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Repair Egress'
        Options.Editing = False
        Width = 54
        Position.ColIndex = 7
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmRepairIngress: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = '### ##0.00 GB'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Repair Ingress'
        Options.Editing = False
        Width = 53
        Position.ColIndex = 8
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmTotalBandwidth: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = '### ##0.00 GB'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Total Bandwidth'
        Options.Editing = False
        Width = 54
        Position.ColIndex = 9
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmTBMonth: TcxTreeListColumn
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taCenter
        Caption.AlignHorz = taCenter
        Caption.Text = 'Avg. TB*M'
        Options.Editing = False
        Width = 54
        Position.ColIndex = 10
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmAllocatedSpace: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = '### ##0 GB'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Allocated space'
        Options.Editing = False
        Width = 53
        Position.ColIndex = 11
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmUsedSpace: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = '### ##0 GB'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Used space'
        Options.Editing = False
        Width = 54
        Position.ColIndex = 12
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmTrash: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = '### ##0 GB'
        Properties.ReadOnly = True
        Caption.AlignHorz = taCenter
        Caption.Text = 'Trash'
        Width = 100
        Position.ColIndex = 13
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmFreeSpace: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = '### ##0 GB'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Free space'
        Options.Editing = False
        Width = 83
        Position.ColIndex = 14
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmPayoutDirty: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = ',0.00 $;-,0.00 $'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Payout dirty'
        Options.Editing = False
        Width = 53
        Position.ColIndex = 15
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmPayoutCleanly: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = ',0.00 $;-,0.00 $'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Payout cleanly'
        Options.Editing = False
        Width = 74
        Position.ColIndex = 16
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmEstimatedDirtyPayout: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = ',0.00 $;-,0.00 $'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Estimated dirty'
        Width = 100
        Position.ColIndex = 17
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmEstimatedCleanlyPayout: TcxTreeListColumn
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DisplayFormat = ',0.00 $;-,0.00 $'
        Caption.AlignHorz = taCenter
        Caption.Text = 'Estimated cleanly'
        Width = 100
        Position.ColIndex = 18
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmVersion: TcxTreeListColumn
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taCenter
        Caption.AlignHorz = taCenter
        Caption.Text = 'Version'
        Options.Editing = False
        Options.Sorting = False
        Width = 64
        Position.ColIndex = 19
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object clmFullPath: TcxTreeListColumn
        VisibleForExpressionEditor = bFalse
        Visible = False
        Caption.Text = 'Full Path'
        Width = 100
        Position.ColIndex = 20
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
    object mmoLog: TcxMemo
      Left = 41
      Top = 368
      Properties.ReadOnly = True
      Properties.ScrollBars = ssVertical
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      TabOrder = 3
      Height = 156
      Width = 846
    end
    object mmoFinishedNodes: TcxMemo
      Left = 893
      Top = 368
      Properties.ReadOnly = True
      Properties.ScrollBars = ssVertical
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      TabOrder = 4
      Height = 156
      Width = 354
    end
    object btnClearLog: TcxButton
      Left = 10
      Top = 350
      Width = 25
      Height = 25
      Hint = 'Clear log'
      Caption = 'btnClearLog'
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'iMaginary'
      OptionsImage.Glyph.SourceDPI = 96
      OptionsImage.Glyph.SourceHeight = 20
      OptionsImage.Glyph.SourceWidth = 20
      OptionsImage.Glyph.Data = {
        3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D225554
        462D38223F3E0D0A3C7376672076657273696F6E3D22312E31222069643D22D0
        A1D0BBD0BED0B95F322220786D6C6E733D22687474703A2F2F7777772E77332E
        6F72672F323030302F7376672220786D6C6E733A786C696E6B3D22687474703A
        2F2F7777772E77332E6F72672F313939392F786C696E6B2220783D2230707822
        20793D22307078222076696577426F783D223020302033322033322220737479
        6C653D22656E61626C652D6261636B67726F756E643A6E657720302030203332
        2033323B2220786D6C3A73706163653D227072657365727665223E262331333B
        262331303B3C7374796C6520747970653D22746578742F6373732220786D6C3A
        73706163653D227072657365727665223E2E426C61636B7B66696C6C3A233732
        373237323B7D262331333B262331303B2623393B2E59656C6C6F777B66696C6C
        3A234646423131353B7D3C2F7374796C653E0D0A3C706F6C79676F6E20636C61
        73733D2259656C6C6F772220706F696E74733D22382C323220362C3330203236
        2C33302032342C323220222F3E0D0A3C7061746820636C6173733D22426C6163
        6B2220643D224D32302C3136682D32563463302D312E312D302E392D322D322D
        32732D322C302E392D322C32763132682D32632D322E322C302D342C312E382D
        342C346831364332342C31372E382C32322E322C31362C32302C31367A222F3E
        0D0A3C2F7376673E0D0A}
      PaintStyle = bpsGlyph
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = btnClearLogClick
    end
    inline frameButtons: TframeButtons
      Left = 5
      Top = 0
      Width = 1247
      Height = 42
      VertScrollBar.Visible = False
      TabOrder = 0
      ExplicitLeft = 5
      ExplicitWidth = 1247
      inherited dxLayoutControl: TdxLayoutControl
        Width = 1247
        ExplicitWidth = 1247
        inherited btnSettings: TcxButton
          Left = 1242
          Top = 5
          ExplicitLeft = 1242
          ExplicitTop = 5
        end
        inherited btnChangeLog: TcxButton
          Left = 1206
          Top = 5
          ExplicitLeft = 1206
          ExplicitTop = 5
        end
        inherited btnShowGlobalLog: TcxButton
          Left = 1170
          Top = 5
          ExplicitLeft = 1170
          ExplicitTop = 5
        end
        inherited btnShowGridView: TcxButton
          Left = 257
          Top = 5
          ExplicitLeft = 257
          ExplicitTop = 5
        end
        inherited btnShowFinancialData: TcxButton
          Left = 221
          Top = 5
          ExplicitLeft = 221
          ExplicitTop = 5
        end
        inherited btnShowCheckNeighboursForm: TcxButton
          Left = 185
          Top = 5
          ExplicitLeft = 185
          ExplicitTop = 5
        end
        inherited btnShowDaily: TcxButton
          Left = 149
          Top = 5
          ExplicitLeft = 149
          ExplicitTop = 5
        end
        inherited btnResetSorting: TcxButton
          Left = 113
          Top = 5
          ExplicitLeft = 113
          ExplicitTop = 5
        end
        inherited btnRefresh: TcxButton
          Left = 41
          Top = 5
          ExplicitLeft = 41
          ExplicitTop = 5
        end
        inherited btnTreeSettings: TcxButton
          Left = 77
          Top = 5
          ExplicitLeft = 77
          ExplicitTop = 5
        end
        inherited lblDisqualified: TcxLabel
          Left = 861
          Top = 5
          ExplicitLeft = 861
          ExplicitTop = 5
          ExplicitHeight = 13
        end
        inherited lblSuspended: TcxLabel
          Left = 861
          Top = 24
          ExplicitLeft = 861
          ExplicitTop = 24
          ExplicitHeight = 13
        end
        inherited lblAuditRed: TcxLabel
          Left = 947
          Top = 5
          ExplicitLeft = 947
          ExplicitTop = 5
          ExplicitHeight = 13
        end
        inherited lblAuditYellow: TcxLabel
          Left = 947
          Top = 24
          ExplicitLeft = 947
          ExplicitTop = 24
          ExplicitHeight = 13
        end
        inherited btnShowHealthData: TcxButton
          Left = 293
          Top = 5
          ExplicitLeft = 293
          ExplicitTop = 5
        end
        inherited lblSuspensionRed: TcxLabel
          Left = 1084
          Top = 5
          ExplicitLeft = 1084
          ExplicitTop = 5
          ExplicitHeight = 13
        end
        inherited lblSuspensionYellow: TcxLabel
          Left = 1084
          Top = 24
          ExplicitLeft = 1084
          ExplicitTop = 24
        end
        inherited lblOnlineRed: TcxLabel
          Left = 1008
          Top = 5
          ExplicitLeft = 1008
          ExplicitTop = 5
          ExplicitHeight = 13
        end
        inherited lblOnlineYellow: TcxLabel
          Left = 1008
          Top = 24
          ExplicitLeft = 1008
          ExplicitTop = 24
          ExplicitHeight = 13
        end
        inherited lblWholeSNUsed: TcxLabel
          Left = 706
          Top = 5
          ExplicitLeft = 706
          ExplicitTop = 5
          ExplicitWidth = 149
          ExplicitHeight = 13
        end
        inherited btnExportData: TcxButton
          Left = 329
          Top = 5
          ExplicitLeft = 329
          ExplicitTop = 5
        end
        inherited btnShowNeighboursTree: TcxButton
          Left = 365
          Top = 5
          ExplicitLeft = 365
          ExplicitTop = 5
        end
        inherited btnShowDailyGridView: TcxButton
          Left = 401
          Top = 5
          ExplicitLeft = 401
          ExplicitTop = 5
        end
        inherited btnRebootPC: TcxButton
          Left = 437
          Top = 5
          TabOrder = 14
          ExplicitLeft = 437
          ExplicitTop = 5
        end
        inherited framePCRebootersStatus1: TframePCRebootersStatus
          Left = 653
          Top = 3
          Height = 34
          ExplicitLeft = 653
          ExplicitTop = 3
          ExplicitHeight = 34
          inherited pnl: TFlowPanel
            Height = 34
            StyleElements = [seFont, seClient, seBorder]
            ExplicitHeight = 34
          end
        end
        inherited btnOpenChart: TcxButton
          Left = 473
          Top = 5
          TabOrder = 15
          ExplicitLeft = 473
          ExplicitTop = 5
        end
        inherited btnRestartUsedDiffer: TcxButton
          Left = 509
          Top = 5
          TabOrder = 24
          ExplicitLeft = 509
          ExplicitTop = 5
        end
        inherited btnLightRefresh: TcxButton
          Left = 5
          Top = 5
          ExplicitLeft = 5
          ExplicitTop = 5
        end
        inherited btnOpenLocalHistory: TcxButton
          Left = 545
          Top = 5
          TabOrder = 25
          ExplicitLeft = 545
          ExplicitTop = 5
        end
        inherited btnDashboard: TcxButton
          Left = 581
          Top = 5
          ExplicitLeft = 581
          ExplicitTop = 5
        end
        inherited btnOpenSSHExecute: TcxButton
          Left = 617
          Top = 5
          ExplicitLeft = 617
          ExplicitTop = 5
        end
        inherited laylblDisqualified: TdxLayoutItem
          ControlOptions.OriginalHeight = 13
        end
        inherited laylblSuspended: TdxLayoutItem
          ControlOptions.OriginalHeight = 13
        end
        inherited laylblAuditRed: TdxLayoutItem
          ControlOptions.OriginalHeight = 13
        end
        inherited laylblAuditYellow: TdxLayoutItem
          ControlOptions.OriginalHeight = 13
        end
        inherited laygrpMainHealth: TdxLayoutGroup
          Index = 16
        end
        inherited laylblSuspensionRed: TdxLayoutItem
          ControlOptions.OriginalHeight = 13
        end
        inherited laylblOnlineRed: TdxLayoutItem
          ControlOptions.OriginalHeight = 13
        end
        inherited laylblOnlineYellow: TdxLayoutItem
          ControlOptions.OriginalHeight = 13
        end
        inherited laylblWholeSNUsed: TdxLayoutItem
          ControlOptions.OriginalHeight = 13
          ControlOptions.OriginalWidth = 149
          Index = 13
        end
        inherited laybtnExportData: TdxLayoutItem
          Visible = False
        end
        inherited laybtnRebootPC: TdxLayoutItem
          Index = 14
        end
        inherited laybtnOpenChart: TdxLayoutItem
          Index = 15
        end
        inherited laybtnRestartUsedDiffer: TdxLayoutItem
          Index = 17
        end
        inherited laybtnOpenLocalHistory: TdxLayoutItem
          Index = 18
        end
      end
      inherited dxUIAdornerManager: TdxUIAdornerManager
        inherited badgeREDCount: TdxBadge
          Font.Height = -11
          Font.Name = 'Tahoma'
        end
      end
    end
    inline frameStatusBar: TframeStatusBar
      Left = 0
      Top = 525
      Width = 1257
      Height = 40
      TabOrder = 5
      ExplicitTop = 525
      ExplicitWidth = 1257
      inherited dxLayoutControl: TdxLayoutControl
        Width = 1257
        ExplicitWidth = 1257
        inherited StatusBar: TdxStatusBar
          Width = 1211
          ExplicitWidth = 1211
        end
      end
    end
    object dxLayoutControlGroup_Root: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avClient
      Hidden = True
      ItemIndex = 3
      Padding.Top = -5
      Padding.AssignedValues = [lpavTop]
      ShowBorder = False
      Index = -1
    end
    object layTreeList: TdxLayoutItem
      Parent = dxLayoutControlGroup_Root
      AlignHorz = ahClient
      AlignVert = avClient
      Control = TreeList
      ControlOptions.OriginalHeight = 511
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object laymmoLog: TdxLayoutItem
      Parent = dxLayoutGroup1
      AlignHorz = ahClient
      CaptionOptions.Text = 'Log'
      CaptionOptions.Layout = clTop
      Control = mmoLog
      ControlOptions.OriginalHeight = 156
      ControlOptions.OriginalWidth = 185
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object laygrpBottomLogs: TdxLayoutGroup
      Parent = dxLayoutControlGroup_Root
      CaptionOptions.Text = 'New Group'
      ItemIndex = 1
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 2
    end
    object laymmoFinishedNodes: TdxLayoutItem
      Parent = laygrpBottomLogs
      AlignHorz = ahLeft
      AlignVert = avClient
      CaptionOptions.AlignHorz = taCenter
      CaptionOptions.Text = 'Finished Nodes'
      CaptionOptions.Layout = clTop
      Control = mmoFinishedNodes
      ControlOptions.OriginalHeight = 89
      ControlOptions.OriginalWidth = 354
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutGroup1: TdxLayoutGroup
      Parent = laygrpBottomLogs
      AlignHorz = ahClient
      CaptionOptions.Text = 'New Group'
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 0
    end
    object laybtnClearLog: TdxLayoutItem
      Parent = dxLayoutGroup2
      SizeOptions.Width = 25
      CaptionOptions.Text = 'cxButton2'
      CaptionOptions.Visible = False
      CaptionOptions.Layout = clTop
      Control = btnClearLog
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutGroup2: TdxLayoutGroup
      Parent = dxLayoutGroup1
      CaptionOptions.Text = 'New Group'
      SizeOptions.Width = 25
      ShowBorder = False
      Index = 0
    end
    object layFrameButtons: TdxLayoutItem
      Parent = dxLayoutControlGroup_Root
      Padding.Bottom = -5
      Padding.Left = -5
      Padding.Right = -5
      Padding.Top = -5
      Padding.AssignedValues = [lpavBottom, lpavLeft, lpavRight, lpavTop]
      Control = frameButtons
      ControlOptions.AutoColor = True
      ControlOptions.OriginalHeight = 42
      ControlOptions.OriginalWidth = 1082
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object layframeStatusBar: TdxLayoutItem
      Parent = dxLayoutControlGroup_Root
      Padding.Bottom = -7
      Padding.Left = -10
      Padding.Right = -10
      Padding.Top = -5
      Padding.AssignedValues = [lpavBottom, lpavLeft, lpavRight, lpavTop]
      Control = frameStatusBar
      ControlOptions.AutoColor = True
      ControlOptions.OriginalHeight = 40
      ControlOptions.OriginalWidth = 1102
      ControlOptions.ShowBorder = False
      Index = 3
    end
  end
  object treePopupMenu: TPopupMenu
    Left = 216
    Top = 216
    object T1: TMenuItem
      Caption = 'Open node in browser'
      OnClick = T1Click
    end
    object T2: TMenuItem
      Caption = 'Check neighbours'
      OnClick = T2Click
    end
    object Refreshnode1: TMenuItem
      Caption = 'Refresh node'
      OnClick = Refreshnode1Click
    end
    object btnNodeExecuteSSH: TMenuItem
      Caption = 'Execute SSH'
      OnClick = btnNodeExecuteSSHClick
    end
  end
end
