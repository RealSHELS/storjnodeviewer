unit ufrmTreeSettings;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  System.IOUtils,
  Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters, cxClasses,
  dxLayoutContainer, dxLayoutControl, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, dxDateRanges, Data.DB, cxDBData,
  cxGridLevel, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxCheckBox, cxTextEdit,
  dxLayoutControlAdapters, Vcl.Menus, Vcl.StdCtrls, cxButtons, cxTL,
  cxTLdxBarBuiltInMenu, cxInplaceContainer, cxMaskEdit, cxDBTL, cxTLData,
  System.JSON, Storj.Types, System.Generics.Collections, dxSkinBasic,
  dxSkiniMaginary,
  dxSkinSeven,
  dxSkinSevenClassic,
  dxSkinWhiteprint, dxScrollbarAnnotations, cxDropDownEdit,
  Storj.Consts, uNotifyCenter, dxSkinWXI, dxSkinOffice2013LightGray,
  dxSkinOffice2019Black, dxSkinOffice2019DarkGray, dxSkinOffice2019White;

type
  TfrmTreeSettings = class(TForm)
    dxLayoutControl1Group_Root: TdxLayoutGroup;
    dxLayoutControl1: TdxLayoutControl;
    laygrpMain: TdxLayoutGroup;
    laygrpBottom: TdxLayoutGroup;
    btnSave: TcxButton;
    laybtnSave: TdxLayoutItem;
    btnCancel: TcxButton;
    laybtnCancel: TdxLayoutItem;
    treeStructure: TcxTreeList;
    laytreeStructure: TdxLayoutItem;
    clmTitle: TcxTreeListColumn;
    clmAddress: TcxTreeListColumn;
    clmPort: TcxTreeListColumn;
    clmIsActive: TcxTreeListColumn;
    clmAutoExpand: TcxTreeListColumn;
    clmComment: TcxTreeListColumn;
    clmCost: TcxTreeListColumn;
    clmCurrency: TcxTreeListColumn;
    clmHDDVolume: TcxTreeListColumn;
    btnShowSSHSettings: TcxButton;
    laybtnShowSSHSettings: TdxLayoutItem;
    laygrpBottomButtons: TdxLayoutGroup;
    procedure btnCancelClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure treeStructureDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure treeStructureDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnShowSSHSettingsClick(Sender: TObject);
  private
    function GetChildsJSON(ANode: TcxTreeListNode; const aParentPath: String = ''): TJSONArray;
    procedure SetJArrNodes(AJArr: TJSONArray; AParentNode: TcxTreeListNode);
    procedure SaveTreeToJSON;
    procedure LoadTreeFromJSON;

    procedure ShowSettings(const Data: TValue);

    procedure CommandBeforeStartGetNodeData(const aData: TValue);
    procedure CommandAfterGetNodeData(const aData: TValue);
  public
  end;

var
  frmTreeSettings: TfrmTreeSettings;

implementation

{$R *.dfm}

uses
  uDM;

{ TfrmSettings }

procedure TfrmTreeSettings.btnCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmTreeSettings.btnSaveClick(Sender: TObject);
begin
  treeStructure.FindCriteria.Text := '';
  SaveTreeToJSON;
  DM.ReloadFromJSON;
  DM.LoadNodesData(TGetInfoMode.gimLight, TLoadSourceMode.lsmManual);
  Close;
end;

procedure TfrmTreeSettings.btnShowSSHSettingsClick(Sender: TObject);
begin
  TNotifyCenter.Instance.SendMessage(COMMAND_SHOW_SSH_SETTINGS, nil);
end;

procedure TfrmTreeSettings.CommandAfterGetNodeData(const aData: TValue);
begin
  laybtnSave.Enabled := True;
end;

procedure TfrmTreeSettings.CommandBeforeStartGetNodeData(const aData: TValue);
begin
  laybtnSave.Enabled := False;
end;

procedure TfrmTreeSettings.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  treeStructure.FindCriteria.Text := '';
end;

procedure TfrmTreeSettings.FormCreate(Sender: TObject);
begin
  TNotifyCenter.Instance.Subscribe(COMMAND_SHOW_TREE_SETTINGS, ShowSettings);
  TNotifyCenter.Instance.Subscribe(COMMAND_BEFORE_START_GET_NODE, CommandBeforeStartGetNodeData);
  TNotifyCenter.Instance.Subscribe(COMMAND_AFTER_GET_NODE_DATA, CommandAfterGetNodeData);
end;

procedure TfrmTreeSettings.FormDestroy(Sender: TObject);
begin
  TNotifyCenter.Instance.UnSubscribe(COMMAND_SHOW_TREE_SETTINGS, ShowSettings);
  TNotifyCenter.Instance.UnSubscribe(COMMAND_BEFORE_START_GET_NODE, CommandBeforeStartGetNodeData);
  TNotifyCenter.Instance.UnSubscribe(COMMAND_AFTER_GET_NODE_DATA, CommandAfterGetNodeData);
end;

procedure TfrmTreeSettings.treeStructureDragDrop(Sender, Source: TObject; X,
  Y: Integer);
var
  RecieverNode, DragNode: TcxTreeListNode;
begin
  RecieverNode := TcxTreeList(Sender).GetNodeAt(X, Y);
  DragNode := TcxTreeListNode(TcxTreeList(Source).DragNode);

  if DragNode.Parent = RecieverNode then
    RecieverNode.MoveTo(DragNode, TcxTreeListNodeAttachMode.tlamAdd);
end;

procedure TfrmTreeSettings.treeStructureDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := True;
end;

function TfrmTreeSettings.GetChildsJSON(ANode: TcxTreeListNode; const aParentPath: String = ''): TJSONArray;
var
  Node: TcxTreeListNode;
  JObj: TJSONObject;
  Title, FullPath: String;
begin
  Node := nil;
  Result := TJSONArray.Create;
  repeat
    if Node = nil then
      Node := ANode.getFirstChild
    else
      Node := ANode.GetNextChild(Node);

    Title := VarToStr(Node.Values[clmTitle.Position.ColIndex]);

    if aParentPath.Length = 0 then
      FullPath := Title
    else
      FullPath := aParentPath + ' - ' + Title;

    JObj := TJSONObject.Create;
    JObj.AddPair('Title', Title);
    JObj.AddPair('Address', VarToStr(Node.Values[clmAddress.Position.ColIndex]));
    JObj.AddPair('Port', VarToStr(Node.Values[clmPort.Position.ColIndex]));
    JObj.AddPair('FullPath', FullPath);
    if Node.Values[clmIsActive.Position.ColIndex] = null then
      JOBj.AddPair('IsActive', TJSONBool.Create(False))
    else
      JObj.AddPair('IsActive', TJSONBool.Create(Node.Values[clmIsActive.Position.ColIndex]));

    if Node.Values[clmAutoExpand.Position.ColIndex] = null then
      JOBj.AddPair('AutoExpand', TJSONBool.Create(False))
    else
      JObj.AddPair('AutoExpand', TJSONBool.Create(Node.Values[clmAutoExpand.Position.ColIndex]));

    if Node.HasChildren then
      JObj.AddPair('Childs', GetChildsJSON(Node, FullPath));

    JObj.AddPair('Comment', VarToStr(Node.Values[clmComment.Position.ColIndex]));

    JObj.AddPair('Cost', VarToStr(Node.Values[clmCost.Position.ColIndex]));
    JObj.AddPair('Currency', VarToStr(Node.Values[clmCurrency.Position.ColIndex]));
    JObj.AddPair('HDDVolume', VarToStr(Node.Values[clmHDDVolume.Position.ColIndex]));

    Result.AddElement(JObj);
  until Node.IsLast;
end;

procedure TfrmTreeSettings.LoadTreeFromJSON;
var
  StrList: TStringList;
  JArr: TJSONArray;
begin
  var Path := ExtractFilePath(Application.ExeName);

  if not FileExists(Path + TreeJSONFileName) then
    Exit;

  JArr := nil;
  StrList := TStringList.Create;
  try
    StrList.LoadFromFile(Path + TreeJSONFileName);
    JArr := TJSONObject.ParseJSONValue(StrList.Text) as TJSONArray;
    if not Assigned(JArr) then
      Exit;

    SetJArrNodes(JArr, treeStructure.Root);
  finally
    JArr.Free;
    StrList.Free;
  end;
end;

procedure TfrmTreeSettings.SaveTreeToJSON;
var
  JArr: TJSONArray;
  StrList: TStringList;
begin
  var Path := ExtractFilePath(Application.ExeName);

  if TFile.Exists(Path + TreeJSONFileName) then
    TFile.Copy(Path + TreeJSONFileName, Path + TreeJSONFileNameBackup, true);

  JArr := nil;
  StrList := TStringList.Create;
  try
    JArr := GetChildsJSON(treeStructure.Root);
    StrList.Text := JArr.ToString;
    StrList.SaveToFile(Path + TreeJSONFileName);
  finally
    JArr.Free;
    StrList.Free;
  end;
end;

procedure TfrmTreeSettings.SetJArrNodes(AJArr: TJSONArray; AParentNode: TcxTreeListNode);
var
  i: integer;
  JObj: TJSONObject;
  JValue: TJSONValue;
  Node: TcxTreeListNode;
begin
  for i := 0 to AJarr.Count - 1 do begin
    JObj := AJArr.Items[i] as TJSONObject;
    Node := AParentNode.AddChild;
    Node.Values[clmTitle.Position.ColIndex] := JObj.GetValue<String>('Title');
    Node.Values[clmAddress.Position.ColIndex] := JObj.GetValue<String>('Address');
    Node.Values[clmIsActive.Position.ColIndex] := JObj.GetValue<Boolean>('IsActive');
    Node.Values[clmPort.Position.ColIndex] := JObj.GetValue<String>('Port');
    Node.Values[clmAutoExpand.Position.ColIndex] := JObj.GetValue<Boolean>('AutoExpand', False);
    Node.Values[clmComment.Position.ColIndex] := JObj.GetValue<String>('Comment', '');
    Node.Values[clmCost.Position.ColIndex] := JObj.GetValue<String>('Cost', '');
    Node.Values[clmCurrency.Position.ColIndex] := JObj.GetValue<String>('Currency', '');
    Node.Values[clmHDDVolume.Position.ColIndex] := JObj.GetValue<String>('HDDVolume', '');

    JValue := JObj.GetValue<TJSONArray>('Childs', nil);
    if JValue <> nil then
      SetJArrNodes(JValue as TJSONArray, Node);
  end;
end;

procedure TfrmTreeSettings.ShowSettings(const Data: TValue);
begin
  treeStructure.Clear;
  LoadTreeFromJSON;
  treeStructure.Root.Expand(true);
  Show;
end;

end.
