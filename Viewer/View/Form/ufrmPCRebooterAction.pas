unit ufrmPCRebooterAction;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBasic, dxSkiniMaginary, dxSkinSeven,
  dxSkinSevenClassic, dxLayoutcxEditAdapters, dxLayoutControlAdapters,
  cxContainer, cxEdit, Vcl.ExtCtrls, dxLayoutContainer, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxClasses, dxLayoutControl, Storj.Settings, Vcl.Menus,
  Vcl.StdCtrls, cxButtons, uNotifyCenter, Storj.Consts,
  PCRebooter.ForWebserver, System.UITypes, dxSkinOffice2013LightGray,
  dxSkinOffice2019Black, dxSkinOffice2019DarkGray, dxSkinOffice2019White,
  dxSkinWhiteprint;

type
  TfrmPCRebooterAction = class(TForm)
    dxLayoutControlGroup_Root: TdxLayoutGroup;
    dxLayoutControl: TdxLayoutControl;
    comboPCRebooters: TcxComboBox;
    laycomboPCRebooters: TdxLayoutItem;
    pnlActionButtons: TFlowPanel;
    laypnlActionButtons: TdxLayoutItem;
    cxButton1: TcxButton;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);

    procedure CommandShow(const aData: TValue);
    procedure CommandSettingsChanged(const aData: TValue);
    procedure comboPCRebootersPropertiesEditValueChanged(Sender: TObject);

    procedure OnButtonActionClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPCRebooterAction: TfrmPCRebooterAction;

implementation

{$R *.dfm}

procedure TfrmPCRebooterAction.comboPCRebootersPropertiesEditValueChanged(Sender: TObject);
var
  i: integer;
  Btn: TcxButton;
  PCRebooter: TPCRebooterSettings;
  PCNames: TArray<String>;
begin
  i := 0;
  while i < pnlActionButtons.ControlCount do
    if pnlActionButtons.Controls[i] is TcxButton then
      pnlActionButtons.Controls[i].Free
    else
      i := i + 1;

  if comboPCRebooters.ItemIndex < 0 then
    Exit;

  for var Item in GetSettings.PCRebooters do
    if Item.Name = comboPCRebooters.Text then
      PCRebooter := Item;

  PCNames := PCRebooter.PCNames.Split([',']);

  if PCRebooter.Nums > 0 then
    for i := 1 to PCRebooter.Nums do begin
      Btn := TcxButton.Create(pnlActionButtons);
      Btn.Parent := pnlActionButtons;
      Btn.Caption := i.ToString;
      Btn.Tag := i;
      Btn.Width := pnlActionButtons.Width - 10;
      Btn.Height := 30;
      Btn.OnClick := OnButtonActionClick;

      if (i <= Length(PCNames)) and (PCNames[i - 1].Length > 0) then
        Btn.Caption := Btn.Caption + ' - ' + PCNames[i - 1];
    end;
end;

procedure TfrmPCRebooterAction.CommandSettingsChanged(const aData: TValue);
begin
  if Self.Showing then
    FormShow(nil);
end;

procedure TfrmPCRebooterAction.CommandShow(const aData: TValue);
begin
  Show;
end;

procedure TfrmPCRebooterAction.FormCreate(Sender: TObject);
begin
  TNotifyCenter.Instance.Subscribe(COMMAND_SHOW_PCREBOOTER, CommandShow);
  TNotifyCenter.Instance.Subscribe(COMMAND_SETTING_CHANGED, CommandSettingsChanged);
end;

procedure TfrmPCRebooterAction.FormDestroy(Sender: TObject);
begin
  TNotifyCenter.Instance.UnSubscribe(COMMAND_SHOW_PCREBOOTER, CommandShow);
  TNotifyCenter.Instance.UnSubscribe(COMMAND_SETTING_CHANGED, CommandSettingsChanged);
end;

procedure TfrmPCRebooterAction.FormShow(Sender: TObject);
var
  i: integer;
begin
  var Rebooters := GetSettings.PCRebooters;

  comboPCRebooters.ItemIndex := -1;
  comboPCRebooters.Properties.Items.Clear;

  for var Rebooter in Rebooters do
    if Rebooter.IsActive then
      comboPCRebooters.Properties.Items.Add(Rebooter.Name);

  i := 0;
  while i < pnlActionButtons.ControlCount do
    if pnlActionButtons.Controls[i] is TcxButton then
      pnlActionButtons.Controls[i].Free
    else
      i := i + 1;
end;

procedure TfrmPCRebooterAction.OnButtonActionClick(Sender: TObject);
var
  PCRebooter: TPCRebooterSettings;
begin
  for var Item in GetSettings.PCRebooters do
    if Item.Name = comboPCRebooters.Text then
      PCRebooter := Item;

  if PCRebooter.URL.Length = 0 then
    Exit;

  var AskText := 'Are you sure want reboot PC �' + TcxButton(Sender).Tag.ToString + '?';
  if MessageDlg(AskText, TMsgDlgType.mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
    if SendReboot(PCRebooter, TcxButton(Sender).Tag) then
      MessageDlg('Sucesfull rebooted', TMsgDlgType.mtInformation, [mbOk], 0)
    else
      MessageDlg('Something went wrong', TMsgDlgType.mtError, [mbOk], 0);
  end;
end;

end.
