create table if not exists RestartUsedDifferHistory (
	ID INTEGER PRIMARY KEY,
	NodeID text,
	Title text,
	OldUsed INTEGER,
	NewUsed INTEGER,
	OldUptime INTEGER,
	NewUptime INTEGER,
	OldTrash INTEGER,
	NewTrash INTEGER,
	CreationDT DATETIME
);