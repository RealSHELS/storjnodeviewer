const express = require('express');
const axios = require('axios');
const cron = require('node-cron');

const app = express();
const PORT = process.env.PORT || 3000;
const API_URL = 'https://api.getgeoapi.com/v2/currency/convert';
const API_KEY = 'c53f04d25ea73c8578f2bbe206fe6fb719ca9188';
let exchangeRates = {};

async function fetchExchangeRates() {
    try {
        const response = await axios.get(`${API_URL}?api_key=${API_KEY}&from=USD&amount=1&format=json`);
        exchangeRates = response.data;
    } catch (error) {
        console.error('Error get rate data:', error.message);
    }
}

fetchExchangeRates();

cron.schedule('0 2 * * *', fetchExchangeRates);

app.get('/api/rates', (req, res) => {
    res.json(exchangeRates);
});

app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
});
