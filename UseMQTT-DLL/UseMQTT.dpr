library UseMQTT;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  System.SysUtils,
  System.Classes,
  uMQTTComps, uMQTT;

type
  TOnMsgOuterCallback = procedure (aTopic: PChar; aMessage: PChar) of object;
  TOnMonOuterCallback = procedure (aMessage: PChar) of object;
  TOnOuterCallback = procedure of object;

  TMQTTReceiver = class
    procedure OnMsg(Sender: TObject; aTopic: UTF8String; aMessage: AnsiString; aQos: TMQTTQOSType; aRetained : boolean);
    procedure OnMon(Sender : TObject; aStr : string);
    procedure OnOnline(Sender: TObject);
  end;

var
  MQTT: TMQTTClient = nil;
  Receiver: TMQTTReceiver;
  OnMsgCallback: TOnMsgOuterCallback = nil;
  OnMonCallback: TOnMonOuterCallback = nil;
  OnOnlineCallback: TOnOuterCallback = nil;

{$R *.res}

procedure TMQTTReceiver.OnMon(Sender: TObject; aStr: string);
begin
  if Assigned(OnMonCallback) then
    OnMonCallback(PChar(aStr));
end;

procedure TMQTTReceiver.OnMsg(Sender: TObject; aTopic: UTF8String; aMessage: AnsiString; aQos: TMQTTQOSType; aRetained : boolean);
begin
  if Assigned(OnMsgCallback) then
    OnMsgCallback(PChar(UTF8ToString(aTopic)), PChar(String(aMessage)));
end;

procedure TMQTTReceiver.OnOnline(Sender: TObject);
begin
  if Assigned(OnOnlineCallback) then
    OnOnlineCallback;
end;

procedure SetOnMsg(aProc: TOnMsgOuterCallback);
begin
  OnMsgCallback := aProc;
end;

procedure SetOnMon(aProc: TOnMonOuterCallback);
begin
  OnMonCallback := aProc;
end;

procedure SetOnOnline(aProc: TOnOuterCallback);
begin
  OnOnlineCallback := aProc;
end;

function Connect(aHost: PChar; aPort: Integer): Integer;
begin
  Result := 0;

  try
    Receiver := TMQTTReceiver.Create;

    if not Assigned(MQTT) then begin
      MQTT := TMQTTClient.Create(nil);
      MQTT.Host := String(aHost);
      MQTT.Port := aPort;
      MQTT.OnMsg := Receiver.OnMsg;
      MQTT.OnMon := Receiver.OnMon;
      MQTT.OnOnline := Receiver.OnOnline;
      MQTT.Activate(true);
    end else begin
      FreeAndNil(MQTT);
      Result := 2;
    end;
  except
    Result := 1;
  end;
end;

function IsConnected: Boolean;
begin
  Result := false;

  if Assigned(MQTT) then
    Result := MQTT.Online;
end;

procedure Publish(aTopic: PChar; aMessage: PChar);
begin
  MQTT.Publish(UTF8String(aTopic), AnsiString(aMessage), TMQTTQOSType.qtAT_MOST_ONCE);
end;

procedure Subscribe(aTopic: PChar);
begin
  MQTT.Subscribe(UTF8String(aTopic), TMQTTQOSType.qtAT_MOST_ONCE);
end;

procedure Unsubscribe(aTopic: PChar);
begin
  MQTT.Unsubscribe(UTF8String(aTopic));
end;

function IsSubscribed(aTopic: PChar): Boolean;
begin
  Result := MQTT.Subscriptions.IndexOf(String(aTopic)) >= 0;
end;

procedure Disconnect;
begin
  if Assigned(MQTT) then begin
    FreeAndNil(MQTT);
    Receiver.Free;
  end;
end;

exports Connect;
exports IsConnected;
exports Publish;
exports Disconnect;
exports SetOnMsg;
exports SetOnMon;
exports SetOnOnline;
exports Subscribe;
exports Unsubscribe;
exports IsSubscribed;

begin
end.
