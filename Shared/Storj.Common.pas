unit Storj.Common;

interface

  procedure OpenURL(const AURL: String);
  function UptimeMinutesToStr(aUptimeMinutes: Integer): String;

implementation

uses WinApi.Windows, Winapi.ShellAPI,
  System.SysUtils;

procedure OpenURL(const AURL: String);
begin
  ShellExecute(0, 'open', PChar(AURL), nil, nil, SW_SHOWNORMAL);
end;

function UptimeMinutesToStr(aUptimeMinutes: Integer): String;
var
  Minute, Day, Hour: Integer;
begin
  Minute := aUptimeMinutes mod 60;
  Day := aUptimeMinutes div (24 * 60);
  Hour := (aUptimeMinutes - Day * 24 * 60) div 60;

  Result := '';
  if Day > 0 then
    Result := Day.ToString + 'd ';

  if Hour > 0 then
    Result := Result + Hour.ToString + 'h ';

  Result := Result + Minute.ToString + 'm';
end;

end.
