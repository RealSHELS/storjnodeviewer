unit uUseMQTTDLL;

interface

uses
  System.SysUtils, System.IOUtils,
  Winapi.Windows;

type
  TMsgCallback = procedure (aTopic: PChar; aMessage: PChar) of object;
  TMonCallback = procedure (aMessage: PChar) of object;
  TOnlineCallback = procedure of object;

  TMQTTDLL = class
  private
    fDLL: THandle;
  public
    procedure Connect(const aHost: String; aPort: Integer);
    function IsConnected: Boolean;
    procedure Publish(const aTopic, aMessage: String);
    procedure Disconnect;
    procedure SetOnMsgCallback(aProc: TMsgCallback);
    procedure SetOnMonCallback(aProc: TMonCallback);
    procedure SetOnOnlineCallback(aProc: TOnlineCallback);
    procedure Subscribe(aTopic: String);

    constructor Create(const aDLLName: String); reintroduce;
    destructor Destroy; override;
  end;

  TMQTTDLLConnect = procedure (aHost: PChar; aPort: Integer);
  TMQTTDLLIsConnected = function :Boolean;
  TMQTTDLLPublish = procedure (aTopic: PChar; aMessage: PChar);
  TMQTTDLLDisconect = procedure ;
  TMQTTDLLSetOnMsg = procedure (aProc: TMsgCallback);
  TMQTTDLLSetOnMon = procedure (aProc: TMonCallback);
  TMQTTDLLSetOnOnline = procedure (aProc: TOnlineCallback);
  TMQTTDLLSubscribe = procedure (aTopic: PChar);

implementation

{ TMQTTDLL }

procedure TMQTTDLL.Connect(const aHost: String; aPort: Integer);
var
  Func: TMQTTDLLConnect;
begin
  @Func := GetProcAddress(fDll, 'Connect');
  if @Func <> nil then
    Func(PChar(aHost), aPort);
end;

constructor TMQTTDLL.Create(const aDLLName: String);
begin
  if not TFile.Exists(aDLLName) then
    raise Exception.Create('Error Message');

  fDLL := LoadLibrary(PChar(aDLLName));
  if fDLL = 0 then
    raise Exception.Create('Error Message');
end;

destructor TMQTTDLL.Destroy;
begin
  Disconnect;
  FreeLibrary(fDLL);
  inherited;
end;

procedure TMQTTDLL.Disconnect;
var
  Func: TMQTTDLLDisconect;
begin
  @Func := GetProcAddress(fDll, 'Disconnect');
  if @Func <> nil then
    Func;
end;

function TMQTTDLL.IsConnected: Boolean;
var
  Func: TMQTTDLLIsConnected;
begin
  Result := False;

  @Func := GetProcAddress(fDll, 'IsConnected');
  if @Func <> nil then
    Result := Func();
end;

procedure TMQTTDLL.Publish(const aTopic, aMessage: String);
var
  Func: TMQTTDLLPublish;
begin
  @Func := GetProcAddress(fDll, 'Publish');
  if @Func <> nil then
    Func(PChar(aTopic), PChar(aMessage));
end;

procedure TMQTTDLL.SetOnMonCallback(aProc: TMonCallback);
var
  Func: TMQTTDLLSetOnMon;
begin
  @Func := GetProcAddress(fDll, 'SetOnMon');
  if @Func <> nil then
    Func(aProc);
end;

procedure TMQTTDLL.SetOnMsgCallback(aProc: TMsgCallback);
var
  Func: TMQTTDLLSetOnMsg;
begin
  @Func := GetProcAddress(fDll, 'SetOnMsg');
  if @Func <> nil then
    Func(aProc);
end;

procedure TMQTTDLL.SetOnOnlineCallback(aProc: TOnlineCallback);
var
  Func: TMQTTDLLSetOnOnline;
begin
  @Func := GetProcAddress(fDll, 'SetOnOnline');
  if @Func <> nil then
    Func(aProc);
end;

procedure TMQTTDLL.Subscribe(aTopic: String);
var
  Func: TMQTTDLLSubscribe;
begin
  @Func := GetProcAddress(fDll, 'Subscribe');
  if @Func <> nil then
    Func(PChar(aTopic));
end;

end.
