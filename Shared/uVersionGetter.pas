unit uVersionGetter;

interface

uses
  System.SysUtils,
  System.Threading,
  System.Classes,
  System.Net.HTTPClient,
  System.IOUtils;

type
  THTTPResponseAccess = class(THTTPResponse);

  TStorjIOVersion = record
    Version: string;
    Cursor: string;
  end;

  TStrCallback = reference to procedure (AVersion: String);
  TStorjIOVersionCallback = reference to procedure (AStorjIOVersion: TStorjIOVersion);
  TStreamCallback = reference to procedure (AStream: TStream);

  procedure GetGithubLatestVersion(ACallback: TStrCallback);
  procedure GetStorjIOLatestVersion(ACallback: TStorjIOVersionCallback);
  function GetStorjIOLatestVersionSync: TStorjIOVersion;
  procedure UpdateNode(const AFromPath, AToPath, AOldVersion: String);

  function DownloadFromGithub(const AVersion: String): TStream;
  procedure DownloadFromGithubAsync(const AVersion: String; ACallback: TStreamCallback);

  function GetFileSize(const AFileName: String): Int64;
  function FileSizeToStr(AFileSize: Int64): String;

implementation

uses
  System.JSON, WinApi.Windows,
  AMR.AutoObject;

procedure GetGithubLatestVersion(ACallback: TStrCallback);
begin
  TTask.Create(
    procedure
    var
      Version: String;
      HTTP: THTTPClient;
      Resp: IHTTPResponse;
    begin
      Version := '';
      try
        HTTP := THTTPClient.Create;
        try
          Resp := HTTP.Get('https://github.com/storj/storj/releases/latest');
          Version := THTTPResponseAccess(Resp as THTTPResponse).FRequest.URL.ToString;
          Version := Version.Substring(Version.LastIndexOf('v') + 1);
        finally
          HTTP.Free;
        end;
      except
        Version := '-';
      end;

      if Assigned(ACallback) then
        TThread.Queue(nil, procedure begin ACallback(Version) end);
    end
  ).Start;
end;

function GetStorjIOLatestVersionSync: TStorjIOVersion;
var
  HTTP: THTTPClient;
  Resp: IHTTPResponse;
  JSON, SubJSON: TJSONObject;
begin
  Result.Version := '';
  Result.Cursor := '';

  try
    HTTP := THTTPClient.Create;
    try
      Resp := HTTP.Get('https://version.storj.io');
      JSON := TJSONObject.ParseJSONValue(Resp.ContentAsString) as TJSONObject;

      if not Assigned(JSON) then
        Exit;

      try
       SubJSON := (JSON.Values['processes'] as TJSONObject).Values['storagenode'] as TJSONObject;

       Result.Version := (SubJSON.Values['suggested'] as TJSONObject).GetValue<String>('version');
       Result.Cursor := (SubJSON.Values['rollout'] as TJSONObject).GetValue<String>('cursor');
      finally
        JSON.Free;
      end;
    finally
      HTTP.Free;
    end;
  except
    Result.Version := '-';
    Result.Cursor := '-';
  end;
end;

procedure GetStorjIOLatestVersion(ACallback: TStorjIOVersionCallback);
begin
  TTask.Create(
    procedure
    var
      Result: TStorjIOVersion;
    begin
      Result := GetStorjIOLatestVersionSync;

      if Assigned(ACallback) then
        TThread.Queue(nil, procedure begin ACallback(Result) end);
    end
  ).Start;
end;

procedure UpdateNode(const AFromPath, AToPath, AOldVersion: String);
var
  FromFilename, ToFilename, BackupFilename: String;
begin
  FromFileName := IncludeTrailingPathDelimiter(AFromPath) + 'storagenode.exe';
  ToFileName := IncludeTrailingPathDelimiter(AToPath) + 'storagenode.exe';

  if not TFile.Exists(IncludeTrailingPathDelimiter(AToPath) + 'storagenode_v' + AOldVersion + '.exe') then
    BackupFilename := IncludeTrailingPathDelimiter(AToPath) + 'storagenode_v' + AOldVersion + '.exe';

  if not BackupFilename.IsEmpty and not TFile.Exists(BackupFilename) and TFile.Exists(ToFileName) then
    TFile.Move(ToFilename, BackupFilename);

  if TFile.Exists(ToFilename) then
    TFile.Delete(ToFileName);

  TFile.Copy(FromFilename, ToFilename);

  if TFile.Exists(IncludeTrailingPathDelimiter(AToPath) + 'storagenode.log') then
    TFile.Delete(IncludeTrailingPathDelimiter(AToPath) + 'storagenode.log');
end;

function DownloadFromGithub(const AVersion: String): TStream;
begin
  Result := nil;

  var URL := 'https://github.com/storj/storj/releases/download/v{version}/storagenode_windows_amd64.zip'.Replace('{version}', AVersion);

  var HTTP := THTTPClient.Create;
  var AutoFree := TAutoFree.Create(HTTP);

  try
    var Resp := HTTP.Get(URL);
    Result := TMemoryStream.Create;
    Result.CopyFrom(Resp.ContentStream);
    Result.Position := 0;
  except
  end;
end;

procedure DownloadFromGithubAsync(const AVersion: String; ACallback: TStreamCallback);
begin
  TTask.Create(
    procedure
    var
      Result: TStream;
    begin
      Result := DownloadFromGithub(AVersion);

      TThread.Queue(nil,
        procedure
        begin
          try
            if Assigned(ACallback) then
              ACallback(Result);
          finally
            Result.Free;
          end;
        end);
    end
  ).Start;
end;

function GetFileSize(const AFileName: String): Int64;
var
  info: TWin32FileAttributeData;
begin
  Result := -1;

  if not GetFileAttributesEx(PWideChar(AFileName), GetFileExInfoStandard, @info) then
    EXIT;

  Result := Int64(info.nFileSizeLow) or Int64(info.nFileSizeHigh shl 32);
end;

function FileSizeToStr(AFileSize: Int64): String;
const
  BYTES: array [0..3] of string = ('B', 'KB', 'MB', 'GB');
var
  index: integer;
begin
  index := 0;
  while AFileSize > 1000 do begin
    index := index + 1;
    AFileSize := AFileSize div 1000;
  end;

  Result := AFileSize.ToString + ' ' + BYTES[index];
end;

end.
