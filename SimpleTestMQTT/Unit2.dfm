object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 361
  ClientWidth = 702
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 24
    Top = 96
    Width = 441
    Height = 233
    Lines.Strings = (
      'Memo1')
    TabOrder = 0
  end
  object Edit1: TEdit
    Left = 96
    Top = 32
    Width = 185
    Height = 21
    TabOrder = 1
    Text = 'echo'
  end
  object Button1: TButton
    Left = 312
    Top = 30
    Width = 75
    Height = 25
    Caption = 'Send'
    TabOrder = 2
    OnClick = Button1Click
  end
  object MQTTClient1: TMQTTClient
    KeepAlive = 10
    MaxRetries = 8
    RetryTime = 60
    Clean = False
    Broker = False
    AutoSubscribe = False
    Host = 'broker.hivemq.com'
    Port = 1883
    LocalBounce = False
    OnMon = MQTTClient1Mon
    OnOnline = MQTTClient1Online
    OnMsg = MQTTClient1Msg
    Left = 32
    Top = 24
  end
end
