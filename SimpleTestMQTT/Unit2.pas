unit Unit2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, uMQTTComps, uMQTT;

type
  TForm2 = class(TForm)
    MQTTClient1: TMQTTClient;
    Memo1: TMemo;
    Edit1: TEdit;
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure MQTTClient1Online(Sender: TObject);
    procedure MQTTClient1Msg(Sender: TObject; aTopic: UTF8String;
      aMessage: AnsiString; aQos: TMQTTQOSType; aRetained: Boolean);
    procedure MQTTClient1Mon(Sender: TObject; aStr: string);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

procedure TForm2.Button1Click(Sender: TObject);
begin
  MQTTClient1.Publish('/RealSHELS/PCRebooters/1FC4845B/Receive', Edit1.Text, TMQTTQOSType.qtAT_MOST_ONCE);
end;

procedure TForm2.FormCreate(Sender: TObject);
begin
  MQTTClient1.Activate(true);
end;

procedure TForm2.MQTTClient1Mon(Sender: TObject; aStr: string);
begin
  memo1.Lines.Add('OnMon ' + aStr);
end;

procedure TForm2.MQTTClient1Msg(Sender: TObject; aTopic: UTF8String;
  aMessage: AnsiString; aQos: TMQTTQOSType; aRetained: Boolean);
begin
  memo1.Lines.Add('OnMsg ' + aMessage);
end;

procedure TForm2.MQTTClient1Online(Sender: TObject);
begin
  memo1.Lines.Add('MQTT Connected');
  MQTTClient1.Subscribe('/RealSHELS/PCRebooters/1FC4845B/Send', TMQTTQOSType.qtAT_MOST_ONCE);
end;

end.
