object WorkerService: TWorkerService
  DisplayName = 'WorkerService'
  OnStart = ServiceStart
  OnStop = ServiceStop
  Height = 480
  Width = 640
  object tmrCheckRun: TTimer
    Enabled = False
    Interval = 1800000
    OnTimer = tmrCheckRunTimer
    Left = 168
    Top = 160
  end
  object tmrCheckUpdate: TTimer
    Enabled = False
    Interval = 2400000
    OnTimer = tmrCheckUpdateTimer
    Left = 352
    Top = 160
  end
end
