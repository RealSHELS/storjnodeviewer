unit uMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.SvcMgr, Vcl.Dialogs,
  System.IOUtils, WinAPI.ShellAPI, Winapi.WinSvc, Vcl.ExtCtrls, ServiceManager, System.JSON,
  uVersionGetter, PJVersionInfo, System.Zip, System.DateUtils;

type
  TNodeServiceData = record
    Path: String;
    Version: TPJVersionNumber;
  end;

  TUpdatingData = record
    FreshVersionDir: String;
    OldVersionDir: String;
    OldVersion: String;
    IsStartService: Boolean;
    ServiceName: String;
  end;

  TWorkerService = class(TService)
    tmrCheckRun: TTimer;
    tmrCheckUpdate: TTimer;
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
    procedure tmrCheckRunTimer(Sender: TObject);
    procedure tmrCheckUpdateTimer(Sender: TObject);
  private
    procedure LogMessage(const aStr: String);
    function GetWorkingDir: String;
    procedure StartSelectedNode(AServiceName: String);
    function GetNodeVersionByServiceName(const aServiceName: String): TNodeServiceData;
    procedure UnzipStoragenode(aStream: TStream);
    procedure DoUpgradeSelectedNode(const aUpdatingData: TUpdatingData);
  public
    function GetServiceController: TServiceController; override;
    { Public declarations }
  end;

var
  WorkerService: TWorkerService;

const
  Userenv = 'Userenv.dll';
function GetUserProfileDirectory(hToken: THandle; lpProfileDir: LPSTR; var lpcchSize: DWORD): BOOL; stdcall; external Userenv name 'GetUserProfileDirectoryA';

implementation

{$R *.dfm}

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  WorkerService.Controller(CtrlCode);
end;

function TWorkerService.GetNodeVersionByServiceName(const aServiceName: String): TNodeServiceData;
var
  SM: TServiceManager;
  SD: TServiceDescriptor;
  Ver: TPJVersionInfo;
  ExeName, CommandLine: String;
begin
  if AServiceName.IsEmpty then
    raise Exception.Create('ServiceName is empty');

  try
    SM := TServiceManager.Create(nil);
    Ver := TPJVersionInfo.Create(nil);
    try
      SM.Connect;

      SM.QueryServiceConfig(aServiceName, SD);
      CommandLine := SD.CommandLine;
      CommandLine := CommandLine.Remove(0, 1);
      ExeName := CommandLine.Substring(0, CommandLine.IndexOf('"'));

      Ver.FileName := ExeName;
      Result.Version := Ver.FileVersionNumber;
      Result.Path := ExtractFilePath(ExeName);
    finally
      Ver.Free;
      SM.Free;
    end;
  except
    on E: Exception do
      LogMessage(E.Message);
  end;
end;

function TWorkerService.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

function TWorkerService.GetWorkingDir: String;
begin
  Result := TPath.Combine(TPath.GetSharedDocumentsPath, 'WorkerService');
end;

procedure TWorkerService.LogMessage(const aStr: String);
begin
  var lMessage := TDateTime.Now.ToString + ' ' + aStr + sLineBreak;
  TFile.AppendAllText(ExtractFilePath(ParamStr(0)) + 'WorkerServiceProj.log', lMessage);
end;

procedure TWorkerService.ServiceStart(Sender: TService; var Started: Boolean);
begin
  LogMessage('Service starting');
  tmrCheckRun.Enabled := True;
  tmrCheckUpdate.Enabled := True;
end;

procedure TWorkerService.ServiceStop(Sender: TService; var Stopped: Boolean);
begin
  tmrCheckRun.Enabled := False;
  tmrCheckUpdate.Enabled := False;
  LogMessage('Service stoped');
end;

procedure TWorkerService.tmrCheckRunTimer(Sender: TObject);
begin
  var StrList := TStringList.Create;
  try
    StrList.LoadFromFile(ExtractFilePath(ParamStr(0)) + 'SHO Helper SavedData.json');
    var JSON := TJSONObject.ParseJSONValue(StrList.Text) as TJSONObject;
    try
      var JArr := JSON.GetValue<TJSONArray>('AutoStartNodes', nil);
      if Assigned(JArr) then
        for var JItem in JArr do
          StartSelectedNode(JItem.Value);
    finally
      JSON.Free;
    end;
  finally
    StrList.Free;
  end;
end;

procedure TWorkerService.tmrCheckUpdateTimer(Sender: TObject);
var
  IOVer: TPJVersionNumber;
  Stream: TStream;
  UpdatingData: TUpdatingData;
  isNeedDownloadFreshVersionFromGithub: Boolean;
begin
  var IOVersion := GetStorjIOLatestVersionSync;

  if (IOVersion.Cursor.Length > 1) and CharInSet(IOVersion.Cursor[1], ['a'..'f']) then begin
    IOVer := IOVersion.Version;

    var Ver := TPJVersionInfo.Create(nil);
    try
      var StrList := TStringList.Create;
      try
        StrList.LoadFromFile(ExtractFilePath(ParamStr(0)) + 'SHO Helper SavedData.json');
        var JSON := TJSONObject.ParseJSONValue(StrList.Text) as TJSONObject;
        try
          var JArr := JSON.GetValue<TJSONArray>('AutoUpdateNodes', nil);
          if Assigned(JArr) then
            for var JItem in JArr do begin
              var NodeServiceData := GetNodeVersionByServiceName(JItem.Value);

              if NodeServiceData.Version < IOVer then begin
                if TFile.Exists(IncludeTrailingPathDelimiter(GetWorkingDir) + 'storagenode.exe') then begin
                  Ver.FileName := IncludeTrailingPathDelimiter(GetWorkingDir) + 'storagenode.exe';
                  isNeedDownloadFreshVersionFromGithub := Ver.FileVersionNumber < IOVer;
                end else
                  isNeedDownloadFreshVersionFromGithub := true;

                if isNeedDownloadFreshVersionFromGithub then begin
                  Stream := DownloadFromGithub(IOVersion.Version);

                  if Stream <> nil then begin
                    try
                      if TFile.Exists(IncludeTrailingPathDelimiter(GetWorkingDir) + 'storagenode.exe') then
                        TFile.Delete(IncludeTrailingPathDelimiter(GetWorkingDir) + 'storagenode.exe');

                      UnzipStoragenode(Stream);
                    finally
                      Stream.Free;
                    end;
                  end;
                end;

                UpdatingData.FreshVersionDir := GetWorkingDir;
                UpdatingData.OldVersionDir := NodeServiceData.Path;
                UpdatingData.OldVersion := NodeServiceData.Version;
                UpdatingData.IsStartService := true;
                UpdatingData.ServiceName := JITem.Value;
                DoUpgradeSelectedNode(UpdatingData);
              end;
            end;
        finally
          JSON.Free;
        end;
      finally
        StrList.Free;
      end;
    finally
      Ver.Free;
    end;
  end;
end;

procedure TWorkerService.DoUpgradeSelectedNode(const aUpdatingData: TUpdatingData);
var
  Counter: Integer;
  SM: TServiceManager;
begin
  try
    SM := TServiceManager.Create(nil);
    try
      SM.Connect;
      SM.StopService(aUpdatingData.ServiceName);
      Sleep(10000);
      Counter := 0;
      while Counter < 10 do begin
        SM.Refresh;
        LogMessage('Stop service "' + aUpdatingData.ServiceName + '" for update: check ' + Counter.ToString);
        if SM.Services.ServiceByName(aUpdatingData.ServiceName).CurrentState = SERVICE_STOPPED then
          break;
        Sleep(30000);
        Counter := Counter + 1;
      end;
      SM.Refresh;

      if SM.Services.ServiceByName(aUpdatingData.ServiceName).CurrentState = SERVICE_STOPPED then begin
        UpdateNode(aUpdatingData.FreshVersionDir, aUpdatingData.OldVersionDir, aUpdatingData.OldVersion);

        if aUpdatingData.IsStartService then begin
          SM.StartService(aUpdatingData.ServiceName);
          LogMessage('Service "' + aUpdatingData.ServiceName + '" started after update');
        end;
      end;
    finally
      SM.Free;
    end;
  except
    on E: Exception do
      LogMessage(E.Message);
  end;
end;

procedure TWorkerService.StartSelectedNode(AServiceName: String);
var
  SM: TServiceManager;
  ST: TServiceStatus;
begin
  try
    SM := TServiceManager.Create(nil);
    try
      SM.Connect;
      if AServiceName.IsEmpty then
        raise Exception.Create('ServiceName is empty');

      SM.GetServiceStatus(AServiceName, ST);
      if ServiceStateToString(ST.dwCurrentState) = 'Stopped' then begin
        SM.StartService(AServiceName);
        LogMessage('Service "' + AServiceName + '" has started');
      end;
    finally
      SM.Free;
    end;
  except
    on E: Exception do
      LogMessage(E.Message);
  end;
end;

procedure TWorkerService.UnzipStoragenode(aStream: TStream);
var
  Zip: TZipFile;
begin
  Zip := TZipFile.Create;
  try
    Zip.Open(AStream, TZipMode.zmRead);
    var fileNames := Zip.FileNames;
    Zip.Extract('storagenode.exe', GetWorkingDir);
  finally
    Zip.Free;
  end;
end;

end.
