program StorjCopier;

uses
  Vcl.Forms,
  ufrmMain in 'ufrmMain.pas' {frmMain},
  CopierThread in 'CopierThread.pas';

{$R *.res}

begin
  {$IFDEF DEBUG}
    ReportMemoryLeaksOnShutdown := True;
  {$ENDIF}

  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
