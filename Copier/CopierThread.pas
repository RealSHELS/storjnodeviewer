unit CopierThread;

interface

uses
  System.Classes,
  System.SysUtils,
  System.IOUtils,
  System.Diagnostics;

type
  ICopierThreadCallbackReciever = interface
    procedure OnProgress(const aCopiedCount: Integer);
    procedure OnComplete(const aCopiedCount: Integer);
    procedure OnStop(const aCopiedCount: Integer);
    procedure OnLog(const aText: String; const aIsError: Boolean);
  end;

  TCopierThreadSettings = record
    FolderSource: String;
    FolderDest: String;
    CallbackReceiver: ICopierThreadCallbackReciever;
  end;

  TStorjCopierThread = class(TThread)
  private
    fSettings: TCopierThreadSettings;

    procedure CallLog(const aText: String; const aIsError: Boolean);
    procedure CallComplete(const aCount: Integer);
    procedure CallProgress(const aCount: Integer);
    procedure CallStop(const aCount: Integer);

    function GetAllDirectories(const aPath: String): TArray<String>;
  protected
    procedure Execute; override;
  public
    constructor Create(aSettings: TCopierThreadSettings); reintroduce;
  end;

implementation

{ TStorjCopierThread }

procedure TStorjCopierThread.CallComplete(const aCount: Integer);
begin
  if Assigned(fSettings.CallbackReceiver) then
    TThread.Synchronize(nil,
      procedure
      begin
        fSettings.CallbackReceiver.OnComplete(aCount);
      end);
end;

procedure TStorjCopierThread.CallLog(const aText: String; const aIsError: Boolean);
begin
  if Assigned(fSettings.CallbackReceiver) then
    TThread.Synchronize(nil,
      procedure
      begin
        fSettings.CallbackReceiver.OnLog(aText, aIsError);
      end);
end;

procedure TStorjCopierThread.CallProgress(const aCount: Integer);
begin
   if Assigned(fSettings.CallbackReceiver) then
    TThread.Synchronize(nil,
      procedure
      begin
        fSettings.CallbackReceiver.OnProgress(aCount);
      end);
end;

procedure TStorjCopierThread.CallStop(const aCount: Integer);
begin
  if Assigned(fSettings.CallbackReceiver) then
    TThread.Synchronize(nil,
      procedure
      begin
        fSettings.CallbackReceiver.OnStop(aCount);
      end);
end;

constructor TStorjCopierThread.Create(aSettings: TCopierThreadSettings);
begin
  fSettings := aSettings;

  Self.FreeOnTerminate := True;

  inherited Create(False);
end;

procedure TStorjCopierThread.Execute;
var
  Count: Integer;
  SourcePath, DestPath: String;
  SourcePathArr, DestPathArr, SourceFileArr, DestFileArr: TArray<String>;
  SourceFullPath, DestFullPath, RelativePath: String;
  SourceFullFileName: String;
  IsNeedCopy: Boolean;
  SourceFileSize, DestFileSize: Int64;
  SourceLastWriteTime, DestLastWriteTime: TDateTime;
  FileName: String;
begin
  Count := 0;
  SourcePath := IncludeTrailingPathDelimiter(fSettings.FolderSource);
  DestPath := IncludeTrailingPathDelimiter(fSettings.FolderDest);

  if not DirectoryExists(SourcePath) then begin
    CallLog('Path "' + SourcePath + '" is not exists', True);
    Exit;
  end;

//  if not DirectoryExists(DestPath) then begin
//    CallLog('Path "' + DestPath + '" is not exists', True);
//    Exit;
//  end;

  var SW := TStopWatch.StartNew;
  SourcePathArr := [SourcePath] + Self.GetAllDirectories(SourcePath);
  SW.Stop;
  CallLog('Self.GetAllDirectories: ' + SW.ElapsedMilliseconds.ToString + 'ms, items count: ' + Length(SourcePathArr).ToString, False);

  CallLog('Start loading directories', False);
  SW := TStopWatch.StartNew;
  SourcePathArr := [SourcePath] + TDirectory.GetDirectories(SourcePath, '*', TSearchOption.soAllDirectories);
  SW.Stop;
  CallLog('TDirectory.GetDirectories: ' + SW.ElapsedMilliseconds.ToString + 'ms, items count: ' + Length(SourcePathArr).ToString, False);

//  CallLog('Start copying files', False);
//  for SourceFullPath in SourcePathArr do begin
//    RelativePath := IncludeTrailingPathDelimiter(SourceFullPath).Replace(SourcePath, '');
//    DestFullPath := DestPath + RelativePath;
//
//    //copy source path files to dest
//    SourceFileArr := TDirectory.GetFiles(SourceFullPath);
//    for SourceFullFileName in SourceFileArr do begin
//      FileName := ExtractFileName(SourceFullFileName);
//      IsNeedCopy := False;
//      if not TFile.Exists(DestFullPath + FileName) then
//        IsNeedCopy := True;
//
//      if not IsNeedCopy then begin
//        SourceFileSize := TFile.GetSize(SourceFullFileName);
//        SourceLastWriteTime := TFile.GetLastWriteTimeUtc(SourceFullFileName);
//
//        DestFileSize := TFile.GetSize(DestFullPath + FileName);
//        DestLastWriteTime := TFile.GetLastWriteTimeUtc(DestFullPath + FileName);
//
//        IsNeedCopy := (SourceFileSize <> DestFileSize) or (SourceLastWriteTime <> DestLastWriteTime);
//      end;
//
//      if IsNeedCopy then begin
//        try
//          if not TDirectory.Exists(DestFullPath) then
//            TDirectory.CreateDirectory(DestFullPath);
//
//          TFile.Copy(SourceFullFileName, DestFullPath + FileName, true);
//          Count := Count + 1;
//        except
//          on E: Exception do
//            CallLog('Error on copying file "' + SourceFullFileName + '": ' + E.Message, True);
//        end;
//      end;
//
//      if Terminated then begin
//        CallStop(Count);
//        Exit;
//      end;
//
//      if Count mod 10 = 0 then
//        CallProgress(Count);
//    end;
//
//    //delete dest path files that not exists on source path
//    if TDirectory.Exists(DestFullPath) then begin
//      DestFileArr := TDirectory.GetFiles(DestFullPath);
//      for var i := 0 to Length(DestFileArr) - 1 do begin
//        var IsNeedDelete := True;
//        for var j := 0 to Length(SourceFileArr) - 1 do
//          if ExtractFileName(DestFileArr[i]) = ExtractFileName(SourceFileArr[j]) then begin
//            IsNeedDelete := False;
//            break;
//          end;
//
//        if IsNeedDelete then
//          TFile.Delete(DestFileArr[i]);
//      end;
//    end;
//  end;
//
//  //delete folders that not exists in Source
//  CallLog('Start delete directories', False);
//  DestPathArr := TDirectory.GetDirectories(DestPath, '*', TSearchOption.soAllDirectories);
//  for var i := 0 to Length(DestPathArr) - 1 do begin
//    var IsNeedDelete := True;
//    for var j := 0 to Length(SourcePathArr) - 1 do begin
//      var SourceRelativePath := IncludeTrailingPathDelimiter(SourcePathArr[j]).Replace(SourcePath, '');
//      var DestRelativePath := IncludeTrailingPathDelimiter(DestPathArr[i]).Replace(DestPath, '');
//
//      if SourceRelativePath = DestRelativePath then begin
//        IsNeedDelete := False;
//        break;
//      end;
//    end;
//
//    if IsNeedDelete then
//      TDirectory.Delete(DestPathArr[i], true);
//  end;

  CallLog('Finished', False);
  CallComplete(Count);
end;

function TStorjCopierThread.GetAllDirectories(const aPath: String): TArray<String>;
var
  SR: TSearchRec;
begin
  Result := [];
  var IsFound := FindFirst(aPath + '*.*', faAnyFile, SR) = 0;
  while IsFound do begin
    if ((SR.Attr and faDirectory) <> 0) and (SR.Name[1] <> '.') then begin
      Result := Result + [aPath + SR.Name];
      Result := Result + GetAllDirectories(aPath + SR.Name + '\');
    end;
    IsFound := FindNext(SR) = 0;
  end;
  FindClose(SR);
end;

end.
