unit ufrmMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBasic, dxSkinOffice2013LightGray,
  dxSkinOffice2019Black, dxSkinOffice2019DarkGray, dxSkinOffice2019White,
  dxSkinWhiteprint, dxSkinWXI, cxClasses, dxLayoutContainer, dxLayoutControl,
  dxLayoutcxEditAdapters, cxContainer, cxEdit, dxLayoutLookAndFeels, cxMemo,
  cxTextEdit, cxMaskEdit, cxButtonEdit, dxLayoutControlAdapters, Vcl.Menus,
  Vcl.StdCtrls, cxButtons, dxStatusBar, Vcl.ExtCtrls, CopierThread;

type
  TfrmMain = class(TForm, ICopierThreadCallbackReciever)
    dxLayoutControl1Group_Root: TdxLayoutGroup;
    dxLayoutControl1: TdxLayoutControl;
    edtFolderSource: TcxButtonEdit;
    layedtFolderSource: TdxLayoutItem;
    edtFolderDest: TcxButtonEdit;
    layedtFolderDest: TdxLayoutItem;
    mmoLog: TcxMemo;
    dxLayoutItem2: TdxLayoutItem;
    dxLayoutLookAndFeelList: TdxLayoutLookAndFeelList;
    dxLayoutSkinLookAndFeel1: TdxLayoutSkinLookAndFeel;
    btnStart: TcxButton;
    dxLayoutItem4: TdxLayoutItem;
    OpenDialog: TFileOpenDialog;
    StatusBar: TdxStatusBar;
    layStatusBar: TdxLayoutItem;
    Timer: TTimer;
    procedure edtFolderSourcePropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure btnStartClick(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
  private
    fCopierThread: TStorjCopierThread;
    fStartedAt: TDateTime;
  public
    procedure OnProgress(const aCopiedCount: Integer);
    procedure OnComplete(const aCopiedCount: Integer);
    procedure OnStop(const aCopiedCount: Integer);
    procedure OnLog(const aText: String; const aIsError: Boolean);
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

uses
  System.IOUtils;

procedure TfrmMain.btnStartClick(Sender: TObject);
var
  Settings: TCopierThreadSettings;
begin
  if btnStart.Caption = 'Start' then begin
    Settings.FolderSource := edtFolderSource.Text;
    Settings.FolderDest := edtFolderDest.Text;
    Settings.CallbackReceiver := Self;

    OnLog('Copying started', False);
    btnStart.Caption := 'Stop';
    fStartedAt := Now;
    fCopierThread := TStorjCopierThread.Create(Settings);
  end else if btnStart.Caption = 'Stop' then begin
    btnStart.Caption := 'Start';
    OnLog('Copying stoping', False);
    fCopierThread.Terminate;
  end;
end;

procedure TfrmMain.edtFolderSourcePropertiesButtonClick(Sender: TObject; AButtonIndex: Integer);
begin
  if OpenDialog.Execute then
    TcxButtonEdit(Sender).Text := OpenDialog.FileName;
end;

procedure TfrmMain.OnComplete(const aCopiedCount: Integer);
begin
  ShowMessage('Complete. File count: ' + aCopiedCount.ToString);
  btnStart.Caption := 'Start';
end;

procedure TfrmMain.OnLog(const aText: String; const aIsError: Boolean);
var
  Inner: String;
begin
  if aIsError then
    Inner := ' Error: '
  else
    Inner := ' ';

  mmoLog.Lines.Add(DateTimeToStr(Now) + Inner + aText);
end;

procedure TfrmMain.OnProgress(const aCopiedCount: Integer);
begin
  StatusBar.Panels[0].Text := 'Copied count: ' + aCopiedCount.ToString;
end;

procedure TfrmMain.OnStop(const aCopiedCount: Integer);
begin
  OnLog('Copying stopped at count: ' + aCopiedCount.ToString, False);
end;

procedure TfrmMain.TimerTimer(Sender: TObject);
begin
  if btnStart.Caption = 'Stop' then
    StatusBar.Panels[1].Text := 'Running: ' + FormatDateTime('hh:nn:ss', Now - fStartedAt);
end;

end.
