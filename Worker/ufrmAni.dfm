object frmAni: TfrmAni
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'SNO Helper'
  ClientHeight = 83
  ClientWidth = 566
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxLayoutControl: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 566
    Height = 83
    Align = alClient
    TabOrder = 0
    object dxActivityIndicator: TdxActivityIndicator
      Left = 10
      Top = 55
      Width = 546
      Height = 18
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'iMaginary'
      PropertiesClassName = 'TdxActivityIndicatorHorizontalDotsProperties'
      Transparent = True
    end
    object lblText: TcxLabel
      Left = 10
      Top = 10
      AutoSize = False
      Caption = 'Text'
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -19
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.HotTrack = False
      Style.IsFontAssigned = True
      Properties.Alignment.Horz = taCenter
      Properties.Alignment.Vert = taVCenter
      Transparent = True
      Height = 39
      Width = 546
      AnchorX = 283
      AnchorY = 30
    end
    object dxLayoutControlGroup_Root: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avClient
      ButtonOptions.Buttons = <>
      Hidden = True
      ShowBorder = False
      Index = -1
    end
    object layActivitiIndicator: TdxLayoutItem
      Parent = dxLayoutControlGroup_Root
      CaptionOptions.Text = 'dxActivityIndicator1'
      CaptionOptions.Visible = False
      Control = dxActivityIndicator
      ControlOptions.OriginalHeight = 18
      ControlOptions.OriginalWidth = 300
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object laylblText: TdxLayoutItem
      Parent = dxLayoutControlGroup_Root
      CaptionOptions.Text = 'cxLabel1'
      CaptionOptions.Visible = False
      Control = lblText
      ControlOptions.OriginalHeight = 39
      ControlOptions.OriginalWidth = 46
      ControlOptions.ShowBorder = False
      Index = 0
    end
  end
end
