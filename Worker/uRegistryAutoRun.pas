unit uRegistryAutoRun;

interface

uses
  WinAPI.Windows,
  System.Win.Registry,

  ComObj, ActiveX, Variants, SysUtils;

  procedure RunOnStartup(const aProgTitle, aCmdLine: string);
  procedure RemoveOnStartup(const aProgTitle: string);
  function CheckOnStartup(const aProgTitle: String): Boolean;

  procedure RunOnStartupAsTask(const aProgTitle, aCmdLine: string);

implementation

procedure RunOnStartupAsTask(const aProgTitle, aCmdLine: string);
var
  Service: OleVariant;
  RootFolder: OleVariant;
  TaskDef: OleVariant;
  LogonTrigger: OleVariant;
  ExecAction: OleVariant;
  Principal: OleVariant;
  TaskFolder: OleVariant;
  User: OleVariant;
begin
  // ����������� COM ��������
  CoInitialize(nil);
  try
    // ��������� ��������� Task Scheduler
    Service := CreateOleObject('Schedule.Service');
    Service.Connect('', '', '', '');

    // �������� �������� ����� �������
    RootFolder := Service.GetFolder('\');

    // ��������� �������� � ����� ����� ��'��, ���� ���� ����
    try
      RootFolder.DeleteTask(aProgTitle, 0);
    except
      // ���� �������� �� ��������, ���������� �������
    end;

    // ��������� ���� ��������
    TaskDef := Service.NewTask(0);

    // ����������� ���������� ��� ��������� ��������
    TaskDef.RegistrationInfo.Description := 'Auto-start ' + aProgTitle;
    TaskDef.RegistrationInfo.Author := 'RealSHELS';

    // ����������� ������ ��� ������� ��� ���� � �������
    LogonTrigger := TaskDef.Triggers.Create(1); // 1 ������ TASK_TRIGGER_LOGON
    LogonTrigger.StartBoundary := FormatDateTime('yyyy-mm-dd"T"hh:nn:ss', Now);

    // ����������� �� ��� ������� ��������
    ExecAction := TaskDef.Actions.Create(0); // 0 ������ TASK_ACTION_EXEC
    ExecAction.Path := aCmdLine;

    // ����������� ����� �������������
    Principal := TaskDef.Principal;
    Principal.UserId := 'NT AUTHORITY\SYSTEM'; // ������������� ����� ����� ����������� SYSTEM
    Principal.LogonType := 3; // TASK_LOGON_INTERACTIVE_TOKEN
    Principal.RunLevel := 1; // TASK_RUNLEVEL_HIGHEST

    // �������� ��������
    TaskFolder := RootFolder.RegisterTaskDefinition(aProgTitle, TaskDef, 6, '', '', 3, ''); // 6 ������ TASK_CREATE_OR_UPDATE, 3 ������ TASK_LOGON_INTERACTIVE_TOKEN
  finally
    // ������������ COM ��������
    CoUninitialize;
  end;
end;

procedure RunOnStartup(const aProgTitle, aCmdLine: string);
var
  Reg: TRegistry;
begin
  Reg := TRegistry.Create;
  try
    ////Reg.RootKey := HKEY_LOCAL_MACHINE;
    if Reg.OpenKey('Software\Microsoft\Windows\CurrentVersion\Run', True) then begin
      Reg.WriteString(aProgTitle, aCmdLine);
      Reg.CloseKey;
    end;
  finally
    Reg.Free;
  end;
end;

procedure RemoveOnStartup(const aProgTitle: string);
var
  Reg : TRegistry;
begin
  Reg := TRegistry.Create;
  try
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    if Reg.OpenKey('Software\Microsoft\Windows\CurrentVersion\Run', false) then
      Reg.DeleteKey(aProgTitle);
  finally
    Reg.Free;
  end;
end;

function CheckOnStartup(const aProgTitle: String): Boolean;
var
  Reg : TRegistry;
begin
  Reg := TRegistry.Create;
  try
    //Reg.RootKey := HKEY_LOCAL_MACHINE;
    Reg.OpenKey('Software\Microsoft\Windows\CurrentVersion\Run', false);
    Result := Reg.ValueExists(aProgTitle);
  finally
    Reg.Free;
  end;
end;

end.
