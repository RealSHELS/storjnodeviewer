object frmNewNodeInstaller: TfrmNewNodeInstaller
  Left = 0
  Top = 0
  Caption = 'New node installer'
  ClientHeight = 191
  ClientWidth = 400
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OnShow = FormShow
  TextHeight = 13
  object CardPanel: TCardPanel
    Left = 0
    Top = 0
    Width = 400
    Height = 191
    Align = alClient
    ActiveCard = cardNewFullNode
    BevelOuter = bvNone
    Caption = 'CardPanel'
    TabOrder = 0
    ExplicitWidth = 408
    ExplicitHeight = 192
    object cardFirst: TCard
      Left = 0
      Top = 0
      Width = 404
      Height = 192
      Caption = 'cardFirst'
      CardIndex = 0
      TabOrder = 0
      object GridPanelCardFirst: TGridPanel
        Left = 0
        Top = 0
        Width = 404
        Height = 192
        Align = alClient
        ColumnCollection = <
          item
            Value = 100.000000000000000000
          end>
        ControlCollection = <
          item
            Column = 0
            Control = btnCardFirstNewEmptyNode
            Row = 0
          end
          item
            Column = 0
            Control = btnCardFirstNewFullNode
            Row = 1
          end
          item
            Column = 0
            Control = btnCardFirstNewNodeOldService
            Row = 2
          end>
        RowCollection = <
          item
            Value = 33.333070111006360000
          end
          item
            Value = 33.333279865015780000
          end
          item
            Value = 33.333650023977860000
          end>
        TabOrder = 0
        object btnCardFirstNewEmptyNode: TcxButton
          AlignWithMargins = True
          Left = 11
          Top = 11
          Width = 382
          Height = 48
          Margins.Left = 10
          Margins.Top = 10
          Margins.Right = 10
          Margins.Bottom = 5
          Align = alClient
          Caption = 'New empty node on this PC'
          LookAndFeel.NativeStyle = False
          LookAndFeel.SkinName = 'iMaginary'
          TabOrder = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = btnCardFirstNewEmptyNodeClick
        end
        object btnCardFirstNewFullNode: TcxButton
          AlignWithMargins = True
          Left = 11
          Top = 69
          Width = 382
          Height = 54
          Margins.Left = 10
          Margins.Top = 5
          Margins.Right = 10
          Margins.Bottom = 5
          Align = alClient
          Caption = 'New full node on this PC'
          LookAndFeel.NativeStyle = False
          LookAndFeel.SkinName = 'iMaginary'
          TabOrder = 1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = btnCardFirstNewFullNodeClick
        end
        object btnCardFirstNewNodeOldService: TcxButton
          AlignWithMargins = True
          Left = 11
          Top = 133
          Width = 382
          Height = 48
          Margins.Left = 10
          Margins.Top = 5
          Margins.Right = 10
          Margins.Bottom = 10
          Align = alClient
          Caption = 'New node on old service'
          LookAndFeel.NativeStyle = False
          LookAndFeel.SkinName = 'iMaginary'
          TabOrder = 2
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = btnCardFirstNewNodeOldServiceClick
        end
      end
    end
    object cardNewNodeOnOldService: TCard
      Left = 0
      Top = 0
      Width = 404
      Height = 192
      Caption = 'cardNewNodeOnOldService'
      CardIndex = 1
      TabOrder = 1
      ExplicitWidth = 408
      object dxLayoutControlNewNodeOldService: TdxLayoutControl
        Left = 0
        Top = 0
        Width = 404
        Height = 192
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 408
        object lblNewNodeOldServiceText: TcxLabel
          Left = 10
          Top = 10
          AutoSize = False
          Caption = 
            'Prepare config in directory and select at which node need create' +
            ' new node structure'
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -15
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.HotTrack = False
          Style.IsFontAssigned = True
          Properties.Alignment.Horz = taCenter
          Properties.Alignment.Vert = taVCenter
          Properties.WordWrap = True
          Transparent = True
          Height = 45
          Width = 384
          AnchorX = 202
          AnchorY = 33
        end
        object btnNewNodeOldServicePrev: TcxButton
          Left = 124
          Top = 157
          Width = 75
          Height = 25
          Caption = 'Cancel'
          LookAndFeel.NativeStyle = False
          LookAndFeel.SkinName = 'iMaginary'
          OptionsImage.Glyph.SourceDPI = 96
          OptionsImage.Glyph.SourceHeight = 16
          OptionsImage.Glyph.SourceWidth = 16
          OptionsImage.Glyph.Data = {
            3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D225554
            462D38223F3E0D0A3C7376672076657273696F6E3D22312E31222069643D224C
            617965725F312220786D6C6E733D22687474703A2F2F7777772E77332E6F7267
            2F323030302F7376672220786D6C6E733A786C696E6B3D22687474703A2F2F77
            77772E77332E6F72672F313939392F786C696E6B2220783D223070782220793D
            22307078222076696577426F783D2230203020333220333222207374796C653D
            22656E61626C652D6261636B67726F756E643A6E657720302030203332203332
            3B2220786D6C3A73706163653D227072657365727665223E262331333B262331
            303B3C7374796C6520747970653D22746578742F6373732220786D6C3A737061
            63653D227072657365727665223E2E426C75657B66696C6C3A23313137374437
            3B7D262331333B262331303B2623393B2E59656C6C6F777B66696C6C3A234646
            423131353B7D262331333B262331303B2623393B2E426C61636B7B66696C6C3A
            233732373237323B7D262331333B262331303B2623393B2E477265656E7B6669
            6C6C3A233033394332333B7D262331333B262331303B2623393B2E5265647B66
            696C6C3A234431314331433B7D262331333B262331303B2623393B2E7374307B
            6F7061636974793A302E37353B7D262331333B262331303B2623393B2E737431
            7B6F7061636974793A302E353B7D3C2F7374796C653E0D0A3C672069643D2250
            726576696F7573223E0D0A09093C706F6C79676F6E20636C6173733D22426C75
            652220706F696E74733D2232362C342032362C323820362C3136202623393B22
            2F3E0D0A093C2F673E0D0A3C2F7376673E0D0A}
          TabOrder = 7
          OnClick = btnNewNodeOldServicePrevClick
        end
        object btnNewNodeOldServiceFinish: TcxButton
          Left = 205
          Top = 157
          Width = 75
          Height = 25
          Caption = 'Install'
          LookAndFeel.NativeStyle = False
          LookAndFeel.SkinName = 'iMaginary'
          OptionsImage.Glyph.SourceDPI = 96
          OptionsImage.Glyph.SourceHeight = 16
          OptionsImage.Glyph.SourceWidth = 16
          OptionsImage.Glyph.Data = {
            3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D225554
            462D38223F3E0D0A3C7376672076657273696F6E3D22312E31222069643D224C
            617965725F312220786D6C6E733D22687474703A2F2F7777772E77332E6F7267
            2F323030302F7376672220786D6C6E733A786C696E6B3D22687474703A2F2F77
            77772E77332E6F72672F313939392F786C696E6B2220783D223070782220793D
            22307078222076696577426F783D2230203020333220333222207374796C653D
            22656E61626C652D6261636B67726F756E643A6E657720302030203332203332
            3B2220786D6C3A73706163653D227072657365727665223E262331333B262331
            303B3C7374796C6520747970653D22746578742F6373732220786D6C3A737061
            63653D227072657365727665223E2E426C75657B66696C6C3A23313137374437
            3B7D262331333B262331303B2623393B2E59656C6C6F777B66696C6C3A234646
            423131353B7D262331333B262331303B2623393B2E426C61636B7B66696C6C3A
            233732373237323B7D262331333B262331303B2623393B2E477265656E7B6669
            6C6C3A233033394332333B7D262331333B262331303B2623393B2E5265647B66
            696C6C3A234431314331433B7D262331333B262331303B2623393B2E7374307B
            6F7061636974793A302E37353B7D262331333B262331303B2623393B2E737431
            7B6F7061636974793A302E353B7D3C2F7374796C653E0D0A3C672069643D224E
            657874223E0D0A09093C706F6C79676F6E20636C6173733D22426C7565222070
            6F696E74733D22362C3420362C32382032362C3136202623393B222F3E0D0A09
            3C2F673E0D0A3C2F7376673E0D0A}
          OptionsImage.Layout = blGlyphRight
          TabOrder = 8
          OnClick = btnNewNodeOldServiceFinishClick
        end
        object btnNewNodeOldServiceRefresh: TcxButton
          Left = 371
          Top = 62
          Width = 23
          Height = 25
          Caption = 'btnNewNodeOldServiceRefresh'
          LookAndFeel.NativeStyle = False
          LookAndFeel.SkinName = 'iMaginary'
          OptionsImage.Glyph.SourceDPI = 96
          OptionsImage.Glyph.SourceHeight = 16
          OptionsImage.Glyph.SourceWidth = 16
          OptionsImage.Glyph.Data = {
            3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D225554
            462D38223F3E0D0A3C7376672076657273696F6E3D22312E31222069643D224C
            617965725F312220786D6C6E733D22687474703A2F2F7777772E77332E6F7267
            2F323030302F7376672220786D6C6E733A786C696E6B3D22687474703A2F2F77
            77772E77332E6F72672F313939392F786C696E6B2220783D223070782220793D
            22307078222076696577426F783D2230203020333220333222207374796C653D
            22656E61626C652D6261636B67726F756E643A6E657720302030203332203332
            3B2220786D6C3A73706163653D227072657365727665223E262331333B262331
            303B3C7374796C6520747970653D22746578742F6373732220786D6C3A737061
            63653D227072657365727665223E2E426C75657B66696C6C3A23313137374437
            3B7D262331333B262331303B2623393B2E59656C6C6F777B66696C6C3A234646
            423131353B7D262331333B262331303B2623393B2E426C61636B7B66696C6C3A
            233732373237323B7D262331333B262331303B2623393B2E477265656E7B6669
            6C6C3A233033394332333B7D262331333B262331303B2623393B2E5265647B66
            696C6C3A234431314331433B7D262331333B262331303B2623393B2E7374307B
            6F7061636974793A302E37353B7D262331333B262331303B2623393B2E737431
            7B6F7061636974793A302E353B7D3C2F7374796C653E0D0A3C672069643D2252
            6566726573685F315F223E0D0A09093C7061746820636C6173733D2247726565
            6E2220643D224D32342E352C372E354332322E332C352E332C31392E332C342C
            31362C344331302E312C342C352E312C382E332C342E322C313468342E316330
            2E392D332E342C342D362C372E372D3663322E322C302C342E322C302E392C35
            2E362C322E344C31382C313420202623393B2623393B68352E3768342E314832
            3856344C32342E352C372E357A222F3E0D0A09093C7061746820636C6173733D
            22477265656E2220643D224D31362E322C3234632D322E322C302D342E322D30
            2E392D352E362D322E346C332E362D332E3648382E3448342E3448342E327631
            306C332E352D332E3563322E322C322E322C352E322C332E352C382E352C332E
            3520202623393B2623393B4332322E312C32382C32372C32332E372C32382C31
            38682D342E314332332C32312E342C31392E392C32342C31362E322C32347A22
            2F3E0D0A093C2F673E0D0A3C2F7376673E0D0A}
          PaintStyle = bpsGlyph
          TabOrder = 2
          OnClick = btnNewNodeOldServiceRefreshClick
        end
        object lookNewNodeOldServiceNodeSelect: TcxLookupComboBox
          Left = 75
          Top = 64
          Properties.KeyFieldNames = 'ServiceName'
          Properties.ListColumns = <
            item
              Caption = 'Servicename'
              FieldName = 'ServiceName'
            end
            item
              Caption = 'Storage path'
              FieldName = 'StoragePath'
            end>
          Style.BorderColor = clWindowFrame
          Style.BorderStyle = ebs3D
          Style.HotTrack = False
          Style.ButtonStyle = bts3D
          Style.PopupBorderStyle = epbsFrame3D
          TabOrder = 1
          Width = 290
        end
        object edtStoragepath: TcxTextEdit
          Left = 75
          Top = 95
          Style.BorderColor = clWindowFrame
          Style.BorderStyle = ebs3D
          Style.HotTrack = False
          Style.TransparentBorder = False
          TabOrder = 3
          TextHint = 'D:\Storj\Data'
          Width = 288
        end
        object edtKeypath: TcxTextEdit
          Left = 75
          Top = 126
          Style.BorderColor = clWindowFrame
          Style.BorderStyle = ebs3D
          Style.HotTrack = False
          Style.TransparentBorder = False
          TabOrder = 5
          TextHint = 'D:\Storj\Keys'
          Width = 288
        end
        object btnSelectStoragepath: TcxButton
          Left = 369
          Top = 95
          Width = 25
          Height = 25
          Caption = 'btnSelectStoragepath'
          LookAndFeel.NativeStyle = False
          LookAndFeel.SkinName = 'iMaginary'
          OptionsImage.Glyph.SourceDPI = 96
          OptionsImage.Glyph.SourceHeight = 16
          OptionsImage.Glyph.SourceWidth = 16
          OptionsImage.Glyph.Data = {
            3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D225554
            462D38223F3E0D0A3C7376672076657273696F6E3D22312E31222069643D224C
            617965725F312220786D6C6E733D22687474703A2F2F7777772E77332E6F7267
            2F323030302F7376672220786D6C6E733A786C696E6B3D22687474703A2F2F77
            77772E77332E6F72672F313939392F786C696E6B2220783D223070782220793D
            22307078222076696577426F783D2230203020333220333222207374796C653D
            22656E61626C652D6261636B67726F756E643A6E657720302030203332203332
            3B2220786D6C3A73706163653D227072657365727665223E262331333B262331
            303B3C7374796C6520747970653D22746578742F6373732220786D6C3A737061
            63653D227072657365727665223E2E426C75657B66696C6C3A23313137374437
            3B7D262331333B262331303B2623393B2E59656C6C6F777B66696C6C3A234646
            423131353B7D262331333B262331303B2623393B2E426C61636B7B66696C6C3A
            233732373237323B7D262331333B262331303B2623393B2E477265656E7B6669
            6C6C3A233033394332333B7D262331333B262331303B2623393B2E5265647B66
            696C6C3A234431314331433B7D262331333B262331303B2623393B2E7374307B
            6F7061636974793A302E37353B7D262331333B262331303B2623393B2E737431
            7B6F7061636974793A302E353B7D3C2F7374796C653E0D0A3C672069643D2246
            6F6C6465724F70656E223E0D0A09093C6720636C6173733D22737430223E0D0A
            0909093C7061746820636C6173733D2259656C6C6F772220643D224D322E322C
            32352E326C352E352D313063302E332D302E372C312D312E322C312E382D312E
            32483234762D3363302D302E362D302E342D312D312D31483132563763302D30
            2E362D302E342D312D312D31483343322E342C362C322C362E352C322C377631
            3820202623393B2623393B2623393B63302C302E322C302C302E332C302E312C
            302E3443322E312C32352E342C322E322C32352E332C322E322C32352E327A22
            2F3E0D0A09093C2F673E0D0A09093C7061746820636C6173733D2259656C6C6F
            772220643D224D32392E332C313648392E364C342C32366831392E3863302E35
            2C302C312E312D302E322C312E332D302E366C342E392D382E394333302E312C
            31362E322C32392E382C31362C32392E332C31367A222F3E0D0A093C2F673E0D
            0A3C2F7376673E0D0A}
          PaintStyle = bpsGlyph
          TabOrder = 4
          OnClick = btnSelectStoragepathClick
        end
        object btnSelectKeypath: TcxButton
          Left = 369
          Top = 126
          Width = 25
          Height = 25
          Caption = 'btnSelectKeypath'
          LookAndFeel.NativeStyle = False
          LookAndFeel.SkinName = 'iMaginary'
          OptionsImage.Glyph.SourceDPI = 96
          OptionsImage.Glyph.SourceHeight = 16
          OptionsImage.Glyph.SourceWidth = 16
          OptionsImage.Glyph.Data = {
            3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D225554
            462D38223F3E0D0A3C7376672076657273696F6E3D22312E31222069643D224C
            617965725F312220786D6C6E733D22687474703A2F2F7777772E77332E6F7267
            2F323030302F7376672220786D6C6E733A786C696E6B3D22687474703A2F2F77
            77772E77332E6F72672F313939392F786C696E6B2220783D223070782220793D
            22307078222076696577426F783D2230203020333220333222207374796C653D
            22656E61626C652D6261636B67726F756E643A6E657720302030203332203332
            3B2220786D6C3A73706163653D227072657365727665223E262331333B262331
            303B3C7374796C6520747970653D22746578742F6373732220786D6C3A737061
            63653D227072657365727665223E2E426C75657B66696C6C3A23313137374437
            3B7D262331333B262331303B2623393B2E59656C6C6F777B66696C6C3A234646
            423131353B7D262331333B262331303B2623393B2E426C61636B7B66696C6C3A
            233732373237323B7D262331333B262331303B2623393B2E477265656E7B6669
            6C6C3A233033394332333B7D262331333B262331303B2623393B2E5265647B66
            696C6C3A234431314331433B7D262331333B262331303B2623393B2E7374307B
            6F7061636974793A302E37353B7D262331333B262331303B2623393B2E737431
            7B6F7061636974793A302E353B7D3C2F7374796C653E0D0A3C672069643D2246
            6F6C6465724F70656E223E0D0A09093C6720636C6173733D22737430223E0D0A
            0909093C7061746820636C6173733D2259656C6C6F772220643D224D322E322C
            32352E326C352E352D313063302E332D302E372C312D312E322C312E382D312E
            32483234762D3363302D302E362D302E342D312D312D31483132563763302D30
            2E362D302E342D312D312D31483343322E342C362C322C362E352C322C377631
            3820202623393B2623393B2623393B63302C302E322C302C302E332C302E312C
            302E3443322E312C32352E342C322E322C32352E332C322E322C32352E327A22
            2F3E0D0A09093C2F673E0D0A09093C7061746820636C6173733D2259656C6C6F
            772220643D224D32392E332C313648392E364C342C32366831392E3863302E35
            2C302C312E312D302E322C312E332D302E366C342E392D382E394333302E312C
            31362E322C32392E382C31362C32392E332C31367A222F3E0D0A093C2F673E0D
            0A3C2F7376673E0D0A}
          PaintStyle = bpsGlyph
          TabOrder = 6
          OnClick = btnSelectKeypathClick
        end
        object dxLayoutControlNewNodeOldServiceGroup_Root: TdxLayoutGroup
          AlignHorz = ahClient
          AlignVert = avClient
          Hidden = True
          ItemIndex = 3
          ShowBorder = False
          Index = -1
        end
        object laylblNewNodeOldServiceText: TdxLayoutItem
          Parent = dxLayoutControlNewNodeOldServiceGroup_Root
          AlignVert = avClient
          CaptionOptions.Text = 'cxLabel1'
          CaptionOptions.Visible = False
          Control = lblNewNodeOldServiceText
          ControlOptions.OriginalHeight = 40
          ControlOptions.OriginalWidth = 46
          ControlOptions.ShowBorder = False
          Index = 0
        end
        object laybtnNewNodeOldServicePrev: TdxLayoutItem
          Parent = laygrpNewNodeOldServiceButtons
          CaptionOptions.Text = 'cxButton1'
          CaptionOptions.Visible = False
          Control = btnNewNodeOldServicePrev
          ControlOptions.OriginalHeight = 25
          ControlOptions.OriginalWidth = 75
          ControlOptions.ShowBorder = False
          Index = 0
        end
        object laybtnNewNodeOldServiceFinish: TdxLayoutItem
          Parent = laygrpNewNodeOldServiceButtons
          CaptionOptions.Text = 'cxButton2'
          CaptionOptions.Visible = False
          Control = btnNewNodeOldServiceFinish
          ControlOptions.OriginalHeight = 25
          ControlOptions.OriginalWidth = 75
          ControlOptions.ShowBorder = False
          Index = 1
        end
        object laygrpNewNodeOldServiceButtons: TdxLayoutGroup
          Parent = dxLayoutControlNewNodeOldServiceGroup_Root
          AlignHorz = ahCenter
          AlignVert = avBottom
          CaptionOptions.Text = 'New Group'
          ItemIndex = 1
          LayoutDirection = ldHorizontal
          ShowBorder = False
          Index = 4
        end
        object dxLayoutGroup1: TdxLayoutGroup
          Parent = dxLayoutControlNewNodeOldServiceGroup_Root
          AlignHorz = ahClient
          AlignVert = avClient
          CaptionOptions.Text = 'New Group'
          LayoutDirection = ldHorizontal
          ShowBorder = False
          Index = 1
        end
        object dxLayoutItem1: TdxLayoutItem
          Parent = dxLayoutGroup1
          AlignVert = avCenter
          CaptionOptions.Text = 'cxButton3'
          CaptionOptions.Visible = False
          Control = btnNewNodeOldServiceRefresh
          ControlOptions.OriginalHeight = 25
          ControlOptions.OriginalWidth = 23
          ControlOptions.ShowBorder = False
          Index = 1
        end
        object dxLayoutItem7: TdxLayoutItem
          Parent = dxLayoutGroup1
          AlignHorz = ahClient
          AlignVert = avCenter
          CaptionOptions.Text = 'Select node'
          Control = lookNewNodeOldServiceNodeSelect
          ControlOptions.OriginalHeight = 21
          ControlOptions.OriginalWidth = 145
          ControlOptions.ShowBorder = False
          Index = 0
        end
        object dxLayoutItem5: TdxLayoutItem
          Parent = laygrpStoragePath
          AlignHorz = ahClient
          CaptionOptions.Text = 'Storagepath'
          Control = edtStoragepath
          ControlOptions.OriginalHeight = 21
          ControlOptions.OriginalWidth = 121
          ControlOptions.ShowBorder = False
          Index = 0
        end
        object dxLayoutItem6: TdxLayoutItem
          Parent = laygrpKeysPath
          AlignHorz = ahClient
          CaptionOptions.Text = 'Keypath'
          Control = edtKeypath
          ControlOptions.OriginalHeight = 21
          ControlOptions.OriginalWidth = 121
          ControlOptions.ShowBorder = False
          Index = 0
        end
        object laygrpStoragePath: TdxLayoutGroup
          Parent = dxLayoutControlNewNodeOldServiceGroup_Root
          CaptionOptions.Text = 'New Group'
          LayoutDirection = ldHorizontal
          ShowBorder = False
          Index = 2
        end
        object laygrpKeysPath: TdxLayoutGroup
          Parent = dxLayoutControlNewNodeOldServiceGroup_Root
          CaptionOptions.Text = 'New Group'
          LayoutDirection = ldHorizontal
          ShowBorder = False
          Index = 3
        end
        object dxLayoutItem8: TdxLayoutItem
          Parent = laygrpStoragePath
          CaptionOptions.Text = 'cxButton1'
          CaptionOptions.Visible = False
          Control = btnSelectStoragepath
          ControlOptions.OriginalHeight = 25
          ControlOptions.OriginalWidth = 25
          ControlOptions.ShowBorder = False
          Index = 1
        end
        object dxLayoutItem9: TdxLayoutItem
          Parent = laygrpKeysPath
          CaptionOptions.Text = 'cxButton2'
          CaptionOptions.Visible = False
          Control = btnSelectKeypath
          ControlOptions.OriginalHeight = 25
          ControlOptions.OriginalWidth = 25
          ControlOptions.ShowBorder = False
          Index = 1
        end
      end
    end
    object cardNewEmptyNode: TCard
      Left = 0
      Top = 0
      Width = 400
      Height = 191
      Caption = 'cardNewEmptyNode'
      CardIndex = 2
      TabOrder = 2
      ExplicitWidth = 404
      ExplicitHeight = 192
    end
    object cardNewFullNode: TCard
      Left = 0
      Top = 0
      Width = 400
      Height = 191
      Caption = 'cardNewFullNode'
      CardIndex = 3
      TabOrder = 3
      ExplicitWidth = 408
      ExplicitHeight = 192
      object dxLayoutControlNewFullNode: TdxLayoutControl
        Left = 0
        Top = 0
        Width = 404
        Height = 192
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 408
        object cxLabel1: TcxLabel
          Left = 10
          Top = 10
          AutoSize = False
          Caption = 'Will install new windows service on this PC'
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -19
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.HotTrack = False
          Style.IsFontAssigned = True
          Properties.Alignment.Horz = taCenter
          Properties.Alignment.Vert = taVCenter
          Properties.WordWrap = True
          Transparent = True
          Height = 89
          Width = 384
          AnchorX = 202
          AnchorY = 55
        end
        object btnNewFullNodeCancel: TcxButton
          Left = 124
          Top = 157
          Width = 75
          Height = 25
          Caption = 'Cancel'
          LookAndFeel.NativeStyle = False
          LookAndFeel.SkinName = 'iMaginary'
          OptionsImage.Glyph.SourceDPI = 96
          OptionsImage.Glyph.SourceHeight = 16
          OptionsImage.Glyph.SourceWidth = 16
          OptionsImage.Glyph.Data = {
            3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D225554
            462D38223F3E0D0A3C7376672076657273696F6E3D22312E31222069643D224C
            617965725F312220786D6C6E733D22687474703A2F2F7777772E77332E6F7267
            2F323030302F7376672220786D6C6E733A786C696E6B3D22687474703A2F2F77
            77772E77332E6F72672F313939392F786C696E6B2220783D223070782220793D
            22307078222076696577426F783D2230203020333220333222207374796C653D
            22656E61626C652D6261636B67726F756E643A6E657720302030203332203332
            3B2220786D6C3A73706163653D227072657365727665223E262331333B262331
            303B3C7374796C6520747970653D22746578742F6373732220786D6C3A737061
            63653D227072657365727665223E2E426C75657B66696C6C3A23313137374437
            3B7D262331333B262331303B2623393B2E59656C6C6F777B66696C6C3A234646
            423131353B7D262331333B262331303B2623393B2E426C61636B7B66696C6C3A
            233732373237323B7D262331333B262331303B2623393B2E477265656E7B6669
            6C6C3A233033394332333B7D262331333B262331303B2623393B2E5265647B66
            696C6C3A234431314331433B7D262331333B262331303B2623393B2E7374307B
            6F7061636974793A302E37353B7D262331333B262331303B2623393B2E737431
            7B6F7061636974793A302E353B7D3C2F7374796C653E0D0A3C672069643D2250
            726576696F7573223E0D0A09093C706F6C79676F6E20636C6173733D22426C75
            652220706F696E74733D2232362C342032362C323820362C3136202623393B22
            2F3E0D0A093C2F673E0D0A3C2F7376673E0D0A}
          TabOrder = 2
          OnClick = btnNewFullNodeCancelClick
        end
        object btnNewFullNodeInstall: TcxButton
          Left = 205
          Top = 157
          Width = 75
          Height = 25
          Caption = 'Install'
          LookAndFeel.NativeStyle = False
          LookAndFeel.SkinName = 'iMaginary'
          OptionsImage.Glyph.SourceHeight = 16
          OptionsImage.Glyph.SourceWidth = 16
          OptionsImage.Layout = blGlyphRight
          TabOrder = 3
          OnClick = btnNewFullNodeInstallClick
        end
        object edtServiceName: TcxTextEdit
          Left = 103
          Top = 105
          Style.BorderColor = clWindowFrame
          Style.BorderStyle = ebs3D
          Style.HotTrack = False
          Style.TransparentBorder = False
          TabOrder = 1
          Width = 291
        end
        object dxLayoutControlNewFullNodeGroup_Root: TdxLayoutGroup
          AlignHorz = ahClient
          AlignVert = avClient
          Hidden = True
          ItemIndex = 1
          ShowBorder = False
          Index = -1
        end
        object dxLayoutItem2: TdxLayoutItem
          Parent = dxLayoutControlNewFullNodeGroup_Root
          AlignHorz = ahClient
          AlignVert = avClient
          CaptionOptions.Text = 'cxLabel1'
          CaptionOptions.Visible = False
          Control = cxLabel1
          ControlOptions.OriginalHeight = 23
          ControlOptions.OriginalWidth = 46
          ControlOptions.ShowBorder = False
          Index = 0
        end
        object dxLayoutItem3: TdxLayoutItem
          Parent = dxLayoutGroup2
          CaptionOptions.Text = 'cxButton1'
          CaptionOptions.Visible = False
          Control = btnNewFullNodeCancel
          ControlOptions.OriginalHeight = 25
          ControlOptions.OriginalWidth = 75
          ControlOptions.ShowBorder = False
          Index = 0
        end
        object dxLayoutItem4: TdxLayoutItem
          Parent = dxLayoutGroup2
          CaptionOptions.Text = 'cxButton2'
          CaptionOptions.Visible = False
          Control = btnNewFullNodeInstall
          ControlOptions.OriginalHeight = 25
          ControlOptions.OriginalWidth = 75
          ControlOptions.ShowBorder = False
          Index = 1
        end
        object dxLayoutGroup2: TdxLayoutGroup
          Parent = dxLayoutControlNewFullNodeGroup_Root
          AlignHorz = ahCenter
          CaptionOptions.Text = 'New Group'
          LayoutDirection = ldHorizontal
          ShowBorder = False
          Index = 3
        end
        object dxLayoutGroup3: TdxLayoutGroup
          Parent = dxLayoutControlNewFullNodeGroup_Root
          AlignHorz = ahClient
          AlignVert = avClient
          CaptionOptions.Text = 'New Group'
          LayoutDirection = ldHorizontal
          ShowBorder = False
          Index = 2
        end
        object layedtServiceName: TdxLayoutItem
          Parent = dxLayoutControlNewFullNodeGroup_Root
          CaptionOptions.Text = 'New Service name'
          Control = edtServiceName
          ControlOptions.OriginalHeight = 21
          ControlOptions.OriginalWidth = 121
          ControlOptions.ShowBorder = False
          Index = 1
        end
      end
    end
  end
  object FileOpenDialog: TFileOpenDialog
    FavoriteLinks = <>
    FileTypes = <>
    Options = [fdoPickFolders]
    Left = 56
    Top = 40
  end
end
