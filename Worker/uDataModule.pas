unit uDataModule;

interface

uses
  System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  ServiceManager, PJVersionInfo, Storj.ConfigParser, uVersionGetter,
  uMessageShower,
  uLogger, uNotificationService, Storj.Consts, System.Threading, WinAPI.Windows, uSavedData;

type
  TUpdatingData = record
    FreshVersionDir: String;
    OldVersionDir: String;
    OldVersion: String;
    IsStartService: Boolean;
    ServiceName: String;
  end;

  TDM = class(TDataModule)
    mtblNodes: TFDMemTable;
    dtsrcNodes: TDataSource;
    mtblNodesServiceName: TStringField;
    mtblNodesServiceState: TStringField;
    mtblNodesInstallPath: TStringField;
    mtblNodesVersion: TStringField;
    mtblNodesDashboardPort: TStringField;
    mtblNodesStoragePath: TStringField;
    mtblNodesStartupType: TStringField;
    mtblNodesLogSize: TStringField;
    mtblNodesAutoStart: TBooleanField;
    mtblNodesAutoUpdate: TBooleanField;
    procedure DataModuleCreate(Sender: TObject);
  private
    FGithubVersion: String;
    FMessageShower: IMessageShower;

    function GetNodePathForUpgrade: String;
    procedure RecieveGithubFile(AStream: TStream);
    procedure CheckWorkingDir;
    function NormalizeLogFileName(const AConfValue: String): String;
    procedure DoUpgradeSelectedNode(const ASourceDir: String; aIsStartNode: Boolean = True); overload;
    procedure DoUpgradeSelectedNode(const aUpdatingData: TUpdatingData); overload;

    procedure RecieveStorjioVersionForAutoUpdate(AStorjIOVersion: TStorjIOVersion);

    function IsAutoUpdateEnable: Boolean;
    function IsAutoStartEnable: Boolean;
  public const
    CONFIG_NAME = 'config.yaml';
  public
    procedure LoadServiceNodes;
    function GetExeFromCmdParameters(const ACmdParams: String): String;

    procedure UnzipStoragenode(aStream: TStream);

    procedure StartSelectedNode(AServiceName: String = '');
    procedure StopSelectedNode(AServiceName: String = '');
    procedure UpgradeSelectedNode;
    procedure UpgradeCheckedNodes;
    procedure ChangeStartupType(const AServiceName: String; AStartupType: TServiceStartupType);

    procedure RegisterService(const AServiceName, AStoragenodePath: String);
    procedure UnregisterService(const AServiceName: String);
    procedure CreateNodeStructure(const AStoragenodePath, AStoragePath, AKeyPath: String);
    function CreateNextStorjFolder(aFolderName: String = ''): String;
    function GetNextStorjServiceName: String;
    function GetWorkingDir: String;
    procedure AutoStartNodes;

    procedure SaveData;

    property GithubVersion: String read FGithubVersion write FGithubVersion;
  end;

var
  DM: TDM;

implementation

uses
  System.StrUtils, System.IOUtils, System.Zip, System.SysUtils, WinAPI.WinSvc,
  WinAPI.ShellAPI,
  System.Types;

{%CLASSGROUP 'Vcl.Controls.TControl'}
{$R *.dfm}
{ TDM }

procedure TDM.AutoStartNodes;
begin
  if not IsAutoStartEnable then
    Exit;

  mtblNodes.DisableControls;
  try
    mtblNodes.First;
    while not mtblNodes.Eof do begin
      if mtblNodes.FieldByName('AutoStart').AsBoolean then
        StartSelectedNode(mtblNodes.FieldByName('ServiceName').AsString);

      mtblNodes.Next;
    end;
  finally
    mtblNodes.EnableControls;
  end;
end;

procedure TDM.ChangeStartupType(const AServiceName: String; AStartupType: TServiceStartupType);
var
  SM: TServiceManager;
  SD: TServiceDescriptor;
begin
  SM := TServiceManager.Create(nil);
  try
    SM.Connect;
    SM.QueryServiceConfig(AServiceName, SD);
    SD.StartupType := AStartupType;

    SM.ConfigService(AServiceName, SD);
  finally
    SM.Free;
  end;
end;

procedure TDM.CheckWorkingDir;
begin
  if not TDirectory.Exists(GetWorkingDir) then
    TDirectory.CreateDirectory(GetWorkingDir);
end;

function TDM.CreateNextStorjFolder(aFolderName: String = ''): String;
var
  Num: String;
begin
  if mtblNodes.RecordCount = 0 then
    Num := ''
  else
    Num := mtblNodes.RecordCount.ToString;

  if aFolderName.IsEmpty then
    aFolderName := 'Storj' + Num;

  Result := 'C:\Program Files\' + aFolderName + '\Storage Node\';
  TDirectory.CreateDirectory(Result);
end;

procedure TDM.CreateNodeStructure(const AStoragenodePath, AStoragePath, AKeyPath: String);
var
  SEInfo: TShellExecuteInfo;
  ExitCode: DWORD;
  ParamString, StartInString: string;
  StorageDir, ConfigDir, KeyDir: string;
begin
  if TFile.Exists(TPath.Combine(GetWorkingDir, CONFIG_NAME)) then
    TFile.Delete(TPath.Combine(GetWorkingDir, CONFIG_NAME));

  FillChar(SEInfo, SizeOf(SEInfo), 0);
  SEInfo.cbSize := SizeOf(TShellExecuteInfo);
  SEInfo.fMask := SEE_MASK_NOCLOSEPROCESS;
  SEInfo.Wnd := 0;
  SEInfo.lpFile := PChar('cmd.exe');

  StorageDir := ExcludeTrailingPathDelimiter(AStoragePath);
  ConfigDir := ExcludeTrailingPathDelimiter(GetWorkingDir);
  KeyDir := ExcludeTrailingPathDelimiter(AKeyPath);
  ParamString := '/K storagenode.exe setup --storage.path "' + StorageDir + '" --config-dir "' + ConfigDir + '" --identity-dir "' + KeyDir + '"';
  SEInfo.lpParameters := PChar(ParamString);

  StartInString := IncludeTrailingPathDelimiter(AStoragenodePath);
  SEInfo.lpDirectory := PChar(StartInString);
  SEInfo.nShow := SW_SHOWNORMAL;

  if ShellExecuteEx(@SEInfo) then begin
    repeat
      Sleep(20);
      GetExitCodeProcess(SEInfo.hProcess, ExitCode);
    until (ExitCode <> STILL_ACTIVE);
  end
  else
    raise Exception.Create('Error create node structure');
end;

procedure TDM.DataModuleCreate(Sender: TObject);
begin
  FGithubVersion := '-';
  CheckWorkingDir;
  FMessageShower := TVCLMessageShower.Create;

  TLogSaver.GetInstance.AddLoger(TFileLogger.Create('SNOHelper.log'));
end;

procedure TDM.DoUpgradeSelectedNode(const aUpdatingData: TUpdatingData);
begin
  TTask.Create(
    procedure
    var
      Counter: Integer;
      SM: TServiceManager;
    begin
      try
        SM := TServiceManager.Create(nil);
        try
          SM.Connect;
          SM.StopService(aUpdatingData.ServiceName);
          Counter := 0;
          while Counter < 60 do begin
            SM.Refresh;
            if SM.Services.ServiceByName(aUpdatingData.ServiceName).CurrentState = SERVICE_STOPPED then
              break;
            Sleep(2000);
            Counter := Counter + 1;
          end;
          SM.Refresh;

          if SM.Services.ServiceByName(aUpdatingData.ServiceName).CurrentState = SERVICE_STOPPED then begin
            UpdateNode(aUpdatingData.FreshVersionDir, aUpdatingData.OldVersionDir, aUpdatingData.OldVersion);

            if aUpdatingData.IsStartService then
              SM.StartService(aUpdatingData.ServiceName);
          end;
        finally
          SM.Free;
        end;
      except
        on E: Exception do begin
          var EMessage := E.Message;
          TThread.Synchronize(nil,
            procedure
            begin
              LoadServiceNodes;
              FMessageShower.ShowError(EMessage);
            end);
        end;
      end;
    end).Start;
end;

procedure TDM.DoUpgradeSelectedNode(const ASourceDir: String; aIsStartNode: Boolean = True);
var
  NodeInstallPath, NodeVersion, EMessage, NodeServiceName: String;
begin
  NodeServiceName := mtblNodesServiceName.AsString;
  NodeInstallPath := mtblNodesInstallPath.AsString;
  NodeVersion := mtblNodesVersion.AsString;

  TTask.Create(
    procedure
    var
      Counter: Integer;
      SM: TServiceManager;
    begin
      try
        SM := TServiceManager.Create(nil);
        try
          SM.Connect;
          SM.StopService(NodeServiceName);
          Counter := 0;
          while Counter < 3 do begin
            SM.Refresh;
            if SM.Services.ServiceByName(NodeServiceName).CurrentState = SERVICE_STOPPED then
              break;
            Sleep(1000);
            Counter := Counter + 1;
          end;
          UpdateNode(ASourceDir, NodeInstallPath, NodeVersion);

          if aIsStartNode then begin
            SM.StartService(NodeServiceName);
            Counter := 0;
            while Counter < 3 do begin
              SM.Refresh;
              if SM.Services.ServiceByName(NodeServiceName).CurrentState = SERVICE_RUNNING then
                break;
              Sleep(1000);
              Counter := Counter + 1;
            end;
          end;
        finally
          SM.Free;
        end;

        TThread.Queue(nil,
          procedure
          begin
            NotificationService.SendMessage(COMMAND_HIDE_ANI, nil);
            LoadServiceNodes;
          end);
      except
        on E: Exception do begin
          EMessage := E.Message;
          TThread.Synchronize(nil,
            procedure
            begin
              NotificationService.SendMessage(COMMAND_HIDE_ANI, nil);
              LoadServiceNodes;
              FMessageShower.ShowError(EMessage);
            end);
        end;
      end;
    end).Start;
  NotificationService.SendMessage(COMMAND_SHOW_ANI_WITH_TEXT, 'Updating node');
end;

function TDM.GetExeFromCmdParameters(const ACmdParams: String): String;
begin
  Result := ACmdParams.Remove(0, 1);
  Result := Result.Substring(0, Result.IndexOf('"'));
end;

function TDM.GetNextStorjServiceName: String;
var
  Num: String;
begin
  if mtblNodes.RecordCount = 0 then
    Num := ''
  else
    Num := mtblNodes.RecordCount.ToString;

  Result := 'storagenode' + Num;
end;

function TDM.GetNodePathForUpgrade: String;
var
  RecNo: Integer;
  GithubVer, NodeVer: TPJVersionNumber;
begin
  Result := '';
  RecNo := mtblNodes.RecNo;
  GithubVer := FGithubVersion;

  mtblNodes.DisableControls;
  try
    mtblNodes.First;
    while not mtblNodes.Eof do begin
      NodeVer := mtblNodesVersion.AsString;
      if NodeVer = GithubVer then begin
        Result := mtblNodesInstallPath.AsString;
        break;
      end;

      mtblNodes.Next;
    end;

    mtblNodes.RecNo := RecNo;
  finally
    mtblNodes.EnableControls;
  end;
end;

function TDM.GetWorkingDir: String;
begin
  Result := TPath.Combine(TPath.GetSharedDocumentsPath, 'SNO_Helper');
end;

function TDM.IsAutoStartEnable: Boolean;
begin
  Result := False;
end;

function TDM.IsAutoUpdateEnable: Boolean;
begin
  Result := False;
end;

procedure TDM.LoadServiceNodes;
var
  SM: TServiceManager;
  SE: TServiceEntry;
  SD: TServiceDescriptor;
  i: Integer;
  Ver: TPJVersionInfo;
  InstallPath, ExeName: String;
  RecordIndex: Integer;
  ConfigParser: TStorjConfigParser;
begin
  Ver := nil;
  SM := TServiceManager.Create(nil);
  var SavedData := TSavedData.Create;
  try
    SM.RefreshOnConnect := True;
    SM.Connect;

    Ver := TPJVersionInfo.Create(nil);

    mtblNodes.DisableControls;
    try
      RecordIndex := mtblNodes.RecNo;
      if mtblNodes.Active then
        mtblNodes.EmptyDataSet
      else
        mtblNodes.CreateDataSet;

      for i := 0 to SM.Services.Count - 1 do begin
        SE := SM.Services.Items[i];
        if ContainsText(SE.ServiceName, 'storagenode') and not ContainsText(SE.ServiceName, 'update') then begin
          SM.QueryServiceConfig(SE.ServiceName, SD);
          mtblNodes.Append;
          mtblNodes['AutoStart'] := SavedData.AutoStartNodes.Contains(SE.ServiceName);
          mtblNodes['AutoUpdate'] := SavedData.AutoUpdateNodes.Contains(SE.ServiceName);
          mtblNodes['ServiceName'] := SE.ServiceName;
          mtblNodes['ServiceState'] := ServiceStateToString(SE.CurrentState);
          mtblNodes['StartupType'] := SD.GetStartupTypeAsString;
          ExeName := GetExeFromCmdParameters(SD.CommandLine);
          InstallPath := ExtractFilePath(ExeName);
          mtblNodes['InstallPath'] := InstallPath;
          Ver.FileName := ExeName;
          mtblNodes['Version'] := Ver.FileVersionNumber;

          if FileExists(InstallPath + CONFIG_NAME) then begin
            ConfigParser := TStorjConfigParser.Create(InstallPath + CONFIG_NAME);
            try
              mtblNodes['DashboardPort'] := ConfigParser.GetPort('console.address');
              mtblNodes['StoragePath'] := ConfigParser.GetValue('storage.path');
              mtblNodes['LogSize'] := FileSizeToStr(uVersionGetter.GetFileSize(NormalizeLogFileName(ConfigParser.GetValue('log.output'))));
            finally
              ConfigParser.Free;
            end;
          end;
          mtblNodes.Post;
        end;
      end;

      mtblNodes.RecNo := RecordIndex;
    finally
      mtblNodes.EnableControls;
    end;
  finally
    Ver.Free;
    SM.Free;
    SavedData.Free;
  end;
end;

function TDM.NormalizeLogFileName(const AConfValue: String): String;
const
  REMOVE_STR = 'winfile:///';
begin
  Result := AConfValue.Remove(AConfValue.IndexOf(REMOVE_STR), REMOVE_STR.Length).Replace('\\', '\', [rfReplaceAll]);
end;

procedure TDM.RecieveGithubFile(AStream: TStream);
begin
  NotificationService.SendMessage(COMMAND_HIDE_ANI, nil);

  if AStream = nil then
    Exit;

  try
    if TFile.Exists(IncludeTrailingPathDelimiter(GetWorkingDir) + 'storagenode.exe') then
      TFile.Delete(IncludeTrailingPathDelimiter(GetWorkingDir) + 'storagenode.exe');

    UnzipStoragenode(AStream);

    DoUpgradeSelectedNode(GetWorkingDir);
  except
    on E: Exception do
      TLogSaver.GetInstance.Log('TDM.RecieveGithubFile: ' + E.Message);
  end;
end;

procedure TDM.RecieveStorjioVersionForAutoUpdate(AStorjIOVersion: TStorjIOVersion);
var
  IOVer, NodeVer: TPJVersionNumber;
  UpdatingData: TUpdatingData;
  isNeedDownloadFreshVersionFromGithub: Boolean;
  Stream: TStream;
begin
  if (AStorjIOVersion.Cursor.Length > 1) and CharInSet(AStorjIOVersion.Cursor[1], ['a'..'f']) then begin
    IOVer := AStorjIOVersion.Version;

    var Ver := TPJVersionInfo.Create(nil);
    try
      mtblNodes.First;
      while not mtblNodes.Eof do begin
        if mtblNodes.FieldByName('AutoUpdate').AsBoolean then begin
          NodeVer := mtblNodes.FieldByName('Version').AsString;
          if NodeVer < IOVer then begin
            if TFile.Exists(IncludeTrailingPathDelimiter(GetWorkingDir) + 'storagenode.exe') then begin
              Ver.FileName := IncludeTrailingPathDelimiter(GetWorkingDir) + 'storagenode.exe';
              isNeedDownloadFreshVersionFromGithub := Ver.FileVersionNumber < IOVer;
            end else
              isNeedDownloadFreshVersionFromGithub := true;

            if isNeedDownloadFreshVersionFromGithub then begin
              Stream := DownloadFromGithub(AStorjIOVersion.Version);

              if Stream <> nil then begin
                try
                  if TFile.Exists(IncludeTrailingPathDelimiter(GetWorkingDir) + 'storagenode.exe') then
                    TFile.Delete(IncludeTrailingPathDelimiter(GetWorkingDir) + 'storagenode.exe');

                  UnzipStoragenode(Stream);
                finally
                  Stream.Free;
                end;
              end;
            end;

            UpdatingData.FreshVersionDir := GetWorkingDir;
            UpdatingData.OldVersionDir := mtblNodes.FieldByName('InstallPath').AsString;
            UpdatingData.OldVersion := mtblNodes.FieldByName('Version').AsString;
            UpdatingData.IsStartService := true;
            UpdatingData.ServiceName := mtblNodes.FieldByName('ServiceName').AsString;
            DoUpgradeSelectedNode(UpdatingData);
          end;
        end;

        mtblNodes.Next;
      end;
    finally
      Ver.Free;
    end;
  end;
end;

procedure TDM.RegisterService(const AServiceName, AStoragenodePath: String);
var
  SM: TServiceManager;
  SD: TServiceDescriptor;
  StorageNodeExe, ConfigPath: String;
begin
  SM := TServiceManager.Create(nil);
  try
    SM.Connect;
    SD.Name := AServiceName;
    SD.DisplayName := AServiceName;
    SD.LoadOrderGroup := '';
    SD.UserName := '';
    SD.Password := '';
    SD.Dependencies := '';
    SD.DesiredAccess := SERVICE_ALL_ACCESS;
    SD.ServiceType := SERVICE_WIN32_OWN_PROCESS;
    SD.StartupType := sstAuto;
    SD.ErrorControl := SERVICE_ERROR_NORMAL;
    StorageNodeExe := TPath.Combine(AStoragenodePath, 'storagenode.exe');
    ConfigPath := IncludeTrailingPathDelimiter(AStoragenodePath) + '\';
    SD.CommandLine := '"' + StorageNodeExe + '" run --config-dir "' + ConfigPath + '"';

    SM.RegisterService(SD);
  finally
    SM.Free;
  end;
end;

procedure TDM.SaveData;
begin
  var RecNo := mtblNodes.RecNo;

  var SavedData := TSavedData.Create;
  try
    SavedData.AutoStartNodes.Clear;
    SavedData.AutoUpdateNodes.Clear;

    mtblNodes.DisableControls;
    try
      mtblNodes.First;
      while not mtblNodes.Eof do begin
        if mtblNodes.FieldByName('AutoStart').AsBoolean then
          SavedData.AutoStartNodes.Add(mtblNodes.FieldByName('ServiceName').AsString);

        if mtblNodes.FieldByName('AutoUpdate').AsBoolean then
          SavedData.AutoUpdateNodes.Add(mtblNodes.FieldByName('ServiceName').AsString);

        mtblNodes.Next;
      end;
      mtblNodes.RecNo := RecNo;
      SavedData.Save;
    finally
      mtblNodes.EnableControls;
    end;
  finally
    SavedData.Free;
  end;
end;

procedure TDM.StartSelectedNode(AServiceName: String = '');
var
  SM: TServiceManager;
  ST: TServiceStatus;
begin
  if mtblNodes.IsEmpty then
    Exit;

  SM := TServiceManager.Create(nil);
  try
    SM.Connect;
    if AServiceName.IsEmpty then
      AServiceName := mtblNodesServiceName.AsString;

    SM.GetServiceStatus(AServiceName, ST);
    if ServiceStateToString(ST.dwCurrentState) = 'Stopped' then
      SM.StartService(AServiceName);
  finally
    SM.Free;
  end;
end;

procedure TDM.StopSelectedNode(AServiceName: String = '');
var
  SM: TServiceManager;
begin
  if mtblNodes.IsEmpty then
    Exit;

  SM := TServiceManager.Create(nil);
  try
    SM.Connect;
    if AServiceName.IsEmpty then
      AServiceName := mtblNodesServiceName.AsString;
    SM.StopService(AServiceName);
  finally
    SM.Free;
  end;
end;

procedure TDM.UnregisterService(const AServiceName: String);
var
  SM: TServiceManager;
begin
  SM := TServiceManager.Create(nil);
  try
    SM.Connect;
    SM.UnregisterService(AServiceName);
  finally
    SM.Free;
  end;
end;

procedure TDM.UnzipStoragenode(aStream: TStream);
var
  Zip: TZipFile;
begin
  Zip := TZipFile.Create;
  try
    Zip.Open(AStream, TZipMode.zmRead);
    var fileNames := Zip.FileNames;
    Zip.Extract('storagenode.exe', GetWorkingDir);
  finally
    Zip.Free;
  end;
end;

procedure TDM.UpgradeCheckedNodes;
begin
  if IsAutoUpdateEnable then
    GetStorjIOLatestVersion(RecieveStorjioVersionForAutoUpdate);
end;

procedure TDM.UpgradeSelectedNode;
var
  GithubVer, NodeVer: TPJVersionNumber;
  PathForCopy: String;
begin
  if FGithubVersion.IsEmpty or FGithubVersion.Equals('-') then begin
    TLogSaver.GetInstance.Log('GitHub version is unknown');
    FMessageShower.PrintMessage('GitHub version is unknown');
    Exit;
  end;

  if not mtblNodes.Active or mtblNodes.IsEmpty then begin
    TLogSaver.GetInstance.Log('Node is not selected');
    FMessageShower.PrintMessage('Node is not selected');
    Exit;
  end;

  GithubVer := FGithubVersion;
  NodeVer := mtblNodesVersion.AsString;

  if NodeVer < GithubVer then begin
    PathForCopy := GetNodePathForUpgrade;
    if PathForCopy.IsEmpty then begin
      DownloadFromGithubAsync(FGithubVersion, RecieveGithubFile);
      NotificationService.SendMessage(COMMAND_SHOW_ANI_WITH_TEXT, 'Downloading new version from github');
    end else
      DoUpgradeSelectedNode(PathForCopy);
  end else begin
    TLogSaver.GetInstance.Log('Not need update');
    FMessageShower.PrintMessage('Not need update');
  end;
end;

//procedure GetDiskInfo(Node: IXMLNode);
//begin
//  var infoNode := Node.ChildNodes['Hard_Disk_Summary'];
//  form2.Memo1.Lines.Add('Disk num: ' + infoNode.ChildValues['Hard_Disk_Number']);
//  form2.Memo1.Lines.Add('Model: ' + VarToStr(infoNode.ChildValues['Hard_Disk_Model_ID']));
//  form2.Memo1.Lines.Add('Drive: ' + VarToStr(infoNode.ChildValues['Logical_Drive_s']));
//  form2.Memo1.Lines.Add('Temp: ' + VarToStr(infoNode.ChildValues['Current_Temperature']));
//  form2.Memo1.Lines.Add('Health: ' + VarToStr(infoNode.ChildValues['Health']));
//  form2.Memo1.Lines.Add('Disk activity: ' + VarToStr(Node.ChildNodes['Transfer_Rate_Information'].ChildValues['Current_Disk_Activity']));
//end;
//
//procedure TForm2.Button1Click(Sender: TObject);
//var
//  XMLDoc: IXMLDocument;
//  RootNode: IXMLNode;
//begin
//  NetHTTPClient1.CustomHeaders['Authorization'] := 'Basic QWxleDo0ODE1MTYyMzQy';
//  var response := NetHTTPClient1.Get('http://192.168.0.112:61220/xml');
//
//  XMLDoc := TXMLDocument.Create(nil);
//  XMLDoc.LoadFromXML(response.ContentAsString(TEncoding.ANSI));
//  RootNode := XMLDoc.ChildNodes['Hard_Disk_Sentinel'];
//  for var i := 0 to 20 do begin
//    var index := RootNode.ChildNodes.IndexOf('Physical_Disk_Information_Disk_' + i.ToString);
//    if index >= 0 then begin
//      memo1.Lines.Add('Disk ' + i.ToString + ' exists');
//      GetDiskInfo(RootNode.ChildNodes[index]);
//    end else
//      memo1.Lines.Add('Disk ' + i.ToString + ' not exists');
//  end;
//
//  memo1.Lines.Add('');
//
//  for var i := 0 to RootNode.ChildNodes.Count - 1 do
//    memo1.Lines.Add(RootNode.ChildNodes[i].NodeName);
//
//
//  memo1.Lines.Add(response.ContentAsString(TEncoding.ANSI));
//end;

end.
