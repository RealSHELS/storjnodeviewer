unit ufrmSettings;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBasic, dxSkinOffice2013LightGray,
  dxSkinOffice2019DarkGray, dxSkinOffice2019White, cxClasses, dxLayoutContainer,
  dxLayoutControl, dxLayoutcxEditAdapters, cxContainer, cxEdit, cxCheckBox,
  uRegistryAutoRun, dxSkiniMaginary, dxSkinSeven, dxSkinSevenClassic, dxSkinWXI,
  dxSkinOffice2019Black, dxSkinWhiteprint;

type
  TfrmSettings = class(TForm)
    dxLayoutControlGroup_Root: TdxLayoutGroup;
    dxLayoutControl: TdxLayoutControl;
    chkAutoRun: TcxCheckBox;
    laychkAutoRun: TdxLayoutItem;
    chkAutoUpdate: TcxCheckBox;
    laychkAutoUpdate: TdxLayoutItem;
    chkAutoStart: TcxCheckBox;
    laychkAutoStart: TdxLayoutItem;
    procedure FormCreate(Sender: TObject);
    procedure chkAutoRunPropertiesChange(Sender: TObject);
  private
    const AUTO_RUN_TITLE = 'SNO_Helper';
  public
    { Public declarations }
  end;

var
  frmSettings: TfrmSettings;

implementation

{$R *.dfm}

procedure TfrmSettings.chkAutoRunPropertiesChange(Sender: TObject);
begin
  if chkAutoRun.Checked then
    //RunOnStartup(AUTO_RUN_TITLE, Application.ExeName)
    RunOnStartupAsTask(AUTO_RUN_TITLE, Application.ExeName)
  else
    RemoveOnStartup(AUTO_RUN_TITLE);
end;

procedure TfrmSettings.FormCreate(Sender: TObject);
begin
  chkAutoRun.Checked := CheckOnStartup(AUTO_RUN_TITLE);
end;

end.
