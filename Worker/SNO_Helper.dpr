program SNO_Helper;

uses
  Vcl.Forms,
  ufrmMain in 'ufrmMain.pas' {frmMain},
  uDataModule in 'uDataModule.pas' {DM: TDataModule},
  Vcl.Themes,
  Vcl.Styles,
  Storj.ConfigParser in 'Storj.ConfigParser.pas',
  ufrmChangeServiceStartupType in 'ufrmChangeServiceStartupType.pas' {frmChangeServiceStartupType},
  uMessageShower in 'uMessageShower.pas',
  ufrmAni in 'ufrmAni.pas' {frmAni},
  uLogger in 'uLogger.pas',
  uNotificationService in 'uNotificationService.pas',
  Storj.Consts in 'Storj.Consts.pas',
  ufrmNewNodeInstaller in 'ufrmNewNodeInstaller.pas' {frmNewNodeInstaller},
  uVersionGetter in '..\Shared\uVersionGetter.pas',
  AMR.AutoObject in '..\Libs\AutoManagementRecords\AMR.AutoObject.pas',
  uRegistryAutoRun in 'uRegistryAutoRun.pas',
  ufrmSettings in 'ufrmSettings.pas' {frmSettings},
  uSavedData in 'uSavedData.pas',
  ServiceManager in '..\Libs\ServiceManager.pas',
  PJVersionInfo in '..\Libs\PJVersionInfo.pas';

{$R *.res}

begin
  {$IFDEF DEBUG}
    ReportMemoryLeaksOnShutdown := True;
  {$ENDIF}

  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := '';
  Application.CreateForm(TDM, DM);
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TfrmChangeServiceStartupType, frmChangeServiceStartupType);
  Application.CreateForm(TfrmAni, frmAni);
  Application.CreateForm(TfrmNewNodeInstaller, frmNewNodeInstaller);
  Application.CreateForm(TfrmSettings, frmSettings);
  Application.Run;
end.
