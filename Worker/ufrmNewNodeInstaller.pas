unit ufrmNewNodeInstaller;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.WinXPanels, Vcl.ExtCtrls,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore,
  cxControls, cxContainer, cxEdit, cxLabel, Vcl.StdCtrls, cxButtons, cxClasses,
  dxLayoutContainer, dxLayoutControl, dxLayoutcxEditAdapters,
  dxLayoutControlAdapters, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, uDataModule, dxSkinsDefaultPainters,
  dxSkinBasic, dxSkinOffice2013LightGray,
  dxSkinOffice2019DarkGray, dxSkinOffice2019White, dxSkinWhiteprint;

type
  TfrmNewNodeInstaller = class(TForm)
    CardPanel: TCardPanel;
    cardFirst: TCard;
    cardNewNodeOnOldService: TCard;
    cardNewEmptyNode: TCard;
    cardNewFullNode: TCard;
    GridPanelCardFirst: TGridPanel;
    btnCardFirstNewEmptyNode: TcxButton;
    btnCardFirstNewFullNode: TcxButton;
    btnCardFirstNewNodeOldService: TcxButton;
    dxLayoutControlNewNodeOldServiceGroup_Root: TdxLayoutGroup;
    dxLayoutControlNewNodeOldService: TdxLayoutControl;
    lblNewNodeOldServiceText: TcxLabel;
    laylblNewNodeOldServiceText: TdxLayoutItem;
    btnNewNodeOldServicePrev: TcxButton;
    laybtnNewNodeOldServicePrev: TdxLayoutItem;
    btnNewNodeOldServiceFinish: TcxButton;
    laybtnNewNodeOldServiceFinish: TdxLayoutItem;
    laygrpNewNodeOldServiceButtons: TdxLayoutGroup;
    dxLayoutGroup1: TdxLayoutGroup;
    btnNewNodeOldServiceRefresh: TcxButton;
    dxLayoutItem1: TdxLayoutItem;
    dxLayoutControlNewFullNodeGroup_Root: TdxLayoutGroup;
    dxLayoutControlNewFullNode: TdxLayoutControl;
    cxLabel1: TcxLabel;
    dxLayoutItem2: TdxLayoutItem;
    btnNewFullNodeCancel: TcxButton;
    dxLayoutItem3: TdxLayoutItem;
    btnNewFullNodeInstall: TcxButton;
    dxLayoutItem4: TdxLayoutItem;
    dxLayoutGroup2: TdxLayoutGroup;
    dxLayoutGroup3: TdxLayoutGroup;
    lookNewNodeOldServiceNodeSelect: TcxLookupComboBox;
    dxLayoutItem7: TdxLayoutItem;
    edtStoragepath: TcxTextEdit;
    dxLayoutItem5: TdxLayoutItem;
    edtKeypath: TcxTextEdit;
    dxLayoutItem6: TdxLayoutItem;
    laygrpStoragePath: TdxLayoutGroup;
    laygrpKeysPath: TdxLayoutGroup;
    btnSelectStoragepath: TcxButton;
    dxLayoutItem8: TdxLayoutItem;
    btnSelectKeypath: TcxButton;
    dxLayoutItem9: TdxLayoutItem;
    FileOpenDialog: TFileOpenDialog;
    edtServiceName: TcxTextEdit;
    layedtServiceName: TdxLayoutItem;
    procedure FormShow(Sender: TObject);
    procedure btnNewNodeOldServiceRefreshClick(Sender: TObject);
    procedure btnCardFirstNewNodeOldServiceClick(Sender: TObject);
    procedure btnNewNodeOldServicePrevClick(Sender: TObject);
    procedure btnNewNodeOldServiceFinishClick(Sender: TObject);
    procedure btnCardFirstNewEmptyNodeClick(Sender: TObject);
    procedure btnCardFirstNewFullNodeClick(Sender: TObject);
    procedure btnNewFullNodeCancelClick(Sender: TObject);
    procedure btnNewFullNodeInstallClick(Sender: TObject);
    procedure btnSelectStoragepathClick(Sender: TObject);
    procedure btnSelectKeypathClick(Sender: TObject);
  private
    FInstallPath: String;

    procedure RecieveGithubFile(AStream: TStream);
    procedure InstallNewFullNode;
  public
    { Public declarations }
  end;

var
  frmNewNodeInstaller: TfrmNewNodeInstaller;

implementation

{$R *.dfm}

uses
  uVersionGetter, System.IOUtils, uNotificationService, Storj.Consts, System.Zip, uLogger, System.UItypes,
  FileCtrl;

procedure TfrmNewNodeInstaller.btnCardFirstNewEmptyNodeClick(Sender: TObject);
begin
  CardPanel.ActiveCard := cardNewEmptyNode;
end;

procedure TfrmNewNodeInstaller.btnCardFirstNewFullNodeClick(Sender: TObject);
begin
  CardPanel.ActiveCard := cardNewFullNode;
end;

procedure TfrmNewNodeInstaller.btnCardFirstNewNodeOldServiceClick(
  Sender: TObject);
begin
  CardPanel.ActiveCard := cardNewNodeOnOldService;
end;

procedure TfrmNewNodeInstaller.btnNewFullNodeCancelClick(Sender: TObject);
begin
  CardPanel.ActiveCard := cardFirst;
end;

procedure TfrmNewNodeInstaller.btnNewFullNodeInstallClick(Sender: TObject);
begin
  InstallNewFullNode;
end;

procedure TfrmNewNodeInstaller.btnNewNodeOldServiceFinishClick(Sender: TObject);
var
  StoragenodePath, StoragePath, KeyPath: String;
begin
  if DM.mtblNodes.Locate('ServiceName', lookNewNodeOldServiceNodeSelect.EditValue) then begin
    StoragenodePath := DM.mtblNodesInstallPath.AsString;
    StoragePath := edtStoragepath.Text;
    KeyPath := edtKeypath.Text;

    if StoragePath.IsEmpty then begin
      MessageDlg('Not specified storage path', TMsgDlgType.mtError, [mbOk], 0);
      Exit;
    end;

    if KeyPath.IsEmpty then begin
      MessageDlg('Not specified key path', TMsgDlgType.mtError, [mbOk], 0);
      Exit;
    end;

    DM.CreateNodeStructure(StoragenodePath, StoragePath, KeyPath);
    ShowMessage('Node structure created');
    Close;
  end;
end;

procedure TfrmNewNodeInstaller.btnNewNodeOldServicePrevClick(Sender: TObject);
begin
  CardPanel.ActiveCard := cardFirst;
end;

procedure TfrmNewNodeInstaller.btnNewNodeOldServiceRefreshClick(Sender: TObject);
begin
  DM.LoadServiceNodes;
end;

procedure TfrmNewNodeInstaller.btnSelectKeypathClick(Sender: TObject);
begin
  if FileOpenDialog.Execute then
    edtKeypath.Text := FileOpenDialog.FileName;
end;

procedure TfrmNewNodeInstaller.btnSelectStoragepathClick(Sender: TObject);
begin
  if FileOpenDialog.Execute then
    edtStoragepath.Text := FileOpenDialog.FileName;
end;

procedure TfrmNewNodeInstaller.FormShow(Sender: TObject);
begin
  CardPanel.ActiveCard := cardFirst;
  FInstallPath := '';
end;

procedure TfrmNewNodeInstaller.InstallNewFullNode;
var
  ServiceName: String;
begin
  if not String(edtServiceName.Text).Trim.IsEmpty then begin
    FInstallPath := DM.CreateNextStorjFolder(edtServiceName.Text);
    ServiceName := edtServiceName.Text;
  end else begin
    FInstallPath := DM.CreateNextStorjFolder;
    ServiceName := DM.GetNextStorjServiceName;
  end;
  DM.RegisterService(ServiceName, FInstallPath);

  DownloadFromGithubAsync(DM.GithubVersion, RecieveGithubFile);
  NotificationService.SendMessage(COMMAND_SHOW_ANI_WITH_TEXT, 'Downloading from github');
end;

procedure TfrmNewNodeInstaller.RecieveGithubFile(AStream: TStream);
begin
  NotificationService.SendMessage(COMMAND_HIDE_ANI, nil);

  if AStream = nil then
    Exit;

  try
    if TFile.Exists(IncludeTrailingPathDelimiter(DM.GetWorkingDir) + 'storagenode.exe') then
      TFile.Delete(IncludeTrailingPathDelimiter(DM.GetWorkingDir) + 'storagenode.exe');

    var dir := DM.GetWorkingDir;

    DM.UnzipStoragenode(AStream);

    UpdateNode(DM.GetWorkingDir, FInstallPath, '0.0.0.0');
    DM.LoadServiceNodes;

    ShowMessage('New full node installed. You can check settings and start your new node');
    Close;
  except
    on E: Exception do
      TLogSaver.GetInstance.Log('TDM.RecieveGithubFile: ' + E.Message);
  end;
end;

end.
