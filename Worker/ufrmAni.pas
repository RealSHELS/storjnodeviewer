unit ufrmAni;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxLayoutContainer, dxLayoutcxEditAdapters,
  cxContainer, cxEdit, cxLabel, dxActivityIndicator, cxClasses, dxLayoutControl,
  uNotificationService, dxSkinsDefaultPainters, dxSkinBasic,
  dxSkinOffice2013LightGray, dxSkinOffice2019DarkGray,
  dxSkinOffice2019White, dxSkinWhiteprint;

type
  TfrmAni = class(TForm)
    dxLayoutControlGroup_Root: TdxLayoutGroup;
    dxLayoutControl: TdxLayoutControl;
    dxActivityIndicator: TdxActivityIndicator;
    layActivitiIndicator: TdxLayoutItem;
    lblText: TcxLabel;
    laylblText: TdxLayoutItem;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    procedure OnCommandShowWithText(const AData: TValue);
    procedure OnCommandHide(const AData: TValue);
  public
    procedure ShowWithText(const AText: String);
  end;

var
  frmAni: TfrmAni;

implementation

{$R *.dfm}

uses
  Storj.Consts;

procedure TfrmAni.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dxActivityIndicator.Active := False;
end;

procedure TfrmAni.FormCreate(Sender: TObject);
begin
  NotificationService.Subscribe(COMMAND_SHOW_ANI_WITH_TEXT, OnCommandShowWithText);
  NotificationService.Subscribe(COMMAND_HIDE_ANI, OnCommandHide);
end;

procedure TfrmAni.FormDestroy(Sender: TObject);
begin
  NotificationService.UnSubscribe(COMMAND_SHOW_ANI_WITH_TEXT, OnCommandShowWithText);
  NotificationService.UnSubscribe(COMMAND_HIDE_ANI, OnCommandHide);
end;

procedure TfrmAni.FormShow(Sender: TObject);
begin
  dxActivityIndicator.Active := True;
end;

procedure TfrmAni.OnCommandHide(const AData: TValue);
begin
  Close;
end;

procedure TfrmAni.OnCommandShowWithText(const AData: TValue);
begin
  ShowWithText(AData.AsString);
end;

procedure TfrmAni.ShowWithText(const AText: String);
begin
  lblText.Caption := AText;
  if not Showing then
    ShowModal;
end;

end.
