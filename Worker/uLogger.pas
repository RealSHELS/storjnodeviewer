unit uLogger;

interface

uses
  System.SysUtils,
  System.SyncObjs,
  System.Generics.Collections;

type
  ILogger = interface
    procedure Log(const AMsg: String);
  end;

  TLogSaver = class
  private
    FCS: TCriticalSection;
    FLoggers: TList<ILogger>;

    constructor ActualCreate;
    destructor ActualDestroy;
    class destructor Destroy;

    class var FLogSaver: TLogSaver;
  public
    procedure AddLoger(ALogger: ILogger);
    procedure Log(const AMsg: String);

    constructor Create;
    class function GetInstance: TLogSaver;
  end;

  TFileLogger = class(TInterfacedObject, ILogger)
  private
    FFileName: String;
  public
    constructor Create(const AFileName: String);
    procedure Log(const AMsg: String);
  end;

implementation

uses
  System.IOUtils;

{ TLogSaver }

constructor TLogSaver.ActualCreate;
begin
  inherited Create;

  FCS := TCriticalSection.Create;
  FLoggers := TList<ILogger>.Create;
end;

destructor TLogSaver.ActualDestroy;
begin
  FCS.Free;
  FLoggers.Free;

  inherited Destroy;
end;

procedure TLogSaver.AddLoger(ALogger: ILogger);
begin
  if not FLoggers.Contains(ALogger) then
    FLoggers.Add(ALogger);
end;

constructor TLogSaver.Create;
begin
  raise Exception.Create('Attempt to create an instance of TLogSaver');
end;

class destructor TLogSaver.Destroy;
begin
  if FLogSaver <> nil then
    FLogSaver.ActualDestroy;
end;

class function TLogSaver.GetInstance: TLogSaver;
begin
  if FLogSaver = nil then
    FLogSaver := TLogSaver.ActualCreate;

  Result := FLogSaver;
end;

procedure TLogSaver.Log(const AMsg: String);
var
  Logger: ILogger;
begin
  FCS.Enter;
  try
    if FLoggers.Count > 0 then
      for Logger in FLoggers do
        Logger.Log(DateTimeToStr(Now) + ' ' + AMsg);
  finally
    FCS.Leave;
  end;
end;

{ TFileLogger }

constructor TFileLogger.Create(const AFileName: String);
begin
  FFileName := AFileName;
end;

procedure TFileLogger.Log(const AMsg: String);
begin
  TFile.AppendAllText(FFileName, AMsg);
end;

end.
