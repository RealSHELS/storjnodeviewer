object DM: TDM
  OnCreate = DataModuleCreate
  Height = 338
  Width = 583
  object mtblNodes: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 112
    Top = 72
    object mtblNodesServiceName: TStringField
      FieldName = 'ServiceName'
      Size = 100
    end
    object mtblNodesServiceState: TStringField
      FieldName = 'ServiceState'
      Size = 100
    end
    object mtblNodesInstallPath: TStringField
      FieldName = 'InstallPath'
      Size = 200
    end
    object mtblNodesVersion: TStringField
      FieldName = 'Version'
    end
    object mtblNodesDashboardPort: TStringField
      FieldName = 'DashboardPort'
      Size = 5
    end
    object mtblNodesStoragePath: TStringField
      FieldName = 'StoragePath'
      Size = 150
    end
    object mtblNodesStartupType: TStringField
      FieldName = 'StartupType'
      Size = 50
    end
    object mtblNodesLogSize: TStringField
      FieldName = 'LogSize'
      Size = 50
    end
    object mtblNodesAutoStart: TBooleanField
      FieldName = 'AutoStart'
    end
    object mtblNodesAutoUpdate: TBooleanField
      FieldName = 'AutoUpdate'
    end
  end
  object dtsrcNodes: TDataSource
    DataSet = mtblNodes
    Left = 112
    Top = 128
  end
end
