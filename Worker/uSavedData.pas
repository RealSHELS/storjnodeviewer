unit uSavedData;

interface

uses
  System.SysUtils, System.Generics.Collections, System.Classes,
  System.JSON;

type
  TSavedData = class
  private
    fAutoStartNodes: TList<string>;
    fAutoUpdateNodes: TList<string>;

    procedure LoadFromJSON;
  private const
    FILENAME = 'SHO Helper SavedData.json';
  public
    constructor Create;
    destructor Destroy; override;

    procedure Save;

    property AutoStartNodes: TList<string> read fAutoStartNodes;
    property AutoUpdateNodes: TList<string> read fAutoUpdateNodes;
  end;

implementation

{ TSavedData }

constructor TSavedData.Create;
begin
  fAutoStartNodes := TList<string>.Create;
  fAutoUpdateNodes := TList<string>.Create;

  LoadFromJSON;
end;

destructor TSavedData.Destroy;
begin
  fAutoStartNodes.Free;
  fAutoUpdateNodes.Free;

  inherited;
end;

procedure TSavedData.LoadFromJSON;
begin
  if not FileExists(FILENAME) then
    Exit;

  fAutoStartNodes.Clear;
  fAutoUpdateNodes.Clear;

  var StrList := TStringList.Create;
  try
    StrList.LoadFromFile(FILENAME);
    var JSON := TJSONObject.ParseJSONValue(StrList.Text);

    try
      if Assigned(JSON) then begin
        var JArrAutoStartNodes := JSON.GetValue<TJSONArray>('AutoStartNodes', nil);
        var JArrAutoUpdateNodes := JSON.GetValue<TJSONArray>('AutoUpdateNodes', nil);

        if Assigned(JArrAutoStartNodes) then
          for var Item in JArrAutoStartNodes do
            fAutoStartNodes.Add(Item.Value);

        if Assigned(JArrAutoUpdateNodes) then
          for var Item in JArrAutoUpdateNodes do
            fAutoUpdateNodes.Add(Item.Value);
      end;
    finally
      JSON.Free;
    end;
  finally
    StrList.Free;
  end;
end;

procedure TSavedData.Save;
begin
  var JSON := TJSONObject.Create;

  try
    var JArrAutoStart := TJSONArray.Create;
    for var Item in fAutoStartNodes do
        JArrAutoStart.Add(Item);
    JSON.AddPair('AutoStartNodes', JArrAutoStart);

     var JArrAutoUpdate := TJSONArray.Create;
    for var Item in fAutoUpdateNodes do
        JArrAutoUpdate.Add(Item);
    JSON.AddPair('AutoUpdateNodes', JArrAutoUpdate);

    var StrList := TStringList.Create;
    try
      StrList.Text := JSON.Format;
      StrList.SaveToFile(FILENAME);
    finally
      StrList.Free;
    end;
  finally
    JSON.Free;
  end;
end;

end.
