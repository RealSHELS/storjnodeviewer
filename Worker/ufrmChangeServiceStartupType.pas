unit ufrmChangeServiceStartupType;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  System.UItypes,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxLayoutContainer, dxLayoutcxEditAdapters,
  dxLayoutControlAdapters, cxContainer, cxEdit, Vcl.Menus, Vcl.StdCtrls,
  cxButtons, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxClasses, dxLayoutControl,
  dxSkinsDefaultPainters;

type
  TfrmChangeServiceStartupType = class(TForm)
    dxLayoutControlGroup_Root: TdxLayoutGroup;
    dxLayoutControl: TdxLayoutControl;
    lblInfo: TLabel;
    laylblInfo: TdxLayoutItem;
    comboStartupTypes: TcxComboBox;
    laycomboStartupTypes: TdxLayoutItem;
    btnApply: TcxButton;
    laybtnApply: TdxLayoutItem;
    btnCancel: TcxButton;
    laybtnCancel: TdxLayoutItem;
    laygrpButtons: TdxLayoutGroup;
    procedure btnCancelClick(Sender: TObject);
    procedure btnApplyClick(Sender: TObject);
  private
    FServiceName: String;
  public
    procedure ShowForEditService(const AServiceName, AStartupTypeText: String);
  end;

var
  frmChangeServiceStartupType: TfrmChangeServiceStartupType;

implementation

uses
  ServiceManager, uDataModule;

{$R *.dfm}

{ TfrmChangeServiceStartupType }

procedure TfrmChangeServiceStartupType.btnApplyClick(Sender: TObject);
var
  StartupType: TServiceStartupType;
begin
  if comboStartupTypes.ItemIndex < 0 then begin
    MessageDlg('Not select startup type', mtError, [mbOk], 0);
    Exit;
  end;

  StartupType := TServiceStartupType(comboStartupTypes.ItemIndex);
  DM.ChangeStartupType(FServiceName, StartupType);
  DM.LoadServiceNodes;
  Close;
end;

procedure TfrmChangeServiceStartupType.btnCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmChangeServiceStartupType.ShowForEditService(const AServiceName, AStartupTypeText: String);
var
  Index: Integer;
begin
  FServiceName := AServiceName;

  lblInfo.Caption := 'Select new startup type for service ' + AServiceName.QuotedString;
  Index := comboStartupTypes.Properties.Items.IndexOf(AStartupTypeText);
  if Index >= 0 then
    comboStartupTypes.ItemIndex := Index;
  ShowModal;
end;

end.
