unit Storj.ConfigParser;

interface

uses
  System.SysUtils,
  System.Classes;

type
  EStorjConfigParserException = class(Exception);

  TStorjConfigParser = class
  private
    FConfig: TStringList;
  public
    constructor Create(const AFileName: String);
    destructor Destroy; override;

    function GetValue(const AKey: String): String;
    function GetPort(const AKey: String): String;
  end;

implementation

{ TStorjConfigParser }

constructor TStorjConfigParser.Create(const AFileName: String);
begin
  if not FileExists(AFileName) then
    raise EStorjConfigParserException.CreateFmt('File %s not exists', [AFileName]);

  FConfig := TStringList.Create;
  FConfig.LoadFromFile(AFileName);
end;

destructor TStorjConfigParser.Destroy;
begin
  FConfig.Free;
  inherited;
end;

function TStorjConfigParser.GetPort(const AKey: String): String;
begin
  Result := '';

  var V := GetValue(AKey);
  if not V.Trim.IsEmpty then
    Result := V.Substring(V.IndexOf(':') + 1).Trim;
end;

function TStorjConfigParser.GetValue(const AKey: String): String;
begin
  Result := '';

  if AKey.Trim.IsEmpty then
    Exit;

  for var S in FConfig do
    if S.StartsWith(AKey) then
      Exit(S.Substring(S.IndexOf(AKey) + AKey.Length + 1).Trim);
end;

end.
