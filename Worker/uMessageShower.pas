unit uMessageShower;

interface

type
  IMessageShower = interface
    procedure PrintMessage(const AMsg: String);
    procedure ShowError(const AMsg: String);
  end;

  TVCLMessageShower = class(TInterfacedObject, IMessageShower)
  public
    procedure PrintMessage(const AMsg: String);
    procedure ShowError(const AMsg: String);
  end;

implementation

uses
  VCL.Dialogs, System.UITypes;

{ TVCLMessageShower }

procedure TVCLMessageShower.PrintMessage(const AMsg: String);
begin
  MessageDlg(AMsg, TMsgDlgType.mtInformation, [mbOk], 0);
end;

procedure TVCLMessageShower.ShowError(const AMsg: String);
begin
  MessageDlg(AMsg, TMsgDlgType.mtError, [mbOk], 0);
end;

end.
