unit uNotificationService;

interface

uses
  System.RTTI,
  System.SysUtils,
  System.Generics.Collections;

type
  TValue = System.RTTI.TValue;

  TAppEventNotify = procedure (const Data: TValue) of object;

  TNotificationService = class
  private
    FSubscribers: TDictionary<String, TList<TAppEventNotify>>;
  public
    constructor Create;
    destructor Destroy; override;
    procedure SendMessage(const ACommand: String; const AData: TValue);
    procedure Subscribe(const ACommand: String; AEvent: TAppEventNotify);
    procedure UnSubscribe(const ACommand: String; AEvent: TAppEventNotify);
  end;

function NotificationService: TNotificationService;

implementation

var
  Instance: TNotificationService = nil;

function NotificationService: TNotificationService;
begin
  if Instance = nil then
    Instance := TNotificationService.Create;
  Result := Instance;
end;

constructor TNotificationService.Create;
begin
  FSubscribers := TDictionary<String, TList<TAppEventNotify>>.Create;
end;

destructor TNotificationService.Destroy;
var
  Pair: TPair<String, TList<TAppEventNotify>>;
begin
  for Pair in FSubscribers do
    Pair.Value.Free;

  FSubscribers.Free;
  inherited;
end;

procedure TNotificationService.SendMessage(const ACommand: String; const AData: TValue);
var
  Pair: TPair<String, TList<TAppEventNotify>>;
  Item: TAppEventNotify;
begin
  for Pair in FSubscribers do
    if SameText(Pair.Key, ACommand) then
      for Item in Pair.Value do
        try
          Item(AData);
        except
        end;
end;

procedure TNotificationService.Subscribe(const ACommand: String; AEvent: TAppEventNotify);
begin
  if not FSubscribers.ContainsKey(ACommand) then
    FSubscribers.Add(ACommand, TList<TAppEventNotify>.Create);

  if FSubscribers.Items[ACommand].IndexOf(AEvent) < 0 then
    FSubscribers.Items[ACommand].Add(AEvent);
end;

procedure TNotificationService.UnSubscribe(const ACommand: String; AEvent: TAppEventNotify);
begin
  if FSubscribers.ContainsKey(ACommand) then begin
    FSubscribers.Items[ACommand].Delete(FSubscribers.Items[ACommand].IndexOf(AEvent));
    if FSubscribers.Items[ACommand].Count = 0 then begin
      FSubscribers.Items[ACommand].Free;
      FSubscribers.Remove(ACommand);
    end;
  end;
end;

initialization

finalization
  Instance.Free;

end.
