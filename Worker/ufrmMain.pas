unit ufrmMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, Vcl.StdCtrls, cxButtons,
  dxSkinsDefaultPainters, cxControls, cxClasses, dxLayoutContainer,
  dxLayoutControl, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, dxDateRanges, Data.DB, cxDBData, cxGridLevel,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, uDataModule, dxLayoutControlAdapters,
  dxLayoutcxEditAdapters, cxContainer, cxTextEdit, cxMemo,
  cxDataControllerConditionalFormattingRulesManagerDialog, dxSkiniMaginary,
  ufrmChangeServiceStartupType, dxStatusBar, uVersionGetter, uLogger, cxSplitter,
  dxSkinBasic, dxSkinOffice2013LightGray,
  dxSkinOffice2019DarkGray, dxSkinOffice2019White, dxSkinWhiteprint,
  dxScrollbarAnnotations,
  dxSkinDevExpressStyle,
  dxSkinOffice2013DarkGray, dxLayoutLookAndFeels, dxSkinSeven,
  dxSkinSevenClassic, Vcl.ExtCtrls, cxLabel, cxCheckBox,
  ufrmSettings, dxSkinWXI, dxSkinOffice2019Black;

type
  TfrmMain = class(TForm, ILogger)
    dxLayoutControlGroup_Root: TdxLayoutGroup;
    dxLayoutControl: TdxLayoutControl;
    gridNodesDBTableView1: TcxGridDBTableView;
    gridNodesLevel1: TcxGridLevel;
    gridNodes: TcxGrid;
    laygridNodes: TdxLayoutItem;
    clmNodeServiceName: TcxGridDBColumn;
    clmNodeServiceState: TcxGridDBColumn;
    clmNodePath: TcxGridDBColumn;
    clmNodeVersion: TcxGridDBColumn;
    clmDashboardPort: TcxGridDBColumn;
    clmStoragePath: TcxGridDBColumn;
    laygrpTopControls: TdxLayoutGroup;
    btnRunService: TcxButton;
    laybtnRunNode: TdxLayoutItem;
    btnStopNode: TcxButton;
    laybtnStopNode: TdxLayoutItem;
    btnRefresh: TcxButton;
    laybtnRefresh: TdxLayoutItem;
    clmStartupType: TcxGridDBColumn;
    btnChangeStartupType: TcxButton;
    dxLayoutItem1: TdxLayoutItem;
    laygrpNodeUpdater: TdxLayoutGroup;
    dxStatusBar: TdxStatusBar;
    layStausBar: TdxLayoutItem;
    dxStatusBarContainer0: TdxStatusBarContainerControl;
    btnRefreshVersion: TcxButton;
    btnUpgrade: TcxButton;
    dxLayoutItem2: TdxLayoutItem;
    clmLogSize: TcxGridDBColumn;
    mmoLog: TcxMemo;
    laymmoLog: TdxLayoutItem;
    btnInstall: TcxButton;
    dxLayoutItem3: TdxLayoutItem;
    tmNodeAutoStart: TTimer;
    lblAutoStartNodeName: TcxLabel;
    dxLayoutItem4: TdxLayoutItem;
    clmAutoStart: TcxGridDBColumn;
    clmAutoUpdate: TcxGridDBColumn;
    tmrAutoUpdate: TTimer;
    btnSettings: TcxButton;
    dxLayoutItem5: TdxLayoutItem;
    procedure btnRunServiceClick(Sender: TObject);
    procedure btnStopNodeClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnRefreshClick(Sender: TObject);
    procedure btnChangeStartupTypeClick(Sender: TObject);
    procedure btnRefreshVersionClick(Sender: TObject);
    procedure btnUpgradeClick(Sender: TObject);
    procedure btnInstallClick(Sender: TObject);
    procedure tmNodeAutoStartTimer(Sender: TObject);
    procedure tmrAutoUpdateTimer(Sender: TObject);
    procedure btnSettingsClick(Sender: TObject);
    procedure clmAutoUpdatePropertiesEditValueChanged(Sender: TObject);
  private

  public
    procedure RecieveGithubVersion(AVersion: String);
    procedure RecieveStorjioVersion(AStorjIOVersion: TStorjIOVersion);

    procedure Log(const AMsg: String);
  end;

  //todo: 7. add info about node updater
  //todo: 8. add func to start/stop updater service and change startup type
  //todo: 12. add new node install feature
  //todo: 12.1. port suggesting
  //todo: 13. add log analyzer
  //todo: 13.1 log monitoring
  //todo: 14. add settings and app startup with OS
  //todo: 15. add send stats
  //todo: 16. add recieve commands
  //todo: config editor

  {
  �������� �������� ����:
    1. ������ ���� �� ��, ������
    2. ������ ���� �� ��, ������
    3. ������ ���� �� ��, ������
      3.1 ������� ����� ����� � Program Files
      3.2 ������� ���� ����� ������ ���������
      3.3 ������������ ������� ������
      3.4 ������� ������
    4. ������ ���� �� ��, ������
    5. ����� ���� �� ����� ������� �������
       5.1 ��������� storagenode.exe ��� ��������� ���������
  }

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

uses ufrmNewNodeInstaller;

procedure TfrmMain.btnChangeStartupTypeClick(Sender: TObject);
var
  ServiceName, ServiceStartupType: String;
begin
  ServiceName := DM.mtblNodesServiceName.AsString;
  ServiceStartupType := DM.mtblNodesStartupType.AsString;

  frmChangeServiceStartupType.ShowForEditService(ServiceName, ServiceStartupType);
end;

procedure TfrmMain.btnInstallClick(Sender: TObject);
begin
//  DM.CreateNodeStructure('D:\Stuff\storagenode_windows_amd64\', 'D:\StorjTest\Data');
  frmNewNodeInstaller.Show;
end;

procedure TfrmMain.btnRefreshClick(Sender: TObject);
begin
  DM.LoadServiceNodes;
end;

procedure TfrmMain.btnRefreshVersionClick(Sender: TObject);
begin
  GetGithubLatestVersion(RecieveGithubVersion);
  GetStorjIOLatestVersion(RecieveStorjioVersion);
end;

procedure TfrmMain.btnRunServiceClick(Sender: TObject);
begin
  DM.StartSelectedNode;
  DM.LoadServiceNodes;
end;

procedure TfrmMain.btnSettingsClick(Sender: TObject);
begin
  frmSettings.Show;
end;

procedure TfrmMain.btnStopNodeClick(Sender: TObject);
begin
  DM.StopSelectedNode;
  DM.LoadServiceNodes;
end;

procedure TfrmMain.btnUpgradeClick(Sender: TObject);
begin
  DM.UpgradeSelectedNode;
end;

procedure TfrmMain.clmAutoUpdatePropertiesEditValueChanged(Sender: TObject);
begin
  DM.SaveData;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  DM.LoadServiceNodes;
  GetGithubLatestVersion(RecieveGithubVersion);
  GetStorjIOLatestVersion(RecieveStorjioVersion);

  TLogSaver.GetInstance.AddLoger(Self);
end;

procedure TfrmMain.Log(const AMsg: String);
begin
  mmoLog.Lines.Add(AMsg);
end;

procedure TfrmMain.RecieveGithubVersion(AVersion: String);
begin
  DM.GithubVersion := AVersion;
  dxStatusBar.Panels[1].Text := 'Github: v' + AVersion;
end;

procedure TfrmMain.RecieveStorjioVersion(AStorjIOVersion: TStorjIOVersion);
begin
  dxStatusBar.Panels[2].Text := 'Storj.io: v' + AStorjIOVersion.Version + ', Cursor: ' + AStorjIOVersion.Cursor;
end;

procedure TfrmMain.tmNodeAutoStartTimer(Sender: TObject);
begin
  DM.AutoStartNodes;
end;

procedure TfrmMain.tmrAutoUpdateTimer(Sender: TObject);
begin
  DM.UpgradeCheckedNodes;
end;

end.
