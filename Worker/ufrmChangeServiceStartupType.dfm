object frmChangeServiceStartupType: TfrmChangeServiceStartupType
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Change service startup type'
  ClientHeight = 114
  ClientWidth = 401
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object dxLayoutControl: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 401
    Height = 114
    Align = alClient
    TabOrder = 0
    ExplicitLeft = 224
    ExplicitTop = 40
    ExplicitWidth = 300
    ExplicitHeight = 250
    object lblInfo: TLabel
      Left = 10
      Top = 10
      Width = 381
      Height = 13
      Caption = 'lblInfo'
    end
    object comboStartupTypes: TcxComboBox
      Left = 76
      Top = 29
      Properties.Items.Strings = (
        'Auto (delayed start)'
        'Auto'
        'Manual'
        'Disabled')
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 0
      Text = 'Choose startup type'
      Width = 315
    end
    object btnApply: TcxButton
      Left = 122
      Top = 79
      Width = 75
      Height = 25
      Caption = 'Apply'
      TabOrder = 1
      OnClick = btnApplyClick
    end
    object btnCancel: TcxButton
      Left = 203
      Top = 79
      Width = 75
      Height = 25
      Caption = 'Cancel'
      TabOrder = 2
      OnClick = btnCancelClick
    end
    object dxLayoutControlGroup_Root: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avClient
      ButtonOptions.Buttons = <>
      Hidden = True
      ItemIndex = 2
      ShowBorder = False
      Index = -1
    end
    object laylblInfo: TdxLayoutItem
      Parent = dxLayoutControlGroup_Root
      CaptionOptions.Text = 'Label1'
      CaptionOptions.Visible = False
      Control = lblInfo
      ControlOptions.AutoColor = True
      ControlOptions.OriginalHeight = 13
      ControlOptions.OriginalWidth = 31
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object laycomboStartupTypes: TdxLayoutItem
      Parent = dxLayoutControlGroup_Root
      CaptionOptions.Text = 'Startup type'
      Control = comboStartupTypes
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object laybtnApply: TdxLayoutItem
      Parent = laygrpButtons
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnApply
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object laybtnCancel: TdxLayoutItem
      Parent = laygrpButtons
      CaptionOptions.Text = 'cxButton2'
      CaptionOptions.Visible = False
      Control = btnCancel
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object laygrpButtons: TdxLayoutGroup
      Parent = dxLayoutControlGroup_Root
      AlignHorz = ahCenter
      AlignVert = avBottom
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ItemIndex = 1
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 2
    end
  end
end
