unit Unit3;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, uUseMQTTDLL;

type
  TForm3 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Memo1: TMemo;
    Button6: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
  private
    fMQTTDL: TMQTTDLL;

    procedure OnMsg(aTopic: PChar; aMessage: PChar);
    procedure OnMon(aMessage: PChar);
    procedure OnOnline;
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

{$R *.dfm}

procedure TForm3.Button1Click(Sender: TObject);
begin
  fMQTTDL.Connect('broker.hivemq.com', 1883);
end;

procedure TForm3.Button2Click(Sender: TObject);
begin
  var IsConnected := fMQTTDL.IsConnected;

  if IsConnected then
    ShowMessage('Connected')
  else
    ShowMessage('Disconnected');
end;

procedure TForm3.Button3Click(Sender: TObject);
begin
  fMQTTDL.Publish('/RealSHELS/PCRebooters/1FC4845B/Receive', 'echo');
end;

procedure TForm3.Button4Click(Sender: TObject);
begin
  fMQTTDL := TMQTTDLL.Create('UseMQTT.dll');
  fMQTTDL.SetOnMsgCallback(OnMsg);
  fMQTTDL.SetOnMonCallback(OnMon);
  fMQTTDL.SetOnOnlineCallback(OnOnline);
end;

procedure TForm3.Button5Click(Sender: TObject);
begin
  fMQTTDL.Free;
end;

procedure TForm3.OnMon(aMessage: PChar);
begin
  memo1.Lines.Add(String(aMessage));
end;

procedure TForm3.OnMsg(aTopic, aMessage: PChar);
begin
  memo1.Lines.Add(String(aTopic) + ': ' + String(aMessage));
end;

procedure TForm3.OnOnline;
begin
  fMQTTDL.Subscribe('/RealSHELS/PCRebooters/1FC4845B/Send');
end;

end.
