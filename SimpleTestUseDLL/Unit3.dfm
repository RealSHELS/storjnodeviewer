object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'Form3'
  ClientHeight = 375
  ClientWidth = 673
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 56
    Top = 32
    Width = 75
    Height = 25
    Caption = 'Connect'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 56
    Top = 72
    Width = 75
    Height = 25
    Caption = 'IsConnected'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 56
    Top = 112
    Width = 75
    Height = 25
    Caption = 'Publish'
    TabOrder = 2
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 472
    Top = 32
    Width = 75
    Height = 25
    Caption = 'Create'
    TabOrder = 3
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 472
    Top = 88
    Width = 75
    Height = 25
    Caption = 'Destroy'
    TabOrder = 4
    OnClick = Button5Click
  end
  object Memo1: TMemo
    Left = 16
    Top = 143
    Width = 417
    Height = 224
    Lines.Strings = (
      'Memo1')
    TabOrder = 5
  end
  object Button6: TButton
    Left = 208
    Top = 32
    Width = 75
    Height = 25
    Caption = 'Set callback'
    TabOrder = 6
  end
end
