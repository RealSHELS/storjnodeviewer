object DM: TDM
  Height = 377
  Width = 553
  object FDConnection: TFDConnection
    Left = 264
    Top = 200
  end
  object mtblDBValidation: TFDMemTable
    Active = True
    FieldDefs = <
      item
        Name = 'DBName'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'IsValid'
        DataType = ftBoolean
      end>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 80
    Top = 48
    object mtblDBValidationDBName: TStringField
      FieldName = 'DBName'
      Size = 255
    end
    object mtblDBValidationIsValid: TBooleanField
      FieldName = 'IsValid'
    end
  end
  object dtsrcDBValidation: TDataSource
    DataSet = mtblDBValidation
    Left = 80
    Top = 120
  end
  object FDSQLiteValidate: TFDSQLiteValidate
    DriverLink = FDPhysSQLiteDriverLink
    Left = 80
    Top = 232
  end
  object FDPhysSQLiteDriverLink: TFDPhysSQLiteDriverLink
    Left = 320
    Top = 296
  end
  object dtsrcUsedSpace: TDataSource
    DataSet = qryUsedSpace
    Left = 280
    Top = 56
  end
  object qryUsedSpace: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'with cte_RealStored as ('
      '     select Sum(Total) as RealTotal from piece_space_used '
      
        '     inner join satellites.satellites on satellites.satellites.n' +
        'ode_id = piece_space_used.satellite_id'
      ')'
      'select round(total / 1000000000, 0) as stored,'
      '       case when length(hash) = 0 then '#39'total'#39
      '            when hash = '#39'7472617368746F74616C'#39' then '#39'trash'#39
      '            else address'
      '       end as satellite,'
      
        '       round(cte_RealStored.RealTotal / 1000000000, 0) as real_s' +
        'tored         '
      'from ('
      
        'SELECT total, address, cast(HEX(satellite_id) as varchar) as has' +
        'h'
      '       FROM piece_space_used'
      
        '       left join satellites.satellites as s on s.node_id = satel' +
        'lite_id'
      ')'
      'join cte_RealStored')
    Left = 272
    Top = 120
  end
  object qryCommand: TFDQuery
    Connection = FDConnection
    Left = 408
    Top = 72
  end
end
