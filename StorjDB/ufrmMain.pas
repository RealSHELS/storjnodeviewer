unit ufrmMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Dialogs,
  cxControls, cxGraphics, cxLookAndFeelPainters, cxLookAndFeels,
  dxSkinsCore, cxContainer, cxEdit, dxNavBar, cxClasses,
  dxLayoutLookAndFeels, dxLayoutContainer, dxLayoutControl,
  dxSkinsForm, dxSkinsFluentDesignForm, dxSkinOffice2019Colorful, dxSkinBasic,
  dxSkinOffice2013LightGray, dxSkinOffice2019Black, dxSkinOffice2019DarkGray,
  dxSkinOffice2019White, dxSkinWhiteprint, dxSkinWXI, dxCore, dxNavBarCollns,
  dxNavBarBase, System.ImageList, Vcl.ImgList, cxImageList,
  dxLayoutControlAdapters, Vcl.ExtCtrls, Vcl.WinXPanels, Vcl.StdCtrls,
  cxTextEdit, cxMaskEdit, cxButtonEdit, dxLayoutcxEditAdapters, Vcl.Menus,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  dxDateRanges, dxScrollbarAnnotations, Data.DB, cxDBData, cxGridLevel,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, cxButtons, cxCustomListBox, cxListBox, uDM, cxLabel, cxCurrencyEdit,
  cxDBEdit, dxSkiniMaginary, dxSkinSeven, dxSkinSevenClassic;

type
  TfrmMain = class(TdxFluentDesignForm)
    dxNavBar: TdxNavBar;
    dxSkinController: TdxSkinController;
    dxLayoutLookAndFeelList: TdxLayoutLookAndFeelList;
    dxLayoutSkinLookAndFeel1: TdxLayoutSkinLookAndFeel;
    dxNavBarGroup: TdxNavBarGroup;
    navbaritemSettings: TdxNavBarItem;
    navbaritemValidate: TdxNavBarItem;
    navbaritemUsed: TdxNavBarItem;
    cxImageList: TcxImageList;
    CardPanel: TCardPanel;
    cardSettings: TCard;
    cardUsedSpace: TCard;
    cardValidate: TCard;
    laycontrolSettingsGroup_Root: TdxLayoutGroup;
    laycontrolSettings: TdxLayoutControl;
    edtDBPath: TcxButtonEdit;
    layedtDBPath: TdxLayoutItem;
    FileOpenDialog: TFileOpenDialog;
    dxLayoutControlGroup_Root: TdxLayoutGroup;
    dxLayoutControl: TdxLayoutControl;
    dxLayoutItem1: TdxLayoutItem;
    laycontrolValidateGroup_Root: TdxLayoutGroup;
    laycontrolValidate: TdxLayoutControl;
    btnValidateDBs: TcxButton;
    layValidateDBs: TdxLayoutItem;
    gridDBValidationDBTableView: TcxGridDBTableView;
    gridDBValidationLevel1: TcxGridLevel;
    gridDBValidation: TcxGrid;
    laygridDBValidation: TdxLayoutItem;
    laycontrolUsedSpaceGroup_Root: TdxLayoutGroup;
    laycontrolUsedSpace: TdxLayoutControl;
    btnDisplayUsedSpace: TcxButton;
    laybtnDisplayUsedSpace: TdxLayoutItem;
    btnUpdateUsed: TcxButton;
    laybtnUpdateUsed: TdxLayoutItem;
    gridUsedSpaceDBTableView: TcxGridDBTableView;
    gridUsedSpaceLevel1: TcxGridLevel;
    gridUsedSpace: TcxGrid;
    laygridUsedSpace: TdxLayoutItem;
    listboxFiles: TcxListBox;
    laylistboxFiles: TdxLayoutItem;
    clmDBValidationDBName: TcxGridDBColumn;
    clmDBValidationIsValid: TcxGridDBColumn;
    clmUsedSpaceStored: TcxGridDBColumn;
    clmUsedSpaceSatellite: TcxGridDBColumn;
    edtdbRealStored: TcxDBCurrencyEdit;
    layedtdbRealStored: TdxLayoutItem;
    dxLayoutGroup1: TdxLayoutGroup;
    procedure navbaritemSettingsClick(Sender: TObject);
    procedure edtDBPathPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure dxFluentDesignFormCreate(Sender: TObject);
    procedure clmDBValidationIsValidCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure btnValidateDBsClick(Sender: TObject);
    procedure clmDBValidationIsValidGetDisplayText(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AText: string);
    procedure btnDisplayUsedSpaceClick(Sender: TObject);
    procedure edtDBPathKeyPress(Sender: TObject; var Key: Char);
    procedure btnUpdateUsedClick(Sender: TObject);
  private
    procedure DisplayListDBFiles;
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

procedure TfrmMain.btnDisplayUsedSpaceClick(Sender: TObject);
begin
  DM.LoadUsedSpace;
  gridUsedSpaceDBTableView.ApplyBestFit;
end;

procedure TfrmMain.btnUpdateUsedClick(Sender: TObject);
begin
  DM.UpdateUsedSpace;
  gridUsedSpaceDBTableView.ApplyBestFit;
end;

procedure TfrmMain.btnValidateDBsClick(Sender: TObject);
begin
  DM.ValidateDBs;
end;

procedure TfrmMain.clmDBValidationIsValidCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  if Boolean(AViewInfo.Value) then
    ACanvas.Brush.Color := $d4f9d4
  else
    ACanvas.Brush.Color := RGB(255, 236, 230);
end;

procedure TfrmMain.clmDBValidationIsValidGetDisplayText(Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord; var AText: string);
begin
  if AText.ToBoolean then
    AText := 'Good'
  else
    AText := 'Bad';
end;

procedure TfrmMain.DisplayListDBFiles;
var
  Path: String;
begin
  Path := edtDBPath.Text;

  laylistboxFiles.Caption := 'DB files for path ' + Path;
  DM.SetPath(Path);
  listboxFiles.Items.Clear();
  for var Item in DM.GetDBFiles do
    listboxFiles.Items.Add(Item);
end;

procedure TfrmMain.dxFluentDesignFormCreate(Sender: TObject);
begin
  CardPanel.ActiveCardIndex := 0;
  DisplayListDBFiles;
end;

procedure TfrmMain.edtDBPathKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
    DisplayListDBFiles;
end;

procedure TfrmMain.edtDBPathPropertiesButtonClick(Sender: TObject; AButtonIndex: Integer);
begin
  if FileOpenDialog.Execute then begin
    edtDBPath.Text := FileOpenDialog.FileName;
    DisplayListDBFiles;
  end;
end;

procedure TfrmMain.navbaritemSettingsClick(Sender: TObject);
begin
  CardPanel.ActiveCardIndex := (Sender as TdxNavBarItem).Tag;
end;

end.
