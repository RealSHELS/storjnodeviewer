program StorjDB;

{$R *.dres}

uses
  System.SysUtils,
  System.IOUtils,
  System.Classes,
  Vcl.Forms,
  WinAPI.Windows,
  ufrmMain in 'ufrmMain.pas' {frmMain: TdxFluentDesignForm},
  uDM in 'uDM.pas' {DM: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;

  var FileName := ExtractFilePath(Application.ExeName) + 'sqlite3.dll';
  if not TFile.Exists(FileName) then begin
    var ResStream := TResourceStream.Create(HInstance, 'sqlite3_dll', RT_RCDATA);
    try
      ResStream.Position := 0;
      ResStream.SaveToFile(FileName);
    finally
      ResStream.Free;
    end;
  end;

  Application.CreateForm(TDM, DM);
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
