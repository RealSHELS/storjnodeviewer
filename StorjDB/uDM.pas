unit uDM;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait,
  Data.DB, FireDAC.Comp.Client, System.IOUtils, FireDAC.Stan.Param,
  FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Phys.SQLite,
  FireDAC.Stan.ExprFuncs, FireDAC.Phys.SQLiteWrapper.Stat,
  FireDAC.Phys.SQLiteDef, FireDAC.DApt;

type
  TDM = class(TDataModule)
    FDConnection: TFDConnection;
    mtblDBValidation: TFDMemTable;
    dtsrcDBValidation: TDataSource;
    mtblDBValidationDBName: TStringField;
    mtblDBValidationIsValid: TBooleanField;
    FDSQLiteValidate: TFDSQLiteValidate;
    FDPhysSQLiteDriverLink: TFDPhysSQLiteDriverLink;
    dtsrcUsedSpace: TDataSource;
    qryUsedSpace: TFDQuery;
    qryCommand: TFDQuery;
  private
    fPath: String;

    procedure ConnectToDB;
  public
    procedure SetPath(const aPath: String);

    function GetDBFiles(const aIsSimple: Boolean = True): TArray<String>;
    procedure ValidateDBs;

    procedure LoadUsedSpace;
    procedure UpdateUsedSpace;
  end;

var
  DM: TDM;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TDM }

procedure TDM.ConnectToDB;
begin
  if FDConnection.Connected then
    Exit;

  FDConnection.DriverName := 'SQLite';
  FDConnection.Params.Database := IncludeTrailingPathDelimiter(fPath) + 'piece_spaced_used.db';
  FDConnection.Connected := True;
  FDConnection.ExecSQL('ATTACH ' + QuotedStr(IncludeTrailingPathDelimiter(fPath) + 'satellites.db') + ' as satellites');
end;

function TDM.GetDBFiles(const aIsSimple: Boolean = True): TArray<String>;
begin
  Result := [];

  if TDirectory.Exists(fPath) then begin
    Result := TDirectory.GetFiles(fPath, '*.db');

    if aIsSimple and (Length(Result) > 0) then
      for var i := 0 to Length(Result) - 1 do
        Result[i] := ExtractFileName(Result[i]);
  end;
end;

procedure TDM.LoadUsedSpace;
begin
  ConnectToDB;

  if qryUsedSpace.Active then
    qryUsedSpace.Active := False;

  qryUsedSpace.Open;
end;

procedure TDM.SetPath(const aPath: String);
begin
  FDConnection.Connected := False;
  fPath := aPath;
end;

procedure TDM.UpdateUsedSpace;
begin
  ConnectToDB;

  qryCommand.ExecSQL('update piece_space_used ' +
                      'set total = (select Sum(total) from piece_space_used ' +
                                    'inner join satellites.satellites on satellites.satellites.node_id = piece_space_used.satellite_id ' +
                                   '), ' +
                      'content_size = (select Sum(content_size) from piece_space_used ' +
                                      'inner join satellites.satellites on satellites.satellites.node_id = piece_space_used.satellite_id ' +
                                     ') ' +
                      'where satellite_id is null');
  LoadUsedSpace;
end;

procedure TDM.ValidateDBs;
begin
  mtblDBValidation.DisableControls;
  try
    mtblDBValidation.EmptyDataSet;

    var dbs := GetDBFiles(False);
    for var Item in dbs do begin
      FDSQLiteValidate.Database := Item;
      var Result := FDSQLiteValidate.CheckOnly;

      mtblDBValidation.Append;
      mtblDBValidation['DBName'] := ExtractFileName(Item);
      mtblDBValidation['IsValid'] := Result;
      mtblDBValidation.Post;
    end;
  finally
    mtblDBValidation.EnableControls;
  end;
end;

end.
